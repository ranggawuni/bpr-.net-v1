﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPrintDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If gvTemp IsNot Nothing Then
                    gvTemp.Dispose()
                    gvTemp = Nothing
                End If
                If gTemp IsNot Nothing Then
                    gTemp.Dispose()
                    gTemp = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nBottomMargin = New DevExpress.XtraEditors.TextEdit()
        Me.nRightMargin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nTopMargin = New DevExpress.XtraEditors.TextEdit()
        Me.nLeftMargin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.optPrint = New DevExpress.XtraEditors.RadioGroup()
        Me.cbPaper = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.nPaperWidth = New DevExpress.XtraEditors.TextEdit()
        Me.nPaperHeight = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdProses = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.nBottomMargin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nRightMargin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTopMargin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLeftMargin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.optPrint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPaper.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPaperWidth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPaperHeight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.nBottomMargin)
        Me.PanelControl2.Controls.Add(Me.nRightMargin)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nTopMargin)
        Me.PanelControl2.Controls.Add(Me.nLeftMargin)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(376, 58)
        Me.PanelControl2.TabIndex = 13
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(329, 34)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl8.TabIndex = 12
        Me.LabelControl8.Text = "mm"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(329, 12)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl7.TabIndex = 11
        Me.LabelControl7.Text = "mm"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(125, 34)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "mm"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(125, 12)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "mm"
        '
        'nBottomMargin
        '
        Me.nBottomMargin.Location = New System.Drawing.Point(284, 8)
        Me.nBottomMargin.Name = "nBottomMargin"
        Me.nBottomMargin.Size = New System.Drawing.Size(39, 20)
        Me.nBottomMargin.TabIndex = 8
        '
        'nRightMargin
        '
        Me.nRightMargin.Location = New System.Drawing.Point(284, 31)
        Me.nRightMargin.Name = "nRightMargin"
        Me.nRightMargin.Size = New System.Drawing.Size(39, 20)
        Me.nRightMargin.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(211, 34)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Right Margin"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(211, 11)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Bottom Margin"
        '
        'nTopMargin
        '
        Me.nTopMargin.Location = New System.Drawing.Point(80, 8)
        Me.nTopMargin.Name = "nTopMargin"
        Me.nTopMargin.Size = New System.Drawing.Size(39, 20)
        Me.nTopMargin.TabIndex = 4
        '
        'nLeftMargin
        '
        Me.nLeftMargin.Location = New System.Drawing.Point(80, 31)
        Me.nLeftMargin.Name = "nLeftMargin"
        Me.nLeftMargin.Size = New System.Drawing.Size(39, 20)
        Me.nLeftMargin.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Left Margin"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Top Margin"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.optPrint)
        Me.PanelControl1.Controls.Add(Me.cbPaper)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.nPaperWidth)
        Me.PanelControl1.Controls.Add(Me.nPaperHeight)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 67)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(376, 145)
        Me.PanelControl1.TabIndex = 14
        '
        'optPrint
        '
        Me.optPrint.Location = New System.Drawing.Point(80, 5)
        Me.optPrint.Name = "optPrint"
        Me.optPrint.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Preview"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "P&df"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ms. Excel (xl&s)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ms. Excel (&xlsx)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ms. &Excel (csv)")})
        Me.optPrint.Size = New System.Drawing.Size(291, 67)
        Me.optPrint.TabIndex = 275
        '
        'cbPaper
        '
        Me.cbPaper.Location = New System.Drawing.Point(80, 76)
        Me.cbPaper.Name = "cbPaper"
        Me.cbPaper.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPaper.Size = New System.Drawing.Size(191, 20)
        Me.cbPaper.TabIndex = 274
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(125, 126)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl9.TabIndex = 12
        Me.LabelControl9.Text = "inc"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(125, 104)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl10.TabIndex = 11
        Me.LabelControl10.Text = "inc"
        '
        'nPaperWidth
        '
        Me.nPaperWidth.Location = New System.Drawing.Point(80, 100)
        Me.nPaperWidth.Name = "nPaperWidth"
        Me.nPaperWidth.Size = New System.Drawing.Size(39, 20)
        Me.nPaperWidth.TabIndex = 8
        '
        'nPaperHeight
        '
        Me.nPaperHeight.Location = New System.Drawing.Point(80, 123)
        Me.nPaperHeight.Name = "nPaperHeight"
        Me.nPaperHeight.Size = New System.Drawing.Size(39, 20)
        Me.nPaperHeight.TabIndex = 7
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(7, 126)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl13.TabIndex = 6
        Me.LabelControl13.Text = "Height"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(7, 103)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl14.TabIndex = 5
        Me.LabelControl14.Text = "Width"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(7, 79)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl15.TabIndex = 2
        Me.LabelControl15.Text = "Paper Size"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl16.TabIndex = 1
        Me.LabelControl16.Text = "Print To"
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.cmdKeluar)
        Me.PanelControl5.Controls.Add(Me.cmdProses)
        Me.PanelControl5.Location = New System.Drawing.Point(2, 218)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(376, 33)
        Me.PanelControl5.TabIndex = 15
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(301, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(70, 24)
        Me.cmdKeluar.TabIndex = 1
        Me.cmdKeluar.Text = "Keluar"
        '
        'cmdProses
        '
        Me.cmdProses.Location = New System.Drawing.Point(225, 5)
        Me.cmdProses.Name = "cmdProses"
        Me.cmdProses.Size = New System.Drawing.Size(70, 24)
        Me.cmdProses.TabIndex = 0
        Me.cmdProses.Text = "Proses"
        '
        'FrmPrintDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 254)
        Me.Controls.Add(Me.PanelControl5)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "FrmPrintDialog"
        Me.Text = "Print Dialog"
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.nBottomMargin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nRightMargin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTopMargin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLeftMargin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.optPrint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPaper.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPaperWidth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPaperHeight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBottomMargin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nRightMargin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTopMargin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nLeftMargin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPaperWidth As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPaperHeight As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdProses As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbPaper As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents optPrint As DevExpress.XtraEditors.RadioGroup
End Class
