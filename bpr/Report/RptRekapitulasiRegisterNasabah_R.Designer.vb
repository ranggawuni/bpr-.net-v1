﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class RptRekapitulasiRegisterNasabah_R
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If cDataSource IsNot Nothing Then
                cDataSource.Dispose()
                cDataSource = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.cTime = New DevExpress.XtraReports.UI.XRLabel()
        Me.cPeriode = New DevExpress.XtraReports.UI.XRLabel()
        Me.cNamaLaporan = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.cNo = New DevExpress.XtraReports.UI.XRTableCell()
        Me.cKode = New DevExpress.XtraReports.UI.XRTableCell()
        Me.cNama = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nTabunganJumlah = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nTabunganAktif = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nTabunganNonAktif = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nDepositoJumlah = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nDepositoAktif = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nDepositoNonAktif = New DevExpress.XtraReports.UI.XRTableCell()
        Me.White = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.TableHeaderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.LavenderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.LightBlue = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.nKreditJumlah = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nKreditAktif = New DevExpress.XtraReports.UI.XRTableCell()
        Me.nKreditNonAktif = New DevExpress.XtraReports.UI.XRTableCell()
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'TableStyle
        '
        Me.TableStyle.BackColor = System.Drawing.Color.White
        Me.TableStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TableStyle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
        Me.TableStyle.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.TableStyle.Font = New System.Drawing.Font("Calibri", 36.0!)
        Me.TableStyle.ForeColor = System.Drawing.Color.DarkRed
        Me.TableStyle.Name = "TableStyle"
        Me.TableStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254.0!)
        '
        'PageHeader
        '
        Me.PageHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.cTime, Me.cPeriode, Me.cNamaLaporan})
        Me.PageHeader.Dpi = 254.0!
        Me.PageHeader.HeightF = 299.8864!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.StylePriority.UseBackColor = False
        Me.PageHeader.StylePriority.UseBorderColor = False
        '
        'cTime
        '
        Me.cTime.Dpi = 254.0!
        Me.cTime.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.cTime.LocationFloat = New DevExpress.Utils.PointFloat(2485.729!, 0.0!)
        Me.cTime.Name = "cTime"
        Me.cTime.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.cTime.SizeF = New System.Drawing.SizeF(304.2709!, 61.49477!)
        Me.cTime.StylePriority.UseFont = False
        Me.cTime.StylePriority.UseTextAlignment = False
        Me.cTime.Text = "Format(Now, ""dd-mm-yyyy hh:mm:ss"")"
        Me.cTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'cPeriode
        '
        Me.cPeriode.BorderWidth = 0.0!
        Me.cPeriode.Dpi = 254.0!
        Me.cPeriode.Font = New System.Drawing.Font("Calibri", 20.0!)
        Me.cPeriode.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 203.6536!)
        Me.cPeriode.Name = "cPeriode"
        Me.cPeriode.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.cPeriode.SizeF = New System.Drawing.SizeF(2790.0!, 96.23276!)
        Me.cPeriode.StyleName = "TableStyle"
        Me.cPeriode.StylePriority.UseFont = False
        Me.cPeriode.Text = "Periode"
        '
        'cNamaLaporan
        '
        Me.cNamaLaporan.BorderWidth = 0.0!
        Me.cNamaLaporan.Dpi = 254.0!
        Me.cNamaLaporan.Font = New System.Drawing.Font("Calibri", 20.0!)
        Me.cNamaLaporan.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 107.4209!)
        Me.cNamaLaporan.Name = "cNamaLaporan"
        Me.cNamaLaporan.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.cNamaLaporan.SizeF = New System.Drawing.SizeF(2790.0!, 96.23276!)
        Me.cNamaLaporan.StyleName = "TableStyle"
        Me.cNamaLaporan.StylePriority.UseFont = False
        Me.cNamaLaporan.Text = "Laporan Register Nasabah"
        '
        'xrTable1
        '
        Me.xrTable1.BorderColor = System.Drawing.Color.White
        Me.xrTable1.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable1.Dpi = 254.0!
        Me.xrTable1.EvenStyleName = "LavenderStyle"
        Me.xrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable1.Name = "xrTable1"
        Me.xrTable1.OddStyleName = "White"
        Me.xrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow1})
        Me.xrTable1.SizeF = New System.Drawing.SizeF(2790.0!, 63.5!)
        Me.xrTable1.StylePriority.UseBorderColor = False
        Me.xrTable1.StylePriority.UseBorders = False
        '
        'xrTableRow1
        '
        Me.xrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.cNo, Me.cKode, Me.cNama, Me.nTabunganJumlah, Me.nTabunganAktif, Me.nTabunganNonAktif, Me.nDepositoJumlah, Me.nDepositoAktif, Me.nDepositoNonAktif, Me.nKreditJumlah, Me.nKreditAktif, Me.nKreditNonAktif})
        Me.xrTableRow1.Dpi = 254.0!
        Me.xrTableRow1.Name = "xrTableRow1"
        Me.xrTableRow1.Weight = 1.0R
        '
        'cNo
        '
        Me.cNo.Dpi = 254.0!
        Me.cNo.Name = "cNo"
        Me.cNo.StylePriority.UseTextAlignment = False
        Me.cNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.cNo.Weight = 0.3536824954485055R
        '
        'cKode
        '
        Me.cKode.Dpi = 254.0!
        Me.cKode.Name = "cKode"
        Me.cKode.StylePriority.UseTextAlignment = False
        Me.cKode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.cKode.Weight = 0.55830004227662644R
        '
        'cNama
        '
        Me.cNama.Dpi = 254.0!
        Me.cNama.Name = "cNama"
        Me.cNama.StylePriority.UseTextAlignment = False
        Me.cNama.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.cNama.Weight = 2.2314931014018522R
        '
        'nTabunganJumlah
        '
        Me.nTabunganJumlah.Dpi = 254.0!
        Me.nTabunganJumlah.Name = "nTabunganJumlah"
        Me.nTabunganJumlah.StylePriority.UseTextAlignment = False
        Me.nTabunganJumlah.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nTabunganJumlah.Weight = 0.45031785622964043R
        '
        'nTabunganAktif
        '
        Me.nTabunganAktif.Dpi = 254.0!
        Me.nTabunganAktif.Name = "nTabunganAktif"
        Me.nTabunganAktif.StylePriority.UseTextAlignment = False
        Me.nTabunganAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nTabunganAktif.Weight = 0.33139875198700763R
        '
        'nTabunganNonAktif
        '
        Me.nTabunganNonAktif.Dpi = 254.0!
        Me.nTabunganNonAktif.Name = "nTabunganNonAktif"
        Me.nTabunganNonAktif.StylePriority.UseTextAlignment = False
        Me.nTabunganNonAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nTabunganNonAktif.Weight = 0.31156355828572957R
        '
        'nDepositoJumlah
        '
        Me.nDepositoJumlah.Dpi = 254.0!
        Me.nDepositoJumlah.Name = "nDepositoJumlah"
        Me.nDepositoJumlah.StylePriority.UseTextAlignment = False
        Me.nDepositoJumlah.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nDepositoJumlah.Weight = 0.43815205600786356R
        '
        'nDepositoAktif
        '
        Me.nDepositoAktif.Dpi = 254.0!
        Me.nDepositoAktif.Name = "nDepositoAktif"
        Me.nDepositoAktif.StylePriority.UseTextAlignment = False
        Me.nDepositoAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nDepositoAktif.Weight = 0.28704606038412495R
        '
        'nDepositoNonAktif
        '
        Me.nDepositoNonAktif.Dpi = 254.0!
        Me.nDepositoNonAktif.Name = "nDepositoNonAktif"
        Me.nDepositoNonAktif.StylePriority.UseTextAlignment = False
        Me.nDepositoNonAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nDepositoNonAktif.Weight = 0.30521681181262494R
        '
        'White
        '
        Me.White.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(228, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.White.BorderWidth = 0.0!
        Me.White.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.White.ForeColor = System.Drawing.Color.Black
        Me.White.Name = "White"
        Me.White.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254.0!)
        '
        'Detail
        '
        Me.Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable1})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 64.0!
        Me.Detail.Name = "Detail"
        Me.Detail.OddStyleName = "LightBlue"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageFooter
        '
        Me.PageFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1})
        Me.PageFooter.Dpi = 254.0!
        Me.PageFooter.HeightF = 60.85417!
        Me.PageFooter.Name = "PageFooter"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Format = "Page {0} of {1}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(2572.042!, 0.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(216.9584!, 47.83664!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'TopMargin
        '
        Me.TopMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 3.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.StylePriority.UseBackColor = False
        Me.TopMargin.StylePriority.UseBorderColor = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TableHeaderStyle
        '
        Me.TableHeaderStyle.BackColor = System.Drawing.Color.Maroon
        Me.TableHeaderStyle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold)
        Me.TableHeaderStyle.ForeColor = System.Drawing.Color.White
        Me.TableHeaderStyle.Name = "TableHeaderStyle"
        Me.TableHeaderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2})
        Me.GroupHeader1.Dpi = 254.0!
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("ProductName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 173.2499!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'xrTable2
        '
        Me.xrTable2.BackColor = System.Drawing.Color.White
        Me.xrTable2.BorderColor = System.Drawing.Color.White
        Me.xrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTable2.Dpi = 254.0!
        Me.xrTable2.Font = New System.Drawing.Font("Segoe UI", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrTable2.ForeColor = System.Drawing.Color.Black
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow2, Me.XrTableRow3})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(2790.0!, 173.2499!)
        Me.xrTable2.StyleName = "TableHeaderStyle"
        Me.xrTable2.StylePriority.UseBorderColor = False
        Me.xrTable2.StylePriority.UseBorders = False
        Me.xrTable2.StylePriority.UseFont = False
        Me.xrTable2.StylePriority.UseTextAlignment = False
        Me.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell4, Me.xrTableCell3, Me.xrTableCell7, Me.xrTableCell8, Me.xrTableCell12, Me.XrTableCell10})
        Me.xrTableRow2.Dpi = 254.0!
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 1.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.BorderColor = System.Drawing.Color.White
        Me.xrTableCell4.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell4.Dpi = 254.0!
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.ProcessDuplicatesTarget = DevExpress.XtraReports.UI.ProcessDuplicatesTarget.Tag
        Me.xrTableCell4.RowSpan = 2
        Me.xrTableCell4.StylePriority.UseBorderColor = False
        Me.xrTableCell4.StylePriority.UseBorders = False
        Me.xrTableCell4.StylePriority.UseTextAlignment = False
        Me.xrTableCell4.Tag = "1"
        Me.xrTableCell4.Text = "No."
        Me.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.xrTableCell4.Weight = 0.44255612884790385R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.BorderColor = System.Drawing.Color.White
        Me.xrTableCell3.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell3.Dpi = 254.0!
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.RowSpan = 2
        Me.xrTableCell3.StylePriority.UseBorderColor = False
        Me.xrTableCell3.StylePriority.UseBorders = False
        Me.xrTableCell3.Tag = "Kode"
        Me.xrTableCell3.Text = "Kode"
        Me.xrTableCell3.Weight = 0.69859050873489537R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.BorderColor = System.Drawing.Color.White
        Me.xrTableCell7.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell7.Dpi = 254.0!
        Me.xrTableCell7.Font = New System.Drawing.Font("Segoe UI", 14.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.RowSpan = 2
        Me.xrTableCell7.StyleName = "TableHeaderStyle"
        Me.xrTableCell7.StylePriority.UseBorderColor = False
        Me.xrTableCell7.StylePriority.UseBorders = False
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.Text = "Nama Nasabah"
        Me.xrTableCell7.Weight = 2.7922249158604107R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.BorderColor = System.Drawing.Color.White
        Me.xrTableCell8.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell8.Dpi = 254.0!
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.StylePriority.UseBorderColor = False
        Me.xrTableCell8.StylePriority.UseBorders = False
        Me.xrTableCell8.Text = "Tabungan"
        Me.xrTableCell8.Weight = 1.3680004685098937R
        '
        'xrTableCell12
        '
        Me.xrTableCell12.BorderColor = System.Drawing.Color.White
        Me.xrTableCell12.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrTableCell12.Dpi = 254.0!
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.StylePriority.UseBorderColor = False
        Me.xrTableCell12.StylePriority.UseBorders = False
        Me.xrTableCell12.Text = "Deposito"
        Me.xrTableCell12.Weight = 1.2893389079376991R
        '
        'XrTableCell10
        '
        Me.XrTableCell10.BorderColor = System.Drawing.Color.White
        Me.XrTableCell10.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell10.Dpi = 254.0!
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.StylePriority.UseBorderColor = False
        Me.XrTableCell10.StylePriority.UseBorders = False
        Me.XrTableCell10.Text = "Kredit"
        Me.XrTableCell10.Weight = 1.3997060546673015R
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell5, Me.XrTableCell11, Me.XrTableCell13, Me.XrTableCell6, Me.XrTableCell14, Me.XrTableCell16, Me.XrTableCell9, Me.XrTableCell17, Me.XrTableCell18, Me.XrTableCell15})
        Me.XrTableRow3.Dpi = 254.0!
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Dpi = 254.0!
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254.0!)
        Me.XrTableCell1.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge
        Me.XrTableCell1.ProcessDuplicatesTarget = DevExpress.XtraReports.UI.ProcessDuplicatesTarget.Tag
        Me.XrTableCell1.StylePriority.UsePadding = False
        Me.XrTableCell1.Tag = "1"
        Me.XrTableCell1.Text = "No."
        Me.XrTableCell1.Weight = 0.44255612884790385R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Dpi = 254.0!
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge
        Me.XrTableCell2.Tag = "Kode"
        Me.XrTableCell2.Text = "Kode"
        Me.XrTableCell2.Weight = 0.69859050873489537R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.Dpi = 254.0!
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Text = "Nama Nasabah"
        Me.XrTableCell5.Weight = 2.7922248940102161R
        '
        'XrTableCell11
        '
        Me.XrTableCell11.BorderColor = System.Drawing.Color.White
        Me.XrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrTableCell11.Dpi = 254.0!
        Me.XrTableCell11.Name = "XrTableCell11"
        Me.XrTableCell11.StylePriority.UseBorderColor = False
        Me.XrTableCell11.StylePriority.UseBorders = False
        Me.XrTableCell11.Text = "Jumlah"
        Me.XrTableCell11.Weight = 0.56347458128704786R
        '
        'XrTableCell13
        '
        Me.XrTableCell13.BorderColor = System.Drawing.Color.White
        Me.XrTableCell13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell13.Dpi = 254.0!
        Me.XrTableCell13.Name = "XrTableCell13"
        Me.XrTableCell13.StylePriority.UseBorderColor = False
        Me.XrTableCell13.StylePriority.UseBorders = False
        Me.XrTableCell13.Text = "Aktif"
        Me.XrTableCell13.Weight = 0.41467194239679817R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.BorderColor = System.Drawing.Color.White
        Me.XrTableCell6.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell6.Dpi = 254.0!
        Me.XrTableCell6.Multiline = True
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.StylePriority.UseBorderColor = False
        Me.XrTableCell6.StylePriority.UseBorders = False
        Me.XrTableCell6.Text = "Non Aktif"
        Me.XrTableCell6.Weight = 0.389853966676242R
        '
        'XrTableCell14
        '
        Me.XrTableCell14.BorderColor = System.Drawing.Color.White
        Me.XrTableCell14.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell14.Dpi = 254.0!
        Me.XrTableCell14.Name = "XrTableCell14"
        Me.XrTableCell14.StylePriority.UseBorderColor = False
        Me.XrTableCell14.StylePriority.UseBorders = False
        Me.XrTableCell14.Text = "Jumlah"
        Me.XrTableCell14.Weight = 0.54825144873697751R
        '
        'XrTableCell16
        '
        Me.XrTableCell16.BorderColor = System.Drawing.Color.White
        Me.XrTableCell16.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell16.Dpi = 254.0!
        Me.XrTableCell16.Name = "XrTableCell16"
        Me.XrTableCell16.StylePriority.UseBorderColor = False
        Me.XrTableCell16.StylePriority.UseBorders = False
        Me.XrTableCell16.Text = "Aktif"
        Me.XrTableCell16.Weight = 0.35917559762217727R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.BorderColor = System.Drawing.Color.White
        Me.XrTableCell9.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell9.Dpi = 254.0!
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.StylePriority.UseBorderColor = False
        Me.XrTableCell9.StylePriority.UseBorders = False
        Me.XrTableCell9.Text = "Non Aktif"
        Me.XrTableCell9.Weight = 0.38191063796763175R
        '
        'XrTableCell17
        '
        Me.XrTableCell17.BorderColor = System.Drawing.Color.White
        Me.XrTableCell17.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell17.Dpi = 254.0!
        Me.XrTableCell17.Name = "XrTableCell17"
        Me.XrTableCell17.StylePriority.UseBorderColor = False
        Me.XrTableCell17.StylePriority.UseBorders = False
        Me.XrTableCell17.Text = "Jumlah"
        Me.XrTableCell17.Weight = 0.52829014807841868R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.BorderColor = System.Drawing.Color.White
        Me.XrTableCell18.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell18.Dpi = 254.0!
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.StylePriority.UseBorderColor = False
        Me.XrTableCell18.StylePriority.UseBorders = False
        Me.XrTableCell18.Text = "Aktif"
        Me.XrTableCell18.Weight = 0.485279184458394R
        '
        'XrTableCell15
        '
        Me.XrTableCell15.BorderColor = System.Drawing.Color.White
        Me.XrTableCell15.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell15.Dpi = 254.0!
        Me.XrTableCell15.Name = "XrTableCell15"
        Me.XrTableCell15.StylePriority.UseBorderColor = False
        Me.XrTableCell15.StylePriority.UseBorders = False
        Me.XrTableCell15.Text = "Non Aktif"
        Me.XrTableCell15.Weight = 0.38613794574140103R
        '
        'LavenderStyle
        '
        Me.LavenderStyle.BackColor = System.Drawing.Color.RosyBrown
        Me.LavenderStyle.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.LavenderStyle.ForeColor = System.Drawing.Color.Black
        Me.LavenderStyle.Name = "LavenderStyle"
        Me.LavenderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 254.0!)
        '
        'LightBlue
        '
        Me.LightBlue.BackColor = System.Drawing.Color.FromArgb(CType(CType(119, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.LightBlue.Name = "LightBlue"
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'BottomMargin
        '
        Me.BottomMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 5.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'nKreditJumlah
        '
        Me.nKreditJumlah.Dpi = 254.0!
        Me.nKreditJumlah.Name = "nKreditJumlah"
        Me.nKreditJumlah.StylePriority.UseTextAlignment = False
        Me.nKreditJumlah.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nKreditJumlah.Weight = 0.42219775563834605R
        '
        'nKreditAktif
        '
        Me.nKreditAktif.Dpi = 254.0!
        Me.nKreditAktif.Name = "nKreditAktif"
        Me.nKreditAktif.StylePriority.UseTextAlignment = False
        Me.nKreditAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nKreditAktif.Weight = 0.38782643072206341R
        '
        'nKreditNonAktif
        '
        Me.nKreditNonAktif.Dpi = 254.0!
        Me.nKreditNonAktif.Name = "nKreditNonAktif"
        Me.nKreditNonAktif.StylePriority.UseTextAlignment = False
        Me.nKreditNonAktif.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.nKreditNonAktif.Weight = 0.30859286192829977R
        '
        'RptRekapitulasiRegisterNasabah_R
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.GroupHeader1, Me.PageHeader, Me.PageFooter})
        Me.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.75!)
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(5, 0, 3, 5)
        Me.PageHeight = 2159
        Me.PageWidth = 2794
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.SnapGridSize = 31.75!
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.LightBlue, Me.TableHeaderStyle, Me.TableStyle, Me.White, Me.LavenderStyle})
        Me.Version = "14.1"
        CType(Me.xrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents TableStyle As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents cPeriode As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents cNamaLaporan As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents xrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents cNo As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents cKode As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents cNama As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nTabunganJumlah As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nTabunganAktif As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nTabunganNonAktif As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nDepositoJumlah As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nDepositoAktif As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nDepositoNonAktif As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents White As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents TableHeaderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents LavenderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents LightBlue As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents cTime As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nKreditJumlah As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nKreditAktif As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents nKreditNonAktif As DevExpress.XtraReports.UI.XRTableCell
End Class
