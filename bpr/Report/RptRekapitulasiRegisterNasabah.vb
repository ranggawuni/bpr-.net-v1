﻿Imports System.Collections
Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid
Imports bpr.FrmRpt
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Repository
Imports bpr.GridLookUpEditCBMultipleSelection
Imports System.Text

Public Class RptRekapitulasiRegisterNasabah
    Private gridCheckMarksSA As GridCheckMarksSelection

    ReadOnly objData As New data()
    Dim cDataTable As New DataSet
    Dim dbTabungan As New DataTable
    Dim dbDeposito As New DataTable
    Dim dbKredit As New DataTable
    Dim vaArray As New DataTable
    Dim cFilter As String
    Dim dvMain As DataView

    Private Sub GetData()
        Dim dbData As New DataTable
        Try
            Dim n As Integer, i As Integer
            Dim nTabAktif, nTabNonAKtif As Integer
            Dim nDepAktif, nDepNonAktif As Integer
            Dim nPembAktif, nPembNonAktif As Integer
            Dim baris As DataRow

            dTglFilterKantor = dTgl1.DateTime
            cQueryFilter = cFilterKantor.Text
            cFilter = GetQueryFilter(cFilterKantor.Text, cCabang.Text, dTgl1.DateTime, "left(kode,2)").ToString
            Dim cWhere As String
            If cFilter <> "" Then
                cWhere = String.Format("And TglRegister <='{0}' ", formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd)) & cFilter
            Else
                cWhere = String.Format("And TglRegister <='{0}' ", formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd))
            End If
            Dim cSQL As String = String.Format("select kode,nama,tglregister from registernasabah where tglregister >= '{0}' {1}", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd), cWhere)
            dbData = objData.SQL(GetDSN, cSQL)
            If dbData.Rows.Count > 0 Then
                n = 0
                nTabAktif = 0
                nTabNonAKtif = 0
                nDepAktif = 0
                nDepNonAktif = 0
                nPembAktif = 0
                nPembNonAktif = 0

                vaArray.Reset()
                AddColumn(vaArray, "Nomor", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "Kode", System.Type.GetType("System.String"))
                AddColumn(vaArray, "Nama", System.Type.GetType("System.String"))
                AddColumn(vaArray, "JumlahTabungan", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "TabunganAktif", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "TabunganNonAktif", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "JumlahDeposito", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "DepositoAktif", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "DepositoNonAktif", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "JumlahKredit", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "KreditAktif", System.Type.GetType("System.Int32"))
                AddColumn(vaArray, "KreditNonAktif", System.Type.GetType("System.Int32"))

                For n = 0 To dbData.Rows.Count - 1
                    With dbData.Rows(n)
                        dbTabungan = objData.Browse(GetDSN, "Tabungan", "Kode,Close", "Kode", , .Item("Kode").ToString)
                        For i = 0 To dbTabungan.Rows.Count - 1
                            If dbTabungan.Rows(i).Item("Close").ToString = "" Then
                                nTabAktif = nTabAktif + 1
                            ElseIf dbTabungan.Rows(i).Item("Close").ToString = "1" Then
                                nTabNonAKtif = nTabNonAKtif + 1
                            End If
                        Next

                        dbDeposito = objData.Browse(GetDSN, "Deposito", "Kode,Status", "Kode", , .Item("Kode").ToString)
                        For i = 0 To dbDeposito.Rows.Count - 1
                            If dbDeposito.Rows(i).Item("Status").ToString = "" Then
                                nDepAktif = nDepAktif + 1
                            ElseIf dbDeposito.Rows(i).Item("Status").ToString = "1" Then
                                nDepNonAktif = nDepNonAktif + 1
                            End If
                        Next

                        dbKredit = objData.SQL(GetDSN, String.Format("Select Kode,StatusPencairan From Debitur Where Kode='{0}'", .Item("Kode")))
                        For i = 0 To dbKredit.Rows.Count - 1
                            If dbKredit.Rows(i).Item("statuspencairan").ToString = "" Then
                                nPembAktif = nPembAktif + 1
                            ElseIf dbKredit.Rows(i).Item("statuspencairan").ToString = "1" Then
                                nPembNonAktif = nPembNonAktif + 1
                            End If
                        Next

                        baris = vaArray.NewRow()
                        baris(0) = n + 1
                        baris(1) = dbData.Rows(n).Item("Kode")
                        baris(2) = dbData.Rows(n).Item("Nama")
                        baris(3) = dbTabungan.Rows.Count - 1
                        baris(4) = nTabAktif
                        baris(5) = nTabNonAKtif
                        baris(6) = dbDeposito.Rows.Count - 1
                        baris(7) = nDepAktif
                        baris(8) = nDepNonAktif
                        baris(9) = dbKredit.Rows.Count - 1
                        baris(10) = nPembAktif
                        baris(11) = nPembNonAktif
                        vaArray.Rows.Add(baris)

                        nTabAktif = 0
                        nTabNonAKtif = 0
                        nDepAktif = 0
                        nDepNonAktif = 0
                        nPembAktif = 0
                        nPembNonAktif = 0
                    End With
                Next
                'GetRpt()
                Using xRpt As New RptRekapitulasiRegisterNasabah_R
                    With xRpt
                        .cDataSource = vaArray
                        .cNo.DataBindings.Add("Text", Nothing, "Nomor")
                        .cKode.DataBindings.Add("Text", Nothing, "Kode")
                        .cNama.DataBindings.Add("Text", Nothing, "Nama")
                        .nTabunganJumlah.DataBindings.Add("Text", Nothing, "JumlahTabungan")
                        .nTabunganAktif.DataBindings.Add("Text", Nothing, "TabunganAktif")
                        .nTabunganNonAktif.DataBindings.Add("Text", Nothing, "TabunganNonAktif")
                        .nDepositoJumlah.DataBindings.Add("Text", Nothing, "JumlahDeposito")
                        .nDepositoAktif.DataBindings.Add("Text", Nothing, "DepositoAktif")
                        .nDepositoNonAktif.DataBindings.Add("Text", Nothing, "DepositoNonAktif")
                        .nKreditJumlah.DataBindings.Add("Text", Nothing, "JumlahKredit")
                        .nKreditAktif.DataBindings.Add("Text", Nothing, "KreditAktif")
                        .nKreditNonAktif.DataBindings.Add("Text", Nothing, "KreditNonAktif")
                        .lLandScape = True
                        .Print()
                    End With
                End Using
            Else
                MsgBox("Data tidak ada.", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        dbData.Dispose()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub InitValue()
        dTgl.Text = formatValue(Now.Date, formatType.dd_MM_yyyy)
        dTgl1.Text = formatValue(Now.Date, formatType.dd_MM_yyyy)
        cCabang.EditValue = aCfg(eCfg.msKodeCabang).ToString
        cFilterKantor.EditValue = ""
    End Sub

    Private Sub RptRekapitulasiRegisterNasabah_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("cabang", cCabang, "jeniskantor<>'K'")
        If cCabang.Text = "" Then
            cCabang.EditValue = "01"
        End If
        isiFilterCabang()
    End Sub

    Private Sub RptRekapitulasiRegisterNasabah_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me)
        InitValue()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dTgl1.TabIndex, n)
        SetTabIndex(cCabang.TabIndex, n)
        SetTabIndex(cFilterKantor.TabIndex, n)

        SetTabIndex(cmdPreview.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        dTgl1.EnterMoveNextControl = True
        cCabang.EnterMoveNextControl = True
        cFilterKantor.EnterMoveNextControl = True
    End Sub

    Private Sub cmdPreview_Click(sender As Object, e As EventArgs) Handles cmdPreview.Click
        GetData()
    End Sub

    Private Sub GetRpt()
        Cursor = Cursors.WaitCursor
        Try
            Using xRpt As New FrmRpt()
                With xRpt
                    '.DataSource = vaArray

                    .AddReportTitle("Laporan Rekapitulasi Nasabah")
                    .AddReportTitle(String.Format("Antara Tanggal : {0} s.d {1}", Format(dTgl.DateTime, "dd-MM-yyyy"), Format(dTgl1.DateTime, "dd-MM-yyyy")))
                    .CreateXRTable()
                    '.CreateReportGroupHeader()
                    '.AddReportGroupHeader("tgl")
                    .AddReportHeader("No. Register", 7, "cNoRegister", , , , FontStyle.Bold)
                    .AddReportHeader("Nama Nasabah", 21, "cNama", , , , FontStyle.Bold)
                    .AddReportHeader("Tabungan", 9, "nJumlahTabungan", , , , FontStyle.Bold)
                    .AddReportHeader("Tabungan Aktif", 9, "nTabunganAktif", , , , FontStyle.Bold)
                    .AddReportHeader("Tabungan Non Aktif", 8, "nTabunganNonAktif", , , , FontStyle.Bold)
                    .AddReportHeader("Deposito", 9, "nJumlahDeposito", , , , FontStyle.Bold)
                    .AddReportHeader("Deposito Aktif", 9, "nDepositoAktif", , , , FontStyle.Bold)
                    .AddReportHeader("Deposito Non Aktif", 8, "nDepositoNonAktif", , , , FontStyle.Bold)
                    .AddReportHeader("Kredit", 9, "nJumlahKredit", , , , FontStyle.Bold)
                    .AddReportHeader("Kredit Aktif", 9, "nKreditAktif", , , , FontStyle.Bold)
                    .AddReportHeader("Kredit Non Aktif", 8, "nKreditNonAktif", , , , FontStyle.Bold)
                    .CreateReportHeader()

                    .AddDetail("noRegister", 7, "cNoRegister", FrmRpt.ReportFieldFormat.Rpt_dd_MM_yyyy)
                    .AddDetail("Nama", 21, "cNama", , DevExpress.XtraPrinting.TextAlignment.MiddleLeft)
                    .AddDetail("Tabungan", 9, "nJumlahTabungan", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("TabunganAktif", 9, "nTabunganAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("TabunganNonAktif", 8, "nTabunganNonAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("Deposito", 9, "nJumlahDeposito", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("DepositoAktif", 9, "nDepositoAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("DepositoNonAktif", 8, "nDepositoNonAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("Kredit", 9, "nJumlahKredit", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("KreditAktif", 9, "nKreditAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddDetail("KreditNonAktif", 8, "nKreditNonAktif", FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .CreateDetail()
                    .AddReportFooter("Total", 28, "nTotal")
                    .AddReportFooter("Tabungan", 9, "nJumlahTabungan", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("TabunganAktif", 9, "nTabunganAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("TabunganNonAktif", 8, "nTabunganNonAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("Deposito", 9, "nJumlahDeposito", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("DepositoAktif", 9, "nDepositoAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("DepositoNonAktif", 8, "nDepositoNonAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("Kredit", 9, "nJumlahKredit", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("KreditAktif", 9, "nKreditAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .AddReportFooter("KreditNonAktif", 8, "nKreditNonAktif", True, DevExpress.XtraReports.UI.SummaryFunc.Sum, FrmRpt.ReportFieldFormat.Rpt_Number, DevExpress.XtraPrinting.TextAlignment.MiddleRight)
                    .CreateReportFooter()
                    .FinishCreateXRTable()

                    'Dim printTool As New ReportPrintTool(xRpt)
                    'printTool.ShowPreviewDialog()
                    .Print(vaArray, , True)
                End With
            End Using

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub isiFilterCabang()
        Dim db As New DataTable
        db = objData.Browse(GetDSN, "cabang", "kode,keterangan", "jeniskantor", , "K", , "Kode")
        If db.Rows.Count > 0 Then
            With cFilterKantor.Properties
                .View.OptionsBehavior.AutoPopulateColumns = False
                .DataSource = db
                .DisplayMember = "kode"
                .ValueMember = "kode"
                .View.OptionsSelection.MultiSelect = True
            End With
            AddHandler cFilterKantor.CustomDisplayText, AddressOf gCabangFilter_CustomDisplayText
            cFilterKantor.Properties.PopulateViewColumns()

            gridCheckMarksSA = New GridCheckMarksSelection(cFilterKantor.Properties)
            AddHandler gridCheckMarksSA.SelectionChanged, AddressOf gridCheckMarks_SelectionChanged
            gridCheckMarksSA.SelectAll(db.DefaultView)
            cFilterKantor.Properties.Tag = gridCheckMarksSA
        End If
    End Sub

    Private Sub gridCheckMarks_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs)
        If TypeOf ActiveControl Is GridLookUpEdit Then
            Dim sb As New StringBuilder()
            For Each rv As DataRowView In (TryCast(sender, GridCheckMarksSelection)).Selection
                If sb.ToString().Length > 0 Then
                    sb.Append(", ")
                End If
                sb.Append(rv("kode").ToString())
            Next rv
            TryCast(ActiveControl, GridLookUpEdit).Text = sb.ToString()
        End If
    End Sub

    Private Sub gCabangFilter_CustomDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs)
        Dim sb As New StringBuilder()
        Dim gridCheckMark As GridCheckMarksSelection = If(TypeOf sender Is GridLookUpEdit, TryCast((TryCast(sender, GridLookUpEdit)).Properties.Tag, GridCheckMarksSelection), TryCast((TryCast(sender, RepositoryItemGridLookUpEdit)).Tag, GridCheckMarksSelection))
        If gridCheckMark Is Nothing Then
            Return
        End If
        For Each rv As DataRowView In gridCheckMark.Selection
            If sb.ToString().Length > 0 Then
                sb.Append(", ")
            End If
            sb.Append(rv("kode").ToString())
        Next rv
        e.DisplayText = sb.ToString()
    End Sub
End Class