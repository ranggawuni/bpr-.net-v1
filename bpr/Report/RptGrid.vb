﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid

Public Class RptGrid
    ReadOnly objData As New data()


    Private Sub GetData()
        Try
            Dim cField As String = "concat(t.golongantabungan,' - ',g.keterangan) as GolonganTabungan,"
            cField = cField & "m.Faktur,m.rekening,t.rekeninglama,r.nama,m.kodetransaksi,m.keterangan,"
            cField = cField & "m.debet,m.kredit,m.username,m.tgl,m.cabangentry"
            Dim vaJoin() As Object = {"left join tabungan t on t.rekening= m.rekening", "left join  registernasabah r on r.kode=t.kode", _
                                      "left join golongantabungan g on g.kode=t.GolonganTabungan"}
            Dim cWhere As String = String.Format(" m.tgl >= '{0}' ", formatValue(CDate(dTgl.Text), formatType.yyyy_MM_dd))
            cWhere = String.Format("{0}and m.Tgl <='{1}' ", cWhere, formatValue(CDate(dTgl1.Text), formatType.yyyy_MM_dd))
            Dim cDataTable = objData.Browse(GetDSN, "mutasitabungan m", cField, , , , cWhere, "golongantabungan,m.tgl,m.rekening", vaJoin)
            Dim cTableGrid As DataTable = New DataTable
            Dim nRow As Double = cDataTable.Rows.Count
            Dim n As Integer = 0
            cTableGrid.BeginInit()
            AddColumn(cTableGrid, "GolonganTabungan", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Faktur", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Rekening", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "RekeningLama", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Nama", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "KodeTransaksi", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Keterangan", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Debet", System.Type.GetType("System.Single"), True)
            AddColumn(cTableGrid, "Kredit", System.Type.GetType("System.Single"), True)
            AddColumn(cTableGrid, "UserName", System.Type.GetType("System.String"), True)
            AddColumn(cTableGrid, "Tanggal", System.Type.GetType("System.DateTime"), True)
            AddColumn(cTableGrid, "CabangEntry", System.Type.GetType("System.String"), True)
            Do Until n > nRow - 1
                With cDataTable.Rows(n)
                    cTableGrid.Rows.Add(New Object() {.Item("GolonganTabungan").ToString, _
                                        .Item("Faktur").ToString, .Item("Rekening").ToString, .Item("RekeningLama").ToString, _
                                        .Item("Nama").ToString, .Item("KodeTransaksi").ToString, .Item("Keterangan").ToString, _
                                        .Item("Debet"), .Item("Kredit"), .Item("UserName").ToString, .Item("Tgl").ToString, _
                                        .Item("CabangEntry").ToString})
                End With
                n = n + 1
            Loop
            GridControl1.DataSource = cTableGrid
            GridControl1.DefaultView.PopulateColumns()
            With (GridView1)
                Dim arrayFooter(,) As String = New String(,) {{"Debet", "{0:n2}"}, {"Kredit", "{0:n2}"}}
                Dim arrayGroup() As String = New String() {"GolonganTabungan"}
                Dim arrayGroupSummary(,) As String = New String(,) {{"Debet", "{0:n2}"}, {"Kredit", "{0:n2}"}}
                InitGrid(GridView1, True, arrayFooter, True, arrayGroup, arrayGroupSummary)

                GridColumnFormat(GridView1, "GolonganTabungan")
                GridColumnFormat(GridView1, "Faktur", , , , DevExpress.Utils.HorzAlignment.Center, True)
                GridColumnFormat(GridView1, "Rekening", , , , DevExpress.Utils.HorzAlignment.Center)
                GridColumnFormat(GridView1, "RekeningLama", , , , DevExpress.Utils.HorzAlignment.Center)
                GridColumnFormat(GridView1, "Nama")
                GridColumnFormat(GridView1, "KodeTransaksi", , , 50, DevExpress.Utils.HorzAlignment.Center)
                GridColumnFormat(GridView1, "Keterangan", , , -2)
                GridColumnFormat(GridView1, "Debet", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, 120)
                GridColumnFormat(GridView1, "Kredit", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, 120)
                GridColumnFormat(GridView1, "UserName", , , , DevExpress.Utils.HorzAlignment.Center)
                GridColumnFormat(GridView1, "Tanggal", , , , DevExpress.Utils.HorzAlignment.Center)
                GridColumnFormat(GridView1, "CabangEntry", , , 40, DevExpress.Utils.HorzAlignment.Center)
            End With

            Dim nValue As Integer = MessageBox.Show("Preview Data?", "Grid", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            If nValue = vbYes Then
                ShowGridPreview(GridControl1)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        GetData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPreview.Click
        Using savdlg As New SaveFileDialog
            savdlg.Filter = "Excel Documents (*.xls)|"
            savdlg.DefaultExt = "xls"
            savdlg.AddExtension = True
            If savdlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
                MsgBox(savdlg.FileName)
                GridView1.ExportToXls(savdlg.FileName)
                MessageBox.Show("Export Excel Success", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Using
    End Sub

    Private Sub InitValue()
        dTgl.Text = formatValue(Now.Date, formatType.dd_MM_yyyy)
        dTgl1.Text = formatValue(Now.Date, formatType.dd_MM_yyyy)
    End Sub

    Private Sub RptGrid_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        WindowState = FormWindowState.Maximized
        resizeForm()
    End Sub

    Private Sub RptGrid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InitValue()
        WindowState = FormWindowState.Maximized
        resizeForm()
    End Sub

    Private Sub resizeForm()
        PanelControl1.Width = Width - 25
        GridControl1.Width = PanelControl1.Width
        GridControl1.Height = Height - PanelControl1.Height - 50
    End Sub

    Private Sub RptGrid_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        resizeForm()
    End Sub

End Class