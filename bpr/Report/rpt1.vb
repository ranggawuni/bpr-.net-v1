﻿Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class rpt1
    ' keterangan konversi jarak
    ' 1 cm = 50
    ' 1 mm = 5
    Shared dtLabel As New DataTable
    Shared dtPreview As New DataTable

    Public Shared Sub AddLabel(ByVal cName As String, ByVal cText As String, Optional ByVal nWidth As Integer = 100, Optional ByVal nHeight As Integer = 50, _
                 Optional ByVal nPointX As Integer = 50, Optional ByVal nPointY As Integer = 20, Optional ByVal nAlignment As TextAlignment = Nothing, _
                 Optional ByVal cFont As String = "Tahoma", Optional ByVal nFontSize As Integer = 10, Optional ByVal lFontBold As Boolean = False, _
                 Optional ByVal lFontItalic As Boolean = False, Optional ByVal lFontUnderline As Boolean = False, Optional ByVal lFontStrikeOut As Boolean = False, _
                 Optional ByVal cForeColor As Color = Nothing, Optional ByVal cBackColor As Color = Nothing, _
                 Optional ByVal lVisible As Boolean = True, Optional ByVal lWordWrap As Boolean = False, _
                 Optional ByVal lMultiLine As Boolean = False, Optional ByVal lCanGrow As Boolean = False, Optional ByVal lCanShrink As Boolean = False, _
                 Optional ByVal lUseBorder As Boolean = False, Optional ByVal lBorderLeft As Boolean = False, Optional ByVal lBorderRight As Boolean = False, _
                 Optional ByVal lBorderTop As Boolean = False, Optional ByVal lBorderBottom As Boolean = False, _
                 Optional ByVal cBorderColor As Color = Nothing, Optional ByVal nBorderWidth As Integer = 1)
        If cBorderColor = Nothing Then
            cBorderColor = Color.FromName("black")
        End If
        If cForeColor = Nothing Then
            cForeColor = Color.Black
        End If
        If cBackColor = Nothing Then
            cBackColor = Color.Transparent
        End If
        If nAlignment = Nothing Then
            nAlignment = TextAlignment.MiddleCenter
        End If
        Dim row As DataRow = dtLabel.NewRow()
        row("cName") = cName
        row("cText") = cText
        row("nWidth") = nWidth
        row("nHeight") = nHeight
        row("nPointX") = nPointX
        row("nPointY") = nPointY
        row("nAlignment") = nAlignment
        row("cFont") = cFont
        row("nFontSize") = nFontSize
        row("lFontBold") = lFontBold
        row("lFontItalic") = lFontItalic
        row("lFontUnderline") = lFontUnderline
        row("lFontStrikeOut") = lFontStrikeOut
        row("cForeColor") = cForeColor.Name
        row("cBackColor") = cBackColor.Name
        row("lVisible") = lVisible
        row("lWordWrap") = lWordWrap
        row("lMultiLine") = lMultiLine
        row("lCanGrow") = lCanGrow
        row("lCanShrink") = lCanShrink
        row("lUseBorder") = lUseBorder
        row("lBorderLeft") = lBorderLeft
        row("lBorderRight") = lBorderRight
        row("lBorderTop") = lBorderTop
        row("lBorderBottom") = lBorderBottom
        row("cBorderColor") = cBorderColor.Name
        row("nBorderWidth") = nBorderWidth
        dtLabel.Rows.Add(row)
    End Sub

    Shared Sub InitTable()
        Dim dtLabel1 As DataTable = New DataTable
        AddColumn(dtLabel1, "cName", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "cText", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "nWidth", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "nHeight", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "nPointX", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "nPointY", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "nAlignment", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "cFont", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "nFontSize", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "lFontBold", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lFontItalic", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lFontUnderline", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lFontStrikeOut", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "cForeColor", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "cBackColor", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "lVisible", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lWordWrap", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lMultiLine", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lCanGrow", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lCanShrink", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lUseBorder", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lBorderLeft", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lBorderRight", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lBorderTop", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "lBorderBottom", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtLabel1, "cBorderColor", System.Type.GetType("System.String"), True)
        AddColumn(dtLabel1, "nBorderWidth", System.Type.GetType("System.Int32"), True)
        dtLabel = dtLabel1.Copy()

        Dim dtPreview1 As New DataTable
        AddColumn(dtPreview1, "lLandscape", System.Type.GetType("System.Boolean"), True)
        AddColumn(dtPreview1, "nPaperSize", System.Type.GetType("System.String"), True)
        AddColumn(dtPreview1, "nPageHeight", System.Type.GetType("System.Int32"), True)
        AddColumn(dtPreview1, "nPageWidth", System.Type.GetType("System.Int32"), True)
        AddColumn(dtPreview1, "nLeftMargin", System.Type.GetType("System.Int32"), True)
        AddColumn(dtPreview1, "nRightMargin", System.Type.GetType("System.Int32"), True)
        AddColumn(dtPreview1, "nTopMargin", System.Type.GetType("System.Int32"), True)
        AddColumn(dtPreview1, "nBottomMargin", System.Type.GetType("System.Int32"), True)
        dtPreview = dtPreview1.Copy
    End Sub

    Shared Sub InitLabelPreview(Optional ByVal lLandscape As Boolean = False, Optional ByVal nPaperType As Printing.PaperKind = Printing.PaperKind.Letter, _
                                Optional ByVal nPaperWidth As Integer = 0, Optional ByVal nPaperHeight As Integer = 0, _
                                Optional ByVal nLeftMargin As Integer = 0, Optional ByVal nRightMargin As Integer = 0, _
                                Optional ByVal nTopMargin As Integer = 0, Optional ByVal nBottomMargin As Integer = 0)
        InitTable()
        Dim row As DataRow = dtPreview.NewRow()
        row("lLandscape") = lLandscape
        row("nPaperSize") = CType(nPaperType, Integer)
        If nPaperType = Printing.PaperKind.Custom Then
            row("nPageWidth") = nPaperWidth
            row("nPageHeight") = nPaperHeight
        End If
        row("nLeftMargin") = nLeftMargin
        row("nRightMargin") = nRightMargin
        row("nTopMargin") = nTopMargin
        row("nBottomMargin") = nBottomMargin
        dtPreview.Rows.Add(row)
    End Sub

    Public Sub New()
        Dim r As XtraReport = Me
        Dim cLabel = New XRLabel()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' ukuran kertas :
            ' ukuran kertas dalam milimeter x 10
            With dtPreview.Rows(0)
                r.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
                r.PaperKind = CType(CInt(.Item("nPaperSize")), Printing.PaperKind)
                r.PageHeight = CInt(.Item("nPageHeight")) * 10 ' 1000
                r.PageWidth = CInt(.Item("nPageWidth")) * 10 ' 2100
                Dim nLeftMargin As Integer = CType(.Item("nLeftMargin"), Integer)
                Dim nRightMargin As Integer = CInt(.Item("nRightMargin"))
                Dim nTopMargin As Integer = CInt(.Item("nTopMargin"))
                Dim nBottomMargin As Integer = CInt(.Item("nBottomMargin"))
                r.Margins = New System.Drawing.Printing.Margins(nLeftMargin, nRightMargin, nTopMargin, nBottomMargin)
                r.Landscape = CType(.Item("lLandscape"), Boolean)
            End With
            For n As Integer = 0 To dtLabel.Rows.Count - 1
                With dtLabel.Rows(n)
                    cLabel = New XRLabel()
                    cLabel.Name = .Item("cName").ToString
                    cLabel.Text = .Item("cText").ToString
                    cLabel.SizeF = New SizeF(CSng(.Item("nWidth")), CSng(.Item("nHeight")))
                    cLabel.Location = New Point(CInt(.Item("nPointX")), CInt(.Item("nPointY")))
                    cLabel.TextAlignment = CType(.Item("nAlignment"), TextAlignment)
                    cLabel.ForeColor = Color.FromName(.Item("cForeColor").ToString)
                    cLabel.BackColor = Color.FromName(.Item("cBackColor").ToString)
                    cLabel.Borders = BorderSide.None
                    cLabel.BorderColor = Color.FromName(.Item("cBorderColor").ToString)
                    cLabel.Multiline = CType(.Item("lMultiLine"), Boolean)
                    cLabel.CanGrow = CType(.Item("lCanGrow"), Boolean)
                    cLabel.CanShrink = CType(.Item("lCanShrink"), Boolean)
                    cLabel.Visible = CType(.Item("lVisible"), Boolean)
                    cLabel.WordWrap = CType(.Item("lWordWrap"), Boolean)

                    If CType(.Item("lUseBorder"), Boolean) Then
                        Dim cBorder As BorderSide
                        If CType(.Item("lBorderLeft"), Boolean) Then
                            cBorder = cLabel.Borders Or BorderSide.Left
                        Else
                            cBorder = cLabel.Borders
                        End If
                        If CType(.Item("lBorderRight"), Boolean) Then
                            cBorder = cBorder Or BorderSide.Right
                        End If
                        If CType(.Item("lBorderTop"), Boolean) Then
                            cBorder = cBorder Or BorderSide.Top
                        End If
                        If CType(.Item("lBorderBottom"), Boolean) Then
                            cBorder = cBorder Or BorderSide.Bottom
                        End If
                        cLabel.Borders = cBorder
                        cLabel.BorderColor = Color.FromName(.Item("cBorderColor").ToString)
                        cLabel.BorderWidth = CInt(.Item("BorderWidth"))

                    End If

                    Dim fstNewFontStyle As FontStyle
                    If Not CType(.Item("lFontBold"), Boolean) Then
                        fstNewFontStyle = cLabel.Font.Style And Not FontStyle.Bold
                    Else
                        fstNewFontStyle = cLabel.Font.Style Or FontStyle.Bold
                    End If
                    cLabel.Font = New Font(.Item("cFont").ToString, CSng(.Item("nFontSize")), fstNewFontStyle)
                    If Not CType(.Item("lFontItalic"), Boolean) Then
                        fstNewFontStyle = cLabel.Font.Style And Not FontStyle.Italic
                    Else
                        fstNewFontStyle = cLabel.Font.Style Or FontStyle.Italic
                    End If
                    cLabel.Font = New Font(.Item("cFont").ToString, CSng(.Item("nFontSize")), fstNewFontStyle)
                    If Not CType(.Item("lFontUnderline"), Boolean) Then
                        fstNewFontStyle = cLabel.Font.Style And Not FontStyle.Underline
                    Else
                        fstNewFontStyle = cLabel.Font.Style Or FontStyle.Underline
                    End If
                    cLabel.Font = New Font(.Item("cFont").ToString, CSng(.Item("nFontSize")), fstNewFontStyle)
                    If Not CType(.Item("lFontStrikeOut"), Boolean) Then
                        fstNewFontStyle = cLabel.Font.Style And Not FontStyle.Strikeout
                    Else
                        fstNewFontStyle = cLabel.Font.Style Or FontStyle.Strikeout
                    End If
                    cLabel.Font = New Font(.Item("cFont").ToString, CSng(.Item("nFontSize")), fstNewFontStyle)
                    Detail.Controls.Add(cLabel)
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class