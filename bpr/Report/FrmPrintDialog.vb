﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class FrmPrintDialog
    Public nProses As Integer = 0
    Public gvTemp As DevExpress.XtraGrid.Views.Grid.GridView
    Public gTemp As DevExpress.XtraGrid.GridControl
    Dim lCustomPaper As Boolean
    Dim nPaperSize As System.Drawing.Printing.PaperKind

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cbPaper.Properties
        With properties
            .Items.AddRange(New String() {"Letter (8.5 x 11 in)", "Legal (8.5 x 13 in)", "Custom", "Half Page", "Quarter Page"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbPaper.SelectedIndex = 0
    End Sub

    Private Sub FrmPrintDialog_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        cmdProses.Focus()
    End Sub

    Private Sub FrmPrintDialog_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , , , cmdKeluar, , , cmdProses)
        InitForm(Me)
        optPrint.SelectedIndex = 0
        IsiCombo()
        GetPrintDialog()

        SetTabIndex(nTopMargin.TabIndex, n)
        SetTabIndex(nLeftMargin.TabIndex, n)
        SetTabIndex(nBottomMargin.TabIndex, n)
        SetTabIndex(nRightMargin.TabIndex, n)
        SetTabIndex(optPrint.TabIndex, n)
        SetTabIndex(cbPaper.TabIndex, n)
        SetTabIndex(nPaperWidth.TabIndex, n)
        SetTabIndex(nPaperHeight.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        nTopMargin.EnterMoveNextControl = True
        nLeftMargin.EnterMoveNextControl = True
        nBottomMargin.EnterMoveNextControl = True
        nRightMargin.EnterMoveNextControl = True
        optPrint.EnterMoveNextControl = True
        cbPaper.EnterMoveNextControl = True
        nPaperWidth.EnterMoveNextControl = True
        nPaperHeight.EnterMoveNextControl = True

        If cPrintDialog = "1" Then
            cPrintDialog = ""
            Close()
        End If
    End Sub

    Private Sub SavePrintSetting()
        Dim va() As String = {nTopMargin.Text, nLeftMargin.Text, nBottomMargin.Text, nRightMargin.Text, nPaperWidth.Text, nPaperHeight.Text, cbPaper.Text}
        SaveRegistry(eRegistry.reg_Printer, Join(va, "~"))
    End Sub

    Private Sub GetPrintDialog()
        Dim cPrinter As String = GetRegistry(eRegistry.reg_Printer)
        Dim vaArray() As String = Split(cPrinter, "~")

        nTopMargin.Text = GA(vaArray, 0, CStr(10)).ToString
        nLeftMargin.Text = GA(vaArray, 1, CStr(10)).ToString
        nBottomMargin.Text = GA(vaArray, 2, CStr(10)).ToString
        nRightMargin.Text = GA(vaArray, 3, CStr(10)).ToString

        nPaperWidth.Text = GA(vaArray, 4, CStr(8.5)).ToString
        nPaperHeight.Text = GA(vaArray, 5, CStr(11)).ToString
        cbPaper.Text = GA(vaArray, 6, "Letter (8.5 x 11 in)").ToString
    End Sub

    Private Shared Function GA(ByVal va() As String, ByVal n As Integer, ByVal cDefault As String) As String
        If UBound(va) >= n Then
            GA = va(n)
        Else
            GA = cDefault
        End If
    End Function

    Private Sub cbPaper_Click(sender As Object, e As EventArgs) Handles cbPaper.Click
        Dim va(,) As Object = {{8.5, 11}, {8.5, 13}, {8.5, 11}, {8.5, 5.5}, {8.5, 2.75}}
        Dim nIndex As Integer = cbPaper.SelectedIndex
        nPaperWidth.Text = va(nIndex, 0).ToString
        nPaperHeight.Text = va(nIndex, 1).ToString
        Select Case nIndex
            Case 0
                nPaperSize = Printing.PaperKind.Letter
            Case 1
                nPaperSize = Printing.PaperKind.A4
            Case 2
                nPaperSize = Printing.PaperKind.Custom
            Case 3
                nPaperSize = Printing.PaperKind.Custom
            Case 4
                nPaperSize = Printing.PaperKind.Custom
            Case Else
                Exit Select
        End Select
        If nIndex = 2 Then
            nPaperWidth.Enabled = True
            nPaperHeight.Enabled = True
            lCustomPaper = True
        Else
            nPaperWidth.Enabled = False
            nPaperHeight.Enabled = False
            lCustomPaper = False
        End If
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        SavePrintSetting()
        nProses = 1
        Select Case optPrint.SelectedIndex
            Case 0
                FuncGrid.ShowGridPreview(gTemp, , lCustomPaper, CInt(nPaperWidth.Text), CInt(nPaperHeight.Text), nPaperSize,
                                         CInt(nLeftMargin.Text), CInt(nRightMargin.Text), CInt(nTopMargin.Text), CInt(nBottomMargin.Text))
            Case 1
                FuncGrid.exportReport(gvTemp, ePrintExport.ePDF)
            Case 2
                FuncGrid.exportReport(gvTemp, ePrintExport.eXls)
            Case 3
                FuncGrid.exportReport(gvTemp, ePrintExport.eXlsx)
            Case 4
                FuncGrid.exportReport(gvTemp, ePrintExport.eCsv)
            Case Else
                Exit Select
        End Select
        Close()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        nProses = 0
        Close()
    End Sub
End Class