﻿Imports DevExpress.XtraReports.UI

Public Class RptRekapitulasiRegisterNasabah_R
    Public cDataSource As DataTable = Nothing
    Public lLandScape As Boolean = False
    Public nTopMargin As Integer = 0
    Public nBottomMargin As Integer = 0
    Public nLeftMargin As Integer = 0
    Public nRightMargin As Integer = 0

    Sub Print()
        DataSource = cDataSource
        Landscape = lLandScape
        With Margins
            .Bottom = nBottomMargin
            .Top = nTopMargin
            .Left = nLeftMargin
            .Right = nRightMargin
        End With
        Using printTool As New ReportPrintTool(Me)
            printTool.ShowPreviewDialog()
        End Using
    End Sub

End Class