﻿Imports DevExpress.XtraReports.UI

Public Class FrmRpt
    Public Enum fontName
        fArial = 0
        fTimesNewRoman = 1
        fTahoma = 2
    End Enum

    Public Enum ReportFieldFormat
        Rpt_None = 0
        Rpt_Number = 1
        Rpt_Number2 = 2
        Rpt_dd_MM_yyyy = 3
        Rpt_MM_dd_yyyy = 4
        Rpt_yyyy_MM_dd = 5
        Rpt_dd_MM_yy = 6
    End Enum

    Private ReadOnly table As New XRTable()
    Private ReadOnly tableHeader As New XRTable()
    Private ReadOnly tableFooter As New XRTable()
    Dim row As New XRTableRow()
    Dim rowHeader As New XRTableRow()
    Dim rowFooter As New XRTableRow()
    Dim ghBand As New GroupHeaderBand()
    Const nCellWidthMultiplier As Integer = 10
    Private ReadOnly vaFormat() As String = {"", "{0:###,###,###,###,###,##0}", "{0:###,###,###,###,###,##0.00}", "{0:dd-MM-yyyy}", "{0:MM-dd-yyyy}", "{0:yyyy-MM-dd}", "{0:dd-MM-yy}"}

    Private Shared Function getFontName(ByVal nType As fontName) As String
        Dim cFont As String = ""
        Select Case nType
            Case 0
                cFont = "Arial"
            Case CType(1, fontName)
                cFont = "Times New Roman"
            Case CType(2, fontName)
                cFont = "Tahoma"
        End Select
        getFontName = cFont
    End Function

    Public Sub AddReportTitle(ByVal cKeterangan As String, Optional ByVal nHeight As Single = 21, Optional ByVal nWidth As Single = 100, _
                              Optional ByVal fontType As fontName = fontName.fTimesNewRoman, Optional ByVal nFontSize As Single = 12)
        ' create label
        Dim label As New XRLabel()

        ' add the label to the report's detail band
        ReportHeader.Controls.Add(label)

        ' set it's location and size
        Dim nLeft = (PageWidth / 2) - 175
        Dim nTop = (PageHeight / 2) - 500
        label.LeftF = CSng(nLeft)
        label.TopF = CSng(nTop)
        label.HeightF = nHeight
        label.WidthF = nWidth

        label.Text = cKeterangan
        label.Font = New Font(getFontName(fontType), nFontSize)
        'label.Borders = DevExpress.XtraPrinting.BorderSide.Bottom

    End Sub

    Public Sub CreateReportGroupHeader()
        ' Create a group header band and add it to the report. 
        Bands.Add(ghBand)
    End Sub

    Public Sub AddReportGroupHeader(ByVal cGroupField As String)
        ' Create a group field,  
        ' and assign it to the group header band. 
        Dim groupField As New GroupField(cGroupField)
        ghBand.GroupFields.Add(groupField)

        ' Create bound labels, and add them to the report's bands. 
        Dim labelGroup As New XRLabel()
        labelGroup.DataBindings.Add("Text", DataSource, cGroupField)
        ghBand.Controls.Add(labelGroup)

    End Sub

    Public Sub CreateXRTable()
        ' Create an empty table and set its size. 
        PageHeader.Controls.Add(tableHeader)
        Detail.Controls.Add(table)
        ReportFooter.Controls.Add(tableFooter)
        'Dim nTableWidth As Single = Me.PageWidth - Me.Margins.Left - Me.Margins.Right
        'Dim nTableHeight As Single = Me.PageHeight - Me.Margins.Top - Me.Margins.Bottom

        tableHeader.Borders = DevExpress.XtraPrinting.BorderSide.All
        table.Borders = DevExpress.XtraPrinting.BorderSide.Bottom Or DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right
        tableFooter.Borders = DevExpress.XtraPrinting.BorderSide.Bottom Or DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right

        ' Start table initialization. 
        table.BeginInit()
        table.Name = "Table1"
        table.Width = (PageWidth - (Margins.Left + Margins.Right))
        'table.CanGrow = True
        'table.CanShrink = False
        tableHeader.BeginInit()
        tableFooter.BeginInit()
        ' Enable table borders to see its boundaries. 
        table.BorderWidth = 1


        CreateDataTableReportHeader()
        CreateDataTableDetail()
        CreateDataTableReportFooter()
    End Sub

    Public Function FinishCreateXRTable() As XRTable
        ' Finish table initialization. 
        table.EndInit()
        tableHeader.EndInit()
        tableFooter.EndInit()

        Return table
        Return tableHeader
        Return tableFooter
    End Function

    Dim ReportHeaderTable As DataTable = New DataTable("ReportHeaderTable")
    Dim ReportHeaderRow As DataRow
    Private Sub CreateDataTableReportHeader()
        Dim cFieldNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cFieldName", .ColumnName = "cFieldName", .DefaultValue = ""}
        ReportHeaderTable.Columns.Add(cFieldNameColumn)

        Dim nCellWidthColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nCellWidth", .ColumnName = "nCellWidth", .DefaultValue = 0}
        ReportHeaderTable.Columns.Add(nCellWidthColumn)

        Dim cCellNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cCellName", .ColumnName = "cCellName", .DefaultValue = ""}
        ReportHeaderTable.Columns.Add(cCellNameColumn)

        Dim cCellAlignmentColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "cCellAlignment", .ColumnName = "cCellAlignment", .DefaultValue = DevExpress.XtraPrinting.TextAlignment.MiddleCenter}
        ReportHeaderTable.Columns.Add(cCellAlignmentColumn)

        Dim nFontType As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontType", .ColumnName = "nFontType", .DefaultValue = fontName.fTimesNewRoman}
        ReportHeaderTable.Columns.Add(nFontType)

        Dim nFontSize As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontSize", .ColumnName = "nFontSize", .DefaultValue = 12}
        ReportHeaderTable.Columns.Add(nFontSize)

        Dim nFontStyle As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontStyle", .ColumnName = "nFontStyle", .DefaultValue = FontStyle.Regular}
        ReportHeaderTable.Columns.Add(nFontStyle)
    End Sub

    Public Sub AddReportHeader(ByVal cFieldName As String, ByVal nCellWidth As Single, ByVal cCellName As String, _
                             Optional ByVal cCellAlignment As DevExpress.XtraPrinting.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter, _
                             Optional ByVal nFontType As fontName = fontName.fTimesNewRoman, Optional ByVal nFontSize As Single = 12, _
                             Optional ByVal nFontStyle As FontStyle = FontStyle.Regular)
        ReportHeaderRow = ReportHeaderTable.NewRow()
        ReportHeaderRow(0) = cFieldName
        ReportHeaderRow(1) = nCellWidth * nCellWidthMultiplier
        ReportHeaderRow(2) = cCellName
        ReportHeaderRow(3) = cCellAlignment
        ReportHeaderRow(4) = nFontType
        ReportHeaderRow(5) = nFontSize
        ReportHeaderRow(6) = nFontStyle
        ReportHeaderTable.Rows.Add(ReportHeaderRow)
    End Sub

    Public Sub CreateReportHeader()
        Dim nTableWidth As Integer = 0
        Dim rowReportHeader As DataRow
        Dim currentRows() As DataRow = ReportHeaderTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If currentRows.Length > 1 Then
            For Each rowReportHeader In currentRows
                Dim cell As New XRTableCell()
                rowHeader.Cells.Add(cell)
                cell.Text = CStr(rowReportHeader(0))
                cell.WidthF = CSng(rowReportHeader(1))
                cell.Name = CStr(rowReportHeader(2))
                cell.TextAlignment = CType(rowReportHeader(3), DevExpress.XtraPrinting.TextAlignment)
                Dim nFontType As Single = CSng(rowReportHeader(4))
                Dim nFontSize As Single = CSng(rowReportHeader(5))
                Dim nFontStyle As FontStyle = CType(rowReportHeader(6), FontStyle)
                cell.Font = New Font(getFontName(CType(nFontType, fontName)), nFontSize, nFontStyle)
                nTableWidth += CInt(rowReportHeader(1))
            Next
        End If
        ReportHeader.HeightF = 21
        tableHeader.Rows.Add(rowHeader)
        rowHeader.HeightF = 21
        rowHeader.WidthF = tableHeader.WidthF
        tableHeader.WidthF = nTableWidth
    End Sub

    Dim detailTable As DataTable = New DataTable("DetailTable")
    Dim detailRow As DataRow
    Private Sub CreateDataTableDetail()
        Dim cFieldNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cFieldName", .ColumnName = "cFieldName", .DefaultValue = ""}
        detailTable.Columns.Add(cFieldNameColumn)

        Dim nCellWidthColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nCellWidth", .ColumnName = "nCellWidth", .DefaultValue = 0}
        detailTable.Columns.Add(nCellWidthColumn)

        Dim cCellNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cCellName", .ColumnName = "cCellName", .DefaultValue = ""}
        detailTable.Columns.Add(cCellNameColumn)

        Dim cFormatType As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cFormatType", .ColumnName = "cFormatType", .DefaultValue = ""}
        detailTable.Columns.Add(cFormatType)

        Dim cCellAlignmentColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "cCellAlignment", .ColumnName = "cCellAlignment", .DefaultValue = DevExpress.XtraPrinting.TextAlignment.MiddleCenter}
        detailTable.Columns.Add(cCellAlignmentColumn)

        Dim nFontType As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontType", .ColumnName = "nFontType", .DefaultValue = fontName.fTimesNewRoman}
        detailTable.Columns.Add(nFontType)

        Dim nFontSize As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontSize", .ColumnName = "nFontSize", .DefaultValue = 12}
        detailTable.Columns.Add(nFontSize)

        Dim nFontStyle As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontStyle", .ColumnName = "nFontStyle", .DefaultValue = FontStyle.Regular}
        detailTable.Columns.Add(nFontStyle)
    End Sub

    Public Sub AddDetail(ByVal cFieldName As String, ByVal nCellWidth As Single, ByVal cCellName As String, _
                         Optional ByVal nFormatType As ReportFieldFormat = ReportFieldFormat.Rpt_None, _
                         Optional ByVal cCellAlignment As DevExpress.XtraPrinting.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter, _
                         Optional ByVal nFontType As fontName = fontName.fTimesNewRoman, Optional ByVal nFontSize As Single = 10, _
                         Optional ByVal nFontStyle As FontStyle = FontStyle.Regular)
        detailRow = detailTable.NewRow()
        detailRow(0) = cFieldName
        detailRow(1) = nCellWidth * nCellWidthMultiplier
        detailRow(2) = cCellName
        detailRow(3) = vaformat(nFormatType)
        detailRow(4) = cCellAlignment
        detailRow(5) = nFontType
        detailRow(6) = nFontSize
        detailRow(7) = nFontStyle
        detailTable.Rows.Add(detailRow)
    End Sub

    Public Sub CreateDetail()
        Dim nTableWidth As Integer = 0
        Dim rowDetail As DataRow
        Dim currentRows() As DataRow = detailTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If currentRows.Length > 1 Then
            For Each rowDetail In currentRows
                Dim cell As New XRTableCell()
                row.Cells.Add(cell)
                cell.DataBindings.Add("Text", DataSource, CStr(rowDetail(0)), CStr(rowDetail(3)))
                cell.WidthF = CSng(rowDetail(1))
                cell.Name = CStr(rowDetail(2))
                cell.Summary.FormatString = CStr(rowDetail(3))
                cell.TextAlignment = CType(rowDetail(4), DevExpress.XtraPrinting.TextAlignment)
                Dim nFontType As Single = CSng(rowDetail(5))
                Dim nFontSize As Single = CSng(rowDetail(6))
                Dim nFontStyle As FontStyle = CType(rowDetail(7), FontStyle)
                cell.Font = New Font(getFontName(CType(nFontType, fontName)), nFontSize, nFontStyle)
                nTableWidth += CInt(rowDetail(1))
            Next
        End If
        Detail.Height = 21
        table.Rows.Add(row)
        'row.Height = 21
        row.WidthF = table.WidthF
        table.WidthF = nTableWidth
    End Sub

    Dim reportFooterTable As DataTable = New DataTable("reportFooterTable")
    Dim reportFooterRow As DataRow
    Private Sub CreateDataTableReportFooter()
        Dim cFieldNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cFieldName", .ColumnName = "cFieldName", .DefaultValue = ""}
        reportFooterTable.Columns.Add(cFieldNameColumn)

        Dim nCellWidthColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nCellWidth", .ColumnName = "nCellWidth", .DefaultValue = 0}
        reportFooterTable.Columns.Add(nCellWidthColumn)

        Dim cCellNameColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cCellName", .ColumnName = "cCellName", .DefaultValue = ""}
        reportFooterTable.Columns.Add(cCellNameColumn)

        Dim lUseFunction As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Boolean"), .AllowDBNull = False, .Caption = "lUseFunction", .ColumnName = "lUseFunction", .DefaultValue = False}
        reportFooterTable.Columns.Add(lUseFunction)

        Dim nFunctionColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFunction", .ColumnName = "nFunction", .DefaultValue = Nothing}
        reportFooterTable.Columns.Add(nFunctionColumn)

        Dim cFormatType As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.String"), .AllowDBNull = False, .Caption = "cFormatType", .ColumnName = "cFormatType", .DefaultValue = ""}
        reportFooterTable.Columns.Add(cFormatType)

        Dim cCellAlignmentColumn As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "cCellAlignment", .ColumnName = "cCellAlignment", .DefaultValue = DevExpress.XtraPrinting.TextAlignment.MiddleCenter}
        reportFooterTable.Columns.Add(cCellAlignmentColumn)

        Dim nFontType As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontType", .ColumnName = "nFontType", .DefaultValue = fontName.fTimesNewRoman}
        reportFooterTable.Columns.Add(nFontType)

        Dim nFontSize As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontSize", .ColumnName = "nFontSize", .DefaultValue = 12}
        reportFooterTable.Columns.Add(nFontSize)

        Dim nFontStyle As DataColumn = New DataColumn() With {.DataType = Type.GetType("System.Single"), .AllowDBNull = False, .Caption = "nFontStyle", .ColumnName = "nFontStyle", .DefaultValue = FontStyle.Regular}
        reportFooterTable.Columns.Add(nFontStyle)
    End Sub

    Public Sub AddReportFooter(ByVal cFieldName As String, ByVal nCellWidth As Single, ByVal cCellName As String, _
                               Optional ByVal lUseFunction As Boolean = False, Optional ByVal nFunction As SummaryFunc = Nothing, _
                               Optional ByVal nFormatType As ReportFieldFormat = ReportFieldFormat.Rpt_None, _
                               Optional ByVal cCellAlignment As DevExpress.XtraPrinting.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter, _
                               Optional ByVal nFontType As fontName = fontName.fTimesNewRoman, Optional ByVal nFontSize As Single = 10, _
                               Optional ByVal nFontStyle As FontStyle = FontStyle.Regular)
        reportFooterRow = reportFooterTable.NewRow()
        reportFooterRow(0) = cFieldName
        reportFooterRow(1) = nCellWidth * nCellWidthMultiplier
        reportFooterRow(2) = cCellName
        reportFooterRow(3) = lUseFunction
        reportFooterRow(4) = nFunction
        reportFooterRow(5) = vaFormat(nFormatType)
        reportFooterRow(6) = cCellAlignment
        reportFooterRow(7) = nFontType
        reportFooterRow(8) = nFontSize
        reportFooterRow(9) = nFontStyle
        reportFooterTable.Rows.Add(reportFooterRow)
    End Sub

    Public Sub CreateReportFooter()
        Dim nTableWidth As Integer = 0
        Dim rowReportFooter As DataRow
        Dim currentRows() As DataRow = reportFooterTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If currentRows.Length > 0 Then
            For Each rowReportFooter In currentRows
                Dim cell1 As New XRTableCell()
                rowFooter.Cells.Add(cell1)
                If CBool(rowReportFooter(3)) Then
                    cell1.DataBindings.Add("Text", DataSource, CStr(rowReportFooter(0)), CStr(rowReportFooter(4)))
                    cell1.Summary.Running = SummaryRunning.Report
                    cell1.Summary.Func = CType(rowReportFooter(4), SummaryFunc)
                    cell1.Summary.FormatString = CStr(rowReportFooter(5))
                Else
                    cell1.Text = CStr(rowReportFooter(0))
                End If
                cell1.WidthF = CSng(rowReportFooter(1))
                cell1.Name = CStr(rowReportFooter(2))
                cell1.TextAlignment = CType(rowReportFooter(6), DevExpress.XtraPrinting.TextAlignment)
                Dim nFontType As Single = CSng(rowReportFooter(7))
                Dim nFontSize As Single = CSng(rowReportFooter(8))
                Dim nFontStyle As FontStyle = CType(rowReportFooter(9), FontStyle)
                cell1.Font = New Font(getFontName(CType(nFontType, fontName)), nFontSize, nFontStyle)
                nTableWidth += CInt(rowReportFooter(1))
            Next
        End If
        ReportFooter.Height = 21
        tableFooter.Rows.Add(rowFooter)
        'rowFooter.Height = 21
        rowFooter.WidthF = table.WidthF
        tableFooter.WidthF = nTableWidth
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        cTime.Text = Format(Now, "dd-mm-yyyy hh:mm:ss")
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub Print(ByVal dbSource As DataTable, Optional ByVal lPreview As Boolean = True, Optional ByVal lLandscape As Boolean = False,
                     Optional ByVal nTopMargin As Integer = 0, Optional ByVal nBottomMargin As Integer = 0, Optional ByVal nLeftMargin As Integer = 0,
                     Optional ByVal nRightMargin As Integer = 0)
        DataSource = dbSource
        Landscape = lLandscape
        With Margins
            .Bottom = nBottomMargin
            .Top = nTopMargin
            .Left = nLeftMargin
            .Right = nRightMargin
        End With
        Using printTool As New ReportPrintTool(Me)
            If lPreview Then
                printTool.ShowPreviewDialog()
            Else
                printTool.Print()
            End If
        End Using
    End Sub
End Class