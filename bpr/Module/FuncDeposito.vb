﻿Imports bpr.MySQL_Data_Library

Module FuncDeposito
    ReadOnly objData As New data

    Public Structure TypeDeposito
        Public cRekening As String
        Public nBunga As Double
        Public nPajak As Double
        Public nSukuBunga As Double
        Public dTgl As Date
        Public dJthTmp As Date
        Public nNominal As Double
        Public nLama As Double
        Public cNomorBilyet As String
        Public lCair As Boolean
        Public cNama As String
        Public cARO As String
        Public cGolonganDeposan As String
        Public cNamaGolonganDeposan As String
        Public cGolonganDeposito As String
        Public cNamaGolonganDeposito As String
        Public cKode As String
        Public nTotalSaldoTabungan As Double
        Public cWajibPajak As String
        Public nTotalBunga As Double
        Public nTotalNominalDeposito As Double
        Public dTglPembukaan As Date
        Public cRekeningTabungan As String
        Public cAlamat As String
        Public lTutup As Boolean
        Public nNominalSetoranDeposito As Double
        Public cNamaNasabahTabungan As String
        Public cCabang As String
        Public lHitungPajak As Boolean
        Public IsAntarBank As Boolean
        Public nJumlahHari As Double
        Public nBungaHarian As Double
        Public nPajakBulanan As Double
        Public nTotalHariKeseluruhan As Double
        Public cJenisDeposan As String
        Public nHariAccrual1 As Double
        Public nHariAccrual2 As Double
        Public nBungaAccrual1 As Double
        Public nBungaAccrual2 As Double
        Public nPajakAccrual1 As Double
        Public nPajakAccrual2 As Double
        Public lHitungPajakPerpanjangan As Boolean
        Public lStatusPajakPerpanjangan As Boolean
        Public nTarifPajak As Single
        Public cAlamatLengkap As String
        Public dTglPeriode As Date
        Public dJthTmpPeriode As Date
        Public nNominalAktual As Double
        Public nNominalUntukMutasi As Double
        Public cWilayah As String
        Public cNamaWilayah As String
        Public nBungaAccrual As Double
        Public nPajakAccrual As Double
        Public lStatusBlokir As Boolean
        Public cKeterkaitan As String
        Public dTglCair As Date
        Public nPencairanPokokDeposito As Double
        Public nPerpanjanganKe As Integer
        Public nPerpanjanganKeSekarang As Integer
        Public nSukuBungaAccrual As Single
        Public cAlamatNasabahTabungan As String
        Public dTglBulanBerikutnya As Date
        Public dJthTmpBulanIni As Date
    End Structure

    Public Enum eMutasiDeposito
        Setoran_Deposito = 1
        Pencairan_Deposito = 2
        Bunga_Pajak_Deposito = 3
        Perpanjangan_Deposito = 4
    End Enum

    Sub UpdSetorDeposito(ByVal cRekening As String, ByVal nNominal As Double, ByVal cCabang As String,
                         ByVal cFaktur As String, ByVal dTgl As Date, ByVal dJthTmp As Date, ByVal cKas As String,
                         Optional ByVal lDeleteWhenExist As Boolean = False)
        Dim dbData As New DataTable
        Dim cRekeningTabungan As String = ""
        Dim cRekeningDeposito As String
        Dim cRekJurnalDeposito As String = ""
        Dim cKodePenarikanPemindahbukuan As String = ""
        Dim cKeterangan As String = ""
        Dim cAsalSetoran As String = ""
        Dim cGolonganDeposito As String = ""
        Dim cJenisDeposito As String = ""
        Dim cField As String = ""

        If cKas = "T" Then
            cRekeningTabungan = ""
            cGolonganDeposito = ""
            cJenisDeposito = ""
            cField = "RekeningTabungan,GolonganDeposito,JenisDeposito"
            dbData = objData.Browse(GetDSN, "deposito", cField, "Rekening", , cRekening)
            If dbData.Rows.Count > 0 Then
                With dbData.Rows(0)
                    cRekeningTabungan = .Item("RekeningTabungan").ToString
                    cGolonganDeposito = .Item("GolonganDeposito").ToString
                    cJenisDeposito = .Item("JenisDeposito").ToString
                End With
            End If
            cKodePenarikanPemindahbukuan = aCfg(eCfg.msKodePenarikanPemindahBukuan, "").ToString
            dbData = objData.Browse(GetDSN, "KodeTransaksi", "Rekening", "Kode", , cKodePenarikanPemindahbukuan)
            If dbData.Rows.Count > 0 Then
                'simpan di MutasiTabungan
                cKeterangan = "Setor Deposito Ke Rekening " & cRekening
                UpdMutasiTabungan(cCabang, cKodePenarikanPemindahbukuan, cFaktur, dTgl, cRekeningTabungan, _
                                  nNominal, True, cKeterangan, False, SNow)

            End If
        End If

        dbData = objData.Browse(GetDSN, "GolonganDeposito", , "Kode", , cGolonganDeposito)
        If dbData.Rows.Count > 0 Then
            cRekeningDeposito = dbData.Rows(0).Item("RekeningAkuntansi").ToString
        Else
            cRekeningDeposito = ""
        End If

        If cJenisDeposito = "1" Then ' perorangan
            cRekJurnalDeposito = cRekeningDeposito
        Else
            cRekJurnalDeposito = aCfg(eCfg.msRekeningPemindahBukuan).ToString
        End If
        If cKas = "K" Then
            cAsalSetoran = "1"
        ElseIf cKas = "T" Then
            cAsalSetoran = "2"
        ElseIf cKas = "P" Then
            cAsalSetoran = "3"
        End If
        Dim vaField() As Object = {"Nominal", "Status", "AsalSetoran"}
        Dim vaValue() As Object = {nNominal, "0", cAsalSetoran}
        objData.Edit(GetDSN, "Deposito", String.Format("Rekening = '{0}'", cRekening), vaField, vaValue)

        UpdMutasiDeposito(cCabang, cFaktur, cRekening, dTgl, dJthTmp, nNominal, , , , , lDeleteWhenExist, , , , cKas)
    End Sub

    Sub UpdMutasiDeposito(ByVal cCabang As String, ByVal cFaktur As String,
                          ByVal cRekening As String, ByVal dTgl As Date, Optional dJthTmp As Date = #1/1/1900#,
                          Optional ByVal nSetoranPlafond As Double = 0, Optional ByVal nPencairanPlafond As Double = 0,
                          Optional ByVal nBunga As Double = 0, Optional ByVal nPajak As Double = 0,
                          Optional ByVal nPinalty As Double = 0, Optional ByVal lDeleteWhenExist As Boolean = False,
                          Optional ByVal lUpdateRekening As Boolean = True, Optional ByVal cUser As String = "",
                          Optional ByVal cNow As DateTime = Nothing, Optional ByVal cKas As String = "K",
                          Optional ByVal nPersenBunga As Double = 0, Optional ByVal cNomorCek As String = "",
                          Optional ByVal nSisaBunga As Double = 0, Optional ByVal nSisaPajak As Double = 0,
                          Optional ByVal cKeterangan As String = "")
        Dim cJenis As String = IIf(dJthTmp = #1/1/1900# And nPencairanPlafond = 0, "0", "1").ToString
        Dim nTabungan As Double
        Dim cRekeningTabungan As String
        Dim db As New DataTable
        Dim cRekeningJurnal As String = ""
        Dim nStatus As Integer
        Dim cWhere As String
        Dim vaField() As Object
        Dim vaValue() As Object

        cUser = GetNull(cUser.ToString, cUserName).ToString
        cNow = CType(GetNull(CType(cNow, DateTime), SNow), DateTime)
        If lDeleteWhenExist Then
            objData.Delete(GetDSN, "MutasiDeposito", "Faktur", , cFaktur)
        End If

        If cKas <> "E" Then
            If nPencairanPlafond > 0 Then
                nStatus = 2
            Else
                nStatus = 1
            End If
            cWhere = String.Format(" and date_format(tgl,'%Y%m') = date_format('{0}','%Y%m') ", formatValue(dTgl, formatType.yyyy_MM_dd))
            cWhere = String.Format("{0} and status = {1}", cWhere, nStatus)
            objData.Delete(GetDSN, "jadwalbungadeposito", "rekening", , cRekening, cWhere)

            vaField = {"rekening", "tgl", "jthtmp",
                       "status", "tgldibayar", "faktur",
                       "cabangentry", "datetime"}
            vaValue = {cRekening, formatValue(dTgl, formatType.yyyy_MM_dd), formatValue(dJthTmp, formatType.yyyy_MM_dd), _
                       nStatus, formatValue(dTgl, formatType.yyyy_MM_dd), cFaktur, _
                       cCabang, SNow()}
            objData.Add(GetDSN, "jadwalbungadeposito", vaField, vaValue)
        End If

        If nSetoranPlafond + nPencairanPlafond + nBunga + nPajak + nPinalty <> 0 Or dJthTmp <> #1/1/1900# Then
            vaField = {"Jenis", "Faktur", "Rekening", "Tgl", "JTHTMP", _
                       "SetoranPlafond", "PencairanPlafond", "Bunga", "Pajak", _
                       "Pinalty", "Kas", "UserName", "DateTime", _
                       "PersenBunga", "CabangEntry", "Nomor", "SisaBunga", _
                       "SisaPajak", "Keterangan", "IP", "Version"}
            vaValue = {cJenis, cFaktur, cRekening, dTgl, dJthTmp, _
                       nSetoranPlafond, nPencairanPlafond, nBunga, nPajak, _
                       nPinalty, cKas, cUser, cNow, _
                       nPersenBunga, cCabang, cNomorCek, nSisaBunga, _
                       nSisaPajak, cKeterangan, cIPNumberLocal, cAppVersion}
            objData.Add(GetDSN, "MutasiDeposito", vaField, vaValue)

            If cKas = "T" Then ' jika setor ke tabungan
                db = objData.Browse(GetDSN, "Deposito d", "t.Rekening", "d.Rekening", , cRekening, , , _
                                    {"Left Join Tabungan t on d.RekeningTabungan = t.Rekening"})
                If db.Rows.Count > 0 Then
                    With db.Rows(0)
                        If GetNull(.Item("Rekening").ToString, "").ToString <> "" Then
                            If nPencairanPlafond > 0 Or nBunga > 0 Then
                                DelMutasiTabungan(cFaktur)
                            End If
                            cRekeningTabungan = GetNull(.Item("Rekening").ToString, "").ToString
                            cRekeningJurnal = aCfg(eCfg.msRekeningSetoranTabunganPB).ToString
                            If nPencairanPlafond > 0 Then ' jika plafond dicairkan
                                nTabungan = nPencairanPlafond ' update nominal pencairan plafond
                                UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString, cFaktur, dTgl,
                                                  cRekeningTabungan, nTabungan, False, "Setoran Dari Deposito", False, , cRekeningJurnal)

                                nTabungan = nBunga - nPajak - nPinalty ' update bunga
                                UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString, cFaktur, dTgl,
                                                  cRekeningTabungan, nTabungan, False, "Setoran Dari Deposito", False, , cRekeningJurnal)
                            ElseIf nBunga > 0 And nPencairanPlafond = 0 Then ' jika hanya pencairan bunga
                                nTabungan = nBunga - nPajak - nPinalty ' update bunga
                                UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString, cFaktur, dTgl,
                                                  cRekeningTabungan, nTabungan, False, "Setoran Dari Deposito Rek. " & cRekening, False,
                                                  , cRekeningJurnal)
                            End If
                        End If
                    End With
                End If
            End If
        End If
        db.Dispose()

        If lUpdateRekening Then
            UpdRekDepositoDenganAccrual(cFaktur)
        End If
    End Sub

    Sub UpdRekDepositoDenganAccrual(ByVal cFaktur As String)
        Dim db As New DataTable
        Dim db1 As New DataTable
        Dim dbPusat As New DataTable
        Dim dTgl As Date
        Dim nTotal As Double
        Dim nTotal1 As Double
        Dim nDKas As Double
        Dim nKKas As Double
        Dim nDKas1 As Double
        Dim nKKas1 As Double
        Dim nPajak As Double
        Dim nBunga As Double
        Dim nSisaPajak As Double
        Dim nSisaBunga As Double
        Dim nBungaAccrual As Double
        Dim nPajakAccrual As Double
        Dim cCabang As String

        Dim cRekening As String
        Dim cCabangEntryTemp As String
        Dim cKas As String
        Dim nKas As Double
        Dim cRekeningJurnalTabungan As String = ""
        Dim X As TypeDeposito = Nothing
        Dim cKeteranganBunga As String
        Dim cKeteranganAccrual As String
        Dim cKeteranganPajak As String
        Dim cKeteranganPokok As String

        Dim cRekeningKas As String
        Dim cRekeningJurnal As String
        Dim cRekeningRRP As String
        Dim cRekeningTitipanPajak As String
        Dim cRekeningAntarKantorPasiva As String
        Dim cRekeningAntarKantorAktiva As String
        Dim cRekJurnal As String
        Dim cRekJurnal1 As String

        Dim cField As String = "m.faktur,m.Kas,m.ID,m.SetoranPlafond,m.PencairanPlafond,m.sisabunga,m.sisapajak,m.DateTime,"
        cField = cField & "m.Bunga,m.Pajak,m.Pinalty,m.cabangentry,m.rekening,"
        cField = cField & "m.Tgl as TglTransaksi,r.Nama as NamaCustomer,"
        cField = cField & "d.RekeningTabungan,g.RekeningBunga,g.RekeningPajakBunga,d.AsalSetoran,"
        cField = cField & "g.RekeningPinalti,g.RekeningAkuntansi,g.RekeningAccrual,"
        cField = cField & "m.username as username"
        Dim vaJoin() As Object = {"Left Join Deposito d on m.Rekening = d.Rekening", _
                                  "Left Join GolonganDeposito g on d.GolonganDeposito = g.Kode", _
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode"}
        db = objData.Browse(GetDSN, "MutasiDeposito m", cField, "m.Faktur", , cFaktur, , , vaJoin)
        If db.Rows.Count > 0 Then
            For n = 0 To db.Rows.Count - 1
                With db.Rows(n)
                    cRekeningRRP = GetNull(.Item("RekeningAccrual").ToString, "").ToString
                    If .Item("Kas").ToString = "T" Then
                        vaJoin = {"left join golongantabungan g on g.kode = t.golongantabungan"}
                        db1 = objData.Browse(GetDSN, "tabungan t", "g.Rekening as RekeningJurnal","t.rekening", , .Item("RekeningTabungan").ToString, , , vaJoin)
                        If db1.Rows.Count > 0 Then
                            cRekeningJurnalTabungan = GetNull(db1.Rows(0).Item("RekeningJurnal").ToString, "").ToString
                        Else
                            cRekeningJurnalTabungan = ""
                        End If
                    End If
                    Select Case .Item("Kas").ToString
                        Case "K"
                            cRekeningKas = cKasTeller
                        Case "R"
                            cRekeningKas = cRekeningRRP
                        Case "T"
                            cRekeningKas = cRekeningJurnalTabungan
                        Case Else
                            cRekeningKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    End Select
                    If Debugger.IsAttached And UCase(cUserName) = "HEASOFT" Then
                        If .Item("Kas").ToString = "K" Then
                            db1 = objData.Browse(GetDSN, "username", "kasteller", "username", , .Item("UserName").ToString)
                            If db1.Rows.Count > 0 Then
                                cRekeningKas = GetNull(db1.Rows(0).Item("KasTeller").ToString, cRekeningKas).ToString
                                If .Item("UserName").ToString = "01TYA" Then
                                    cRekeningKas = "1.110.10.02"
                                End If
                            End If
                        End If
                    End If
                    cRekeningTitipanPajak = aCfg(eCfg.msRekeningTitipanPajakDeposito).ToString
                    nBunga = Val(.Item("Bunga"))
                    nPajak = Val(.Item("Pajak"))
                    GetDeposito(.Item("Rekening").ToString, DateAdd("m", -1, CDate(.Item("TglTransaksi"))), X)
                    nSisaBunga = X.nBungaAccrual2  '.item("SisaBunga").ToString
                    nSisaPajak = X.nPajakAccrual2  '.item("SisaPajak").ToString
                    nBungaAccrual = (nBunga - nPajak) - (nSisaBunga - nSisaPajak)
                    nPajakAccrual = nPajak - nSisaPajak
                    cCabang = .Item("CabangEntry").ToString
                    cRekening = .Item("Rekening").ToString
                    cKas = .Item("Kas").ToString
                    nTotal = Val(.Item("SetoranPlafond")) - Val(.Item("PencairanPlafond")) - Val(.Item("Bunga")) + Val(.Item("Pajak")) + Val(.Item("Pinalty"))
                    nTotal1 = Val(.Item("SetoranPlafond")) - Val(.Item("PencairanPlafond")) - nSisaBunga - nBungaAccrual + Val(.Item("Pinalty"))
                    nDKas = Val(IIf(nTotal > 0, nTotal, 0))
                    nKKas = Val(IIf(nTotal < 0, -nTotal, 0))
                    nDKas1 = Val(IIf(nTotal1 > 0, nTotal1, 0))
                    nKKas1 = Val(IIf(nTotal1 < 0, -nTotal1, 0))

                    dTgl = CDate(.Item("TglTransaksi"))
                    cRekeningJurnal = GetNull(.Item("RekeningAkuntansi").ToString, "").ToString

                    cRekeningAntarKantorPasiva = aCfg(eCfg.msRekeningAntarKantorPasiva).ToString
                    cRekeningAntarKantorAktiva = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString

                    ' jika ada transaksi antar kantor
                    If Not IsSatuCabang(cCabang, .Item("Rekening").ToString.Substring(0, 2)) Then
                        ' Jurnal
                        ' Dr. Kas                                     nDKas
                        ' Dr. Rekening Antar Kantor Aktiva/Pasiva     nDKas
                        '   Cr. Kas                                     nKKas
                        '   Cr. Rekening Antar Kantor Aktiva/Pasiva     nKKas
                        nKas = Val(IIf(nDKas = 0, nKKas, nDKas))
                        cRekJurnal = IIf(nDKas = 0, cRekeningAntarKantorAktiva, cRekeningKas).ToString
                        cRekJurnal1 = IIf(nKKas = 0, cRekeningAntarKantorPasiva, cRekeningKas).ToString
                        cKeteranganPokok = "Antar Kantor Deposito an. " & GetNull(.Item("NamaCustomer").ToString, "").ToString
                        UpdBukuBesar(Val(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTgl, cRekJurnal,
                                     cKeteranganPokok, nKas, , , .Item("UserName").ToString)
                        UpdBukuBesar(Val(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTgl, cRekJurnal1,
                                     cKeteranganPokok, , nKas, , .Item("UserName").ToString)
                    End If

                    cRekJurnal = cRekeningKas
                    cCabangEntryTemp = .Item("CabangEntry").ToString
                    If Not IsSatuCabang(cCabang, .Item("Rekening").ToString.Substring(0, 2)) Then
                        cRekJurnal = cRekeningAntarKantorAktiva
                        cCabangEntryTemp = cRekening.Substring(0, 2)
                    End If

                    Dim cAsalSetoran As String = IIf(GetNull(.Item("AsalSetoran").ToString, "").ToString = "1", "K", "N").ToString
                    Dim cNowTemp As String = GetNull(CType(.Item("DateTime"), DateTime), "1999-99-99 01:01:01").ToString
                    Dim cUserTemp As String = GetNull(.Item("UserName").ToString, "").ToString
                    If Val(.Item("SetoranPlafond")) > 0 Then ' jika setoran deposito
                        If cKas <> "E" Then
                            ' Jurnal
                            ' Dr. Kas/PB/Tabungan
                            '   Cr. Deposito
                            cKeteranganPokok = "Setoran Deposito an. " & GetNull(.Item("NamaCustomer").ToString, "").ToString
                            UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekJurnal,
                                         cKeteranganPokok, nTotal, , cNowTemp, cUserTemp)
                            UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekeningJurnal,
                                         cKeteranganPokok, , nTotal, cNowTemp, cUserTemp)
                        End If
                    ElseIf Val(.Item("PencairanPlafond")) > 0 Then ' jika pencairan deposito
                        ' Jurnal
                        ' Dr. Deposito
                        '   Cr. Kas/PB/Tabungan
                        cKeteranganPokok = "Pencairan Deposito an. " & GetNull(.Item("NamaCustomer").ToString, "").ToString
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekeningJurnal,
                                     cKeteranganPokok, Val(GetNull(.Item("PencairanPlafond"))), , cNowTemp, cUserTemp)
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekJurnal,
                                     cKeteranganPokok, , Val(GetNull(.Item("PencairanPlafond"))), cNowTemp, cUserTemp)
                    End If

                    If Val(.Item("Bunga")) > 0 Or (Val(.Item("SetoranPlafond")) > 0 And cKas = "E") Then ' jika pencairan bunga
                        cKeteranganBunga = "Bunga Deposito an. " & GetNull(.Item("NamaCustomer").ToString, "").ToString
                        cKeteranganAccrual = String.Format("Accrual Bunga Deposito an. {0} - {1}", GetNull(.Item("NamaCustomer").ToString, ""), .Item("Rekening"))
                        cKeteranganPajak = "Pajak Bunga Deposito an. " & GetNull(.Item("NamaCustomer").ToString, "").ToString

                        Dim cRekBunga As String = GetNull(.Item("RekeningBunga").ToString, "").ToString
                        Dim cRekPajakBunga As String = GetNull(.Item("RekeningPajakBunga").ToString, "").ToString
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekBunga, cKeteranganBunga,
                                     nSisaBunga, , , .Item("UserName").ToString)
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekeningRRP, cKeteranganAccrual,
                                     nBunga - nSisaBunga, , , .Item("UserName").ToString)
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekJurnal, cKeteranganBunga,
                                     , nBunga - nPajak, , .Item("UserName").ToString)
                        UpdBukuBesar(Val(.Item("Id")), cCabangEntryTemp, cFaktur, dTgl, cRekPajakBunga, cKeteranganPajak,
                                     , nPajak, , .Item("UserName").ToString)

                    End If
                End With
            Next
        End If
        db.Dispose()
        db1.Dispose()
        dbPusat.Dispose()
    End Sub

    Sub UpdPembukaanDeposito(ByVal cRekening As String, ByVal dTgl As Date, ByVal dJthTmp As Date, _
                         ByVal cNoRegister As String, ByVal cGolonganDeposan As String, ByVal cGolonganDeposito As String, _
                         ByVal cAO As String, ByVal cARO As String, ByVal cDateTime As DateTime, _
                         ByVal cTujuanPencairanPokok As String, ByVal cTujuanPencairanBunga As String, _
                         ByVal cAsalSetoran As String, ByVal nSukuBunga As Double, _
                         ByVal cRekeningTabungan As String, ByVal cStatusPajak As String, _
                         ByVal cJenisDeposan As String, ByVal cKeterkaitan As String, _
                         ByVal cWilayah As String, ByVal cNomorBilyet As String, ByVal cJenisDeposito As String, _
                         ByVal nLama As Double, Optional ByVal lEdit As Boolean = False, Optional ByVal cNasabah As String = "01")

        If GetNull(CDate(cDateTime), "").ToString = "" Then cDateTime = Now
        Dim vaField() As Object = {"Rekening", "Tgl", "jthTmp", "Kode", _
                                   "GolonganDeposan", "GolonganDeposito", _
                                   "ARO", "DateTime", "TujuanPencairanPokok", _
                                   "TujuanPencairanBunga", "AsalSetoran", "SukuBunga", _
                                   "RekeningTabungan", "StatusPajak", "JenisDeposan", _
                                   "Wilayah", "Keterkaitan", "JenisDeposito", _
                                   "NoBilyet", "UserName", "AO", "CabangEntry", _
                                   "Lama", "Nasabah"}
        Dim vaValue() As Object = {cRekening, dTgl, dJthTmp, cNoRegister, _
                                   cGolonganDeposan, cGolonganDeposito, _
                                   cARO, cDateTime, cTujuanPencairanPokok, _
                                   cTujuanPencairanBunga, cAsalSetoran, nSukuBunga, _
                                   cRekeningTabungan, cStatusPajak, cJenisDeposan, _
                                   cWilayah, cKeterkaitan, cJenisDeposito, _
                                   cNomorBilyet, cUserName, cAO, aCfg(eCfg.msKodeCabang), _
                                   nLama, cNasabah}
        If lEdit Then
            objData.Edit(GetDSN, "Deposito", String.Format("rekening ='{0}'", cRekening), vaField, vaValue)
        Else
            objData.Update(GetDSN, "deposito", String.Format("Rekening = '{0}'", cRekening), vaField, vaValue)
        End If
    End Sub

    Function GetTglValutaDeposito(ByVal dTglValuta As Date, ByVal nLama As Double, ByVal dTgl As Date, ByVal cARO As String) As Date
        Dim d As Date = dTglValuta
        Dim n As Double
        If cARO = "Y" Then
            n = nLama
            d = DateAdd("m", n, dTglValuta)
            Do While d < dTgl
                n = n + nLama
                d = DateAdd("m", n, dTglValuta)
            Loop
            If d >= dTgl Then
                n = n - nLama
                d = DateAdd("m", n, dTglValuta)
            End If
        End If
        GetTglValutaDeposito = d
    End Function

    Function GetJTHTMPDeposito(ByVal dTglValuta As Date, ByVal nLama As Double, ByVal dTgl As Date, ByVal cARO As String) As Date
        Dim d As Date

        If cARO = "Y" Then
            dTglValuta = GetTglValutaDeposito(dTglValuta, nLama, dTgl, cARO)
        End If
        d = DateAdd("m", nLama, dTglValuta)

        Do While IsHoliday(d)
            d = DateAdd("d", 1, d)
        Loop
        GetJTHTMPDeposito = d
    End Function

    Sub GetDeposito(ByVal cRekening As String, ByVal dTgl As Date, X As TypeDeposito, _
                    Optional ByVal lMutasiPerpanjangan As Boolean = False)
        Dim db As New DataTable
        Dim db1 As New DataTable
        Dim cWhere As String
        Dim nHari As Double
        Dim n As Double
        Dim nTotalBunga As Double
        Dim dTglAkhirBulan As Date
        Dim lCountPajak As Boolean
        Dim lSetorPertama As Boolean = True
        Dim cAlamatLengkap As String
        Dim dTglPenutupan As Date
        Dim dDate As Date
        Dim nSukuBungaTemp As Single
        Dim nSukuBungaLama As Single
        Dim dTglTemp As Date
        Dim lPerpanjangan As Boolean = False
        Dim nBungaBulanan As Double

        X.cRekening = ""
        X.dJthTmp = Now.Date
        X.dTgl = Now.Date
        X.nBunga = 0
        X.nPajak = 0
        X.nLama = 0
        X.nNominal = 0
        X.nNominalAktual = 0
        X.nNominalUntukMutasi = 0
        X.nSukuBunga = 0
        X.cNomorBilyet = ""
        X.lCair = False
        X.cNama = ""
        X.cARO = ""
        X.cGolonganDeposan = ""
        X.cGolonganDeposito = ""
        X.cNamaGolonganDeposito = ""
        X.cNamaGolonganDeposan = ""
        X.cKode = ""
        X.nTotalSaldoTabungan = 0
        X.cWajibPajak = "Y"
        X.nTotalBunga = 0
        X.nTotalNominalDeposito = 0
        X.dTglPembukaan = Now.Date
        X.cRekeningTabungan = ""
        X.cAlamat = ""
        X.lTutup = False
        X.nNominalSetoranDeposito = 0
        X.cNamaNasabahTabungan = ""
        X.cCabang = ""
        X.lHitungPajak = True
        X.IsAntarBank = False
        X.nJumlahHari = 0
        X.nBungaHarian = 0
        X.nPajakBulanan = 0
        X.nTotalHariKeseluruhan = 0
        X.cJenisDeposan = ""
        X.nHariAccrual1 = 0
        X.nHariAccrual2 = 0
        X.nBungaAccrual1 = 0
        X.nBungaAccrual2 = 0
        X.nPajakAccrual1 = 0
        X.nPajakAccrual2 = 0
        X.lHitungPajakPerpanjangan = False
        X.lStatusPajakPerpanjangan = False
        X.nTarifPajak = 0
        X.cAlamatLengkap = ""
        X.dTglPeriode = Now.Date
        X.dJthTmpPeriode = Now.Date
        X.cWilayah = ""
        X.cNamaWilayah = ""
        X.nBungaAccrual = 0
        X.nPajakAccrual = 0
        X.lStatusBlokir = False
        X.cKeterkaitan = ""
        X.dTglCair = Now.Date
        X.lCair = False
        X.nPencairanPokokDeposito = 0
        X.nPerpanjanganKe = 0
        X.nPerpanjanganKeSekarang = 0
        X.nSukuBungaAccrual = 0
        X.cAlamatNasabahTabungan = ""
        X.dTglBulanBerikutnya = Now.Date
        X.dJthTmpBulanIni = Now.Date

        Dim cTgl As String = formatValue(dTgl, formatType.yyyy_MM_dd).ToString
        Dim cField As String = String.Format("d.*,r.Nama,r.Alamat,g.Keterangan as NamaGolonganDeposito,valutadeposito('{0}', d.lama, d.tgl) as tglvaluta,tempodeposito('{0}', d.lama, d.tgl) as tempo, date_add(valutadeposito('{0}', d.lama, d.tgl), interval 1 month) as tglbulanberikutnya,", cTgl)
        cField = cField & "o.Keterangan as NamaGolonganDeposan,g.PajakBunga,w.Keterangan as NamaWilayah,"
        cField = String.Format("{0}r.kodya,kd.keterangan as NamaKodya,r.kecamatan,kc.keterangan as NamaKecamatan,", cField)
        cField = cField & "r.kelurahan,kl.keterangan as NamaKelurahan"
        Dim vaJoin() As Object = {"Left Join GolonganDeposito g on d.GolonganDeposito = g.Kode", _
                                   "Left Join RegisterNasabah r on d.Kode = r.Kode", _
                                   "Left join GolonganDeposan o on d.golongandeposan=o.kode", _
                                   "Left Join Kodya kd on kd.kode = r.kodya", _
                                   "Left join Kecamatan kc on kc.kode = r.kecamatan and kc.kodya = r.kodya", _
                                   "Left Join Kelurahan kl on kl.kode = r.kelurahan and kl.kecamatan = r.kecamatan and kl.kodya = r.kodya", _
                                   "left Join wilayah w on w.kode=d.wilayah"}
        db = objData.Browse(GetDSN, "Deposito d", cField, "d.Rekening", , cRekening, , , vaJoin)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                dTglPenutupan = CDate(IIf(IsNothing(.Item("TglCair")) Or .Item("TglCair").ToString = "9999-99-99", "9999-01-01", .Item("TglCair").ToString))

                X.lCair = dTglPenutupan <= dTgl
                X.cRekening = GetNull(.Item("Rekening").ToString, "").ToString
                X.cKode = GetNull(.Item("Kode").ToString, "").ToString
                X.cNomorBilyet = GetNull(.Item("nobilyet").ToString, "").ToString
                X.dJthTmp = CDate(GetNull(.Item("JthTmp"), #1/1/1900#))
                X.dTgl = CDate(GetNull(.Item("Tgl"), #1/1/1900#))
                X.nLama = Val(GetNull(.Item("Lama")))
                X.dTglPembukaan = CDate(GetNull(.Item("Tgl"), #1/1/1900#))
                X.cRekeningTabungan = GetNull(.Item("RekeningTabungan").ToString, "").ToString
                X.nSukuBunga = CSng(GetNull(.Item("SukuBunga")))
                nSukuBungaTemp = CSng(GetNull(.Item("SukuBunga")))
                X.cNama = GetNull(.Item("Nama").ToString, "").ToString
                X.cAlamat = GetNull(.Item("Alamat").ToString, "").ToString
                X.cARO = GetNull(.Item("ARO").ToString, "").ToString
                X.cGolonganDeposan = GetNull(.Item("GolonganDeposan").ToString, "").ToString
                X.cNamaGolonganDeposan = GetNull(.Item("NamaGolonganDeposan").ToString).ToString
                X.cGolonganDeposito = GetNull(.Item("GolonganDeposito"), "").ToString
                X.cNamaGolonganDeposito = GetNull(.Item("NamaGolonganDeposito"), "").ToString
                X.cWajibPajak = GetNull(.Item("StatusPajak"), "").ToString
                X.cCabang = GetCabang(, True, .Item("Rekening").ToString)
                X.cJenisDeposan = .Item("JenisDeposan").ToString
                X.nTarifPajak = CSng(GetNull(.Item("PajakBunga")))
                X.dTglPeriode = CDate(.Item("tglvaluta"))
                X.dTglBulanBerikutnya = CDate(.Item("tglbulanberikutnya"))
                X.dJthTmpPeriode = CDate(.Item("Tempo"))
                X.dJthTmpBulanIni = CDate(.Item("Tempo"))
                X.cWilayah = GetNull(.Item("Wilayah"), "").ToString
                X.cNamaWilayah = GetNull(.Item("NamaWilayah"), "").ToString
                If .Item("StatusBlokir").ToString = "Y" Then X.lStatusBlokir = True
                X.cKeterkaitan = GetNull(.Item("Keterkaitan"), "").ToString
                X.nNominalAktual = Val(GetNull(.Item("Nominal")))
                X.nNominalUntukMutasi = Val(GetNull(.Item("Nominal")))
                X.nNominal = Val(GetNull(.Item("Nominal")))

                cAlamatLengkap = GetNull(.Item("Alamat"), "").ToString
                If GetNull(.Item("NamaKelurahan"), "").ToString <> "" Then
                    cAlamatLengkap = String.Format("{0}, {1}", cAlamatLengkap, GetNull(.Item("NamaKelurahan"), ""))
                End If
                If GetNull(.Item("namakecamatan"), "").ToString <> "" Then
                    cAlamatLengkap = String.Format("{0}, {1}", cAlamatLengkap, GetNull(.Item("namakecamatan"), ""))
                End If
                If GetNull(.Item("NamaKodya"), "").ToString <> "" Then
                    cAlamatLengkap = String.Format("{0}, {1}", cAlamatLengkap, GetNull(.Item("NamaKodya"), ""))
                End If
                X.cAlamatLengkap = cAlamatLengkap

                If .Item("JenisDeposito").ToString = "2" Then
                    X.IsAntarBank = True
                End If

                If .Item("StatusPajak").ToString = "T" Then
                    X.lHitungPajak = False
                End If
            End With
            ' Hitung Saldo Tabungan
            cField = "d.Rekening,Sum(m.Kredit-m.Debet) as Saldo"
            cWhere = " Group by d.Kode,d.Rekening"
            vaJoin = {String.Format("Left Join MutasiTabungan m on m.Rekening = d.Rekening and m.Tgl <= '{0}'", formatValue(dTgl, formatType.yyyy_MM_dd))}
            db = objData.Browse(GetDSN, "Tabungan d", cField, "d.Kode", , X.cKode, cWhere, , vaJoin)
            If db.Rows.Count > 0 Then
                X.nTotalSaldoTabungan = 0
                For n = 0 To db.Rows.Count - 1
                    X.nTotalSaldoTabungan = X.nTotalSaldoTabungan + Val(GetNull(db.Rows(CInt(n)).Item("Saldo")))
                Next
            End If

            ' Cari Untuk Nominal Deposito Terbaru
            cField = ""
            cField = "Sum(SetoranPlafond) as SetoranPlafond,Max(Tgl) as Tgl,Max(JTHTMP) as JT,"
            cField = cField & "max(CaraPencairan) as CaraPencairan,Sum(PencairanPlafond) as PencairanPlafond"
            dDate = dTgl
            cWhere = ""
            cWhere = String.Format("and Rekening = '{0}' ", X.cRekening)
            cWhere = String.Format("{0}and date_format(Tgl,'%Y%m') <= date_format('{1}','%Y%m')", cWhere, formatValue(dDate, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "MutasiDeposito", cField, "CaraPencairan", , "E", cWhere)
            If db.Rows.Count > 0 Then
                X.nNominalAktual = X.nNominalAktual + Val(GetNull(db.Rows(0).Item("SetoranPlafond")))
            End If

            cWhere = "and pencairanplafond > 0"
            db = objData.Browse(GetDSN, "MutasiDeposito", "PencairanPlafond", "rekening", , cRekening, cWhere)
            If db.Rows.Count > 0 Then
                X.nPencairanPokokDeposito = Val(GetNull(db.Rows(0).Item("PencairanPlafond")))
            End If

            If dTglPenutupan = #12/31/9999# And X.nPencairanPokokDeposito > 0 Then
                X.lCair = X.nPencairanPokokDeposito > 0
            End If

            ' cari untuk nominal deposito laporan mutasi harian deposito
            cField = ""
            cField = "Sum(SetoranPlafond) as SetoranPlafond,Max(Tgl) as Tgl,Max(JTHTMP) as JT,"
            cField = cField & "max(CaraPencairan) as CaraPencairan,Sum(PencairanPlafond) as PencairanPlafond"
            dDate = dTgl
            cWhere = ""
            cWhere = String.Format("and Rekening = '{0}' ", X.cRekening)
            cWhere = String.Format("{0}and Tgl <= '{1}'", cWhere, formatValue(dDate, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "MutasiDeposito", cField, "CaraPencairan", , "E", cWhere)
            If db.Rows.Count > 0 Then
                X.nNominalUntukMutasi = X.nNominalUntukMutasi + Val(GetNull(db.Rows(0).Item("SetoranPlafond")))
            End If

            ' Cari Untuk Nominal Deposito 1 bulan Terakhir
            cField = ""
            cField = "Sum(SetoranPlafond) as SetoranPlafond,Max(Tgl) as Tgl,Max(JTHTMP) as JT,"
            cField = cField & "max(CaraPencairan) as CaraPencairan,Sum(PencairanPlafond) as PencairanPlafond"
            dDate = DateAdd("m", -1, dTgl)
            cWhere = ""
            cWhere = String.Format("and Rekening = '{0}' ", X.cRekening)
            cWhere = String.Format("{0}and date_format(Tgl,'%Y%m') <= date_format('{1}','%Y%m')", cWhere, formatValue(dDate, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "MutasiDeposito", cField, "CaraPencairan", , "E", cWhere)
            If db.Rows.Count > 0 Then
                X.nNominal = X.nNominal + Val(GetNull(db.Rows(0).Item("SetoranPlafond")))
            End If

            cWhere = ""
            cWhere = String.Format(" and status =0 and date_format(tgl,'%Y%m') <= date_format('{0}','%Y%m') ", formatValue(DateAdd("m", -1, dTgl), formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "jadwalbungadeposito", , "rekening", , cRekening, cWhere, "tgl desc limit 1")
            If db.Rows.Count > 0 Then
                X.nPerpanjanganKe = CInt(Val(GetNull(db.Rows(0).Item("PerpanjanganKe"))) + 1)
            End If

            ' Cari Untuk Tgl Perpanjangan Terakhir
            cField = "Max(Tgl) as Tgl,Max(JthTmp) as JthTmp"
            cWhere = ""
            cWhere = String.Format(" And Rekening = '{0}' and Tgl <= '{1}' ", X.cRekening, formatValue(dTgl, formatType.yyyy_MM_dd))
            cWhere = cWhere & " and Kas='E'"
            db = objData.Browse(GetDSN, "MutasiDeposito", cField, "Jenis", , "1", cWhere, , , "Rekening")
            If db.Rows.Count > 0 Then
                If dTgl >= CDate(db.Rows(0).Item("Tgl")) And dTgl <= CDate(db.Rows(0).Item("JthTmp")) Then
                    lPerpanjangan = True
                End If
                lSetorPertama = False
            End If
            ' Hitung total deposito untuk perhitungan pajak
            X.nTotalNominalDeposito = GetSaldoNasabah(dTgl, X.cKode)
            If X.IsAntarBank = False Then
                If X.nTotalNominalDeposito <= 0 Then
                    X.nTotalNominalDeposito = X.nNominal
                End If
            End If

            ' Hitung Total Setoran Deposito Berdasarkan Kode Register
            cField = "Sum(m.SetoranPlafond-m.PencairanPlafond) as Saldo"
            vaJoin = {String.Format("Left Join MutasiDeposito m on m.Rekening = d.Rekening and m.Tgl <= '{0}'", formatValue(dTgl, formatType.yyyy_MM_dd))}
            db = objData.Browse(GetDSN, "Deposito d", cField, "d.Kode", , X.cKode, , , vaJoin)
            If db.Rows.Count > 0 Then
                X.nNominalSetoranDeposito = Val(GetNull(db.Rows(0).Item("Saldo")))
            End If

            'jika jenis mutasinya adl perpanjangan deposito maka tanggal parameternya dikurangi satu bulan untuk
            'mencari data bunga pada periode sebelumnya
            If lMutasiPerpanjangan Then
                dTglTemp = DateAdd("m", -1, dTgl)
            Else
                dTglTemp = dTgl
            End If
            ' cari tanggal dan jatuh tempo untuk menghitung bunga deposito bulan sekarang
            cWhere = ""
            cWhere = String.Format(" and status =0 and date_format(tgl,'%Y%m') = date_format('{0}','%Y%m') ", formatValue(dTglTemp, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "jadwalbungadeposito", , "rekening", , cRekening, cWhere)
            If db.Rows.Count > 0 Then
                With db.Rows(0)
                    X.dJthTmpBulanIni = CDate(.Item("JthTmp"))
                    X.nSukuBunga = CSng(IIf(CSng(.Item("SukuBunga")) = 0, X.nSukuBunga, CSng(.Item("SukuBunga"))))
                    X.nPerpanjanganKeSekarang = CInt(.Item("PerpanjanganKe"))
                    ' mencari tanggal dan jatuh tempo per periode perpanjangan
                    cField = "min(tgl) as TglValuta,max(jthtmp) as Jthtmp"
                    cWhere = String.Format(" and faktur = '{0}'", .Item("faktur"))
                    db1 = objData.Browse(GetDSN, "jadwalbungadeposito", cField, "rekening", , cRekening, cWhere)
                    If db1.Rows.Count > 0 Then
                        If .Item("faktur").ToString <> "" Then
                            Dim a As Double = Val(DateDiff("m", X.dTglPeriode, X.dJthTmpPeriode))
                            Dim b As Double = Val(DateDiff("m", X.dTglPeriode, X.dJthTmpPeriode))
                            X.nLama = Val(IIf(a = 0, X.nLama, b))
                        End If
                    End If
                End With
            Else
                dTglTemp = DateAdd("m", -1, dTgl)
                cWhere = ""
                cWhere = String.Format(" and status =0 and date_format(tgl,'%Y%m') = date_format('{0}','%Y%m') ", formatValue(dTglTemp, formatType.yyyy_MM_dd))
                db = objData.Browse(GetDSN, "jadwalbungadeposito", , "rekening", , cRekening, cWhere)
                If db.Rows.Count > 0 Then
                    Dim a As Single = CSng(db.Rows(0).Item("SukuBunga"))
                    Dim b As Single = CSng(db.Rows(0).Item("SukuBunga"))
                    X.nSukuBunga = CSng(IIf(a = 0, X.nSukuBunga, b))
                End If
                ' jika belum diperpanjang maka ditampilkan tanggal jatuh tempo paling akhir
                cWhere = ""
                cWhere = String.Format(" and status =0 and date_format(tgl,'%Y%m') <= date_format('{0}','%Y%m') ", formatValue(dTglTemp, formatType.yyyy_MM_dd))
                cWhere = cWhere & " order by tgl desc limit 1"
                db = objData.Browse(GetDSN, "jadwalbungadeposito", , "rekening", , cRekening, cWhere)
                If db.Rows.Count > 0 Then
                    X.dJthTmpBulanIni = CDate(db.Rows(0).Item("JthTmp"))
                End If
            End If

            Dim dTglPerubahan As Date = DateAdd("m", -1, dTgl)
            dTglPerubahan = EOM(dTglPerubahan)
            cWhere = String.Format("and tgl <= '{0}'", formatValue(dTglPerubahan, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "detailsukubungadeposito", "sukubunga", "rekening", , cRekening, cWhere, "tgl desc limit 1")
            If db.Rows.Count > 0 Then
                X.nSukuBunga = CSng(GetNull(CSng(db.Rows(0).Item("SukuBunga")), X.nSukuBunga))
            End If

            X.nSukuBungaAccrual = CSng(X.nSukuBunga)

            ' mencari jumlah hari
            nHari = DateDiff("d", X.dTglPeriode, X.dJthTmpPeriode)
            X.nTotalHariKeseluruhan = nHari

            nHari = DateDiff("d", X.dTgl, X.dJthTmp)
            X.nJumlahHari = Val(IIf(nHari <= 0, 0, nHari))

            '    nTotalBunga = Round(X.nNominal * X.nSukuBunga / 100 / 365 * nHari)
            nBungaBulanan = Math.Round(X.nNominal * X.nSukuBunga / 1200)
            nTotalBunga = Math.Round(X.nNominal * X.nSukuBunga / 100 / 12 * X.nLama)

            ' Hitung Bunga Deposito
            X.nTotalBunga = nTotalBunga
            If X.nTotalHariKeseluruhan = 0 Then X.nTotalHariKeseluruhan = 1
            X.nBungaHarian = Math.Round(X.nTotalBunga / X.nTotalHariKeseluruhan, 2)
            X.nBunga = Math.Round(X.nNominal * X.nSukuBunga / 100 / 365 * X.nJumlahHari)
            X.nBunga = Math.Round(nBungaBulanan)

            ' hitung pajak
            lCountPajak = False
            If X.nTotalNominalDeposito >= Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Or X.nNominalSetoranDeposito >= Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Or X.lStatusPajakPerpanjangan Then
                lCountPajak = True
                X.nPajak = Math.Round(X.nTotalBunga * X.nTarifPajak / 100)
                X.nPajakBulanan = Math.Round(X.nBunga * X.nTarifPajak / 100)
                If Not X.lHitungPajak Or X.nBunga = 0 Then
                    X.nPajak = 0
                    X.nPajakBulanan = 0
                End If
            End If
            X.lHitungPajakPerpanjangan = lCountPajak
            ' jika belum ada perpanjangan sama sekali (baru setoran plafond saja) dan
            ' ternyata pajaknya dihitung (karena field StatusPajak defaultnya adalah "Y") dan
            ' nominal setoran plafond < saldo minimum kena pajak atau
            ' total saldo nasabah < saldo minimum kena pajak
            ' maka nominal pajak yang sudah dihitung dinolkan
            If X.nNominalSetoranDeposito < Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Then
                X.lStatusPajakPerpanjangan = False
                X.nPajak = 0
                X.nPajakBulanan = 0
            End If
            If lSetorPertama And lCountPajak And X.nTotalNominalDeposito < Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Then
                X.lStatusPajakPerpanjangan = False
                X.nPajak = 0
                X.nPajakBulanan = 0
            End If

            ' hitung accrual bunga deposito
            Dim nBungaAccrualTemp As Double
            Dim nSukuBungaAccrualTemp As Single

            nSukuBungaLama = CSng(X.nSukuBunga)
            X.nSukuBungaAccrual = CSng(X.nSukuBunga)
            db = objData.Browse(GetDSN, "deposito", "sukuBunga", "rekening", , cRekening)
            If db.Rows.Count > 0 Then
                nSukuBungaTemp = CSng(GetNull(db.Rows(0).Item("SukuBunga")))
                nSukuBungaAccrualTemp = CSng(GetNull(db.Rows(0).Item("SukuBunga")))
            End If
            cField = ""
            cField = "rekening,min(tgl) as tgl,max(jthtmp) as jthtmp,sukubunga"
            cWhere = ""
            cWhere = String.Format(" and date_format('{0}','%Y%m') ", formatValue(dTgl, formatType.yyyy_MM_dd))
            cWhere = cWhere & " between date_format(tgl,'%Y%m') and date_format(jthtmp,'%Y%m') "
            cWhere = cWhere & " and status=0 "
            db = objData.Browse(GetDSN, "jadwalbungadeposito", cField, "rekening", , cRekening, cWhere, "tgl", , "sukubunga")
            If db.Rows.Count > 0 Then
                Dim a As Integer
                Dim va(1, 3) As Object
                Dim i As Integer = db.Rows.Count
                n = 0
                For a = 0 To i - 1
                    With db.Rows(a)
                        va(a, 0) = .Item("Rekening")
                        va(a, 1) = .Item("SukuBunga")
                        va(a, 2) = .Item("Tgl")
                        va(a, 3) = .Item("JthTmp")
                    End With
                Next
                nSukuBungaAccrualTemp = CSng(va(0, 1))
            End If

            dTglAkhirBulan = EOM(X.dTgl)

            ' untuk accrual, pengurang dan pembagi pakai 30, bukan berdasarkan tanggal kalender
            Dim nHari2 As Double
            Dim nHari1 As Double = DateDiff("d", X.dTglPeriode, X.dTglBulanBerikutnya)  '30
            nHari2 = DateDiff("d", X.dTgl, dTglAkhirBulan)
            If nHari2 < 0 Then
                nHari2 = 0
            End If
            nBungaAccrualTemp = X.nNominal * nSukuBungaAccrualTemp / 1200

            X.nSukuBungaAccrual = nSukuBungaAccrualTemp
            If nBungaAccrualTemp = 0 Then
                X.nBungaAccrual = X.nBunga
            Else
                X.nBungaAccrual = Math.Round(nBungaAccrualTemp)
            End If
            X.nJumlahHari = nHari1  ' 30
            X.nHariAccrual2 = nHari1 - nHari2
            X.nJumlahHari = nHari1
            nHari2 = Max(0, EOM(dTgl).Day - X.dTgl.Day)
            ' rangga #02-04-2014 ' untuk handle deposito yang pembukaannya melebihi tanggal akhir bulan berjalan.
            ' contoh : tgl pembukaan 31-12-2013, tanggal posting 31-04-2014, rekening 01.42.000708, 01.44.000684, 01.44.000710, 01.51.000039
            If X.dTglPembukaan.Day > X.dTgl.Day Then
                nHari2 = 0
            End If

            X.nBungaAccrual1 = Math.Round(X.nBunga / 30 * nHari2)
            X.nBungaAccrual2 = X.nBunga - X.nBungaAccrual1
            X.nHariAccrual1 = nHari2

            If lCountPajak Then
                X.nPajakAccrual1 = Math.Round(X.nBungaAccrual1 * X.nTarifPajak / 100)
                X.nPajakAccrual2 = X.nPajakBulanan - X.nPajakAccrual1
            End If
            If Not X.lHitungPajak And Not X.lStatusPajakPerpanjangan Then
                X.nPajakAccrual1 = 0
                X.nPajakAccrual2 = 0
            End If

            vaJoin = {"left join registernasabah r on t.kode=r.kode"}
            db = objData.Browse(GetDSN, "Tabungan t", "r.Nama,r.Alamat", "t.rekening", , X.cRekeningTabungan, , , vaJoin)
            If db.Rows.Count > 0 Then
                X.cNamaNasabahTabungan = GetNull(db.Rows(0).Item("Nama"), "").ToString
                X.cAlamatNasabahTabungan = GetNull(db.Rows(0).Item("Alamat"), "").ToString
            End If

            ' menentukan status apakah deposito sudah ditutup atau belum
            cWhere = String.Format("and tgl <= '{0}'", formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "MutasiDeposito", "sum(PencairanPlafond) as Plafond", "Rekening", , cRekening, cWhere)
            If db.Rows.Count > 0 Then
                If Val(GetNull(db.Rows(0).Item("Plafond"))) > 0 Then
                    X.lTutup = True
                End If
            End If
        End If
        db.Dispose()
        db1.Dispose()
    End Sub

    Sub DelMutasiDeposito(ByVal cFaktur As String, ByVal nStatus As eMutasiDeposito)
        Dim db As New DataTable
        Dim vaValue() As Object
        Dim vaField() As Object
        Dim cWhere As String
        Dim cField As String

        If Trim(cFaktur) <> "" Then
            If nStatus = eMutasiDeposito.Setoran_Deposito Then
                ' Edit tabel deposito untuk nominal jika mutasi yang dihapus adalah setoran deposito
                cField = "SetoranPlafond,Rekening"
                db = objData.Browse(GetDSN, "mutasideposito", cField, "faktur", , cFaktur)
                If db.Rows.Count > 0 Then
                    If Val(db.Rows(0).Item("SetoranPlafond")) > 0 Then
                        vaField = {"Nominal"}
                        vaValue = {0}
                        cWhere = String.Format("rekening='{0}'", db.Rows(0).Item("Rekening"))
                        objData.Edit(GetDSN, "deposito", cWhere, vaField, vaValue)
                    End If
                End If
            End If

            ' Hapus Pada table Mutasi Deposito
            objData.Delete(GetDSN, "MutasiDeposito", "Faktur", , cFaktur)

            ' Hapus Pada Table Mutasi Tabungan
            DelMutasiTabungan(cFaktur)

            ' Hapus Pada Table BukuBesar
            DelBukuBesar(cFaktur)

            ' Hapus pada table jadwalbungadeposito
            objData.Delete(GetDSN, "jadwalbungadeposito", "Faktur", , cFaktur)
        End If
        db.Dispose()
    End Sub

    Sub DelPencairanDeposito(ByVal cRekening As String, ByVal dTanggal As Date)
        Dim db As New DataTable
        Dim dbData As New DataTable
        Dim cFaktur As String
        Dim cWhere As String

        dbData = objData.Browse(GetDSN, "Deposito", , "Rekening", , cRekening)
        If dbData.rows.count > 0 Then
            cWhere = String.Format(" and tgl = '{0}' and pencairanplafond > 0", formatValue(dTanggal, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "MutasiDeposito", , "Rekening", , cRekening, cWhere)
            For n As Integer = 0 To db.Rows.Count - 1
                cFaktur = db.Rows(n).Item("faktur").ToString

                ' Hapus mutasi
                objData.Delete(GetDSN, "MutasiDeposito", "Faktur", , cFaktur)

                ' Hapus Buku Besar
                objData.Delete(GetDSN, "Bukubesar", "Faktur", , cFaktur)

                ' Hapus Mutasi Tabungan
                DelMutasiTabungan(cFaktur)
            Next

            objData.Edit(GetDSN, "Deposito", String.Format("Rekening='{0}'", cRekening), {"Status", "TglCair"}, {"0", "9999-12-31"})
        End If
        db.Dispose()
        dbData.Dispose()
    End Sub
End Module
