﻿Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns

Module systemProgram
    Function SNow() As String
        SNow = Format(Now, "yyyy-MM-dd hh:mm:ss")
    End Function

    Sub FormatTextBox(ByVal objTextBox As DevExpress.XtraEditors.TextEdit, ByVal objFormatType As formatType, ByVal cAlignment As HorzAlignment, _
                      Optional ByVal nMaskType As DevExpress.XtraEditors.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None, _
                      Optional ByVal cFormatString As String = "", Optional ByVal cPlaceHolder As String = "_")
        With objTextBox
            .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
            Select Case objFormatType
                Case formatType.BilRpPict
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                    .Properties.Mask.EditMask = "n0"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.BilRpPict2
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                    .Properties.Mask.EditMask = "n2"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.BilRpPict_Default
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.dd_MM_yyyy
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd-MM-yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.dd_MMMM_yyyy
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd-MMMM-yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.yyyy_MM_dd
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd-MMMM-yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.dd_MM_yy_Dot
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd.MM.yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.dd_MM_yy_Minus
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd-MM-yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.dd_MM_yy_Slash
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "dd/MM/yyyy"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.hh_mm_ss
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                    .Properties.Mask.EditMask = "hh:mm:ss"
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
                Case formatType.string_char
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
                Case formatType.custom
                    '.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom
                    .Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
                    .Properties.Mask.EditMask = cFormatString
                    .Properties.Appearance.TextOptions.HAlignment = cAlignment
            End Select
        End With
        objTextBox.Properties.Mask.SaveLiteral = False
        objTextBox.Properties.Mask.ShowPlaceHolders = False
        objTextBox.Properties.Mask.UseMaskAsDisplayFormat = True
        objTextBox.Properties.Mask.PlaceHolder = CChar(cPlaceHolder)
    End Sub

    Function formatValue(ByVal Value As Object, ByVal bFormat As formatType, Optional ByVal cNegatifSeparator As String = "") As String
        Dim vaFormat() As String = {"yyyy-MM-dd", "dd-MM-yyyy", "###,###,###,###,###,##0.00", _
                                    "###,###,###,###,###,##0", "dd-MMMM-yyyy", "dd-MM-YY", "dd/MM/YY", _
                                    "dd.MM.yy", "#####", "", "hh:mm:ss"}
        If Len(cNegatifSeparator) > 0 Then
            If Val(Value) >= 0 Then
                cNegatifSeparator = Space(Len(cNegatifSeparator))
            End If

            formatValue = Left(cNegatifSeparator, 1) & Format(Math.Abs(Val(Value)), vaFormat(bFormat)) & Right(cNegatifSeparator, 1)
        Else
            Dim cFormatType As String = vaFormat(bFormat)
            formatValue = Format(Value, cFormatType)
        End If
    End Function

    Function FormatValueDX(ByVal nJenis As formatType) As String
        Dim vaFormat() As String = {"yyyy-MM-dd", "dd-MM-yyyy", "n2", _
                                   "n0", "dd-MMMM-yyyy", "dd-MM-yy", "dd/MM/yy", _
                                   "dd.MM.yy", "#####", "", "hh:mm:ss"}
        FormatValueDX = vaFormat(nJenis)
    End Function

    Function Padl(Optional ByVal cCharacter As String = "", Optional ByVal nLen As Integer = 0, Optional ByVal cTipeChar As Object = " ") As String
        Dim n As Integer
        Dim x As String = ""
        If Len(cCharacter) < nLen Then
            For n = 1 To nLen - Len(cCharacter)
                x = CStr(cTipeChar) & x
            Next
            Padl = x & cCharacter
        Else
            Padl = Mid(cCharacter, 1, nLen)
        End If
    End Function

    Function Padr(Optional ByVal cCharacter As String = "", Optional ByVal nLen As Integer = 0, Optional ByVal cTipeChar As Object = " ") As String
        Dim n As Integer
        Dim x As String = ""

        cCharacter = Left(cCharacter, nLen)
        If Len(cCharacter) < nLen Then
            For n = 1 To nLen - Len(cCharacter)
                x = CStr(cTipeChar) & x
            Next
            Padr = cCharacter + x
        Else
            Padr = Mid(cCharacter, 1, nLen)
        End If
    End Function

    Function Pad(ByVal cCharacter As String, Optional ByVal nLen As Integer = 0, Optional ByVal cTipeChar As Object = " ") As String
        Pad = Padr(cCharacter, nLen, cTipeChar)
    End Function

    Function setKode(ByVal cCabangTemp As String, ByVal cKodeTemp As String) As String
        Return String.Format("{0}.{1}", cCabangTemp, cKodeTemp)
    End Function

    Function setRekening(ByVal cCabangTemp As String, ByVal cGolonganTemp As String, ByVal cUrutTemp As String) As String
        Return String.Format("{0}.{1}.{2}", cCabangTemp, cGolonganTemp, cUrutTemp)
    End Function

    Function GetDSN() As String
        GetDSN = String.Format("Database={0}; Convert Zero Datetime=True; Allow Zero Datetime=True; Data Source={1};User Id=Heasoft;Password=HeaI;Port={2}", cDatabase, cIPNumber, cPort)
    End Function

    Function Replicate(cString As String, nCount As Integer) As String
        Dim n As Integer
        Dim cRetVal As String = ""
        For n = 1 To nCount
            cRetVal = cRetVal & cString
        Next
        Return cRetVal
    End Function

    Function GetMonth(nMonth As Integer, Optional ByVal lSortMonth As Boolean = False) As String
        Dim vaMonth() As String
        If lSortMonth Then
            vaMonth = {"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Juli", "Agt", "Sept", "Okt", "Nov", "Des"}
        Else
            vaMonth = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"}
        End If
        GetMonth = vaMonth(nMonth - 1)
    End Function
End Module
