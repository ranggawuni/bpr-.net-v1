﻿Imports bpr.MySQL_Data_Library

Module version
    Private ReadOnly objData As New data()

    Sub CheckVersion()
        Dim cSQL As String = ""

    End Sub

    Private Function CekTable(ByVal cTableName As String) As Boolean
        CekTable = False
        Dim cSQL As String = "show tables from " & cDatabase
        Dim db As DataTable = objData.SQL(GetDSN, cSQL)
        Dim n As Integer = 0
        Do Until n >= db.Rows.Count
            If cTableName = db.Rows(0).Item(0).ToString Then
                CekTable = True
                Exit Do
            End If
            n = n + 1
        Loop
    End Function

    Private Sub DropField(ByVal cTableName As String, ByVal cFieldName As String)
        Dim lModif As Boolean = False
        Dim dbdata As DataTable = objData.SQL(GetDSN, "Show Fields From " & cTableName)
        Dim n As Integer = 0
        Do Until n > dbdata.Rows.Count
            If LCase(dbdata.Rows(0).Item(0).ToString) = LCase(cFieldName) Then
                lModif = True
            End If
            n = n + 1
        Loop
        If lModif Then
            objData.SQL(GetDSN, String.Format("ALTER TABLE {0} DROP {1}", cTableName, cFieldName))
        End If
    End Sub

    Private Sub AddField(ByVal cTableName As String, ByVal cFieldName As String, _
                         ByVal cFieldType As String, ByVal cDefault As String, _
                         Optional ByVal cFieldAfter As String = "", _
                         Optional ByVal lMySQL41 As Boolean = False, _
                         Optional ByVal cComment As String = "")
        Dim lModif As Boolean = True
        Dim cSQL As String

        Dim dbdata As DataTable = objData.SQL(GetDSN, String.Format("Show Fields From {0} where field = '{1}'", cTableName, cFieldName))
        Dim n As Integer = 0
        Do Until n >= dbdata.Rows.Count
            If LCase(dbdata.Rows(0).Item(0).ToString) = LCase(cFieldName) Then
                lModif = False
            End If
            n = n + 1
        Loop
        If lModif Then
            If UCase(cFieldAfter) = UCase("First") Then
                If cFieldAfter <> "" Then
                    cFieldAfter = UCase(cFieldAfter)
                End If
            Else
                If cFieldAfter <> "" Then
                    cFieldAfter = " AFTER " & cFieldAfter
                End If
            End If
            If Not lMySQL41 Then ' mysql 4.0
                cSQL = String.Format("ALTER TABLE {0} ADD {1} {2} DEFAULT {3} COMMENT '{4}' {5}", cTableName, cFieldName, cFieldType, cDefault, cComment, cFieldAfter)
                objdata.SQL(GetDSN, cSQL)
            Else
                cSQL = String.Format("ALTER TABLE {0} ADD COLUMN {1} {2} CHARACTER SET 'latin1' NOT NULL DEFAULT '{3}' COMMENT '{4}' {5}", cTableName, cFieldName, cFieldType, cDefault, cComment, cFieldAfter)
                objdata.SQL(GetDSN, cSQL)
            End If
        End If
    End Sub

    Private Sub EditField(ByVal cTableName As String, ByVal cOldFieldName As String, ByVal cNewFieldName As String, _
                          ByVal cFieldType As String, ByVal cDefault As String, Optional ByVal cFieldAfter As String = "", _
                          Optional ByVal lMySQL41 As Boolean = False, Optional ByVal cComment As String = "")
        Dim lModif As Boolean = True
        Dim cSQL As String
        Dim dbdata As DataTable = objData.SQL(GetDSN, String.Format("Show Fields From {0} where field = '{1}'", cTableName, cNewFieldName))
        Dim n As Integer = 0
        Do Until n >= dbdata.Rows.Count
            If LCase(dbdata.Rows(0).Item(0).ToString) = LCase(cNewFieldName) Then
                If dbdata.Rows(0).Item("Type").ToString = LCase(cFieldType) Then
                    lModif = False
                Else
                    lModif = True
                End If
            End If
            n = n + 1
        Loop
        If lModif Then
            If UCase(cFieldAfter) = UCase("First") Then
                If cFieldAfter <> "" Then
                    cFieldAfter = UCase(cFieldAfter)
                End If
            Else
                If cFieldAfter <> "" Then
                    cFieldAfter = " AFTER " & cFieldAfter
                End If
            End If
            cSQL = String.Format("ALTER TABLE {0} CHANGE COLUMN {1} {2} {3} CHARACTER SET 'latin1' NOT NULL DEFAULT '{4}' COMMENT '{5}' {6}", cTableName, cOldFieldName, cNewFieldName, cFieldType, cDefault, cComment, cFieldAfter)
            objdata.SQL(GetDSN, cSQL)
        End If
    End Sub

    Private Sub AddTable(ByVal cTableName As String, ByVal cSQL As String)
        Dim lAddTable As Boolean = True
        Dim dbdata As DataTable = objData.SQL(GetDSN, "Show Tables")
        Dim n As Integer = 0
        Do Until n >= dbdata.Rows.Count
            If LCase(dbdata.Rows(0).Item(0).ToString) = LCase(cTableName) Then
                lAddTable = False
            End If
            n = n + 1
        Loop

        If lAddTable Then
            objdata.SQL(GetDSN, cSQL)
        End If
    End Sub



End Module
