﻿Imports bpr.rpt1
Imports DevExpress.XtraReports.UI
Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Module Cetak
    ReadOnly objData As New data()

    Public Sub CetakValidasiTabunganDetail(ByVal cFaktur As String, Optional lBaru As Boolean = False)
        Dim n As Integer = 0
        Dim nHeight As Integer = 0
        Dim nWidth As Integer = 0
        Dim cKet As String = ""
        Dim cKet1 As String = ""
        Dim cKet2 As String = ""

        Dim cTemp As String = ""
        Dim cNama As String = ""
        Dim dTgl As Date
        Dim cRekening As String = ""
        Dim nDebet As Double = 0
        Dim nKredit As Double = 0
        Dim db As New DataTable

        Dim cKodeTransaksi As String = ""
        Dim cNamaKodeTransaksi As String = ""
        Dim cDK As String = ""

        Dim cField As String = "b.rekening as RekeningJurnal,b.debet,b.kredit,r.Nama,b.faktur,a.rekening,a.tgl,"
        cField = cField & "a.kodetransaksi,k.keterangan as NamaKodeTransaksi,k.DK"
        Const cWhere As String = ""
        Dim vaJoin() As Object = {"Left Join mutasitabungan a on a.faktur = b.faktur", _
                                  "Left Join tabungan d on d.Rekening = a.Rekening", _
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode", _
                                  "Left Join KodeTransaksi k on k.kode = a.kodetransaksi"}
        db = objData.Browse(GetDSN, "BukuBesar b", cField, "b.Faktur", , cFaktur, cWhere, , vaJoin)
        If db.Rows.Count > 0 Then
            nDebet = 0
            nKredit = 0
            For n = 0 To db.Rows.Count - 1
                With db.Rows(n)
                    cRekening = .Item("Rekening").ToString
                    cNama = .Item("Nama").ToString
                    dTgl = CDate(.Item("Tgl").ToString)
                    nKredit = Val(GetNull(.Item("Kredit").ToString).ToString)
                    nDebet = Val(GetNull(.Item("debet").ToString).ToString)
                    cKodeTransaksi = GetNull(.Item("KodeTransaksi").ToString).ToString
                    cNamaKodeTransaksi = GetNull(.Item("NamaKodeTransaksi").ToString).ToString
                    cDK = GetNull(.Item("DK").ToString, "").ToString
                    If nDebet <> 0 Then
                        cTemp = cTemp & .Item("RekeningJurnal").ToString & Space(1) & Padl(formatValue(nDebet, formatType.BilRpPict), 20).ToString & vbCrLf
                    Else
                        cTemp = cTemp & Space(5) & .Item("RekeningJurnal").ToString & Space(1) & Padl(formatValue(nKredit, formatType.BilRpPict), 20).ToString & vbCrLf
                    End If
                End With
            Next
            If nWidth = 0 Then nWidth = 270
            If nHeight = 0 Then nHeight = 110
            If lBaru = True Then
                If cDK = "K" Then
                    cKet = String.Format("{0}   {1}   {2}   {3}", cFaktur, formatValue(dTgl, formatType.dd_MM_yyyy), cRekening, ReplaceSpecialCharacter(cNama))
                    cKet1 = String.Format("{0}   {1}   {2}", cUserName, cKodeTransaksi, cNamaKodeTransaksi)
                Else
                    cKet = String.Format("{0}   {1}   {2}", cFaktur, formatValue(dTgl, formatType.dd_MM_yyyy), cRekening)
                    cKet1 = String.Format("{0}   {1}", ReplaceSpecialCharacter(cNama), cUserName)
                    cKet2 = String.Format("{0} {1}", cKodeTransaksi, cNamaKodeTransaksi)
                End If
            Else
                cKet = String.Format("{0}   {1}   {2}   {3}", cFaktur, formatValue(dTgl, formatType.dd_MM_yyyy), cRekening, ReplaceSpecialCharacter(cNama))
                cKet1 = String.Format("{0}   {1}   {2}", cUserName, cKodeTransaksi, cNamaKodeTransaksi)
            End If
            InitLabelPreview(, Printing.PaperKind.Custom, nWidth, nHeight)
            If lBaru Then
                If cDK = "K" Then
                    AddLabel("cKeterangan1", cKet, 350, , 62, 2, , , , , True)
                    AddLabel("cKeterangan2", cKet1, 310, , 62, 12, , , , , True)
                    AddLabel("cTemp", cTemp, 310, , 113, 22, , , , , True)
                Else
                    AddLabel("cKeterangan1", cKet, 150, , 20, 10, , , , , True)
                    AddLabel("cKeterangan2", cKet1, 110, , 20, 14, , , , , True)
                    AddLabel("cKeterangan3", cKet2, 110, , 20, 18, , , , , True)
                    AddLabel("cTemp", cTemp, 110, , 20, 22, , , , , True)
                End If
            Else
                AddLabel("cKeterangan1", cKet, 150, , 15, 75, , , , , True)
                AddLabel("cKeterangan2", cKet1, 110, , 15, 79, , , , , True)
                AddLabel("cTemp", cTemp, 110, , 15, 83, , , , , True)
            End If
            RptPreview.Show()
        End If
        db.Dispose()
    End Sub

    Public Sub CetakValidasiDepositoDetail(ByVal cFaktur As String)
        Dim db As New DataTable
        Dim nTop As Integer
        Dim nLeft As Integer
        Dim nWidth As Integer
        Dim nHeight As Integer
        Dim cKeterangan As String
        Dim nDebet As Double
        Dim nKredit As Double
        Dim cRekening As String = ""
        Dim cNama As String = ""
        Dim dTgl As Date
        Dim cTemp As String = ""

        Const cField As String = "b.rekening as RekeningJurnal,b.debet,b.kredit,r.Nama,b.faktur,a.rekening,a.tgl"
        Const cWhere As String = ""
        Dim vaJoin() As Object = {"Left Join mutasideposito a on a.faktur = b.faktur",
                                  "Left Join deposito d on d.Rekening = a.Rekening",
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode"}
        db = objData.Browse(GetDSN, "BukuBesar b", cField, "b.Faktur", , cFaktur, cWhere, , vaJoin)
        If db.Rows.Count > 0 Then
            nDebet = 0
            nKredit = 0
            For n = 0 To db.Rows.Count - 1
                With db.Rows(n)
                    cRekening = .Item("Rekening").ToString
                    cNama = .Item("Nama").ToString
                    dTgl = CDate(.Item("Tgl"))
                    nKredit = Val(GetNull(.Item("Kredit")))
                    nDebet = Val(GetNull(.Item("debet")))
                    If nDebet <> 0 Then
                        cTemp = cTemp & .Item("RekeningJurnal").ToString & Space(1) & Padl(formatValue(nDebet, formatType.yyyy_MM_dd), 20) & vbCrLf
                    Else
                        cTemp = cTemp & Space(5) & .Item("RekeningJurnal").ToString & Space(1) & Padl(formatValue(nKredit, formatType.yyyy_MM_dd), 20) & vbCrLf
                    End If
                End With
            Next

            nTop = 0
            nLeft = 0
            nHeight = 0
            nWidth = 0
            If nWidth = 0 Then nWidth = 273
            If nHeight = 0 Then nHeight = 110
            InitLabelPreview(, Printing.PaperKind.Custom, nWidth, nHeight)
            Dim cUserTemp As String = String.Format("{0} {1}", cUserName, SNow())
            cKeterangan = String.Format("{0}  {1}  {2}  {3}", cFaktur, formatValue(dTgl, formatType.yyyy_MM_dd), cRekening, ReplaceSpecialCharacter(cNama))
            AddLabel("cUser", cUserTemp, 350, , 62, 2, , , , , True)
            AddLabel("cKeterangan", cKeterangan, 310, , 62, 12, , , , , True)
            AddLabel("cTemp", cTemp, 310, , 113, 22, , , , , True)
            RptPreview.Show()
        End If
        db.Dispose()
    End Sub
End Module
