﻿Module FuncMenu
    Function Zip(ByVal cPassword As String, Optional ByVal lTile As Boolean = True) As String
        Dim n As Integer
        Dim cRetval As String = ""
        Dim cZip As String = ""
        Dim cPad As String = ""

        If lTile Then
            cPassword = Trim(cPassword)
            If cPassword = "" Then
                cPassword = " "
            End If

            Do While Len(cPad) <= 20
                cPad = cPad & cPassword
            Loop
            cPassword = Left(cPad, 20)
        End If

        For n = 1 To Len(cPassword)
            cRetval = cRetval & Trim(Str(Asc(Mid(cPassword, n, 1)) + (n * 59) + 1978))
        Next

        For n = 1 To Len(cRetval) Step 2
            cZip = cZip & Chr(CInt(Val(Mid(cRetval, n, 2)) + 65))
        Next

        Return cZip
    End Function

    Function UnZip(ByVal cPassword As String) As String
        Dim n As Integer
        Dim cRetval As String = ""
        Dim cUnZip As String = ""
        Dim i As Integer
        Dim c As String

        For n = 1 To Len(cPassword)
            c = Trim(Str(Asc(Mid(cPassword, n, 1)) - 65))
            cUnZip = cUnZip & CStr(IIf(Len(c) = 1, "0", "")) & c
        Next

        For n = 1 To Len(cUnZip) Step 4
            i = i + 1
            cRetval = cRetval & Chr(CInt(Val(Mid(cUnZip, n, 4)) - (i * 59) - 1978))
        Next

        Return cRetval
    End Function

    Function ValidMenu(ByVal dbData As DataTable) As Boolean
        ValidMenu = True
        If dbData.Rows.Count > 0 Then
            ValidMenu = True
        Else
            ValidMenu = dbData.Rows(0).Item("Status").ToString = "T"
        End If
    End Function

    Function GetUserName(Optional par As Byte = 0, Optional cValue As String = "", Optional lPar As Boolean = False) As String
        Static cUser As String
        Static cPassword As String
        Static cFullname As String
        Static nLevel As Single
        Static cID As String
        Static cIP As String
        GetUserName = ""

        Try
            If par = 1 Then           ' User
                GetUserName = cUser
                cUser = IIf(lPar, cValue, cUser).ToString
            ElseIf par = 2 Then        ' Password
                GetUserName = cPassword
                cPassword = IIf(lPar, cValue, cPassword).ToString
            ElseIf par = 3 Then        ' FullName
                GetUserName = cFullname
                cFullname = IIf(lPar, cValue, cFullname).ToString
            ElseIf par = 4 Then        ' Password
                GetUserName = nLevel.ToString
                nLevel = CSng(IIf(lPar, cValue, nLevel))
            ElseIf par = 5 Then
                GetUserName = cID
                cID = IIf(lPar, cValue, cID).ToString
            ElseIf par = 6 Then
                GetUserName = cIP
                cIP = IIf(lPar, cValue, cIP).ToString
            End If
        Catch ex As Exception
            Debug.Write(ex.Message)
        End Try
    End Function
End Module
