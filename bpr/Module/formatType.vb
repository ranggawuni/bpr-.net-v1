Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns


Public Enum formatType
    yyyy_MM_dd = 0
    dd_MM_yyyy = 1
    BilRpPict2 = 2
    BilRpPict = 3
    dd_MMMM_yyyy = 4
    dd_MM_yy_Minus = 5
    dd_MM_yy_Slash = 6
    dd_MM_yy_Dot = 7
    BilRpPict_Default = 8
    string_char = 9
    hh_mm_ss = 10
    custom = 11
End Enum
