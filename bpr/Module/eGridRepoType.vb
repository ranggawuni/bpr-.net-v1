Imports DevExpress.XtraGrid
Imports DevExpress.XtraEditors.Repository


Public Enum eGridRepoType
    eNone = 0
    eCheckBox = 1
    eComboBox = 2
    eLookUpEdit = 3
End Enum
