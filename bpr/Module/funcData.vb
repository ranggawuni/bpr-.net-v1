﻿Imports bpr.MySQL_Data_Library

Module funcData
    Private ReadOnly objData As New data()

    Private Sub CheckTableName(ByVal cConnectionString As String)
        Static lCheck As Boolean
        Dim lTableFound As Boolean
        Dim cSQL As String
        Dim n As Integer = 0

        If Not lCheck Then
            lTableFound = False
            Dim cDataTable As DataTable = objData.Browse(GetDSN, cConnectionString, "show tables from mysql")
            Do Until n >= cDataTable.Rows.Count
                If Trim(LCase(cDataTable.Rows(0).Item(n).ToString)) = "db_history" Then
                    lTableFound = True
                End If
                n = n + 1
            Loop

            If Not lTableFound Then
                cSQL = "CREATE TABLE `mysql`.`db_history` ("
                cSQL = cSQL & "`ID` VARCHAR(100) NULL DEFAULT '',"
                cSQL = cSQL & "`IDCabang` BIGINT(20) NOT NULL AUTO_INCREMENT,"
                cSQL = cSQL & "`Cabang` CHAR(2) NOT NULL DEFAULT '',"
                cSQL = cSQL & "`Tgl` DATE NULL DEFAULT '0000-00-00',"
                cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
                cSQL = cSQL & "`TableName` VARCHAR(100) NULL DEFAULT '',"
                cSQL = cSQL & "`Activity` CHAR(1) NULL DEFAULT '',"
                cSQL = cSQL & "`History` TEXT NULL,"
                cSQL = cSQL & "`UserName` VARCHAR(100) NULL DEFAULT '',"
                cSQL = cSQL & "`DateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00',"
                cSQL = cSQL & "PRIMARY KEY (`Cabang`, `IDCabang`),"
                cSQL = cSQL & "INDEX `UserTgl` (`UserName`, `Tgl`, `DataBaseName`, `TableName`, `Activity`),"
                cSQL = cSQL & "INDEX `Database` (`DataBaseName`, `TableName`, `Tgl`, `ID`),"
                cSQL = cSQL & "INDEX `DatabaseActivity` (`DataBaseName`, `TableName`, `Activity`, `Tgl`, `ID`))"
                cSQL = cSQL & "COLLATE='utf8_general_ci'"
                cSQL = cSQL & "ENGINE = MyISAM AUTO_INCREMENT=0;"
                objData.SQL(cConnectionString, cSQL)
            End If
            lTableFound = False
            cDataTable = objData.SQL(GetDSN, "show tables from mysql", False)
            Do Until n >= cDataTable.Rows.Count
                If Trim(LCase(cDataTable.Rows(0).Item(n).ToString)) = "db_lastid" Then
                    lTableFound = True
                End If
                n = n + 1
            Loop

            If Not lTableFound Then
                cSQL = "CREATE TABLE `mysql`.`db_lastid` ("
                cSQL = cSQL & "`Cabang` CHAR(2) NULL DEFAULT '',"
                cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
                cSQL = cSQL & "`ID` BIGINT(20) NOT NULL DEFAULT '0')"
                cSQL = cSQL & "COLLATE='utf8_general_ci'"
                cSQL = cSQL & "ENGINE=MyISAM;"
                cDataTable = objData.SQL(GetDSN, cSQL, False)
            End If
            lCheck = True
        End If
    End Sub

End Module
