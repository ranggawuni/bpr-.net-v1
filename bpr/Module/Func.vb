﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports System.Reflection
Imports System.IO

Module Func
    Public nUserLevel As Integer = 0
    Public cUserID As String = ""
    Public cUserName As String = ""
    Public cFullName As String = ""
    Public cKasTeller As String = ""
    ReadOnly objData As New data()
    Public cComputerName As String = ""
    Public cIPNumber As String = ""
    Public cDatabase As String = ""
    Public cPort As String = ""
    Public cKodeKantorInduk As String = ""
    Public StatusACC As AccFiat
    Public cUserLogin As String = ""
    Public cNamaKantorPublic As String = ""
    Public cAlamatKantorPublic As String = ""
    Public cJenisKantor As String = ""
    Public cAppVersion As String = ""
    Public cIPNumberLocal As String = ""
    Public cPrintDialog As String = ""
    Public cListFilterKantor As String = ""
    Public dTglFilterKantor As Date = Date.Today
    Public cQueryFilter As String = ""
    Public cKodeAplikasi As String = "BPR"
    Public nBulanExpired As Integer = 2

    Enum JenisStatusKredit
        Semua = 0
        Cair = 1
        Belum_Cair = 2
    End Enum

    Public Enum myPos
        normal = 0
        add = 1
        edit = 2
        delete = 3
    End Enum

    Public Enum eFormPosition
        manualPosition = 0
        centerScreen = 1
        defaultLocation = 2
        defaultBounds = 3
        centerParent = 4
    End Enum

    Public Enum eModule
        mTabungan = 0
        mDeposito = 1
        mKredit = 2
    End Enum

    Public Enum eCfg
        msKodeKas = 0
        msNama = 1
        msAlamat = 2
        msTelepon = 3
        msFax = 4
        msKota = 5
        msEmail = 6
        msProvinsi = 7
        msKodeCabang = 8
        msKodeBunga = 9
        msKodePajakBunga = 10
        msRekeningPemindahBukuan = 11
        msKodePenarikanPemindahBukuan = 12
        msKodePenyetoranPemindahbukuan = 13
        msKodeAdministrasi = 14
        msKodeGolonganNasabahTerkait = 15
        msKodeLaba = 16

        msTopMargin1 = 17
        msTopMargin2 = 18
        msLeftMargin = 19
        msLebarNomor = 20
        msLebarTgl = 21
        msLebarSandi = 22
        msLebarMutasi = 23
        msLebarKredit = 24
        msLebarSaldo = 25
        msPaperHight = 26
        msPaperWidth = 27

        ' Cetak Header Tabungan
        msTopHeaderTabungan = 28
        msLeftHeaderTabungan = 29
        msTopValidasiTabungan = 30
        msLeftValidasiTabungan = 31

        msSaldoMinimumKenaPajak = 32

        msKodePenarikanTunai = 33
        msVersion = 34
        msSandiBank = 35
        msSandiCabang = 36

        msDirectoryFoto = 37
        msKodeSetoranTunai = 38

        ' Cetak Buku Tabungan Yang Di Bank
        msTopMarginBank1 = 39
        msTopMarginBank2 = 40
        msLeftMarginBank = 41
        msLebarNomorBank = 42
        msLebarTglBank = 43
        msLebarSandiBank = 44
        msLebarMutasiBank = 45
        msLebarKreditBank = 46
        msLebarSaldoBank = 47
        msPaperHightBank = 48
        msPaperWidthBank = 49

        ' Cetak Header Tabungan Yang Di Bank
        msTopHeaderTabunganBank = 50
        msLeftHeaderTabunganBank = 51

        msKodeAdmPemeliharaan = 52
        msJabatanDirut = 53
        msAlamatDirut = 54
        msNamaDirut = 55
        msKodeLabaTahunLalu = 56

        ' Cetak Kartu Angsuran
        msTopMarginKredit = 57
        msLeftMarginKredit = 58
        msPaperHeightKredit = 59
        msPaperWidthKredit = 60
        msLebarNomorKredit = 61
        msLebarTglKredit = 62
        msLebarPokokKredit = 63
        msLebarBungaKredit = 64
        msLebarJumlahKredit = 65
        msLebarUserNameKredit = 66

        ' Cetak Header Kartu Angsuran Halaman 1
        msTopHeaderKredit = 67
        msLeftHeaderKredit = 68
        msPaperHightHeaderKredit = 69
        msPaperWidthHeaderKredit = 70

        ' Penjualan aktiva
        msRekPenjualanAktiva = 71

        ' file cabang & pusat
        msFileCabang = 72
        msFilePusat = 73

        msKecamatan = 74

        msPejabatBank = 75
        msNamaDirektur = 76

        ' Cetak Header Kartu Angsuran Halaman 2
        msTopHeaderKredit1 = 77
        msLeftHeaderKredit1 = 78
        msPaperHightHeaderKredit1 = 79
        msPaperWidthHeaderKredit1 = 80

        ' Rekening jurnal setoran tabungan jika setoran pemindahbukuan
        msRekeningSetoranTabunganPB = 81

        ' Rekening Taksiran Pajak Pph 25
        msKodeTaksiranPphPasal25 = 82

        ' Preview cabang di laporan
        msPreviewSemuaCabang = 83

        ' untuk cek versi
        msAutoVersion = 84

        ' rekening jurnal untuk RRP Bunga Yang Masih Harus Dibayar
        msRekeningTitipanPajakTabungan = 85
        msRekeningTitipanPajakDeposito = 86

        ' rekening pendapatan simarmas
        msRekeningPendapatanSimarMas = 87

        ' Kode penutupan tabungan
        msKodePenutupanTabungan = 88

        ' Rekening Jurnal Untuk Taksiran Pajak Yang ditampilkan di Laba Rugi Bulanan
        msRekeningJurnalTaksiranPajak = 89

        ' Lebar kolom user name saat cetak mutasi ke buku tabungan
        msUserNameBukuTabungan = 90

        ' Rekening Antar Kantor
        msRekeningAntarKantorAktiva = 91
        msRekeningAntarKantorPasiva = 92

        ' Rekening Amortisasi Provisi
        msRekeningAmortisasiProvisi = 93

        msRekeningPendapatan = 94

        ' Nama Kepala Cabang
        msNamaKepalaCabang = 95

        msKodePembayaranPokokRekeningKoran = 96
        msKodePenarikanPokokRekeningKoran = 97
        msKodeBungaRekeningKoran = 98

        msPembulatanPPAP = 99

        ' kode rekening jurnal untuk penjualan aktiva tetap
        msKodeRekeningDebetAktivaTetap = 100
        msKodeRekeningKreditAktivaTetap = 101

        ' kode rekening hapus buku kredit
        msRekeningHapusBuku = 102

        ' kode rekening ayda
        msRekeningAYDA = 103

        'kode Rekening tunggakan bunga
        msRekeningTunggakanBunga = 104

        'kode Rekening OverDraft
        msRekeningOverdraft = 105

        ' kode rekening pendapatan dan biaya operasional / non operasional
        msRekeningPendapatanOperasional = 106
        msRekeningPendapatanOperasional1 = 107
        msRekeningPendapatanNonOperasional = 108
        msRekeningPendapatanNonOperasional1 = 109
        msRekeningBiayaOperasional = 110
        msRekeningBiayaOperasional1 = 111
        msRekeningBiayaNonOperasional = 112
        msRekeningBiayaNonOperasional1 = 113

        ' kode wilayah default
        msKodeDefaultWilayah = 114
    End Enum

    Public Enum eFormatType
        yyyy_MM_dd = 0
        dd_MM_yyyy = 1
        BilRpPict2 = 2
        BilRpPict = 3
        Default_Value = 4
        dd_MM_YY = 5
        dd_MMMM_YYYY = 6
        hh_mm_ss = 7
    End Enum

    Public Enum eAngsuran
        ' Mutasi Debet
        ags_jadwal = 0
        ags_Kewajiban = 1
        ags_Realisasi = 2
        ags_Koreksi = 3

        ' Mutasi Kredit
        ags_Angsuran = 5
        ags_Titipan = 6
    End Enum

    Public Enum eFaktur
        fkt_MutasiTabungan = 0
        fkt_Angsuran = 1
        fkt_Relisasi = 2
        fkt_Jurnal = 3
        fkt_Deposito = 4
        fkt_KasMasuk = 5
        fkt_KasKeluar = 6
        fkt_PembelianAktiva = 7
        fkt_PenjualanAktiva = 8
        fkt_BreakAktiva = 9
        fkt_RincianMataUang = 10
        fkt_AddendumKredit = 11
        fkt_Jadwal_Angsuran = 12
        fkt_PenerimaanKasBesar = 13
        fkt_PengeluaranKasBesar = 14
        fkt_AYDA = 15
        fkt_HapusBuku = 16
        fkt_OverDraft = 17
        fkt_BungaRekeningKoran = 18
        fkt_AccrualKredit = 19
        fkt_AccrualDeposito = 20
        fkt_AktivaTetap = 21
        fkt_Angsuran_Instansi = 22
        fkt_Amortisasi_Provisi = 23
    End Enum

    Public Enum eRegistry
        reg_DSN = 0
        reg_WallPaper = 1
        reg_PicturePath = 2
        reg_IP = 3
        reg_QuickMenu = 4
        reg_Database = 5
        reg_LocalIP = 6
        reg_Printer = 7
        reg_Port = 8
        reg_Printer_Kertas = 9
    End Enum

    Public Enum AccFiat
        acRequest = 0
        acACC = 1
        acTolak = 2
        acBatal = 3
        acNoProses = 9
    End Enum

    Public Enum eTypeRekening
        eAktiva = 1
        eHutang = 2
        eModal = 3
        ePendapatan = 4
        eBiaya = 5
        eAdministratif = 6
    End Enum

    Public Sub CenterForm(ByVal cForm As Form, Optional ByVal pPosition As eFormPosition = eFormPosition.defaultLocation)
        Select Case pPosition
            Case eFormPosition.centerParent
                cForm.StartPosition = FormStartPosition.CenterParent
            Case eFormPosition.centerScreen
                cForm.StartPosition = FormStartPosition.CenterScreen
            Case eFormPosition.defaultBounds
                cForm.StartPosition = FormStartPosition.WindowsDefaultBounds
            Case eFormPosition.defaultLocation
                cForm.StartPosition = FormStartPosition.WindowsDefaultLocation
            Case eFormPosition.manualPosition
                cForm.StartPosition = FormStartPosition.Manual
        End Select
    End Sub

    Public Function GetNull(ByRef pValue As Object, Optional ByVal pDefault As Object = 0) As Object
        Return IIf(IsDBNull(pValue) Or IsNothing(pValue), pDefault, pValue)
    End Function

    Sub SetTabIndex(ByRef nObj As Integer, ByRef n As Integer)
        nObj = CInt(n)
        n = n + 1
    End Sub

    Public Sub SetButton(ByRef cmdSimpan As DevExpress.XtraEditors.SimpleButton, ByRef cmdKeluar As DevExpress.XtraEditors.SimpleButton, _
                         ByRef cmdAdd As DevExpress.XtraEditors.SimpleButton, ByRef cmdEdit As DevExpress.XtraEditors.SimpleButton, _
                         ByRef cmdHapus As DevExpress.XtraEditors.SimpleButton, ByRef nPos As myPos, _
                         ByRef lPar As Boolean, Optional ByRef cmdAktivasi As DevExpress.XtraEditors.SimpleButton = Nothing)
        Try
            cmdSimpan.Enabled = lPar
            cmdAdd.Enabled = Not lPar
            cmdEdit.Enabled = Not lPar
            cmdHapus.Enabled = Not lPar
            If Not IsNothing(cmdAktivasi) Then cmdAktivasi.Visible = nUserLevel = 0
            If lPar Then
                'cmdKeluar.Picture = aMainMenu.pcCancel.Picture
                cmdKeluar.Text = "&Batal"
            Else
                'cmdKeluar.Picture = aMainMenu.pcExit.Picture
                cmdKeluar.Text = "&Keluar"

                Select Case nPos
                    Case Func.myPos.add
                        cmdAdd.Focus()
                    Case Func.myPos.edit
                        cmdEdit.Focus()
                    Case Func.myPos.delete
                        cmdHapus.Focus()
                    Case Else
                        Exit Select
                End Select
                nPos = Func.myPos.normal
            End If

            If Not lPar Then
                'GetFormLevel(cmdSimpan.Parent.Name, nUserLevel, cmdAdd, cmdEdit, cmdHapus)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Function CheckData(ByVal cData As Object, ByVal cMsg As String) As Boolean
        Return True
        If Len(Trim(CStr(cData))) = 0 Or Val(cData) = 0 Then
            Return False
            MessageBox.Show(cMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Function

    Sub CheckKeyPress(ByVal e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            SendKeys.Send("{Tab}")
        End If
    End Sub

    Function SetUserName(Optional ByVal par As Byte = 0, Optional ByVal cValue As String = "", Optional ByVal lPar As Boolean = False) As Object
        Static cUser As String
        Static cPassword As String
        Static cFullName As String
        Static nLevel As Single
        Static cID As String
        Static cIP As String

        SetUserName = ""
        If par = 1 Then           ' User
            SetUserName = cUser

            cUser = CStr(IIf(lPar, cValue, cUser))
        ElseIf par = 2 Then        ' Password
            SetUserName = cPassword

            cPassword = CStr(IIf(lPar, cValue, cPassword))
        ElseIf par = 3 Then        ' FullName
            SetUserName = cFullName

            cFullName = CStr(IIf(lPar, cValue, cFullName))
        ElseIf par = 4 Then        ' Password
            SetUserName = nLevel

            nLevel = CSng(IIf(lPar, cValue, nLevel))
        ElseIf par = 5 Then
            SetUserName = cID

            cID = CStr(IIf(lPar, cValue, cID))
        ElseIf par = 6 Then
            SetUserName = cIP

            cIP = CStr(IIf(lPar, cValue, cIP))
        End If
    End Function

    Sub RunGaugeMyDLL(Optional ByRef nPar As Single = 0, Optional ByRef nMax As Integer = 0)
        Static nDisplay As Single
        On Error Resume Next
        If nPar = 0 And nDisplay = 1 Then
            If Int(FrmLogin.pb.Value) < FrmLogin.pb.Maximum Then
                FrmLogin.pb.Value = Int(FrmLogin.pb.Value + 1)
            End If
        End If
        If nPar <> 0 Then
            nDisplay = nPar
            If nPar = 1 Then
                FrmLogin.pb.Visible = True
                FrmLogin.pb.Minimum = 0
            ElseIf nPar = 2 Then
                FrmLogin.Visible = False
            End If
        End If
        If nMax > 0 Then
            FrmLogin.pb.Maximum = nMax
        End If
    End Sub

    Function GetOpt(ByVal opt As DevExpress.XtraEditors.RadioGroup) As String
        Dim n As Integer, i As Integer, lChar As Boolean
        GetOpt = ""
        For n = 0 To opt.Properties.Items.Count - 1
            'If CBool(opt.Properties.Items(n).Value) Then
            If opt.SelectedIndex = n Then
                With opt.Properties.Items(n)
                    For i = 1 To Len(.ToString)
                        If lChar Then
                            Return UCase(Mid(.ToString, i, 1))
                            Exit Function
                        End If
                        If Mid(.ToString, i, 1) = "&" Then
                            lChar = True
                        End If
                    Next
                End With
            End If
        Next
    End Function

    Sub SetOpt(ByVal opt As DevExpress.XtraEditors.RadioGroup, ByVal cTemp As String)
        Dim n As Integer, i As Integer, lChar As Boolean
        'cTemp = ""
        opt.SelectedIndex = 0
        For n = 0 To opt.Properties.Items.Count - 1
            With opt.Properties.Items(n)
                For i = 1 To Len(.ToString)
                    If lChar Then
                        lChar = False
                        If UCase(Mid(.ToString, i, 1)) = UCase(cTemp) Then
                            'opt.Properties.Items(n).Value = True
                            opt.SelectedIndex = n
                            Exit Sub
                        End If
                    End If

                    If Mid(.ToString, i, 1) = "&" Then
                        lChar = True
                    End If
                Next
            End With
        Next
    End Sub

    Sub SetButtonPicture(Optional ByVal cForm As Form = Nothing, Optional ByRef cmdAdd As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdEdit As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdHapus As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdSimpan As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdKeluar As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdAktivasi As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdPreview As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdOK As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdCheck As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdUncheck As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdAppOpen As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdCari As DevExpress.XtraEditors.SimpleButton = Nothing, _
                         Optional ByRef cmdPrint As DevExpress.XtraEditors.SimpleButton = Nothing, Optional ByRef cmdRefresh As DevExpress.XtraEditors.SimpleButton = Nothing)
        On Error Resume Next
        cForm.CancelButton = cmdKeluar
        With aMainMenu.imlButton
            cmdAktivasi.Image = .Images.Item(0)
            If cmdAktivasi.ToolTip = "" Then
                cmdAktivasi.ToolTip = "Aktivasi"
            End If
            cmdAdd.Image = .Images.Item(1)
            If cmdAdd.ToolTip = "" Then
                cmdAdd.ToolTip = "Tambah"
            End If
            cmdEdit.Image = .Images.Item(2)
            If cmdEdit.ToolTip = "" Then
                cmdEdit.ToolTip = "Koreksi"
            End If
            cmdHapus.Image = .Images.Item(3)
            If cmdHapus.ToolTip = "" Then
                cmdHapus.ToolTip = "Hapus"
            End If
            cmdSimpan.Image = .Images.Item(4)
            If cmdSimpan.ToolTip = "" Then
                cmdSimpan.ToolTip = "Simpan"
            End If
            cmdKeluar.Image = .Images.Item(5)
            If cmdKeluar.ToolTip = "" Then
                cmdKeluar.ToolTip = "Keluar / Batal"
            End If
            If cmdKeluar.Text <> "" And Left(cmdKeluar.Text, 1) <> "&" Then
                cmdKeluar.Text = "&" & cmdKeluar.Text
            End If
            cmdOK.Image = .Images.Item(6)
            If cmdOK.ToolTip = "" Then
                cmdOK.ToolTip = "OK"
            End If
            cmdPreview.Image = .Images.Item(7)
            If cmdPreview.ToolTip = "" Then
                cmdPreview.ToolTip = "Preview"
            End If
            cmdUncheck.Image = .Images.Item(8)
            If cmdUncheck.ToolTip = "" Then
                cmdUncheck.ToolTip = "Clear All"
            End If
            cmdCheck.Image = .Images.Item(9)
            If cmdCheck.ToolTip = "" Then
                cmdCheck.ToolTip = "Select All"
            End If
            cmdAppOpen.Image = .Images.Item(10)
            If cmdAppOpen.ToolTip = "" Then
                cmdAppOpen.ToolTip = "Buka Form"
            End If
            cmdCari.Image = .Images.Item(11)
            If cmdCari.ToolTip = "" Then
                cmdCari.ToolTip = "Cari Data"
            End If
            cmdPrint.Image = .Images.Item(12)
            If cmdPrint.ToolTip = "" Then
                cmdPrint.ToolTip = "Print"
            End If
            cmdRefresh.Image = .Images.Item(13)
            If cmdRefresh.ToolTip = "" Then
                cmdRefresh.ToolTip = "Refresh"
            End If
        End With
    End Sub

    Function GetAppPath() As String
        Dim location = Assembly.GetExecutingAssembly().Location
        Dim cPath As String = Path.GetDirectoryName(location)
        'path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)
        GetAppPath = cPath
    End Function

    Function aCfg(ByVal Par As eCfg, Optional ByVal cDefault As String = "") As Object
        Dim cAppExeName As String = GetAppExeName()
        Dim vaRetval As Object = GetSetting("Heasoft", cAppExeName, "Cfg" & Par, "[C]" & CStr(cDefault))
        Dim cTipe As String = Mid(CStr(vaRetval), 2, 3)
        Dim cValue As String = Mid(CStr(vaRetval), 6)
        Select Case cTipe
            Case "[D]"
                Return DateSerial(CInt(Left(cValue, 4)), CInt(Mid(cValue, 5, 2)), CInt(Mid(cValue, 7, 2)))
            Case Else
                Return cValue
        End Select
    End Function

    Sub UpdCfg(ByVal par As eCfg, ByVal Keterangan As Object, Optional ByVal cCabang As String = "")
        Dim cTipe As String = "C"
        Dim cAppExeName As String = GetAppExeName()
        Dim cKodeCabang As String
        If VarType(Keterangan) = VariantType.Date Then
            Keterangan = Format(Keterangan, "yyyymmdd")
            cTipe = "D"
        End If
        Try
            If cCabang = "" Then
                cKodeCabang = CStr(aCfg(eCfg.msKodeCabang))
            Else
                cKodeCabang = cCabang
            End If
            SaveSetting("Heasoft", cAppExeName, "Cfg" & CInt(par), String.Format("[{0}]{1}", cTipe, Keterangan))
            Dim vaField() As Object = {"jenis", "Keterangan", "tipe", "cabangentry"}
            Dim vaValue() As Object = {CInt(par), Keterangan, cTipe, cKodeCabang}
            Dim cWhere As String = String.Format("jenis = '{0}'", CInt(par))
            objData.Update(GetDSN, "config", cWhere, vaField, vaValue)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub InitCfg()
        Try
            Dim cDataTable = objData.Browse(GetDSN, "config")
            With cDataTable
                Dim nRow As Integer = .Rows.Count
                Dim n As Integer = 0
                Dim cTipe As String = ""
                Dim cExeName As String = GetAppExeName()
                Do Until nRow = n
                    Dim cKeterangan As String = .Rows(n).Item("keterangan").ToString
                    Dim cPar As String = .Rows(n).Item("jenis").ToString
                    cTipe = CStr(IIf(cKeterangan = "D", "[D]", "[C]"))
                    SaveSetting("Heasoft", cExeName, "Cfg" & cPar, String.Format("[{0}]{1}", cTipe, cKeterangan))
                    n = n + 1
                Loop
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub GetDataLookup(ByVal cTable As String, ByVal cLookUpEdit As DevExpress.XtraEditors.LookUpEdit, Optional ByVal cWhere As String = "", _
                      Optional ByVal cField As String = "kode,keterangan", Optional ByVal vaJoin() As Object = Nothing)
        Dim bsTemp As BindingSource
        Dim n As Integer
        Try
            Dim cDataSet = objData.BrowseDataSet(GetDSN, cTable, cField, , , , cWhere, "kode", vaJoin)
            bsTemp = New BindingSource(cDataSet, cTable)
            With cLookUpEdit
                .EditValue = Nothing
                .DataBindings.Clear()
                '.DataBindings.Add("EditValue", bsTemp, "kode")
                .Properties.DisplayMember = "kode"
                .Properties.ValueMember = "kode"
                .Properties.DataSource = bsTemp
                .Properties.NullText = ""
                .Properties.TextEditStyle = TextEditStyles.Standard
                Dim coll As LookUpColumnInfoCollection = .Properties.Columns
                coll.Clear()
                Dim va() As String = Split(cField, ",")
                For n = 0 To va.GetUpperBound(0)
                    coll.Add(New LookUpColumnInfo(va(n), 0))
                Next
                .Properties.BestFitMode = BestFitMode.BestFitResizePopup
                .Properties.SearchMode = SearchMode.AutoFilter
                .EnterMoveNextControl = True
                .Properties.HotTrackItems = True
                .Properties.AutoSearchColumnIndex = 0
                .Properties.ValidateOnEnterKey = True
                .EditValue = Nothing
                cDataSet.Dispose()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub GetKeteranganLookUp(ByVal objKode As DevExpress.XtraEditors.LookUpEdit, ByVal objNama As DevExpress.XtraEditors.TextEdit, _
                            Optional ByVal cValueColumn As String = "kode", Optional ByVal cTargetColumn As String = "keterangan")
        Dim r As Integer = objKode.Properties.GetDataSourceRowIndex(objKode.Properties.Columns(cValueColumn), objKode.Text)
        If r >= 0 Then
            Dim cValue As String = objKode.Properties.GetDataSourceValue(cTargetColumn, r).ToString
            objNama.Text = cValue
        End If
    End Sub

    Function GetKeteranganLookUpFunction(ByVal objKode As DevExpress.XtraEditors.LookUpEdit, _
                                         Optional ByVal cValueColumn As String = "kode", Optional ByVal cTargetColumn As String = "keterangan") As String
        GetKeteranganLookUpFunction = ""
        Dim r As Integer = objKode.Properties.GetDataSourceRowIndex(objKode.Properties.Columns(cValueColumn), objKode.Text)
        If r >= 0 Then
            Dim cValue As String = objKode.Properties.GetDataSourceValue(cTargetColumn, r).ToString
            GetKeteranganLookUpFunction = cValue
        End If
    End Function

    Sub LookupSearch(ByVal cTable As String, ByVal cLookUpEdit As DevExpress.XtraEditors.LookUpEdit, _
                     ByVal cDisplayField As String, Optional ByVal cWhere As String = "", _
                     Optional ByVal cField As String = "*", Optional ByVal vaJoin() As Object = Nothing, _
                     Optional ByVal cOrder As String = "", Optional ByVal cGroup As String = "", _
                     Optional ByVal nColumnIndexSearch As Integer = 0)
        Dim bsTemp As BindingSource
        Dim n As Integer
        Dim va() As String
        Dim va1() As String
        Dim cTemp As String
        Try
            If cOrder = "" Then
                Dim cTableTemp() As String = Split(cTable, " ")
                If cTableTemp.GetUpperBound(0) > 0 Then
                    va = Split(cDisplayField, ".")
                    If va.GetUpperBound(0) > 0 Then
                        cOrder = cDisplayField
                    Else
                        cOrder = String.Format("{0}.{1}", cTableTemp(1), cDisplayField)
                    End If
                End If
            End If
            Dim cDataSet = objData.BrowseDataSet(GetDSN, cTable, cField, , , , _
                                                 cWhere, cOrder, vaJoin, cGroup)
            bsTemp = New BindingSource(cDataSet, cTable)
            With cLookUpEdit
                .EditValue = Nothing
                .DataBindings.Clear()
                va = Split(cDisplayField, ".")
                If va.GetUpperBound(0) > 0 Then
                    cTemp = va(1)
                Else
                    cTemp = va(0)
                End If
                .Properties.DisplayMember = cTemp
                .Properties.ValueMember = cTemp
                .Properties.DataSource = bsTemp
                .Properties.NullText = ""
                .Properties.TextEditStyle = TextEditStyles.Standard
                Dim coll As LookUpColumnInfoCollection = .Properties.Columns
                coll.Clear()
                va = Split(cField, ",")
                For n = 0 To va.GetUpperBound(0)
                    va1 = Split(va(n), ".")
                    coll.Add(New LookUpColumnInfo(va1(1), 0))
                Next
                .Properties.BestFitMode = BestFitMode.BestFitResizePopup
                .Properties.SearchMode = SearchMode.AutoFilter
                .EnterMoveNextControl = True
                .Properties.HotTrackItems = True
                .Properties.AutoSearchColumnIndex = nColumnIndexSearch
                .Properties.ValidateOnEnterKey = True
                .EditValue = Nothing
                cDataSet.Dispose()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub KeteranganLookupSearch(ByVal objKode As DevExpress.XtraEditors.LookUpEdit, ByVal objNama As DevExpress.XtraEditors.TextEdit, _
                               Optional ByVal cValueColumn As String = "kode", Optional ByVal cTargetColumn As String = "keterangan")
        Dim r As Integer = objKode.Properties.GetDataSourceRowIndex(objKode.Properties.Columns(cValueColumn), objKode.Text)
        If r >= 0 Then
            Dim cValue As String = objKode.Properties.GetDataSourceValue(cTargetColumn, r).ToString
            objNama.Text = cValue
        End If
    End Sub

    Sub SendTab(ByVal e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Public Sub CenterFormManual(ByVal objForm As Form, Optional ByVal lZeroTopLeft As Boolean = False, Optional ByVal lZeroTop As Boolean = False)
        Dim mainScreen As Screen = Screen.FromPoint(objForm.Location)
        Dim x As Double = 0, y As Double = 0
        If lZeroTopLeft Then
            x = (mainScreen.WorkingArea.Width - objForm.Width) / 2 + mainScreen.WorkingArea.Left
            y = 0
        ElseIf lZeroTop Then
            x = 0
            y = 0 '(mainScreen.WorkingArea.Height - objForm.Height) / 2 + mainScreen.WorkingArea.Top - 100
        Else
            x = (mainScreen.WorkingArea.Width - objForm.Width) / 2 + mainScreen.WorkingArea.Left
            y = (mainScreen.WorkingArea.Height - objForm.Height) / 2 + mainScreen.WorkingArea.Top - 50
        End If
        objForm.StartPosition = FormStartPosition.Manual
        objForm.Location = New Point(CInt(x), CInt(y))
        objForm.Icon = aMainMenu.Icon
    End Sub

    Function GetLastFaktur(ByVal nPar As eFaktur, ByVal dTgl As Date, Optional ByVal cFaktur As String = "", _
                           Optional ByVal lUpdate As Boolean = False, Optional ByVal lCetakRincian As Boolean = False) As String
        Dim cNomor As String = "1"
        Dim cKode As String = ""
        Dim nCount As Double = 0
        Dim cDataTable As DataTable

        Dim vaFaktur() As String = {"TB", "AG", "RO", "JR", "DP", "TM", "KK", "BA", "JK", "RA", "MT", _
                                    "AK", "JA", "AA", "ZZ", "AY", "HB", "OV", "BK", "AC", "AD", "AT", _
                                    "AI", "PV"}
        Dim cKarakter As String = vaFaktur(nPar)
        If Not lCetakRincian Then
            cKode = cKarakter & CStr(aCfg(eCfg.msKodeCabang)) & Format(dTgl, "yyyyMMdd")
        Else
            cKode = cKarakter & Year(dTgl)
        End If
        If Trim(cFaktur) = "" Then
            If lUpdate Then
                objData.Add(GetDSN, "nomorfaktur", {"Kode"}, {cKode})
                ' Untuk Menghemat Ukuran Table Hapus jika Nomor ID < Nomor yang aktif
                cDataTable = objData.SQL(GetDSN, "Select Last_Insert_id() as Total")
                If cDataTable.Rows.Count > 0 Then
                    objData.Delete(GetDSN, "nomorfaktur", "Kode", , cKode, " and id < " & CDbl(GetNull(cDataTable.Rows(0).Item("total").ToString)))
                End If
                nCount = 0
            Else
                cDataTable = objData.Browse(GetDSN, "NomorFaktur", "max(ID) as total", "kode", , cKode)
                nCount = 1
            End If
            If cDataTable.Rows.Count > 0 Then
                cNomor = CStr(Val(GetNull(cDataTable.Rows(0).Item("total").ToString)) + nCount)
            End If
        Else
            cNomor = cFaktur
        End If
        cNomor = Padl(Trim(cNomor), 7, "0")
        cNomor = cKarakter & CStr(aCfg(eCfg.msKodeCabang)) & Format(dTgl, "yyMMdd") & cUserID & cNomor
        GetLastFaktur = cNomor
    End Function

    Function GetTglTransaksi() As Date
        Dim dTgl As Date
        Dim cDataTable As DataTable = objData.Browse(GetDSN, "tgltransaksi", "tgl", "status", , "0")
        If cDataTable.Rows.Count > 0 Then
            dTgl = CDate(cDataTable.Rows(0).Item("tgl").ToString)
        Else
            dTgl = CDate(formatValue(Date.Today, formatType.yyyy_MM_dd))
        End If
        GetTglTransaksi = dTgl
    End Function

    Function checkTglTransaksi(ByVal dTanggal As Date, Optional ByVal lCetak As Boolean = False) As Boolean
        Dim dTglTemp As Date = #1/1/1900#
        Dim dTglTransaksi As Date = GetTglTransaksi()
        checkTglTransaksi = True
        If nUserLevel = 0 Or nUserLevel = 9 Then
            dTglTemp = BOM(dTglTransaksi)
            If dTanggal < dTglTemp And Not lCetak And Debugger.IsAttached Then
                MessageBox.Show(String.Format("Bulan tanggal transaksi harus bulan {0}.....!!!!", GetNamaBulan(GetTglTransaksi)), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                checkTglTransaksi = False
            End If
        Else
            dTglTemp = dTglTransaksi
            If dTglTemp < BOM(dTglTransaksi) Then
                dTglTemp = BOM(dTglTransaksi)
            End If
            If dTanggal < dTglTemp And Not lCetak And Debugger.IsAttached Then
                MessageBox.Show(String.Format("Tanggal transaksi tidak boleh kurang dari tanggal {0}.....!!!!", formatValue(dTglTemp, formatType.yyyy_MM_dd)), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                checkTglTransaksi = False
            End If
        End If
    End Function

    Function BOM(Optional ByVal dTanggal As Date = #1/1/1900#) As Date
        If dTanggal = #1/1/1900# Then
            dTanggal = Date.Today
        End If
        BOM = DateSerial(Year(dTanggal), Month(dTanggal), 1)
    End Function

    Function EOM(Optional ByVal dTanggal As Date = #1/1/1900#) As Date
        If dTanggal = #1/1/1900# Then
            dTanggal = Date.Today
        End If
        EOM = DateSerial(Year(dTanggal), Month(dTanggal) + 1, 0)
    End Function

    Function GetNamaBulan(ByVal dTanggal As Date, Optional ByVal lBulanRomawi As Boolean = False) As String
        Dim nBulan As Integer = Month(dTanggal)
        Dim vaBulan() As String
        If Not lBulanRomawi Then
            vaBulan = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", _
                       "Juli", "Agustus", "September", "Oktober", "November", "Desember"}
        Else
            vaBulan = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"}
        End If
        GetNamaBulan = vaBulan(nBulan - 1)
    End Function

    Function KasTeller() As Boolean
        KasTeller = True
        If Trim(cKasTeller) = "" Then
            MessageBox.Show("Kode Kas Teller Tidak Ada, Anda Tidak Bisa Menjalakan Modul Ini" + vbCrLf + _
                            "Hubungi Suppervisor Anda Untuk Melakukan Setup Kas Teller", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            KasTeller = False
        End If
    End Function

    Sub SetTglTransaksi(ByVal dTgl As DevExpress.XtraEditors.DateEdit)
        '  dTgl.Enabled = False
        '  If nUserLevel = 0 Or nUserLevel = 9 Then
        dTgl.Enabled = True
        '  End If
    End Sub

    Function IsOpenTransaksi() As Boolean
        'Dim cKodeCabang As String = CStr(aCfg(eCfg.msKodeCabang))
        IsOpenTransaksi = True
        'If cKodeCabang <> "01" Then
        '    If nUserLevel < 0 Then
        '        Dim cDataTable As DataTable = objData.Browse(GetDSN,"tgltransaksi", "tgl", "cabangentry", , cKodeCabang)
        '        If cDataTable.Rows.Count > 0 Then
        '            Dim cWhere As String = "and cabangEntry = '" & cDataTable.Rows(0).Item("cabangentry").ToString & ""
        '            cDataTable = objData.Browse(GetDSN,"TglTransaksi", "tgl", "status", , "0", cWhere)
        '            If cDataTable.Rows.Count > 0 Then
        '                MessageBox.Show("Tanggal Transaksi Belum Disetup, Entry Data Penerimaan Kas Besar Untuk Setup Tanggal Transaksi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '                IsOpenTransaksi = False
        '                Exit Function
        '            ElseIf CDate(cDataTable.Rows(0).Item("tgl").ToString) <> Date.Today Then
        '                IsOpenTransaksi = MessageBox.Show("Tanggal System Tidak Sama Dengan Tanggal Komputer, Transaksi Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes
        '            End If
        '        End If
        '    End If
        'End If
    End Function

    Function GetRegistry(ByVal par As eRegistry, Optional ByVal cDefault As String = "") As String
        GetRegistry = GetSetting("Heasoft", GetAppExeName, "Reg" & par, cDefault)
    End Function

    Sub SaveRegistry(ByVal par As eRegistry, ByVal cValue As String)
        SaveSetting("Heasoft", GetAppExeName, "Reg" & par, cValue)
    End Sub

    Function GetInduk(Optional ByVal cRekening As String = "", Optional ByVal lPad As Boolean = True) As String
        Dim lStop As Boolean = False
        Dim nOldLen As Integer = cRekening.Length
        Do While Not lStop
            cRekening = Trim(cRekening)
            If Right(cRekening, 1) = "." Then
                cRekening = Left(cRekening, Len(cRekening) - 1)
            Else
                lStop = True
            End If
        Loop
        If lPad Then
            GetInduk = Pad(cRekening, nOldLen)
        Else
            GetInduk = cRekening
        End If
    End Function

    Function GetAppExeName() As String
        Dim asmExecuting As Assembly = Assembly.GetExecutingAssembly
        Dim cAppExeName As String = Path.GetFileName(asmExecuting.Location)
        Return cAppExeName
    End Function

    Sub InitForm(ByVal cForm As Form, Optional ByVal lMaximize As Boolean = False, Optional ByVal lMinimize As Boolean = False, _
             Optional ByVal cmdBatal As DevExpress.XtraEditors.SimpleButton = Nothing,
             Optional ByVal nBorderStyle As FormBorderStyle = FormBorderStyle.FixedSingle)
        With cForm
            .KeyPreview = True
            .MaximizeBox = lMaximize
            .MinimizeBox = lMinimize
            .CancelButton = cmdBatal
            .FormBorderStyle = nBorderStyle ' FormBorderStyle.FixedSingle
        End With
    End Sub

    Function ReplaceSpecialCharacter(ByVal cKeterangan As String) As String
        ReplaceSpecialCharacter = Replace(cKeterangan, "'", "''")
    End Function

    Function GetLocalDate(ByVal dTgl As Date, Optional ByVal lDay As Boolean = True, Optional ByVal lMonth As Boolean = True, Optional ByVal lYear As Boolean = True, Optional lSortMonth As Boolean = False) As String
        GetLocalDate = Trim(String.Format("{0} {1} {2}", IIf(lDay, (dTgl.Day), ""), IIf(lMonth, GetMonth(dTgl.Month, lSortMonth), ""), IIf(lYear, dTgl.Year, "")))
    End Function

    Function IsHoliday(ByVal dTgl As Date, Optional ByVal lSabtuDiperhitungkan As Boolean = True) As Boolean
        Dim db As New DataTable
        db = objData.Browse(GetDSN, "HariLibur", , "Tgl", , formatValue(dTgl, formatType.yyyy_MM_dd))
        If db.Rows.Count > 0 Then
            IsHoliday = True
        Else
            IsHoliday = False
        End If

        If lSabtuDiperhitungkan Then
            If Weekday(dTgl) = 1 Or Weekday(dTgl) = 7 Then
                IsHoliday = True
            End If
        Else
            If Weekday(dTgl) = 1 Then
                IsHoliday = True
            End If
        End If
        db.Dispose()
    End Function

    Function GetID(ByVal dTgl As Date) As Double
        Dim cKode As String = String.Format("LD{0}{1}", aCfg(eCfg.msKodeCabang), Format(dTgl, "yyyyMMdd"))
        Dim db As New DataTable
        Dim nRetval As Double
        objData.Add(GetDSN, "nomorfaktur", {"Kode"}, {cKode})
        db = objData.SQL(GetDSN, "Select Last_Insert_id() as Total")
        If db.Rows.Count > 0 Then
            nRetval = Val(GetNull(db.Rows(0).Item("Total")))
        Else
            nRetval = 1
        End If

        ' Untuk Menghemat Ukuran Table Hapus jika Nomor ID < Nomor yang aktif
        If db.Rows.Count > 0 Then
            objData.Delete(GetDSN, "NomorFaktur", "Kode", , cKode, " and id < " & nRetval)
        End If

        GetID = nRetval
    End Function

    Public Function IsSatuCabang(ByVal cCabangA As String, ByVal cCabangB As String) As Boolean
        Dim dbData As New DataTable
        Dim cSQL As String = String.Format("select induk from cabang where kode = '{0}' and induk in ", cCabangA)
        cSQL = String.Format("{0} (select induk from cabang where kode = '{1}')", cSQL, cCabangB)
        dbData = objData.SQL(GetDSN, cSQL)
        If dbData.Rows.Count > 0 Then
            IsSatuCabang = True
        Else
            IsSatuCabang = False
        End If
        dbData.Dispose()
    End Function

    Function GetCabang(Optional ByVal cFaktur As String = "", Optional ByVal lUseRekening As Boolean = False, Optional ByVal cRekening As String = "") As String
        If Not lUseRekening Then
            GetCabang = Mid(cFaktur, 3, 2)
        Else
            GetCabang = Left(cRekening, 2)
        End If
    End Function

    Function GetSaldoNasabah(ByVal dTgl As Date, ByVal cKode As String, Optional ByVal lPajakSaatJatuhTempo As Boolean = False,
                             Optional ByVal dJthTmp As Date = #1/1/1900#, Optional ByVal lSaldoPerModul As Boolean = False,
                             Optional ByVal lTabungan As Boolean = True) As Double
        Dim db As New DataTable
        Dim nTotal As Double = 0
        Dim dTanggalcair As Date
        Dim cField As String
        Dim cWhere As String
        Dim db1 As New DataTable
        Dim dTglTemp As Date
        Const cFieldCriteria As String = "Kode"
        Dim nSaldoTabungan As Double = 0
        Dim nSaldoDeposito As Double = 0
        Dim n As Integer

        If lPajakSaatJatuhTempo Then
            dTglTemp = dJthTmp
        Else
            dTglTemp = dTgl
        End If
        db = objData.Browse(GetDSN, "tabungan", , cFieldCriteria, , cKode)
        For n = 0 To db.Rows.Count - 1
            nTotal = nTotal + GetSaldoTabungan(db.Rows(n).Item("Rekening").ToString, dTglTemp)
            nSaldoTabungan = nTotal
        Next

        db = objData.Browse(GetDSN, "deposito", "rekening,tglcair,tgl,status,nominal", cFieldCriteria, , cKode)
        For n = 0 To db.Rows.Count - 1
            '    dTanggalcair = IIf(db!TglCair = "9999-99-99", db!Tgl, "9999-12-31")
            With db.Rows(n)
                dTanggalcair = CDate(IIf(GetNull(CDate(.Item("TglCair")), #12/31/9999#).ToString = "9999-99-99", "9999-12-31", .Item("TglCair").ToString))
                If CDate(.Item("Tgl")) <= dTgl Then
                    If Format(dTanggalcair, "yyyymm") >= Format(dTgl, "yyyymm") Then
                        nTotal = nTotal + Val(GetNull(.Item("Nominal")))
                        cField = ""
                        cField = "Sum(SetoranPlafond) as SetoranPlafond,Max(Tgl) as Tgl,Max(JTHTMP) as JT,"
                        cField = cField & "max(CaraPencairan) as CaraPencairan,Sum(PencairanPlafond) as PencairanPlafond"
                        cWhere = ""
                        cWhere = String.Format("and Rekening = '{0}' ", .Item("Rekening"))
                        cWhere = String.Format("{0}and date_format(Tgl,'%Y%m') <= date_format('{1}','%Y%m')", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
                        db1 = objData.Browse(GetDSN, "MutasiDeposito", cField, "CaraPencairan", , "I", cWhere)
                        If db1.Rows.Count > 0 Then
                            nTotal = nTotal + Val(GetNull(db1.Rows(0).Item("SetoranPlafond")))
                            nSaldoDeposito = nTotal
                        End If
                    End If
                End If
            End With
        Next
        If lSaldoPerModul Then
            If lTabungan Then
                nTotal = nSaldoTabungan
            Else
                nTotal = nSaldoDeposito
            End If
        End If
        GetSaldoNasabah = nTotal
    End Function

    Function Min(n As Double, a As Double) As Double
        Min = Val(IIf(n < a, n, a))
    End Function

    Function Max(n As Double, a As Double) As Double
        Max = Val(IIf(n < a, a, n))
    End Function

    Function GetSaldoRekening(ByVal cRekening As String, ByVal dTgl As Date, Optional ByVal cCabang As String = "00",
                              Optional ByVal Op As data.myOperator = data.myOperator.Assign, Optional ByVal cWhere2 As String = "") As Double
        Dim db As New DataTable
        Dim nSaldo As Double
        Dim cWhere As String
        If cCabang <> "00" Then
            cWhere = String.Format(" and induk = '{0}'", cCabang)
        Else
            cWhere = ""
        End If
        cWhere = cWhere & cWhere2
        db = objData.Browse(GetDSN, "BukuBesar", "Sum(Debet-Kredit) as Saldo", "Rekening", Op, cRekening,
                            String.Format(" and Tgl <= '{0}'{1}", formatValue(dTgl, formatType.yyyy_MM_dd), cWhere))
        If db.Rows.Count > 0 Then
            nSaldo = Val(GetNull(db.Rows(0).Item("Saldo")))
        Else
            nSaldo = 0
        End If

        If TypeRekening(cRekening) = eTypeRekening.eHutang Or TypeRekening(cRekening) = eTypeRekening.eModal Or TypeRekening(cRekening) = eTypeRekening.ePendapatan Then
            nSaldo = -nSaldo
        End If
        GetSaldoRekening = nSaldo
    End Function

    Function TypeRekening(ByVal cRekening As String) As eTypeRekening
        cRekening = cRekening.Substring(0, 1)
        TypeRekening = CType(cRekening, eTypeRekening)
    End Function

    Function GetNamaHari(ByVal dDate As Date) As String
        Dim vaHari() As String = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"}
        Dim nHari As Integer = Weekday(dDate)
        GetNamaHari = vaHari(nHari - 1)
    End Function

    Function Devide(ByVal a As Double, ByVal b As Double) As Double
        If a = 0 Or b = 0 Then
            Devide = 0
        Else
            Devide = a / b
        End If
    End Function

    Function GetPembulatan(ByVal nJumlah As Double, Optional nPembulatan As Double = 50, Optional lUseFromDatabase As Boolean = False,
                           Optional ByVal cRekening As String = "") As Double
        Dim nMod As Double
        Dim lNegative As Boolean
        Dim a As Double
        Dim nSelisih As Double
        Dim db As New DataTable

        If lUseFromDatabase Then
            db = objData.Browse(GetDSN, "Debitur", "Pembulatan", "Rekening", , cRekening)
            If db.Rows.Count > 0 Then
                nPembulatan = CDbl(db.Rows(0).Item("Pembulatan"))
            End If
        End If
        a = nPembulatan
        If a >= 0 Then
            lNegative = nJumlah < 0
            nJumlah = Math.Round(Math.Abs(nJumlah), 0)
            nMod = Int(Devide(nJumlah, a))
            nMod = nJumlah - (nMod * a)
            If nPembulatan > 0 Then
                nSelisih = nPembulatan - nMod
            End If
            If nMod > 0 Then
                nJumlah = nJumlah + nSelisih
            End If
        Else
            a = Math.Abs(a)
            lNegative = nJumlah < 0
            nJumlah = Math.Round(Math.Abs(nJumlah), 0)
            nMod = Int(Devide(nJumlah, a))
            nMod = nJumlah - (nMod * a)
            If nPembulatan < 0 Then
                nJumlah = nJumlah - nMod
            End If
        End If
        GetPembulatan = nJumlah
    End Function

    Function Mod1000(ByVal nJumlah As Double, Optional ByVal lRoundUp As Boolean = False) As Double
        Dim nMod As Double
        Dim cJumlah As String
        Dim lNegative As Boolean = nJumlah < 0
        Const a As Double = 1000
        nJumlah = Math.Round(Math.Abs(nJumlah), 0)
        nMod = Int(Devide(nJumlah, a))
        nMod = nJumlah - (nMod * a)
        If Not lRoundUp Then
            If nMod >= 500 Then
                nJumlah = nJumlah + 500
            End If
        Else
            If nMod > 0 Then
                nJumlah = nJumlah + 1000
            End If
        End If
        cJumlah = nJumlah.ToString
        cJumlah = Padl(cJumlah, cJumlah.Length + 3, "0")
        cJumlah = Left(cJumlah, cJumlah.Length - 3)
        If lNegative Then
            Mod1000 = -CDbl(cJumlah)
        Else
            Mod1000 = CDbl(cJumlah)
        End If
    End Function

    Function Between(ByVal Value As Object, ByVal Lower As Object, ByVal Upper As Object) As Boolean
        Between = CDbl(GetNull(Value)) >= CDbl(Lower) And CDbl(GetNull(Value)) <= CDbl(Upper)
    End Function

    Function GetPembulatan500(ByVal nJumlah As Double) As Double
        Dim a As Double = Math.Round(nJumlah)
        Dim nTotal As Double
        a = CDbl(Right(a.ToString, 3))
        nTotal = Math.Round(nJumlah) - Val(a)
        If Val(a) > 500 Then
            GetPembulatan500 = nTotal + 500
        ElseIf Val(a) = 500 Then
            GetPembulatan500 = nJumlah
        Else
            GetPembulatan500 = nTotal
        End If
    End Function

    Function SeekData(ByVal cTableName As String, ByVal cFieldName As String, ByVal cValue As String) As Boolean
        Dim db As New DataTable

        db = objData.Browse(GetDSN, cTableName, , cFieldName, , cValue)
        SeekData = db.Rows.Count > 0
    End Function

    Function GetSaldoRegister(ByVal cKode As String, ByVal dTgl As Date) As Double
        Dim db As New DataTable
        Dim nSaldo As Double = 0
        db = objData.Browse(GetDSN, "Tabungan", "Rekening", "Kode", , cKode)
        For n As Integer = 0 To db.Rows.Count - 1
            nSaldo = nSaldo + GetSaldoTabungan(db.Rows(n).Item("Rekening").ToString, dTgl)
        Next

        db = objData.Browse(GetDSN, "Deposito", "Nominal,TglCair,Status,Tgl", "Kode", , cKode)
        For n = 0 To db.Rows.Count - 1
            If db.Rows(n).Item("Status").ToString = "0" Then
                nSaldo = nSaldo + CDbl(db.Rows(n).Item("Nominal"))
            End If
        Next
        GetSaldoRegister = nSaldo
    End Function

    Sub SetCabang(ByVal cFormName As Form, Optional ByVal lNeraca As Boolean = False)
        Dim lValue As Boolean
        If cJenisKantor = "K" Then
            lValue = False
        Else
            lValue = True
        End If
        With cFormName
            If Not lNeraca Then
                For Each oControl As Control In cFormName.Controls
                    If TypeOf oControl Is DevExpress.XtraEditors.TextEdit And oControl.Name = "cFilterKantor" Then
                        oControl.Visible = lValue
                        oControl.Text = ""
                        Exit For
                    End If
                Next
            End If
            For Each oControl As Control In cFormName.Controls
                If TypeOf oControl Is DevExpress.XtraEditors.LookUpEdit And oControl.Name = "cCabang" Then
                    oControl.Visible = lValue
                    oControl.Text = ""
                    If lValue Then
                        oControl.Text = aCfg(eCfg.msKodeCabang).ToString
                        Exit For
                    End If
                End If
            Next
        End With
    End Sub

    Function GetQueryFilter(ByVal cKantor As String, ByVal cCabangTemp As String, ByVal dTanggal As Date, ByVal cField As String) As String
        Dim va() As String
        Dim n As Integer
        Dim cTemp As String = ""
        Dim cTemp1 As String = ""

        If cCabangTemp = "00" Then
            GetQueryFilter = ""
            Exit Function
        End If
        If cJenisKantor <> "K" Then
            If cKantor = "" Then
                cKantor = GetBranch2(dTanggal, cCabangTemp) 'GetListKantor(cCabangTemp, dTanggal, lSaldoAwalNeraca)
            End If
            cListFilterKantor = cKantor
            cTemp = ""
            va = Split(cKantor, ",")
            For n = 0 To UBound(va)
                cTemp = String.Format("{0}'{1}',", cTemp, va(n))
            Next
            If Len(cTemp) > 1 Then
                cTemp1 = Left(cTemp, Len(cTemp) - 1)
            End If
            If cCabangTemp = "01" And cKantor = "" Then
                cTemp = ""
            Else
                cTemp = String.Format(" and {0} in ({1})", cField, cTemp1)
            End If
        Else
            cTemp = String.Format(" and {0} = '{1}'", cField, aCfg(eCfg.msKodeCabang))
        End If
        GetQueryFilter = cTemp
    End Function

    Private Function ListKantorPusat(ByVal dTgl As Date) As String
        Dim dbData As New DataTable
        Dim cRetVal As String = ""
        Dim cSQL As String = String.Format(" select kode from cabang_mutasi where tgl <= '{0}' and induk = '01'", formatValue(dTgl, formatType.yyyy_MM_dd))
        cSQL = cSQL & " and kode not in"
        cSQL = String.Format("{0} (select kode from cabang_mutasi where induk <> '01' and tgl <=  '{1}' )", cSQL, formatValue(dTgl, formatType.yyyy_MM_dd))
        cSQL = cSQL & " order by kode;"
        dbData = objData.SQL(GetDSN, cSQL)
        For n As Integer = 0 To dbData.Rows.Count - 1
            cRetVal = String.Format("{0}'{1}'", cRetVal, dbData.Rows(n).Item("Kode"))
            If dbData.Rows.Count > 0 Then
                cRetVal = cRetVal & ","
            End If
        Next
        dbData.Dispose()
        cRetVal = String.Format("({0})", cRetVal)
        ListKantorPusat = cRetVal
    End Function


    Private Function ListKantorCabang(ByVal dTgl As Date, ByVal cKodeCabang As String) As String
        Dim dbData As New DataTable
        Dim cRetVal As String = ""
        Dim cSQL As String = String.Format(" select * from cabang_mutasi where tgl <= '{0}' and induk = '{1}'", formatValue(dTgl, formatType.yyyy_MM_dd), cKodeCabang)
        cSQL = cSQL & " and tgl in"
        cSQL = String.Format("{0} (select tgl from cabang_mutasi where induk = '{1}' and tgl  <=  '{2}' )", cSQL, cKodeCabang, formatValue(dTgl, formatType.yyyy_MM_dd))
        cSQL = cSQL & " order by kode;"
        dbData = objData.SQL(GetDSN, cSQL)
        For n As Integer = 0 To dbData.Rows.Count - 1
            cRetVal = String.Format("{0}'{1}'", cRetVal, dbData.Rows(n).Item("Kode"))
            If dbData.Rows.Count > 0 Then
                cRetVal = cRetVal & ","
            End If
        Next
        dbData.Dispose()
        cRetVal = String.Format("({0})", cRetVal)
        ListKantorCabang = cRetVal
    End Function

    Public Function GetBranch(ByVal dTgl As Date, ByVal cBranch As String) As String
        If cBranch = "01" Then
            GetBranch = ListKantorPusat(dTgl)
        Else
            GetBranch = ListKantorCabang(dTgl, cBranch)
        End If
    End Function

    Public Function GetBranch2(ByVal dTgl As Date, ByVal cBranch As String, Optional ByVal lWithThick As Boolean = False) As String
        Dim cRetVal As String = ""
        Dim cRetVal1 As String = "''"
        Select Case cBranch
            Case "01"
                Select Case dTgl
                    Case Is >= DateSerial(2014, 1, 20)
                        cRetVal = "01,11,13,14,15,16,17,18,20,21,22,23,24,26,27"
                        cRetVal1 = "'01','11','13','14','15','16','17','18','20','21','22','23','24','26','27'"
                    Case Is >= DateSerial(2014, 1, 2)
                        cRetVal = "01,11,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27"
                        cRetVal1 = "'01','11','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27'"
                    Case Is > DateSerial(1990, 1, 1)
                        cRetVal = "01,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27"
                        cRetVal1 = "'01','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27'"

                    Case Else
                        Exit Select
                End Select
            Case "02"
                Select Case dTgl
                    Case Is >= DateSerial(2014, 1, 20)
                        cRetVal = "02,12,19,25,28,29,30,31"
                        cRetVal1 = "'02','12','19','25','28','29','30','31'"
                    Case Is >= DateSerial(2014, 1, 2)
                        cRetVal = "02,12"
                        cRetVal1 = "'02','12'"

                    Case Else
                        Exit Select
                End Select
            Case Else
                Exit Select
        End Select

        If lWithThick Then
            GetBranch2 = cRetVal1
        Else
            GetBranch2 = cRetVal
        End If
    End Function

    Function formatFilter(ByVal cFilter As String) As String
        Dim n As Integer
        Dim cF As String() = Split(cFilter, ",")
        Dim cRetVal As String = ""
        For n = 0 To UBound(cF)
            cRetVal = String.Format("{0}'{1}'", cRetVal, cF(n))
            If n < UBound(cF) Then
                cRetVal = cRetVal & ","
            End If
        Next
        formatFilter = cRetVal
    End Function

    Function GetSaldoSimpananRegister(ByVal dTgl As Date, ByVal cKode As String, Optional ByVal lPajakSaatJatuhTempo As Boolean = False,
                                      Optional ByVal dJthTmp As Date = #1/1/1900#) As Double
        Dim db As New DataTable
        Dim nSaldo As Double = 0
        Dim dTanggalcair As Date
        Dim cField As String
        Dim cWhere As String
        Dim db1 As New DataTable
        Dim dTglTemp As Date
        Dim cFieldCriteria As String
        Dim dbRegister As New DataTable

        If lPajakSaatJatuhTempo Then
            dTglTemp = dJthTmp
        Else
            dTglTemp = dTgl
        End If

        dbRegister = objData.Browse(GetDSN, "registernasabah", "kode,kodeinduk", "kodeinduk", , cKode)
        For n As Integer = 0 To dbRegister.Rows.Count - 1
            cFieldCriteria = "Kode"
            db = objData.Browse(GetDSN, "tabungan", , cFieldCriteria, , dbRegister.Rows(n).Item("Kode").ToString)
            For i As Integer = 0 To db.Rows.Count - 1
                nSaldo = nSaldo + GetSaldoTabungan(db.Rows(i).Item("Rekening").ToString, dTglTemp)
            Next

            db = objData.Browse(GetDSN, "deposito", "rekening,tglcair,tgl,status,nominal", cFieldCriteria, , dbRegister.Rows(n).Item("Kode").ToString)
            For i As Integer = 0 To db.Rows.Count - 1
                With db.Rows(i)
                    dTanggalcair = CDate(IIf(GetNull(.Item("TglCair"), "9999-12-31").ToString = "9999-99-99", "9999-12-31", .Item("TglCair")))
                    If CDate(.Item("Tgl")) <= dTgl Then
                        If Format(dTanggalcair, "yyyymm") >= Format(dTgl, "yyyymm") Then
                            nSaldo = nSaldo + CDbl(GetNull(.Item("Nominal")))
                            cField = ""
                            cField = "Sum(SetoranPlafond) as SetoranPlafond,Max(Tgl) as Tgl,Max(JTHTMP) as JT,"
                            cField = cField & "max(CaraPencairan) as CaraPencairan,Sum(PencairanPlafond) as PencairanPlafond"
                            cWhere = ""
                            cWhere = String.Format("and Rekening = '{0}' ", .Item("Rekening"))
                            cWhere = String.Format("{0}and date_format(Tgl,'%Y%m') <= date_format('{1}','%Y%m')", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
                            db1 = objData.Browse(GetDSN, "MutasiDeposito", cField, "CaraPencairan", , "E", cWhere)
                            If db1.Rows.Count > 0 Then
                                nSaldo = nSaldo + CDbl(GetNull(db1.Rows(0).Item("SetoranPlafond")))
                            End If
                        End If
                    End If
                End With
            Next
        Next
        db1.Dispose()
        db.Dispose()
        dbRegister.Dispose()
        GetSaldoSimpananRegister = nSaldo
    End Function
End Module

