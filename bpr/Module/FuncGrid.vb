﻿Imports DevExpress.XtraGrid
Imports DevExpress.XtraEditors.Repository
Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports System.Data
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraPrinting

Module FuncGrid
    ReadOnly objData As New data()

    Enum ePrintExport
        eXls = 0
        eXlsx = 1
        ePDF = 2
        eCsv = 3
    End Enum

    Sub GridColumnFormat(ByVal grid As DevExpress.XtraGrid.Views.Grid.GridView, ByVal cName As String, _
                     Optional ByVal nFormatType As DevExpress.Utils.FormatType = DevExpress.Utils.FormatType.None, _
                     Optional ByVal nFormatString As formatType = formatType.string_char, Optional ByVal nWidth As Integer = -1, _
                     Optional ByVal nAlignment As DevExpress.Utils.HorzAlignment = DevExpress.Utils.HorzAlignment.Default, _
                     Optional ByVal lEditable As Boolean = False, Optional ByVal cCaption As String = "")
        With grid
            .Columns(cName).DisplayFormat.FormatType = nFormatType
            .Columns(cName).DisplayFormat.FormatString = FormatValueDX(nFormatString)
            If cCaption <> "" Then
                .Columns(cName).Caption = cCaption
            End If
            If nWidth = -1 Then
                .Columns(cName).Width = .Columns(cName).GetBestWidth()
            ElseIf nWidth > -1 Then
                .Columns(cName).Width = nWidth
            End If
            .Columns(cName).AppearanceCell.TextOptions.HAlignment = nAlignment
            .Columns(cName).OptionsColumn.AllowEdit = lEditable
        End With
    End Sub

    Sub InitGrid(ByVal grid As DevExpress.XtraGrid.Views.Grid.GridView,
                 Optional ByVal lShowFooter As Boolean = False, Optional ByVal arrayFooter(,) As String = Nothing,
                 Optional ByVal lGrouping As Boolean = False, Optional ByVal arrayGroup() As String = Nothing,
                 Optional ByVal arrayGroupSummary(,) As String = Nothing, Optional ByVal lWordWrap As Boolean = True,
                 Optional ByVal nHeaderHeight As Integer = 0, Optional ByVal nColumnType As eGridRepoType = eGridRepoType.eNone,
                 Optional ByVal cComboBoxColumnName As String = "", Optional ByVal arrayDataComboBox() As String = Nothing,
                 Optional ByVal nKeyColumnLookUpEditor As Integer = 0, Optional ByVal nGridKeyColumn As Integer = 0,
                 Optional ByVal cTableLookUpEditor As String = "", Optional ByVal cFieldLookUpEditor As String = "",
                 Optional ByVal cWhereLookUpEditor As String = "", Optional ByVal cOrderLookUpEditor As String = "")
        With grid
            .OptionsView.ShowFooter = lShowFooter
            If lShowFooter And Not IsNothing(arrayFooter) Then
                For n As Integer = 0 To arrayFooter.GetUpperBound(0)
                    .Columns(arrayFooter(n, 0)).Summary.Add(DevExpress.Data.SummaryItemType.Sum, arrayFooter(n, 0), arrayFooter(n, 1))
                Next
            End If
            If lGrouping Then
                .ClearGrouping()
                For n As Integer = 0 To arrayGroup.GetUpperBound(0)
                    .Columns(arrayGroup(n).ToString).GroupIndex = n
                Next
                For n = 0 To arrayGroupSummary.GetUpperBound(0)
                    .GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, arrayGroupSummary(n, 0), .Columns(arrayGroupSummary(n, 0)), arrayGroupSummary(n, 1))
                Next
                .ExpandAllGroups()
                .OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleIfExpanded
                .VertScrollVisibility = Views.Base.ScrollVisibility.Always
                .HorzScrollVisibility = Views.Base.ScrollVisibility.Always
            End If
            .Appearance.HeaderPanel.Options.UseTextOptions = True
            If lWordWrap Then
                .Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            End If
            If nHeaderHeight = 0 Then
                .ColumnPanelRowHeight = 50
            End If
            .Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            .OptionsBehavior.SmartVertScrollBar = True
            .VertScrollVisibility = Views.Base.ScrollVisibility.Auto
            .HorzScrollVisibility = Views.Base.ScrollVisibility.Auto
            If nColumnType <> eGridRepoType.eNone Then
                Select Case nColumnType
                    Case eGridRepoType.eCheckBox
                        .OptionsSelection.MultiSelect = True
                        .OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect

                    Case eGridRepoType.eComboBox
                        Dim repoCombo As RepositoryItemComboBox = New RepositoryItemComboBox
                        For n As Integer = 0 To arrayDataComboBox.Count - 1
                            repoCombo.Items.Add(arrayDataComboBox(n).ToString)
                        Next
                        .Columns(cComboBoxColumnName).ColumnEdit = repoCombo
                    Case eGridRepoType.eLookUpEdit
                        Try
                            Dim cDataTable As New DataTable
                            cDataTable = objData.Browse(GetDSN, cTableLookUpEditor, cFieldLookUpEditor, , , , cWhereLookUpEditor, cOrderLookUpEditor)
                            Dim cKolom As String = cDataTable.Columns(nKeyColumnLookUpEditor).ColumnName
                            Dim repoLookUp As RepositoryItemLookUpEdit = New RepositoryItemLookUpEdit()
                            With repoLookUp
                                .DisplayMember = cKolom
                                .ValueMember = cKolom
                                .TextEditStyle = TextEditStyles.Standard
                                .NullText = ""
                                .BestFitMode = BestFitMode.BestFitResizePopup
                                .SearchMode = SearchMode.AutoFilter
                                .HotTrackItems = True
                                .AutoSearchColumnIndex = nKeyColumnLookUpEditor
                                .ValidateOnEnterKey = True
                                .DataSource = cDataTable
                            End With
                            .Columns(nGridKeyColumn).ColumnEdit = repoLookUp
                            cDataTable.Dispose()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    Case Else
                        Exit Select
                End Select
            End If
        End With
    End Sub

    Sub AddColumn(ByVal data As DataTable, ByVal name As String, ByVal type As Type)
        AddColumn(data, name, type, False)
    End Sub

    Sub AddColumn(ByVal data As DataTable, ByVal name As String, ByVal type As Type, ByVal ro As Boolean)
        Dim col As DataColumn = New DataColumn(name, type) With {.Caption = name, .ReadOnly = ro}
        data.Columns.Add(col)
    End Sub

    Sub ShowGridPreview(ByVal grid As GridControl, Optional ByVal lLandScape As Boolean = False, Optional ByVal lCustomPaper As Boolean = False,
                        Optional ByVal nPaperWidth As Integer = 0, Optional ByVal nPaperHeight As Integer = 0,
                        Optional ByVal nPaperSize As System.Drawing.Printing.PaperKind = System.Drawing.Printing.PaperKind.A4,
                        Optional ByVal nLeftMargin As Integer = 3, Optional ByVal nRightMargin As Integer = 3,
                        Optional ByVal nTopMargin As Integer = 3, Optional ByVal nBottomMargin As Integer = 3)
        ' Check whether the GridControl can be previewed.
        If Not grid.IsPrintingAvailable Then
            MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error")
            Return
        End If

        ' Opens the Preview window.
        'grid.ShowPrintPreview()
        Dim ps As New PrintingSystem()
        Dim link As New PrintableComponentLink(ps)
        With link
            .Component = grid
            If Not lCustomPaper Then
                .PaperKind = nPaperSize
            Else
                .PaperKind = System.Drawing.Printing.PaperKind.Custom
                .CustomPaperSize = New Size(nPaperWidth, nPaperHeight)
            End If
            .Landscape = lLandScape
            .Margins.Left = nLeftMargin
            .Margins.Right = nRightMargin
            .Margins.Top = nTopMargin
            .Margins.Bottom = nBottomMargin
            .ShowPreview()
        End With
    End Sub

    Sub exportReport(ByVal grid As DevExpress.XtraGrid.Views.Grid.GridView, Optional ByVal nExportType As ePrintExport = ePrintExport.eXlsx)
        Using savdlg As New SaveFileDialog
            Dim cFilter As String = ""
            Dim cFileFilter As String = ""
            Select Case nExportType
                Case ePrintExport.eXls
                    cFilter = "Excel Documents (*.xls)|"
                    cFileFilter = "xls"
                Case ePrintExport.eXlsx
                    cFilter = "Excel Documents (*.xlsx)|"
                    cFileFilter = "xlsx"
                Case ePrintExport.eCsv
                    cFilter = "CSV Documents (*.csv)|"
                    cFileFilter = "csv"
                Case ePrintExport.ePDF
                    cFilter = "PDF Documents (*.pdf)|"
                    cFileFilter = "pdf"
            End Select
            savdlg.Filter = cFilter
            savdlg.DefaultExt = cFileFilter
            savdlg.AddExtension = True
            If savdlg.ShowDialog() = DialogResult.OK Then
                MsgBox(savdlg.FileName)
                Select Case nExportType
                    Case ePrintExport.eXls
                        grid.ExportToXls(savdlg.FileName)
                    Case ePrintExport.eXlsx
                        grid.ExportToXlsx(savdlg.FileName)
                    Case ePrintExport.eCsv
                        grid.ExportToCsv(savdlg.FileName)
                    Case ePrintExport.ePDF
                        grid.ExportToPdf(savdlg.FileName)
                End Select
                MessageBox.Show("Export Success", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Using
    End Sub
End Module
