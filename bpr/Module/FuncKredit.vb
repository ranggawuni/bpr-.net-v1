﻿Imports bpr.MySQL_Data_Library

Module FuncKredit
    ReadOnly objData As New data()

    Public Structure TypeKolektibilitas
        Public nTunggakanPokok As Double
        Public nTunggakanBunga As Double
        Public nKolek As Integer
        Public nBakiDebet As Double
        Public nFrekuensi As Double
        Public nHariTunggakan As Double
        Public nBulanTunggakan As Double
        Public nSukuBunga As Double
        Public cJaminan As String
        Public nNilaiJaminan As Double
        Public nPlafond As Double
        Public dJthTmp As Date
        Public nKewajibanPokok As Double
        Public nKewajibanBunga As Double
        Public nPembayaranPokok As Double
        Public nPembayaranBunga As Double
        Public cAO As String
        Public nGracePeriod As Single
        Public nJadwalPembayaranPokok As Double
        Public nJadwalPembayaranBunga As Double
        Public cKodeJaminan As String
        Public nPPAPWD As Double
        Public cDinotariskan As String
        Public nTunggakanBungaSampaiDenganTanggal As Double
        Public cCaraPerhitungan As String
        Public cNamaDebitur As String
        Public dTglRealisasi As Date
        Public cRekeningTabungan As String
        Public dTglPelunasan As Date
        Public nFrekPokok As Double
        Public nFrekBunga As Double
        Public nProsentaseDenda As Double
        Public nTotalJaminan As Double
        Public nPlafondEfektif As Double
        Public nPersenPPAP As Single
        Public nTaksasiDiperhitungkan As Double
        Public nTotalHariTunggakan As Double
        Public cStatusPencairan As String
        Public nBulanJatuhTempo As Integer
        Public nHariJatuhTempo As Integer
        Public cJenisPengikatan As String
        Public lKolekRelasi As Boolean
        Public cKolekRelasi As String
        Public dTanggalAngsurTerakhir As Date
        Public nDenda As Double
    End Structure

    Public Structure TypeAccrualKredit
        Public nBungaAccrualHarian As Double
        Public nHariAccrual As Single
        Public nBungaJadwal As Double
        Public nPokokJadwal As Double
        Public nTotalHariAccrual As Single
        Public nAccrualBungaJadwal As Double
        Public nAccrualPokokJadwal As Double
        Public nBungaAccrualHarianJadwal As Double
        Public nSisaAccrual As Double
    End Structure

    Sub GetAccrualKredit(ByVal cRekening As String, ByVal dTgl As Date, tpAccrual As TypeAccrualKredit)
        Dim db As New DataTable
        Dim nPlafond As Double
        Dim nBakiTemp As Double
        Dim nLama As Integer
        Dim nKe As Integer
        Dim dTglBuka As Date
        Dim n As Integer
        Dim cCaraPerhitungan As String
        Dim nSukuBunga As Double

        Dim vaJadwal As New DataTable
        Dim vaKartuAngsuran As New DataTable

        Dim lFound As Boolean

        Dim nBungaAccrualHarian As Double
        Dim nPokokAccrual As Double
        Dim nBungaAccrual As Double
        Dim nJumlahHariAccrual As Integer
        Dim nHariAccrual As Integer
        Dim nAccrualBungaJadwal As Double
        Dim nAccrualPokokJadwal As Double
        Dim nBungaAccrualHarianJadwal As Double
        Dim nSisaBunga As Double
        Dim nSisaBunga1 As Double
        Dim nSisaBunga2 As Double

        With tpAccrual
            .nBungaAccrualHarian = 0
            .nHariAccrual = 0
            .nBungaJadwal = 0
            .nPokokJadwal = 0
            .nTotalHariAccrual = 0
            .nAccrualBungaJadwal = 0
            .nAccrualPokokJadwal = 0
            .nBungaAccrualHarianJadwal = 0
        End With

        Dim cField As String = "d.*,r.Nama"
        Dim cWhere As String = " and statuspencairan <> '0' Group by d.Rekening"
        Dim vaJoin() As Object = {"left join registerNasabah r on r.kode = d.kode"}
        db = objData.Browse(GetDSN, "debitur d", cField, "d.Rekening", , cRekening, cWhere, , vaJoin)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                cCaraPerhitungan = CStr(IIf(.Item("CaraPerhitungan").ToString = "", "0", .Item("CaraPerhitungan").ToString))
                '    If db!CaraPerhitungan = "7" Then ' jika RK
                '      nLamaTemp = GetLamaAddendum(cRekening, dTgl)
                '      nLama = GetNull(db!Lama) + nLamaTemp
                '    Else
                nLama = CInt(GetNull(.Item("Lama")))
                '    End If
                dTglBuka = CDate(GetNull(.Item("Tgl"), #1/1/1900#))
                nSukuBunga = CDbl(GetNull(.Item("SukuBunga")))

                nKe = CInt(DateDiff("m", dTglBuka, dTgl)) ' Ke maximal sejumlah Lama
                Dim dTglJatuhTempoBulanIni As Date = DateAdd("m", nKe, dTglBuka)
                If dTgl < dTglJatuhTempoBulanIni Then
                    nKe = nKe - 1
                End If
                nKe = CInt(Min(nKe, nLama))

                ' jika ada pelunasan sebelum jatuh tempo maka :
                ' - untuk flat : accrual dihitung dari bunga berjalan + bunga bulan berikutnya
                ' - untuk anuitas : accrual dihitung dari bunga berjalan + ( sukubunga per tahun / 12 * plafond)
                nHariAccrual = CInt(DateDiff("d", BOM(dTgl), EOM(dTgl)) + 1)
                Dim nCountHari As Integer = CInt(EOM(dTgl).Day - dTglBuka.Day)
                Dim dTglTemp As Date
                If nCountHari < 0 Then
                    dTglTemp = DateAdd("d", Math.Abs(nCountHari), DateAdd("m", 1, dTgl))
                Else
                    dTglTemp = DateAdd("m", 1, dTgl)
                End If
                nJumlahHariAccrual = nCountHari
                If nKe = nLama Then
                    If nCountHari < 0 Then
                        dTglTemp = DateAdd("d", Math.Abs(nCountHari), dTgl)
                    Else
                        dTglTemp = dTgl
                    End If
                End If

                cField = "Tgl,DPokok,DBunga,date_format(tgl,'%Y%m') as bulan"
                cWhere = String.Format("and status= {0} and tgl<='{1}'", eAngsuran.ags_jadwal, formatValue(DateAdd("m", 2, dTgl), formatType.yyyy_MM_dd))
                db = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening, cWhere, "Tgl")
                If db.Rows.Count > 0 Then
                    vaJadwal = db.Copy
                Else
                    vaJadwal = Nothing
                End If

                cField = "Tgl,KPokok,KBunga,date_format(tgl,'%Y%m') as bulan"
                cWhere = String.Format("and (status = {0} or status = {1}) ", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
                cWhere = String.Format("{0}and tgl <= '{1}'", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
                db = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening, cWhere, "Tgl")
                If db.Rows.Count > 0 Then
                    vaKartuAngsuran = db.Copy
                End If

                Dim nBaris As DataRow()
                '      If nCountHari < 0 And App.LogMode = 0 Then Stop
                If cCaraPerhitungan <> "7" Then ' jika bukan RK
                    ' mencari accrual untuk angsuran berjalan
                    ' jika jumlah bunga ditemukan dalam jadwal maka kredit masih aktif
                    ' jika jumlah bunga tidak ditemukan maka selanjutnya dicek apakah angsuran merupakan angsuran terakhir
                    ' karena jika angsuran terakhir / pelunasan maka ada rumus tersendiri untuk beberapa cara perhitungan
                    Dim cBulanTemp As String = Format(dTglTemp, "YYYYMM")
                    lFound = False
                    If vaJadwal.Rows.Count >= 0 Then
                        nBaris = vaJadwal.Select(String.Format("bulan = '{0}'", cBulanTemp))
                    Else
                        nBaris = Nothing
                    End If
                    If Not IsNothing(nBaris) Then
                        nPokokAccrual = CDbl(nBaris(1).ToString)
                        nBungaAccrual = CDbl(nBaris(2).ToString)
                        nBungaAccrualHarian = Math.Round(nBungaAccrual / nHariAccrual * nCountHari, 0)
                        nBungaAccrualHarian = Max(nBungaAccrualHarian, 0)
                        nSisaBunga = nBungaAccrual - nBungaAccrualHarian
                        lFound = True
                    End If
                    If Not lFound Then
                        nBakiTemp = GetBakiDebet(cRekening, dTgl)
                        If nBakiTemp <= 0 Then
                            If nKe = nLama Then ' jika jatuh tempo
                                If vaJadwal.Rows.Count - 1 >= 0 Then
                                    nPokokAccrual = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(1))
                                    nBungaAccrual = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(2))
                                    nBungaAccrualHarian = Math.Round(nBungaAccrual / nHariAccrual * nCountHari, 0)
                                    nBungaAccrualHarian = Max(nBungaAccrualHarian, 0)
                                    nSisaBunga = nBungaAccrual - nBungaAccrualHarian
                                End If
                            Else ' jika pelunasan
                                Dim dTglBerikutnya As Date
                                Dim nPokokAccrual1 As Double
                                Dim nBungaAccrual1 As Double
                                Dim nBungaAccrualHarian1 As Double
                                Dim nPokokAccrual2 As Double
                                Dim nBungaAccrual2 As Double
                                Dim nBungaAccrualHarian2 As Double
                                ' menghitung bunga accrual bulan sekarang
                                If vaJadwal.Rows.Count >= 0 Then
                                    nBaris = vaJadwal.Select(String.Format("bulan = '{0}'", cBulanTemp))
                                Else
                                    nBaris = Nothing
                                End If
                                If Not IsNothing(nBaris) Then
                                    nPokokAccrual1 = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(1))
                                    nBungaAccrual1 = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(2))
                                    nBungaAccrualHarian1 = Math.Round(nBungaAccrual1 / nHariAccrual * nCountHari, 0)
                                    nBungaAccrualHarian1 = Max(nBungaAccrualHarian1, 0)
                                    nSisaBunga1 = nBungaAccrual1 - nBungaAccrualHarian1
                                End If
                                ' menghitung bunga accrual bulan berikutnya
                                If Val(cCaraPerhitungan) = 1 Then ' flat
                                    dTglBerikutnya = DateAdd("m", 1, dTglTemp)
                                    cBulanTemp = Format(dTglBerikutnya, "YYYYMM")
                                    If vaJadwal.Rows.Count >= 0 Then
                                        nBaris = vaJadwal.Select(String.Format("bulan = '{0}'", cBulanTemp))
                                    Else
                                        nBaris = Nothing
                                    End If
                                    If Not IsNothing(nBaris) Then
                                        nPokokAccrual2 = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(1))
                                        nBungaAccrual2 = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(2))
                                        nBungaAccrualHarian2 = Math.Round(nBungaAccrual2 / nHariAccrual * nCountHari, 0)
                                        nBungaAccrualHarian2 = Max(nBungaAccrualHarian2, 0)
                                        nSisaBunga2 = nBungaAccrual2 - nBungaAccrualHarian2
                                    End If
                                ElseIf Val(cCaraPerhitungan) > 1 And Val(cCaraPerhitungan) < 7 Then ' non flat selain RK
                                    nBungaAccrual2 = (nSukuBunga / 100 / 12 * nPlafond)
                                    nBungaAccrualHarian2 = Math.Round(nBungaAccrual2 / nHariAccrual * nCountHari, 0)
                                    nBungaAccrualHarian2 = Max(nBungaAccrualHarian2, 0)
                                End If
                                nBungaAccrualHarian = nBungaAccrualHarian1 + nBungaAccrualHarian2
                                nSisaBunga = nSisaBunga1 + nSisaBunga2
                            End If
                        End If
                    End If

                    cBulanTemp = Format(dTgl, "YYYYMM")
                    If vaJadwal.Rows.Count >= 0 Then
                        nBaris = vaJadwal.Select(String.Format("bulan = '{0}'", cBulanTemp))
                    Else
                        nBaris = Nothing
                    End If
                    If Not IsNothing(nBaris) Then
                        nAccrualPokokJadwal = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(1))
                        nAccrualBungaJadwal = CDbl(vaJadwal.Rows(vaJadwal.Rows.Count - 1).Item(2))
                        nBungaAccrualHarianJadwal = Math.Round(nAccrualBungaJadwal / nHariAccrual * nCountHari, 0)
                        nBungaAccrualHarianJadwal = Max(nBungaAccrualHarianJadwal, 0)
                    End If

                    ' mengecek apakah pembayaran angsuran melebihi jadwal atau tidak.
                    ' jika melebihi jadwal maka tidak ada accrual
                    Dim nSelisihBunga As Double
                    Dim nJadwalBunga As Double = 0
                    Dim nBayarBunga As Double = 0
                    For n = 0 To vaJadwal.Rows.Count - 1
                        If CDate(vaJadwal.Rows(n).Item(0)) <= dTgl Then
                            nJadwalBunga = nJadwalBunga + CDbl(vaJadwal.Rows(n).Item(2))
                        End If
                    Next
                    For n = 0 To vaKartuAngsuran.Rows.Count - 1
                        If CDate(vaKartuAngsuran.Rows(n).Item(0)) <= dTgl Then
                            nBayarBunga = nBayarBunga + CDbl(vaKartuAngsuran.Rows(n).Item(2))
                        End If
                    Next
                    nSelisihBunga = nJadwalBunga - nBayarBunga
                    If nSelisihBunga < 0 Then
                        nSelisihBunga = CDbl(IIf(nSelisihBunga < 0, Math.Abs(nSelisihBunga), nSelisihBunga))
                        If nSelisihBunga > nBungaAccrualHarian Then
                            nBungaAccrualHarian = 0
                        End If
                    End If
                Else ' jika RK
                    ' untuk RK tidak ada perhitungan accrual
                    nBungaAccrual = 0
                    nBungaAccrualHarian = 0
                    nJumlahHariAccrual = 0
                    nHariAccrual = 0
                    nAccrualBungaJadwal = 0
                    nAccrualPokokJadwal = 0
                    nBungaAccrualHarianJadwal = 0
                    nSisaBunga = 0
                    nPokokAccrual = 0
                    '      If nCountHari > 0 Then
                    '        dTglAccrual = DateSerial(Year(dTgl), Month(dTgl), Day(dTglBuka))
                    '        nBungaAccrual = getDataBungaRK(cRekening, nSukuBunga, DateAdd("m", 1, dTglAccrual))
                    '        nBungaAccrualHarian = GetAccrualBungaRKLaporan(cRekening, nSukuBunga, dTglAccrual)
                    '        nBungaAccrualHarian = Max(nBungaAccrualHarian, 0)
                    '      End If
                End If
            End With
        End If

        With tpAccrual
            .nBungaAccrualHarian = nBungaAccrualHarian
            .nHariAccrual = nJumlahHariAccrual
            .nBungaJadwal = nBungaAccrual
            .nPokokJadwal = nPokokAccrual
            .nTotalHariAccrual = nHariAccrual
            .nAccrualBungaJadwal = nAccrualBungaJadwal
            .nAccrualPokokJadwal = nAccrualPokokJadwal
            .nBungaAccrualHarianJadwal = nBungaAccrualHarianJadwal
            .nSisaAccrual = nSisaBunga
        End With
    End Sub

    Sub GetTunggakan(ByVal cRekening As String, ByVal dTgl As Date, tpKolektibilitas As TypeKolektibilitas,
                     Optional lHitungUlangKolektibilitas As Boolean = True, Optional lHarian As Boolean = False,
                     Optional ByVal lCheckRekeningRelasi As Boolean = False, Optional lKolekIntern As Boolean = True,
                     Optional ByVal cKodeRegister As String = "", Optional ByVal lKolek0 As Boolean = False,
                     Optional ByVal lDenda As Boolean = False)
        Dim db As New DataTable
        Dim dbData1 As New DataTable
        Dim nPlafond As Double = 0
        Dim nLama As Double = 0
        Dim nKe As Double = 0
        Dim dTglBuka As Date
        Dim dTglTemp As Date
        Dim n As Integer = 0
        Dim cCaraPerhitungan As String = ""
        Dim nBakiDebet As Double = 0

        Dim nSukuBunga As Double = 0
        Dim nGracePeriod As Double = 0
        Dim dJthTmp As Date

        Dim nFrekuensiBunga As Double = 0
        Dim nFrekuensiPokok As Double = 0
        Dim nFrekuensiJTHTMP As Double = 0

        Dim nAngsuranPokok As Double = 0
        Dim nAngsuranBunga As Double = 0

        Dim nTotJadwalPokok As Double = 0
        Dim nTotJadwalBunga As Double = 0

        Dim nTunggakanPokok As Double = 0
        Dim nTunggakanBunga As Double = 0

        Dim nSisaPokok As Double = 0
        Dim nSisaBunga As Double = 0

        Dim vaAngsuran As New DataTable
        Dim cAO As String = ""

        Dim nKeAkhirAngsuran As Double = 0
        Dim nBulanTunggakan As Double = 0
        Dim nNilaiJaminan As Double = 0

        Const nKewajibanBungaJTHTMP As Double = 0
        Dim dTglPelunasan As Date
        Dim cRekeningTabungan As String = ""
        Dim cNama As String = ""
        Dim nBaki As Double = 0
        Dim nDenda As Double = 0
        Const cKodeJaminan As String = ""
        Dim X1 As TypeKolektibilitas = Nothing
        Dim nKolTerburuk As Single = -1

        Dim lKolekKhusus As Boolean
        Dim nKolek As String = ""
        Dim nTPokok As Double = 0
        Dim nTBunga As Double = 0
        Dim nFBulan As Single = 0
        Dim nFHari As Single = 0
        Dim nSelisihAkhirBulan As Single = 0
        Dim nTotalJaminan As Double = 0
        Dim dTglAngsurTerakhir As Date

        Dim lReschedule As Boolean = False
        Dim nDPokok As Double = 0
        Dim nDBunga As Double = 0
        Dim nPersentasePPAP As Single = 0
        Dim nJaminanDiperhitungkan As Double = 0
        Dim nTotalHariTunggakan As Double = 0
        Dim cStatusPencairan As String = ""
        Dim nBulanJatuhTempo As Integer = 0
        Dim lRelasiKolektibilitas As Boolean = False
        Dim cRelasiKolektibilitas As String = ""
        Dim cField As String = ""
        Dim cWhere As String = ""
        Dim vaJadwal As New DataTable
        Dim vaKartuAngsuran As New DataTable
        Dim nBulanTunggakanTemp As Double = 0

        Dim cSQL As String = ""

        ' Setup Awal Parameter
        lHitungUlangKolektibilitas = True
        If Not lHitungUlangKolektibilitas Then
            cWhere = String.Format(" and Kode = '{0}' and Tgl = '{1}'", cRekening, formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "TmpSaldo", , "Status", , "2", cWhere)
            If db.Rows.Count > 0 Then
                Dim va() As String = Split(db.Rows(0).Item("Keterangan").ToString, "~")
                With tpKolektibilitas
                    .nBakiDebet = CDbl(GetArray(va, 0, "0"))
                    .nBulanTunggakan = CDbl(GetArray(va, 1, "0"))
                    .nFrekuensi = CDbl(GetArray(va, 2, "0"))
                    .nHariTunggakan = CDbl(GetArray(va, 3, "0"))
                    .nKolek = CInt(GetArray(va, 4, "0"))
                    .nSukuBunga = CDbl(GetArray(va, 5, "0"))
                    .nTunggakanBunga = CDbl(GetArray(va, 6, "0"))
                    .nTunggakanPokok = CDbl(GetArray(va, 7, "0"))
                    .cJaminan = GetArray(va, 8, "")
                    .nNilaiJaminan = CDbl(GetArray(va, 9, "0"))
                    .nPlafond = CDbl(GetArray(va, 10, "0"))
                    .dJthTmp = CDate(GetArray(va, 11, formatValue(Date.Today, formatType.yyyy_MM_dd)))
                    .nKewajibanPokok = CDbl(GetArray(va, 12, "0"))
                    .nKewajibanBunga = CDbl(GetArray(va, 13, "0"))
                    .nPembayaranPokok = CDbl(GetArray(va, 14, "0"))
                    .nPembayaranBunga = CDbl(GetArray(va, 15, "0"))
                    .cAO = GetArray(va, 16, "")
                    .nGracePeriod = CSng(GetArray(va, 17, "0"))
                    .nJadwalPembayaranPokok = CDbl(GetArray(va, 18, "0"))
                    .nJadwalPembayaranBunga = CDbl(GetArray(va, 19, "0"))
                End With
            Else
                lHitungUlangKolektibilitas = True
            End If
        End If

        ' Kolektibilitas akan di hitung berdasarkan Kolektibilitas yang terburuk
        ' Untuk Kredit dengan Register yang sama
        If lCheckRekeningRelasi Then
            cField = ""
            cField = "d.Rekening,d.plafond,"
            cField = String.Format("{0}if(d.caraperhitungan<>'7', d.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = d.rekening AND (a.status = {1} or status = {2}) AND a.tgl <= '{3}'),0),", cField, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl, formatType.yyyy_MM_dd))
            cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = d.rekening and a.tgl <= '{1}'),0)) as bakidebet", cField, formatValue(dTgl, formatType.yyyy_MM_dd))
            cWhere = String.Format(" and d.Rekening <> '{0}'", cRekening)
            dbData1 = objData.Browse(GetDSN, "debitur d", cField, "d.Kode", , cKodeRegister, cWhere, , , "d.Rekening having bakidebet > 0")
            For n = 0 To dbData1.Rows.Count - 1
                Dim nBakiDebetTemp As Double = CDbl(GetNull(dbData1.Rows(0).Item("BakiDebet")))
                If nBakiDebetTemp > 0 And nKolTerburuk < 4 Then
                    GetTunggakan(dbData1.Rows(0).Item("Rekening").ToString, dTgl, X1, lHitungUlangKolektibilitas, , False)
                    nKolTerburuk = CSng(Max(nKolTerburuk, X1.nKolek))
                    lRelasiKolektibilitas = True
                    If cRelasiKolektibilitas = "" Then
                        cRelasiKolektibilitas = nKolTerburuk.ToString
                    Else
                        cRelasiKolektibilitas = String.Format("{0},{1}", cRelasiKolektibilitas, nKolTerburuk)
                    End If
                End If
            Next
        End If

        If lHitungUlangKolektibilitas Then
            With tpKolektibilitas
                .nTunggakanPokok = 0
                .nTunggakanBunga = 0
                .nFrekuensi = 0
                .nKolek = 0
                .nBakiDebet = 0
                .nHariTunggakan = 0
                .cJaminan = ""
                .nNilaiJaminan = 0
                .nPlafond = 0
                .dJthTmp = Date.Today
                .nKewajibanPokok = 0
                .nKewajibanBunga = 0
                .nPembayaranPokok = 0
                .nPembayaranBunga = 0
                .nJadwalPembayaranPokok = 0
                .nJadwalPembayaranBunga = 0
                .nGracePeriod = 1
                .cAO = ""
                .nTunggakanBungaSampaiDenganTanggal = 0
                .cCaraPerhitungan = "0"
                .nPlafondEfektif = 0
                .nBulanTunggakan = 0
                .cStatusPencairan = ""
                .cJenisPengikatan = ""
                .lKolekRelasi = False
                .cKolekRelasi = ""
                .nDenda = 0
            End With
            dTglPelunasan = CDate("9999-12-31")
            cSQL = "select d.*,r.nama,"
            cSQL = String.Format("{0}if(d.caraperhitungan<>'7', d.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = d.rekening AND (a.status = {1} or status = {2}) AND a.tgl <= '{3}'),0),", cSQL, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = d.rekening and a.tgl <= '{1}'),0)) as bakidebet,", cSQL, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = String.Format("{0}@jpokok := ifnull((select sum(dpokok) from angsuran where rekening='{1}' and status={2} and tgl<='{3}') ,0) as jadwalpokok,", cSQL, cRekening, eAngsuran.ags_jadwal, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = String.Format("{0}@jbunga := ifnull((select sum(dbunga) from angsuran where rekening='{1}' and status={2} and tgl<='{3}') ,0) as jadwalbunga,", cSQL, cRekening, eAngsuran.ags_jadwal, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = String.Format("{0}@apokok := ifnull((select sum(kpokok-dpokok) from angsuran where rekening='{1}' and (status={2} or status={3}) and tgl<='{4}') ,0) as angsuranpokok,", cSQL, cRekening, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = String.Format("{0}@abunga := ifnull((select sum(kbunga-dbunga) from angsuran where rekening='{1}' and (status={2} or status={3}) and tgl<='{4}') ,0) as angsuranbunga,", cSQL, cRekening, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl, formatType.yyyy_MM_dd))
            cSQL = cSQL & "@jpokok-@apokok as TunggakanPokok,@jbunga-@abunga as TunggakanBunga "
            cSQL = cSQL & "from debitur d "
            cSQL = cSQL & "Left join registernasabah r on r.kode = d.kode "
            cSQL = String.Format("{0}where d.rekening ='{1}' and statuspencairan<>'0' ", cSQL, cRekening)
            db = objData.SQL(GetDSN, cSQL)
            If db.Rows.Count > 0 Then
                With db.Rows(0)
                    If .Item("Kolektibilitas").ToString.Trim <> "" Then
                        lKolekKhusus = True
                        nKolek = .Item("Kolektibilitas").ToString
                        nTPokok = CDbl(GetNull(.Item("TunggakanPokok")))
                        nTBunga = CDbl(GetNull(.Item("TunggakanBunga")))
                        nFBulan = CSng(CDbl(GetNull(.Item("FrekuensiBulan"))))
                        nFHari = CSng(CDbl(GetNull(.Item("FrekuensiHari"))))
                    End If
                    nBakiDebet = CDbl(GetNull(.Item("BakiDebet")))
                    cStatusPencairan = GetNull(.Item("statuspencairan"), "").ToString
                    lReschedule = CBool(IIf(.Item("Reschedule").ToString = "Y", True, False))
                    cCaraPerhitungan = IIf(.Item("CaraPerhitungan").ToString = "", "0", .Item("CaraPerhitungan")).ToString
                    cRekeningTabungan = GetNull(.Item("RekeningTabungan"), "").ToString
                    nSelisihAkhirBulan = CSng(Max(Val(EOM(dTgl).Day) - Val(CDate(.Item("Tgl")).Day), 0))
                    cNama = GetNull(.Item("Nama"), "").ToString
                    cAO = GetNull(.Item("AO"), "").ToString
                    nPlafond = CDbl(GetNull(.Item("Plafond")))
                    nLama = CDbl(GetNull(.Item("Lama")))
                    dTglBuka = CDate(GetNull(.Item("Tgl"), #1/1/1900#))
                    nSukuBunga = CDbl(GetNull(.Item("SukuBunga")))
                    nTunggakanPokok = CDbl(GetNull(.Item("TunggakanPokok")))
                    nTunggakanBunga = CDbl(GetNull(.Item("TunggakanBunga")))
                    nAngsuranPokok = CDbl(GetNull(.Item("AngsuranPokok")))
                    nAngsuranBunga = CDbl(GetNull(.Item("AngsuranBunga")))

                    nKe = DateDiff("m", dTglBuka, dTgl) ' Ke maximal sejumlah Lama
                    Dim dTglJatuhTempoBulanIni As Date = DateAdd("m", nKe, dTglBuka)
                    If dTgl < dTglJatuhTempoBulanIni Then
                        nKe = nKe - 1
                    End If
                    nKe = Min(nKe, nLama)

                    dJthTmp = DateAdd("m", nLama, dTglBuka)
                    nBulanJatuhTempo = CInt(DateDiff("m", dTgl, dJthTmp))

                    cField = "Tgl,DPokok,DBunga,date_format(tgl,'%Y%m') as bulan"
                    cWhere = ""
                    cWhere = "and status= " & eAngsuran.ags_jadwal
                    cWhere = String.Format("{0} and tgl <= '{1}'", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
                    db = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening, cWhere, "Tgl")
                    If db.Rows.Count > 0 Then
                        vaJadwal = db.Copy
                    Else
                        vaJadwal = Nothing
                    End If

                    cField = "Tgl,sum(KPokok-DPokok),sum(KBunga-DBunga),date_format(tgl,'%Y%m') as bulan"
                    cWhere = ""
                    cWhere = String.Format("and (status = {0} or status = {1}) ", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
                    cWhere = String.Format("{0} and tgl <= '{1}'", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
                    db = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening, cWhere, "Tgl", , "tgl")
                    If db.Rows.Count > 0 Then
                        vaKartuAngsuran = db.Copy
                        dTglAngsurTerakhir = CDate(vaKartuAngsuran.Rows(vaKartuAngsuran.Rows.Count - 1).Item(0))
                    End If

                    ' Ambil Jadwal Angsuran
                    '      If cRekening = "01.71.000367.01" And App.LogMode = 0 Then Stop
                    AddColumn(vaAngsuran, "PokokJadwal", System.Type.GetType("System.Double"))
                    AddColumn(vaAngsuran, "BungaJadwal", System.Type.GetType("System.Double"))
                    AddColumn(vaAngsuran, "PokokAngsuran", System.Type.GetType("System.Double"))
                    AddColumn(vaAngsuran, "BungaAngsuran", System.Type.GetType("System.Double"))
                    AddColumn(vaAngsuran, "TglJadwal", System.Type.GetType("System.DateTime"))
                    For n = 0 To vaJadwal.Rows.Count - 1
                        vaAngsuran.Rows.Add(New Object() {0, 0, 0, 0, #1/1/1900#})
                    Next
                    For n = 0 To vaJadwal.Rows.Count - 1
                        With vaJadwal.Rows(n)
                            nDPokok = CDbl(.Item(1))
                            nDBunga = CDbl(.Item(2))
                            If nDPokok <> 0 Or nDBunga <> 0 Then
                                vaAngsuran.Rows(n).Item(0) = nDPokok
                                vaAngsuran.Rows(n).Item(1) = nDBunga
                                vaAngsuran.Rows(n).Item(4) = CDate(.Item(0))
                                nTotJadwalPokok = nTotJadwalPokok + nDPokok
                                nTotJadwalBunga = nTotJadwalBunga + nDBunga
                            End If
                        End With
                    Next

                    n = 0
                    nSisaPokok = Max(nAngsuranPokok, 0)
                    nSisaBunga = Max(nAngsuranBunga, 0)
                    '      if cRekening = "01.73.000107.01" and app.logmode=0 then stop
                    nBaki = nPlafond
                    For n = 0 To vaKartuAngsuran.Rows.Count - 1
                        nBaki = nBaki - CDbl(vaKartuAngsuran.Rows(n).Item(1))
                        If nBaki = 0 Then
                            dTglPelunasan = CDate(vaKartuAngsuran.Rows(n).Item(0))
                            Exit For
                        End If
                    Next

                    Dim nSelisihTemp As Double
                    'Dim nPersenTemp As Double
                    Dim nSelisihBungaTemp As Double
                    'Dim nPersenBungaTemp As Double
                    'Dim nPembayaranKe As Double
                    Dim nPorsiPokok As Double, nPorsiBunga As Double

                    n = 0
                    Do While n <= vaAngsuran.Rows.Count - 1
                        With vaAngsuran.Rows(n)
                            nPorsiPokok = Min(CDbl(.Item(0)), nSisaPokok)
                            nPorsiBunga = Min(CDbl(.Item(1)), nSisaBunga)
                            .Item(2) = (nPorsiPokok)
                            .Item(3) = (nPorsiBunga)
                            nSisaPokok = nSisaPokok - nPorsiPokok
                            nSisaBunga = nSisaBunga - nPorsiBunga
                            n = n + 1
                        End With
                    Loop

                    ' hitung frekuensi
                    ' frekuensi dikurangi 1 jika tunggakan angsuran pokok atau angsuran bunga selisihnya kurang dari 50%
                    nFrekuensiPokok = 0
                    nFrekuensiBunga = 0
                    For n = 0 To vaAngsuran.Rows.Count - 1
                        With vaAngsuran.Rows(n)
                            nSelisihTemp = Val(.Item(0)) - Val(.Item(2))
                            'If nSelisihTemp > 0 Then nPersenTemp = (nSelisihTemp / .Item(0)) * 100
                            If nSelisihTemp <> 0 Then
                                nFrekuensiPokok = nFrekuensiPokok + 1
                                'If nPersenTemp < 50 Then nFrekuensiPokok = nFrekuensiPokok - 1
                            End If
                            nSelisihBungaTemp = Val(.Item(1)) - Val(.Item(3))
                            'If nSelisihBungaTemp > 0 Then nPersenBungaTemp = (nSelisihBungaTemp / .Item(1)) * 100
                            If nSelisihBungaTemp <> 0 Then
                                nFrekuensiBunga = nFrekuensiBunga + 1
                                'If nPersenBungaTemp < 50 Then nFrekuensiBunga = nFrekuensiBunga - 1
                            End If
                        End With
                    Next

                    nFrekuensiBunga = nFrekuensiBunga + 1
                    nFrekuensiPokok = nFrekuensiPokok + 1
                End With
            End If
            nBulanTunggakan = Max(Max(nFrekuensiBunga, nFrekuensiPokok) - 1, 0)
            nKeAkhirAngsuran = nKe - nBulanTunggakan

            ' Jika Sudah Jatuh Tempo
            ' kredit RK tidak ada tunggakan bunga dan tunggakan pokok muncul saat sudah jatuh tempo
            If dTgl >= dJthTmp Then
                nFrekuensiJTHTMP = DateDiff("M", dJthTmp, dTgl)

                nFrekuensiPokok = nFrekuensiPokok + nFrekuensiJTHTMP
                nFrekuensiBunga = nFrekuensiBunga + nFrekuensiJTHTMP

                nFrekuensiJTHTMP = nFrekuensiJTHTMP + 1
            End If

            ' jika sudah jatuh tempo dan angsuran pokok atau angsuran bunga 0 maka frekuensi pokok atau bunga jadi 0
            If dTgl >= dJthTmp Then
                If nTunggakanPokok = 0 Then nFrekuensiPokok = 0
                If nTunggakanBunga = 0 Then nFrekuensiBunga = 0
            End If

            nBulanTunggakanTemp = 0
            ' cek status ayda
            With tpKolektibilitas
                .lKolekRelasi = lRelasiKolektibilitas
                .cStatusPencairan = cStatusPencairan
                .dTglRealisasi = dTglBuka
                .cCaraPerhitungan = cCaraPerhitungan
                .cNamaDebitur = cNama
                .cRekeningTabungan = cRekeningTabungan
                .dTglPelunasan = dTglPelunasan
                .nFrekPokok = nFrekuensiPokok
                .nFrekBunga = nFrekuensiBunga
                .cAO = cAO
                .nBakiDebet = nBakiDebet
                '      If CekDataAYDA(cRekening, dTgl) Then .nBakiDebet = 0
                .nTunggakanBunga = Max(nTunggakanBunga + nKewajibanBungaJTHTMP, 0)
                .nTunggakanPokok = Max(nTunggakanPokok, 0)
                nBulanTunggakanTemp = Max(nFrekuensiPokok, nFrekuensiBunga)
                .nFrekuensi = Max(nFrekuensiPokok - 1, nFrekuensiBunga - 1)
                .nBulanTunggakan = .nFrekuensi
                .nHariTunggakan = -1
                .nHariJatuhTempo = -1
                .nGracePeriod = CSng(nGracePeriod)
                nTotalHariTunggakan = 0
                nDenda = 0
                If lDenda = True Then
                    JadwalDenda(cRekening, .dTglRealisasi, dTgl, .cCaraPerhitungan, .nPlafond, , , nDenda)
                End If
                .nDenda = nDenda
                Do While .nHariTunggakan < 0
                    nBulanTunggakanTemp = Max(nBulanTunggakanTemp - 1, 0)
                    .nFrekBunga = Max(.nFrekBunga - 1, 0)
                    .nFrekPokok = Max(.nFrekPokok - 1, 0)
                    dTglTemp = DateAdd("m", nBulanTunggakanTemp + nKe - nBulanTunggakan, dTglBuka)
                    .nHariTunggakan = DateDiff("d", dTglTemp, dTgl) + 1
                    dTglTemp = DateAdd("m", nKe - nBulanTunggakanTemp, dTglBuka)
                    nTotalHariTunggakan = DateDiff("d", dTglTemp, dTgl) + 1
                    .nTotalHariTunggakan = nTotalHariTunggakan
                Loop
                '      .nBulanTunggakan = nBulanTunggakanTemp
                .nPlafond = nPlafond
                .dJthTmp = CDate(formatValue(dJthTmp, formatType.yyyy_MM_dd))
                .nKewajibanPokok = nTotJadwalPokok
                .nKewajibanBunga = nTotJadwalBunga
                .nPembayaranPokok = nAngsuranPokok
                .nPembayaranBunga = nAngsuranBunga
                .nJadwalPembayaranPokok = nTotJadwalPokok
                .nJadwalPembayaranBunga = nTotJadwalBunga
                .dTanggalAngsurTerakhir = dTglAngsurTerakhir

                '      If .nGracePeriod > 1 And .nTunggakanPokok > 5000 Then
                '        nFrekuensiJTHTMP = Max(nFrekuensiJTHTMP, 1)
                '      End If

                If .nFrekuensi > 12 Or nFrekuensiJTHTMP >= 3 Then
                    .nKolek = 4
                ElseIf .nFrekuensi >= 7 Or nFrekuensiJTHTMP >= 2 Then
                    .nKolek = 3
                ElseIf .nFrekuensi >= 4 Or nFrekuensiJTHTMP >= 1 Then
                    .nKolek = 2
                Else
                    .nKolek = 1
                End If

                If Not lKolekIntern Then
                    If .nFrekuensi > 12 Or nFrekuensiJTHTMP >= 3 Then
                        .nKolek = 4
                        If .nFrekuensi = 13 And nFrekuensiJTHTMP < 3 Then
                            .nKolek = 3
                        End If

                    ElseIf .nFrekuensi >= 7 Or nFrekuensiJTHTMP >= 2 Then
                        .nKolek = 3
                        If .nFrekuensi = 7 And nFrekuensiJTHTMP < 2 Then
                            .nKolek = 2
                        End If
                    ElseIf .nFrekuensi >= 4 Or nFrekuensiJTHTMP >= 1 Then
                        .nKolek = 2
                        If .nFrekuensi = 4 And nFrekuensiJTHTMP = 0 Then
                            .nKolek = 1
                        End If
                    Else
                        .nKolek = 1
                    End If
                    If .dJthTmp <= dTgl And .nKolek <= 1 Then
                        .nKolek = CInt(Min(.nKolek + 1, 4))
                    End If
                End If

                If lKolekKhusus = True Then
                    .nKolek = CInt(nKolek)
                    If nTPokok <> 0 Or nTBunga <> 0 Then
                        .nTunggakanPokok = nTPokok
                        .nTunggakanBunga = nTBunga
                        .nBulanTunggakan = nFBulan
                        .nHariTunggakan = nFHari
                    End If
                End If

                .cKolekRelasi = String.Format("{0},{1}", .nKolek, cRelasiKolektibilitas)

                If nKolTerburuk > .nKolek Then
                    .nKolek = CInt(nKolTerburuk)
                End If

                nTotalJaminan = GetNilaiJaminan(cRekening, nNilaiJaminan, nPersentasePPAP, nJaminanDiperhitungkan, .nKolek, .nPlafond)
                .nTotalJaminan = nTotalJaminan
                .cKodeJaminan = cKodeJaminan
                .nPPAPWD = Max(.nBakiDebet - nNilaiJaminan, 0)
                If .nKolek = 0 Or .nKolek = 1 Then
                    .nPPAPWD = .nBakiDebet * 0.005
                ElseIf .nKolek = 2 Then
                    .nPPAPWD = .nPPAPWD * 0.1
                ElseIf .nKolek = 3 Then
                    .nPPAPWD = .nPPAPWD * 0.5
                End If

                If aCfg(eCfg.msPembulatanPPAP, "0").ToString = "1" Then
                    .nPPAPWD = Mod1000(.nPPAPWD, True) * 1000
                End If

                .nPPAPWD = Math.Round(.nPPAPWD, 0)
                .nNilaiJaminan = nNilaiJaminan
                .nPersenPPAP = nPersentasePPAP
                .nTaksasiDiperhitungkan = nJaminanDiperhitungkan

                ' Jika Lunas maka Tunggakan dan Kolektibilitas sudah 0
                If .nBakiDebet = 0 Then
                    .nKolek = 1
                    .nTunggakanBunga = 0
                    .nTunggakanPokok = 0
                End If

                ' Jika Tunggakan Pokok & Tunggakan Bunga = 0 maka Frekuensi hari & bulan di nol ken
                If .nTunggakanBunga = 0 And .nTunggakanPokok = 0 Then
                    .nHariTunggakan = 0
                    .nBulanTunggakan = 0
                    .nFrekuensi = 0
                    If lKolek0 = True Then
                        .nKolek = 0
                    End If
                End If

                ' Jika belum Jatuh Tempo
                ' kredit RK tidak ada tunggakan bunga dan tunggakan pokok muncul saat sudah jatuh tempo
                ' dan kolektibilitasnya masih tetap 1
                If dTgl < dJthTmp And .cCaraPerhitungan = "7" Then
                    .nTunggakanBunga = 0
                    .nTunggakanPokok = 0
                    .nKolek = 1
                End If

                .nPlafondEfektif = .nPlafond - .nKewajibanPokok
                .nPlafondEfektif = Max(.nPlafondEfektif, 0)
            End With
        End If
        vaJadwal.Dispose()
    End Sub

    Function GetNilaiJaminan(ByVal cRekening As String, ByRef nTaksasi As Double, Optional ByRef nPersentase As Single = 0,
                             Optional ByRef nDiperhitungkan As Double = 0, Optional ByVal nKolek As Integer = 0,
                             Optional ByVal nPlafond As Double = 0) As Double
        Dim db As New DataTable
        Dim nNilaiJaminan As Double = 0
        Dim nNilaiJaminan1 As Double
        Dim nNilaiJaminanSebelumDiPerhitungankan As Double

        nTaksasi = 0
        Const cField As String = "a.Kode,a.NilaiJaminan,a.NilaiNJOP,a.NilaiPengikatan,g.Diperhitungkan"
        Dim vaJoin() As Object = {"Left Join gagunan g on a.Kode = g.Kode"}
        db = objData.Browse(GetDSN, "Agunan a", cField, "a.Rekening", , cRekening, , "a.id", vaJoin)
        For n As Integer = 0 To db.Rows.Count - 1
            With db.Rows(n)
                nNilaiJaminan1 = CDbl(GetNull(.Item("NilaiJaminan")))
                ' diremark karena ada perubahan di form pengikatan jaminan
                '    If db!Kode = "02" Then ' jika sertifikat (SKMHT)
                '      nNilaiJaminan1 = GetNull(db!NilaiNJOP)
                '    ElseIf db!Kode = "18" Then ' jika sertifikat (APHT)
                '      nNilaiJaminan1 = GetNull(db!NilaiPengikatan)
                '    End If
                If nNilaiJaminan1 = 0 Then nNilaiJaminan = CDbl(GetNull(.Item("NilaiJaminan")))
                If CDbl(.Item("Kode")) = 24 Then
                    nNilaiJaminan1 = 0
                End If
                'ditambahkan tgl 16-01-2014
                'sesuai dengan permintaan direksi bank jombang karena ada kesalahan entry data nilai jaminan maka untuk :
                'kol 1 BPKB 10% dari plafond
                'kol 1 SHM 20% dari plafond
                'kol 2 dicermati dan dipilah2 (alias dihitung manual via excel)
                'kol 3 dan 4 data apa adanya
                Dim cKodeTemp As String = .Item("Kode").ToString
                If nKolek = 1 Then
                    If cKodeTemp = "02" Or cKodeTemp = "18" Or cKodeTemp = "23" Or cKodeTemp = "19" Then
                        nNilaiJaminan1 = 20 / 100 * nPlafond
                    ElseIf cKodeTemp = "01" Or cKodeTemp = "17" Or cKodeTemp = "16" Then
                        nNilaiJaminan1 = 10 / 100 * nPlafond
                    End If
                End If
                nNilaiJaminan = nNilaiJaminan + nNilaiJaminan1
                nNilaiJaminanSebelumDiPerhitungankan = nNilaiJaminanSebelumDiPerhitungankan + nNilaiJaminan1
                nPersentase = CSng(GetNull(.Item("Diperhitungkan"), 0))
                nDiperhitungkan = (nNilaiJaminan1 * nPersentase / 100)
                nTaksasi = nTaksasi + nDiperhitungkan
            End With
        Next
        db.Dispose()
        GetNilaiJaminan = nNilaiJaminanSebelumDiPerhitungankan
    End Function

    Function JadwalDenda(ByVal cRekening As String, ByVal dTanggalPK As Date, ByVal dTglDenda As Date, ByVal cCaraPerhitungan As String,
                          ByVal nPlafond As Double, Optional ByRef nDenda As Double = 0, Optional ByRef nDendadibayar As Double = 0,
                          Optional ByRef nDendaSisa As Double = 0) As DataTable
        Dim n As Integer = 0
        Dim i As Integer = 0
        Dim lBayar As Boolean
        Dim xArray As New DataTable
        Dim nTotalBunga As Double = 0
        Dim nTotalPokok As Double = 0
        Dim dbData As New DataTable
        Dim row As DataRow

        AddColumn(xArray, "No", System.Type.GetType("System.Int32"))
        AddColumn(xArray, "TglJadwal", System.Type.GetType("System.DateTime"))
        AddColumn(xArray, "PokokJadwal", System.Type.GetType("System.Double"))
        AddColumn(xArray, "BungaJadwal", System.Type.GetType("System.Double"))
        AddColumn(xArray, "DendaJadwal", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Tunggakan", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Hari", System.Type.GetType("System.Int32"))
        AddColumn(xArray, "TglBayar", System.Type.GetType("System.DateTime"))
        AddColumn(xArray, "PokokBayar", System.Type.GetType("System.Double"))
        AddColumn(xArray, "BungaBayar", System.Type.GetType("System.Double"))
        AddColumn(xArray, "DendaBayar", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Saldo", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Tgl", System.Type.GetType("System.DateTime"))
        AddColumn(xArray, "Selisih", System.Type.GetType("System.Double"))
        Const cField As String = "Tgl,DPokok,DBunga,KPokok,KBunga,Denda"
        Dim cWhere As String = String.Format("and (status={0} or status={1})", eAngsuran.ags_jadwal, eAngsuran.ags_Angsuran)
        dbData = objData.Browse(GetDSN, "angsuran", cField, "Rekening", , cRekening, cWhere, "Tgl,Dpokok desc,Dbunga desc")
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    If CDbl(.Item("DPokok")) > 0 Or CDbl(.Item("DBunga")) > 0 Then
                        If xArray.Rows(n).Item(8).ToString <> "" And CDate(xArray.Rows(n).Item(8)) = CDate(.Item("Tgl")) And CDbl(xArray.Rows(n).Item(9)) = 0 And CDbl(xArray.Rows(n).Item(10)) = 0 And CDbl(xArray.Rows(n).Item(11)) = 0 Then
                            xArray.Rows(n).Delete()
                        End If
                        row = xArray.NewRow()
                        i = i + 1
                        row(0) = n
                        row(1) = CDate(.Item("Tgl"))
                        row(2) = String.Format("Angs Ke {0} / {1}", i, .Item("Tgl").ToString)
                        row(3) = CDbl(.Item("DPokok"))
                        row(4) = CDbl(.Item("DBunga"))
                        row(5) = (0) 'Denda
                        row(6) = CDbl(xArray.Rows(n - 1).Item(6)) + CDbl(.Item("DPokok")) + CDbl(.Item("DBunga")) 'Tunggakn
                        row(7) = (0) 'Hari
                        row(8) = ""
                        row(9) = (0) 'Pokok
                        row(10) = (0) 'Bunga
                        row(11) = (0) 'byr Denda
                        row(12) = (0) 'Saldo
                        row(13) = CDate(.Item("Tgl"))
                        xArray.Rows(n - 1).Item(7) = DateDiff("d", CDate(xArray.Rows(n - 1).Item(13)), CDate(row(13)))
                        nTotalPokok = nTotalPokok + CDbl(row(3))
                        nTotalBunga = nTotalBunga + CDbl(row(4))
                        xArray.Rows.Add(row)
                        lBayar = False
                    Else
                        If xArray.Rows(n).Item(8).ToString <> "" And CDbl(xArray.Rows(n).Item(9)) = 0 And CDbl(xArray.Rows(n).Item(10)) = 0 And CDbl(xArray.Rows(n).Item(11)) = 0 Then
                            xArray.Rows(n).Delete()
                        End If
                        row = xArray.NewRow()
                        row(0) = n
                        row(1) = "" '(dbData!Tgl)
                        row(2) = "" '"Angs Ke " & n & " / " & (dbData!Tgl)
                        row(3) = (0) '(dbData!DPokok)
                        row(4) = (0) '(dbData!DBunga)
                        row(5) = (0) 'Denda
                        row(6) = CDbl(xArray.Rows(n - 1).Item(6)) - CDbl(.Item("KPokok")) - CDbl(.Item("KBunga")) 'Tunggakn
                        row(7) = (0) 'Hari
                        row(8) = CDate(.Item("Tgl"))
                        row(9) = CDbl(.Item("KPokok")) 'Pokok
                        row(10) = CDbl(.Item("KBunga")) 'Bunga
                        row(11) = CDbl(.Item("Denda")) 'byr Denda
                        row(12) = (0) 'Saldo
                        row(13) = CDate(.Item("Tgl"))
                        xArray.Rows(n - 1).Item(7) = DateDiff("d", CDate(xArray.Rows(n - 1).Item(13)), CDate(row(13)))
                        lBayar = True
                    End If
                    Dim dTglTemp As Date = DateAdd("m", -1, dTglDenda)
                    Dim dTglTemp1 As Date = DateAdd("m", -1, CDate(xArray.Rows(n).Item(13)))
                    Dim dTglTemp2 As Date = CDate(xArray.Rows(n).Item(13))
                    If Between(dTglTemp, dTglTemp1, dTglTemp2) And lBayar = False Then
                        row = xArray.NewRow()
                        row(0) = n
                        row(1) = "" '(dbData!Tgl)
                        row(2) = "" '"Angs Ke " & n & " / " & (dbData!Tgl)
                        row(3) = (0) '(dbData!DPokok)
                        row(4) = (0) '(dbData!DBunga)
                        row(5) = (0) 'Denda
                        row(6) = CDbl(xArray.Rows(n - 1).Item(6)) - (0) - (0) 'Tunggakn
                        row(7) = (0) 'Hari
                        row(8) = (dTglDenda)
                        row(9) = (0) 'Pokok
                        row(10) = (0) 'Bunga
                        row(11) = (0) 'byr Denda
                        row(12) = (0) 'Saldo
                        row(13) = (DateAdd("m", 0, dTglDenda))
                        xArray.Rows(n - 1).Item(7) = DateDiff("d", CDate(xArray.Rows(n - 1).Item(13)), CDate(dTglDenda))
                    End If
                End With
            Next
        End If
        If cCaraPerhitungan <> "7" Then
            xArray.Rows(0).Item(12) = nPlafond
        Else
            xArray.Rows(0).Item(12) = GetBakiDebet(cRekening, dTanggalPK)
        End If
        For n = 1 To xArray.Rows.Count - 1
            xArray.Rows(n).Item(12) = CDbl(xArray.Rows(n - 1).Item(12)) - CDbl(xArray.Rows(n).Item(9))
        Next
        For n = 1 To xArray.Rows.Count - 1
            If CDbl(xArray.Rows(n).Item(6)) > 0 Then
                If CDbl(xArray.Rows(n).Item(7)) > 2 Then
                    If formatValue(xArray.Rows(n).Item(13), formatType.yyyy_MM_dd) <= formatValue(DateSerial(dTglDenda.Year, dTglDenda.Month, dTanggalPK.Day), formatType.yyyy_MM_dd) Then 'FormatValue(dTglDenda, formatType.yyyy_MM_dd) Then '
                        xArray.Rows(n).Item(5) = 0.15 / 100 * CDbl(xArray.Rows(n).Item(7)) * CDbl(xArray.Rows(n).Item(6))
                    End If
                End If
            End If
            If n > 0 Then
                If formatValue(xArray.Rows(n).Item(13), formatType.yyyy_MM_dd) <= formatValue(dTglDenda, formatType.yyyy_MM_dd) Then  'FormatValue(DateSerial(Year(dTglDenda), Month(dTglDenda), Day(dTanggalPK)), formatType.yyyy_MM_dd) Then '
                    xArray.Rows(n).Item(14) = CDbl(xArray.Rows(n - 1).Item(14)) + CDbl(xArray.Rows(n).Item(5)) - CDbl(xArray.Rows(n).Item(11))
                    nDenda = nDenda + CDbl(xArray.Rows(n).Item(5))
                    nDendadibayar = nDendadibayar + CDbl(xArray.Rows(n).Item(11))
                End If
            End If
        Next
        If xArray.Rows.Count >= 0 Then
            xArray.Rows(0).Item(7) = (0)
        End If
        xArray.Rows.Item(13).Delete()

        nDenda = Math.Round(nDenda, 0)
        nDendaSisa = nDenda - nDendadibayar
        JadwalDenda = xArray
        dbData.Dispose()
        xArray.Dispose()
    End Function

    Function GetArray(ByVal va() As String, ByVal nCol As Double, ByVal cDefault As String) As String
        If va.Count >= nCol And nCol >= 0 Then
            GetArray = va(CInt(nCol))
        Else
            GetArray = cDefault
        End If
    End Function

    Function GetBakiDebet(ByVal cRekening As String, ByVal dTgl As Date, _
                           Optional ByVal lKartuAngsuran As Boolean = False, _
                           Optional ByVal cFaktur As String = "", Optional ByVal nID As Double = 0) As Double
        Dim nSaldo As Double = 0
        Dim nIDTemp As Double = 0
        Dim cCaraPerhitungan As String = ""
        Dim nPlafond As Double = 0
        Dim cField As String = ""
        Dim cWhere As String = ""
        Dim cDataTable As New DataTable
        cDataTable = objData.Browse(GetDSN, "debitur", "caraperhitungan,plafond", "rekening", , cRekening)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cCaraPerhitungan = .Item("caraperhitungan").ToString
                nPlafond = CDbl(.Item("plafond").ToString)
            End With
        End If

        If cCaraPerhitungan = "7" Then
            If nID = 0 Then
                cField = "tgl,sum(debet-kredit) as jumlah"
                cWhere = String.Format(" and tgl <= '{0}' group by rekening", formatValue(dTgl, formatType.yyyy_MM_dd))
            Else
                cField = "tgl,sum(debet-kredit) as jumlah"
                cWhere = String.Format(" and tgl <= '{0}' and id <= {1} group by rekening", formatValue(dTgl, formatType.yyyy_MM_dd), nID)
            End If
            cDataTable = objData.Browse(GetDSN, "mutasitabungan", cField, "rekening", , cWhere)
            If cDataTable.Rows.Count > 0 Then
                Return CDbl(cDataTable.Rows(0).Item("jumlah").ToString)
            End If
        Else
            cField = ""
            cField = "d.plafond,d.RekeningTabungan,d.CaraPerhitungan,"
            cField = String.Format("{0}ifnull((select Sum(a.KPokok-a.DPokok) from angsuran a where a.rekening = d.rekening and (a.status={1} or a.status={2}) and a.tgl <= '{3}'),0) as Saldo ", cField, CInt(eAngsuran.ags_Angsuran), CInt(eAngsuran.ags_Koreksi), formatValue(dTgl, formatType.yyyy_MM_dd))
            cWhere = " and d.statuspencairan<>0"
            If lKartuAngsuran Then
                cDataTable = objData.Browse(GetDSN, "angsuran", "id", "Faktur", , cFaktur)
                If cDataTable.Rows.Count > 0 Then
                    nIDTemp = CDbl(cDataTable.Rows(0).Item("id").ToString)
                Else
                    nIDTemp = 0
                End If
                cField = ""
                cField = "d.plafond,d.RekeningTabungan,d.CaraPerhitungan,"
                cField = String.Format("{0}ifnull(d.plafond - (select Sum(a.KPokok-a.DPokok) from angsuran a where a.rekening = d.rekening and (a.status={1} or a.status={2}) and a.tgl <= '{3}' and a.ID <= {4}),0) as Saldo ", cField, CInt(eAngsuran.ags_Angsuran), CInt(eAngsuran.ags_Koreksi), formatValue(dTgl, formatType.yyyy_MM_dd), nIDTemp)
            End If
            cDataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", , cRekening, cWhere, , , "d.rekening")
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    nSaldo = CDbl(.Item("saldo").ToString)
                End With
            End If
            cDataTable.Dispose()
            Return nSaldo
        End If
    End Function

    Sub updRekOverDraft(ByVal cFaktur As String)
        ' hapus data lama
        Dim cWhere As String = String.Format("and Faktur = '{0}'", cFaktur)
        objData.Delete(GetDSN, "BukuBesar", "status", , eTrigger.msAngsuranKredit.ToString, cWhere)
        Dim cField As String = ""
        cField = "a.Rekening,a.status,a.Tgl,a.saldo,r.Nama as NamaDebitur,"
        cField = cField & "a.UserName,a.DateTime,g.Rekening as RekeningPokok,a.kas,a.ID"
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening", _
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode", _
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        Dim cDataTable As New DataTable
        cDataTable = objData.Browse(GetDSN, "overdraft", cField, "a.faktur", , cFaktur, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                Dim nID As Integer = CInt(.Item("ID").ToString)
                Dim cRekKredit As String = GetNull(.Item("RekeningPokok").ToString, "").ToString
                Dim cRekOverDraft As String = aCfg(eCfg.msRekeningOverdraft).ToString
                Dim nBakiDebet As Double = CDbl(.Item("Saldo").ToString)
                Dim cKeterangan As String = ""
                Dim cCabang As String = Left(.Item("Rekening").ToString, 2)
                Dim dTgl As Date = CDate(.Item("Tgl").ToString)
                Dim cWaktu As String = GetNull(.Item("DateTime").ToString, SNow).ToString
                Dim cNamaUser As String = GetNull(.Item("UserName").ToString, "").ToString
                If .Item("Status").ToString = "Y" Then
                    cKeterangan = "Masuk Overdraft Kredit an. " & GetNull(.Item("NamaDebitur").ToString, "").ToString
                    ' jika masuk OverDraft
                    ' overdraft    xxxxxxx            ' bakidebet
                    '     pokok           xxxxxxx     ' bakidebet
                    ' Debet
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekOverDraft, cKeterangan, nBakiDebet, , cWaktu, cNamaUser)
                    'Kredit
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekKredit, cKeterangan, , nBakiDebet, cWaktu, cNamaUser)
                ElseIf .Item("Status").ToString = "T" Then
                    cKeterangan = "Pembayaran Overdraft an. " & GetNull(.Item("NamaDebitur").ToString, "").ToString
                    If .Item("Kas").ToString = "K" Then
                        cRekKredit = aCfg(eCfg.msKodeKas).ToString
                    ElseIf .Item("Kas").ToString = "P" Then
                        cRekKredit = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    ElseIf .Item("Kas").ToString = "D" Then
                        cRekKredit = GetNull(.Item("RekeningPokok").ToString, "").ToString
                    End If
                    ' jika bayar OverDraft
                    ' Pokok        xxxxxxx            ' bakidebet
                    '     Overdraft       xxxxxxx     ' bakidebet
                    ' Debet
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekKredit, cKeterangan, nBakiDebet, , cWaktu, cNamaUser)
                    'Kredit
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekOverDraft, cKeterangan, , nBakiDebet, cWaktu, cNamaUser)
                End If
            End With
        End If
        cDataTable.Dispose()
    End Sub

    Sub UpdPencairanRekeningKoran(ByVal cFaktur As String, ByVal cRekening As String, ByVal nPenarikan As Double, _
                                  Optional ByVal cNow As String = "")
        Dim cSQL As String = ""
        cSQL = cSQL & " Select m.*, t.Kode, k.Kas, k.Rekening as RekeningKodeTransaksi,"
        cSQL = cSQL & " k.Keterangan as KeteranganKodeTransaksi,t.hapusbuku,t.TglHapusBuku,t.Tgl as TglBuka,"
        cSQL = cSQL & " g.Rekening as RekeningPerkiraanTabungan,r.Nama as NamaNasabah,g.RekeningBunga,g.RekeningAccrual"
        cSQL = cSQL & " From MutasiTabungan m"
        cSQL = cSQL & " Left Join Debitur t on m.Rekening = t.Rekening"
        cSQL = cSQL & " Left Join GolonganKredit g on g.Kode = t.GolonganKredit"
        cSQL = cSQL & " Left Join KodeTransaksi k on k.Kode = m.KodeTransaksi"
        cSQL = cSQL & " Left Join RegisterNasabah r on r.Kode = t.Kode"
        cSQL = String.Format("{0} Where m.Faktur='{1}'", cSQL, cFaktur)
        Dim cDataTable As DataTable = objData.SQL(GetDSN, cSQL)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                Dim nID As Integer = CInt(.Item("ID").ToString)
                Dim nPokok As Double = CDbl(.Item("Jumlah").ToString)
                Dim cRekKas As String = ""
                Const nAccrual As Double = 0
                'GetAccrualKredit(.Item("Rekening").ToString, dTglAkhir, x)
                'nAccrual = X.nBungaAccrualHarian
                ' KAS          xxxxxxx            ' Pokok
                '     Pokok           xxxxxxx     ' Pokok
                Select Case .Item("KasRK").ToString
                    Case Is = "K"
                        cRekKas = aCfg(eCfg.msKodeKas).ToString
                    Case Is = "P"
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    Case Is = "T"
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    Case Else
                        Exit Select
                End Select
                If .Item("KodeTransaksi").ToString = aCfg(eCfg.msKodeBungaRekeningKoran).ToString Then
                    cRekKas = .Item("RekeningKodeTransaksi").ToString
                End If

                Dim lHapusBuku As Boolean = False
                If CInt(.Item("HapusBuku").ToString) = 1 And CDate(.Item("TglHapusBuku").ToString) = CDate(.Item("tgl").ToString) Then
                    lHapusBuku = True
                    cRekKas = aCfg(eCfg.msRekeningHapusBuku).ToString
                End If
                Dim cDebet As String
                Dim cKredit As String
                If .Item("DK").ToString = "K" Then
                    cDebet = cRekKas
                    cKredit = GetNull(.Item("RekeningPerkiraanTabungan").ToString, "").ToString
                Else
                    cDebet = GetNull(.Item("RekeningPerkiraanTabungan").ToString, "").ToString
                    cKredit = cRekKas
                End If
                Dim cKeterangan As String
                If lHapusBuku Then
                    cKeterangan = "Hapus Buku Pinjaman an. " & .Item("NamaNasabah").ToString
                Else
                    cKeterangan = .Item("Keterangan").ToString
                End If
                Dim cCabang As String = Left(.Item("Rekening").ToString, 2)
                Dim cWaktu As String = GetNull(.Item("DateTime").ToString, SNow).ToString
                Dim cNamaUser As String = GetNull(.Item("UserName").ToString, "").ToString
                Dim dTanggal As Date = CDate(GetNull(.Item("Tgl").ToString, "").ToString)
                UpdBukuBesar(nID, cCabang, cFaktur, dTanggal, cDebet, cKeterangan, nPokok, , cWaktu, cNamaUser)

                'mencari mutasi pembayaran sebelumnya pada bulan yang sama
                Dim nPembayaranSebelumnya As Double
                Dim dTglTemp As Date = BOM(CDate(.Item("tgl").ToString))
                Const cField As String = "sum(debet-kredit) as saldo"
                Dim cWhere As String = ""
                cWhere = String.Format("and tgl>='{0}' and tgl <='{0}'", formatValue(dTglTemp, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0} and kodetransaksi='{1}'", cWhere, aCfg(eCfg.msKodeBungaRekeningKoran))
                Dim cDataTableMutasi As DataTable = objData.Browse(GetDSN, "mutasitabungan", cField, "rekening", , cRekening, cWhere)
                If cDataTableMutasi.Rows.Count > 0 Then
                    nPembayaranSebelumnya = CDbl(GetNull(cDataTable.Rows(0).Item("Saldo"), "").ToString)
                Else
                    nPembayaranSebelumnya = 0
                End If
                cWhere = ""
                cWhere = String.Format("and tgl>='{0}' and tgl <='{0}'", formatValue(dTglTemp, formatType.yyyy_MM_dd))
                cDataTableMutasi = objData.Browse(GetDSN, "mutasitabungan", cField, "rekening", , cRekening, cWhere)
                If cDataTableMutasi.Rows.Count > 0 Then
                    nPembayaranSebelumnya = nPembayaranSebelumnya + CDbl(GetNull(cDataTable.Rows(0).Item("Saldo"), "").ToString)
                End If
                Dim nAccrualTemp As Double = nAccrual - nPembayaranSebelumnya
                Dim nBayarBunga As Double = nPokok - nAccrualTemp
                ' posisi kredit
                If nAccrual <> 0 And .Item("KodeTransaksi").ToString = "12" Then
                    If nBayarBunga < 0 Then
                        nBayarBunga = 0
                        nAccrualTemp = nPokok
                    End If
                    UpdBukuBesar(nID, cCabang, cFaktur, dTanggal, cKredit, cKeterangan, , nBayarBunga, cWaktu, cNamaUser)
                    UpdBukuBesar(nID, cCabang, cFaktur, dTanggal, .Item("RekeningAccrual").ToString, cKeterangan, , nAccrualTemp, cWaktu, cNamaUser)
                Else
                    UpdBukuBesar(nID, cCabang, cFaktur, dTanggal, cKredit, cKeterangan, , nPokok, cWaktu, cNamaUser)
                End If
            End With
        End If
    End Sub

    Public Function HitungAnuitas(ByVal dTgl As Date, ByVal nPlafond As Double, ByVal nSukuBunga As Double, ByVal nLama As Integer,
                                  Optional ByRef nJumlahBunga As Double = 0, Optional ByRef nJumlahPokok As Double = 0) As DataTable
        Dim n As Integer
        Dim nTotalAngsuran As Double
        Dim nPokok As Double = nPlafond
        Dim vaArray As New DataTable

        ' kolom
        ' 1 --> Ke
        ' 2 --> JTHTMP
        ' 3 --> Bunga
        ' 4 --> Pokok
        ' 5 --> Jumlah
        ' 6 --> SisaBunga
        ' 7 --> SisaPokok

        AddColumn(vaArray, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(vaArray, "JthTmp", System.Type.GetType("System.DateTime"))
        AddColumn(vaArray, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(vaArray, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(vaArray, "Jumlah", System.Type.GetType("System.Double"))
        AddColumn(vaArray, "SisaPokok", System.Type.GetType("System.Double"))
        AddColumn(vaArray, "SisaBunga", System.Type.GetType("System.Double"))
        For n = 0 To nLama
            vaArray.Rows.Add(New Object() {n, #1/1/1900#, 0, 0, 0, 0})
        Next
        nJumlahPokok = 0
        nJumlahBunga = 0
        For n = 0 To nLama
            With vaArray
                .Rows(n).Item(1) = DateAdd("m", n, dTgl)
                If n = 0 Then
                    .Rows(n).Item(2) = 0
                    .Rows(n).Item(3) = 0
                    .Rows(n).Item(4) = 0
                Else
                    nTotalAngsuran = Math.Round((nPokok * (nSukuBunga / 100 / 12)) / (1 - 1 / (1 + nSukuBunga / 100 / 12) ^ nLama))
                    .Rows(n).Item(4) = nTotalAngsuran 'TOTAL ANGSURAN
                    .Rows(n).Item(2) = Math.Round(Val(.Rows(n - 1).Item(6)) * nSukuBunga / 100 / 12) 'BUNGA
                    .Rows(n).Item(3) = Val(.Rows(n).Item(4)) - Val(.Rows(n).Item(2)) 'POKOK
                    nJumlahPokok = nJumlahPokok + Val(.Rows(n).Item(3))
                    nJumlahBunga = nJumlahBunga + Val(.Rows(n).Item(2))
                End If
                If n = 0 Then
                    .Rows(n).Item(6) = nPokok
                    .Rows(n).Item(5) = nPokok * nSukuBunga / 100 / 12 * nLama
                Else
                    .Rows(n).Item(6) = Val(.Rows(n - 1).Item(6)) - Val(.Rows(n).Item(3))
                    .Rows(n).Item(5) = Val(.Rows(n - 1).Item(5)) - Val(.Rows(n).Item(2))
                End If
                If n = nLama Then
                    If Val(.Rows(n).Item(6)) <> 0 Then
                        .Rows(n).Item(3) = Val(.Rows(n).Item(3)) + Val(.Rows(n).Item(6))
                        nJumlahPokok = nJumlahPokok + Val(.Rows(n).Item(6))
                        .Rows(n).Item(6) = 0
                    End If
                    If Val(.Rows(n).Item(5)) <> 0 Then
                    End If
                    .Rows(n).Item(4) = Val(.Rows(n).Item(2)) + Val(.Rows(n).Item(3))
                End If
            End With
        Next
        vaArray.Rows(0).Item(5) = nJumlahBunga
        For n = 1 To vaArray.Rows.Count - 1
            With vaArray
                .Rows(n).Item(5) = Val(.Rows(n - 1).Item(5)) - Val(.Rows(n).Item(2))
                .Rows(n).Item(7) = Val(.Rows(n).Item(5)) + Val(.Rows(n).Item(6))
            End With
        Next
        HitungAnuitas = vaArray
    End Function

    Function GetJadwal(ByVal dTgl As Date, ByVal nPlafond As Double, ByVal nLama As Integer, ByVal nSukuBunga As Single,
                       ByVal cCaraPerhitungan As String, Optional ByVal nSkimPokok As Double = 1, Optional ByVal nSkimBunga As Double = 1,
                       Optional ByVal nPembulatan As Double = 50, Optional ByRef nAngsuranPerBulan As Double = 0,
                       Optional ByVal nSukuBungaBerikutnya As Single = 0, Optional ByRef nTotBunga As Double = 0) As DataTable
        Dim n As Integer
        Dim dTanggal As Date
        Dim nTotalBunga As Double = 0
        Dim nTotalPokok As Double = 0
        Dim nBakiDebet As Double
        Dim nAngsuranPokok As Double
        Dim nAngsuranBunga As Double
        Dim xArray As New DataTable
        Dim nJumlah As Double
        Dim nTotalPokokBerjalan As Double = 0
        Dim nTotalBungaBerjalan As Double = 0

        ' kolom
        ' 1 --> Ke
        ' 2 --> JTHTMP
        ' 3 --> Bunga
        ' 4 --> Pokok
        ' 5 --> Jumlah
        ' 6 --> Bunga
        ' 7 --> Pokok
        ' 8 --> Total

        If cCaraPerhitungan = "4" Then 'ANUITAS
            GetJadwal = HitungAnuitas(dTgl, nPlafond, nSukuBunga, nLama, nTotBunga)
            Exit Function
        End If

        xArray.Reset()
        AddColumn(xArray, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(xArray, "JthTmp", System.Type.GetType("System.DateTime"))
        AddColumn(xArray, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Jumlah", System.Type.GetType("System.Double"))
        AddColumn(xArray, "SisaBunga", System.Type.GetType("System.Double"))
        AddColumn(xArray, "SisaPokok", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Total", System.Type.GetType("System.Double"))
        For n = 0 To nLama
            xArray.Rows.Add(New Object() {n, #1/1/1900#, 0, 0, 0, 0, 0, 0})
        Next

        dTanggal = (DateAdd("m", 1, dTgl))
        nBakiDebet = nPlafond
        nAngsuranPokok = GetAngsuranPokokJadwal(nPlafond, cCaraPerhitungan, nLama, 1, nBakiDebet, nSukuBunga, nPembulatan)
        nAngsuranBunga = GetAngsuranBungaJadwal(cCaraPerhitungan, nSukuBunga, nLama, 1, nBakiDebet, nPlafond, nPembulatan)
        nJumlah = nAngsuranPokok + nAngsuranBunga
        nJumlah = GetPembulatan(nJumlah, nPembulatan)
        For n = 1 To nLama
            With xArray
                .Rows(n).Item(0) = n
                .Rows(n).Item(1) = DateAdd("m", n, dTgl)
                .Rows(n).Item(3) = GetAngsuranPokokJadwal(nPlafond, cCaraPerhitungan, nLama, n, nBakiDebet, nSukuBunga, nPembulatan, nTotalPokokBerjalan)
                nBakiDebet = nBakiDebet - Val(GetNull(.Rows(n - 1).Item(3)))
                If n > 12 And nSukuBungaBerikutnya > 0 Then
                    .Rows(n).Item(2) = GetAngsuranBungaJadwal(cCaraPerhitungan, nSukuBungaBerikutnya, nLama, n, nBakiDebet, nPlafond, nPembulatan)
                Else
                    .Rows(n).Item(2) = GetAngsuranBungaJadwal(cCaraPerhitungan, nSukuBunga, nLama, n, nBakiDebet, nPlafond, nPembulatan, nTotalBungaBerjalan)
                End If
                If (cCaraPerhitungan = "5" Or cCaraPerhitungan = "6" Or cCaraPerhitungan = "7") And n > 1 Then
                    .Rows(n).Item(3) = GetAngsuranPokokJadwal(nPlafond, cCaraPerhitungan, nLama, n, nBakiDebet, nSukuBunga, nPembulatan)
                End If
                nTotalPokokBerjalan = nTotalPokokBerjalan + Val(.Rows(n).Item(3))
                nTotalBungaBerjalan = nTotalBungaBerjalan + Val(.Rows(n).Item(2))
                nAngsuranPokok = Val(.Rows(n).Item(3))
                nAngsuranBunga = Val(.Rows(n).Item(2))
                nJumlah = nAngsuranPokok + nAngsuranBunga
                .Rows(n).Item(4) = nJumlah
                nTotalBunga = nTotalBunga + Val(.Rows(n).Item(2))
                nTotalPokok = nTotalPokok + Val(.Rows(n).Item(3))
                dTanggal = (DateAdd("m", 1, CDate(.Rows(n).Item(1))))
            End With
        Next

        xArray.Rows(0).Item(5) = nTotalBunga
        xArray.Rows(0).Item(6) = nPlafond
        xArray.Rows(0).Item(1) = dTgl
        For n = 1 To xArray.Rows.Count - 1
            With xArray
                .Rows(n).Item(5) = Val(.Rows(n - 1).Item(5)) - Val(.Rows(n).Item(2))
                .Rows(n).Item(6) = Val(IIf(IsNothing(.Rows(n - 1).Item(6)) = True, 0, .Rows(n - 1).Item(6))) - Val(.Rows(n).Item(3))
                .Rows(n).Item(7) = Val(.Rows(n).Item(5)) + Val(.Rows(n).Item(6))
            End With
        Next

        If xArray.Rows.Count > 0 Then
            If cCaraPerhitungan = "5" Then
                nAngsuranPerBulan = Val(xArray.Rows(2).Item(4))
            Else
                nAngsuranPerBulan = Val(xArray.Rows(1).Item(4))
            End If
        End If

        Dim nJumlahBaris As Integer = xArray.Rows.Count - 1
        With xArray
            .Rows(nJumlahBaris).Item(3) = Val(.Rows(nJumlahBaris).Item(3)) + Val(.Rows(nJumlahBaris).Item(6))
            .Rows(nJumlahBaris).Item(4) = Val(.Rows(nJumlahBaris).Item(4)) + Val(.Rows(nJumlahBaris).Item(6))
            If .Rows.Count = nLama And Val(.Rows(nJumlahBaris).Item(6)) <> 0 Then
                .Rows(nJumlahBaris).Item(6) = Val(.Rows(nJumlahBaris - 1).Item(6)) + Val(.Rows(nJumlahBaris).Item(6))
                .Rows(nJumlahBaris).Item(6) = 0
            End If
            If xArray.Rows.Count = nLama And Val(.Rows(nJumlahBaris).Item(7)) <> 0 Then
                .Rows(nJumlahBaris).Item(7) = Val(.Rows(nJumlahBaris - 1).Item(7)) + Val(.Rows(nJumlahBaris).Item(7))
                .Rows(nJumlahBaris).Item(7) = 0
            End If

            Dim a As Double = 0
            Dim b As Double = 0
            If nSkimPokok = 0 Then nSkimPokok = 1
            If nSkimBunga = 0 Then nSkimBunga = 1
            For n = 1 To .Rows.Count - 1
                If n Mod nSkimPokok <> 0 Then
                    a = a + Val(.Rows(n).Item(3))
                    .Rows(n).Item(3) = 0
                Else
                    .Rows(n).Item(3) = Val(.Rows(n).Item(3)) + a
                    a = 0
                End If
                If n Mod nSkimBunga <> 0 Then
                    b = b + Val(.Rows(n).Item(2))
                    .Rows(n).Item(2) = 0
                Else
                    .Rows(n).Item(2) = Val(.Rows(n).Item(2)) + b
                    b = 0
                End If
                If Val(.Rows(n).Item(3)) = 0 And Val(.Rows(n).Item(2)) = 0 Then
                    .Rows(n).Item(4) = 0
                Else
                    .Rows(n).Item(4) = Val(.Rows(n).Item(3)) + Val(.Rows(n).Item(2))
                End If
                .Rows(n).Item(5) = Val(.Rows(n - 1).Item(5)) - Val(.Rows(n).Item(2))
                .Rows(n).Item(6) = Val(.Rows(n - 1).Item(6)) - Val(.Rows(n).Item(3))
                .Rows(n).Item(7) = Val(.Rows(n).Item(5)) + Val(.Rows(n).Item(6))
            Next
        End With
        GetJadwal = xArray
    End Function

    Function GetAngsuranPokokJadwal(ByVal nPlafondKredit As Double, ByVal cCaraPerhitungan As String, ByVal nLamaKredit As Integer,
                                    ByVal nKe As Integer, ByVal nBakiDebet As Double, ByVal nSukuBunga As Single,
                                    Optional ByVal nPembulatan As Double = 50, Optional ByVal nTotalPokokBerjalan As Double = 0) As Double
        Dim nRetval As Double
        Dim n As Double
        Dim nTotAngsuran As Double
        Dim nBunga As Double
        Dim nStatus As DueDate
        Dim nAngsuran As Double

        ' Flat & Sliding
        If cCaraPerhitungan = "1" Or cCaraPerhitungan = "3" Then
            nRetval = Math.Round(Devide(nPlafondKredit, nLamaKredit), 0)
            If nKe = nLamaKredit Then
                nTotAngsuran = 0
                For n = 1 To nLamaKredit - 1
                    nTotAngsuran = nTotAngsuran + nRetval
                Next
                nRetval = nPlafondKredit - nTotAngsuran
            End If
        ElseIf cCaraPerhitungan = "8" Then ' anuitas
            nAngsuran = GetTotalAngsuranAnuitas(nSukuBunga / 100, nPlafondKredit, nLamaKredit)
            If nPembulatan = 0 Then
                nTotAngsuran = Math.Round(nAngsuran, 0)
            Else
                nTotAngsuran = GetPembulatan(nAngsuran, nPembulatan)
            End If
            If nKe = nLamaKredit Then
                nRetval = nPlafondKredit - nTotalPokokBerjalan
            Else
                nBunga = GetAngsuranBungaJadwal(cCaraPerhitungan, nSukuBunga, nLamaKredit, nKe, nBakiDebet, nPlafondKredit, nPembulatan)
                nRetval = nTotAngsuran - nBunga
            End If
            nRetval = Math.Round(nRetval)
        ElseIf cCaraPerhitungan = "5" Or cCaraPerhitungan = "6" Then ' 5= advance, 6=arrear
            nRetval = 0
            If cCaraPerhitungan = "5" Then
                nStatus = DueDate.BegOfPeriod
            Else
                nStatus = DueDate.EndOfPeriod
            End If
            nTotAngsuran = Financial.Pmt(Val(nSukuBunga) / 100 / 12, Val(nLamaKredit), -nPlafondKredit, 0, nStatus)
            nBunga = Math.Round(nBakiDebet * nSukuBunga / 1200, 0)
            nBunga = GetPembulatan(nBunga, nPembulatan)
            nTotAngsuran = GetPembulatan(Math.Round(nTotAngsuran, 0), nPembulatan)
            If nKe = 1 And cCaraPerhitungan = "5" Then
                nBunga = 0
            End If
            If nKe = 1 And cCaraPerhitungan = "5" Then
                nRetval = GetPembulatan(nTotAngsuran - nBunga, nPembulatan)
            Else
                nRetval = nTotAngsuran - nBunga
            End If
            If nKe = nLamaKredit Then
                nRetval = nBakiDebet
            End If
        Else ' Reguler
            If nKe >= nLamaKredit Then
                nRetval = nBakiDebet
            Else
                nRetval = 0
            End If
        End If
        GetAngsuranPokokJadwal = GetPembulatan(nRetval, nPembulatan)
    End Function

    Function GetAngsuranBungaJadwal(ByVal cCaraHitung As String, ByVal nSukuBunga As Single, ByVal nLamaKredit As Integer,
                                    ByVal nKe As Single, ByVal nBakiDebet As Double, ByVal nPlafondKredit As Double,
                                    Optional ByVal nPembulatan As Double = 50, Optional ByVal nTotalBungaBerjalan As Double = 0) As Double
        Dim nRetval As Double
        Dim nTotAgsBunga As Double
        Dim n As Double
        Dim nAngsuran As Double
        Dim nPersBunga As Single
        Dim nBunga As Double
        Dim nBungaEfektif As Double
        Dim nTotBunga As Double = Math.Round(nPlafondKredit * nSukuBunga / 100 / 12 * nLamaKredit, 0)
        If cCaraHitung = "1" Then      ' Bunga Flat
            nRetval = Math.Round(Devide(nTotBunga, nLamaKredit), 0)
            If nKe = nLamaKredit Then
                nTotAgsBunga = 0
                For n = 1 To nLamaKredit - 1
                    nTotAgsBunga = nTotAgsBunga + nRetval
                Next
                nRetval = nTotBunga - nTotAgsBunga
            End If
        ElseIf cCaraHitung = "8" Then ' anuitas
            nBungaEfektif = BungaEfektif(nPersBunga, nPlafondKredit, nTotBunga, nLamaKredit)
            nBungaEfektif = nBungaEfektif / 12 / 100
            nAngsuran = GetAnuitas(nBungaEfektif, nPlafondKredit, nLamaKredit)
            If nKe < nLamaKredit Then
                For n = 1 To nLamaKredit - 1
                    nBunga = Math.Round(nBungaEfektif * nPlafondKredit, 0)
                    'nBunga = Mod50(nBunga)
                    nPlafondKredit = nPlafondKredit - (nAngsuran - nBunga)
                    If n = nKe Then
                        nRetval = nBunga
                        Exit For
                    End If
                Next
            Else
                nBunga = nTotBunga - nTotalBungaBerjalan
            End If
            nRetval = Math.Round(nBunga, 0)
        Else
            nRetval = Math.Round(nBakiDebet * nSukuBunga / 1200, 0)
            If cCaraHitung = "5" Or cCaraHitung = "6" Then
                nRetval = GetPembulatan(nRetval, nPembulatan)
            End If
            If cCaraHitung = "5" And nKe = 1 Then
                nRetval = 0
            End If
        End If
        GetAngsuranBungaJadwal = GetPembulatan(nRetval, nPembulatan)
    End Function

    Function GetTotalAngsuranAnuitas(ByVal nSukuBunga As Single, _
                                     ByVal nPlafond As Double, ByVal nLama As Single) As Double
        Dim a As Double = nSukuBunga * nLama * nPlafond
        Dim b As Double = a / 12
        Dim c As Double = b + nPlafond
        Dim d As Double
        If nLama > 0 Then
            d = c / nLama
        Else
            d = 0
        End If
        GetTotalAngsuranAnuitas = d
    End Function

    Function GetSukuBungaEfektifKredit(ByVal nTotalAngsuran As Double, ByVal nLama As Single, _
                                       ByVal nPlafond As Double) As Single
        Dim a As Double

        If nLama > 0 Then
            a = Financial.Rate(nLama, nTotalAngsuran, -nPlafond) * 12
        Else
            a = 0
        End If
        GetSukuBungaEfektifKredit = CSng(a) * 100
    End Function

    Function BungaEfektif(ByVal nSukuBungaPA As Double, ByVal nPlafond As Double, Optional ByVal nBunga As Double = 0,
                          Optional ByVal nLama As Integer = 0) As Double
        Dim nAngsuranPA As Double
        Dim nEfektif As Double = 50
        Dim nSkip As Double = 50
        Dim nAngsuranEfektif As Double
        Dim nEfektifBulanan As Double
        If nBunga = 0 Then
            nBunga = Math.Round(nPlafond * nSukuBungaPA / 100 / 12 * nLama, 0)
        End If
        nAngsuranPA = Math.Round(((nPlafond + nBunga) / nLama), 1)
        Do While nAngsuranPA <> nAngsuranEfektif
            nEfektifBulanan = Devide(Devide(nEfektif, 12), 100)
            nAngsuranEfektif = Math.Round(GetAnuitas(nEfektifBulanan, nPlafond, nLama, 10), 1)

            nSkip = Math.Round(nSkip / 2, 10)
            If nAngsuranEfektif < nAngsuranPA Then
                nEfektif = nEfektif + nSkip
            ElseIf nAngsuranEfektif > nAngsuranPA Then
                nEfektif = nEfektif - nSkip
            End If
        Loop
        BungaEfektif = nEfektif
    End Function

    Function GetAnuitas(ByVal nSukuBunga As Double, ByVal nPlafond As Double, ByVal nLama As Integer, Optional nRound As Integer = 0) As Double
        ' Hitung Anuitas Untuk Mengetahui Jumlah Angsuran
        '       a*P
        ' ----------------
        ' 1-(1/((1+a)^n)))
        ' a = Sukubunga / Bulan
        ' P = Plafond
        ' n = Lama

        If nSukuBunga <> 0 Then
            GetAnuitas = Math.Round((nSukuBunga * nPlafond) / (1 - (1 / ((1 + nSukuBunga) ^ nLama))), nRound)
        End If
    End Function

    Sub UpdPengajuanKredit(ByVal cRekening As String, ByVal dTgl As Date, ByVal cKode As String, ByVal cWilayah As String,
                           ByVal cAO As String, ByVal nPlafond As Double, ByVal nLama As Double, ByVal cUser As String,
                           ByVal cDateTime As DateTime, Optional ByVal dTglPenolakan As Date = #1/1/1900#)
        Dim vaField() As Object = {"Rekening", "Tgl", "Kode", "Wilayah", "AO",
                                   "Plafond", "Lama", "UserName", "DateTime", "TglPenolakan",
                                   "CabangEntry"}
        Dim vaValue() As Object = {cRekening, dTgl, cKode, cWilayah, cAO,
                                   nPlafond, nLama, cUser, cDateTime, dTglPenolakan, aCfg(eCfg.msKodeCabang)}
        objData.Update(GetDSN, "PengajuanKredit", String.Format("Rekening = '{0}'", cRekening), vaField, vaValue)
    End Sub

    Sub UpdRealisasi(ByVal cRekening As String, ByVal cWilayah As String,
                     ByVal cKode As String, ByVal cGolonganKredit As String, ByVal cGolonganDebitur As String,
                     ByVal cSektorEkonomi As String, ByVal cSifatKredit As String,
                     ByVal cJenisPenggunaan As String, ByVal cGolonganPenjamin As String, ByVal cNoSPK As String,
                     ByVal nPersBunga As Double, ByVal nSukuBungaEfektif As Double, ByVal dTgl As Date,
                     ByVal cCaraPerhitungan As String, ByVal nPlafond As Double, ByVal nLama As Double,
                     ByVal cAO As String, ByVal nAdministrasi As Double, ByVal nAsuransi As Double,
                     ByVal nMaterai As Double, ByVal nNotaris As Double, ByVal nProvisi As Double,
                     ByVal cNoPengajuan As String, ByVal nTotalBunga As Double, ByVal cRekeningTabungan As String,
                     ByVal nGracePeriodPokok As Double, ByVal nGracePeriodBunga As Double,
                     Optional ByVal nPembulatan As Double = 50, Optional ByVal nAngsuran As Double = 0,
                     Optional ByVal cHitungDenda As String = "Y", Optional ByVal cStatusRealisasi As String = "0",
                     Optional ByVal cBagianYangDijamin As String = "0", Optional ByVal cJenisPengikatan As String = "I",
                     Optional ByVal cJenisProvisi As String = "T", Optional ByVal cAutoDebet As String = "T",
                     Optional ByVal cCabang As String = "", Optional ByVal cKeterkaitan As String = "",
                     Optional ByVal cSumberDana As String = "", Optional ByVal cPeriodePembayaran As String = "",
                     Optional ByVal cJenisUsaha As String = "", Optional ByVal cKaryawan As String = "T",
                     Optional ByVal cInstansi As String = "T", Optional ByVal cBendahara As String = "",
                     Optional nSukuBungaBerikutnya As Single = 0, Optional ByVal nPos As myPos = myPos.add)
        If cCabang = "" Then cCabang = aCfg(eCfg.msKodeCabang).ToString
        Dim vaField() As Object = {"Rekening", "Wilayah", "Kode", "GolonganKredit",
                                    "GolonganDebitur", "SektorEkonomi", "SifatKredit", "JenisPenggunaan",
                                    "GolonganPenjamin", "NoSPK", "SukuBunga", "Tgl",
                                    "CaraPerhitungan", "Plafond", "Lama", "ao",
                                    "Administrasi", "Asuransi", "Materai", "Notaris",
                                    "Provisi", "UserName", "DateTime", "TotalBunga",
                                    "NoPengajuan", "RekeningTabungan", "GracePeriodePokok", "GracePeriodeBunga",
                                    "Pembulatan", "AngsuranPerBulan", "HitungDenda", "StatusRealisasi",
                                    "BagianYangDijamin", "JenisPengikatan", "JenisProvisi", "AutoDebet",
                                    "BungaEfektif", "CabangEntry", "otorisasi",
                                    "keterkaitan", "sumberdana", "periodepembayaran", "jenisusaha",
                                    "karyawan", "instansi", "bendahara", "sukubungaberikutnya"}
        Dim vaValue() As Object = {cRekening, cWilayah, cKode, cGolonganKredit,
                                   cGolonganDebitur, cSektorEkonomi, cSifatKredit, cJenisPenggunaan,
                                   cGolonganPenjamin, cNoSPK, nPersBunga, dTgl,
                                   cCaraPerhitungan, nPlafond, nLama, cAO,
                                   nAdministrasi, nAsuransi, nMaterai, nNotaris,
                                   nProvisi, cUserName, SNow(), nTotalBunga,
                                   cNoPengajuan, cRekeningTabungan, nGracePeriodPokok, nGracePeriodBunga,
                                   nPembulatan, nAngsuran, cHitungDenda, cStatusRealisasi,
                                   cBagianYangDijamin, cJenisPengikatan, cJenisProvisi, cAutoDebet,
                                   nSukuBungaEfektif, cCabang, 1,
                                   cKeterkaitan, cSumberDana, cPeriodePembayaran, cJenisUsaha,
                                   cKaryawan, cInstansi, cBendahara, Val(nSukuBungaBerikutnya)}
        If nPos = myPos.add Then
            objData.Delete(GetDSN, "debitur", "rekening", , cRekening)
            objData.Add(GetDSN, "debitur", vaField, vaValue)
        Else
            objData.Update(GetDSN, "Debitur", String.Format("Rekening = '{0}'", cRekening), vaField, vaValue)
        End If
    End Sub

    Sub UpdAngsuranPembiayaan(ByVal cCabang As String, ByVal cFaktur As String, ByVal dTgl As Date,
                              ByVal cRekening As String, ByVal cStatus As eAngsuran, ByVal nPokok As Double,
                              ByVal nBunga As Double, ByVal nDenda As Double, ByVal cKeterangan As String,
                              Optional ByVal lUpdateBukuBesar As Boolean = True, Optional ByVal lDeleteWhenExist As Boolean = False,
                              Optional ByVal nTabungan As Double = 0, Optional ByVal nDTitipan As Double = 0,
                              Optional ByVal nKTitipan As Double = 0, Optional ByVal cKas As String = "K",
                              Optional ByVal lTunggak As Boolean = False, Optional ByVal cStatusPembayaran As String = "0",
                              Optional ByVal cUser As String = "", Optional ByVal lTarikTitipan As Boolean = False,
                              Optional ByVal lAYDA As Boolean = False, Optional ByVal lHapusBuku As Boolean = False,
                              Optional ByVal lRekeningKoran As Boolean = False, Optional ByVal lInstansi As Boolean = False,
                              Optional ByVal cIPTW As String = "0")

        Dim nDPokok As Double = 0
        Dim nKPokok As Double = 0
        Dim nDBunga As Double = 0
        Dim nKBunga As Double = 0
        Dim db As New DataTable
        Dim nTotal As Double = nPokok + nBunga
        Dim cKodeTransaksi As String
        Dim cRekeningTabungan As String
        Dim cStatusAngsuran As String
        Dim cCaraHitung As String
        Dim cStatusTitipan As String
        Dim cStatusInstansi As String

        If lDeleteWhenExist Then
            ' Hapus Data Angsuran
            DelAngsuranKredit(cStatus, cFaktur)
        End If

        If cUser = "" Then
            cUser = cUserName
        End If
        If cStatus < 5 Then
            nDPokok = nPokok
            nDBunga = nBunga
        Else
            nKPokok = nPokok
            nKBunga = nBunga
        End If

        ' Jika Koreksi maka KPOkok & KBunga Minus
        If nKPokok < 0 Then
            nDPokok = -nKPokok
            nKPokok = 0
        End If
        If nKBunga < 0 Then
            nDBunga = -nKBunga
            nKBunga = 0
        End If
        If lTunggak Then
            cStatusAngsuran = "1"
        Else
            cStatusAngsuran = "0"
        End If
        If lInstansi Then
            cStatusInstansi = "1"
        Else
            cStatusInstansi = "0"
        End If

        If cStatus = eAngsuran.ags_Angsuran Then
            ' jika jumlah titipan yang ditarik < jumlah angsuran maka dianggap tidak ada penarikan titipan
            If nKTitipan < nPokok + nBunga Then
                nKTitipan = 0
            End If
            If cKas = "K" And nKTitipan < nPokok + nBunga Then
                nKTitipan = 0
            ElseIf cKas = "K" And nKTitipan = nPokok + nBunga Then
                cKas = "I"
            End If
        Else
            cStatusAngsuran = "0"
        End If
        If lTarikTitipan Then
            cStatusTitipan = "1"
        Else
            cStatusTitipan = "0"
        End If

        ' Update Table Angsuran
        Dim vaField() As Object = {"Status", "Faktur", "Tgl", "Rekening", "Denda",
                                    "DPokok", "KPokok", "DBunga", "KBunga",
                                    "DTitipan", "KTitipan", "Kas", "Keterangan",
                                    "DateTime", "UserName", "CabangEntry", "StatusPembayaran",
                                    "StatusTitipan", "statusInstansi", "IP", "Version", "iptw"}
        Dim vaValue() As Object = {cStatus, cFaktur, dTgl, cRekening, nDenda,
                                    nDPokok, nKPokok, nDBunga, nKBunga,
                                    nDTitipan, nKTitipan, cKas, cKeterangan,
                                    Format(Now, "yyyy-mm-dd hh:mm:ss"), cUser, cCabang, cStatusPembayaran,
                                    cStatusTitipan, cStatusInstansi, cIPNumberLocal, cAppVersion, cIPTW}
        objData.Add(GetDSN, "Angsuran", vaField, vaValue)
        If Left(cFaktur, 2) = "JA" Or cStatus = eAngsuran.ags_jadwal Then Exit Sub
        If cKas = "T" Then
            db = objData.Browse(GetDSN, "Debitur", , "Rekening", , cRekening)
            If db.Rows.Count > 0 Then
                cRekeningTabungan = GetNull(db.Rows(0).Item("RekeningTabungan"), "").ToString
                cCaraHitung = db.Rows(0).Item("CaraPerhitungan").ToString

                db = objData.Browse(GetDSN, "Tabungan", , "Rekening", , cRekeningTabungan)
                If db.Rows.Count > 0 Then
                    cKodeTransaksi = IIf(cKas = "K", aCfg(eCfg.msKodePenarikanTunai).ToString, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString).ToString
                    UpdMutasiTabungan(cCabang, cKodeTransaksi, cFaktur, dTgl, cRekeningTabungan, nTotal + nDenda, True, "Setoran Dari Angsuran ", False, , , cUser)
                End If
            End If
        End If

        ' 1. Update Rekening Buku Besar
        If lUpdateBukuBesar Then
            If lAYDA Then
                updRekAYDA(cFaktur)
            ElseIf lHapusBuku Then
                updRekHapusBuku(cFaktur)
            Else
                If lRekeningKoran Then
                    UpdPencairanRekeningKoran(cFaktur, cRekening, nPokok)
                Else
                    UpdRekAngsuranPembiayaanPerJurnal(cFaktur, lHapusBuku, lInstansi)
                End If
            End If
        End If
    End Sub

    Sub DelAngsuranKredit(ByVal cStatus As eAngsuran, ByVal cFaktur As String)
        If Trim(cFaktur) <> "" Then
            ' 1. Hapus Angsuran
            ' 2. Hapus Buku Besar
            ' 3. Hapus Mutasi Tabungan

            objData.Delete(GetDSN, "Angsuran", "Status", , cStatus.ToString, String.Format(" and Faktur Like '{0}%'", cFaktur))

            DelBukuBesar(cFaktur)

            DelMutasiTabungan(cFaktur)

        End If
    End Sub

    Sub updRekAYDA(ByVal cFaktur As String)
        Dim db As New DataTable

        ' Hapus Data Lama
        objData.Delete(GetDSN, "BukuBesar", "faktur", , cFaktur)

        Dim cFields As String = "a.Rekening,a.status,a.Tgl,a.saldo,r.Nama as NamaDebitur,"
        cFields = cFields & "a.UserName,a.DateTime,g.Rekening as RekeningPokok,a.ID"
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        db = objData.Browse(GetDSN, "ayda a", cFields, "a.Faktur", , cFaktur, , , vaJoin)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                Dim nID As Double = CDbl(.Item("Id"))
                Dim cRekKredit As String = GetNull(.Item("RekeningPokok"), "").ToString
                Dim cRekAYDA As String = aCfg(eCfg.msRekeningAYDA).ToString
                Dim cRekKas As String = aCfg(eCfg.msKodeKas).ToString
                Dim nBakiDebet As Double = CDbl(.Item("Saldo"))
                Dim cKeterangan As String = String.Format("AYDA an. {0}", .Item("NamaDebitur").ToString)
                Dim cTime As String = GetNull(.Item("DateTime"), SNow).ToString
                Dim cUser As String = GetNull(.Item("UserName"), "").ToString
                Dim cKodeCabang As String = .Item("Rekening").ToString.Substring(0, 2)
                If .Item("Status").ToString = "Y" Then
                    ' jika masuk AYDA
                    ' RAA          xxxxxxx            ' bakidebet
                    '     Pokok           xxxxxxx     ' bakidebet
                    UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekAYDA, cKeterangan, nBakiDebet, , cTime, cUser)
                    UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekKredit, cKeterangan, , nBakiDebet, cTime, cUser)
                ElseIf .Item("Status").ToString = "T" Then
                    ' jika masuk AYDA
                    ' Pokok        xxxxxxx            ' bakidebet
                    '     AYDA            xxxxxxx     ' bakidebet
                    UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekKas, cKeterangan, nBakiDebet, , cTime, cUser)
                    UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekAYDA, cKeterangan, , nBakiDebet, cTime, cUser)
                End If
            End With
        End If
        db.Dispose()
    End Sub

    Sub updRekHapusBuku(ByVal cFaktur As String)
        Dim db As New DataTable

        ' Hapus Data Lama
        objData.Delete(GetDSN, "BukuBesar", "faktur", , cFaktur)

        Dim cFields As String = "a.Rekening,a.Pokok,a.Tgl,a.Bunga,r.Nama as NamaDebitur,"
        cFields = cFields & "a.UserName,a.DateTime,g.Rekening as RekeningPokok,a.kas,a.ID"
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                   "Left Join RegisterNasabah r on d.Kode = r.Kode",
                                   "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        db = objData.Browse(GetDSN, "angsuran a", cFields, "a.Faktur", , cFaktur, , , vaJoin)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                Dim nID As Double = CDbl(.Item("Id"))
                Dim cRekKredit As String = GetNull(.Item("RekeningPokok"), "").ToString
                Dim cRekOverDraft As String = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                Dim nBakiDebet As Double = CDbl(.Item("Pokok"))
                Dim cKeterangan As String = "Masuk Hapus Buku Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                Dim cKodeCabang As String = .Item("Rekening").ToString.Substring(0, 2)
                Dim cTime As String = GetNull(.Item("DateTime"), SNow).ToString
                Dim cUser As String = GetNull(.Item("UserName"), "").ToString

                ' jika masuk Hapus Buku
                ' Hapus Buku    xxxxxxx            ' bakidebet
                '     pokok           xxxxxxx      ' bakidebet
                UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekOverDraft, cKeterangan, nBakiDebet, , cTime, cUser)
                UpdBukuBesar(nID, cKodeCabang, cFaktur, CDate(.Item("Tgl")), cRekKredit, cKeterangan, , nBakiDebet, cTime, cUser)
            End With
        End If
        db.Dispose()
    End Sub

    Sub UpdRekAngsuranPembiayaanPerJurnal(ByVal cFaktur As String, Optional ByVal lHapusBuku As Boolean = False,
                                          Optional ByVal lInstansi As Boolean = False)


        Dim nAngsuran As Double
        Dim cRekKas As String
        Dim nDKas As Double
        Dim nKKas As Double
        Dim cRekBunga As String
        Dim cCabang As String
        'Dim lPusat As Boolean
        Dim lAntarKantor As Boolean
        Dim cRekeningAntarKantorPasiva As String
        Dim cRekeningAntarKantorAktiva As String
        Dim cRekening As String
        Dim cRekJurnal As String
        Dim cRekJurnal1 As String
        Dim nKas As Double
        Dim cKodeCabang As String
        Dim lBelumLunas As Boolean
        Dim nBakiDebet As Double
        Dim nBakiDebetSebelumnya As Double
        Dim x As TypeKolektibilitas = Nothing
        Dim Y As TypeAccrualKredit = Nothing
        Dim dTglAkhir As Date
        Dim nAccrual As Double
        Dim nAngsuranBulanIni As Double
        Dim nBayarBunga As Double
        Dim db As New DataTable
        Dim nPokok As Double
        Dim nBunga As Double
        Dim nDenda As Double
        Dim nTitipan As Double
        Dim cKeteranganPokok As String
        Dim cKeteranganBunga As String
        Dim cKeteranganDenda As String
        Dim cKeteranganTitipan As String
        Dim cKeteranganAccrual As String
        Dim cKeteranganTemp As String = ""
        Dim vaJoin() As Object
        Dim cWhere As String = ""
        Dim dTglTemp As Date
        Dim cCabangEntryTemp As String
        Dim dbData As New DataTable

        ' Hapus Data Lama
        objData.Delete(GetDSN, "BukuBesar", "faktur", , cFaktur)

        Dim cFields As String = "a.ID,a.Status,a.Rekening,a.Tgl,ifnull(a.KPokok,0) as KPokok,ifnull(a.KBunga,0) as KBunga,ifnull(a.DPokok,0) as DPokok,ifnull(a.DBunga,0) as DBunga,"
        cFields = cFields & "ifnull(a.Denda,0) as Denda,a.StatusAngsuran,d.Kode,"
        cFields = cFields & "ifnull(a.DTitipan,0) as DTitipan,ifnull(a.KTitipan,0) as KTitipan,a.Kas,a.cabangentry,a.Rekening,"
        cFields = cFields & "ifnull(r.Nama,'') as NamaDebitur,d.rekeningtabungan,"
        cFields = cFields & "ifnull(g.Rekening,'') as RekeningPokok,"
        cFields = cFields & "ifnull(g.RekeningDenda,'') as RekeningDenda,ifnull(g.RekeningBunga,'') as RekeningPendapatanBunga,"
        cFields = cFields & "ifnull(g.RekeningTitipan,'') as RekeningTitipan,a.username as username,d.StatusPencairan,d.TglPelunasan,"
        cFields = cFields & "ifnull(g.RekeningAccrual,'') as RekeningAccrual,a.datetime"
        Dim vaJoint() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                    "Left Join RegisterNasabah r on d.Kode = r.Kode",
                                    "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        dbData = objData.Browse(GetDSN, "Angsuran a", cFields, "a.Faktur", , cFaktur, " Group by a.Faktur,a.Tgl,a.ID", , vaJoint)
        If dbData.Rows.Count > 0 Then
            With dbData.Rows(0)
                nPokok = CDbl(.Item("KPokok")) - CDbl(.Item("DPokok"))
                nBunga = CDbl(.Item("KBunga")) - CDbl(.Item("DBunga"))
                nDenda = CDbl(.Item("Denda"))
                nTitipan = CDbl(.Item("ktitipan")) - CDbl(.Item("dtitipan"))
                nAngsuran = CDbl(.Item("KPokok")) + CDbl(.Item("KBunga")) + CDbl(.Item("Denda")) - CDbl(.Item("ktitipan")) + CDbl(.Item("dtitipan")) - CDbl(.Item("DPokok")) - CDbl(.Item("DBunga"))
                nDKas = CDbl(IIf(nAngsuran > 0, nAngsuran, 0))
                nKKas = CDbl(IIf(nAngsuran < 0, -nAngsuran, 0))
                If Not lHapusBuku Then
                    If Debugger.IsAttached And UCase(cUserName) = "HEASOFT" Then
                        db = objData.Browse(GetDSN, "username", "kasteller", "username", , .Item("UserName").ToString)
                        If db.Rows.Count > 0 Then
                            cKasTeller = GetNull(db.Rows(0).Item("KasTeller"), KasTeller).ToString
                            If db.Rows(0).Item("UserName").ToString = "01TYA" Then
                                cKasTeller = "1.110.10.02"
                            End If
                        End If
                    End If
                    If .Item("Kas").ToString = "K" Then
                        cRekKas = cKasTeller
                    ElseIf .Item("Kas").ToString = "T" Then
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                        cFields = "g.rekening"
                        vaJoin = {"left join golongantabungan g on g.kode = t.golongantabungan"}
                        db = objData.Browse(GetDSN, "tabungan t", cFields, "t.rekening", , .Item("RekeningTabungan").ToString, , , vaJoin)
                        If db.Rows.Count > 0 Then
                            cRekKas = GetNull(db.Rows(0).Item("Rekening"), cRekKas).ToString
                        End If
                    Else
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    End If
                    '      cRekKas = IIf(dbdata!Kas = "K", cKasTeller, aCfg(msRekeningPemindahBukuan))
                Else
                    cRekKas = aCfg(eCfg.msRekeningHapusBuku).ToString
                End If
                If lInstansi Then
                    cRekKas = .Item("RekeningTitipan").ToString
                End If
                db = objData.Browse(GetDSN, "angsurantemp", "faktur", "fakturangsuran", , cFaktur)
                If db.Rows.Count > 0 Then
                    If GetNull(db.Rows(0).Item("faktur"), "").ToString <> "" Then
                        cRekKas = .Item("RekeningTitipan").ToString
                    End If
                End If
                cRekBunga = GetNull(.Item("RekeningPendapatanBunga"), "").ToString
                cCabang = .Item("CabangEntry").ToString
                cRekening = .Item("Rekening").ToString

                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                '    lPusat = False
                '    Set dbPusat = objdata.Browse(GetDSN, "cabang_mutasi", "Kode,JenisKantor,Induk", _
                '                            "Kode", , Left(cRekening, 2), "and tgl <= '" & FormatValue(dbdata!Tgl, formatType.yyyy_MM_dd) & "'", "tgl desc limit 1")
                '    If Not dbPusat.EOF Then
                '      cInduk = dbPusat!Induk
                '      If dbPusat!JenisKantor = "P" Then lPusat = True
                '    End If

                lAntarKantor = False
                cRekeningAntarKantorPasiva = aCfg(eCfg.msRekeningAntarKantorPasiva).ToString
                cRekeningAntarKantorAktiva = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString

                ' jika ada transaksi dari kantor kas ke kantor cabang maka
                ' transaksi yang terjadi bukan transaksi antar kantor seperti
                ' antar kantor pusat dan cabang
                '    lInduk = True
                '    If cKodeKantorInduk <> cInduk Then
                '      lInduk = False
                '    End If

                dTglAkhir = EOM(DateAdd("m", -1, CDate(.Item("Tgl"))))
                Dim cRekeningTemp As String = .Item("Rekening").ToString
                dTglTemp = CDate(.Item("Tgl"))
                GetTunggakan(cRekeningTemp, dTglAkhir, x, , , True, , .Item("Kode").ToString)
                nBakiDebet = GetBakiDebet(cRekeningTemp, EOM(dTglTemp))
                nBakiDebetSebelumnya = GetBakiDebet(cRekeningTemp, EOM(dTglTemp), True, , CDbl(.Item("Id")) - 1)
                Dim nBakiDebet1 As Double = GetBakiDebet(cRekeningTemp, dTglTemp)
                Dim nBakiDebetSebelumnya1 As Double = GetBakiDebet(cRekeningTemp, dTglTemp, True, , CDbl(.Item("Id")) - 1)
                If nBakiDebet1 = 0 And nBakiDebetSebelumnya1 > 0 Then
                    nBakiDebetSebelumnya = nBakiDebetSebelumnya1
                End If
                lBelumLunas = False
                Dim cStatusPencairanTemp As String = .Item("StatusPencairan").ToString
                If nBakiDebet <> 0 And x.nKolek <= 1 And cStatusPencairanTemp = "1" Then
                    lBelumLunas = True
                Else
                    If x.cCaraPerhitungan = "7" Then
                        If nBakiDebet = 0 And x.nKolek <= 1 And cStatusPencairanTemp = "1" And formatValue(CDate(.Item("TglPelunasan")), formatType.yyyy_MM_dd) >= "9999-99-99" Then
                            lBelumLunas = True
                        End If
                    End If
                End If
                If nBakiDebet = 0 Then
                    If nBakiDebetSebelumnya <> 0 Then
                        If nBakiDebetSebelumnya <> 0 And x.nKolek <= 1 And cStatusPencairanTemp = "1" Then
                            lBelumLunas = True
                        Else
                            If x.cCaraPerhitungan = "7" Then
                                If nBakiDebetSebelumnya = 0 And x.nKolek <= 1 And cStatusPencairanTemp = "1" And formatValue(CDate(.Item("TglPelunasan")), formatType.yyyy_MM_dd) >= "9999-99-99" Then
                                    lBelumLunas = True
                                End If
                            End If
                        End If
                    End If
                End If

                If x.nKolek < 2 Then
                    If formatValue(dTglTemp, formatType.yyyy_MM_dd) < "2014-03-01" Then
                        dTglTemp = DateAdd("m", -1, dTglTemp)
                    Else
                        dTglTemp = CDate(.Item("Tgl"))
                    End If
                    GetAccrualKredit(cRekeningTemp, dTglTemp, Y)
                    nAccrual = Y.nBungaAccrualHarian
                    nAngsuranBulanIni = 0
                    nBayarBunga = 0
                    nAccrual = x.nTunggakanBunga + Y.nBungaAccrualHarian
                End If
                cWhere = String.Format(" and faktur <> '{0}' and tgl > '{1}'", cFaktur, formatValue(dTglAkhir, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0} and tgl <='{1}' and id <{2}", cWhere, formatValue(dTglTemp, formatType.yyyy_MM_dd), CDbl(.Item("Id")))
                db = objData.Browse(GetDSN, "angsuran", "sum(bunga) as Bunga", "rekening", , cRekeningTemp, cWhere)
                If db.Rows.Count > 0 Then
                    nAngsuranBulanIni = CDbl(GetNull(db.Rows(0).Item("Bunga"), 0))
                    If nAccrual - nAngsuranBulanIni > 0 Then
                        nAccrual = nAccrual - nAngsuranBulanIni
                    End If
                End If
                If Not lBelumLunas Then nAccrual = 0
                cCabangEntryTemp = .Item("CabangEntry").ToString
                Dim nIDTemp As Double = CDbl(.Item("ID"))
                Dim cUserTemp As String = .Item("Username").ToString
                ';@adjie-2014-02-06
                ' jika ada transaksi tidak dalam satu cabang berarti transaksi ANTAR KANTOR
                If Not IsSatuCabang(cCabang, Left(cRekening, 2)) Then ' cCabang <> Left(cRekening, 2) And Not lInduk Then
                    '    If cCabang <> cKodeCabang And Not lInduk Then
                    lAntarKantor = True
                    ' jika pada saat diposting cabang entry sama dengan
                    ' kode cabang yang aktif ( di cfgsystem )

                    ' Jurnal
                    ' Dr. PB / Kas                                nKas
                    ' Dr. Rekening Antar Kantor Aktiva/Pasiva     nKas
                    '   Cr. PB / Kas                                nKas
                    '   Cr. Rekening Antar Kantor Aktiva/Pasiva     nKas
                    nKas = CDbl(IIf(nDKas = 0, nKKas, nDKas))
                    cRekJurnal = IIf(nDKas = 0, cRekeningAntarKantorAktiva, cRekKas).ToString
                    cRekJurnal1 = IIf(nKKas = 0, cRekeningAntarKantorPasiva, cRekKas).ToString
                    cKeteranganTemp = "Angsuran Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                    UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekJurnal, cKeteranganTemp, nKas, , , cUserTemp)
                    UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekJurnal1, cKeteranganTemp, , nKas, , cUserTemp)
                End If

                ' PB / Kas    xxxxxxx           nPokok
                '     Pokok           xxxxxxx       KPokok

                ' PB / Kas    xxxxxxx           nBunga
                '     Bunga           xxxxxxx       nBunga

                ' PB / Kas    xxxxxxx           nTitipan
                '     Titipan         xxxxxxx       nTitipan

                ' PB / Kas    xxxxxxx           nDenda
                '     Denda           xxxxxxx       nDenda

                '    If cCabang <> cKodeCabang And Not lInduk Then
                If Not IsSatuCabang(cCabang, cRekening.Substring(0, 2)) Then 'cCabang <> Left(dbdata!Rekening, 2) And Not lInduk Then
                    cRekKas = cRekeningAntarKantorAktiva
                    cCabangEntryTemp = Left(cRekening, 2)
                    'karena kantor kas ploso (12) naik status menjadi cabang ploso (02)
                    'maka untuk rekening nasabah ploso, cabang entry-nya diganti jadi 02
                    'tgl 02-01-2014 adalah tgl kantor kas ploso naik status menjadi kantor cabang

                    '####@adjie- 06-02-2014 - code dibawah saya blok karena tidak ngefek mulai 2 januari 2014####
                    '      If cCabangEntryTemp = "12" And dbdata!Tgl >= CDate("02-01-2014") Then
                    '        cCabangEntryTemp = "02"
                    '      End If
                End If

                '    If Not lAntarKantor Or cKodeCabang <> cCabang Then
                '    If lAntarKantor Or Left(cRekening, 2) <> cCabang Then
                '    End If
                cKeteranganPokok = "Angsuran Pokok Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                cKeteranganBunga = "Angsuran Bunga Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                cKeteranganTitipan = "Titipan Angsuran Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                cKeteranganDenda = "Denda Angsuran Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString
                cKeteranganAccrual = "Accrual Bunga Kredit an. " & GetNull(.Item("NamaDebitur"), "").ToString

                ' Pokok
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganPokok, nPokok, , , cUserTemp)
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, GetNull(.Item("RekeningPokok"), "").ToString, cKeteranganPokok, , nPokok, , cUserTemp)

                ' Bunga
                If formatValue(dTglTemp, formatType.dd_MM_yyyy) >= "01-11-2013" Then
                    If nAccrual <> 0 Then
                        If nAccrual > nBunga Then
                            nAccrual = nBunga
                            nBayarBunga = 0
                        ElseIf nAccrual = nBunga Then
                            nAccrual = nBunga
                            nBayarBunga = 0
                        Else
                            nBayarBunga = nBunga - nAccrual
                        End If
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganBunga, nBayarBunga, , , cUserTemp)
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekBunga, cKeteranganBunga, , nBayarBunga, , cUserTemp)
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganAccrual, nAccrual, , , cUserTemp)
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, GetNull(.Item("RekeningAccrual"), "").ToString, cKeteranganAccrual, , nAccrual, , cUserTemp)
                    Else
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganBunga, nBunga, , , cUserTemp)
                        UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekBunga, cKeteranganBunga, , nBunga, , cUserTemp)
                    End If
                Else
                    UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganBunga, nBunga, , , cUserTemp)
                    UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekBunga, cKeteranganBunga, , nBunga, , cUserTemp)
                End If

                ' Denda
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganDenda, nDenda, , , cUserTemp)
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, GetNull(.Item("RekeningDenda"), "").ToString, cKeteranganDenda, , nDenda, , cUserTemp)

                ' Titipan
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, cRekKas, cKeteranganTitipan, nTitipan, , , cUserTemp)
                UpdBukuBesar(nIDTemp, cCabangEntryTemp, cFaktur, dTglTemp, .Item("RekeningTitipan").ToString, cKeteranganTitipan, , nTitipan, , cUserTemp)
            End With
        End If
        dbData.Dispose()
    End Sub

    Sub UpdPencairanKredit(ByVal cRekening As String, ByVal cFaktur As String, ByVal nPencairanPokok As Double, ByVal dTgl As Date,
                           Optional cKas As String = "K", Optional ByVal lPencairanRekeningKoran As Boolean = False)
        Dim dbData As New DataTable
        Dim cNoPengajuan As String
        Dim cCabang As String
        Dim cWhere As String
        Dim nTotalBunga As Double
        Dim cKeterangan As String

        Const cField As String = "rekening,totalbunga,nopengajuan,caraperhitungan"
        dbData = objData.Browse(GetDSN, "Debitur", cField, "Rekening", , cRekening)
        If dbData.rows.count > 0 Then
            ' Hapus Realisasi Tapi tanpa menghapus record pada Debitur
            '    DeleteRealisasi cRekening, cFaktur, False
            DelBukuBesar(cFaktur)

            With dbData.Rows(0)
                cNoPengajuan = .Item("NoPengajuan").ToString
                cCabang = aCfg(eCfg.msKodeCabang).ToString
                nTotalBunga = 0
                cKeterangan = "Realisasi Kredit"
                If .Item("CaraPerhitungan").ToString <> "7" Then
                    nTotalBunga = CDbl(GetNull(.Item("TotalBunga")))
                Else
                    cKeterangan = "Pencairan RK"
                End If

                UpdAngsuranPembiayaan(cCabang, cFaktur, dTgl, cRekening, eAngsuran.ags_Realisasi, nPencairanPokok, nTotalBunga, 0,
                                      cKeterangan, False, True, , , , cKas)

                If Not lPencairanRekeningKoran Then
                    ' berlaku untuk semua cara perhitungan selain R/K dan penarikan plafond R/K pertama kali
                    ' Update Buku Besar
                    UpdRekRealisasiKredit(cFaktur)

                    ' data status pencairan dan faktur hanya disimpan saat penarikan plafond pertama kali untuk RK
                    cWhere = String.Format("Rekening = '{0}' ", cRekening)
                    objData.Edit(GetDSN, "Debitur", cWhere, {"Faktur", "StatusPencairan"}, {cFaktur, "1"})

                    ' edit status pengajuan
                    If cNoPengajuan <> "" Then
                        cWhere = String.Format("Rekening='{0}'", cNoPengajuan)
                        objData.Edit(GetDSN, "PengajuanKredit", cWhere, {"StatusPengajuan"}, {"1"})
                    End If
                Else
                    ' berlaku untuk penarikan R/K selanjutnya, kalau pada saat penarikan pertama kali nominalnya tidak sama
                    ' dengan plafond
                    ' Update Buku Besar
                    UpdRekPenarikanRekeningKoran(cFaktur)
                End If
            End With
        End If
        dbData.Dispose()
    End Sub

    Sub UpdRekRealisasiKredit(ByVal cFaktur As String, Optional ByVal cNow As String = "", Optional ByVal lAddToMutasiTabungan As Boolean = True)
        Dim cRekeningKas As String
        Dim nPlafond As Double
        Dim dbData As New DataTable
        Dim cCabang As String
        Dim cRekeningProvisi As String
        Dim cNamaDebitur As String

        cNow = CStr(IIf(cNow = "", SNow, cNow))
        ' Hapus Data Lama

        DelBukuBesar(cFaktur)

        Dim cField As String = "d.CaraPerhitungan,d.SukuBunga,d.PenarikanPlafond,d.ID,d.StatusPencairan,"
        cField = cField & "d.CaraPencairan,d.Rekening,a.Tgl,d.RekeningTabungan,a.cabangentry,"
        cField = cField & "ifnull(a.DPokok,0) as Dpokok,ifnull(d.Administrasi,0) as Administrasi,ifnull(d.Materai,0) as Materai,ifnull(d.Notaris,0) as Notaris,ifnull(d.Asuransi,0) as Asuransi,"
        cField = cField & "ifnull(d.TotalBunga,0) as TotalBunga,ifnull(d.Provisi,0) as Provisi,g.RekeningPendapatanProvisi,"
        cField = cField & "g.Rekening as RekeningRealisasi,g.RekeningAdministrasi,d.JenisProvisi,"
        cField = cField & "g.RekeningMaterai,g.RekeningNotaris,g.RekeningAsuransi,g.RekeningProvisi,"
        cField = cField & "c.Nama as NamaDebitur,a.Kas,d.username as username,d.RekeningTabungan "
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                   "Left Join GolonganKredit g on d.GolonganKredit = g.Kode",
                                   "Left Join RegisterNasabah c on c.Kode = d.Kode"}
        dbData = objData.Browse(GetDSN, "Angsuran a", cField, "a.Faktur", , cFaktur, , , vaJoin)
        If dbData.Rows.Count > 0 Then
            With dbData.Rows(0)
                '    If dbData!StatusPencairan <> "0" Then
                '    If cFaktur = "R0012006022700000012" Then Stop
                cRekeningKas = IIf(.Item("Kas").ToString = "K", cKasTeller, aCfg(eCfg.msRekeningPemindahBukuan)).ToString
                cRekeningProvisi = IIf(.Item("JenisProvisi").ToString = "Y", .Item("RekeningPendapatanProvisi").ToString, .Item("RekeningProvisi").ToString).ToString
                nPlafond = CDbl(.Item("DPokok"))
                cCabang = .Item("CabangEntry").ToString
                cNamaDebitur = GetNull(.Item("NamaDebitur"), "").ToString

                ' KYD            xxxxxx                nPlafond
                '   PB           xxxxxx               nPlafond

                ' PB                       xxxxxxxx          ' Administrasi
                '   Administrasi                  xxxxxxx   ' Administrasi

                ' PB                       xxxxxxxx        ' Notaris
                '   Notaris                       xxxxxxx   ' Notaris

                ' PB                       xxxxxxxx        ' Materai
                '   Materai                       xxxxxxx   ' Materai

                ' PB                       xxxxxxxx        'Asuransi
                '   Asuransi                      xxxxxxx   ' Asuransi

                ' PB                       xxxxxxxx        ' Provisi
                '   Provisi                       xxxxxxx   ' Provisi

                ' Plafond
                Dim nID As Double = CDbl(.Item("ID"))
                Dim dTglTemp As Date = CDate(.Item("Tgl"))
                Dim cRekeningRealisasi As String = .Item("RekeningRealisasi").ToString
                Dim cKeteranganTemp As String = "Realisasi Kredit an. " & cNamaDebitur
                Dim nAdministrasi As Double = CDbl(.Item("Administrasi"))
                Dim nProvisi As Double = CDbl(.Item("Provisi"))
                Dim nMaterai As Double = CDbl(.Item("Materai"))
                Dim nNotaris As Double = CDbl(.Item("Notaris"))
                Dim nAsuransi As Double = CDbl(.Item("Asuransi"))
                Dim cRekeningAdministrasi As String = .Item("RekeningAdministrasi").ToString
                Dim cRekeningTabungan As String = .Item("RekeningTabungan").ToString
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningRealisasi, cKeteranganTemp, nPlafond, , cNow, cUserName)
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningKas, cKeteranganTemp, , nPlafond, cNow, cUserName)

                ' Administrasi
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningKas, "Biaya Administrasi Realisasi Kredit an. " & cNamaDebitur, nAdministrasi, , cNow, cUserName)
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningAdministrasi, "Biaya Administrasi Realisasi Kredit an. " & cNamaDebitur, , nAdministrasi, cNow, cUserName)

                ' Notaris
                '      UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, cRekeningKas, "Biaya Notaris Realisasi Kredit an. " & cNamaDebitur, dbdata!Notaris, , "K", cNow, GetNull(dbdata!UserName, "")
                '        UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, GetNull(dbdata!RekeningNotaris, ""), "Biaya Notaris Realisasi Kredit an. " & cNamaDebitur, , dbdata!Notaris, "K", cNow, GetNull(dbdata!UserName, "")

                ' Materai
                '      UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, cRekeningKas, "Biaya Materai Realisasi Kredit an. " & cNamaDebitur, dbdata!Materai, , "K", cNow, GetNull(dbdata!UserName, "")
                '        UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, GetNull(dbdata!RekeningMaterai, ""), "Biaya Materai Realisasi Kredit an. " & cNamaDebitur, , dbdata!Materai, "K", cNow, GetNull(dbdata!UserName, "")

                ' Asuransi
                '      UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, cRekeningKas, "Asuransi Realisasi Kredit an. " & cNamaDebitur, dbdata!Asuransi, , "K", cNow, GetNull(dbdata!UserName, "")
                '        UpdBukuBesar msRealisasiKredit, dbdata!Id, cCabang, cFaktur, dbdata!Tgl, GetNull(dbdata!Rekeningasuransi, ""), "Asuransi Realisasi Kredit an. " & cNamaDebitur, , dbdata!Asuransi, "K", cNow, GetNull(dbdata!UserName, "")

                ' Provisi
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningKas, "Provisi Realisasi Kredit an. " & cNamaDebitur, nProvisi, , cNow, cUserName)
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningProvisi, "Provisi Realisasi Kredit an. " & cNamaDebitur, , nProvisi, cNow, cUserName)

                '      If lAddToMutasiTabungan Then
                If .Item("Kas").ToString = "T" And GetNull(.Item("RekeningTabungan"), "").ToString.Length > 5 Then ' jika pemindah bukuan
                    DelBukuBesar(cFaktur)

                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nPlafond, True, "Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nAdministrasi, False, "Administrasi Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nNotaris, False, "Biaya Notaris Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nMaterai, False, "Materai Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nAsuransi, False, "Asuransi Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenarikanPemindahBukuan).ToString, cFaktur, dTglTemp, cRekeningTabungan, nProvisi, False, "Provisi Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                End If
                '    End If
            End With
            dbData.Dispose()
        End If
    End Sub

    Sub UpdRekPenarikanRekeningKoran(ByVal cFaktur As String, Optional ByVal cNow As String = "")
        Dim cRekeningKas As String
        Dim nPlafond As Double
        Dim dbData As New DataTable
        Dim cCabang As String
        Dim cNamaDebitur As String

        cNow = CStr(IIf(cNow = "", SNow, cNow))
        ' Hapus Data Lama
        DelBukuBesar(cFaktur)

        Dim cField As String = "d.CaraPerhitungan,d.SukuBunga,d.PenarikanPlafond,d.ID,d.StatusPencairan,"
        cField = cField & "d.CaraPencairan,d.Rekening,a.Tgl,d.RekeningTabungan,a.cabangentry,"
        cField = cField & "ifnull(a.DPokok,0) as Dpokok,ifnull(d.Administrasi,0) as Administrasi,ifnull(d.Materai,0) as Materai,ifnull(d.Notaris,0) as Notaris,ifnull(d.Asuransi,0) as Asuransi,"
        cField = cField & "ifnull(d.TotalBunga,0) as TotalBunga,ifnull(d.Provisi,0) as Provisi,g.RekeningPendapatanProvisi,"
        cField = cField & "g.Rekening as RekeningRealisasi,g.RekeningAdministrasi,d.JenisProvisi,"
        cField = cField & "g.RekeningMaterai,g.RekeningNotaris,g.RekeningAsuransi,g.RekeningProvisi,a.keterangan as keterangancair,"
        cField = cField & "c.Nama as NamaDebitur,a.Kas,d.username as username,d.RekeningTabungan "
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode",
                                  "Left Join RegisterNasabah c on c.Kode = d.Kode"}
        dbData = objData.Browse(GetDSN, "Angsuran a", cField, "a.Faktur", , cFaktur, , , vaJoin)
        If dbData.Rows.Count > 0 Then
            '    If dbData!StatusPencairan <> "0" Then
            '    If cFaktur = "R0012006022700000012" Then Stop
            With dbData.Rows(0)
                cRekeningKas = IIf(.Item("Kas").ToString = "K", cKasTeller, aCfg(eCfg.msRekeningPemindahBukuan).ToString).ToString
                nPlafond = CDbl(.Item("DPokok"))
                cCabang = .Item("CabangEntry").ToString
                cNamaDebitur = GetNull(.Item("NamaDebitur"), "").ToString
                Dim nID As Double = CDbl(.Item("ID"))
                Dim dTglTemp As Date = CDate(.Item("Tgl"))
                ' KYD            xxxxxx                nPlafond
                '   PB           xxxxxx               nPlafond

                ' Plafond
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, GetNull(.Item("RekeningRealisasi"), "").ToString, "Pencairan RK an. " & cNamaDebitur, nPlafond, , cNow, cUserName)
                UpdBukuBesar(nID, cCabang, cFaktur, dTglTemp, cRekeningKas, "Pencairan RK an. " & cNamaDebitur, , nPlafond, cNow, cUserName)

                If .Item("Kas").ToString = "P" And GetNull(.Item("RekeningTabungan"), "").ToString.Length > 5 Then ' jika pemindah bukuan
                    DelBukuBesar(cFaktur)

                    UpdMutasiTabungan(cCabang, aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString, cFaktur, dTglTemp, GetNull(.Item("RekeningTabungan"), "").ToString, nPlafond, True, "Relisasi Kredit an. " & cNamaDebitur, True, , , cUserName)
                End If
            End With
        End If
        dbData.Dispose()
    End Sub

    Sub GetKewajibanPokokBunga(ByVal cRekening As String, ByVal dTgl As Date, ByRef nBunga As Double,
                               ByRef nPokok As Double, Optional ByVal lBulanIni As Boolean = False,
                               Optional ByRef nKe As Double = 0)
        Dim nRetvalPokok As Double = 0
        Dim nRetvalBunga As Double = 0
        Dim db As New DataTable
        Dim cField As String
        Dim cWhere As String
        nKe = 0
        If Not lBulanIni Then
            cField = "ifnull(Tgl,'9999-12-31'),ifnull(sum(DPokok),0) as Pokok,ifnull(sum(DBunga),0) as Bunga"
            cWhere = ""
            cWhere = "and status = " & eAngsuran.ags_jadwal
            cWhere = String.Format("{0} and tgl <= '{1}'", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "Angsuran", cField, "rekening", , cRekening, cWhere, , , "rekening")
            If db.Rows.Count > 0 Then
                nRetvalPokok = CDbl(GetNull(db.Rows(0).Item("Pokok")))
                nRetvalBunga = CDbl(GetNull(db.Rows(0).Item("Bunga")))
            End If

            ' Hitung Berapa yang sudah di Bayar
            cField = "Sum(KPokok-DPokok) as Pokok,sum(KBunga-DBunga) as Bunga"
            cWhere = ""
            cWhere = String.Format(" and (status = {0} or status = {1}) ", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
            cWhere = String.Format("{0} and Tgl <= '{1}'", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening, cWhere)
            If db.Rows.Count > 0 Then
                nRetvalPokok = nRetvalPokok - CDbl(GetNull(db.Rows(0).Item("Pokok")))
                nRetvalBunga = nRetvalBunga - CDbl(GetNull(db.Rows(0).Item("Bunga")))
            End If
        Else
            cField = "ifnull(Tgl,'9999-12-31'),ifnull(sum(DPokok),0) as Pokok,ifnull(sum(DBunga),0) as Bunga"
            cWhere = ""
            cWhere = "and status = " & eAngsuran.ags_jadwal
            cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = date_format('{1}', '%Y%m')", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "Angsuran", cField, "rekening", , cRekening, cWhere, , , "rekening")
            If db.Rows.Count > 0 Then
                nRetvalPokok = CDbl(GetNull(db.Rows(0).Item("Pokok")))
                nRetvalBunga = CDbl(GetNull(db.Rows(0).Item("Bunga")))
            End If
            cField = "count(rekening) as jumlah"
            cWhere = ""
            cWhere = "and status = " & eAngsuran.ags_jadwal
            cWhere = String.Format("{0} and date_format(tgl,'%Y%m') <= date_format('{1}', '%Y%m')", cWhere, formatValue(dTgl, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "Angsuran", cField, "rekening", , cRekening, cWhere, , , "rekening")
            If db.Rows.Count > 0 Then
                nKe = CDbl(GetNull(db.Rows(0).Item("Jumlah")))
            End If
        End If
        nPokok = Max(nRetvalPokok, 0)
        nBunga = Max(nRetvalBunga, 0)
    End Sub

    Sub UpdAngsuranInstansi(ByVal cCabang As String, ByVal cFaktur As String, ByVal dTgl As Date,
                            ByVal cRekening As String, ByVal nKe As Double, ByVal nPokok As Double,
                            ByVal nBunga As Double, ByVal cKeterangan As String, ByVal cInstansi As String,
                            Optional ByVal lUpdateBukuBesar As Boolean = True,
                            Optional ByVal lDeleteWhenExist As Boolean = False,
                            Optional ByVal cUser As String = "", Optional ByVal cKas As String = "K")

        Dim db As New DataTable
        Dim cKeteranganTabungan As String
        Dim cRekeningTabungan As String
        Dim cKodeTransaksi As String

        If lDeleteWhenExist Then
            ' Hapus Data Angsuran
            DelAngsuranInstansi(cFaktur)
        End If

        If cUser = "" Then
            cUser = cUserName
        End If

        ' Update Table Angsuran
        Dim vaField() As Object = {"Faktur", "Tgl", "Rekening", "Ke",
                                    "Pokok", "Bunga", "DateTime", "UserName",
                                    "CabangEntry", "Instansi", "kas"}
        Dim vaValue() As Object = {cFaktur, dTgl, cRekening, nKe,
                                    nPokok, nBunga, Format(Now, "yyyy-mm-dd hh:mm:ss"), cUser,
                                    cCabang, cInstansi, cKas}
        objData.Add(GetDSN, "AngsuranTemp", vaField, vaValue)

        If cKas = "T" Then
            cKodeTransaksi = ""
            cRekeningTabungan = ""
            db = objData.Browse(GetDSN, "Debitur", , "Rekening", , cRekening)
            If db.Rows.Count > 0 Then
                cRekeningTabungan = GetNull(db.Rows(0).Item("RekeningTabungan"), "").ToString
                db = objData.Browse(GetDSN, "Tabungan", , "Rekening", , cRekeningTabungan)
                If db.Rows.Count > 0 Then
                    cKodeTransaksi = IIf(cKas = "K", aCfg(eCfg.msKodePenarikanTunai), aCfg(eCfg.msKodePenarikanPemindahBukuan)).ToString
                    cKeteranganTabungan = "Penarikan Untuk Angsuran Instansi Rek. " & cRekening
                    UpdMutasiTabungan(cCabang, cKodeTransaksi, cFaktur, dTgl, cRekeningTabungan, nPokok + nBunga, True, cKeteranganTabungan, False, , , cUser)
                End If
            End If
        End If

        ' 1. Update Rekening Buku Besar
        If lUpdateBukuBesar Then
            UpdRekAngsuranInstansi(cFaktur)
        End If
    End Sub

    Sub DelAngsuranInstansi(ByVal cFaktur As String)
        If Trim(cFaktur) <> "" Then
            ' 1. Hapus Angsuran
            ' 2. Hapus Buku Besar

            objData.Delete(GetDSN, "AngsuranTemp", "faktur", , cFaktur)

            DelBukuBesar(cFaktur)
        End If
    End Sub

    Sub UpdRekAngsuranInstansi(ByVal cFaktur As String)
        Dim nAngsuran As Double
        Dim cRekKas As String
        Dim cCabang As String
        Dim lPusat As Boolean
        Dim dbData As New DataTable
        Dim lAntarKantor As Boolean
        Dim cRekeningAntarKantor As String
        Dim cRekening As String
        Dim cRekJurnal As String
        Dim cRekJurnal1 As String
        Dim cKodeCabang As String
        Dim cInduk As String = ""
        Dim lInduk As Boolean
        Dim lBelumLunas As Boolean
        Dim nBakiDebet As Double
        Dim nBakiDebetSebelumnya As Double
        Dim x As TypeKolektibilitas = Nothing
        Dim dTglAkhir As Date
        Dim db As New DataTable
        Dim dbPusat As New DataTable
        Dim nDKas As Double, nKKas As Double, nKas As Double
        Dim dTglTemp As Date

        ' Hapus Data Lama
        objData.Delete(GetDSN, "BukuBesar", "faktur", , cFaktur)

        Dim cFields As String = "a.Rekening,a.Tgl,ifnull(a.Pokok,0) as Pokok,ifnull(a.Bunga,0) as Bunga,d.kode,a.id,"
        cFields = cFields & "a.cabangentry,ifnull(r.Nama,'') as NamaDebitur,d.rekeningtabungan,"
        cFields = cFields & "ifnull(g.RekeningTitipan,'') as RekeningTitipan,"
        cFields = cFields & "a.username as username,d.StatusPencairan,a.datetime,a.instansi,a.Kas"
        Dim vaJoin() As Object = {"Left Join Debitur d on a.Rekening = d.Rekening",
                                  "Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        dbData = objData.Browse(GetDSN, "AngsuranTemp a", cFields, "a.Faktur", , cFaktur, " Group by a.Faktur,a.Tgl", , vaJoin)
        If dbData.Rows.Count > 0 Then
            For n As Integer = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    nAngsuran = CDbl(.Item("Pokok")) + CDbl(.Item("Bunga"))
                    nDKas = CDbl(IIf(nAngsuran > 0, nAngsuran, 0))
                    nKKas = CDbl(IIf(nAngsuran < 0, -nAngsuran, 0))
                    '      cRekKas = IIf(dbData!Kas = "K", cKasTeller, aCfg(msRekeningPemindahBukuan))

                    If .Item("Kas").ToString = "K" Then
                        cRekKas = cKasTeller
                    ElseIf .Item("Kas").ToString = "T" Then
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                        cFields = "g.rekening"
                        vaJoin = {"left join golongantabungan g on g.kode = t.golongantabungan"}
                        db = objData.Browse(GetDSN, "tabungan t", cFields, "t.rekening", , .Item("RekeningTabungan").ToString, , , vaJoin)
                        If db.Rows.Count > 0 Then
                            cRekKas = GetNull(db.Rows(0).Item("Rekening"), cRekKas).ToString
                        End If
                    Else
                        cRekKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                    End If

                    'If GetNull(.Item("Instansi")).ToString = "022" And formatValue(.Item("Tgl"), formatType.dd_MM_yyyy) = "04-09-2013" And Debugger.IsAttached Then Stop
                    If Debugger.IsAttached Then
                        db = objData.Browse(GetDSN, "Username", , "Username", , .Item("UserName").ToString)
                        If db.Rows.Count > 0 Then
                            cRekKas = GetNull(db.Rows(0).Item("KasTeller"), "").ToString
                        End If
                    End If
                    cCabang = .Item("CabangEntry").ToString
                    cRekening = .Item("Rekening").ToString
                    dTglTemp = CDate(.Item("Tgl"))

                    cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                    lPusat = False
                    dbPusat = objData.Browse(GetDSN, "cabang", "Kode,JenisKantor,Induk", "Kode", , cKodeCabang)
                    If dbPusat.Rows.Count > 0 Then
                        cInduk = dbPusat.Rows(0).Item("Induk").ToString
                        If dbPusat.Rows(0).Item("JenisKantor").ToString = "P" Then lPusat = True
                    End If

                    lAntarKantor = False
                    cRekeningAntarKantor = aCfg(eCfg.msRekeningAntarKantorPasiva).ToString
                    If lPusat Then
                        cRekeningAntarKantor = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString
                    End If

                    ' jika ada transaksi dari kantor kas ke kantor cabang maka
                    ' transaksi yang terjadi bukan transaksi antar kantor seperti
                    ' antar kantor pusat dan cabang
                    If cInduk <> "" Then
                        lInduk = True
                    Else
                        lInduk = False
                    End If

                    dTglAkhir = EOM(DateAdd("m", -1, dTglTemp))
                    GetTunggakan(cRekening, dTglAkhir, x, , , True, , .Item("Kode").ToString)
                    nBakiDebet = GetBakiDebet(cRekening, EOM(dTglTemp))
                    nBakiDebetSebelumnya = GetBakiDebet(cRekening, EOM(dTglTemp), True, , CDbl(.Item("Id")) - 1)
                    Dim nBakiDebet1 As Double = GetBakiDebet(cRekening, dTglTemp)
                    Dim nBakiDebetSebelumnya1 As Double = GetBakiDebet(cRekening, dTglTemp, True, , CDbl(.Item("Id")) - 1)
                    If nBakiDebet1 = 0 And nBakiDebetSebelumnya1 > 0 Then
                        nBakiDebetSebelumnya = nBakiDebetSebelumnya1
                    End If
                    lBelumLunas = False
                    If nBakiDebet <> 0 And x.nKolek <= 1 And GetNull(.Item("statuspencairan")).ToString = "1" Then
                        lBelumLunas = True
                    Else
                        If x.cCaraPerhitungan = "7" Then
                            If nBakiDebet = 0 And x.nKolek <= 1 And GetNull(.Item("statuspencairan")).ToString = "1" And formatValue(.Item("TglPelunasan"), formatType.yyyy_MM_dd) >= "9999-99-99" Then
                                lBelumLunas = True
                            End If
                        End If
                    End If
                    If nBakiDebet = 0 Then
                        If nBakiDebetSebelumnya <> 0 Then
                            If nBakiDebetSebelumnya <> 0 And x.nKolek <= 1 And GetNull(.Item("statuspencairan")).ToString = "1" Then
                                lBelumLunas = True
                            Else
                                If x.cCaraPerhitungan = "7" Then
                                    If nBakiDebetSebelumnya = 0 And x.nKolek <= 1 And GetNull(.Item("statuspencairan")).ToString = "1" And formatValue(.Item("TglPelunasan"), formatType.yyyy_MM_dd) >= "9999-99-99" Then
                                        lBelumLunas = True
                                    End If
                                End If
                            End If
                        End If
                    End If

                    ' jika ada transaksi antar kantor
                    If Not IsSatuCabang(cCabang, cRekening.Substring(0, 2)) Then 'If cCabang <> cKodeCabang And Not lInduk Then
                        lAntarKantor = True
                        ' jika pada saat diposting cabang entry sama dengan
                        ' kode cabang yang aktif ( di cfgsystem )

                        ' Jurnal
                        ' Dr. PB / Kas                                nKas
                        ' Dr. Rekening Antar Kantor Aktiva/Pasiva     nKas
                        '   Cr. PB / Kas                                nKas
                        '   Cr. Rekening Antar Kantor Aktiva/Pasiva     nKas
                        nKas = CDbl(IIf(nDKas = 0, nKKas, nDKas))
                        cRekJurnal = IIf(nDKas = 0, cRekeningAntarKantor, cRekKas).ToString
                        cRekJurnal1 = IIf(nKKas = 0, cRekeningAntarKantor, cRekKas).ToString

                        UpdBukuBesar(CDbl(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTglTemp, cRekJurnal, "Angsuran Kredit Instansi an. " & GetNull(.Item("NamaDebitur"), "").ToString, nKas, , , .Item("UserName").ToString)
                        UpdBukuBesar(CDbl(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTglTemp, cRekJurnal1, "Angsuran Kredit Instansi an. " & GetNull(.Item("NamaDebitur"), "").ToString, , nKas, , .Item("UserName").ToString)
                    End If

                    ' PB / Kas    xxxxxxx           nDKas
                    '     Pokok           xxxxxxx       Pokok
                    '     Bunga           xxxxxxx       Bunga

                    '      If cCabang <> Left(cRekening, 2) And Not lInduk Then
                    If Not IsSatuCabang(cCabang, cRekening.Substring(0, 2)) Then 'If cCabang <> cKodeCabang And Not lInduk Then
                        cRekKas = cRekeningAntarKantor
                    End If

                    '      If Not lAntarKantor Or cKodeCabang <> cCabang Then
                    UpdBukuBesar(CDbl(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTglTemp, cRekKas, "Angsuran Kredit Instansi an. " & GetNull(.Item("NamaDebitur"), "").ToString, nAngsuran, , , .Item("UserName").ToString)

                    UpdBukuBesar(CDbl(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, dTglTemp, GetNull(.Item("RekeningTitipan"), "").ToString, "Angsuran Kredit Instansi an. " & .Item("NamaDebitur").ToString, , CDbl(.Item("Pokok")) + CDbl(.Item("Bunga")), , .Item("UserName").ToString)
                    '          UpdBukuBesar msAngsuranKredit, dbdata!Id, dbdata!CabangEntry, cFaktur, dbdata!tgl, GetNull(dbdata!RekeningTitipan, ""), "Bunga Kredit Instansi an. " & dbdata!NamaDebitur, , dbdata!Bunga, "K", , dbdata!UserName
                    '      End If
                End With
            Next
        End If
        dbData.Dispose()
        dbPusat.Dispose()
    End Sub

    'Function Untuk Mengambil Angsuran Pokok
    Function GetAngsuranPokok(ByVal cRekening As String, ByVal dTanggal As Date) As Double
        Dim nRetval As Double
        Dim db As New DataTable

        Dim cWhere As String = "and status=" & eAngsuran.ags_jadwal
        cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = '{1}'", cWhere, Format(dTanggal, "YYYYmm"))
        db = objData.Browse(GetDSN, "Angsuran", "DPokok", "Rekening", , cRekening, cWhere)
        If db.Rows.Count > 0 Then
            nRetval = CDbl(GetNull(db.Rows(0).Item("DPokok")))
        End If
        GetAngsuranPokok = Math.Round(nRetval, 0)
    End Function

    'Function untuk mendapatkan Bunga Angsuran Per Bulan
    Function GetAngsuranBunga(ByVal cRekening As String, ByVal dTanggal As Date) As Double
        Dim nRetval As Double
        Dim db As New DataTable

        Dim cWhere As String = "and status=" & eAngsuran.ags_jadwal
        cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = '{1}'", cWhere, Format(dTanggal, "YYYYmm"))
        db = objData.Browse(GetDSN, "Angsuran", "DBunga", "Rekening", , cRekening, cWhere)
        If db.Rows.Count > 0 Then
            nRetval = CDbl(GetNull(db.Rows(0).Item("DBunga")))
        End If
        GetAngsuranBunga = Math.Round(nRetval, 0)
    End Function

    'Function Untuk Mengambil Angsuran Pokok Dan Bunga
    Sub GetAngsuranPokokBunga(ByVal cRekening As String, ByVal dTanggal As Date, nPokok As Double, nBunga As Double)
        Dim nRetvalPokok As Double
        Dim nRetvalBunga As Double
        Dim db As New DataTable

        Dim cWhere As String = "and status=" & eAngsuran.ags_jadwal
        cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = '{1}'", cWhere, Format(dTanggal, "YYYYmm"))
        db = objData.Browse(GetDSN, "Angsuran", "DPokok,DBunga", "Rekening", , cRekening, cWhere)
        If db.Rows.Count > 0 Then
            nRetvalPokok = CDbl(GetNull(db.Rows(0).Item("DPokok")))
            nRetvalBunga = CDbl(GetNull(db.Rows(0).Item("DBunga")))
        End If
        nPokok = Math.Round(nRetvalPokok, 0)
        nBunga = Math.Round(nRetvalBunga, 0)
    End Sub

    Function getDataBungaRK(ByVal cRekening As String, ByVal nSukuBunga As Single, ByVal dTgl As Date, ByVal dTglRealisasi As Date) As Double
        Dim dJthTmpBulanLalu As Date = DateAdd("m", -1, dTgl)
        Dim nHari As Integer = CInt(DateDiff("d", dJthTmpBulanLalu, dTgl))
        Dim n As Double
        Dim nBakiDebet As Double
        Dim nBungaHarian As Double
        Dim nBungaBulanan As Double
        Dim dTglHarian As Date
        Dim nTotal As Double
        Dim nStart As Integer
        'jika realisasi di bulan yang sama maka jumlah harinya adalah mulai tgl realisasi
        'jika tidak, maka dihitung mulai 1
        If Month(dTgl) = Month(dTglRealisasi) And Year(dTgl) = Year(dTglRealisasi) Then
            nStart = dTglRealisasi.Day
            nHari = dTgl.Day
        End If
        For n = 1 To nHari
            dTglHarian = DateAdd("d", n, dJthTmpBulanLalu)
            nBakiDebet = GetBakiDebet(cRekening, dTglHarian)
            nTotal = nBakiDebet
            nBungaHarian = (nTotal * nSukuBunga / 100) / 360
            nBungaBulanan = nBungaBulanan + nBungaHarian
        Next
        getDataBungaRK = GetPembulatan500(nBungaBulanan)
    End Function
End Module
