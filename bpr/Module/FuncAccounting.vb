﻿Imports bpr.MySQL_Data_Library

Module FuncAccounting
    ReadOnly objData As New data()

    Public Enum eTrigger
        msTabungan = 1
        msRealisasiKredit = 2
        msAngsuranKredit = 3
        msTitipanAngsuran = 4
        msKasKeluar = 5
        msKasMasuk = 6
        msDeposito = 7
        msRekeningRRP = 8
        msJurnalLain = 9
        msPenyusutanAktiva = 10
        msAmortisasiProvisi = 11
        msPenambahanPlafond = 12
        msSaldoAwalRekening = 13
        msCadanganBungaTabungan = 14
        msCadanganBungaDeposito = 15
        msAkhirTahun = 16
        msAccualKredit = 17
        msAccrualDeposito = 18
        msJurnalLainEtab = 19
        msAdmProvisiPerpanjanganKredit = 20
    End Enum

    Public Enum eDeposito
        trPembukaan = 1
        trPencairanPokok = 2
        trPencairanBunga = 3
        trPinalti = 4
        trKoreksiBunga = 5
    End Enum

    Sub UpdBukuBesar(ByVal nID As Double, ByVal cCabang As String, ByVal cFaktur As String,
                     ByVal dTgl As Date, ByVal cRekening As String, Optional ByVal cKeterangan As String = "",
                     Optional ByVal nDebet As Double = 0, Optional ByVal nKredit As Double = 0,
                     Optional ByVal cNow As String = "", Optional ByVal cUser As String = "")
        If cNow = "" Then
            cNow = SNow()
        Else
            cNow = Format(cNow, "yyyy-MM-dd hh:mm:ss")
        End If
        If cCabang = "" Then
            cCabang = Mid(cFaktur, 3, 2)
        End If
        If cUser = "" Then
            cUser = cUserName
        End If
        If dTgl >= DateSerial(2013, 8, 1) Then
            If cRekening <> "" Then
                If nDebet <> 0 Or nKredit <> 0 Then
                    Dim vaField() As Object = {"Urut", "Faktur", "CabangEntry", "Tgl", "Rekening", _
                                               "Keterangan", "Debet", "Kredit", "UserName", _
                                               "DateTime"}
                    Dim vaValue() As Object = {nID, cFaktur, cCabang, dTgl, cRekening, _
                                               cKeterangan, nDebet, nKredit, cUser, _
                                               cNow}
                    objData.Add(GetDSN, "BukuBesar", vaField, vaValue)
                End If
            End If
        End If
    End Sub

    Sub DelBukuBesar(ByVal cFaktur As String)
        If cFaktur <> "" Then
            objData.Delete(GetDSN, "BukuBesar", "faktur", , cFaktur)
        End If
    End Sub

    Sub UpdRekJurnal(ByVal cFaktur As String)
        Dim dbData As New DataTable
        ' Hapus Buku Besar
        DelBukuBesar(cFaktur)

        ' Ambil Data Jurnal
        Dim vaJoin() As Object = {"left join totjurnal t on j.faktur=t.faktur"}
        Const cField As String = "j.*,t.username"
        dbData = objData.Browse(GetDSN, "Jurnal j", cField, "j.Faktur", , cFaktur, , , vaJoin)
        If dbData.rows.count > 0 Then
            For n As Integer = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    UpdBukuBesar(CDbl(.Item("Id")), .Item("CabangEntry").ToString, cFaktur, CDate(.Item("Tgl")), .Item("Rekening").ToString,
                                 .Item("Keterangan").ToString, CDbl(.Item("debet")), CDbl(.Item("Kredit")), ,
                                 GetNull(.Item("UserName")).ToString)
                End With
            Next
        End If
        dbData.Dispose()
    End Sub

    Sub UpdTotJurnalLainLain(ByVal cFaktur As String, ByVal dTgl As Date, ByVal cUser As String,
                             ByVal cDateTime As String, ByVal nTotalDebet As Double, ByVal nTotalKredit As Double,
                             ByVal cCabangEntry As String, Optional ByVal cKeterangan As String = "",
                             Optional ByVal cStatusJurnal As String = "0")
        'hapus data lama dalam totjurnal
        objData.Delete(GetDSN, "totjurnal", "Faktur", , cFaktur)

        'Hapus Data Lama dalam JURNAL
        objData.Delete(GetDSN, "JURNAL", "Faktur", , cFaktur)

        Dim vaField() As Object = {"Faktur", "Tgl", "UserName", "DateTime",
                                   "TotalDebet", "TotalKredit", "CabangEntry",
                                   "Keterangan", "status"}
        Dim vaValue() As Object = {cFaktur, dTgl, cUser, cDateTime,
                                   nTotalDebet, nTotalKredit, cCabangEntry,
                                   cKeterangan, cStatusJurnal}
        objData.Update(GetDSN, "TOTJURNAL", String.Format("Faktur = '{0}'", cFaktur), vaField, vaValue)
    End Sub

    Sub UpdJurnalLainLain(ByVal cFaktur As String, ByVal dTgl As Date, ByVal cRekening As String, _
                          ByVal cKeterangan As String, ByVal nDebet As Double, ByVal nKredit As Double, _
                          ByVal cCabangEntry As String)
        If nDebet <> 0 Or nKredit <> 0 Then
            'Simpan ke JURNAL (Detail)
            Dim vaField() As Object = {"Faktur", "tgl", "Rekening", "Keterangan", "Debet", "Kredit", "CabangEntry"}
            Dim vaValue() As Object = {cFaktur, dTgl, cRekening, cKeterangan, nDebet, nKredit, cCabangEntry}
            objData.Add(GetDSN, "JURNAL", vaField, vaValue)
        End If
    End Sub

    Function GetPenyusutan(ByVal nHarga As Double, ByVal nResidu As Double, ByVal nLama As Integer, ByVal nKe As Integer,
                           ByVal nTarif As Single, ByVal cCaraPerhitungan As String) As Double
        Dim nRetval As Double
        Dim nPersentase As Single
        Dim a As Double
        Dim nAkarPangkat As Double
        Dim nPenyusutan As Double
        Dim nTahun As Integer
        Dim n As Integer
        Dim nPenyusutanPerTahun As Double
        Dim nTahunKe As Integer

        Select Case cCaraPerhitungan
            Case Is = "1" ' garis lurus
                If nKe >= 0 Then
                    If nKe >= nLama Then
                        nRetval = nHarga - nResidu
                    Else
                        nRetval = ((nHarga - nResidu) / nLama) * nKe
                    End If
                End If
            Case Is = "2" ' saldo menurun
                If nKe >= 0 Then
                    nTahun = CInt(nLama / 12)
                    nTahunKe = CInt(nKe / 12)
                    nTahunKe = Int(nTahunKe)
                    a = nResidu / nHarga
                    '        nAkarPangkat = Excel.WorksheetFunction.Power(a, 1 / nTahun)
                    nPersentase = CSng(1 - Math.Round(nAkarPangkat, 2))
                    For n = 1 To nTahunKe
                        If n = 1 Then
                            nPenyusutan = nHarga
                        Else
                            nPenyusutan = nRetval
                        End If
                        nPenyusutanPerTahun = (nTarif / 100) * nPenyusutan
                        nRetval = nPenyusutanPerTahun / 12
                    Next
                End If
            Case Else
                Exit Select
        End Select
        GetPenyusutan = Math.Round(nRetval, 0)
    End Function

    Function GetJadwalAktiva(ByVal nHarga As Double, dTglAwal As Date, nPenyusutan As Double, nJumlahKonversi As Double, dTglKonversi As Date,
                             dAkhir As Date, ByVal cJenisPenyusutan As String, ByVal nResidu As Double, ByVal nTarif As Single) As DataTable
        Dim vaArray As New DataTable
        Dim n As Integer
        Dim nJumlah As Double
        Dim dTgl As Date
        Dim nTahun As Integer
        Dim nAkarPangkat As Double
        Dim nPersentase As Single
        Dim a As Double
        Dim nPenyusutanPerTahun As Double
        Dim nTahunKe As Integer
        Dim i As Double
        Dim nRetval As Double
        Dim nSaldo As Double = nHarga - nResidu
        Dim nPenyusutanSblmKonversi As Double = 0
        Dim row As DataRow
        If cJenisPenyusutan = "1" Then ' garis lurus
            If nJumlahKonversi > 0 Then
                nPenyusutanSblmKonversi = Math.Round((nHarga - nResidu - nJumlahKonversi) / (DateDiff("m", dTglAwal, dTglKonversi) + 1))
            End If
            vaArray.Reset()
            AddColumn(vaArray, "Tgl", System.Type.GetType("System.DateTime"))
            AddColumn(vaArray, "Penyusutan", System.Type.GetType("System.Double"))
            AddColumn(vaArray, "Saldo", System.Type.GetType("System.Double"))
            nJumlah = DateDiff("m", dTglAwal, dAkhir)
            row = vaArray.NewRow
            row(0) = #1/1/1900#
            row(1) = 0
            row(2) = nSaldo
            vaArray.Rows.Add(row)
            For n = 1 To CInt(nJumlah + 1)
                dTgl = DateAdd("m", n - 1, dTglAwal)
                row = vaArray.NewRow()
                row(0) = formatValue(EOM(dTgl), formatType.yyyy_MM_dd)
                If nJumlahKonversi > 0 And EOM(dTgl) <= EOM(dTglKonversi) Then
                    If EOM(dTgl) = EOM(dTglKonversi) Then
                        row(1) = nPenyusutanSblmKonversi ' + (nJumlahKonversi - (nSaldo + nPenyusutanSblmKonversi))
                    Else
                        row(1) = nPenyusutanSblmKonversi
                    End If
                Else
                    row(1) = nPenyusutan
                End If
                nSaldo = nSaldo - CDbl(row(1))
                If n = nJumlah + 1 Then
                    row(1) = CDbl(row(1)) - Math.Abs(nSaldo) - nResidu
                    nSaldo = nResidu
                ElseIf EOM(dTgl) = EOM(dTglKonversi) And nJumlahKonversi > 0 Then
                    row(1) = CDbl(row(1)) + (nJumlahKonversi - nSaldo)
                    nSaldo = nJumlahKonversi
                End If
                row(2) = nSaldo
                vaArray.Rows.Add(row)
            Next
        ElseIf cJenisPenyusutan = "2" Then ' menurun
            vaArray.Reset()
            AddColumn(vaArray, "Tgl", System.Type.GetType("System.DateTime"))
            AddColumn(vaArray, "Penyusutan", System.Type.GetType("System.Double"))
            AddColumn(vaArray, "Saldo", System.Type.GetType("System.Double"))
            nJumlah = DateDiff("m", dTglAwal, dAkhir)
            row = vaArray.NewRow
            row(0) = #1/1/1900#
            row(1) = 0
            row(2) = nSaldo
            vaArray.Rows.Add(row)
            For i = 1 To nJumlah + 1
                row = vaArray.NewRow()
                nTahun = CInt(nJumlah / 12)
                nTahunKe = CInt(i / 12)
                nTahunKe = CInt(nTahunKe)
                a = nResidu / nHarga
                '      nAkarPangkat = Excel.WorksheetFunction.Power(a, 1 / nTahun)
                nPersentase = CSng(1 - Math.Round(nAkarPangkat, 2))
                For n = 1 To nTahunKe
                    If n = 1 Then
                        nPenyusutan = nHarga
                    Else
                        nPenyusutan = nRetval
                    End If
                    nPenyusutanPerTahun = (nTarif / 100) * nPenyusutan
                    nRetval = nPenyusutanPerTahun / 12
                Next
                row(0) = formatValue(EOM(dTgl), formatType.yyyy_MM_dd)
                row(1) = nRetval
                nSaldo = nSaldo - CDbl(row(1))
                row(2) = nSaldo
                vaArray.Rows.Add(row)
            Next
        End If
        GetJadwalAktiva = vaArray
    End Function

    Sub UpdRekPembelianAktiva(ByVal cFaktur As String, ByVal cKode As String, ByVal dTgl As Date)
        Dim db As New DataTable
        Dim nNilaiPerolehan As Double
        Dim cRekeningDebet As String
        Dim cRekeningKredit As String
        Dim cKas As String
        Dim cKeterangan As String
        Dim nID As Integer
        Dim vaField() As Object, vaValue() As Object
        Dim cCabang As String
        Const cField As String = "a.*,g.keterangan,g.rekeningdebet,g.rekeningkredit"
        Dim vaJoin As Object() = {"Left Join GolonganAktiva g on a.golongan=g.kode"}
        db = objData.Browse(GetDSN, "Aktiva a", cField, "a.Kode", , cKode, , , vaJoin)
        If db.Rows.Count > 0 Then
            ' kas                         xxxx
            ' Nilai Perolehan             xxxx
            With db.Rows(0)
                cCabang = .Item("CabangEntry").ToString

                nNilaiPerolehan = CDbl(GetNull(.Item("HargaPerolehan")))

                cRekeningDebet = GetNull(.Item("RekeningDebet")).ToString
                cRekeningKredit = GetNull(.Item("RekeningKredit")).ToString
                cKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                cKeterangan = "Pembelian Aktiva"

                ' hapus data lama
                objData.Delete(GetDSN, "totJurnal", "Faktur", , cFaktur)
                objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur)
                DelBukuBesar(cFaktur)

                ' update ke totjurnal
                nID = CInt(GetID(dTgl))
                vaField = {"ID", "Faktur", "Tgl", "TotalDebet", "TotalKredit", "Keterangan", "UserName", "DateTime", "CabangEntry"}
                vaValue = {nID, cFaktur, dTgl, nNilaiPerolehan, nNilaiPerolehan, cKeterangan, cUserName, SNow(), cCabang}
                objData.Add(GetDSN, "TotJurnal", vaField, vaValue)

                ' update ke jurnal
                UpdJurnal(cCabang, cFaktur, dTgl, cRekeningKredit, cKeterangan, nNilaiPerolehan)
                UpdJurnal(cCabang, cFaktur, dTgl, cKas, cKeterangan, , nNilaiPerolehan)

                ' update bukubesar
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekeningKredit, cKeterangan, nNilaiPerolehan)
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cKas, cKeterangan, , nNilaiPerolehan)
            End With
            db.Dispose()
        End If
    End Sub

    Sub UpdRekPenjualanAktiva(ByVal cFaktur As String, ByVal cKode As String, ByVal dTgl As Date)
        Dim db As New DataTable
        Dim nHargaJual As Double
        Dim nNilaiResidu As Double
        Dim nNilaiPerolehan As Double
        Dim nLabaPenjualan As Double
        Dim cRekeningDebet As String
        Dim cRekeningKredit As String
        Dim cKas As String
        Dim cRekLaba As String
        Dim nID As Integer
        Dim vaField() As Object, vaValue() As Object
        Dim cCabang As String

        Const cField As String = "a.*,g.keterangan"
        Dim vaJoin() As Object = {"Left Join GolonganAktiva g on a.golongan=g.kode"}
        Const cWhere As String = " and a.status='2'"
        db = objData.Browse(GetDSN, "Aktiva a", cField, "a.Kode", , cKode, cWhere, , vaJoin)
        If db.Rows.Count > 0 Then
            ' kas                         xxxx
            ' nilai residu                xxxx
            ' Nilai Perolehan             xxxx
            ' Laba Penjualan Aktiva       xxxx
            With db.Rows(0)
                cCabang = .Item("CabangEntry").ToString
                nHargaJual = CDbl(GetNull(.Item("HargaJual")))
                nNilaiResidu = CDbl(GetNull(.Item("residu")))
                nNilaiPerolehan = CDbl(GetNull(.Item("HargaPerolehan")))
                nLabaPenjualan = (nHargaJual + nNilaiResidu) - nNilaiPerolehan

                cRekeningDebet = aCfg(eCfg.msKodeRekeningDebetAktivaTetap).ToString
                cRekeningKredit = aCfg(eCfg.msKodeRekeningKreditAktivaTetap).ToString
                cKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                cRekLaba = aCfg(eCfg.msRekPenjualanAktiva).ToString
                Const cKeterangan As String = "Penjualan Aktiva"

                ' hapus data lama
                objData.Delete(GetDSN, "totJurnal", "Faktur", , cFaktur)
                objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur)
                DelBukuBesar(cFaktur)

                ' update ke totjurnal
                nID = CInt(GetID(dTgl))
                vaField = {"ID", "Faktur", "Tgl", "TotalDebet", "TotalKredit", "Keterangan", "UserName", "DateTime", "CabangEntry"}
                vaValue = {nID, cFaktur, dTgl, nHargaJual + nNilaiResidu, nNilaiPerolehan + nLabaPenjualan, cKeterangan, cUserName, SNow(), cCabang}
                objData.Add(GetDSN, "TotJurnal", vaField, vaValue)

                ' update ke jurnal
                UpdJurnal(cCabang, cFaktur, dTgl, cKas, cKeterangan, nHargaJual)
                UpdJurnal(cCabang, cFaktur, dTgl, cRekeningDebet, cKeterangan, nNilaiResidu)
                UpdJurnal(cCabang, cFaktur, dTgl, cRekeningKredit, cKeterangan, , nNilaiPerolehan)
                UpdJurnal(cCabang, cFaktur, dTgl, cRekLaba, cKeterangan, , nLabaPenjualan)

                ' update bukubesar
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cKas, cKeterangan, nHargaJual)
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekeningDebet, cKeterangan, nNilaiResidu)
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekeningKredit, cKeterangan, , nNilaiPerolehan)
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekLaba, cKeterangan, , nLabaPenjualan)
            End With
            db.Dispose()
        End If
    End Sub

    Sub UpdJurnal(ByVal cCabang As String, ByVal cFaktur As String, ByVal dTgl As Date, ByVal cRekening As String,
                  Optional ByVal cKeterangan As String = "", Optional ByVal nDebet As Double = 0, Optional ByVal nKredit As Double = 0,
                  Optional ByVal cNow As String = "")
        cNow = IIf(cNow = "", SNow, cNow).ToString
        If nDebet <> 0 Or nKredit <> 0 Then
            Dim vaField() As Object = {"Faktur", "Tgl", "Rekening", "Keterangan", "Debet", "Kredit", "CabangEntry"}
            Dim vaValue() As Object = {cFaktur, dTgl, cRekening, cKeterangan, nDebet, nKredit, cCabang}
            objData.Add(GetDSN, "Jurnal", vaField, vaValue)
        End If
    End Sub

    Sub UpdRekBreakAktiva(ByVal cFaktur As String, ByVal cKode As String, ByVal dTgl As Date)
        Dim db As New DataTable
        Dim nHarga As Double
        Dim cRekeningDebet As String
        Dim cRekeningKredit As String
        Dim cKas As String
        Dim nID As Integer
        Dim vaField() As Object, vaValue() As Object
        Dim cCabang As String

        Const cField As String = "a.*,g.keterangan"
        Dim vaJoin() As Object = {"Left Join GolonganAktiva g on a.golongan=g.kode"}
        Const cWhere As String = " and status='3'"
        db = objData.Browse(GetDSN, "Aktiva a", cField, "a.Kode", , cKode, cWhere, , vaJoin)
        If db.Rows.Count > 0 Then
            ' nilai residu                xxxx
            ' Nilai Perolehan             xxxx
            With db.Rows(0)
                cCabang = .Item("CabangEntry").ToString
                nHarga = CDbl(GetNull(.Item("HargaJual")))

                cRekeningDebet = GetNull(.Item("RekeningDebet")).ToString
                cRekeningKredit = GetNull(.Item("RekeningKredit")).ToString
                cKas = aCfg(eCfg.msRekeningPemindahBukuan).ToString
                Const cKeterangan As String = "Break Aktiva"

                ' hapus data lama
                objData.Delete(GetDSN, "totJurnal", "Faktur", , cFaktur)
                objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur)
                DelBukuBesar(cFaktur)

                ' update ke totjurnal
                nID = CInt(GetID(dTgl))
                vaField = {"ID", "Faktur", "Tgl", "TotalDebet", "TotalKredit", "Keterangan", "UserName", "DateTime", "CabangEntry"}
                vaValue = {nID, cFaktur, dTgl, nHarga, nHarga, cKeterangan, cUserName, SNow(), cCabang}
                objData.Add(GetDSN, "TotJurnal", vaField, vaValue)

                ' update ke jurnal
                UpdJurnal(cCabang, cFaktur, dTgl, cRekeningDebet, cKeterangan, nHarga)
                UpdJurnal(cCabang, cFaktur, dTgl, cRekeningKredit, cKeterangan, , nHarga)

                ' update bukubesar
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekeningDebet, cKeterangan, nHarga)
                UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTgl, cRekeningKredit, cKeterangan, , nHarga)
            End With
            db.Dispose()
        End If
    End Sub

    Sub PostingRekapBukuBesar(ByVal dAkhir As Date, Optional ByVal cJmlRekening As DevExpress.XtraEditors.LabelControl = Nothing)
        Dim a As Double = 0
        Dim d As Date = BOM(DateAdd("m", -3, dAkhir))
        Dim dbData As New DataTable
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cWhere As String

        dbData = objData.Browse(GetDSN, "BukuBesar", "CabangEntry,Tgl,Rekening,Sum(Debet) as Debet,Sum(Kredit) as Kredit",
                                "Tgl", data.myOperator.GreaterThanEqual, formatValue(d, formatType.yyyy_MM_dd), , , ,
                                "CabangEntry,Rekening,Tgl")
        For n As Integer = 0 To dbData.Rows.Count - 1
            With dbData.Rows(n)
                a = a + 1
                cJmlRekening.Text = a & " Transaksi"

                vaField = {"CabangEntry", "Tgl", "Rekening", "Debet", "Kredit"}
                vaValue = {.Item("CabangEntry").ToString, EOM(CDate(.Item("Tgl"))), .Item("Rekening").ToString, "&Debet+" & Replace(Str(.Item("debet")), ",", "."), "&Kredit+" & Replace(Str(.Item("Kredit")), ",", ".")}
                cWhere = String.Format("CabangEntry = '{0}' and Tgl = '{1}' and Rekening = '{2}'", .Item("CabangEntry"), formatValue(EOM(CDate(.Item("Tgl"))), formatType.yyyy_MM_dd), .Item("Rekening"))
                objData.Update(GetDSN, "BukuBesar_Rekap", cWhere, vaField, vaValue, , , , False)
            End With
        Next
        cJmlRekening.Text = ""
        dbData.Dispose()
    End Sub
End Module
