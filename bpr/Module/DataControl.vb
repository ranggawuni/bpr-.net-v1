﻿
Namespace MySQL_Data_Library
    Public Class DataControl
        Private ReadOnly myConnection As New MySQL_Data_Library.DataBaseConnection()
        Public Function GetDataSet(ByVal cSQL As String) As DataSet
            Using adapter As New MySqlDataAdapter(cSQL, myConnection.OpenConnection)
                Dim myData As New DataSet()
                adapter.Fill(myData, "data")
                Return myData
            End Using
        End Function
    End Class
End Namespace