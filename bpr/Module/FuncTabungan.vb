﻿Imports bpr.MySQL_Data_Library

Module FuncTabungan
    ReadOnly objData As New data()

    Sub UpdMutasiTabungan(ByVal cCabang As String, ByVal cKodeTransaksi As String, _
                          ByVal cFaktur As String, ByVal dTgl As Date, ByVal cRekening As String, _
                          ByVal nMutasi As Double, Optional ByVal lDeleteExistData As Boolean = False, _
                          Optional ByVal cKeterangan As String = "", Optional ByVal lUpdateToBukuBesar As Boolean = True, _
                          Optional ByVal cNow As String = "", Optional ByVal cRekeningJurnal As String = "", _
                          Optional ByVal cUser As String = "", Optional ByVal lRekeningKoran As Boolean = False, _
                          Optional ByVal cCaraPembayaranRK As String = "K", Optional ByVal lRekeningOverDraft As Boolean = False, _
                          Optional ByVal lDeleteExistingBukuBesar As Boolean = True)
        Dim cDK As String = ""
        Dim cKas As String = ""
        Dim nDebet As Double = 0
        Dim nKredit As Double = 0
        If cNow = "" Then
            cNow = SNow()
        End If
        If Right(cNow, 1) = "M" Then cNow = Format(cNow, "yyyy-MM-dd hh:mm:ss")
        If cUser = "" Then
            cUser = cUserName
        End If
        If lDeleteExistData Then
            objData.Delete(GetDSN, "mutasitabungan", "faktur", , cFaktur)
        End If
        If nMutasi <> 0 Then
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "kodetransaksi", , "kode", , cKodeTransaksi)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cDK = .Item("DK").ToString
                    cKas = .Item("Kas").ToString
                    If cKas = "K" Then
                        cRekeningJurnal = cKasTeller
                    End If
                    If Left(cFaktur, 2) = "AG" Then
                        cRekeningJurnal = .Item("rekening").ToString
                    End If
                End With
            End If
            If cDK = "D" Then
                nDebet = nMutasi
                nKredit = 0
            Else
                nDebet = 0
                nKredit = nMutasi
            End If
            Dim vaField() As Object = {"faktur", "tgl", "kodetransaksi", "rekening", "jumlah", _
                                       "keterangan", "DK", "RekeningJurnal", "Debet", "Kredit", _
                                       "UserName", "DateTime", "CabangEntry", "KasRK"}
            Dim vaValue() As Object = {cFaktur, formatValue(dTgl, formatType.yyyy_MM_dd), cKodeTransaksi, cRekening, nMutasi, _
                                       cKeterangan, cDK, cRekeningJurnal, nDebet, nKredit, _
                                       cUser, cNow, cCabang, cCaraPembayaranRK}
            objData.Add(GetDSN, "mutasitabungan", vaField, vaValue)

            If Not lRekeningKoran Then
                If lUpdateToBukuBesar Then
                    updRekTabungan(cFaktur, lDeleteExistingBukuBesar)
                End If
            Else
                If lRekeningOverDraft Then
                    updRekOverDraft(cFaktur)
                Else
                    If lUpdateToBukuBesar Then
                        updPencairanRekeningKoran(cFaktur, cRekening, nMutasi)
                    End If
                End If
            End If
        End If
    End Sub

    Sub updRekTabungan(ByVal cFaktur As String, Optional ByVal lDeleteWhenExist As Boolean = True)
        If lDeleteWhenExist Then
            objData.Delete(GetDSN, "Bukubesar", "faktur", , cFaktur)
        End If
        Dim cSQL As String = ""
        cSQL = cSQL & " Select m.*, t.Rekening as RekeningTabungan,t.Kode,t.TglPenutupan,"
        cSQL = cSQL & " k.Kas,k.Rekening as RekeningKodeTransaksi,k.Keterangan as KeteranganKodeTransaksi,"
        cSQL = cSQL & " g.Rekening as RekeningPerkiraanTabungan,r.Nama as NamaNasabah,g.RekeningBunga "
        cSQL = cSQL & " From MutasiTabungan m"
        cSQL = cSQL & " Left Join Tabungan t on m.Rekening = t.Rekening"
        cSQL = cSQL & " Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan"
        cSQL = cSQL & " Left Join KodeTransaksi k on k.Kode = m.KodeTransaksi"
        cSQL = cSQL & " Left Join RegisterNasabah r on r.Kode = t.Kode"
        cSQL = String.Format("{0} Where m.Faktur='{1}' ", cSQL, cFaktur)
        cSQL = cSQL & " Group by m.Faktur,m.Tgl,m.ID "
        Dim cDataTable As DataTable = objData.SQL(GetDSN, cSQL)
        Dim nBaris As Integer = cDataTable.Rows.Count
        Dim n As Integer = 0
        Do Until n > nBaris
            With cDataTable.Rows(0)
                Dim cCabang As String = .Item("CabangEntry").ToString
                Dim dTgl As Date = CDate(.Item("Tgl").ToString)
                Dim cDK As String = .Item("DK").ToString
                Dim cKas As String = CStr(GetNull(.Item("Kas").ToString))
                Dim nID As Double = CDbl(.Item("ID").ToString)
                Dim cRekeningTabungan As String = .Item("RekeningPerkiraanTabungan").ToString
                Dim nNominalTabungan As Double = CDbl(.Item("Jumlah").ToString)
                Dim lTutup As Boolean = False
                Dim cKeterangan As String = .Item("Keterangan").ToString
                Dim cNamaUser As String = .Item("UserName").ToString
                Dim cRekeningPendapatanSimarmas As String = ""
                Dim cKodeTransaksiTemp As String = CStr(GetNull(.Item("KodeTransaksi").ToString))
                If CDate(.Item("TglPenutupan").ToString) = CDate(.Item("Tgl").ToString) Then
                    lTutup = True
                    cRekeningPendapatanSimarmas = CStr(aCfg(eCfg.msRekeningPendapatanSimarMas))
                End If

                ' ambil rekening dari table kode transaksi
                Dim cRekeningKodeTransaksi As String
                If CStr(GetNull(.Item("RekeningJurnal").ToString, "")) = "" Then
                    cRekeningKodeTransaksi = CStr(GetNull(.Item("RekeningJurnal").ToString, ""))
                Else
                    cRekeningKodeTransaksi = CStr(GetNull(.Item("RekeningKodeTransaksi").ToString, ""))
                End If

                ' Jika Kode Transaksi bunga maka akan melihat Kode Rekening Bunga
                ' pada Table Golongan Tabungan.
                ' Karena kalau tidak maka Rekening bunga akan menjadi satu pada 1 Rekening
                If cKodeTransaksiTemp = CStr(aCfg(eCfg.msKodeBunga)) And Trim(.Item("RekeningBunga").ToString) <> "" Then
                    cRekeningKodeTransaksi = .Item("RekeningBunga").ToString
                End If

                'ambil rekening akuntansi kas teller
                If cKas = "K" Then
                    Dim cDataTableUser As DataTable = objData.Browse(GetDSN, "UserName", , "UserName", , .Item("UserName").ToString)
                    If cDataTableUser.Rows.Count > 0 Then
                        If .Item("KasTeller").ToString <> "" Then
                            cRekeningKodeTransaksi = .Item("KasTeller").ToString
                        End If
                    End If
                End If
                Dim cKodeCabang As String = CStr(aCfg(eCfg.msKodeCabang))
                Dim lPusat As Boolean = False
                Dim lInduk As Boolean
                Dim cInduk As String = ""
                Dim cDataTableKantor As DataTable = objData.Browse(GetDSN, "cabang", "kode,JenisKantor,induk", "kode", , cKodeCabang)
                If cDataTableKantor.Rows.Count > 0 Then
                    With cDataTableKantor.Rows(0)
                        cInduk = .Item("Induk").ToString
                        If .Item("JenisKantor").ToString = "P" Then
                            lPusat = True
                        End If
                    End With
                End If
                Dim cRekeningAntarKantor As String
                If lPusat Then
                    cRekeningAntarKantor = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString
                Else
                    cRekeningAntarKantor = CStr(aCfg(eCfg.msRekeningAntarKantorPasiva))
                End If
                ' jika ada transaksi dari kantor kas ke kantor cabang maka
                ' transaksi yang terjadi bukan transaksi antar kantor seperti
                ' antar kantor pusat dan cabang
                If cInduk <> "" Then
                    lInduk = True
                Else
                    lInduk = False
                End If
                ' jika ada transaksi antar kantor
                Dim cDebet As String = ""
                Dim cKredit As String = ""
                Dim cDebet1 As String = ""
                Dim cKredit1 As String = ""
                If cCabang <> cKodeCabang And Not lInduk Then
                    ' jika pada saat diposting cabang entry sama dengan kode cabang yang aktif 
                    If cCabang = cKodeCabang Then
                        If cDK = "D" Then
                            ' Dr. Antar Bank Pasiva         nNominalTabungan
                            '     Cr. Rekening Kredit           nNominalTabungan
                            cDebet = cRekeningAntarKantor
                            cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                        Else
                            ' Dr. Rekening Debet            nNominalTabungan
                            '     Cr. Antar Bank Pasiva         nNominalTabungan
                            cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                            cKredit = cRekeningAntarKantor
                        End If
                        'Debet
                        UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cDebet, cKeterangan, nNominalTabungan, , , cNamaUser)
                        'Kredit
                        UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cKredit, cKeterangan, , nNominalTabungan, , cNamaUser)
                        If cKodeTransaksiTemp = aCfg(eCfg.msKodePenutupanTabungan).ToString Then
                            'Debet
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekeningAntarKantor, cKeterangan, nNominalTabungan, , , cNamaUser)
                            'Kredit
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, GetNull(.Item("RekeningKodeTransaksi").ToString).ToString, cKeterangan, , nNominalTabungan, , cNamaUser)
                        End If
                    Else
                        If cDK = "D" Then
                            ' Dr. Antar Kantor Aktiva       nNominalTabungan
                            '     Cr. Rekening Kredit           nNominalTabungan
                            ' Dr. Tabungan                  nNominalTabungan
                            '     Cr. Antar Kantor Aktiva       nNominalTabungan
                            cDebet = cRekeningAntarKantor
                            cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                            cDebet1 = cRekeningTabungan
                            cKredit1 = cRekeningAntarKantor
                        Else
                            ' Dr. Rekening Debet            nNominalTabungan
                            '     Cr. Antar Kantor Aktiva       nNominalTabungan
                            ' Dr. Antar Kantor Aktiva       nNominalTabungan
                            '     Cr. Tabungan                  nNominalTabungan
                            cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                            cKredit = cRekeningAntarKantor
                            cDebet1 = cRekeningAntarKantor
                            cKredit1 = cRekeningTabungan
                        End If
                        If Left(cFaktur, 2) = "DP" Then
                            cDebet = cKasTeller
                            cKredit = cRekeningTabungan

                            'Debet
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cDebet, cKeterangan, nNominalTabungan, , , cNamaUser)
                            'Kredit
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cKredit, cKeterangan, , nNominalTabungan, , cNamaUser)
                        Else
                            'Debet
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cDebet, cKeterangan, nNominalTabungan, , , cNamaUser)
                            'Kredit
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cKredit, cKeterangan, , nNominalTabungan, , cNamaUser)
                            'Debet
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cDebet1, cKeterangan, nNominalTabungan, , , cNamaUser)
                            'Kredit
                            UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cKredit1, cKeterangan, , nNominalTabungan, , cNamaUser)

                        End If
                    End If
                Else
                    If cDK = "D" Then
                        ' Dr. Tabungan     nNominalTabungan
                        '     Cr. Rekening Kredit    nJumlah
                        cDebet = cRekeningTabungan
                        cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                    Else
                        ' Dr. Rekening Debet     nJumlah
                        '     Cr. Tabungan          nNominalTabungan
                        cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                        cKredit = cRekeningTabungan
                    End If
                    'Debet
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cDebet, cKeterangan, nNominalTabungan, , , cNamaUser)
                    'Kredit
                    UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cKredit, cKeterangan, , nNominalTabungan, , cNamaUser)
                    If cKodeTransaksiTemp = aCfg(eCfg.msKodePenutupanTabungan).ToString Then
                        'Debet
                        UpdBukuBesar(nID, cCabang, cFaktur, dTgl, cRekeningKodeTransaksi, cKeterangan, nNominalTabungan, , , cNamaUser)
                        'Kredit
                        UpdBukuBesar(nID, cCabang, cFaktur, dTgl, GetNull(.Item("RekeningKodeTransaksi").ToString).ToString, cKeterangan, , nNominalTabungan, , cNamaUser)
                    End If
                End If
            End With
            n = n + 1
        Loop
    End Sub

    Sub UpdPembukaanTabungan(ByVal cRekening As String, ByVal dTgl As Date, ByVal cGolonganNasabah As String, _
                             ByVal cGolonganTabungan As String, ByVal cKode As String, ByVal cAO As String, _
                             ByVal cWilayah As String, ByVal cKeterkaitan As String, _
                             Optional ByVal cNamaUser As String = "", Optional ByVal cJenisNasabah As String = "N", _
                             Optional ByVal cKaryawan As String = "T")
        If cNamaUser = "" Then
            cNamaUser = cUserName
        End If
        Dim vaField() As Object = {"Rekening", "Tgl", "GolonganNasabah", _
                                   "GolonganTabungan", "Kode", "AO", _
                                   "Wilayah", "Keterkaitan", "CabangEntry", _
                                   "UserName", "JenisNasabah", "Karyawan"}
        Dim vaValue() As Object = {cRekening, dTgl, cGolonganNasabah, _
                                   cGolonganTabungan, cKode, cAO, _
                                   cWilayah, cKeterkaitan, aCfg(eCfg.msKodeCabang), _
                                   cNamaUser, cJenisNasabah, cKaryawan}
        Dim cWhere As String = String.Format("Rekening='{0}'", cRekening)
        objData.Update(GetDSN, "Tabungan", cWhere, vaField, vaValue)
    End Sub

    Function GetSaldoEfektif(ByVal cRekening As String, ByVal dTgl As Date, _
                             Optional ByVal lWithSaldoMinimum As Boolean = True) As Double
        Dim nBakiDebet As Double
        Const cWhere As String = "and CaraPerhitungan = '7'"
        Dim cDataTable As DataTable = objData.Browse(GetDSN, "Debitur", "Rekening", "RekeningTabungan", data.myOperator.Assign, cRekening, cWhere)
        If cDataTable.Rows.Count > 0 Then
            nBakiDebet = GetBakiDebet(cRekening, dTgl)
        Else
            nBakiDebet = 0
        End If
        Dim nSaldoMinimum As Double
        Dim vaJoin() As Object = {"left join golongantabungan g on t.golongantabungan = g.kode"}
        cDataTable = objData.Browse(GetDSN, "tabungan t", "g.saldominimum", "t.rekening", , cRekening, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            nSaldoMinimum = CDbl(cDataTable.Rows(0).Item("saldominimum").ToString)
        Else
            nSaldoMinimum = 0
        End If
        Dim nSaldoTabungan As Double = GetSaldoTabungan(cRekening, dTgl)
        Dim nTemp As Double
        If lWithSaldoMinimum Then
            nTemp = nSaldoMinimum
        Else
            nTemp = 0
        End If
        GetSaldoEfektif = nBakiDebet + nSaldoTabungan - nTemp
    End Function

    Function GetSaldoTabungan(ByVal cRekening As String, ByVal dTglAkhir As Date, Optional ByVal nID As Double = 999999999999.0#, Optional ByVal lTabungan As Boolean = True) As Double
        Dim nAkhir As Double
        Dim cDataTable As DataTable
        Dim cWhere As String = ""
        Dim cField As String = ""

        ' ambil saldo sampai kemarin
        cField = "sum(kredit-debet) as mutasi"
        cWhere = String.Format("rekening = '{0}' and tgl <='{1}'", cRekening, formatValue(dTglAkhir, formatType.yyyy_MM_dd))
        cDataTable = objData.Browse(GetDSN, "mutasitabungan", cField, , , , cWhere)
        If cDataTable.Rows.Count > 0 Then
            nAkhir = Val(GetNull(cDataTable.Rows(0).Item("mutasi").ToString))
        Else
            nAkhir = 0
        End If

        ' ambil saldo hari ini
        cWhere = String.Format("Rekening = '{0}' and Tgl = '{1}' and ID <{2}", cRekening, formatValue(dTglAkhir, formatType.yyyy_MM_dd), nID)
        cDataTable = objData.Browse(GetDSN, "mutasitabungan", cField, , , , cWhere)
        If cDataTable.Rows.Count > 0 Then
            nAkhir = nAkhir + Val(GetNull(cDataTable.Rows(0).Item("mutasi").ToString))
        End If

        If lTabungan And nAkhir < 0 Then
            nAkhir = 0
        End If
        GetSaldoTabungan = nAkhir
    End Function

    Sub DelMutasiTabungan(ByVal cFaktur As String, Optional ByVal cKodeTransaksi As String = "", Optional ByVal lDeleteBukuBesar As Boolean = False)
        Dim db As New DataTable
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cWhere As String = ""
        If Trim(cFaktur) <> "" Then
            ' 1. Buka Kembali Status Rekening
            ' 2. Hapus pada table Mutasi
            ' 3. Hapus pada Table Buku Besar

            ' Buka Status Rekening jika jenis transaksi adalah
            ' penutupan rekening
            If cKodeTransaksi = aCfg(eCfg.msKodeAdministrasi).ToString Then
                db = objData.Browse(GetDSN, "MutasiTabungan", "Rekening", "Faktur", , cFaktur)
                For n As Integer = 0 To db.Rows.Count - 1
                    vaField = {"TglPenutupan", "Close"}
                    vaValue = {"9999-12-31", "0"}
                    cWhere = String.Format("rekening = '{0}'", db.Rows(n).Item("rekening").ToString)
                    objData.Edit(GetDSN, "tabungan", cWhere, vaField, vaValue)
                Next
            End If
            ' Hapus Table Mutasi
            objData.Delete(GetDSN, "MutasiTabungan", "Faktur", , cFaktur)

            ' Hapus Table Buku Besar
            If lDeleteBukuBesar Then
                DelBukuBesar(cFaktur)
            End If
        End If
        db.Dispose()
    End Sub

    Function GetRptBungaHarian(ByVal cRekening As String, ByVal dAwal As Date,
                               ByVal dAkhir As Date, ByRef nTotalBunga As Double,
                               ByRef nTotalPajak As Double, Optional ByRef nSaldoAwal As Double = 0,
                               Optional ByRef nSaldoAkhir As Double = 0, Optional ByRef nMutasiDebet As Double = 0,
                               Optional ByRef nMutasiKredit As Double = 0, Optional lPajakOnly As Boolean = False,
                               Optional ByRef nTotalSaldoTabungan As Double = 0, Optional ByRef nTotalSaldoDeposito As Double = 0,
                               Optional ByRef nTotalSaldoTabunganDeposito As Double = 0, Optional ByVal cKode As String = "",
                               Optional ByVal cGolonganTabungan As String = "", Optional ByVal lProsesPostingBunga As Boolean = False) As DataTable
        Dim nSaldo As Double
        Dim n As Integer
        Dim vaArray As New DataTable
        Dim nAwal As Double
        Dim nHari As Integer
        Dim nSaldoPajak As Double = 0
        Dim nSaldoPajakTanpaGabungan As Double = 0
        Dim nSaldoKenaPajak As Double
        Dim cKodeSukuBunga As String
        Dim cWajibPajak As String = "Y"
        Dim nSaldoMinimum As Double
        Dim nPajakBunga As Single
        Dim nAdministrasiPasif As Double
        Dim db As New DataTable
        Dim nSaldoMinimumDapatBunga As Double
        Dim dbData As New DataTable

        db = objData.Browse(GetDSN, "golongantabungan", , "Kode", , cGolonganTabungan)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                nSaldoMinimumDapatBunga = CDbl(.Item("SaldoMinimumDapatBunga"))
                cKodeSukuBunga = .Item("SukuBunga").ToString
                nPajakBunga = CSng(.Item("PajakBunga"))
                cWajibPajak = IIf(CDbl(.Item("PajakBunga")) = 0, "T", "Y").ToString
                nSaldoMinimum = CDbl(GetNull(.Item("SaldoMinimum")))
                nAdministrasiPasif = CDbl(GetNull(.Item("AdminPasif"), 0))
                nSaldoKenaPajak = CDbl(GetNull(.Item("SaldoMinimumKenaPajak")))
            End With
        End If
        nMutasiDebet = 0
        nMutasiKredit = 0
        nTotalBunga = 0
        nTotalPajak = 0
        nSaldoAwal = 0
        nSaldoAkhir = 0
        nTotalSaldoTabungan = 0
        nTotalSaldoDeposito = 0
        nTotalSaldoTabunganDeposito = 0

        ' Ambil Saldo Awal
        Const cField As String = "sum(Kredit-Debet) as Awal"
        Dim cWhere As String = String.Format("and Tgl < '{0}'", formatValue(dAwal, formatType.yyyy_MM_dd))
        dbData = objData.Browse(GetDSN, "MutasiTabungan", cField, "Rekening", , cRekening, cWhere)
        If dbData.Rows.Count > 0 Then
            nAwal = CDbl(GetNull(dbData.Rows(0).Item("Awal")))
        End If
        nSaldoAwal = nAwal
        nHari = CInt(DateDiff("d", dAwal, dAkhir))
        vaArray.Reset()
        AddColumn(vaArray, "tgl", System.Type.GetType("system.DateTime"))
        AddColumn(vaArray, "saldo", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "sukubunga", System.Type.GetType("system.Single"))
        AddColumn(vaArray, "rumus", System.Type.GetType("system.String"))
        AddColumn(vaArray, "bunga", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "pajak", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "bungabersih", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "saldogabungantabungan", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "saldogabungandeposito", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "jumlahsaldogabungan", System.Type.GetType("system.Double"))
        Dim row As DataRow
        For n = 0 To nHari
            row = vaArray.NewRow()
            row("tgl") = CDate("1900-01-01")
            row("saldo") = 0
            row("sukubunga") = 0
            row("rumus") = ""
            row("bunga") = 0
            row("pajak") = 0
            row("bungabersih") = 0
            row("saldogabungantabungan") = 0
            row("saldogabungandeposito") = 0
            row("jumlahsaldogabungan") = 0
            vaArray.Rows.Add(row)
        Next

        ' Ambil Mutasi Tabungan
        n = 0
        cWhere = ""
        cWhere = String.Format(" and Tgl >= '{0}' ", formatValue(dAwal, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and Tgl <= '{1}' ", cWhere, formatValue(dAkhir, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodeBunga))
        '  cWhere = cWhere & " and kodetransaksi <> '" & aCfg(msKodeAdmPemeliharaan) & "' "
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodePajakBunga))
        dbData = objData.Browse(GetDSN, "MutasiTabungan", , "Rekening", , cRekening, cWhere, "Rekening,Tgl,ID")
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    If CDate(.Item("Tgl")) < dAwal Then
                        n = 0
                    Else
                        n = CInt(DateDiff("d", dAwal, CDate(.Item("Tgl"))))
                    End If
                    vaArray.Rows(n).Item("saldo") = CDbl(GetNull(vaArray.Rows(n).Item("saldo"))) + CDbl(.Item("Kredit")) - CDbl(.Item("debet"))
                    nMutasiDebet = nMutasiDebet + CDbl(.Item("debet"))
                    nMutasiKredit = nMutasiKredit + CDbl(.Item("Kredit"))
                End With
            Next
        End If

        nSaldo = nSaldoAwal
        'Tanggal,saldo,rate,rumus,bunga,pajak,bungabersih
        For n = 0 To vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                nSaldo = nSaldo + CDbl(GetNull(.Item("saldo")))
                .Item(0) = DateAdd("d", n, dAwal)
                If Not lProsesPostingBunga Then
                    .Item("saldogabungantabungan") = GetSaldoNasabah(CDate(.Item("tgl")), cKode, , , True, True)
                    nTotalSaldoTabungan = CDbl(.Item("saldogabungantabungan"))
                    .Item("saldogabungandeposito") = GetSaldoNasabah(CDate(.Item("tgl")), cKode, , , True, False)
                    nTotalSaldoDeposito = CDbl(.Item("saldogabungandeposito"))
                    .Item("jumlahsaldotabungan") = CDbl(.Item("saldogabungantabungan")) + CDbl(.Item("saldogabungandeposito"))
                Else
                    .Item("saldogabungantabungan") = 0
                    .Item("saldogabungandeposito") = 0
                    ' saldo gabungan yang dihitung sementara ini adalah hanya modul tabungan saja, bukan tabungan dan deposito
                    .Item("jumlahsaldogabungan") = GetSaldoSimpananRegister(CDate(.Item("tgl")), cKode)
                End If
                nSaldoPajak = CDbl(.Item("jumlahsaldogabungan"))
                nSaldoPajakTanpaGabungan = nSaldo
                nTotalSaldoTabunganDeposito = CDbl(.Item("jumlahsaldogabungan"))
                .Item("saldo") = nSaldo
                .Item("sukubunga") = GetSukuBunga(cRekening, CDate(.Item("tgl")), nSaldo, cGolonganTabungan)
                .Item("rumus") = String.Format("{0} * {1} % / 365", Format(CDbl(.Item("saldo")), "###,###,###,###,##0.00"), Format(CDbl(.Item("sukubunga")), "##0.00"))
                .Item("bunga") = Math.Round(CDbl(.Item("saldo")) * CDbl(.Item("sukubunga")) / 36500, 2)
                ' perhitungan pajak dengan menggunakan saldo gabungan sementara ini tidak digunakan sampai ada kepastian dari pihak bank jombang
                .Item("pajak") = Math.Round(CDbl(IIf(nSaldoPajak >= nSaldoKenaPajak, CDbl(.Item("bunga")) * 0.2, 0)))
                .Item("bungabersih") = CDbl(.Item("bunga")) - CDbl(.Item("pajak"))
                If lPajakOnly = True Then
                    nTotalBunga = nTotalBunga + CDbl(IIf(nSaldo >= nSaldoMinimumDapatBunga, CDbl(.Item("bunga")), 0))
                    ' perhitungan pajak dengan menggunakan saldo gabungan sementara ini tidak digunakan sampai ada kepastian dari pihak bank jombang
                    nTotalPajak = nTotalPajak + CDbl(IIf(nSaldoPajak >= nSaldoKenaPajak, CDbl(.Item("pajak")), 0))
                Else
                    nTotalBunga = nTotalBunga + CDbl(.Item("bunga"))
                    nTotalPajak = nTotalPajak + CDbl(.Item("pajak"))
                End If
            End With
        Next
        nTotalBunga = Math.Round(nTotalBunga)
        If cWajibPajak <> "Y" Then
            nTotalPajak = 0
        Else
            nTotalPajak = Math.Round(nTotalPajak)
        End If
        nSaldoAkhir = nSaldo
        GetRptBungaHarian = vaArray
        db.Dispose()
    End Function

    Function GetSukuBunga(ByVal cRekening As String, ByVal dTgl As Date, Optional ByVal nSaldoTabungan As Double = 0,
                          Optional ByVal cGolonganTabungan As String = "") As Double
        Dim db As New DataTable
        Dim nSaldo As Double
        Const cField As String = "SukuBunga"
        Dim cWhere As String

        If nSaldoTabungan = 0 Then
            nSaldo = GetSaldoTabungan(cRekening, dTgl)
        Else
            nSaldo = nSaldoTabungan
        End If
        If cGolonganTabungan = "" Then
            cGolonganTabungan = Mid(cRekening, 4, 2)
        End If
        cWhere = String.Format(" and Tgl <= '{0}' and Minimum <= {1} and Maximum >= {1}", formatValue(dTgl, formatType.yyyy_MM_dd), nSaldo)
        db = objData.Browse(GetDSN, "detailsukubunga", cField, "Kode", , cGolonganTabungan, cWhere, "Kode,Tgl Desc,Maximum limit 1")
        If db.Rows.Count > 0 Then
            GetSukuBunga = CDbl(GetNull(db.Rows(0).Item("SukuBunga")))
        Else
            GetSukuBunga = 0
        End If
        db.Dispose()
    End Function

    Function GetRekeningPassif(ByVal cRekening As String, ByVal dTanggal As Date) As Boolean
        Dim dOldTgl As Date
        Dim dbPasif As New DataTable
        Dim cWhere As String
        Dim nSaldoMinimumDapatBunga As Double = 0
        Dim nSaldo As Double
        Dim dTanggalTemp As Date
        Dim dbData As New DataTable

        GetRekeningPassif = True
        Dim cSQL As String = "select g.lamapasif,g.saldominimumdapatbunga from tabungan t "
        cSQL = cSQL & "left join golongantabungan g on g.kode = t.golongantabungan "
        cSQL = String.Format("{0}where t.rekening = '{1}'", cSQL, cRekening)
        dbPasif = objData.SQL(GetDSN, cSQL)
        If dbPasif.Rows.Count > 0 Then
            dOldTgl = DateAdd("m", -CDbl(GetNull(dbPasif.Rows(0).Item("LamaPasif"))), dTanggal)
            nSaldoMinimumDapatBunga = CDbl(GetNull(dbPasif.Rows(0).Item("SaldoMinimumDapatBunga")))
        End If

        dTanggalTemp = dTanggal
        cWhere = String.Format(" and tgl <= '{0}' ", formatValue(dTanggalTemp, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and rekening = '{1}' ", cWhere, cRekening)
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodeAdmPemeliharaan))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodeBunga))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}'", cWhere, aCfg(eCfg.msKodePajakBunga))
        dbPasif = objData.Browse(GetDSN, "MutasiTabungan", "kodetransaksi,tgl,jumlah",
                                 "tgl", data.myOperator.GreaterThanEqual, formatValue(dOldTgl, formatType.yyyy_MM_dd),
                                 cWhere, "Rekening", , "Tgl, kodetransaksi")
        If dbPasif.Rows.Count > 0 Then
            nSaldo = GetSaldoTabungan(cRekening, dTanggal)
            cWhere = ""
            cWhere = String.Format("and tgl='{0}' ", formatValue(dTanggal, formatType.yyyy_MM_dd))
            cWhere = String.Format("{0}and (kodetransaksi ='{1}' ", cWhere, aCfg(eCfg.msKodeAdmPemeliharaan))
            cWhere = String.Format("{0}or kodetransaksi = '{1}' ", cWhere, aCfg(eCfg.msKodeBunga))
            cWhere = String.Format("{0}or kodetransaksi = '{1}')", cWhere, aCfg(eCfg.msKodePajakBunga))
            dbData = objData.Browse(GetDSN, "mutasitabungan", "kodetransaksi,jumlah", "Rekening", , cRekening, cWhere)
            For n = 0 To dbData.Rows.Count - 1
                If dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodeAdmPemeliharaan).ToString Then
                    nSaldo = nSaldo + CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
                ElseIf dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodeBunga).ToString Then
                    nSaldo = nSaldo - CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
                ElseIf dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodePajakBunga).ToString Then
                    nSaldo = nSaldo + CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
                End If
            Next
            ' jika saldo dibawah saldo minimum dapat bunga maka statusnya langsung pasif
            If nSaldo <= nSaldoMinimumDapatBunga Then
                GetRekeningPassif = True
            Else
                GetRekeningPassif = False
            End If
        End If
        ' jika ada transaksi di bulan tanggal awal parameter, maka masih dianggap aktif
        cWhere = ""
        cWhere = String.Format("and tgl>='{0}' ", formatValue(BOM(dOldTgl), formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and tgl <= '{1}' ", cWhere, formatValue(EOM(dOldTgl), formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodeAdmPemeliharaan))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}' ", cWhere, aCfg(eCfg.msKodeBunga))
        cWhere = String.Format("{0} and kodetransaksi <> '{1}'", cWhere, aCfg(eCfg.msKodePajakBunga))
        dbPasif = objData.Browse(GetDSN, "mutasitabungan", "faktur,debet,kredit", "rekening", , cRekening, cWhere)
        If dbPasif.Rows.Count > 0 And GetRekeningPassif Then
            If nSaldo > nSaldoMinimumDapatBunga Then
                GetRekeningPassif = False
            End If
        End If
        dbData.Dispose()
        dbPasif.Dispose()
    End Function
End Module
