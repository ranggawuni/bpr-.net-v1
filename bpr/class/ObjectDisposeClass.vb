Namespace MySQL_Data_Library
    Public Class ObjectDisposeClass
        Implements IDisposable
        Private disposedValue As Boolean
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If
                ' TODO: free shared unmanaged resources
            End If
            disposedValue = True
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
    End Class

End Namespace
