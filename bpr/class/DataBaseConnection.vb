﻿
Namespace MySQL_Data_Library

    Public Class DataBaseConnection
        Dim cConectionString As String = "Database=bpr_private;Data Source=localhost;User Id=Heasoft;Password=HeaI"
        'Private conn As New MySqlConnection(My.Settings.MySQLConnectionString)
        'ReadOnly conn As New MySqlConnection("Database=bpr_private;Data Source=localhost;User Id=Heasoft;Password=HeaI")
        ReadOnly conn As New MySqlConnection(cConectionString)

        Public Sub InitConnection(ByVal cConnection As String)
            cConectionString = cConnection
        End Sub

        Public Function OpenConnection() As MySqlConnection
            Try
                If conn.State <> ConnectionState.Open Then
                    conn.Open()
                End If
            Catch ex As MySqlException
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            Return conn
        End Function

        Public Function CloseConnection() As MySqlConnection
            conn.Close()
            Return conn
        End Function

    End Class
End Namespace
