﻿Namespace MySQL_Data_Library

    Public Class data
        Inherits ObjectDisposeClass
        Private ReadOnly myConnection As New MySQL_Data_Library.DataBaseConnection()

        Enum myOperator
            Assign = 0
            Prefix = 1
            Sufix = 2
            Content = 3
            Difference = 4
            GreaterThanEqual = 5
            LowerThanEqual = 6
            GreaterThan = 7
            LowerThan = 8
        End Enum

        Private Shared Function GetJoin(ByVal vaLeftJoin() As Object) As String
            Try
                GetJoin = String.Join(" ", vaLeftJoin)
                Return CheckSQL(GetJoin)
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Private Shared Function CheckSQL(ByVal cSQL As String) As String
            Dim n As Integer
            Dim va() As String = Split(cSQL, " ")
            Dim lTable As Boolean = False
            Dim cKey As String
            For n = 0 To UBound(va)
                If lTable And Trim(va(n)) <> "" Then
                    va(n) = LCase(va(n))
                    lTable = False
                End If
                cKey = LCase(va(n))
                If cKey = "into" Or cKey = "update" Or cKey = "from" Or cKey = "join" Or cKey = "table" Then
                    lTable = True
                End If
            Next
            Return Join(va, " ")
        End Function

        Private Shared Function GetWhere(Optional ByVal cField As String = "", Optional ByVal cValue As String = "", _
                  Optional ByVal nOperator As myOperator = 0, Optional ByVal cWhere As String = "") As String
            Dim cResult As String = ""
            If cField <> "" Or cWhere <> "" Then
                cResult = " where "
                If cField <> "" Then
                    cResult = cResult & cField
                    If nOperator = myOperator.Assign Then
                        cResult = String.Format("{0} = '{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.Prefix Then
                        cResult = String.Format("{0} like '{1}%' ", cResult, cValue)
                    ElseIf nOperator = myOperator.Sufix Then
                        cResult = String.Format("{0} like '%{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.Content Then
                        cResult = String.Format("{0} like '%{1}%' ", cResult, cValue)
                    ElseIf nOperator = myOperator.Difference Then
                        cResult = String.Format("{0} <> '{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.GreaterThan Then
                        cResult = String.Format("{0} > '{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.GreaterThanEqual Then
                        cResult = String.Format("{0} >= '{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.LowerThan Then
                        cResult = String.Format("{0} < '{1}' ", cResult, cValue)
                    ElseIf nOperator = myOperator.LowerThanEqual Then
                        cResult = String.Format("{0} <= '{1}' ", cResult, cValue)
                    End If
                End If
                cResult = cResult & cWhere
            End If
            Return cResult
        End Function

        Private Sub Query(ByVal cConnectionString As String, ByVal parSQL As String, Optional ByRef cDataTable As DataTable = Nothing, Optional ByVal lUpdateHistory As Boolean = True)
            Try
                myConnection.InitConnection(cConnectionString)
                parSQL = CheckSQL(parSQL)
                If lUpdateHistory Then
                    'UpdateHistory cConnectionString,parsql
                End If

                Dim cQueryType As String = Left(LTrim(parSQL), 6)
                If UCase(cQueryType) <> "SELECT" Then
                    Using myCommand As New MySqlCommand(parSQL, myConnection.OpenConnection)
                        myCommand.ExecuteNonQuery()
                    End Using
                Else
                    Using myCommand As New MySqlCommand(parSQL, myConnection.OpenConnection)
                        Dim myDataAdapter As New MySqlDataAdapter(myCommand)
                        myDataAdapter.Fill(cDataTable)
                    End Using
                End If
                myConnection.CloseConnection()
            Catch ex As MySqlException
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Private Sub QueryDataSet(ByVal cConnectionString As String, ByVal parSQL As String, _
                                 Optional ByRef cDataSet As DataSet = Nothing, Optional ByVal cTable As String = "", _
                                 Optional ByVal lUpdateHistory As Boolean = True)
            Try
                parSQL = CheckSQL(parSQL)
                If lUpdateHistory Then
                    'UpdateHistory cConnectionString,parsql
                End If

                Using myCommand As New MySqlCommand(parSQL, myConnection.OpenConnection)
                    Dim myDataAdapter As New MySqlDataAdapter(myCommand)
                    myDataAdapter.Fill(cDataSet, cTable)
                End Using
                Dim cDataTable As DataTable = cDataSet.Tables(cTable)
                If cDataTable.Rows.Count <= 0 Then
                    'Throw New Exception("Data tidak ada")
                    'MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
                myConnection.CloseConnection()
            Catch ex As MySqlException
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Function Add(ByVal cConnectionString As String, ByVal cTableName As String, _
                            ByVal vaField() As Object, ByVal vaValue() As Object, _
                            Optional ByVal lDisplayError As Boolean = True, _
                            Optional ByVal lUpdateHistory As Boolean = True) As Boolean
            Try
                cTableName = LCase(cTableName)
                Dim cSQL As String = AppendRecord(cTableName, vaField, vaValue)
                Query(cConnectionString, cSQL, , lUpdateHistory)
                Return True
            Catch ex As Exception
                If lDisplayError Then
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                Return False
            End Try
        End Function

        Public Function Edit(ByVal cConnectionString As String, ByVal cTableName As String, ByVal cWhere As String, _
                             ByVal vaField() As Object, ByVal vaValue() As Object, _
                             Optional ByVal lDisplayError As Boolean = True, _
                             Optional ByVal lUpdateHistory As Boolean = True) As Boolean
            Try
                cTableName = LCase(cTableName)
                Dim cSQL As String = EditRecord(cTableName, cWhere, vaField, vaValue)
                Query(cConnectionString, cSQL)
                Return True
            Catch ex As Exception
                Return False
                If lDisplayError Then
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End Try
        End Function

        Public Function Update(ByVal cConnectionString As String, ByVal cTableName As String, _
                               ByVal cWhere As String, ByVal vaField() As Object, _
                               ByVal vaValue() As Object, Optional ByVal lDisplayError As Boolean = True, _
                               Optional ByVal nLower As Double = 0, Optional ByVal nUpper As Double = 0, _
                               Optional ByVal lUpdateHistory As Boolean = True) As Boolean
            Try
                cTableName = LCase(cTableName)
                Dim cSQL As String = UpdateRecord(cConnectionString, cTableName, cWhere, vaField, vaValue) & GetLimit(nLower, nUpper)
                Query(cConnectionString, cSQL)
                Return True
            Catch ex As Exception
                If lDisplayError Then
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                Return False
            End Try
        End Function

        Public Function Delete(ByVal cConnectionString As String, ByVal cTableName As String, _
                               Optional ByVal cField As String = "", _
                               Optional ByVal nOperator As myOperator = myOperator.Assign, _
                               Optional ByVal cValue As String = "", Optional ByVal cWhere As String = "", _
                               Optional ByVal lDisplayError As Boolean = True, Optional ByVal nLower As Double = 0, _
                               Optional ByVal nUpper As Double = 0, Optional ByVal lUpdateHistory As Boolean = True) As Boolean
            Try
                cTableName = LCase(cTableName)
                Dim cSQL As String = String.Format("Delete from {0}{1}{2}", cTableName, GetWhere(cField, cValue, nOperator, cWhere), GetLimit(nLower, nUpper))
                Query(cConnectionString, cSQL, , lUpdateHistory)
                Return True
            Catch ex As Exception
                Return False
                If lDisplayError Then
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End Try
        End Function

        Public Function BrowseDataSet(ByVal cConnectionString As String, ByVal cTableName As String, _
                                      Optional ByVal FieldList As String = "*", _
                                      Optional ByVal FieldCriteria As String = "", _
                                      Optional ByVal nOperator As myOperator = 0, _
                                      Optional ByVal cValue As String = "", _
                                      Optional ByVal cWhere As String = "", _
                                      Optional ByVal cOrder As String = "", _
                                      Optional ByVal vaJoin() As Object = Nothing, _
                                      Optional ByVal cGroup As String = "") As DataSet

            Dim cDataSet As New DataSet
            BrowseDataSet = Nothing
            Try
                cTableName = LCase(cTableName)
                Dim cResult As String = String.Format("select {0} from {1}{2} {3}{4}{5}", FieldList, cTableName, GetJoin(vaJoin), GetWhere(FieldCriteria, cValue, nOperator, cWhere), IIf(Trim(cGroup) <> "", " Group By " & cGroup, ""), IIf(Trim(cOrder) <> "", " Order By " & cOrder, ""))

                QueryDataSet(cConnectionString, cResult, cDataSet, cTableName)
                If IsNothing(cDataSet) Then
                    MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Else
                    Return cDataSet
                End If
            Catch ex As Exception
                MessageBox.Show(String.Format("table : {0} - {1}", cTableName, ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End Function

        Public Function Browse(ByVal cConnectionString As String, ByVal cTableName As String, _
                               Optional ByVal FieldList As String = "*", _
                               Optional ByVal FieldCriteria As String = "", _
                               Optional ByVal nOperator As myOperator = 0, _
                               Optional ByVal cValue As String = "", _
                               Optional ByVal cWhere As String = "", _
                               Optional ByVal cOrder As String = "", _
                               Optional ByVal vaJoin() As Object = Nothing, _
                               Optional ByVal cGroup As String = "") As DataTable

            Dim cDataTable As New DataTable
            Browse = Nothing
            Try
                cTableName = LCase(cTableName)
                Dim cResult As String = String.Format("select {0} from {1} {2} {3} {4} {5}", FieldList, cTableName, GetJoin(vaJoin), GetWhere(FieldCriteria, cValue, nOperator, cWhere), IIf(Trim(cGroup) <> "", " Group By " & cGroup, ""), IIf(Trim(cOrder) <> "", " Order By " & cOrder, ""))

                Query(cConnectionString, cResult, cDataTable)
                If IsNothing(cDataTable) Then
                    MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Else
                    Return cDataTable
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End Function

        Public Function SQL(ByVal cConnectionString As String, ByVal parSQL As String, _
                            Optional ByVal lUpdateHistory As Boolean = True) As DataTable
            Dim cDataTable As New DataTable
            SQL = Nothing
            Try
                parSQL = CheckSQL(parSQL)
                Query(cConnectionString, parSQL, cDataTable, lUpdateHistory)
                Return cDataTable
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End Function

        Public Function SQLDataSet(ByVal cConnectionString As String, ByVal parSQL As String, _
                                   Optional ByVal cTable As String = "Temp", _
                                   Optional ByVal lUpdateHistory As Boolean = True) As DataSet
            Dim cDataSet As New DataSet
            SQLDataSet = Nothing
            Try
                parSQL = CheckSQL(parSQL)
                QueryDataSet(cConnectionString, parSQL, cDataSet, cTable, lUpdateHistory)
                Return cDataSet
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End Function

        Private Shared Function GetLimit(ByVal nStart As Double, ByVal nEnd As Double) As String
            If nStart >= 0 And nEnd > 0 And nEnd > nStart Then
                GetLimit = String.Format(" Limit {0},{1}", nStart, nEnd)
            Else
                GetLimit = ""
            End If
        End Function

        Private Shared Function CheckValues(ByRef chkvalue As Object) As String
            CheckValues = ""
            If VarType(chkvalue) = VariantType.Byte Or VarType(chkvalue) = VariantType.Currency _
                Or VarType(chkvalue) = VariantType.Decimal Or VarType(chkvalue) = VariantType.Double _
                Or VarType(chkvalue) = VariantType.Integer Or VarType(chkvalue) = VariantType.Long Then
                CheckValues = Trim(CStr(chkvalue))
            ElseIf VarType(chkvalue) = VariantType.Date Then
                CheckValues = Format(chkvalue, "yyyy-MM-dd")
                CheckValues = String.Format("'{0}'", CheckValues)
            ElseIf VarType(chkvalue) = VariantType.String Then
                chkvalue = Replace(CStr(chkvalue), Chr(0), "\0")
                chkvalue = Replace(CStr(chkvalue), "\", "\\")
                chkvalue = Replace(CStr(chkvalue), "'", "\'")
                chkvalue = Replace(CStr(chkvalue), Chr(34), "\" & Chr(34))
                chkvalue = RTrim(CStr(chkvalue))
                If Left(CStr(chkvalue), 1) = "&" Then
                    CheckValues = Mid(CStr(chkvalue), 2)
                Else
                    CheckValues = String.Format("'{0}'", chkvalue)
                End If
            ElseIf VarType(chkvalue) = VariantType.Null Then
                CheckValues = "''"
            End If
        End Function

        Private Shared Function AppendRecord(ByVal cTable As String, ByVal vaField() As Object, ByVal vaValues() As Object) As String
            Dim cField As String = String.Format(" ({0}) ", String.Join(",", vaField))
            Dim n As Integer = 0
            For n = 0 To vaValues.GetUpperBound(0)
                vaValues(n) = CheckValues(vaValues(n))
            Next
            Dim cValue As String = String.Format(" Values ({0}) ", String.Join(",", vaValues))
            Dim cRetval As String = String.Format("INSERT INTO {0}{1}{2}", cTable, cField, cValue)
            Return cRetval
        End Function

        Private Shared Function EditRecord(ByVal cTable As String, ByVal cWhere As String, _
                                           ByVal vaField() As Object, ByVal vaValues() As Object) As String
            Dim cRetval As String = String.Format("Update {0} Set ", cTable)
            Dim n As Integer
            Dim cTemp As String
            For n = 0 To UBound(vaField)
                If n < UBound(vaField) Then
                    cRetval = String.Format("{0}{1} = {2}", cRetval, vaField(n), CheckValues(vaValues(n))) & ","
                Else
                    cRetval = String.Format("{0}{1} = {2}", cRetval, vaField(n), CheckValues(vaValues(n)))
                End If
            Next
            If Trim(cWhere) <> "" Then
                cTemp = "Where " & cWhere
            Else
                cTemp = ""
            End If
            cRetval = cRetval & cTemp
            Return cRetval
        End Function

        Private Function UpdateRecord(ByVal cConnectionString As String, ByRef cTable As String, ByRef cWhere As String, ByRef vaField() As Object, _
                                      ByRef vaValues() As Object) As String
            Using cDataTable As New DataTable()
                Dim cSQL As String = String.Format("select * from {0} where {1}", cTable, cWhere)
                Query(cConnectionString, cSQL, cDataTable)
                If cDataTable.Rows.Count > 0 Then
                    UpdateRecord = EditRecord(cTable, cWhere, vaField, vaValues)
                Else
                    UpdateRecord = AppendRecord(cTable, vaField, vaValues)
                End If
            End Using
        End Function

    End Class
End Namespace