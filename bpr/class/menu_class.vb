﻿
Public Class menu_class
    Private ReadOnly ObjData As New bpr.MySQL_Data_Library.data

#Region "Ambil Data Menu di Database"
    Public Sub GetMenu(ByVal cKode As String, ByVal nLevel As Single)
        Dim cKey As String
        Dim nMaxMenu As Integer
        Dim cSQL As String = String.Format("select count(kode) as MaxMenu from Menu where kode='{0}'", cKode)
        cSQL = String.Format("{0} and MenuLevel='{1}'", cSQL, nLevel)
        Dim dsMenu As New DataSet
        dsMenu = ObjData.SQLDataSet(cSQL, "DataMenu")
        Dim dtMenu As DataTable = dsMenu.Tables("DataMenu")
        With dtMenu
            If .Rows.Count > 0 AndAlso .Rows(0).Item("MaxMenu").ToString <> "" Then
                nMaxMenu = CInt(CLng(.Rows(0).Item("MaxMenu").ToString))
                RunGaugeMyDLL(0, nMaxMenu)
                cKey = "F"
                cSQL = ""
                cSQL = String.Format("select urut,status from menu where kode = '{0}'", cKode)
                cSQL = String.Format("{0} and MenuLevel = '{1}' and FileName = '{2}' ", cSQL, nLevel, cKey)
                cSQL = cSQL & "order by urut"
                Dim dsUser As New DataSet
                dsUser = ObjData.SQLDataSet(cSQL, "DataUser")
                Dim dtUser As New DataTable
                dtUser = dsUser.Tables("DataUser")
                Using dvUser As New DataView() With {.Table = dtUser}
                End Using
                If dtUser.DefaultView.Count > 0 Then
                    PopulateMenuItem(dtUser.DefaultView, cKey)
                Else
                    MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                dsUser.Dispose()
                dtUser.Dispose()
            End If
        End With
        dsMenu.Dispose()
    End Sub

    Private Shared Sub PopulateMenuItem(ByVal dvTemp As DataView, ByVal cKey As String)
        Dim IEnum As IEnumerator = aMainMenu.MenuStrip.Items.GetEnumerator
        While IEnum.MoveNext
            Dim aToolStripMenuItem As ToolStripMenuItem = DirectCast(IEnum.Current, ToolStripMenuItem)
            Dim cMenuName As String = cKey & aToolStripMenuItem.Tag.ToString
            dvTemp.RowFilter = String.Format("FileName='{0}'", cMenuName)
            If dvTemp.Count > 0 Then
                aToolStripMenuItem.Visible = True
            Else
                aToolStripMenuItem.Visible = False
            End If
            AddRecursiveItem(dvTemp, aToolStripMenuItem, cKey)
        End While
    End Sub

    Private Shared Sub AddRecursiveItem(ByVal dvView As DataView, ByVal ToolStripMenuItemRef As ToolStripMenuItem, ByVal cKey As String)
        For i As Integer = 0 To ToolStripMenuItemRef.DropDownItems.Count - 1
            If Not (TypeOf ToolStripMenuItemRef.DropDownItems(i) Is ToolStripMenuItem) Then Continue For
            Dim cMenuName As String = cKey & ToolStripMenuItemRef.DropDownItems(i).Tag.ToString
            dvView.RowFilter = String.Format("FileName='{0}'", cMenuName)
            If dvView.Count > 0 Then
                ToolStripMenuItemRef.DropDownItems(i).Visible = True
            Else
                ToolStripMenuItemRef.DropDownItems(i).Visible = False
            End If
            AddRecursiveItem(dvView, CType(ToolStripMenuItemRef.DropDownItems(i), ToolStripMenuItem), cKey)
        Next
    End Sub
#End Region
End Class
