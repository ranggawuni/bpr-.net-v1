﻿Imports System
Imports System.IO

Public Class IniTool
    Private mIniFileName As String
    Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Object, ByVal lpString As String, ByVal lpFileName As String)
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Object, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

    Public Property FileName As String
        Get
            Return mIniFileName
        End Get
        Set(ByVal INIFileName As String)
            If Dir(INIFileName, FileAttribute.Normal) = "" Then
                Try
                    Using sr As StreamReader = New StreamReader(INIFileName)
                        Dim line As String
                        Dim LineTemp As String
                        Do
                            line = sr.ReadLine
                            LineTemp = sr.ReadLine
                            Console.WriteLine(line)
                        Loop Until line Is Nothing
                        INIFileName = LineTemp
                        sr.Close()
                    End Using
                Catch ex As Exception
                    Console.WriteLine("The file could not be read:")
                    Console.WriteLine(ex.Message)
                End Try
            End If
            mIniFileName = INIFileName
        End Set
    End Property


    Public Function GetValue(ByVal Section As String, ByVal Key As String, Optional ByVal DefaultValue As String = "") As String
        Try
            Dim retval As New String(CChar("0"), 255)
            Dim X As Long = GetPrivateProfileString(Section, Key, DefaultValue, retval, Len(retval), mIniFileName)
            GetValue = Trim(Left(retval, CInt(X)))
        Catch ex As Exception
            GetValue = DefaultValue
        End Try
    End Function

    Public Function WriteValue(ByVal Section As String, ByVal Key As String, ByVal Value As String) As Boolean
        On Error GoTo xERR
        Dim X As Long = WritePrivateProfileString(Section, Key, Value, mIniFileName)
        If X <> 0 Then WriteValue = True
        Exit Function
xERR:
    End Function

    Public Function GetAllSections() As Collection
        Dim Value As String
        Dim s() As String, i As Integer
        Dim retval As New String(CChar("0"), 255)
        Dim X As Long = GetPrivateProfileString(vbNullString, "", "", retval, Len(retval), mIniFileName)
        Value = Trim(Left(retval, CInt(X)))
        s = Split(Value, Chr(0))
        GetAllSections = New Collection
        With GetAllSections
            For i = LBound(s) To UBound(s)
                If s(i) <> "" Then .Add(s(i))
            Next
        End With
    End Function

    Public Function GetAllKeys(ByVal Section As String) As Collection
        Dim Value As String, X As Long
        Dim s() As String, i As Integer
        Dim retval As New String(CChar("0"), 255)
        X = GetPrivateProfileString(Section, vbNullString, "", retval, Len(retval), mIniFileName)
        Value = Trim(Left(retval, CInt(X)))
        s = Split(Value, Chr(0))
        GetAllKeys = New Collection
        With GetAllKeys
            For i = LBound(s) To UBound(s)
                If s(i) <> "" Then .Add(s(i))
            Next
        End With
    End Function

    Public Function DeleteSection(ByVal Section As String) As Boolean
        On Error GoTo xERR
        Dim X As Long = WritePrivateProfileString(Section, vbNullString, "", mIniFileName)
        If X <> 0 Then DeleteSection = True
        Exit Function
xERR:
    End Function

    Public Function DeleteKey(ByVal Section As String, ByVal Key As String) As Boolean
        On Error GoTo xERR
        Dim X As Long = WritePrivateProfileString(Section, Key, vbNullString, mIniFileName)
        If X <> 0 Then DeleteKey = True
        Exit Function
xERR:
    End Function


End Class
