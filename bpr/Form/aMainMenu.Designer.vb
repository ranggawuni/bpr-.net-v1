﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class aMainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                    objData = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(aMainMenu))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCabang = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.AkuntansiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RekeningToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AntarBankAktivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AntarBankPasivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganAktivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MataUangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.AgamaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PekerjaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InstansiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DaerahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WilayahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.TabunganToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganNasabahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.PerubahanSukuBungaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.KodeTransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KonfigurasiTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganDeposanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.SifatKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganDebiturToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SektorEkonomiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GolonganPenjaminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.SetupJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SuratPerjanjianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.AccountOfficerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BendaharaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.AdministrasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProvisiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.LogOffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegisterNasabahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.TabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabunganBaruToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MutasiTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BlokirRekeningToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.HapusMutasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TutupRekeningToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatalTutupRekeningToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CetakSlipValidasiBukuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValidasiMutasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HeaderBukuTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakBukuTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenyetoranDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.MutasiDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerubahanSukuBungaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerpanjanganDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BlokirDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.HapusMutasiDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembatalanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.CetakSlipKartuBilyetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BilyetDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TandaTerimaBungaDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuktiPencairanDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValidasiDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.KoreksiDataDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KoreksiTanggalPerpanjanganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KreditToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SimulasiKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.PengajuanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RealisasiKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengikatanJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengambilanJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.RegisterJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegisterJaminanBrankasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakSuratPerjanijanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakIdentitasJaminanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PencairanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenarikanPlafondKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripSeparator()
        Me.AngsuranKreditInstansiTitipanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AngsuranKreditInstansiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AngsuranKreditKasirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PostingBungaRKToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PindahRekeningKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutoDebetAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsuransiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BumiPuteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JamkridaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripSeparator()
        Me.PenolakanPengajuanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusAngsuranKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusAngsuranKreditInstansiTitipanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusAngsuranKreditInstansiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusMutasiAsuransiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatalPencairanKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.KoreksiDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KoreksiDataKreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KoreksiStatusPengajuanKreditStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateJadwalKreditStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakSlipKartuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakHeaderKartuAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakSlipPenerimaanPinjamanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakSlipAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakValidasiAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakSlipAngsuranInstansiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakTandaTerimaPengambilanJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripSeparator()
        Me.PenerimaanKasBesarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengeluaranKasBesarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripSeparator()
        Me.PenerimaanKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengeluaranKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakBuktiKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakBuktiPenerimaanKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakBuktiPengeluaranKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakValidasiKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripSeparator()
        Me.AktivaTetapToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembelianAktivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenjualanAktivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BreakAktivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JurnalLainLainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CetakValidasiJurnalLainLainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripSeparator()
        Me.CariNasabahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CariJaminanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CariJurnalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRptRegisterNasabah = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabunganToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MutasiTabunganToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.PostingBungaTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PostingBungaAntarKantorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripSeparator()
        Me.PostingAkhirBulanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PostingAkhirTahunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripSeparator()
        Me.PembatalanPostingAkhirBulanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatalPostingBungaTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripSeparator()
        Me.KalkulatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GantiPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem22 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuLevelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GantiMenuLevelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem23 = New System.Windows.Forms.ToolStripSeparator()
        Me.PlafondUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnKonfigurasiSystem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrinterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InfoPerusahaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripSeparator()
        Me.KonfigurasiKasTellerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetupHariLiburToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BukuTabunganNasabahToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewWindowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CascadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileVerticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrangeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripSeparator()
        Me.PengecekanDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengecekanSaldoTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.NewToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.OpenToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.SaveToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrintToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.PrintPreviewToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.HelpToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.imlButton = New System.Windows.Forms.ImageList(Me.components)
        Me.imlToolbar = New System.Windows.Forms.ImageList(Me.components)
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.ToolStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenu, Me.TransaksiToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.ToolsMenu, Me.SetupToolStripMenuItem, Me.WindowsMenu, Me.EditMenu, Me.ViewMenu, Me.HelpMenu})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.WindowsMenu
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(632, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCabang, Me.ToolStripSeparator3, Me.AkuntansiToolStripMenuItem, Me.GolonganAktivaToolStripMenuItem, Me.MataUangToolStripMenuItem, Me.ToolStripSeparator5, Me.AgamaToolStripMenuItem, Me.PekerjaanToolStripMenuItem, Me.InstansiToolStripMenuItem, Me.DaerahToolStripMenuItem, Me.WilayahToolStripMenuItem, Me.ToolStripSeparator9, Me.TabunganToolStripMenuItem2, Me.DepositoToolStripMenuItem, Me.KreditToolStripMenuItem, Me.ToolStripSeparator10, Me.LogOffToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(55, 20)
        Me.FileMenu.Text = "&Master"
        '
        'mnuCabang
        '
        Me.mnuCabang.Name = "mnuCabang"
        Me.mnuCabang.Size = New System.Drawing.Size(162, 22)
        Me.mnuCabang.Text = "Cabang"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(159, 6)
        '
        'AkuntansiToolStripMenuItem
        '
        Me.AkuntansiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RekeningToolStripMenuItem, Me.AntarBankAktivaToolStripMenuItem, Me.AntarBankPasivaToolStripMenuItem})
        Me.AkuntansiToolStripMenuItem.Name = "AkuntansiToolStripMenuItem"
        Me.AkuntansiToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.AkuntansiToolStripMenuItem.Text = "Akuntansi"
        '
        'RekeningToolStripMenuItem
        '
        Me.RekeningToolStripMenuItem.Name = "RekeningToolStripMenuItem"
        Me.RekeningToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.RekeningToolStripMenuItem.Text = "Rekening"
        '
        'AntarBankAktivaToolStripMenuItem
        '
        Me.AntarBankAktivaToolStripMenuItem.Name = "AntarBankAktivaToolStripMenuItem"
        Me.AntarBankAktivaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.AntarBankAktivaToolStripMenuItem.Text = "Antar Bank Aktiva"
        '
        'AntarBankPasivaToolStripMenuItem
        '
        Me.AntarBankPasivaToolStripMenuItem.Name = "AntarBankPasivaToolStripMenuItem"
        Me.AntarBankPasivaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.AntarBankPasivaToolStripMenuItem.Text = "Antar Bank Pasiva"
        '
        'GolonganAktivaToolStripMenuItem
        '
        Me.GolonganAktivaToolStripMenuItem.Name = "GolonganAktivaToolStripMenuItem"
        Me.GolonganAktivaToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.GolonganAktivaToolStripMenuItem.Text = "Golongan Aktiva"
        '
        'MataUangToolStripMenuItem
        '
        Me.MataUangToolStripMenuItem.Name = "MataUangToolStripMenuItem"
        Me.MataUangToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.MataUangToolStripMenuItem.Text = "Mata Uang"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(159, 6)
        '
        'AgamaToolStripMenuItem
        '
        Me.AgamaToolStripMenuItem.Name = "AgamaToolStripMenuItem"
        Me.AgamaToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.AgamaToolStripMenuItem.Text = "Agama"
        '
        'PekerjaanToolStripMenuItem
        '
        Me.PekerjaanToolStripMenuItem.Name = "PekerjaanToolStripMenuItem"
        Me.PekerjaanToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.PekerjaanToolStripMenuItem.Text = "Pekerjaan"
        '
        'InstansiToolStripMenuItem
        '
        Me.InstansiToolStripMenuItem.Name = "InstansiToolStripMenuItem"
        Me.InstansiToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.InstansiToolStripMenuItem.Text = "Instansi"
        '
        'DaerahToolStripMenuItem
        '
        Me.DaerahToolStripMenuItem.Name = "DaerahToolStripMenuItem"
        Me.DaerahToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.DaerahToolStripMenuItem.Text = "Daerah"
        '
        'WilayahToolStripMenuItem
        '
        Me.WilayahToolStripMenuItem.Name = "WilayahToolStripMenuItem"
        Me.WilayahToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.WilayahToolStripMenuItem.Text = "Wilayah"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(159, 6)
        '
        'TabunganToolStripMenuItem2
        '
        Me.TabunganToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GolonganNasabahToolStripMenuItem, Me.GolonganTabunganToolStripMenuItem, Me.ToolStripSeparator11, Me.PerubahanSukuBungaToolStripMenuItem, Me.ToolStripSeparator12, Me.KodeTransaksiToolStripMenuItem, Me.KonfigurasiTabunganToolStripMenuItem})
        Me.TabunganToolStripMenuItem2.Name = "TabunganToolStripMenuItem2"
        Me.TabunganToolStripMenuItem2.Size = New System.Drawing.Size(162, 22)
        Me.TabunganToolStripMenuItem2.Text = "Tabungan"
        '
        'GolonganNasabahToolStripMenuItem
        '
        Me.GolonganNasabahToolStripMenuItem.Name = "GolonganNasabahToolStripMenuItem"
        Me.GolonganNasabahToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.GolonganNasabahToolStripMenuItem.Text = "Golongan Nasabah"
        '
        'GolonganTabunganToolStripMenuItem
        '
        Me.GolonganTabunganToolStripMenuItem.Name = "GolonganTabunganToolStripMenuItem"
        Me.GolonganTabunganToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.GolonganTabunganToolStripMenuItem.Text = "Golongan Tabungan"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(194, 6)
        '
        'PerubahanSukuBungaToolStripMenuItem
        '
        Me.PerubahanSukuBungaToolStripMenuItem.Name = "PerubahanSukuBungaToolStripMenuItem"
        Me.PerubahanSukuBungaToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.PerubahanSukuBungaToolStripMenuItem.Text = "Perubahan Suku Bunga"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(194, 6)
        '
        'KodeTransaksiToolStripMenuItem
        '
        Me.KodeTransaksiToolStripMenuItem.Name = "KodeTransaksiToolStripMenuItem"
        Me.KodeTransaksiToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.KodeTransaksiToolStripMenuItem.Text = "Kode Transaksi"
        '
        'KonfigurasiTabunganToolStripMenuItem
        '
        Me.KonfigurasiTabunganToolStripMenuItem.Name = "KonfigurasiTabunganToolStripMenuItem"
        Me.KonfigurasiTabunganToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.KonfigurasiTabunganToolStripMenuItem.Text = "Konfigurasi Tabungan"
        '
        'DepositoToolStripMenuItem
        '
        Me.DepositoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GolonganDeposanToolStripMenuItem, Me.GolonganDepositoToolStripMenuItem})
        Me.DepositoToolStripMenuItem.Name = "DepositoToolStripMenuItem"
        Me.DepositoToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.DepositoToolStripMenuItem.Text = "Deposito"
        '
        'GolonganDeposanToolStripMenuItem
        '
        Me.GolonganDeposanToolStripMenuItem.Name = "GolonganDeposanToolStripMenuItem"
        Me.GolonganDeposanToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.GolonganDeposanToolStripMenuItem.Text = "Golongan Deposan"
        '
        'GolonganDepositoToolStripMenuItem
        '
        Me.GolonganDepositoToolStripMenuItem.Name = "GolonganDepositoToolStripMenuItem"
        Me.GolonganDepositoToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.GolonganDepositoToolStripMenuItem.Text = "Golongan Deposito"
        '
        'KreditToolStripMenuItem
        '
        Me.KreditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GolonganKreditToolStripMenuItem, Me.ToolStripSeparator13, Me.SifatKreditToolStripMenuItem, Me.JToolStripMenuItem, Me.GolonganDebiturToolStripMenuItem, Me.SektorEkonomiToolStripMenuItem, Me.GolonganPenjaminToolStripMenuItem, Me.ToolStripSeparator14, Me.SetupJaminanToolStripMenuItem, Me.SuratPerjanjianToolStripMenuItem, Me.ToolStripSeparator15, Me.AccountOfficerToolStripMenuItem, Me.BendaharaToolStripMenuItem, Me.ToolStripSeparator16, Me.AdministrasiToolStripMenuItem, Me.ProvisiToolStripMenuItem})
        Me.KreditToolStripMenuItem.Name = "KreditToolStripMenuItem"
        Me.KreditToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.KreditToolStripMenuItem.Text = "Kredit"
        '
        'GolonganKreditToolStripMenuItem
        '
        Me.GolonganKreditToolStripMenuItem.Name = "GolonganKreditToolStripMenuItem"
        Me.GolonganKreditToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.GolonganKreditToolStripMenuItem.Text = "Golongan Kredit"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(176, 6)
        '
        'SifatKreditToolStripMenuItem
        '
        Me.SifatKreditToolStripMenuItem.Name = "SifatKreditToolStripMenuItem"
        Me.SifatKreditToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.SifatKreditToolStripMenuItem.Text = "Sifat Kredit"
        '
        'JToolStripMenuItem
        '
        Me.JToolStripMenuItem.Name = "JToolStripMenuItem"
        Me.JToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.JToolStripMenuItem.Text = "Jenis Penggunaan"
        '
        'GolonganDebiturToolStripMenuItem
        '
        Me.GolonganDebiturToolStripMenuItem.Name = "GolonganDebiturToolStripMenuItem"
        Me.GolonganDebiturToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.GolonganDebiturToolStripMenuItem.Text = "Golongan Debitur"
        '
        'SektorEkonomiToolStripMenuItem
        '
        Me.SektorEkonomiToolStripMenuItem.Name = "SektorEkonomiToolStripMenuItem"
        Me.SektorEkonomiToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.SektorEkonomiToolStripMenuItem.Text = "Sektor Ekonomi"
        '
        'GolonganPenjaminToolStripMenuItem
        '
        Me.GolonganPenjaminToolStripMenuItem.Name = "GolonganPenjaminToolStripMenuItem"
        Me.GolonganPenjaminToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.GolonganPenjaminToolStripMenuItem.Text = "Golongan Penjamin"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(176, 6)
        '
        'SetupJaminanToolStripMenuItem
        '
        Me.SetupJaminanToolStripMenuItem.Name = "SetupJaminanToolStripMenuItem"
        Me.SetupJaminanToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.SetupJaminanToolStripMenuItem.Text = "Setup Jaminan"
        '
        'SuratPerjanjianToolStripMenuItem
        '
        Me.SuratPerjanjianToolStripMenuItem.Name = "SuratPerjanjianToolStripMenuItem"
        Me.SuratPerjanjianToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.SuratPerjanjianToolStripMenuItem.Text = "Surat Perjanjian"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(176, 6)
        '
        'AccountOfficerToolStripMenuItem
        '
        Me.AccountOfficerToolStripMenuItem.Name = "AccountOfficerToolStripMenuItem"
        Me.AccountOfficerToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.AccountOfficerToolStripMenuItem.Text = "Account Officer"
        '
        'BendaharaToolStripMenuItem
        '
        Me.BendaharaToolStripMenuItem.Name = "BendaharaToolStripMenuItem"
        Me.BendaharaToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.BendaharaToolStripMenuItem.Text = "Bendahara"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(176, 6)
        '
        'AdministrasiToolStripMenuItem
        '
        Me.AdministrasiToolStripMenuItem.Name = "AdministrasiToolStripMenuItem"
        Me.AdministrasiToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.AdministrasiToolStripMenuItem.Text = "Administrasi"
        '
        'ProvisiToolStripMenuItem
        '
        Me.ProvisiToolStripMenuItem.Name = "ProvisiToolStripMenuItem"
        Me.ProvisiToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ProvisiToolStripMenuItem.Text = "Provisi"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(159, 6)
        '
        'LogOffToolStripMenuItem
        '
        Me.LogOffToolStripMenuItem.Name = "LogOffToolStripMenuItem"
        Me.LogOffToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.LogOffToolStripMenuItem.Text = "Log Off"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'TransaksiToolStripMenuItem
        '
        Me.TransaksiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegisterNasabahToolStripMenuItem, Me.ToolStripSeparator4, Me.TabunganToolStripMenuItem, Me.DepositoToolStripMenuItem1, Me.KreditToolStripMenuItem1, Me.ToolStripMenuItem12, Me.PenerimaanKasBesarToolStripMenuItem, Me.PengeluaranKasBesarToolStripMenuItem, Me.ToolStripMenuItem13, Me.PenerimaanKasToolStripMenuItem, Me.PengeluaranKasToolStripMenuItem, Me.CetakBuktiKasToolStripMenuItem, Me.ToolStripMenuItem14, Me.AktivaTetapToolStripMenuItem, Me.JurnalLainLainToolStripMenuItem, Me.CetakValidasiJurnalLainLainToolStripMenuItem, Me.ToolStripMenuItem15, Me.CariNasabahToolStripMenuItem, Me.CariJaminanToolStripMenuItem, Me.CariJurnalToolStripMenuItem})
        Me.TransaksiToolStripMenuItem.Name = "TransaksiToolStripMenuItem"
        Me.TransaksiToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.TransaksiToolStripMenuItem.Text = "&Transaksi"
        '
        'RegisterNasabahToolStripMenuItem
        '
        Me.RegisterNasabahToolStripMenuItem.Name = "RegisterNasabahToolStripMenuItem"
        Me.RegisterNasabahToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.RegisterNasabahToolStripMenuItem.Text = "Register Nasabah"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(230, 6)
        '
        'TabunganToolStripMenuItem
        '
        Me.TabunganToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TabunganBaruToolStripMenuItem, Me.MutasiTabunganToolStripMenuItem, Me.ToolStripMenuItem1, Me.BlokirRekeningToolStripMenuItem, Me.ToolStripMenuItem2, Me.HapusMutasiToolStripMenuItem, Me.TutupRekeningToolStripMenuItem, Me.BatalTutupRekeningToolStripMenuItem, Me.ToolStripMenuItem3, Me.CetakSlipValidasiBukuToolStripMenuItem})
        Me.TabunganToolStripMenuItem.Name = "TabunganToolStripMenuItem"
        Me.TabunganToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.TabunganToolStripMenuItem.Text = "Tabungan"
        '
        'TabunganBaruToolStripMenuItem
        '
        Me.TabunganBaruToolStripMenuItem.Name = "TabunganBaruToolStripMenuItem"
        Me.TabunganBaruToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.TabunganBaruToolStripMenuItem.Text = "Pembukaan Tabungan"
        '
        'MutasiTabunganToolStripMenuItem
        '
        Me.MutasiTabunganToolStripMenuItem.Name = "MutasiTabunganToolStripMenuItem"
        Me.MutasiTabunganToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.MutasiTabunganToolStripMenuItem.Text = "Mutasi Tabungan"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(212, 6)
        '
        'BlokirRekeningToolStripMenuItem
        '
        Me.BlokirRekeningToolStripMenuItem.Name = "BlokirRekeningToolStripMenuItem"
        Me.BlokirRekeningToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.BlokirRekeningToolStripMenuItem.Text = "Blokir Rekening"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(212, 6)
        '
        'HapusMutasiToolStripMenuItem
        '
        Me.HapusMutasiToolStripMenuItem.Name = "HapusMutasiToolStripMenuItem"
        Me.HapusMutasiToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.HapusMutasiToolStripMenuItem.Text = "Hapus Mutasi"
        '
        'TutupRekeningToolStripMenuItem
        '
        Me.TutupRekeningToolStripMenuItem.Name = "TutupRekeningToolStripMenuItem"
        Me.TutupRekeningToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.TutupRekeningToolStripMenuItem.Text = "Tutup Rekening"
        '
        'BatalTutupRekeningToolStripMenuItem
        '
        Me.BatalTutupRekeningToolStripMenuItem.Name = "BatalTutupRekeningToolStripMenuItem"
        Me.BatalTutupRekeningToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.BatalTutupRekeningToolStripMenuItem.Text = "Batal Tutup Rekening"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(212, 6)
        '
        'CetakSlipValidasiBukuToolStripMenuItem
        '
        Me.CetakSlipValidasiBukuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ValidasiMutasiToolStripMenuItem, Me.HeaderBukuTabunganToolStripMenuItem, Me.CetakBukuTabunganToolStripMenuItem})
        Me.CetakSlipValidasiBukuToolStripMenuItem.Name = "CetakSlipValidasiBukuToolStripMenuItem"
        Me.CetakSlipValidasiBukuToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.CetakSlipValidasiBukuToolStripMenuItem.Text = "Cetak Slip / Validasi / Buku"
        '
        'ValidasiMutasiToolStripMenuItem
        '
        Me.ValidasiMutasiToolStripMenuItem.Name = "ValidasiMutasiToolStripMenuItem"
        Me.ValidasiMutasiToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.ValidasiMutasiToolStripMenuItem.Text = "Validasi Mutasi"
        '
        'HeaderBukuTabunganToolStripMenuItem
        '
        Me.HeaderBukuTabunganToolStripMenuItem.Name = "HeaderBukuTabunganToolStripMenuItem"
        Me.HeaderBukuTabunganToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.HeaderBukuTabunganToolStripMenuItem.Text = "Header Buku Tabungan"
        '
        'CetakBukuTabunganToolStripMenuItem
        '
        Me.CetakBukuTabunganToolStripMenuItem.Name = "CetakBukuTabunganToolStripMenuItem"
        Me.CetakBukuTabunganToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.CetakBukuTabunganToolStripMenuItem.Text = "Cetak Buku Tabungan"
        '
        'DepositoToolStripMenuItem1
        '
        Me.DepositoToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReToolStripMenuItem, Me.PenyetoranDepositoToolStripMenuItem, Me.ToolStripMenuItem4, Me.MutasiDepositoToolStripMenuItem, Me.PerubahanSukuBungaToolStripMenuItem1, Me.PerpanjanganDepositoToolStripMenuItem, Me.BlokirDepositoToolStripMenuItem, Me.ToolStripMenuItem6, Me.HapusMutasiDepositoToolStripMenuItem, Me.PembatalanToolStripMenuItem, Me.ToolStripMenuItem5, Me.CetakSlipKartuBilyetToolStripMenuItem, Me.ToolStripMenuItem7, Me.KoreksiDataDepositoToolStripMenuItem, Me.KoreksiTanggalPerpanjanganToolStripMenuItem})
        Me.DepositoToolStripMenuItem1.Name = "DepositoToolStripMenuItem1"
        Me.DepositoToolStripMenuItem1.Size = New System.Drawing.Size(233, 22)
        Me.DepositoToolStripMenuItem1.Text = "Deposito"
        '
        'ReToolStripMenuItem
        '
        Me.ReToolStripMenuItem.Name = "ReToolStripMenuItem"
        Me.ReToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.ReToolStripMenuItem.Text = "Registrasi Deposito"
        '
        'PenyetoranDepositoToolStripMenuItem
        '
        Me.PenyetoranDepositoToolStripMenuItem.Name = "PenyetoranDepositoToolStripMenuItem"
        Me.PenyetoranDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PenyetoranDepositoToolStripMenuItem.Text = "Penyetoran Deposito"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(239, 6)
        '
        'MutasiDepositoToolStripMenuItem
        '
        Me.MutasiDepositoToolStripMenuItem.Name = "MutasiDepositoToolStripMenuItem"
        Me.MutasiDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.MutasiDepositoToolStripMenuItem.Text = "Mutasi Deposito"
        '
        'PerubahanSukuBungaToolStripMenuItem1
        '
        Me.PerubahanSukuBungaToolStripMenuItem1.Name = "PerubahanSukuBungaToolStripMenuItem1"
        Me.PerubahanSukuBungaToolStripMenuItem1.Size = New System.Drawing.Size(242, 22)
        Me.PerubahanSukuBungaToolStripMenuItem1.Text = "Perubahan Suku Bunga"
        '
        'PerpanjanganDepositoToolStripMenuItem
        '
        Me.PerpanjanganDepositoToolStripMenuItem.Name = "PerpanjanganDepositoToolStripMenuItem"
        Me.PerpanjanganDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PerpanjanganDepositoToolStripMenuItem.Text = "Perpanjangan Deposito"
        '
        'BlokirDepositoToolStripMenuItem
        '
        Me.BlokirDepositoToolStripMenuItem.Name = "BlokirDepositoToolStripMenuItem"
        Me.BlokirDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.BlokirDepositoToolStripMenuItem.Text = "Blokir Deposito"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(239, 6)
        '
        'HapusMutasiDepositoToolStripMenuItem
        '
        Me.HapusMutasiDepositoToolStripMenuItem.Name = "HapusMutasiDepositoToolStripMenuItem"
        Me.HapusMutasiDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusMutasiDepositoToolStripMenuItem.Text = "Hapus Mutasi Deposito"
        '
        'PembatalanToolStripMenuItem
        '
        Me.PembatalanToolStripMenuItem.Name = "PembatalanToolStripMenuItem"
        Me.PembatalanToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PembatalanToolStripMenuItem.Text = "Pembatalan Pencairan Deposito"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(239, 6)
        '
        'CetakSlipKartuBilyetToolStripMenuItem
        '
        Me.CetakSlipKartuBilyetToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BilyetDepositoToolStripMenuItem, Me.TandaTerimaBungaDepositoToolStripMenuItem, Me.BuktiPencairanDepositoToolStripMenuItem, Me.ValidasiDepositoToolStripMenuItem})
        Me.CetakSlipKartuBilyetToolStripMenuItem.Name = "CetakSlipKartuBilyetToolStripMenuItem"
        Me.CetakSlipKartuBilyetToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.CetakSlipKartuBilyetToolStripMenuItem.Text = "Cetak Slip / Kartu / Bilyet"
        '
        'BilyetDepositoToolStripMenuItem
        '
        Me.BilyetDepositoToolStripMenuItem.Name = "BilyetDepositoToolStripMenuItem"
        Me.BilyetDepositoToolStripMenuItem.Size = New System.Drawing.Size(234, 22)
        Me.BilyetDepositoToolStripMenuItem.Text = "Bilyet Deposito"
        '
        'TandaTerimaBungaDepositoToolStripMenuItem
        '
        Me.TandaTerimaBungaDepositoToolStripMenuItem.Name = "TandaTerimaBungaDepositoToolStripMenuItem"
        Me.TandaTerimaBungaDepositoToolStripMenuItem.Size = New System.Drawing.Size(234, 22)
        Me.TandaTerimaBungaDepositoToolStripMenuItem.Text = "Tanda Terima Bunga Deposito"
        '
        'BuktiPencairanDepositoToolStripMenuItem
        '
        Me.BuktiPencairanDepositoToolStripMenuItem.Name = "BuktiPencairanDepositoToolStripMenuItem"
        Me.BuktiPencairanDepositoToolStripMenuItem.Size = New System.Drawing.Size(234, 22)
        Me.BuktiPencairanDepositoToolStripMenuItem.Text = "Bukti Pencairan Deposito"
        '
        'ValidasiDepositoToolStripMenuItem
        '
        Me.ValidasiDepositoToolStripMenuItem.Name = "ValidasiDepositoToolStripMenuItem"
        Me.ValidasiDepositoToolStripMenuItem.Size = New System.Drawing.Size(234, 22)
        Me.ValidasiDepositoToolStripMenuItem.Text = "Validasi Deposito"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(239, 6)
        '
        'KoreksiDataDepositoToolStripMenuItem
        '
        Me.KoreksiDataDepositoToolStripMenuItem.Name = "KoreksiDataDepositoToolStripMenuItem"
        Me.KoreksiDataDepositoToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.KoreksiDataDepositoToolStripMenuItem.Text = "Koreksi Data Deposito"
        '
        'KoreksiTanggalPerpanjanganToolStripMenuItem
        '
        Me.KoreksiTanggalPerpanjanganToolStripMenuItem.Name = "KoreksiTanggalPerpanjanganToolStripMenuItem"
        Me.KoreksiTanggalPerpanjanganToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.KoreksiTanggalPerpanjanganToolStripMenuItem.Text = "Koreksi Tanggal Perpanjangan"
        '
        'KreditToolStripMenuItem1
        '
        Me.KreditToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SimulasiKreditToolStripMenuItem, Me.ToolStripMenuItem8, Me.PengajuanKreditToolStripMenuItem, Me.RealisasiKreditToolStripMenuItem, Me.JaminanToolStripMenuItem, Me.CetakSuratPerjanijanKreditToolStripMenuItem, Me.CetakIdentitasJaminanKreditToolStripMenuItem, Me.PencairanKreditToolStripMenuItem, Me.PenarikanPlafondKreditToolStripMenuItem, Me.ToolStripMenuItem10, Me.AngsuranKreditInstansiTitipanToolStripMenuItem, Me.AngsuranKreditInstansiToolStripMenuItem, Me.AngsuranKreditKasirToolStripMenuItem, Me.PostingBungaRKToolStripMenuItem, Me.PindahRekeningKreditToolStripMenuItem, Me.AutoDebetAngsuranToolStripMenuItem, Me.AsuransiToolStripMenuItem, Me.ToolStripMenuItem11, Me.PenolakanPengajuanKreditToolStripMenuItem, Me.HapusDataToolStripMenuItem, Me.BatalPencairanKreditToolStripMenuItem, Me.ToolStripSeparator17, Me.KoreksiDataToolStripMenuItem, Me.CetakSlipKartuToolStripMenuItem})
        Me.KreditToolStripMenuItem1.Name = "KreditToolStripMenuItem1"
        Me.KreditToolStripMenuItem1.Size = New System.Drawing.Size(233, 22)
        Me.KreditToolStripMenuItem1.Text = "Kredit"
        '
        'SimulasiKreditToolStripMenuItem
        '
        Me.SimulasiKreditToolStripMenuItem.Name = "SimulasiKreditToolStripMenuItem"
        Me.SimulasiKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.SimulasiKreditToolStripMenuItem.Text = "Simulasi Kredit"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(239, 6)
        '
        'PengajuanKreditToolStripMenuItem
        '
        Me.PengajuanKreditToolStripMenuItem.Name = "PengajuanKreditToolStripMenuItem"
        Me.PengajuanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PengajuanKreditToolStripMenuItem.Text = "Pengajuan Kredit"
        '
        'RealisasiKreditToolStripMenuItem
        '
        Me.RealisasiKreditToolStripMenuItem.Name = "RealisasiKreditToolStripMenuItem"
        Me.RealisasiKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.RealisasiKreditToolStripMenuItem.Text = "Realisasi Kredit"
        '
        'JaminanToolStripMenuItem
        '
        Me.JaminanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PengikatanJaminanToolStripMenuItem, Me.PengambilanJaminanToolStripMenuItem, Me.ToolStripMenuItem9, Me.RegisterJaminanToolStripMenuItem, Me.RegisterJaminanBrankasToolStripMenuItem})
        Me.JaminanToolStripMenuItem.Name = "JaminanToolStripMenuItem"
        Me.JaminanToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.JaminanToolStripMenuItem.Text = "Jaminan"
        '
        'PengikatanJaminanToolStripMenuItem
        '
        Me.PengikatanJaminanToolStripMenuItem.Name = "PengikatanJaminanToolStripMenuItem"
        Me.PengikatanJaminanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PengikatanJaminanToolStripMenuItem.Text = "Pengikatan Jaminan"
        '
        'PengambilanJaminanToolStripMenuItem
        '
        Me.PengambilanJaminanToolStripMenuItem.Name = "PengambilanJaminanToolStripMenuItem"
        Me.PengambilanJaminanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PengambilanJaminanToolStripMenuItem.Text = "Pengambilan Jaminan"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(204, 6)
        '
        'RegisterJaminanToolStripMenuItem
        '
        Me.RegisterJaminanToolStripMenuItem.Name = "RegisterJaminanToolStripMenuItem"
        Me.RegisterJaminanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.RegisterJaminanToolStripMenuItem.Text = "Register Jaminan"
        '
        'RegisterJaminanBrankasToolStripMenuItem
        '
        Me.RegisterJaminanBrankasToolStripMenuItem.Name = "RegisterJaminanBrankasToolStripMenuItem"
        Me.RegisterJaminanBrankasToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.RegisterJaminanBrankasToolStripMenuItem.Text = "Register Jaminan Brankas"
        '
        'CetakSuratPerjanijanKreditToolStripMenuItem
        '
        Me.CetakSuratPerjanijanKreditToolStripMenuItem.Name = "CetakSuratPerjanijanKreditToolStripMenuItem"
        Me.CetakSuratPerjanijanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.CetakSuratPerjanijanKreditToolStripMenuItem.Text = "Cetak Surat Perjanijan Kredit"
        '
        'CetakIdentitasJaminanKreditToolStripMenuItem
        '
        Me.CetakIdentitasJaminanKreditToolStripMenuItem.Name = "CetakIdentitasJaminanKreditToolStripMenuItem"
        Me.CetakIdentitasJaminanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.CetakIdentitasJaminanKreditToolStripMenuItem.Text = "Cetak Identitas Jaminan Kredit"
        '
        'PencairanKreditToolStripMenuItem
        '
        Me.PencairanKreditToolStripMenuItem.Name = "PencairanKreditToolStripMenuItem"
        Me.PencairanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PencairanKreditToolStripMenuItem.Text = "Pencairan Kredit"
        '
        'PenarikanPlafondKreditToolStripMenuItem
        '
        Me.PenarikanPlafondKreditToolStripMenuItem.Name = "PenarikanPlafondKreditToolStripMenuItem"
        Me.PenarikanPlafondKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PenarikanPlafondKreditToolStripMenuItem.Text = "Penarikan Plafond Kredit"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(239, 6)
        '
        'AngsuranKreditInstansiTitipanToolStripMenuItem
        '
        Me.AngsuranKreditInstansiTitipanToolStripMenuItem.Name = "AngsuranKreditInstansiTitipanToolStripMenuItem"
        Me.AngsuranKreditInstansiTitipanToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AngsuranKreditInstansiTitipanToolStripMenuItem.Text = "Angsuran Kredit Instansi Titipan"
        '
        'AngsuranKreditInstansiToolStripMenuItem
        '
        Me.AngsuranKreditInstansiToolStripMenuItem.Name = "AngsuranKreditInstansiToolStripMenuItem"
        Me.AngsuranKreditInstansiToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AngsuranKreditInstansiToolStripMenuItem.Text = "Angsuran Kredit Instansi"
        '
        'AngsuranKreditKasirToolStripMenuItem
        '
        Me.AngsuranKreditKasirToolStripMenuItem.Name = "AngsuranKreditKasirToolStripMenuItem"
        Me.AngsuranKreditKasirToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AngsuranKreditKasirToolStripMenuItem.Text = "Angsuran Kredit Kasir"
        '
        'PostingBungaRKToolStripMenuItem
        '
        Me.PostingBungaRKToolStripMenuItem.Name = "PostingBungaRKToolStripMenuItem"
        Me.PostingBungaRKToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PostingBungaRKToolStripMenuItem.Text = "Posting Bunga RK"
        '
        'PindahRekeningKreditToolStripMenuItem
        '
        Me.PindahRekeningKreditToolStripMenuItem.Name = "PindahRekeningKreditToolStripMenuItem"
        Me.PindahRekeningKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PindahRekeningKreditToolStripMenuItem.Text = "Pindah Rekening Kredit"
        '
        'AutoDebetAngsuranToolStripMenuItem
        '
        Me.AutoDebetAngsuranToolStripMenuItem.Name = "AutoDebetAngsuranToolStripMenuItem"
        Me.AutoDebetAngsuranToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AutoDebetAngsuranToolStripMenuItem.Text = "Auto Debet Angsuran"
        '
        'AsuransiToolStripMenuItem
        '
        Me.AsuransiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BumiPuteraToolStripMenuItem, Me.JamkridaToolStripMenuItem})
        Me.AsuransiToolStripMenuItem.Name = "AsuransiToolStripMenuItem"
        Me.AsuransiToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AsuransiToolStripMenuItem.Text = "Asuransi"
        '
        'BumiPuteraToolStripMenuItem
        '
        Me.BumiPuteraToolStripMenuItem.Name = "BumiPuteraToolStripMenuItem"
        Me.BumiPuteraToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.BumiPuteraToolStripMenuItem.Text = "Bumi Putera"
        '
        'JamkridaToolStripMenuItem
        '
        Me.JamkridaToolStripMenuItem.Name = "JamkridaToolStripMenuItem"
        Me.JamkridaToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.JamkridaToolStripMenuItem.Text = "Jamkrida"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(239, 6)
        '
        'PenolakanPengajuanKreditToolStripMenuItem
        '
        Me.PenolakanPengajuanKreditToolStripMenuItem.Name = "PenolakanPengajuanKreditToolStripMenuItem"
        Me.PenolakanPengajuanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.PenolakanPengajuanKreditToolStripMenuItem.Text = "Penolakan Pengajuan Kredit"
        '
        'HapusDataToolStripMenuItem
        '
        Me.HapusDataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HapusAngsuranKreditToolStripMenuItem, Me.HapusAngsuranKreditInstansiTitipanToolStripMenuItem, Me.HapusAngsuranKreditInstansiToolStripMenuItem, Me.HapusMutasiAsuransiToolStripMenuItem})
        Me.HapusDataToolStripMenuItem.Name = "HapusDataToolStripMenuItem"
        Me.HapusDataToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusDataToolStripMenuItem.Text = "Hapus Data"
        '
        'HapusAngsuranKreditToolStripMenuItem
        '
        Me.HapusAngsuranKreditToolStripMenuItem.Name = "HapusAngsuranKreditToolStripMenuItem"
        Me.HapusAngsuranKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusAngsuranKreditToolStripMenuItem.Text = "Angsuran Kredit"
        '
        'HapusAngsuranKreditInstansiTitipanToolStripMenuItem
        '
        Me.HapusAngsuranKreditInstansiTitipanToolStripMenuItem.Name = "HapusAngsuranKreditInstansiTitipanToolStripMenuItem"
        Me.HapusAngsuranKreditInstansiTitipanToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusAngsuranKreditInstansiTitipanToolStripMenuItem.Text = "Angsuran Kredit Instansi Titipan"
        '
        'HapusAngsuranKreditInstansiToolStripMenuItem
        '
        Me.HapusAngsuranKreditInstansiToolStripMenuItem.Name = "HapusAngsuranKreditInstansiToolStripMenuItem"
        Me.HapusAngsuranKreditInstansiToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusAngsuranKreditInstansiToolStripMenuItem.Text = "Angsuran Kredit Instansi"
        '
        'HapusMutasiAsuransiToolStripMenuItem
        '
        Me.HapusMutasiAsuransiToolStripMenuItem.Name = "HapusMutasiAsuransiToolStripMenuItem"
        Me.HapusMutasiAsuransiToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.HapusMutasiAsuransiToolStripMenuItem.Text = "Mutasi Asuransi"
        '
        'BatalPencairanKreditToolStripMenuItem
        '
        Me.BatalPencairanKreditToolStripMenuItem.Name = "BatalPencairanKreditToolStripMenuItem"
        Me.BatalPencairanKreditToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.BatalPencairanKreditToolStripMenuItem.Text = "Batal Pencairan Kredit"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(239, 6)
        '
        'KoreksiDataToolStripMenuItem
        '
        Me.KoreksiDataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KoreksiDataKreditToolStripMenuItem, Me.KoreksiStatusPengajuanKreditStripMenuItem, Me.UpdateJadwalKreditStripMenuItem, Me.UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem})
        Me.KoreksiDataToolStripMenuItem.Name = "KoreksiDataToolStripMenuItem"
        Me.KoreksiDataToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.KoreksiDataToolStripMenuItem.Text = "Koreksi Data"
        '
        'KoreksiDataKreditToolStripMenuItem
        '
        Me.KoreksiDataKreditToolStripMenuItem.Name = "KoreksiDataKreditToolStripMenuItem"
        Me.KoreksiDataKreditToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.KoreksiDataKreditToolStripMenuItem.Text = "Koreksi Data Kredit"
        '
        'KoreksiStatusPengajuanKreditStripMenuItem
        '
        Me.KoreksiStatusPengajuanKreditStripMenuItem.Name = "KoreksiStatusPengajuanKreditStripMenuItem"
        Me.KoreksiStatusPengajuanKreditStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.KoreksiStatusPengajuanKreditStripMenuItem.Text = "Koreksi Status Pengajuan Kredit"
        '
        'UpdateJadwalKreditStripMenuItem
        '
        Me.UpdateJadwalKreditStripMenuItem.Name = "UpdateJadwalKreditStripMenuItem"
        Me.UpdateJadwalKreditStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.UpdateJadwalKreditStripMenuItem.Text = "Update Jadwal Kredit"
        '
        'UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem
        '
        Me.UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem.Name = "UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem"
        Me.UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem.Text = "Update Jadwal Amortisasi Administrasi Provisi"
        '
        'CetakSlipKartuToolStripMenuItem
        '
        Me.CetakSlipKartuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CetakHeaderKartuAngsuranToolStripMenuItem, Me.CetakSlipPenerimaanPinjamanToolStripMenuItem, Me.CetakSlipAngsuranToolStripMenuItem, Me.CetakValidasiAngsuranToolStripMenuItem, Me.CetakSlipAngsuranInstansiToolStripMenuItem, Me.CetakTandaTerimaPengambilanJaminanToolStripMenuItem})
        Me.CetakSlipKartuToolStripMenuItem.Name = "CetakSlipKartuToolStripMenuItem"
        Me.CetakSlipKartuToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.CetakSlipKartuToolStripMenuItem.Text = "Cetak Slip / Kartu"
        '
        'CetakHeaderKartuAngsuranToolStripMenuItem
        '
        Me.CetakHeaderKartuAngsuranToolStripMenuItem.Name = "CetakHeaderKartuAngsuranToolStripMenuItem"
        Me.CetakHeaderKartuAngsuranToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakHeaderKartuAngsuranToolStripMenuItem.Text = "Cetak Header Kartu Angsuran"
        '
        'CetakSlipPenerimaanPinjamanToolStripMenuItem
        '
        Me.CetakSlipPenerimaanPinjamanToolStripMenuItem.Name = "CetakSlipPenerimaanPinjamanToolStripMenuItem"
        Me.CetakSlipPenerimaanPinjamanToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakSlipPenerimaanPinjamanToolStripMenuItem.Text = "Cetak Slip Penerimaan Pinjaman"
        '
        'CetakSlipAngsuranToolStripMenuItem
        '
        Me.CetakSlipAngsuranToolStripMenuItem.Name = "CetakSlipAngsuranToolStripMenuItem"
        Me.CetakSlipAngsuranToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakSlipAngsuranToolStripMenuItem.Text = "Cetak Slip Angsuran"
        '
        'CetakValidasiAngsuranToolStripMenuItem
        '
        Me.CetakValidasiAngsuranToolStripMenuItem.Name = "CetakValidasiAngsuranToolStripMenuItem"
        Me.CetakValidasiAngsuranToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakValidasiAngsuranToolStripMenuItem.Text = "Cetak Validasi Angsuran"
        '
        'CetakSlipAngsuranInstansiToolStripMenuItem
        '
        Me.CetakSlipAngsuranInstansiToolStripMenuItem.Name = "CetakSlipAngsuranInstansiToolStripMenuItem"
        Me.CetakSlipAngsuranInstansiToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakSlipAngsuranInstansiToolStripMenuItem.Text = "Cetak Slip Angsuran Instansi"
        '
        'CetakTandaTerimaPengambilanJaminanToolStripMenuItem
        '
        Me.CetakTandaTerimaPengambilanJaminanToolStripMenuItem.Name = "CetakTandaTerimaPengambilanJaminanToolStripMenuItem"
        Me.CetakTandaTerimaPengambilanJaminanToolStripMenuItem.Size = New System.Drawing.Size(300, 22)
        Me.CetakTandaTerimaPengambilanJaminanToolStripMenuItem.Text = "Cetak Tanda Terima Pengambilan Jaminan"
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(230, 6)
        '
        'PenerimaanKasBesarToolStripMenuItem
        '
        Me.PenerimaanKasBesarToolStripMenuItem.Name = "PenerimaanKasBesarToolStripMenuItem"
        Me.PenerimaanKasBesarToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.PenerimaanKasBesarToolStripMenuItem.Text = "Penerimaan Kas Besar"
        '
        'PengeluaranKasBesarToolStripMenuItem
        '
        Me.PengeluaranKasBesarToolStripMenuItem.Name = "PengeluaranKasBesarToolStripMenuItem"
        Me.PengeluaranKasBesarToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.PengeluaranKasBesarToolStripMenuItem.Text = "Pengeluaran Kas Besar"
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(230, 6)
        '
        'PenerimaanKasToolStripMenuItem
        '
        Me.PenerimaanKasToolStripMenuItem.Name = "PenerimaanKasToolStripMenuItem"
        Me.PenerimaanKasToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.PenerimaanKasToolStripMenuItem.Text = "Penerimaan Kas"
        '
        'PengeluaranKasToolStripMenuItem
        '
        Me.PengeluaranKasToolStripMenuItem.Name = "PengeluaranKasToolStripMenuItem"
        Me.PengeluaranKasToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.PengeluaranKasToolStripMenuItem.Text = "Pengeluaran Kas"
        '
        'CetakBuktiKasToolStripMenuItem
        '
        Me.CetakBuktiKasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CetakBuktiPenerimaanKasToolStripMenuItem, Me.CetakBuktiPengeluaranKasToolStripMenuItem, Me.CetakValidasiKasToolStripMenuItem})
        Me.CetakBuktiKasToolStripMenuItem.Name = "CetakBuktiKasToolStripMenuItem"
        Me.CetakBuktiKasToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.CetakBuktiKasToolStripMenuItem.Text = "Cetak Bukti Kas"
        '
        'CetakBuktiPenerimaanKasToolStripMenuItem
        '
        Me.CetakBuktiPenerimaanKasToolStripMenuItem.Name = "CetakBuktiPenerimaanKasToolStripMenuItem"
        Me.CetakBuktiPenerimaanKasToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.CetakBuktiPenerimaanKasToolStripMenuItem.Text = "Cetak Bukti Penerimaan Kas"
        '
        'CetakBuktiPengeluaranKasToolStripMenuItem
        '
        Me.CetakBuktiPengeluaranKasToolStripMenuItem.Name = "CetakBuktiPengeluaranKasToolStripMenuItem"
        Me.CetakBuktiPengeluaranKasToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.CetakBuktiPengeluaranKasToolStripMenuItem.Text = "Cetak Bukti Pengeluaran Kas"
        '
        'CetakValidasiKasToolStripMenuItem
        '
        Me.CetakValidasiKasToolStripMenuItem.Name = "CetakValidasiKasToolStripMenuItem"
        Me.CetakValidasiKasToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.CetakValidasiKasToolStripMenuItem.Text = "Cetak Validasi Kas"
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(230, 6)
        '
        'AktivaTetapToolStripMenuItem
        '
        Me.AktivaTetapToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PembelianAktivaToolStripMenuItem, Me.PenjualanAktivaToolStripMenuItem, Me.BreakAktivaToolStripMenuItem})
        Me.AktivaTetapToolStripMenuItem.Name = "AktivaTetapToolStripMenuItem"
        Me.AktivaTetapToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.AktivaTetapToolStripMenuItem.Text = "Aktiva Tetap"
        '
        'PembelianAktivaToolStripMenuItem
        '
        Me.PembelianAktivaToolStripMenuItem.Name = "PembelianAktivaToolStripMenuItem"
        Me.PembelianAktivaToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.PembelianAktivaToolStripMenuItem.Text = "Pembelian Aktiva"
        '
        'PenjualanAktivaToolStripMenuItem
        '
        Me.PenjualanAktivaToolStripMenuItem.Name = "PenjualanAktivaToolStripMenuItem"
        Me.PenjualanAktivaToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.PenjualanAktivaToolStripMenuItem.Text = "Penjualan Aktiva"
        '
        'BreakAktivaToolStripMenuItem
        '
        Me.BreakAktivaToolStripMenuItem.Name = "BreakAktivaToolStripMenuItem"
        Me.BreakAktivaToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.BreakAktivaToolStripMenuItem.Text = "Break Aktiva"
        '
        'JurnalLainLainToolStripMenuItem
        '
        Me.JurnalLainLainToolStripMenuItem.Name = "JurnalLainLainToolStripMenuItem"
        Me.JurnalLainLainToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.JurnalLainLainToolStripMenuItem.Text = "Jurnal Lain-Lain"
        '
        'CetakValidasiJurnalLainLainToolStripMenuItem
        '
        Me.CetakValidasiJurnalLainLainToolStripMenuItem.Name = "CetakValidasiJurnalLainLainToolStripMenuItem"
        Me.CetakValidasiJurnalLainLainToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.CetakValidasiJurnalLainLainToolStripMenuItem.Text = "Cetak Validasi Jurnal Lain-Lain"
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(230, 6)
        '
        'CariNasabahToolStripMenuItem
        '
        Me.CariNasabahToolStripMenuItem.Name = "CariNasabahToolStripMenuItem"
        Me.CariNasabahToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.CariNasabahToolStripMenuItem.Text = "Cari Register Nasabah"
        '
        'CariJaminanToolStripMenuItem
        '
        Me.CariJaminanToolStripMenuItem.Name = "CariJaminanToolStripMenuItem"
        Me.CariJaminanToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.CariJaminanToolStripMenuItem.Text = "Cari Jaminan"
        '
        'CariJurnalToolStripMenuItem
        '
        Me.CariJurnalToolStripMenuItem.Name = "CariJurnalToolStripMenuItem"
        Me.CariJurnalToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.CariJurnalToolStripMenuItem.Text = "Cari Jurnal"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRptRegisterNasabah, Me.TabunganToolStripMenuItem1})
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.LaporanToolStripMenuItem.Text = "&Laporan"
        '
        'mnuRptRegisterNasabah
        '
        Me.mnuRptRegisterNasabah.Name = "mnuRptRegisterNasabah"
        Me.mnuRptRegisterNasabah.Size = New System.Drawing.Size(165, 22)
        Me.mnuRptRegisterNasabah.Text = "Register Nasabah"
        '
        'TabunganToolStripMenuItem1
        '
        Me.TabunganToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MutasiTabunganToolStripMenuItem1})
        Me.TabunganToolStripMenuItem1.Name = "TabunganToolStripMenuItem1"
        Me.TabunganToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.TabunganToolStripMenuItem1.Text = "Tabungan"
        '
        'MutasiTabunganToolStripMenuItem1
        '
        Me.MutasiTabunganToolStripMenuItem1.Name = "MutasiTabunganToolStripMenuItem1"
        Me.MutasiTabunganToolStripMenuItem1.Size = New System.Drawing.Size(167, 22)
        Me.MutasiTabunganToolStripMenuItem1.Text = "Mutasi Tabungan"
        '
        'ToolsMenu
        '
        Me.ToolsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PostingBungaTabunganToolStripMenuItem, Me.PostingBungaAntarKantorToolStripMenuItem, Me.ToolStripMenuItem16, Me.PostingAkhirBulanToolStripMenuItem, Me.PostingAkhirTahunToolStripMenuItem, Me.ToolStripMenuItem17, Me.PembatalanPostingAkhirBulanToolStripMenuItem, Me.BatalPostingBungaTabunganToolStripMenuItem, Me.ToolStripMenuItem18, Me.KalkulatorToolStripMenuItem})
        Me.ToolsMenu.Name = "ToolsMenu"
        Me.ToolsMenu.Size = New System.Drawing.Size(50, 20)
        Me.ToolsMenu.Text = "&Utility"
        '
        'PostingBungaTabunganToolStripMenuItem
        '
        Me.PostingBungaTabunganToolStripMenuItem.Name = "PostingBungaTabunganToolStripMenuItem"
        Me.PostingBungaTabunganToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.PostingBungaTabunganToolStripMenuItem.Text = "Posting Bunga Tabungan"
        '
        'PostingBungaAntarKantorToolStripMenuItem
        '
        Me.PostingBungaAntarKantorToolStripMenuItem.Name = "PostingBungaAntarKantorToolStripMenuItem"
        Me.PostingBungaAntarKantorToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.PostingBungaAntarKantorToolStripMenuItem.Text = "Posting Bunga Antar Kantor"
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(234, 6)
        '
        'PostingAkhirBulanToolStripMenuItem
        '
        Me.PostingAkhirBulanToolStripMenuItem.Name = "PostingAkhirBulanToolStripMenuItem"
        Me.PostingAkhirBulanToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.PostingAkhirBulanToolStripMenuItem.Text = "Posting Akhir Bulan"
        '
        'PostingAkhirTahunToolStripMenuItem
        '
        Me.PostingAkhirTahunToolStripMenuItem.Name = "PostingAkhirTahunToolStripMenuItem"
        Me.PostingAkhirTahunToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.PostingAkhirTahunToolStripMenuItem.Text = "Posting Akhir Tahun"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(234, 6)
        '
        'PembatalanPostingAkhirBulanToolStripMenuItem
        '
        Me.PembatalanPostingAkhirBulanToolStripMenuItem.Name = "PembatalanPostingAkhirBulanToolStripMenuItem"
        Me.PembatalanPostingAkhirBulanToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.PembatalanPostingAkhirBulanToolStripMenuItem.Text = "Batal Posting Akhir Bulan"
        '
        'BatalPostingBungaTabunganToolStripMenuItem
        '
        Me.BatalPostingBungaTabunganToolStripMenuItem.Name = "BatalPostingBungaTabunganToolStripMenuItem"
        Me.BatalPostingBungaTabunganToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.BatalPostingBungaTabunganToolStripMenuItem.Text = "Batal Posting Bunga Tabungan"
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(234, 6)
        '
        'KalkulatorToolStripMenuItem
        '
        Me.KalkulatorToolStripMenuItem.Name = "KalkulatorToolStripMenuItem"
        Me.KalkulatorToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.KalkulatorToolStripMenuItem.Text = "Kalkulator"
        '
        'SetupToolStripMenuItem
        '
        Me.SetupToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UserPasswordToolStripMenuItem, Me.GantiPasswordToolStripMenuItem, Me.ResetPasswordToolStripMenuItem, Me.ToolStripMenuItem22, Me.MenuLevelToolStripMenuItem, Me.GantiMenuLevelToolStripMenuItem, Me.ToolStripMenuItem23, Me.PlafondUserToolStripMenuItem, Me.KonfigurasiKasTellerToolStripMenuItem, Me.ToolStripMenuItem19, Me.mnKonfigurasiSystem, Me.PrinterToolStripMenuItem, Me.InfoPerusahaanToolStripMenuItem, Me.ToolStripMenuItem20, Me.SetupHariLiburToolStripMenuItem, Me.BukuTabunganNasabahToolStripMenuItem})
        Me.SetupToolStripMenuItem.Name = "SetupToolStripMenuItem"
        Me.SetupToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
        Me.SetupToolStripMenuItem.Text = "&Setup"
        '
        'UserPasswordToolStripMenuItem
        '
        Me.UserPasswordToolStripMenuItem.Name = "UserPasswordToolStripMenuItem"
        Me.UserPasswordToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.UserPasswordToolStripMenuItem.Text = "User Password"
        '
        'GantiPasswordToolStripMenuItem
        '
        Me.GantiPasswordToolStripMenuItem.Name = "GantiPasswordToolStripMenuItem"
        Me.GantiPasswordToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.GantiPasswordToolStripMenuItem.Text = "Ganti Password"
        '
        'ResetPasswordToolStripMenuItem
        '
        Me.ResetPasswordToolStripMenuItem.Name = "ResetPasswordToolStripMenuItem"
        Me.ResetPasswordToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ResetPasswordToolStripMenuItem.Text = "Reset Password"
        '
        'ToolStripMenuItem22
        '
        Me.ToolStripMenuItem22.Name = "ToolStripMenuItem22"
        Me.ToolStripMenuItem22.Size = New System.Drawing.Size(204, 6)
        '
        'MenuLevelToolStripMenuItem
        '
        Me.MenuLevelToolStripMenuItem.Name = "MenuLevelToolStripMenuItem"
        Me.MenuLevelToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.MenuLevelToolStripMenuItem.Text = "Menu Level"
        '
        'GantiMenuLevelToolStripMenuItem
        '
        Me.GantiMenuLevelToolStripMenuItem.Name = "GantiMenuLevelToolStripMenuItem"
        Me.GantiMenuLevelToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.GantiMenuLevelToolStripMenuItem.Text = "Ganti Menu Level"
        '
        'ToolStripMenuItem23
        '
        Me.ToolStripMenuItem23.Name = "ToolStripMenuItem23"
        Me.ToolStripMenuItem23.Size = New System.Drawing.Size(204, 6)
        '
        'PlafondUserToolStripMenuItem
        '
        Me.PlafondUserToolStripMenuItem.Name = "PlafondUserToolStripMenuItem"
        Me.PlafondUserToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PlafondUserToolStripMenuItem.Text = "Plafond User"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(204, 6)
        '
        'mnKonfigurasiSystem
        '
        Me.mnKonfigurasiSystem.Name = "mnKonfigurasiSystem"
        Me.mnKonfigurasiSystem.Size = New System.Drawing.Size(207, 22)
        Me.mnKonfigurasiSystem.Text = "System"
        '
        'PrinterToolStripMenuItem
        '
        Me.PrinterToolStripMenuItem.Name = "PrinterToolStripMenuItem"
        Me.PrinterToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PrinterToolStripMenuItem.Text = "Printer"
        '
        'InfoPerusahaanToolStripMenuItem
        '
        Me.InfoPerusahaanToolStripMenuItem.Name = "InfoPerusahaanToolStripMenuItem"
        Me.InfoPerusahaanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.InfoPerusahaanToolStripMenuItem.Text = "Info Perusahaan"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(204, 6)
        '
        'KonfigurasiKasTellerToolStripMenuItem
        '
        Me.KonfigurasiKasTellerToolStripMenuItem.Name = "KonfigurasiKasTellerToolStripMenuItem"
        Me.KonfigurasiKasTellerToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.KonfigurasiKasTellerToolStripMenuItem.Text = "Konfigurasi Kas Teller"
        '
        'SetupHariLiburToolStripMenuItem
        '
        Me.SetupHariLiburToolStripMenuItem.Name = "SetupHariLiburToolStripMenuItem"
        Me.SetupHariLiburToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.SetupHariLiburToolStripMenuItem.Text = "Setup Hari Libur"
        '
        'BukuTabunganNasabahToolStripMenuItem
        '
        Me.BukuTabunganNasabahToolStripMenuItem.Name = "BukuTabunganNasabahToolStripMenuItem"
        Me.BukuTabunganNasabahToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.BukuTabunganNasabahToolStripMenuItem.Text = "Buku Tabungan Nasabah"
        '
        'WindowsMenu
        '
        Me.WindowsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewWindowToolStripMenuItem, Me.CascadeToolStripMenuItem, Me.TileVerticalToolStripMenuItem, Me.TileHorizontalToolStripMenuItem, Me.CloseAllToolStripMenuItem, Me.ArrangeIconsToolStripMenuItem})
        Me.WindowsMenu.Name = "WindowsMenu"
        Me.WindowsMenu.Size = New System.Drawing.Size(68, 20)
        Me.WindowsMenu.Text = "&Windows"
        '
        'NewWindowToolStripMenuItem
        '
        Me.NewWindowToolStripMenuItem.Name = "NewWindowToolStripMenuItem"
        Me.NewWindowToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NewWindowToolStripMenuItem.Text = "&New Window"
        '
        'CascadeToolStripMenuItem
        '
        Me.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem"
        Me.CascadeToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.CascadeToolStripMenuItem.Text = "&Cascade"
        '
        'TileVerticalToolStripMenuItem
        '
        Me.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem"
        Me.TileVerticalToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.TileVerticalToolStripMenuItem.Text = "Tile &Vertical"
        '
        'TileHorizontalToolStripMenuItem
        '
        Me.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem"
        Me.TileHorizontalToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.TileHorizontalToolStripMenuItem.Text = "Tile &Horizontal"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.CloseAllToolStripMenuItem.Text = "C&lose All"
        '
        'ArrangeIconsToolStripMenuItem
        '
        Me.ArrangeIconsToolStripMenuItem.Name = "ArrangeIconsToolStripMenuItem"
        Me.ArrangeIconsToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ArrangeIconsToolStripMenuItem.Text = "&Arrange Icons"
        '
        'EditMenu
        '
        Me.EditMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UndoToolStripMenuItem, Me.RedoToolStripMenuItem, Me.ToolStripSeparator6, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.ToolStripSeparator7, Me.SelectAllToolStripMenuItem})
        Me.EditMenu.Name = "EditMenu"
        Me.EditMenu.Size = New System.Drawing.Size(39, 20)
        Me.EditMenu.Text = "&Edit"
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Image = CType(resources.GetObject("UndoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UndoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.UndoToolStripMenuItem.Text = "&Undo"
        '
        'RedoToolStripMenuItem
        '
        Me.RedoToolStripMenuItem.Image = CType(resources.GetObject("RedoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RedoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.RedoToolStripMenuItem.Name = "RedoToolStripMenuItem"
        Me.RedoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RedoToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.RedoToolStripMenuItem.Text = "&Redo"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(161, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Image = CType(resources.GetObject("CutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CutToolStripMenuItem.Text = "Cu&t"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CopyToolStripMenuItem.Text = "&Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.PasteToolStripMenuItem.Text = "&Paste"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(161, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select &All"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolBarToolStripMenuItem, Me.StatusBarToolStripMenuItem})
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(44, 20)
        Me.ViewMenu.Text = "&View"
        '
        'ToolBarToolStripMenuItem
        '
        Me.ToolBarToolStripMenuItem.Checked = True
        Me.ToolBarToolStripMenuItem.CheckOnClick = True
        Me.ToolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToolBarToolStripMenuItem.Name = "ToolBarToolStripMenuItem"
        Me.ToolBarToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.ToolBarToolStripMenuItem.Text = "&Toolbar"
        '
        'StatusBarToolStripMenuItem
        '
        Me.StatusBarToolStripMenuItem.Checked = True
        Me.StatusBarToolStripMenuItem.CheckOnClick = True
        Me.StatusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StatusBarToolStripMenuItem.Name = "StatusBarToolStripMenuItem"
        Me.StatusBarToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.StatusBarToolStripMenuItem.Text = "&Status Bar"
        '
        'HelpMenu
        '
        Me.HelpMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContentsToolStripMenuItem, Me.ToolStripSeparator8, Me.AboutToolStripMenuItem, Me.ToolStripMenuItem21, Me.PengecekanDataToolStripMenuItem, Me.PengecekanSaldoTabunganToolStripMenuItem})
        Me.HelpMenu.Name = "HelpMenu"
        Me.HelpMenu.Size = New System.Drawing.Size(44, 20)
        Me.HelpMenu.Text = "&Help"
        '
        'ContentsToolStripMenuItem
        '
        Me.ContentsToolStripMenuItem.Name = "ContentsToolStripMenuItem"
        Me.ContentsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1), System.Windows.Forms.Keys)
        Me.ContentsToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ContentsToolStripMenuItem.Text = "&Contents"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(224, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.AboutToolStripMenuItem.Text = "&About ..."
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(224, 6)
        '
        'PengecekanDataToolStripMenuItem
        '
        Me.PengecekanDataToolStripMenuItem.Name = "PengecekanDataToolStripMenuItem"
        Me.PengecekanDataToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.PengecekanDataToolStripMenuItem.Text = "Pengecekan Data"
        '
        'PengecekanSaldoTabunganToolStripMenuItem
        '
        Me.PengecekanSaldoTabunganToolStripMenuItem.Name = "PengecekanSaldoTabunganToolStripMenuItem"
        Me.PengecekanSaldoTabunganToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.PengecekanSaldoTabunganToolStripMenuItem.Text = "Pengecekan Saldo Tabungan"
        '
        'ToolStrip
        '
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripButton, Me.OpenToolStripButton, Me.SaveToolStripButton, Me.ToolStripSeparator1, Me.PrintToolStripButton, Me.PrintPreviewToolStripButton, Me.ToolStripSeparator2, Me.HelpToolStripButton})
        Me.ToolStrip.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(632, 25)
        Me.ToolStrip.TabIndex = 6
        Me.ToolStrip.Text = "ToolStrip"
        '
        'NewToolStripButton
        '
        Me.NewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.NewToolStripButton.Image = CType(resources.GetObject("NewToolStripButton.Image"), System.Drawing.Image)
        Me.NewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.NewToolStripButton.Name = "NewToolStripButton"
        Me.NewToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.NewToolStripButton.Text = "New"
        '
        'OpenToolStripButton
        '
        Me.OpenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.OpenToolStripButton.Image = CType(resources.GetObject("OpenToolStripButton.Image"), System.Drawing.Image)
        Me.OpenToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.OpenToolStripButton.Name = "OpenToolStripButton"
        Me.OpenToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.OpenToolStripButton.Text = "Open"
        '
        'SaveToolStripButton
        '
        Me.SaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.SaveToolStripButton.Image = CType(resources.GetObject("SaveToolStripButton.Image"), System.Drawing.Image)
        Me.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.SaveToolStripButton.Name = "SaveToolStripButton"
        Me.SaveToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.SaveToolStripButton.Text = "Save"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'PrintToolStripButton
        '
        Me.PrintToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PrintToolStripButton.Image = CType(resources.GetObject("PrintToolStripButton.Image"), System.Drawing.Image)
        Me.PrintToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintToolStripButton.Name = "PrintToolStripButton"
        Me.PrintToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.PrintToolStripButton.Text = "Print"
        '
        'PrintPreviewToolStripButton
        '
        Me.PrintPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PrintPreviewToolStripButton.Image = CType(resources.GetObject("PrintPreviewToolStripButton.Image"), System.Drawing.Image)
        Me.PrintPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintPreviewToolStripButton.Name = "PrintPreviewToolStripButton"
        Me.PrintPreviewToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.PrintPreviewToolStripButton.Text = "Print Preview"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'HelpToolStripButton
        '
        Me.HelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.HelpToolStripButton.Image = CType(resources.GetObject("HelpToolStripButton.Image"), System.Drawing.Image)
        Me.HelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Black
        Me.HelpToolStripButton.Name = "HelpToolStripButton"
        Me.HelpToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.HelpToolStripButton.Text = "Help"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 217)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(632, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'imlButton
        '
        Me.imlButton.ImageStream = CType(resources.GetObject("imlButton.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlButton.TransparentColor = System.Drawing.Color.Transparent
        Me.imlButton.Images.SetKeyName(0, "padlock_closed_icon&48.png")
        Me.imlButton.Images.SetKeyName(1, "doc_plus_icon&48.png")
        Me.imlButton.Images.SetKeyName(2, "doc_edit_icon&48.png")
        Me.imlButton.Images.SetKeyName(3, "doc_delete_icon&48.png")
        Me.imlButton.Images.SetKeyName(4, "save_icon&48.png")
        Me.imlButton.Images.SetKeyName(5, "delete_icon&48.png")
        Me.imlButton.Images.SetKeyName(6, "round_checkmark_icon&48.png")
        Me.imlButton.Images.SetKeyName(7, "zoom_icon&48.png")
        Me.imlButton.Images.SetKeyName(8, "checkbox_unchecked_icon&48.png")
        Me.imlButton.Images.SetKeyName(9, "checkbox_checked_icon&48.png")
        Me.imlButton.Images.SetKeyName(10, "app_window_black_icon&48.png")
        Me.imlButton.Images.SetKeyName(11, "zoom_icon&48.png")
        Me.imlButton.Images.SetKeyName(12, "print_icon&48.png")
        Me.imlButton.Images.SetKeyName(13, "refresh_icon&48.png")
        '
        'imlToolbar
        '
        Me.imlToolbar.ImageStream = CType(resources.GetObject("imlToolbar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlToolbar.TransparentColor = System.Drawing.Color.Transparent
        Me.imlToolbar.Images.SetKeyName(0, "off_icon&48.png")
        Me.imlToolbar.Images.SetKeyName(1, "reload_icon&48.png")
        Me.imlToolbar.Images.SetKeyName(2, "calc_icon&48.png")
        Me.imlToolbar.Images.SetKeyName(3, "print_icon&48.png")
        Me.imlToolbar.Images.SetKeyName(4, "zoom_icon&48.png")
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.Appearance.BackColor2 = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.Appearance.ForeColor = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.Appearance.Options.UseBackColor = True
        Me.XtraTabbedMdiManager1.Appearance.Options.UseForeColor = True
        Me.XtraTabbedMdiManager1.AppearancePage.PageClient.BackColor = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.AppearancePage.PageClient.BackColor2 = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.AppearancePage.PageClient.ForeColor = System.Drawing.SystemColors.Control
        Me.XtraTabbedMdiManager1.AppearancePage.PageClient.Options.UseBackColor = True
        Me.XtraTabbedMdiManager1.AppearancePage.PageClient.Options.UseForeColor = True
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'aMainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(632, 239)
        Me.Controls.Add(Me.ToolStrip)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "aMainMenu"
        Me.Text = "BPR"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ToolStrip.ResumeLayout(False)
        Me.ToolStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArrangeIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewWindowToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CascadeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileVerticalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileHorizontalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintPreviewToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents PrintToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents NewToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents OpenToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents SaveToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents EditMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCabang As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents imlButton As System.Windows.Forms.ImageList
    Friend WithEvents imlToolbar As System.Windows.Forms.ImageList
    Friend WithEvents TransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegisterNasabahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabunganBaruToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CariNasabahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MutasiTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabunganToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MutasiTabunganToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents AkuntansiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RekeningToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AntarBankAktivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AntarBankPasivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganAktivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MataUangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgamaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PekerjaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InstansiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DaerahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WilayahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TabunganToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganNasabahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LogOffToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PerubahanSukuBungaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KodeTransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KonfigurasiTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganDeposanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SifatKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganDebiturToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SektorEkonomiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GolonganPenjaminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SetupJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuratPerjanjianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AccountOfficerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BendaharaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AdministrasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProvisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BlokirRekeningToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents HapusMutasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TutupRekeningToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BatalTutupRekeningToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CetakSlipValidasiBukuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValidasiMutasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HeaderBukuTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakBukuTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenyetoranDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MutasiDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerubahanSukuBungaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerpanjanganDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BlokirDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents HapusMutasiDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembatalanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CetakSlipKartuBilyetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BilyetDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TandaTerimaBungaDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BuktiPencairanDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValidasiDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KoreksiDataDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KoreksiTanggalPerpanjanganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KreditToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SimulasiKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PengajuanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RealisasiKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSuratPerjanijanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengikatanJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengambilanJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RegisterJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegisterJaminanBrankasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakIdentitasJaminanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PencairanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenarikanPlafondKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AngsuranKreditInstansiTitipanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AngsuranKreditInstansiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AngsuranKreditKasirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PostingBungaRKToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PindahRekeningKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AutoDebetAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsuransiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BumiPuteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JamkridaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PenolakanPengajuanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusAngsuranKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusAngsuranKreditInstansiTitipanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusAngsuranKreditInstansiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusMutasiAsuransiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BatalPencairanKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KoreksiDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KoreksiDataKreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KoreksiStatusPengajuanKreditStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateJadwalKreditStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSlipKartuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakHeaderKartuAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSlipPenerimaanPinjamanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSlipAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakValidasiAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSlipAngsuranInstansiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakTandaTerimaPengambilanJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PenerimaanKasBesarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengeluaranKasBesarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PenerimaanKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengeluaranKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakBuktiKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AktivaTetapToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JurnalLainLainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakValidasiJurnalLainLainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CetakBuktiPenerimaanKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakBuktiPengeluaranKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakValidasiKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembelianAktivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenjualanAktivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BreakAktivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CariJaminanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CariJurnalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRptRegisterNasabah As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnKonfigurasiSystem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PostingBungaTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PostingBungaAntarKantorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PostingAkhirBulanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PostingAkhirTahunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PembatalanPostingAkhirBulanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BatalPostingBungaTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KalkulatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuLevelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GantiMenuLevelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GantiPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlafondUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrinterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InfoPerusahaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KonfigurasiKasTellerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetupHariLiburToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BukuTabunganNasabahToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PengecekanDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengecekanSaldoTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
