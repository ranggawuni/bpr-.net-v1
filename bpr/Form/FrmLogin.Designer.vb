﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Global.System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726")> _
Partial Class FrmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents UsernameLabel As System.Windows.Forms.Label
    Friend WithEvents PasswordLabel As System.Windows.Forms.Label
    Friend WithEvents cUser As System.Windows.Forms.TextBox
    Friend WithEvents cPass As System.Windows.Forms.TextBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLogin))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.UsernameLabel = New System.Windows.Forms.Label()
        Me.PasswordLabel = New System.Windows.Forms.Label()
        Me.cUser = New System.Windows.Forms.TextBox()
        Me.cPass = New System.Windows.Forms.TextBox()
        Me.nLevel = New DevExpress.XtraEditors.TextEdit()
        Me.cCon = New DevExpress.XtraEditors.TextEdit()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.hMenu = New DevExpress.XtraEditors.TextEdit()
        Me.pb = New System.Windows.Forms.ProgressBar()
        Me.cmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLevel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hMenu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(165, 193)
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'UsernameLabel
        '
        Me.UsernameLabel.Location = New System.Drawing.Point(172, 24)
        Me.UsernameLabel.Name = "UsernameLabel"
        Me.UsernameLabel.Size = New System.Drawing.Size(220, 23)
        Me.UsernameLabel.TabIndex = 0
        Me.UsernameLabel.Text = "&User name"
        Me.UsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PasswordLabel
        '
        Me.PasswordLabel.Location = New System.Drawing.Point(172, 81)
        Me.PasswordLabel.Name = "PasswordLabel"
        Me.PasswordLabel.Size = New System.Drawing.Size(220, 23)
        Me.PasswordLabel.TabIndex = 2
        Me.PasswordLabel.Text = "&Password"
        Me.PasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cUser
        '
        Me.cUser.Location = New System.Drawing.Point(174, 44)
        Me.cUser.Name = "cUser"
        Me.cUser.Size = New System.Drawing.Size(220, 20)
        Me.cUser.TabIndex = 1
        '
        'cPass
        '
        Me.cPass.Location = New System.Drawing.Point(174, 101)
        Me.cPass.Name = "cPass"
        Me.cPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.cPass.Size = New System.Drawing.Size(220, 20)
        Me.cPass.TabIndex = 3
        '
        'nLevel
        '
        Me.nLevel.Location = New System.Drawing.Point(77, 219)
        Me.nLevel.Name = "nLevel"
        Me.nLevel.Size = New System.Drawing.Size(26, 20)
        Me.nLevel.TabIndex = 6
        Me.nLevel.Visible = False
        '
        'cCon
        '
        Me.cCon.Location = New System.Drawing.Point(109, 219)
        Me.cCon.Name = "cCon"
        Me.cCon.Size = New System.Drawing.Size(26, 20)
        Me.cCon.TabIndex = 7
        Me.cCon.Visible = False
        '
        'cKode
        '
        Me.cKode.Location = New System.Drawing.Point(197, 219)
        Me.cKode.Name = "cKode"
        Me.cKode.Size = New System.Drawing.Size(26, 20)
        Me.cKode.TabIndex = 8
        Me.cKode.Visible = False
        '
        'hMenu
        '
        Me.hMenu.Location = New System.Drawing.Point(154, 219)
        Me.hMenu.Name = "hMenu"
        Me.hMenu.Size = New System.Drawing.Size(26, 20)
        Me.hMenu.TabIndex = 9
        Me.hMenu.Visible = False
        '
        'pb
        '
        Me.pb.Location = New System.Drawing.Point(174, 133)
        Me.pb.Name = "pb"
        Me.pb.Size = New System.Drawing.Size(220, 18)
        Me.pb.TabIndex = 10
        Me.pb.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(283, 161)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdCancel.SuperTip = SuperToolTip1
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdOK.Location = New System.Drawing.Point(197, 161)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Batal / Keluar"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdOK.SuperTip = SuperToolTip2
        Me.cmdOK.TabIndex = 12
        Me.cmdOK.Text = "&OK"
        '
        'FrmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(401, 193)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.pb)
        Me.Controls.Add(Me.hMenu)
        Me.Controls.Add(Me.cKode)
        Me.Controls.Add(Me.cCon)
        Me.Controls.Add(Me.nLevel)
        Me.Controls.Add(Me.cPass)
        Me.Controls.Add(Me.cUser)
        Me.Controls.Add(Me.PasswordLabel)
        Me.Controls.Add(Me.UsernameLabel)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmLogin"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Login"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLevel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.hMenu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents nLevel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cCon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents hMenu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pb As System.Windows.Forms.ProgressBar
    Friend WithEvents cmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdOK As DevExpress.XtraEditors.SimpleButton

End Class
