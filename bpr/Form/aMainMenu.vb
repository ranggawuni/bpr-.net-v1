﻿Imports bpr.MySQL_Data_Library

Public Class aMainMenu
    Dim objData As New data
    Dim lFirst As Boolean
    Dim cMySQLVersion As String
    Dim lLogOff As Boolean
    Dim cKode As String
    Public cCabangEntry As String
    Public nUserPlafond As Double

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CutToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CopyToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PasteToolStripMenuItem.Click
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click
        ToolStrip.Visible = ToolBarToolStripMenuItem.Checked
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        StatusStrip.Visible = StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private ReadOnly m_ChildFormNumber As Integer

    Private Sub mnuCabang_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuCabang.Click
        mstCabang.MdiParent = Me
        mstCabang.Show()
    End Sub

    Private Sub RegisterNasabahToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles RegisterNasabahToolStripMenuItem.Click
        trRegisterNasabah.MdiParent = Me
        trRegisterNasabah.Show()
    End Sub

    Private Sub TabunganBaruToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TabunganBaruToolStripMenuItem.Click
        trOpenTabungan.MdiParent = Me
        trOpenTabungan.Show()
    End Sub

    Private Sub CariNasabahToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CariNasabahToolStripMenuItem.Click
        trCariRegister.MdiParent = Me
        trCariRegister.Show()
    End Sub

    Private Sub MutasiTabunganToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MutasiTabunganToolStripMenuItem.Click
        trMutasiTabungan.MdiParent = Me
        trMutasiTabungan.Show()
    End Sub

    Private Sub aMainMenu_Activated(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Activated
        If lFirst Then
            Text = String.Format("{0} - {1} - {2}", aCfg(eCfg.msNama), aCfg(eCfg.msKodeCabang), cNamaKantorPublic)
            lFirst = False

            SaveRegistry(eRegistry.reg_LocalIP, cIPNumber)
            Dim dbData As DataTable = objData.SQL(GetDSN, "select version() as version")
            If dbData.Rows.Count > 0 Then
                cMySQLVersion = dbData.Rows(0).Item("version").ToString
            End If

            'ambil data kas teller
            dbData = objData.Browse(GetDSN, "Username", , "Username", , cUserName)
            With dbData
                If .Rows.Count > 0 Then
                    cKasTeller = .Rows(0).Item("KasTeller").ToString
                    nUserLevel = CInt(.Rows(0).Item("Level").ToString)
                    cUserID = GetNull(.Rows(0).Item("ID").ToString, "").ToString
                End If
            End With
            If Debugger.IsAttached And cUserName = "Heasoft" And cKasTeller = "" Then
                cKasTeller = aCfg(eCfg.msKodeKas).ToString
            End If
            nUserPlafond = 0
            dbData = objData.Browse(GetDSN, "SetupPlafond", , "user", , cUserName)
            If dbData.Rows.Count > 0 Then
                nUserPlafond = CDbl(dbData.Rows(0).Item("Maximum").ToString)
            End If
        End If
    End Sub

    Private Sub aMainMenu_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        objData.Edit(GetDSN, "LogMenu", String.Format("IP = '{0}' and HDC <> 0", cIPNumber), _
                     {"HDC", "LogOut"}, _
                     {0, SNow()})
        If Not lLogOff Then
            End
        End If
        lLogOff = False
    End Sub

    Private Sub aMainMenu_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        If Not lLogOff Then
            End
        End If
        lLogOff = False
    End Sub

    Private Sub aMainMenu_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        LoadSkin()
        GetIPNumber(cIPNumber, cDatabase)
        Dim cDataTable As DataTable = objData.SQL(GetDSN, "select * from mysql.cabang limit 1")
        If cDataTable.Rows.Count > 0 Then
            Dim cKodeTemp As String = cDataTable.Rows(0).Item("kode").ToString
            If cKodeTemp <> "" Then
                UpdCfg(eCfg.msKodeCabang, cKodeTemp)
                cDataTable = objData.Browse(GetDSN, "cabang", "keterangan,alamat,jeniskantor", "kode", , cKodeTemp)
                With cDataTable
                    If .Rows.Count > 0 Then
                        cNamaKantorPublic = .Rows(0).Item("Keterangan").ToString
                        cAlamatKantorPublic = .Rows(0).Item("Alamat").ToString
                        cJenisKantor = .Rows(0).Item("JenisKantor").ToString
                    End If
                End With
            Else
                MessageBox.Show("Kode Kantor belum di Register, Mohon hubungi Administrator!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End
            End If
        Else
            MessageBox.Show("Kode Kantor belum di Register, Mohon hubungi Administrator!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End
        End If
        InitCfg()

        lFirst = True
        cKode = "BPR"
        cComputerName = Computer.Name
        CheckVersion()
        cCabangEntry = aCfg(eCfg.msKodeCabang).ToString
        If cCabangEntry = "" Then
            cCabangEntry = "00"
        End If

        cKodeKantorInduk = ""
        Dim dbdata As DataTable = objData.Browse(GetDSN, "cabang", "induk", "kode", , aCfg(eCfg.msKodeCabang).ToString)
        If dbdata.Rows.Count > 0 Then
            cKodeKantorInduk = GetNull(dbdata.Rows(0).Item("Induk").ToString, "").ToString
        End If


        If Debugger.IsAttached And Trim(cUserName) = "" Then cUserName = "Heasoft"

        If Debugger.IsAttached Then
            Dim result = MessageBox.Show("Password?", "Konfirmasi", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            If result = Windows.Forms.DialogResult.Yes Then
                GetLogin()
            ElseIf result = DialogResult.Cancel Then
                End
            ElseIf result = Windows.Forms.DialogResult.No Then
                nUserLevel = 0
                cUserID = "001"
                cUserName = "Heasoft"
                cFullName = "Heasoft"
                GoTo Langsung
            End If
        End If
langsung:

    End Sub

    Private Sub LoadSkin()
        DevExpress.UserSkins.BonusSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.UseDefaultLookAndFeel = True

        Dim cAppExeName As String = GetAppExeName()
        Dim vaRetval As Object = GetSetting("dashboard", cAppExeName, "SkinName", "Blue")
        DefaultLookAndFeel1.LookAndFeel.SkinName = CStr(vaRetval)
    End Sub

    Private Sub GetLogin()
        FrmLogin.cCon.Text = GetDSN()
        FrmLogin.cKode.Text = cKode
        FrmLogin.ShowDialog()
    End Sub

    Private Sub GetIPNumber(ByRef cIPNumber As String, ByRef cDataBase As String)
        Dim cFile As String = GetAppPath() & "\config.ini"
        Dim cData As String = ""
        If IO.File.Exists(cFile) Then
            Dim objReader As New IO.StreamReader(cFile)
            Do While objReader.Peek() <> -1
                cData = Replace(Replace(objReader.ReadLine(), " ", ""), Chr(9), "")
                cIPNumber = GetData(cData, "IP=", cIPNumber)
                cDataBase = GetData(cData, "DATABASE=", cDataBase)
                cPort = GetData(cData, "PORT=", cPort)
            Loop
        End If
        If Trim(cIPNumber) = "" Then
            cIPNumber = "LocalHost"
        End If
        If Trim(cDataBase) = "" Then
            cDataBase = "bpr_jombang_test"
        End If
        If Trim(cPort) = "" Then
            cPort = "3306"
        End If
        SaveRegistry(eRegistry.reg_IP, cIPNumber)
        SaveRegistry(eRegistry.reg_Database, cDataBase)
    End Sub

    Private Shared Function GetData(ByVal cData As String, ByVal cKey As String, ByVal cDefault As String) As String
        Dim n As Double
        cData = Trim(LCase(cData))
        cKey = LCase(cKey)
        GetData = cDefault
        If cData.ToString.Substring(0, 1) <> "#" Then
            n = InStr(1, cData, cKey)
            If n <> 0 Then
                cData = Replace(cData, cKey, "")
                GetData = cData
            End If
        End If
    End Function

    Private Sub MutasiTabunganToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MutasiTabunganToolStripMenuItem1.Click
        RptGrid.MdiParent = Me
        RptGrid.Show()
    End Sub

    Private Sub RekeningToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles RekeningToolStripMenuItem.Click
        MstRekening.MdiParent = Me
        MstRekening.Show()
    End Sub

    Private Sub AntarBankAktivaToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AntarBankAktivaToolStripMenuItem.Click
        MstAntarBankAktiva.MdiParent = Me
        MstAntarBankAktiva.GetStatus("1")
        MstAntarBankAktiva.Show()
    End Sub

    Private Sub AntarBankPasivaToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AntarBankPasivaToolStripMenuItem.Click
        MstAntarBankAktiva.MdiParent = Me
        MstAntarBankAktiva.GetStatus("2")
        MstAntarBankAktiva.Show()
    End Sub

    Private Sub GolonganAktivaToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles GolonganAktivaToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("GolonganAktiva", "Golongan Aktiva", "Golongan Aktiva")
        FrmMaster.Show()
    End Sub

    Private Sub PekerjaanToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PekerjaanToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("Pekerjaan", "Pekerjaan", "Pekerjaan")
        FrmMaster.Show()
    End Sub

    Private Sub AgamaToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AgamaToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("Agama", "Agama", "Agama")
        FrmMaster.Show()
    End Sub

    Private Sub InstansiToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles InstansiToolStripMenuItem.Click
        mstInstansi.MdiParent = Me
        mstInstansi.Show()
    End Sub

    Private Sub GolonganTabunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganTabunganToolStripMenuItem.Click
        MstGolonganTabungan.MdiParent = Me
        MstGolonganTabungan.Show()
    End Sub

    Private Sub GolonganNasabahToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganNasabahToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("GolonganNasabah", "GolonganNasabah", "GolonganNasabah")
        FrmMaster.Show()
    End Sub

    Private Sub PerubahanSukuBungaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PerubahanSukuBungaToolStripMenuItem.Click
        MstSukuBunga.MdiParent = Me
        MstSukuBunga.Show()
    End Sub

    Private Sub KodeTransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KodeTransaksiToolStripMenuItem.Click
        MstKodeTransaksi.MdiParent = Me
        MstKodeTransaksi.Show()
    End Sub

    Private Sub KonfigurasiTabunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KonfigurasiTabunganToolStripMenuItem.Click
        cfgTabungan.MdiParent = Me
        cfgTabungan.Show()
    End Sub

    Private Sub GolonganDeposanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganDeposanToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("GolonganDeposan", "GolonganDeposan", "Golongan Deposan")
        FrmMaster.Show()
    End Sub

    Private Sub GolonganDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganDepositoToolStripMenuItem.Click
        MstGolonganDeposito.MdiParent = Me
        MstGolonganDeposito.Show()
    End Sub

    Private Sub GolonganKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganKreditToolStripMenuItem.Click
        MstGolonganKredit.MdiParent = Me
        MstGolonganKredit.Show()
    End Sub

    Private Sub SifatKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SifatKreditToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("SifatKredit", "SifatKredit", "Sifat Kredit")
        FrmMaster.Show()
    End Sub

    Private Sub JToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("JenisPenggunaan", "JenisPenggunaan", "Jenis Penggunaan")
        FrmMaster.Show()
    End Sub

    Private Sub GolonganDebiturToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganDebiturToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("GolonganDebitur", "GolonganDebitur", "Golongan Debitur")
        FrmMaster.Show()
    End Sub

    Private Sub SektorEkonomiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SektorEkonomiToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("SektorEkonomi", "SektorEkonomi", "Sektor Ekonomi")
        FrmMaster.Show()
    End Sub

    Private Sub GolonganPenjaminToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GolonganPenjaminToolStripMenuItem.Click
        FrmMaster.MdiParent = Me
        FrmMaster.GetForm("GolonganPenjamin", "GolonganPenjamin", "Golongan Penjamin")
        FrmMaster.Show()
    End Sub

    Private Sub SetupJaminanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetupJaminanToolStripMenuItem.Click
        MstJaminan.MdiParent = Me
        MstJaminan.Show()
    End Sub

    Private Sub SuratPerjanjianToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SuratPerjanjianToolStripMenuItem.Click
        MstSuratPerjanjian.MdiParent = Me
        MstSuratPerjanjian.Show()
    End Sub

    Private Sub AccountOfficerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AccountOfficerToolStripMenuItem.Click
        MstAO.MdiParent = Me
        MstAO.Show()
    End Sub

    Private Sub BendaharaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BendaharaToolStripMenuItem.Click
        MstBendahara.MdiParent = Me
        MstBendahara.Show()
    End Sub

    Private Sub AdministrasiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdministrasiToolStripMenuItem.Click
        MstAdmKredit.MdiParent = Me
        MstAdmKredit.Show()
    End Sub

    Private Sub ProvisiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProvisiToolStripMenuItem.Click
        MstProvisiKredit.MdiParent = Me
        MstProvisiKredit.Show()
    End Sub

    Private Sub DaerahToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DaerahToolStripMenuItem.Click
        MstDaerah.MdiParent = Me
        MstDaerah.Show()
    End Sub

    Private Sub WilayahToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WilayahToolStripMenuItem.Click
        MstWilayah.MdiParent = Me
        MstWilayah.Show()
    End Sub

    Private Sub MataUangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MataUangToolStripMenuItem.Click
        MstMataUang.MdiParent = Me
        MstMataUang.Show()
    End Sub

    Private Sub BlokirRekeningToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BlokirRekeningToolStripMenuItem.Click
        trBlokirTabungan.MdiParent = Me
        trBlokirTabungan.Show()
    End Sub

    Private Sub HapusMutasiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HapusMutasiToolStripMenuItem.Click
        trHapusMutasiTabungan.MdiParent = Me
        trHapusMutasiTabungan.Show()
    End Sub

    Private Sub TutupRekeningToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TutupRekeningToolStripMenuItem.Click
        trTutupTabungan.MdiParent = Me
        trTutupTabungan.Show()
    End Sub

    Private Sub BatalTutupRekeningToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatalTutupRekeningToolStripMenuItem.Click
        trBatalTutupTabungan.MdiParent = Me
        trBatalTutupTabungan.Show()
    End Sub

    Private Sub ValidasiMutasiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ValidasiMutasiToolStripMenuItem.Click
        trCetakValidasiTabungan.MdiParent = Me
        trCetakValidasiTabungan.Show()
    End Sub

    Private Sub ReToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReToolStripMenuItem.Click
        trRegistrasiDeposito.MdiParent = Me
        trRegistrasiDeposito.Show()
    End Sub

    Private Sub PenyetoranDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenyetoranDepositoToolStripMenuItem.Click
        trSetorDeposito.MdiParent = Me
        trSetorDeposito.Show()
    End Sub

    Private Sub PerubahanSukuBungaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PerubahanSukuBungaToolStripMenuItem1.Click
        trGantiSukuBungaDeposito.MdiParent = Me
        trGantiSukuBungaDeposito.Show()
    End Sub

    Private Sub PerpanjanganDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PerpanjanganDepositoToolStripMenuItem.Click
        trPerpanjanganDeposito.MdiParent = Me
        trPerpanjanganDeposito.Show()
    End Sub

    Private Sub BlokirDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BlokirDepositoToolStripMenuItem.Click
        trBlokirDeposito.MdiParent = Me
        trBlokirDeposito.Show()
    End Sub

    Private Sub PembatalanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PembatalanToolStripMenuItem.Click
        trPembatalanPencairanDeposito.MdiParent = Me
        trPembatalanPencairanDeposito.Show()
    End Sub

    Private Sub KoreksiDataDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KoreksiDataDepositoToolStripMenuItem.Click
        trKoreksiDeposan.MdiParent = Me
        trKoreksiDeposan.Show()
    End Sub

    Private Sub KoreksiTanggalPerpanjanganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KoreksiTanggalPerpanjanganToolStripMenuItem.Click
        trKoreksiPerpanjanganDeposito.MdiParent = Me
        trKoreksiPerpanjanganDeposito.Show()
    End Sub

    Private Sub SimulasiKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SimulasiKreditToolStripMenuItem.Click
        trSimulasiKredit.MdiParent = Me
        trSimulasiKredit.Show()
    End Sub

    Private Sub PengajuanKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PengajuanKreditToolStripMenuItem.Click
        trPengajuanKredit.MdiParent = Me
        trPengajuanKredit.Show()
    End Sub

    Private Sub RealisasiKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RealisasiKreditToolStripMenuItem.Click
        trRealisasiKredit.MdiParent = Me
        trRealisasiKredit.Show()
    End Sub

    Private Sub CetakSuratPerjanijanKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CetakSuratPerjanijanKreditToolStripMenuItem.Click
        trCetakSuratPerjanjian.MdiParent = Me
        trCetakSuratPerjanjian.Show()
    End Sub

    Private Sub PencairanKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PencairanKreditToolStripMenuItem.Click
        trPencairanKredit.MdiParent = Me
        trPencairanKredit.Show()
    End Sub

    Private Sub PenarikanPlafondKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenarikanPlafondKreditToolStripMenuItem.Click
        trPenarikanPlafondKredit.MdiParent = Me
        trPenarikanPlafondKredit.Show()
    End Sub

    Private Sub AngsuranKreditInstansiTitipanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AngsuranKreditInstansiTitipanToolStripMenuItem.Click
        trAngsuranInstansiTitipan.MdiParent = Me
        trAngsuranInstansiTitipan.Show()
    End Sub

    Private Sub AngsuranKreditInstansiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AngsuranKreditInstansiToolStripMenuItem.Click
        trAngsuranInstansi.MdiParent = Me
        trAngsuranInstansi.Show()
    End Sub

    Private Sub AngsuranKreditKasirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AngsuranKreditKasirToolStripMenuItem.Click
        trAngsuranKredit.MdiParent = Me
        trAngsuranKredit.Show()
    End Sub

    Private Sub PostingBungaRKToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PostingBungaRKToolStripMenuItem.Click
        trPostingBungaRK.MdiParent = Me
        trPostingBungaRK.Show()
    End Sub

    Private Sub AutoDebetAngsuranToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AutoDebetAngsuranToolStripMenuItem.Click
        trAutoDebetAngsuran.MdiParent = Me
        trAutoDebetAngsuran.Show()
    End Sub

    Private Sub PenolakanPengajuanKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenolakanPengajuanKreditToolStripMenuItem.Click
        trPenolakanPengajuanKredit.MdiParent = Me
        trPenolakanPengajuanKredit.Show()
    End Sub

    Private Sub HapusAngsuranKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HapusAngsuranKreditToolStripMenuItem.Click
        trHapusAngsuranKredit.MdiParent = Me
        trHapusAngsuranKredit.Show()
    End Sub

    Private Sub HapusAngsuranKreditInstansiTitipanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HapusAngsuranKreditInstansiTitipanToolStripMenuItem.Click
        trHapusAngsuranKreditInstansiTitipan.MdiParent = Me
        trHapusAngsuranKreditInstansi.Show()
    End Sub

    Private Sub HapusAngsuranKreditInstansiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HapusAngsuranKreditInstansiToolStripMenuItem.Click
        trHapusAngsuranKreditInstansi.MdiParent = Me
        trHapusAngsuranKreditInstansi.Show()
    End Sub

    Private Sub BatalPencairanKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatalPencairanKreditToolStripMenuItem.Click
        trPembatalanPencairanKredit.MdiParent = Me
        trPembatalanPencairanKredit.Show()
    End Sub

    Private Sub KoreksiDataKreditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KoreksiDataKreditToolStripMenuItem.Click
        trKoreksiDebitur.MdiParent = Me
        trKoreksiDebitur.Show()
    End Sub

    Private Sub KoreksiStatusPengajuanKreditStripMenuItem_Click(sender As Object, e As EventArgs) Handles KoreksiStatusPengajuanKreditStripMenuItem.Click
        trKoreksiStatusPengajuan.MdiParent = Me
        trKoreksiStatusPengajuan.Show()
    End Sub

    Private Sub UpdateJadwalKreditStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpdateJadwalKreditStripMenuItem.Click
        trUpdateJadwalKredit.MdiParent = Me
        trUpdateJadwalKredit.Show()
    End Sub

    Private Sub PengeluaranKasBesarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PengeluaranKasBesarToolStripMenuItem.Click
        trPengeluaranKasBesar.MdiParent = Me
        trPengeluaranKasBesar.Show()
    End Sub

    Private Sub PenerimaanKasBesarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenerimaanKasBesarToolStripMenuItem.Click
        trPenerimaanKasBesar.MdiParent = Me
        trPenerimaanKasBesar.Show()
    End Sub

    Private Sub UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpdateJadwalAmortisasiAdministrasiProvisiStripMenuItem.Click
        trEditJadwalProvisiAdm.MdiParent = Me
        trEditJadwalProvisiAdm.Show()
    End Sub

    Private Sub PenerimaanKasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenerimaanKasToolStripMenuItem.Click
        trPenerimaanKasTeller.MdiParent = Me
        trPenerimaanKasTeller.Show()
    End Sub

    Private Sub PengeluaranKasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PengeluaranKasToolStripMenuItem.Click
        trPengeluaranKasTeller.MdiParent = Me
        trPengeluaranKasTeller.Show()
    End Sub

    Private Sub PembelianAktivaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PembelianAktivaToolStripMenuItem.Click
        trPembelianAktiva.MdiParent = Me
        trPembelianAktiva.Show()
    End Sub

    Private Sub PenjualanAktivaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenjualanAktivaToolStripMenuItem.Click
        trPenjualanAktiva.MdiParent = Me
        trPenjualanAktiva.Show()
    End Sub

    Private Sub BreakAktivaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BreakAktivaToolStripMenuItem.Click
        trBreakAktiva.MdiParent = Me
        trBreakAktiva.Show()
    End Sub

    Private Sub JurnalLainLainToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JurnalLainLainToolStripMenuItem.Click
        trJurnalLainLain.MdiParent = Me
        trJurnalLainLain.Show()
    End Sub

    Private Sub mnuRptRegisterNasabah_Click(sender As Object, e As EventArgs) Handles mnuRptRegisterNasabah.Click
        RptRekapitulasiRegisterNasabah.MdiParent = Me
        RptRekapitulasiRegisterNasabah.Show()
    End Sub

    Private Sub mnKonfigurasiSystem_Click(sender As Object, e As EventArgs) Handles mnKonfigurasiSystem.Click
        cfgSystem.MdiParent = Me
        cfgSystem.Show()
    End Sub

    Private Sub PostingBungaTabunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PostingBungaTabunganToolStripMenuItem.Click
        FrmPostingBungaTabungan.MdiParent = Me
        FrmPostingBungaTabungan.Show()
    End Sub

    Private Sub PostingBungaAntarKantorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PostingBungaAntarKantorToolStripMenuItem.Click
        frmPostingBungaAntarKantor.MdiParent = Me
        frmPostingBungaAntarKantor.Show()
    End Sub

    Private Sub PostingAkhirBulanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PostingAkhirBulanToolStripMenuItem.Click
        frmAkhirBulan.MdiParent = Me
        frmAkhirBulan.Show()
    End Sub

    Private Sub PostingAkhirTahunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PostingAkhirTahunToolStripMenuItem.Click
        FrmAkhirTahun.MdiParent = Me
        FrmAkhirTahun.Show()
    End Sub

    Private Sub PembatalanPostingAkhirBulanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PembatalanPostingAkhirBulanToolStripMenuItem.Click
        FrmBatalAkhirBulan.MdiParent = Me
        FrmBatalAkhirBulan.Show()
    End Sub

    Private Sub BatalPostingBungaTabunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatalPostingBungaTabunganToolStripMenuItem.Click
        FrmBatalBungaTabungan.MdiParent = Me
        FrmBatalAkhirBulan.Show()
    End Sub

    Private Sub KalkulatorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KalkulatorToolStripMenuItem.Click
        Shell("calc")
    End Sub

    Private Sub UserPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UserPasswordToolStripMenuItem.Click
        frmUserPassword.MdiParent = Me
        frmUserPassword.Show()
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        frmGantiPassword.MdiParent = Me
        frmGantiPassword.Show()
    End Sub

    Private Sub ResetPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResetPasswordToolStripMenuItem.Click
        frmResetPassword.MdiParent = Me
        frmResetPassword.Show()
    End Sub

    Private Sub GantiMenuLevelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiMenuLevelToolStripMenuItem.Click
        frmResetMenuLevel.MdiParent = Me
        frmResetMenuLevel.Show()
    End Sub
End Class
