﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MstGolonganDeposito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtData IsNot Nothing Then
                    dtData.Dispose()
                    dtData = Nothing
                End If
                If dtRekening IsNot Nothing Then
                    dtRekening.Dispose()
                    dtRekening = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MstGolonganDeposito))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.optWajibPajak = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.nPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningBunga = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.cRekeningJatuhTempo = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningJatuhTempo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningPajakBunga = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningPajakBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAntarBankPasiva = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAntarBankPasiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningPinalti = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningPinalti = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAccrual = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAccrual = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningPajakBMHD = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningPajakBMHD = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.optWajibPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningPajakBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningPajakBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAntarBankPasiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAntarBankPasiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningPinalti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningPinalti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningPajakBMHD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningPajakBMHD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(3, 391)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(945, 33)
        Me.PanelControl7.TabIndex = 18
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(849, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(768, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(192, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(116, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(40, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(3, 185)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(945, 200)
        Me.GridControl1.TabIndex = 17
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekeningAccrual)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAccrual)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekeningPajakBMHD)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningPajakBMHD)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cRekeningAntarBankPasiva)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAntarBankPasiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.cRekeningPinalti)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningPinalti)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.cRekeningJatuhTempo)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningJatuhTempo)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.cRekeningPajakBunga)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningPajakBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.optWajibPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.nPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cNamaRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(945, 176)
        Me.PanelControl1.TabIndex = 16
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(647, 117)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl18.TabIndex = 107
        Me.LabelControl18.Text = "%"
        '
        'optWajibPajak
        '
        Me.optWajibPajak.Location = New System.Drawing.Point(587, 140)
        Me.optWajibPajak.Name = "optWajibPajak"
        Me.optWajibPajak.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optWajibPajak.Size = New System.Drawing.Size(120, 31)
        Me.optWajibPajak.TabIndex = 14
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(469, 117)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl1.TabIndex = 99
        Me.LabelControl1.Text = "Pajak Bunga"
        '
        'nPajak
        '
        Me.nPajak.EditValue = "123456"
        Me.nPajak.Location = New System.Drawing.Point(587, 114)
        Me.nPajak.Name = "nPajak"
        Me.nPajak.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nPajak.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nPajak.Properties.MaxLength = 6
        Me.nPajak.Size = New System.Drawing.Size(54, 20)
        Me.nPajak.TabIndex = 98
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(469, 143)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl13.TabIndex = 93
        Me.LabelControl13.Text = "Wajib Pajak"
        '
        'cRekeningBunga
        '
        Me.cRekeningBunga.Location = New System.Drawing.Point(105, 85)
        Me.cRekeningBunga.Name = "cRekeningBunga"
        Me.cRekeningBunga.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningBunga.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningBunga.TabIndex = 21
        '
        'cNamaRekeningBunga
        '
        Me.cNamaRekeningBunga.Enabled = False
        Me.cNamaRekeningBunga.Location = New System.Drawing.Point(231, 85)
        Me.cNamaRekeningBunga.Name = "cNamaRekeningBunga"
        Me.cNamaRekeningBunga.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningBunga.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 88)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Rek. Bunga"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(105, 59)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(120, 20)
        Me.cRekening.TabIndex = 18
        '
        'cNamaRekening
        '
        Me.cNamaRekening.Enabled = False
        Me.cNamaRekening.Location = New System.Drawing.Point(231, 59)
        Me.cNamaRekening.Name = "cNamaRekening"
        Me.cNamaRekening.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekening.TabIndex = 17
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 62)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Rekening Perkiraan"
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(105, 33)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(266, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 36)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(105, 9)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(34, 20)
        Me.cKode.TabIndex = 0
        '
        'cRekeningJatuhTempo
        '
        Me.cRekeningJatuhTempo.Location = New System.Drawing.Point(105, 137)
        Me.cRekeningJatuhTempo.Name = "cRekeningJatuhTempo"
        Me.cRekeningJatuhTempo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningJatuhTempo.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningJatuhTempo.TabIndex = 113
        '
        'cNamaRekeningJatuhTempo
        '
        Me.cNamaRekeningJatuhTempo.Enabled = False
        Me.cNamaRekeningJatuhTempo.Location = New System.Drawing.Point(231, 137)
        Me.cNamaRekeningJatuhTempo.Name = "cNamaRekeningJatuhTempo"
        Me.cNamaRekeningJatuhTempo.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningJatuhTempo.TabIndex = 112
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(5, 140)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl5.TabIndex = 111
        Me.LabelControl5.Text = "Rek. Jatuh Tempo"
        '
        'cRekeningPajakBunga
        '
        Me.cRekeningPajakBunga.Location = New System.Drawing.Point(105, 111)
        Me.cRekeningPajakBunga.Name = "cRekeningPajakBunga"
        Me.cRekeningPajakBunga.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningPajakBunga.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningPajakBunga.TabIndex = 110
        '
        'cNamaRekeningPajakBunga
        '
        Me.cNamaRekeningPajakBunga.Enabled = False
        Me.cNamaRekeningPajakBunga.Location = New System.Drawing.Point(231, 111)
        Me.cNamaRekeningPajakBunga.Name = "cNamaRekeningPajakBunga"
        Me.cNamaRekeningPajakBunga.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningPajakBunga.TabIndex = 109
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(7, 114)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl6.TabIndex = 108
        Me.LabelControl6.Text = "Rek. Pajak Bunga"
        '
        'cRekeningAntarBankPasiva
        '
        Me.cRekeningAntarBankPasiva.Location = New System.Drawing.Point(587, 36)
        Me.cRekeningAntarBankPasiva.Name = "cRekeningAntarBankPasiva"
        Me.cRekeningAntarBankPasiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAntarBankPasiva.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningAntarBankPasiva.TabIndex = 119
        '
        'cNamaRekeningAntarBankPasiva
        '
        Me.cNamaRekeningAntarBankPasiva.Enabled = False
        Me.cNamaRekeningAntarBankPasiva.Location = New System.Drawing.Point(711, 36)
        Me.cNamaRekeningAntarBankPasiva.Name = "cNamaRekeningAntarBankPasiva"
        Me.cNamaRekeningAntarBankPasiva.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningAntarBankPasiva.TabIndex = 118
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(469, 38)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl8.TabIndex = 117
        Me.LabelControl8.Text = "Rek. Antar Bank Pasiva"
        '
        'cRekeningPinalti
        '
        Me.cRekeningPinalti.Location = New System.Drawing.Point(587, 9)
        Me.cRekeningPinalti.Name = "cRekeningPinalti"
        Me.cRekeningPinalti.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningPinalti.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningPinalti.TabIndex = 116
        '
        'cNamaRekeningPinalti
        '
        Me.cNamaRekeningPinalti.Enabled = False
        Me.cNamaRekeningPinalti.Location = New System.Drawing.Point(711, 9)
        Me.cNamaRekeningPinalti.Name = "cNamaRekeningPinalti"
        Me.cNamaRekeningPinalti.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningPinalti.TabIndex = 115
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(469, 12)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl9.TabIndex = 114
        Me.LabelControl9.Text = "Rek. Pinalti"
        '
        'cRekeningAccrual
        '
        Me.cRekeningAccrual.Location = New System.Drawing.Point(587, 88)
        Me.cRekeningAccrual.Name = "cRekeningAccrual"
        Me.cRekeningAccrual.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAccrual.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningAccrual.TabIndex = 125
        '
        'cNamaRekeningAccrual
        '
        Me.cNamaRekeningAccrual.Enabled = False
        Me.cNamaRekeningAccrual.Location = New System.Drawing.Point(711, 88)
        Me.cNamaRekeningAccrual.Name = "cNamaRekeningAccrual"
        Me.cNamaRekeningAccrual.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningAccrual.TabIndex = 124
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(469, 91)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl10.TabIndex = 123
        Me.LabelControl10.Text = "Rekening Accrual"
        '
        'cRekeningPajakBMHD
        '
        Me.cRekeningPajakBMHD.Location = New System.Drawing.Point(587, 62)
        Me.cRekeningPajakBMHD.Name = "cRekeningPajakBMHD"
        Me.cRekeningPajakBMHD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningPajakBMHD.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningPajakBMHD.TabIndex = 122
        '
        'cNamaRekeningPajakBMHD
        '
        Me.cNamaRekeningPajakBMHD.Enabled = False
        Me.cNamaRekeningPajakBMHD.Location = New System.Drawing.Point(711, 62)
        Me.cNamaRekeningPajakBMHD.Name = "cNamaRekeningPajakBMHD"
        Me.cNamaRekeningPajakBMHD.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningPajakBMHD.TabIndex = 121
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(469, 65)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl11.TabIndex = 120
        Me.LabelControl11.Text = "Rek. Pajak BMHD"
        '
        'MstGolonganDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 426)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MstGolonganDeposito"
        Me.Text = "Golongan Deposito"
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.optWajibPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningPajakBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningPajakBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAntarBankPasiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAntarBankPasiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningPinalti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningPinalti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningPajakBMHD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningPajakBMHD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekeningAccrual As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAccrual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningPajakBMHD As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningPajakBMHD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAntarBankPasiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAntarBankPasiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningPinalti As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningPinalti As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningJatuhTempo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningJatuhTempo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningPajakBunga As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningPajakBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optWajibPajak As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningBunga As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
End Class
