﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MstAntarBankAktiva
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MstAntarBankAktiva))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cTahun = New DevExpress.XtraEditors.TextEdit()
        Me.cBulan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cSukuBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cKualitas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cTglAkhir = New DevExpress.XtraEditors.TextEdit()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTglMulai = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cHari = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterkaitan = New DevExpress.XtraEditors.TextEdit()
        Me.cLokasi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaLokasi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cJenis = New DevExpress.XtraEditors.TextEdit()
        Me.cJenisBank = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaJenisBank = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBank = New DevExpress.XtraEditors.TextEdit()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cTahun.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBulan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSukuBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKualitas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTglAkhir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglMulai.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglMulai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cHari.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cLokasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaLokasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cJenisBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaJenisBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSandiBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(2, 198)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(899, 200)
        Me.GridControl1.TabIndex = 13
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cTahun)
        Me.PanelControl1.Controls.Add(Me.cBulan)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cSukuBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cKualitas)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cTglAkhir)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTglMulai)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.cHari)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.cKeterkaitan)
        Me.PanelControl1.Controls.Add(Me.cLokasi)
        Me.PanelControl1.Controls.Add(Me.cNamaLokasi)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.cJenis)
        Me.PanelControl1.Controls.Add(Me.cJenisBank)
        Me.PanelControl1.Controls.Add(Me.cNamaJenisBank)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cNamaRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cSandiBank)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(899, 189)
        Me.PanelControl1.TabIndex = 12
        '
        'cTahun
        '
        Me.cTahun.EditValue = "123456"
        Me.cTahun.Location = New System.Drawing.Point(659, 31)
        Me.cTahun.Name = "cTahun"
        Me.cTahun.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cTahun.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cTahun.Properties.MaxLength = 6
        Me.cTahun.Size = New System.Drawing.Size(45, 20)
        Me.cTahun.TabIndex = 95
        '
        'cBulan
        '
        Me.cBulan.EditValue = "123456"
        Me.cBulan.Location = New System.Drawing.Point(629, 31)
        Me.cBulan.Name = "cBulan"
        Me.cBulan.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cBulan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cBulan.Properties.MaxLength = 6
        Me.cBulan.Size = New System.Drawing.Size(24, 20)
        Me.cBulan.TabIndex = 94
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(491, 156)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl13.TabIndex = 93
        Me.LabelControl13.Text = "Alamat"
        '
        'cAlamat
        '
        Me.cAlamat.EditValue = "123456"
        Me.cAlamat.Location = New System.Drawing.Point(600, 153)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cAlamat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cAlamat.Properties.MaxLength = 6
        Me.cAlamat.Size = New System.Drawing.Size(292, 20)
        Me.cAlamat.TabIndex = 92
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(491, 131)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl12.TabIndex = 91
        Me.LabelControl12.Text = "Suku Bunga Setahun"
        '
        'cSukuBunga
        '
        Me.cSukuBunga.EditValue = "123456"
        Me.cSukuBunga.Location = New System.Drawing.Point(600, 128)
        Me.cSukuBunga.Name = "cSukuBunga"
        Me.cSukuBunga.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSukuBunga.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSukuBunga.Properties.MaxLength = 6
        Me.cSukuBunga.Size = New System.Drawing.Size(54, 20)
        Me.cSukuBunga.TabIndex = 90
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(491, 106)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl11.TabIndex = 89
        Me.LabelControl11.Text = "Kualitas"
        '
        'cKualitas
        '
        Me.cKualitas.EditValue = "123456"
        Me.cKualitas.Location = New System.Drawing.Point(600, 103)
        Me.cKualitas.Name = "cKualitas"
        Me.cKualitas.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKualitas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKualitas.Properties.MaxLength = 6
        Me.cKualitas.Size = New System.Drawing.Size(23, 20)
        Me.cKualitas.TabIndex = 88
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(491, 82)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl10.TabIndex = 87
        Me.LabelControl10.Text = "Tgl Akhir"
        '
        'cTglAkhir
        '
        Me.cTglAkhir.EditValue = "1234567890"
        Me.cTglAkhir.Location = New System.Drawing.Point(600, 79)
        Me.cTglAkhir.Name = "cTglAkhir"
        Me.cTglAkhir.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cTglAkhir.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cTglAkhir.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cTglAkhir.Properties.MaxLength = 15
        Me.cTglAkhir.Size = New System.Drawing.Size(83, 20)
        Me.cTglAkhir.TabIndex = 86
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(491, 58)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(41, 13)
        Me.lblTglRegister.TabIndex = 85
        Me.lblTglRegister.Text = "Tgl Mulai"
        '
        'dTglMulai
        '
        Me.dTglMulai.EditValue = Nothing
        Me.dTglMulai.Location = New System.Drawing.Point(601, 55)
        Me.dTglMulai.Name = "dTglMulai"
        Me.dTglMulai.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglMulai.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglMulai.Size = New System.Drawing.Size(82, 20)
        Me.dTglMulai.TabIndex = 84
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(491, 34)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl9.TabIndex = 30
        Me.LabelControl9.Text = "Jangka Waktu"
        '
        'cHari
        '
        Me.cHari.EditValue = "123456"
        Me.cHari.Location = New System.Drawing.Point(601, 31)
        Me.cHari.Name = "cHari"
        Me.cHari.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cHari.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cHari.Properties.MaxLength = 6
        Me.cHari.Size = New System.Drawing.Size(22, 20)
        Me.cHari.TabIndex = 29
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(491, 10)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "Keterkaitan Dg Bank"
        '
        'cKeterkaitan
        '
        Me.cKeterkaitan.EditValue = "123456"
        Me.cKeterkaitan.Location = New System.Drawing.Point(601, 7)
        Me.cKeterkaitan.Name = "cKeterkaitan"
        Me.cKeterkaitan.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKeterkaitan.Properties.MaxLength = 6
        Me.cKeterkaitan.Size = New System.Drawing.Size(54, 20)
        Me.cKeterkaitan.TabIndex = 27
        '
        'cLokasi
        '
        Me.cLokasi.Location = New System.Drawing.Point(80, 153)
        Me.cLokasi.Name = "cLokasi"
        Me.cLokasi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cLokasi.Size = New System.Drawing.Size(65, 20)
        Me.cLokasi.TabIndex = 26
        '
        'cNamaLokasi
        '
        Me.cNamaLokasi.Enabled = False
        Me.cNamaLokasi.Location = New System.Drawing.Point(151, 153)
        Me.cNamaLokasi.Name = "cNamaLokasi"
        Me.cNamaLokasi.Size = New System.Drawing.Size(191, 20)
        Me.cNamaLokasi.TabIndex = 25
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(7, 156)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl6.TabIndex = 24
        Me.LabelControl6.Text = "Lokasi"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(7, 131)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl5.TabIndex = 23
        Me.LabelControl5.Text = "Jenis"
        '
        'cJenis
        '
        Me.cJenis.EditValue = "123456"
        Me.cJenis.Location = New System.Drawing.Point(80, 128)
        Me.cJenis.Name = "cJenis"
        Me.cJenis.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cJenis.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cJenis.Properties.MaxLength = 6
        Me.cJenis.Size = New System.Drawing.Size(44, 20)
        Me.cJenis.TabIndex = 22
        '
        'cJenisBank
        '
        Me.cJenisBank.Location = New System.Drawing.Point(80, 79)
        Me.cJenisBank.Name = "cJenisBank"
        Me.cJenisBank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cJenisBank.Size = New System.Drawing.Size(44, 20)
        Me.cJenisBank.TabIndex = 21
        '
        'cNamaJenisBank
        '
        Me.cNamaJenisBank.Enabled = False
        Me.cNamaJenisBank.Location = New System.Drawing.Point(130, 79)
        Me.cNamaJenisBank.Name = "cNamaJenisBank"
        Me.cNamaJenisBank.Size = New System.Drawing.Size(191, 20)
        Me.cNamaJenisBank.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(7, 82)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Jenis Bank"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(80, 55)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(120, 20)
        Me.cRekening.TabIndex = 18
        '
        'cNamaRekening
        '
        Me.cNamaRekening.Enabled = False
        Me.cNamaRekening.Location = New System.Drawing.Point(206, 55)
        Me.cNamaRekening.Name = "cNamaRekening"
        Me.cNamaRekening.Size = New System.Drawing.Size(266, 20)
        Me.cNamaRekening.TabIndex = 17
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 58)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Rekening"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 106)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl1.TabIndex = 15
        Me.LabelControl1.Text = "Sandi Bank"
        '
        'cSandiBank
        '
        Me.cSandiBank.EditValue = "123456"
        Me.cSandiBank.Location = New System.Drawing.Point(80, 103)
        Me.cSandiBank.Name = "cSandiBank"
        Me.cSandiBank.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBank.Properties.MaxLength = 6
        Me.cSandiBank.Size = New System.Drawing.Size(65, 20)
        Me.cSandiBank.TabIndex = 14
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(241, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(80, 7)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(34, 20)
        Me.cKode.TabIndex = 0
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(2, 402)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(899, 33)
        Me.PanelControl7.TabIndex = 14
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(815, 4)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(734, 4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(192, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(116, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(40, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'MstAntarBankAktiva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 440)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "MstAntarBankAktiva"
        Me.Text = "Antar Bank Aktiva"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cTahun.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBulan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSukuBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKualitas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTglAkhir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglMulai.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglMulai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cHari.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cLokasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaLokasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cJenisBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaJenisBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSandiBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBank As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cHari As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterkaitan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cLokasi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaLokasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cJenis As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cJenisBank As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaJenisBank As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglMulai As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSukuBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKualitas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTglAkhir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cTahun As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cBulan As DevExpress.XtraEditors.TextEdit
End Class
