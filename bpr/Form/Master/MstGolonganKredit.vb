﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstGolonganKredit
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String
    Dim dtData As New DataTable
    Dim dtRekening As New DataTable

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cRekening.EditValue = ""
        cNamaRekening.Text = ""
        cRekeningBunga.EditValue = ""
        cNamaRekeningBunga.Text = ""
        cRekeningDenda.EditValue = ""
        cNamaRekeningDenda.Text = ""
        cRekeningAdministrasi.EditValue = ""
        cNamaRekeningAdministrasi.Text = ""
        cRekeningNotaris.EditValue = ""
        cNamaRekeningNotaris.Text = ""
        cRekeningMaterai.EditValue = ""
        cNamaRekeningMaterai.Text = ""
        cRekeningAsuransi.EditValue = ""
        cNamaRekeningAsuransi.Text = ""
        cRekeningProvisi.EditValue = ""
        cNamaRekeningProvisi.Text = ""
        cRekeningPendapatanProvisi.EditValue = ""
        cNamaRekeningPendapatanProvisi.Text = ""
        cRekeningTitipanAngsuran.EditValue = ""
        cNamaRekeningTitipanAngsuran.Text = ""
        cRekeningAccrual.EditValue = ""
        cNamaRekeningAccrual.Text = ""
        nToleransiDenda.Text = "0"
        optAmortisasi.SelectedIndex = 0
    End Sub

    Private Sub InitLookUp()
        GetDataLookup("rekening", cRekening)
        GetDataLookup("rekening", cRekeningBunga)
        GetDataLookup("rekening", cRekeningDenda)
        GetDataLookup("rekening", cRekeningAdministrasi)
        GetDataLookup("rekening", cRekeningNotaris)
        GetDataLookup("rekening", cRekeningMaterai)
        GetDataLookup("rekening", cRekeningAsuransi)
        GetDataLookup("rekening", cRekeningProvisi)
        GetDataLookup("rekening", cRekeningPendapatanProvisi)
        GetDataLookup("rekening", cRekeningTitipanAngsuran)
        GetDataLookup("rekening", cRekeningAccrual)
    End Sub

    Private Sub SetingTabIndex()
        Dim n As Integer
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cRekeningBunga.TabIndex, n)
        SetTabIndex(cRekeningDenda.TabIndex, n)
        SetTabIndex(cRekeningAdministrasi.TabIndex, n)
        SetTabIndex(cRekeningNotaris.TabIndex, n)
        SetTabIndex(cRekeningMaterai.TabIndex, n)
        SetTabIndex(cRekeningAsuransi.TabIndex, n)
        SetTabIndex(cRekeningProvisi.TabIndex, n)
        SetTabIndex(cRekeningPendapatanProvisi.TabIndex, n)
        SetTabIndex(cRekeningTitipanAngsuran.TabIndex, n)
        SetTabIndex(cRekeningAccrual.TabIndex, n)
        SetTabIndex(nToleransiDenda.TabIndex, n)
        SetTabIndex(optAmortisasi.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cRekeningBunga.EnterMoveNextControl = True
        cRekeningDenda.EnterMoveNextControl = True
        cRekeningAdministrasi.EnterMoveNextControl = True
        cRekeningNotaris.EnterMoveNextControl = True
        cRekeningMaterai.EnterMoveNextControl = True
        cRekeningAsuransi.EnterMoveNextControl = True
        cRekeningProvisi.EnterMoveNextControl = True
        cRekeningPendapatanProvisi.EnterMoveNextControl = True
        cRekeningTitipanAngsuran.EnterMoveNextControl = True
        cRekeningAccrual.EnterMoveNextControl = True
        nToleransiDenda.EnterMoveNextControl = True
        optAmortisasi.EnterMoveNextControl = True

        FormatTextBox(nToleransiDenda, formatType.BilRpPict, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Akan Dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            objData.Delete(GetDSN, "golongandeposito", "kode", data.myOperator.Assign, cKode.Text, cWhere)
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Not CheckData(cKode, "Kode Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cKeterangan, "Nama Bank Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKeterangan.Focus()
            Exit Function
        End If
        If Not CheckData(cRekening, "Rekening Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim cAmortisasi As String = GetOpt(optAmortisasi)
                Dim vaField() As Object = {"kode", "keterangan", "Rekening", _
                                           "RekeningAdministrasi", "RekeningDenda", _
                                           "RekeningNotaris", "RekeningMaterai", _
                                           "RekeningBunga", "RekeningAsuransi", "RekeningProvisi", _
                                           "RekeningPendapatanProvisi", "RekeningTitipan", "Toleransi", _
                                           "Amortisasi", "RekeningAccrual"}
                Dim vaValue() As Object = {cKode.Text, cKeterangan.Text, cRekening.Text, _
                                           cRekeningAdministrasi.Text, cRekeningDenda.Text, _
                                           cRekeningNotaris.Text, cRekeningMaterai.Text, _
                                           cRekeningBunga.Text, cRekeningAsuransi.Text, cRekeningProvisi.Text, _
                                           cRekeningPendapatanProvisi.Text, cRekeningTitipanAngsuran.Text, Val(nToleransiDenda.Text), _
                                           cAmortisasi, cRekeningAccrual.Text}
                Dim cWhere As String = String.Format("kode = '{0}' and status = '{1}'", cKode.Text, cStatus)
                objData.Update(GetDSN, "golongankredit", cWhere, vaField, vaValue)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Function GetNamaRekening(cRekening As String) As String
        Dim nBaris As DataRow() = dtRekening.Select(String.Format("kode = {0} ", cRekening))
        If nBaris.Count > 0 Then
            GetNamaRekening = nBaris(0).Item("rekening").ToString
        Else
            GetNamaRekening = ""
        End If
    End Function

    Private Sub GetMemory()
        dtData = objData.Browse(GetDSN, "GolonganKredit", , "Kode", , cKode.Text)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cKode.Text = .Item("Kode").ToString
                cKeterangan.Text = .Item("Keterangan").ToString
                cRekening.Text = .Item("RekeningAkuntansi").ToString
                cNamaRekening.Text = GetNamaRekening(cRekening.Text)
                cRekeningBunga.Text = .Item("RekeningBunga").ToString
                cNamaRekeningBunga.Text = GetNamaRekening(cRekeningBunga.Text)
                cRekeningDenda.Text = .Item("RekeningDenda").ToString
                cNamaRekeningDenda.Text = GetNamaRekening("NamaRekeningDenda")
                cRekeningAdministrasi.Text = .Item("RekeningAdministrasi").ToString
                cNamaRekeningAdministrasi.Text = GetNamaRekening("NamaRekeningAdministrasi")
                cRekeningNotaris.Text = .Item("RekeningNotaris").ToString
                cNamaRekeningNotaris.Text = GetNamaRekening("NamaRekeningNotaris")
                cRekeningMaterai.Text = .Item("RekeningMaterai").ToString
                cNamaRekeningMaterai.Text = GetNamaRekening("NamaRekeningMaterai")
                cRekeningAsuransi.Text = .Item("Rekeningasuransi").ToString
                cNamaRekeningAsuransi.Text = GetNamaRekening("NamaRekeningAsuransi")
                cRekeningProvisi.Text = .Item("RekeningProvisi").ToString
                cNamaRekeningProvisi.Text = GetNamaRekening("NamaRekeningProvisi")
                cRekeningPendapatanProvisi.Text = .Item("RekeningPendapatanProvisi").ToString
                cNamaRekeningPendapatanProvisi.Text = GetNamaRekening("NamaRekeningPendapatanProvisi")
                cRekeningTitipanAngsuran.Text = .Item("RekeningTitipan").ToString
                cNamaRekeningTitipanAngsuran.Text = GetNamaRekening("NamaRekeningTitipan")
                cRekeningAccrual.Text = .Item("RekeningAccrual").ToString
                cNamaRekeningAccrual.Text = GetNamaRekening(cRekeningAccrual.Text)
                nToleransiDenda.Text = .Item("Toleransi").ToString
                SetOpt(optAmortisasi, .Item("Amortisasi").ToString)
            End With
        End If
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Dim cField As String = "Kode,Keterangan,Rekening,RekeningBunga,RekeningDenda,RekeningAdministrasi,RekeningProvisi,"
            cField = cField & "RekeningNotaris,RekeningMaterai,RekeningAsuransi,RekeningPendapatanProvisi,RekeningTitipan,RekeningAccrual,"
            cField = cField & "Toleransi,Amortisasi"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "GolonganKredit", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("GolonganKredit")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 50)
            GridColumnFormat(GridView1, "Keterangan")
            GridColumnFormat(GridView1, "Rekening", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningBunga", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningDenda", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningAdministrasi", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningProvisi", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningNotaris", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningMaterai", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningAsuransi", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningPendapatanProvisi", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningTitipan", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningAccrual", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Toleransi", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Amortisasi", , , 50, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetDataGrid()
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
            cRekening.EditValue = row(2).ToString
            cNamaRekening.Text = GetNamaRekening(cRekening.Text)
            cRekeningBunga.EditValue = row(3).ToString
            cNamaRekeningBunga.Text = GetNamaRekening(cRekeningBunga.Text)
            cRekeningDenda.Text = row(4).ToString
            cNamaRekeningDenda.Text = GetNamaRekening(cNamaRekeningDenda.Text)
            cRekeningAdministrasi.Text = row(5).ToString
            cNamaRekeningAdministrasi.Text = GetNamaRekening(cRekeningAdministrasi.Text)
            cRekeningProvisi.Text = row(6).ToString
            cNamaRekeningProvisi.Text = GetNamaRekening(cRekeningProvisi.Text)
            cRekeningNotaris.Text = row(7).ToString
            cNamaRekeningNotaris.Text = GetNamaRekening(cRekeningNotaris.Text)
            cRekeningMaterai.Text = row(8).ToString
            cNamaRekeningMaterai.Text = GetNamaRekening(cRekeningMaterai.Text)
            cRekeningAsuransi.Text = row(9).ToString
            cNamaRekeningAsuransi.Text = GetNamaRekening(cRekeningAsuransi.Text)
            cRekeningPendapatanProvisi.Text = row(10).ToString
            cNamaRekeningPendapatanProvisi.Text = GetNamaRekening(cRekeningPendapatanProvisi.Text)
            cRekeningTitipanAngsuran.Text = row(11).ToString
            cNamaRekeningTitipanAngsuran.Text = GetNamaRekening(cRekeningTitipanAngsuran.Text)
            cRekeningAccrual.Text = row(12).ToString
            cNamaRekeningAccrual.Text = GetNamaRekening(cRekeningAccrual.Text)
            nToleransiDenda.Text = row(13).ToString
            SetOpt(optAmortisasi, row(14).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim cDataTable = objData.Browse(GetDSN, "GolonganKredit", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, , "kode")
            With cDataTable
                If .Rows.Count > 0 Then
                    If nPos = myPos.add Then
                        MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                Else
                    MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        GetKeteranganLookUp(cRekening, cNamaRekening)
    End Sub

    Private Sub cRekeningBunga_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningBunga.Closed
        GetKeteranganLookUp(cRekeningBunga, cNamaRekeningBunga)
    End Sub

    Private Sub cRekeningAccrual_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAccrual.Closed
        GetKeteranganLookUp(cRekeningAccrual, cNamaRekeningAccrual)
    End Sub

    Private Sub cRekeningAdministrasi_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAdministrasi.Closed
        GetKeteranganLookUp(cRekeningAdministrasi, cNamaRekeningAdministrasi)
    End Sub

    Private Sub cRekeningAsuransi_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAsuransi.Closed
        GetKeteranganLookUp(cRekeningAsuransi, cNamaRekeningAsuransi)
    End Sub

    Private Sub cRekeningDenda_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningDenda.Closed
        GetKeteranganLookUp(cRekeningDenda, cNamaRekeningDenda)
    End Sub

    Private Sub cRekeningMaterai_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningMaterai.Closed
        GetKeteranganLookUp(cRekeningMaterai, cNamaRekeningMaterai)
    End Sub

    Private Sub cRekeningNotaris_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningNotaris.Closed
        GetKeteranganLookUp(cRekeningNotaris, cNamaRekeningNotaris)
    End Sub

    Private Sub cRekeningPendapatanProvisi_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningPendapatanProvisi.Closed
        GetKeteranganLookUp(cRekeningPendapatanProvisi, cNamaRekeningPendapatanProvisi)
    End Sub

    Private Sub cRekeningProvisi_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningProvisi.Closed
        GetKeteranganLookUp(cRekeningProvisi, cNamaRekeningProvisi)
    End Sub

    Private Sub cRekeningTitipanAngsuran_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningTitipanAngsuran.Closed
        GetKeteranganLookUp(cRekeningTitipanAngsuran, cNamaRekeningTitipanAngsuran)
    End Sub

    Private Sub MstGolonganKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        CenterFormManual(Me)
        dtRekening = objData.Browse(GetDSN, "rekening", "Kode,Keterangan", , , , , "Kode")

        InitValue()
        InitLookUp()
        SetingTabIndex()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        GetSQL()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        GetDataGrid()
    End Sub
End Class