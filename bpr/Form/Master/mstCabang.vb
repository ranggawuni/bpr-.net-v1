﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System.Data


Public Class mstCabang
    ReadOnly objData As New data()
    Dim nPos As myPos
    Dim lEdit As Boolean

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "kode,keterangan,alamat,telepon,fax,induk,jeniskantor"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "cabang", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("cabang")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "kode", , , 80)
            GridColumnFormat(GridView1, "keterangan", , , -2)
            GridColumnFormat(GridView1, "alamat", , , -2)
            GridColumnFormat(GridView1, "telepon", , , 40)
            GridColumnFormat(GridView1, "fax", , , 40)
            GridColumnFormat(GridView1, "induk", , , 30)
            GridColumnFormat(GridView1, "jeniskantor", , , 40)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Trim(cKode.Text) = "" Then
            MessageBox.Show("Kode Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cKeterangan, "Keterangan Tidak Boleh Kosong.") Then
            Return False
            cKeterangan.Focus()
            Exit Function
        End If
        If Not CheckData(cInduk, "Kode Induk Kantor Tidak Boleh Kosong") Then
            Return False
            cInduk.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cAlamat.Text = ""
        cTelp.Text = ""
        cFax.Text = ""
        optKantor.SelectedIndex = 0
        cInduk.EditValue = Nothing
        cNamaInduk.Text = ""
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl2.Enabled = lPar
        GridControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
    End Sub

    Private Sub GetMemory()
        Try
            Using cDataTable = objData.Browse(GetDSN, "cabang", , "kode", data.myOperator.Assign, cKode.Text)
                With cDataTable
                    If .Rows.Count > 0 Then
                        cKode.Text = .Rows(0).Item("kode").ToString
                        cKeterangan.Text = .Rows(0).Item("keterangan").ToString
                        cAlamat.Text = .Rows(0).Item("alamat").ToString
                        cTelp.Text = .Rows(0).Item("telepon").ToString
                        cFax.Text = .Rows(0).Item("fax").ToString
                        SetOpt(optKantor, .Rows(0).Item("jeniskantor").ToString)
                    End If
                End With
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                objData.Delete(GetDSN, "cabang", "kode", data.myOperator.Assign, cKode.Text)
                GetData()
                GetEdit(False)
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub mstCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetEdit(False)
        GetData()
        GetDataLookup("cabang", cInduk)
        InitValue()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi, cmdPreview)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cAlamat.TabIndex, n)
        SetTabIndex(cTelp.TabIndex, n)
        SetTabIndex(cFax.TabIndex, n)
        SetTabIndex(optKantor.TabIndex, n)
        SetTabIndex(cInduk.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cAlamat.EnterMoveNextControl = True
        cTelp.EnterMoveNextControl = True
        cFax.EnterMoveNextControl = True
        optKantor.EnterMoveNextControl = True
        cInduk.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cJenisKantor As String = GetOpt(optKantor)
                    Dim cWhere As String = String.Format("kode = '{0}'", cKode.Text)
                    Dim vaField() As Object = {"cabangentry", "kode", "keterangan", "alamat", "telepon", "fax", "induk", "jeniskantor"}
                    Dim vaValue() As Object = {"01", cKode.Text, cKeterangan.Text, cAlamat.Text, cTelp.Text, cFax.Text, cInduk.Text, cJenisKantor}
                    objData.Update(GetDSN, "cabang", cWhere, vaField, vaValue)
                    GetEdit(False)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles cKode.LostFocus
        Try
            If cKode.Text <> "" Then
                Using cDataTable = objData.Browse(GetDSN, "cabang", , "kode", data.myOperator.Assign, cKode.Text)
                    If cDataTable.Rows.Count > 0 Then
                        If nPos = myPos.add Then
                            MessageBox.Show("Kode sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                        GetMemory()
                        If nPos = myPos.delete Then
                            DeleteData()
                        End If
                    ElseIf nPos <> myPos.add Then
                        MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cInduk_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cInduk.Closed
        GetKeteranganLookUp(cInduk, cNamaInduk)
    End Sub

    Private Sub cInduk_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cInduk.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cInduk, cNamaInduk)
        End If
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
            cAlamat.Text = row(2).ToString
            cTelp.Text = row(3).ToString
            cFax.Text = row(4).ToString
            SetOpt(optKantor, row(6).ToString)
            cInduk.EditValue = row(5).ToString
            Dim cDataTable = objData.Browse(GetDSN, "cabang", "keterangan", "kode", data.myOperator.Assign, cInduk.Text)
            If cDataTable.Rows.Count > 0 Then
                cNamaInduk.Text = cDataTable.Rows(0).Item("keterangan").ToString
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class