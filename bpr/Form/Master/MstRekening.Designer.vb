﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mstRekening
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mstRekening))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.tabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI1 = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis1 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode1 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI2 = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis2 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode2 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI3 = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis3 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode3 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI4 = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis4 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode4 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.cSandiBI5 = New DevExpress.XtraEditors.TextEdit()
        Me.optJenis5 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode5 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl6 = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdPreview = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.tabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cSandiBI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cSandiBI1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cSandiBI2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cSandiBI3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.cSandiBI4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        CType(Me.cSandiBI5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabControl1
        '
        Me.tabControl1.Location = New System.Drawing.Point(1, 0)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.tabControl1.Size = New System.Drawing.Size(655, 359)
        Me.tabControl1.TabIndex = 0
        Me.tabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GridControl1)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage1.Text = "1. Aktiva"
        '
        'GridControl1
        '
        Me.GridControl1.Enabled = False
        Me.GridControl1.Location = New System.Drawing.Point(5, 127)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(640, 200)
        Me.GridControl1.TabIndex = 11
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cSandiBI)
        Me.PanelControl1.Controls.Add(Me.optJenis)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl1.TabIndex = 10
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl1.TabIndex = 15
        Me.LabelControl1.Text = "Sandi BI"
        '
        'cSandiBI
        '
        Me.cSandiBI.EditValue = "123456"
        Me.cSandiBI.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI.Name = "cSandiBI"
        Me.cSandiBI.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI.Properties.MaxLength = 6
        Me.cSandiBI.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI.TabIndex = 14
        '
        'optJenis
        '
        Me.optJenis.Location = New System.Drawing.Point(81, 55)
        Me.optJenis.Name = "optJenis"
        Me.optJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis.Size = New System.Drawing.Size(117, 29)
        Me.optJenis.TabIndex = 13
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "Jenis Rekening"
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(80, 8)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cKode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(86, 20)
        Me.cKode.TabIndex = 0
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage2.Text = "2. Pasiva"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.cSandiBI1)
        Me.PanelControl2.Controls.Add(Me.optJenis1)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.cKeterangan1)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.cKode1)
        Me.PanelControl2.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl2.TabIndex = 12
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl2.TabIndex = 15
        Me.LabelControl2.Text = "Sandi BI"
        '
        'cSandiBI1
        '
        Me.cSandiBI1.EditValue = "123456"
        Me.cSandiBI1.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI1.Name = "cSandiBI1"
        Me.cSandiBI1.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI1.Properties.MaxLength = 6
        Me.cSandiBI1.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI1.TabIndex = 14
        '
        'optJenis1
        '
        Me.optJenis1.Location = New System.Drawing.Point(81, 55)
        Me.optJenis1.Name = "optJenis1"
        Me.optJenis1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis1.Size = New System.Drawing.Size(117, 29)
        Me.optJenis1.TabIndex = 13
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Jenis Rekening"
        '
        'cKeterangan1
        '
        Me.cKeterangan1.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan1.Name = "cKeterangan1"
        Me.cKeterangan1.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan1.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Keterangan"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl8.TabIndex = 1
        Me.LabelControl8.Text = "Kode"
        '
        'cKode1
        '
        Me.cKode1.EditValue = "1234567890"
        Me.cKode1.Location = New System.Drawing.Point(80, 8)
        Me.cKode1.Name = "cKode1"
        Me.cKode1.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKode1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode1.Properties.MaxLength = 15
        Me.cKode1.Size = New System.Drawing.Size(86, 20)
        Me.cKode1.TabIndex = 0
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(5, 127)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(640, 200)
        Me.GridControl2.TabIndex = 11
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage3.Controls.Add(Me.GridControl3)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage3.Text = "3. Modal"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.cSandiBI2)
        Me.PanelControl3.Controls.Add(Me.optJenis2)
        Me.PanelControl3.Controls.Add(Me.LabelControl10)
        Me.PanelControl3.Controls.Add(Me.cKeterangan2)
        Me.PanelControl3.Controls.Add(Me.LabelControl11)
        Me.PanelControl3.Controls.Add(Me.LabelControl12)
        Me.PanelControl3.Controls.Add(Me.cKode2)
        Me.PanelControl3.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl3.TabIndex = 14
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl9.TabIndex = 15
        Me.LabelControl9.Text = "Sandi BI"
        '
        'cSandiBI2
        '
        Me.cSandiBI2.EditValue = "123456"
        Me.cSandiBI2.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI2.Name = "cSandiBI2"
        Me.cSandiBI2.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI2.Properties.MaxLength = 6
        Me.cSandiBI2.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI2.TabIndex = 14
        '
        'optJenis2
        '
        Me.optJenis2.Location = New System.Drawing.Point(81, 55)
        Me.optJenis2.Name = "optJenis2"
        Me.optJenis2.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis2.Size = New System.Drawing.Size(117, 29)
        Me.optJenis2.TabIndex = 13
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl10.TabIndex = 10
        Me.LabelControl10.Text = "Jenis Rekening"
        '
        'cKeterangan2
        '
        Me.cKeterangan2.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan2.Name = "cKeterangan2"
        Me.cKeterangan2.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan2.TabIndex = 3
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl11.TabIndex = 2
        Me.LabelControl11.Text = "Keterangan"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl12.TabIndex = 1
        Me.LabelControl12.Text = "Kode"
        '
        'cKode2
        '
        Me.cKode2.EditValue = "1234567890"
        Me.cKode2.Location = New System.Drawing.Point(80, 8)
        Me.cKode2.Name = "cKode2"
        Me.cKode2.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKode2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode2.Properties.MaxLength = 15
        Me.cKode2.Size = New System.Drawing.Size(86, 20)
        Me.cKode2.TabIndex = 0
        '
        'GridControl3
        '
        Me.GridControl3.Enabled = False
        Me.GridControl3.Location = New System.Drawing.Point(5, 127)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(640, 200)
        Me.GridControl3.TabIndex = 13
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.PanelControl4)
        Me.XtraTabPage4.Controls.Add(Me.GridControl4)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage4.Text = "4. Pendapatan"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.LabelControl13)
        Me.PanelControl4.Controls.Add(Me.cSandiBI3)
        Me.PanelControl4.Controls.Add(Me.optJenis3)
        Me.PanelControl4.Controls.Add(Me.LabelControl14)
        Me.PanelControl4.Controls.Add(Me.cKeterangan3)
        Me.PanelControl4.Controls.Add(Me.LabelControl15)
        Me.PanelControl4.Controls.Add(Me.LabelControl16)
        Me.PanelControl4.Controls.Add(Me.cKode3)
        Me.PanelControl4.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl4.TabIndex = 14
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl13.TabIndex = 15
        Me.LabelControl13.Text = "Sandi BI"
        '
        'cSandiBI3
        '
        Me.cSandiBI3.EditValue = "123456"
        Me.cSandiBI3.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI3.Name = "cSandiBI3"
        Me.cSandiBI3.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI3.Properties.MaxLength = 6
        Me.cSandiBI3.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI3.TabIndex = 14
        '
        'optJenis3
        '
        Me.optJenis3.Location = New System.Drawing.Point(81, 55)
        Me.optJenis3.Name = "optJenis3"
        Me.optJenis3.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis3.Size = New System.Drawing.Size(117, 29)
        Me.optJenis3.TabIndex = 13
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl14.TabIndex = 10
        Me.LabelControl14.Text = "Jenis Rekening"
        '
        'cKeterangan3
        '
        Me.cKeterangan3.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan3.Name = "cKeterangan3"
        Me.cKeterangan3.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan3.TabIndex = 3
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl15.TabIndex = 2
        Me.LabelControl15.Text = "Keterangan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl16.TabIndex = 1
        Me.LabelControl16.Text = "Kode"
        '
        'cKode3
        '
        Me.cKode3.EditValue = "1234567890"
        Me.cKode3.Location = New System.Drawing.Point(80, 8)
        Me.cKode3.Name = "cKode3"
        Me.cKode3.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKode3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode3.Properties.MaxLength = 15
        Me.cKode3.Size = New System.Drawing.Size(86, 20)
        Me.cKode3.TabIndex = 0
        '
        'GridControl4
        '
        Me.GridControl4.Enabled = False
        Me.GridControl4.Location = New System.Drawing.Point(5, 127)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(640, 200)
        Me.GridControl4.TabIndex = 13
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.PanelControl5)
        Me.XtraTabPage5.Controls.Add(Me.GridControl5)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage5.Text = "5. Biaya"
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.LabelControl17)
        Me.PanelControl5.Controls.Add(Me.cSandiBI4)
        Me.PanelControl5.Controls.Add(Me.optJenis4)
        Me.PanelControl5.Controls.Add(Me.LabelControl18)
        Me.PanelControl5.Controls.Add(Me.cKeterangan4)
        Me.PanelControl5.Controls.Add(Me.LabelControl19)
        Me.PanelControl5.Controls.Add(Me.LabelControl20)
        Me.PanelControl5.Controls.Add(Me.cKode4)
        Me.PanelControl5.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl5.TabIndex = 14
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl17.TabIndex = 15
        Me.LabelControl17.Text = "Sandi BI"
        '
        'cSandiBI4
        '
        Me.cSandiBI4.EditValue = "123456"
        Me.cSandiBI4.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI4.Name = "cSandiBI4"
        Me.cSandiBI4.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI4.Properties.MaxLength = 6
        Me.cSandiBI4.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI4.TabIndex = 14
        '
        'optJenis4
        '
        Me.optJenis4.Location = New System.Drawing.Point(81, 55)
        Me.optJenis4.Name = "optJenis4"
        Me.optJenis4.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis4.Size = New System.Drawing.Size(117, 29)
        Me.optJenis4.TabIndex = 13
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl18.TabIndex = 10
        Me.LabelControl18.Text = "Jenis Rekening"
        '
        'cKeterangan4
        '
        Me.cKeterangan4.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan4.Name = "cKeterangan4"
        Me.cKeterangan4.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan4.TabIndex = 3
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl19.TabIndex = 2
        Me.LabelControl19.Text = "Keterangan"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl20.TabIndex = 1
        Me.LabelControl20.Text = "Kode"
        '
        'cKode4
        '
        Me.cKode4.EditValue = "1234567890"
        Me.cKode4.Location = New System.Drawing.Point(80, 8)
        Me.cKode4.Name = "cKode4"
        Me.cKode4.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKode4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode4.Properties.MaxLength = 15
        Me.cKode4.Size = New System.Drawing.Size(86, 20)
        Me.cKode4.TabIndex = 0
        '
        'GridControl5
        '
        Me.GridControl5.Enabled = False
        Me.GridControl5.Location = New System.Drawing.Point(5, 127)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(640, 200)
        Me.GridControl5.TabIndex = 13
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView5.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.PanelControl6)
        Me.XtraTabPage6.Controls.Add(Me.GridControl6)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage6.Text = "6. Rek. Administratif"
        '
        'PanelControl6
        '
        Me.PanelControl6.Controls.Add(Me.LabelControl21)
        Me.PanelControl6.Controls.Add(Me.cSandiBI5)
        Me.PanelControl6.Controls.Add(Me.optJenis5)
        Me.PanelControl6.Controls.Add(Me.LabelControl22)
        Me.PanelControl6.Controls.Add(Me.cKeterangan5)
        Me.PanelControl6.Controls.Add(Me.LabelControl23)
        Me.PanelControl6.Controls.Add(Me.LabelControl24)
        Me.PanelControl6.Controls.Add(Me.cKode5)
        Me.PanelControl6.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(640, 118)
        Me.PanelControl6.TabIndex = 14
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl21.TabIndex = 15
        Me.LabelControl21.Text = "Sandi BI"
        '
        'cSandiBI5
        '
        Me.cSandiBI5.EditValue = "123456"
        Me.cSandiBI5.Location = New System.Drawing.Point(81, 88)
        Me.cSandiBI5.Name = "cSandiBI5"
        Me.cSandiBI5.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cSandiBI5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cSandiBI5.Properties.MaxLength = 6
        Me.cSandiBI5.Size = New System.Drawing.Size(54, 20)
        Me.cSandiBI5.TabIndex = 14
        '
        'optJenis5
        '
        Me.optJenis5.Location = New System.Drawing.Point(81, 55)
        Me.optJenis5.Name = "optJenis5"
        Me.optJenis5.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Induk"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Detail")})
        Me.optJenis5.Size = New System.Drawing.Size(117, 29)
        Me.optJenis5.TabIndex = 13
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl22.TabIndex = 10
        Me.LabelControl22.Text = "Jenis Rekening"
        '
        'cKeterangan5
        '
        Me.cKeterangan5.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan5.Name = "cKeterangan5"
        Me.cKeterangan5.Size = New System.Drawing.Size(473, 20)
        Me.cKeterangan5.TabIndex = 3
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl23.TabIndex = 2
        Me.LabelControl23.Text = "Keterangan"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl24.TabIndex = 1
        Me.LabelControl24.Text = "Kode"
        '
        'cKode5
        '
        Me.cKode5.EditValue = "1234567890"
        Me.cKode5.Location = New System.Drawing.Point(80, 8)
        Me.cKode5.Name = "cKode5"
        Me.cKode5.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.cKode5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode5.Properties.MaxLength = 15
        Me.cKode5.Size = New System.Drawing.Size(86, 20)
        Me.cKode5.TabIndex = 0
        '
        'GridControl6
        '
        Me.GridControl6.Enabled = False
        Me.GridControl6.Location = New System.Drawing.Point(5, 127)
        Me.GridControl6.MainView = Me.GridView6
        Me.GridControl6.Name = "GridControl6"
        Me.GridControl6.Size = New System.Drawing.Size(640, 200)
        Me.GridControl6.TabIndex = 13
        Me.GridControl6.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.GridControl = Me.GridControl6
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView6.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdPreview)
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(1, 360)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(650, 33)
        Me.PanelControl7.TabIndex = 11
        '
        'cmdPreview
        '
        Me.cmdPreview.Location = New System.Drawing.Point(5, 5)
        Me.cmdPreview.Name = "cmdPreview"
        Me.cmdPreview.Size = New System.Drawing.Size(27, 24)
        Me.cmdPreview.TabIndex = 12
        Me.cmdPreview.Visible = False
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(37, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Image = CType(resources.GetObject("cmdKeluar.Image"), System.Drawing.Image)
        Me.cmdKeluar.Location = New System.Drawing.Point(565, 4)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(484, 4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(222, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(146, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(70, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'mstRekening
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdKeluar
        Me.ClientSize = New System.Drawing.Size(654, 395)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.tabControl1)
        Me.KeyPreview = True
        Me.Name = "mstRekening"
        Me.Text = "Rekening"
        CType(Me.tabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cSandiBI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cSandiBI1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cSandiBI2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.cSandiBI3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.PanelControl5.PerformLayout()
        CType(Me.cSandiBI4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage6.ResumeLayout(False)
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        Me.PanelControl6.PerformLayout()
        CType(Me.cSandiBI5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents optJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdPreview As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl6 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optJenis1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optJenis2 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optJenis3 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optJenis4 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSandiBI5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optJenis5 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode5 As DevExpress.XtraEditors.TextEdit
End Class
