﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstWilayah
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String
    Dim dtData As New DataTable

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cDefaultWilayah.EditValue = ""
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "Kode,Keterangan"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "wilayah", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("wilayah")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", , , 5, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "Keterangan")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cDefaultWilayah.Text = aCfg(eCfg.msKodeDefaultWilayah).ToString
    End Sub

    Private Sub MstWilayah_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("wilayah", cDefaultWilayah)
    End Sub

    Private Sub MstWilayah_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetSQL()
        InitValue()
        GetEdit(False)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cDefaultWilayah.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cDefaultWilayah.EnterMoveNextControl = True

    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(cKode.Text, "Kode Harus Diisi, Silahkan Mengulangi Pengisian") Then
            ValidSaving = False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(cKeterangan.Text, "Nama Wilayah Harus Diisi, Silahkan Mengulangi Pengisian") Then
            ValidSaving = False
            cKeterangan.Focus()
            Exit Function
        End If
    End Function

    Private Sub DeleteData()
        If MsgBox("Data Benar-benar Dihapus ?", vbYesNo) = vbYes Then
            objData.Delete(GetDSN, "Wilayah", "kode", , cKode.Text)
            GetSQL()
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data benar-benar sudah VALID ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                Dim vaField() As Object = {"kode", "keterangan"}
                Dim vaValue() As Object = {cKode.Text, cKeterangan.Text}
                objData.Update(GetDSN, "Wilayah", String.Format("kode = '{0}'", cKode.Text), vaField, vaValue)
                UpdCfg(eCfg.msKodeDefaultWilayah, cDefaultWilayah.Text)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(sender As Object, e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        Try
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
                If cKode.Text.Trim <> "" Then
                    cKode.Text = Padl(cKode.Text, 2, "0")
                    Dim cDataTable = objData.Browse(GetDSN, "Wilayah", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, , "kode")
                    With cDataTable
                        If .Rows.Count > 0 Then
                            If nPos = myPos.add Then
                                MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                Exit Sub
                            End If
                            GetMemory()
                            If nPos = myPos.delete Then
                                DeleteData()
                            End If
                        Else
                            MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            cKode.Focus()
                        End If
                    End With
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetMemory()
        Try
            Const cField As String = "Kode,Keterangan"
            dtData = objData.Browse(GetDSN, "Wilayah", cField, "kode", , cKode.Text)
            If dtData.Rows.Count > 0 Then
                With dtData.Rows(0)
                    cKode.Text = .Item("Kode").ToString
                    cKeterangan.Text = .Item("Keterangan").ToString
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetDataGrid()
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function GetKeterangan(ByVal cKodeTemp As String, ByVal cTable As String) As String
        Dim cDataTable = objData.Browse(GetDSN, cTable, "keterangan", "kode", data.myOperator.Assign, cKodeTemp)
        If cDataTable.Rows.Count > 0 Then
            GetKeterangan = cDataTable.Rows(0).Item("keterangan").ToString
        Else
            GetKeterangan = ""
        End If
    End Function

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        GetDataGrid()
    End Sub
End Class