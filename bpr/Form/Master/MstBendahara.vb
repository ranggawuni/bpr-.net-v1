﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors

Public Class MstBendahara
    ReadOnly objData As New data()
    Dim nPos As myPos
    Dim lEdit As Boolean

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "Kode,Nama,Alamat,Telepon,HP,Kota,Provinsi,CabangEntry,Instansi,TglAktivasi"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "bendahara", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("bendahara")
            cDataSet.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Trim(cKode.Text) = "" Then
            MessageBox.Show("Kode Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cNama, "Nama Tidak Boleh Kosong.") Then
            Return False
            cNama.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cKode.Text = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelp.Text = ""
        cHP.Text = ""
        cKota.Text = ""
        cProvinsi.Text = ""
        cInstansi.EditValue = ""
        cNamaInstansi.Text = ""
        dTglAktivasi.DateTime = Now.Date
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl2.Enabled = lPar
        GridControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
    End Sub

    Private Sub GetMemory()
        Try
            Const cField As String = "a.*,b.nama as NamaInstansi"
            Dim vaJoin() As Object = {"left join instansi b on b.kode = a.instansi"}
            Using cDataTable = objData.Browse(GetDSN, "bendahara a", cField, "a.kode", data.myOperator.Assign, cKode.Text, , , vaJoin)
                With cDataTable
                    If .Rows.Count > 0 Then
                        cKode.Text = .Rows(0).Item("kode").ToString
                        cNama.Text = .Rows(0).Item("nama").ToString
                        cAlamat.Text = .Rows(0).Item("alamat").ToString
                        cTelp.Text = .Rows(0).Item("telepon").ToString
                        cHP.Text = .Rows(0).Item("hp").ToString
                        cKota.Text = .Rows(0).Item("kota").ToString
                        cProvinsi.Text = .Rows(0).Item("provinsi").ToString
                        cInstansi.Text = .Rows(0).Item("instansi").ToString
                        cNamaInstansi.Text = .Rows(0).Item("cnamainstansi").ToString
                        dTglAktivasi.DateTime = CDate(.Rows(0).Item("tglaktivasi").ToString)
                    End If
                End With
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                objData.Delete(GetDSN, "bendahara", "kode", data.myOperator.Assign, cKode.Text)
                GetData()
                GetEdit(False)
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cWhere As String = String.Format("kode = '{0}'", cKode.Text)
                    Dim vaField() As Object = {"cabangentry", "kode", "nama", "alamat", "telepon", "hp", _
                                               "kota", "provinsi", "instansi", "tglaktivasi"}
                    Dim vaValue() As Object = {"01", cKode.Text, cNama.Text, cAlamat.Text, cTelp.Text, cHP.Text, _
                                               cKota.Text, cProvinsi.Text, cInstansi.Text, CDate(dTglAktivasi.DateTime)}
                    objData.Update(GetDSN, "bendahara", cWhere, vaField, vaValue)
                    GetEdit(False)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles cKode.LostFocus
        Try
            If cKode.Text <> "" Then
                Using cDataTable = objData.Browse(GetDSN, "bendahara", , "kode", data.myOperator.Assign, cKode.Text)
                    If cDataTable.Rows.Count > 0 Then
                        If nPos = myPos.add Then
                            MessageBox.Show("Kode sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                        GetMemory()
                        If nPos = myPos.delete Then
                            DeleteData()
                        End If
                    ElseIf nPos <> myPos.add Then
                        MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cInstansi_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cInstansi.Closed
        GetKeteranganLookUp(cInstansi, cNamaInstansi)
    End Sub

    Private Sub cInstansi_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cInstansi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cInstansi, cNamaInstansi)
        End If
    End Sub

    Private Sub MstBendahara_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetEdit(False)
        GetData()
        GetDataLookup("instansi", cInstansi)
        InitValue()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cNama.TabIndex, n)
        SetTabIndex(cAlamat.TabIndex, n)
        SetTabIndex(cTelp.TabIndex, n)
        SetTabIndex(cHP.TabIndex, n)
        SetTabIndex(cKota.TabIndex, n)
        SetTabIndex(cProvinsi.TabIndex, n)
        SetTabIndex(cInstansi.TabIndex, n)
        SetTabIndex(dTglAktivasi.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cNama.EnterMoveNextControl = True
        cAlamat.EnterMoveNextControl = True
        cTelp.EnterMoveNextControl = True
        cHP.EnterMoveNextControl = True
        cKota.EnterMoveNextControl = True
        cProvinsi.EnterMoveNextControl = True
        cInstansi.EnterMoveNextControl = True
        dTglAktivasi.EnterMoveNextControl = True
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cNama.Text = row(1).ToString
            cAlamat.Text = row(2).ToString
            cTelp.Text = row(3).ToString
            cHP.Text = row(4).ToString
            cKota.Text = row(5).ToString
            cProvinsi.Text = row(6).ToString
            cInstansi.EditValue = row(7).ToString
            Dim cDataTable = objData.Browse(GetDSN, "instansi", "keterangan", "kode", data.myOperator.Assign, cInstansi.Text)
            If cDataTable.Rows.Count > 0 Then
                cNamaInstansi.Text = cDataTable.Rows(0).Item("keterangan").ToString
            End If
            dTglAktivasi.DateTime = CDate(row(8).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class