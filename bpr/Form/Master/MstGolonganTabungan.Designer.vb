﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MstGolonganTabungan
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
            If dtData IsNot Nothing Then
                dtData.Dispose()
                dtData = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MstGolonganTabungan))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.optWajibPajak = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoDapatBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nLamaPasif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.nPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.nAdministrasi = New DevExpress.XtraEditors.TextEdit()
        Me.nPemeliharaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoKenaPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.nAdminPasif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nSetoranMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.cRekeningBunga = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.optWajibPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoDapatBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLamaPasif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoKenaPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAdminPasif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.optWajibPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.nSaldoDapatBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.nLamaPasif)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.nPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.nAdministrasi)
        Me.PanelControl1.Controls.Add(Me.nPemeliharaan)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.nSaldoKenaPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.nAdminPasif)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.nSetoranMinimum)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.nSaldoMinimum)
        Me.PanelControl1.Controls.Add(Me.cRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cNamaRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(758, 228)
        Me.PanelControl1.TabIndex = 13
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(193, 166)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl18.TabIndex = 107
        Me.LabelControl18.Text = "%"
        '
        'optWajibPajak
        '
        Me.optWajibPajak.Location = New System.Drawing.Point(615, 189)
        Me.optWajibPajak.Name = "optWajibPajak"
        Me.optWajibPajak.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optWajibPajak.Size = New System.Drawing.Size(132, 28)
        Me.optWajibPajak.TabIndex = 14
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(491, 114)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl17.TabIndex = 106
        Me.LabelControl17.Text = "Pemeliharaan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(491, 88)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl16.TabIndex = 105
        Me.LabelControl16.Text = "Adm. Tutup Tabungan"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(491, 62)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl10.TabIndex = 104
        Me.LabelControl10.Text = "Saldo Min. Dapat Bunga"
        '
        'nSaldoDapatBunga
        '
        Me.nSaldoDapatBunga.EditValue = "123456"
        Me.nSaldoDapatBunga.Location = New System.Drawing.Point(615, 59)
        Me.nSaldoDapatBunga.Name = "nSaldoDapatBunga"
        Me.nSaldoDapatBunga.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nSaldoDapatBunga.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nSaldoDapatBunga.Properties.MaxLength = 6
        Me.nSaldoDapatBunga.Size = New System.Drawing.Size(132, 20)
        Me.nSaldoDapatBunga.TabIndex = 103
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(193, 196)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl6.TabIndex = 102
        Me.LabelControl6.Text = "Bulan"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(7, 192)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl5.TabIndex = 101
        Me.LabelControl5.Text = "Pasif"
        '
        'nLamaPasif
        '
        Me.nLamaPasif.EditValue = "123456"
        Me.nLamaPasif.Location = New System.Drawing.Point(133, 189)
        Me.nLamaPasif.Name = "nLamaPasif"
        Me.nLamaPasif.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nLamaPasif.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nLamaPasif.Properties.MaxLength = 6
        Me.nLamaPasif.Size = New System.Drawing.Size(54, 20)
        Me.nLamaPasif.TabIndex = 100
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 166)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl1.TabIndex = 99
        Me.LabelControl1.Text = "Pajak Bunga"
        '
        'nPajak
        '
        Me.nPajak.EditValue = "123456"
        Me.nPajak.Location = New System.Drawing.Point(133, 163)
        Me.nPajak.Name = "nPajak"
        Me.nPajak.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nPajak.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nPajak.Properties.MaxLength = 6
        Me.nPajak.Size = New System.Drawing.Size(54, 20)
        Me.nPajak.TabIndex = 98
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(7, 140)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl15.TabIndex = 97
        Me.LabelControl15.Text = "Nama Rek. Biaya Bunga"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(7, 88)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl14.TabIndex = 96
        Me.LabelControl14.Text = "Nama Rekening"
        '
        'nAdministrasi
        '
        Me.nAdministrasi.EditValue = "123456"
        Me.nAdministrasi.Location = New System.Drawing.Point(615, 85)
        Me.nAdministrasi.Name = "nAdministrasi"
        Me.nAdministrasi.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nAdministrasi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nAdministrasi.Properties.MaxLength = 6
        Me.nAdministrasi.Size = New System.Drawing.Size(132, 20)
        Me.nAdministrasi.TabIndex = 95
        '
        'nPemeliharaan
        '
        Me.nPemeliharaan.EditValue = "123456"
        Me.nPemeliharaan.Location = New System.Drawing.Point(615, 111)
        Me.nPemeliharaan.Name = "nPemeliharaan"
        Me.nPemeliharaan.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nPemeliharaan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nPemeliharaan.Properties.MaxLength = 6
        Me.nPemeliharaan.Size = New System.Drawing.Size(132, 20)
        Me.nPemeliharaan.TabIndex = 94
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(491, 196)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl13.TabIndex = 93
        Me.LabelControl13.Text = "Nasabah Wajib Pajak"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(491, 166)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl12.TabIndex = 91
        Me.LabelControl12.Text = "Saldo Min. Kena Pajak"
        '
        'nSaldoKenaPajak
        '
        Me.nSaldoKenaPajak.EditValue = "123456"
        Me.nSaldoKenaPajak.Location = New System.Drawing.Point(615, 163)
        Me.nSaldoKenaPajak.Name = "nSaldoKenaPajak"
        Me.nSaldoKenaPajak.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nSaldoKenaPajak.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nSaldoKenaPajak.Properties.MaxLength = 6
        Me.nSaldoKenaPajak.Size = New System.Drawing.Size(132, 20)
        Me.nSaldoKenaPajak.TabIndex = 90
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(491, 140)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl11.TabIndex = 89
        Me.LabelControl11.Text = "Administrasi Pasif"
        '
        'nAdminPasif
        '
        Me.nAdminPasif.EditValue = "123456"
        Me.nAdminPasif.Location = New System.Drawing.Point(615, 137)
        Me.nAdminPasif.Name = "nAdminPasif"
        Me.nAdminPasif.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nAdminPasif.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nAdminPasif.Properties.MaxLength = 6
        Me.nAdminPasif.Size = New System.Drawing.Size(132, 20)
        Me.nAdminPasif.TabIndex = 88
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(491, 36)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl9.TabIndex = 30
        Me.LabelControl9.Text = "Setoran Minimum"
        '
        'nSetoranMinimum
        '
        Me.nSetoranMinimum.EditValue = "123456"
        Me.nSetoranMinimum.Location = New System.Drawing.Point(615, 33)
        Me.nSetoranMinimum.Name = "nSetoranMinimum"
        Me.nSetoranMinimum.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nSetoranMinimum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nSetoranMinimum.Properties.MaxLength = 6
        Me.nSetoranMinimum.Size = New System.Drawing.Size(132, 20)
        Me.nSetoranMinimum.TabIndex = 29
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(491, 10)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "Saldo Minimum"
        '
        'nSaldoMinimum
        '
        Me.nSaldoMinimum.EditValue = "123456"
        Me.nSaldoMinimum.Location = New System.Drawing.Point(615, 7)
        Me.nSaldoMinimum.Name = "nSaldoMinimum"
        Me.nSaldoMinimum.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nSaldoMinimum.Properties.MaxLength = 6
        Me.nSaldoMinimum.Size = New System.Drawing.Size(132, 20)
        Me.nSaldoMinimum.TabIndex = 27
        '
        'cRekeningBunga
        '
        Me.cRekeningBunga.Location = New System.Drawing.Point(133, 111)
        Me.cRekeningBunga.Name = "cRekeningBunga"
        Me.cRekeningBunga.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningBunga.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningBunga.TabIndex = 21
        '
        'cNamaRekeningBunga
        '
        Me.cNamaRekeningBunga.Enabled = False
        Me.cNamaRekeningBunga.Location = New System.Drawing.Point(133, 137)
        Me.cNamaRekeningBunga.Name = "cNamaRekeningBunga"
        Me.cNamaRekeningBunga.Size = New System.Drawing.Size(266, 20)
        Me.cNamaRekeningBunga.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(7, 114)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Rek. Biaya Bunga"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(133, 59)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(120, 20)
        Me.cRekening.TabIndex = 18
        '
        'cNamaRekening
        '
        Me.cNamaRekening.Enabled = False
        Me.cNamaRekening.Location = New System.Drawing.Point(133, 85)
        Me.cNamaRekening.Name = "cNamaRekening"
        Me.cNamaRekening.Size = New System.Drawing.Size(266, 20)
        Me.cNamaRekening.TabIndex = 17
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 62)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Rekening Perkiraan"
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(133, 33)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(266, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 36)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(133, 7)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(34, 20)
        Me.cKode.TabIndex = 0
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(3, 236)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(758, 200)
        Me.GridControl1.TabIndex = 14
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(3, 442)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(758, 33)
        Me.PanelControl7.TabIndex = 15
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(672, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(591, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(192, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(116, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(40, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'MstGolonganTabungan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 479)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MstGolonganTabungan"
        Me.Text = "Golongan Tabungan"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.optWajibPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoDapatBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLamaPasif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoKenaPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAdminPasif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLamaPasif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAdministrasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPemeliharaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoKenaPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAdminPasif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSetoranMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cRekeningBunga As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoDapatBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optWajibPajak As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
End Class
