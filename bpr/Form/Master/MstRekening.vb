﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System.Data
Imports DevExpress.XtraTab

Public Class mstRekening
    ReadOnly objData As New data()
    Dim nPos As myPos
    Dim lEdit As Boolean

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "kode,keterangan,Jenis,SandiBI"
            Dim cWhere As String = "left(kode,1)='1'"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl1.DataSource = cDataSet.Tables("Rekening")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "kode", , , 50)
            GridColumnFormat(GridView1, "keterangan", , , -2)
            GridColumnFormat(GridView1, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)

            Dim cDataSet2 As New DataSet
            cWhere = "left(kode,1)='2'"
            cDataSet2.Clear()
            cDataSet2 = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl2.DataSource = cDataSet2.Tables("Rekening")
            cDataSet2.Dispose()
            InitGrid(GridView2)
            GridColumnFormat(GridView2, "kode", , , 50)
            GridColumnFormat(GridView2, "keterangan", , , -2)
            GridColumnFormat(GridView2, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView2, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)

            Dim cDataSet3 As New DataSet
            cWhere = "left(kode,1)='3'"
            cDataSet3.Clear()
            cDataSet3 = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl3.DataSource = cDataSet3.Tables("Rekening")
            cDataSet3.Dispose()
            InitGrid(GridView3)
            GridColumnFormat(GridView3, "kode", , , 50)
            GridColumnFormat(GridView3, "keterangan", , , -2)
            GridColumnFormat(GridView3, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView3, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)

            Dim cDataSet4 As New DataSet
            cWhere = "left(kode,1)='4'"
            cDataSet4.Clear()
            cDataSet4 = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl4.DataSource = cDataSet4.Tables("Rekening")
            cDataSet4.Dispose()
            InitGrid(GridView4)
            GridColumnFormat(GridView4, "kode", , , 50)
            GridColumnFormat(GridView4, "keterangan", , , -2)
            GridColumnFormat(GridView4, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView4, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)

            Dim cDataSet5 As New DataSet
            cWhere = "left(kode,1)='5'"
            cDataSet5.Clear()
            cDataSet5 = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl5.DataSource = cDataSet5.Tables("Rekening")
            cDataSet5.Dispose()
            InitGrid(GridView5)
            GridColumnFormat(GridView5, "kode", , , 50)
            GridColumnFormat(GridView5, "keterangan", , , -2)
            GridColumnFormat(GridView5, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView5, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)

            Dim cDataSet6 As New DataSet
            cWhere = "left(kode,1)='6'"
            cDataSet6.Clear()
            cDataSet6 = objData.BrowseDataSet(GetDSN, "Rekening", cField, , , , cWhere, "kode")
            GridControl6.DataSource = cDataSet6.Tables("Rekening")
            cDataSet6.Dispose()
            InitGrid(GridView6)
            GridColumnFormat(GridView6, "kode", , , 50)
            GridColumnFormat(GridView6, "keterangan", , , -2)
            GridColumnFormat(GridView6, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView6, "SandiBI", , , 50, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        Select Case tabControl1.SelectedTabPage.Text.Substring(1, 1).ToString
            Case Is = "1"
                Return ValidDetail(cKode, cKeterangan)
            Case Is = "2"
                Return ValidDetail(cKode1, cKeterangan1)
            Case Is = "3"
                Return ValidDetail(cKode2, cKeterangan2)
            Case Is = "4"
                Return ValidDetail(cKode3, cKeterangan3)
            Case Is = "5"
                Return ValidDetail(cKode4, cKeterangan4)
            Case Is = "6"
                Return ValidDetail(cKode5, cKeterangan5)
            Case Else
                Exit Select
        End Select
    End Function

    Private Function ValidDetail(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit) As Boolean
        Return True
        If Trim(cKodeTemp.Text) = "" Then
            MessageBox.Show("Kode Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cKodeTemp.Focus()
            Exit Function
        End If
        If Not CheckData(cKeteranganTemp, "Keterangan Tidak Boleh Kosong.") Then
            Return False
            cKeteranganTemp.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        optJenis.SelectedIndex = 0
        cSandiBI.Text = ""
        cKode1.Text = ""
        cKeterangan1.Text = ""
        optJenis1.SelectedIndex = 0
        cSandiBI1.Text = ""
        cKode2.Text = ""
        cKeterangan2.Text = ""
        optJenis2.SelectedIndex = 0
        cSandiBI2.Text = ""
        cKode3.Text = ""
        cKeterangan3.Text = ""
        optJenis3.SelectedIndex = 0
        cSandiBI3.Text = ""
        cKode4.Text = ""
        cKeterangan4.Text = ""
        optJenis4.SelectedIndex = 0
        cSandiBI4.Text = ""
        cKode5.Text = ""
        cKeterangan5.Text = ""
        optJenis5.SelectedIndex = 0
        cSandiBI5.Text = ""
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        GridControl1.Enabled = lPar
        PanelControl2.Enabled = lPar
        GridControl2.Enabled = lPar
        PanelControl3.Enabled = lPar
        GridControl3.Enabled = lPar
        PanelControl4.Enabled = lPar
        GridControl4.Enabled = lPar
        PanelControl5.Enabled = lPar
        GridControl5.Enabled = lPar
        PanelControl6.Enabled = lPar
        GridControl6.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
    End Sub

    Private Sub GetMemory(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit, _
                          ByVal optJenisTemp As RadioGroup, ByVal cSandiBITemp As TextEdit)
        Try
            Using cDataTable = objData.Browse(GetDSN, "rekening", , "kode", data.myOperator.Assign, cKodeTemp.Text)
                With cDataTable
                    If .Rows.Count > 0 Then
                        cKodeTemp.Text = .Rows(0).Item("kode").ToString
                        cKeteranganTemp.Text = .Rows(0).Item("keterangan").ToString
                        SetOpt(optJenisTemp, .Rows(0).Item("jenis").ToString)
                        cSandiBITemp.Text = .Rows(0).Item("SandiBI").ToString
                    End If
                End With
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1)
                Select Case cTemp
                    Case Is = "1"
                        HapusData(cKode)
                    Case Is = "2"
                        HapusData(cKode1)
                    Case Is = "3"
                        HapusData(cKode2)
                    Case Is = "4"
                        HapusData(cKode3)
                    Case Is = "5"
                        HapusData(cKode4)
                    Case Is = "6"
                        HapusData(cKode5)
                    Case Else
                        Exit Select
                End Select
                GetData()
                GetEdit(False)
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub HapusData(ByVal cKodeTemp As TextEdit)
        objData.Delete(GetDSN, "rekening", "kode", data.myOperator.Assign, cKodeTemp.Text)
    End Sub

    Shared Sub InitForm(ByVal cForm As Form, Optional ByVal lMaximize As Boolean = False, Optional ByVal lMinimize As Boolean = False, _
                        Optional ByVal cmdBatal As SimpleButton = Nothing)
        With cForm
            .KeyPreview = True
            .MaximizeBox = lMaximize
            .MinimizeBox = lMinimize
            .CancelButton = cmdBatal
        End With
    End Sub

    Private Sub mstCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, , , cmdKeluar)
        GetEdit(False)
        GetData()
        InitValue()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi, cmdPreview)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(cSandiBI.TabIndex, n)
        SetTabIndex(cKode1.TabIndex, n)
        SetTabIndex(cKeterangan1.TabIndex, n)
        SetTabIndex(optJenis1.TabIndex, n)
        SetTabIndex(cSandiBI1.TabIndex, n)
        SetTabIndex(cKode2.TabIndex, n)
        SetTabIndex(cKeterangan2.TabIndex, n)
        SetTabIndex(optJenis2.TabIndex, n)
        SetTabIndex(cSandiBI2.TabIndex, n)
        SetTabIndex(cKode3.TabIndex, n)
        SetTabIndex(cKeterangan3.TabIndex, n)
        SetTabIndex(optJenis3.TabIndex, n)
        SetTabIndex(cKeterangan3.TabIndex, n)
        SetTabIndex(cKode4.TabIndex, n)
        SetTabIndex(cKeterangan4.TabIndex, n)
        SetTabIndex(optJenis4.TabIndex, n)
        SetTabIndex(cSandiBI4.TabIndex, n)
        SetTabIndex(cKode5.TabIndex, n)
        SetTabIndex(cKeterangan5.TabIndex, n)
        SetTabIndex(optJenis5.TabIndex, n)
        SetTabIndex(cSandiBI5.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdPreview.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        optJenis.EnterMoveNextControl = True
        cSandiBI.EnterMoveNextControl = True
        cKode1.EnterMoveNextControl = True
        cKeterangan1.EnterMoveNextControl = True
        optJenis1.EnterMoveNextControl = True
        cSandiBI1.EnterMoveNextControl = True
        cKode2.EnterMoveNextControl = True
        cKeterangan2.EnterMoveNextControl = True
        optJenis2.EnterMoveNextControl = True
        cSandiBI2.EnterMoveNextControl = True
        cKode3.EnterMoveNextControl = True
        cKeterangan3.EnterMoveNextControl = True
        optJenis3.EnterMoveNextControl = True
        cSandiBI3.EnterMoveNextControl = True
        cKode4.EnterMoveNextControl = True
        cKeterangan4.EnterMoveNextControl = True
        optJenis4.EnterMoveNextControl = True
        cSandiBI4.EnterMoveNextControl = True
        cKode5.EnterMoveNextControl = True
        cKeterangan5.EnterMoveNextControl = True
        optJenis5.EnterMoveNextControl = True
        cSandiBI5.EnterMoveNextControl = True

        FormatTextBox(cKode, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "1.999.99.99.99", " ")
        FormatTextBox(cSandiBI, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99", " ")
        FormatTextBox(cKode1, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "2.999.99.99.99", " ")
        FormatTextBox(cSandiBI1, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99")
        FormatTextBox(cKode2, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "3.999.99.99.99", " ")
        FormatTextBox(cSandiBI2, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99")
        FormatTextBox(cKode3, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "4.999.99.99.99", " ")
        FormatTextBox(cSandiBI3, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99")
        FormatTextBox(cKode4, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "5.999.99.99.99", " ")
        FormatTextBox(cSandiBI4, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99")
        FormatTextBox(cKode5, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "6.999.99.99.99", " ")
        FormatTextBox(cSandiBI5, formatType.custom, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.Simple, "999.99")
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
                    Select Case cTemp
                        Case Is = "1"
                            SimpanData(cKode, cKeterangan, optJenis, cSandiBI)
                        Case Is = "2"
                            SimpanData(cKode1, cKeterangan1, optJenis1, cSandiBI1)
                        Case Is = "3"
                            SimpanData(cKode2, cKeterangan2, optJenis2, cSandiBI2)
                        Case Is = "4"
                            SimpanData(cKode3, cKeterangan3, optJenis3, cSandiBI3)
                        Case Is = "5"
                            SimpanData(cKode4, cKeterangan4, optJenis4, cSandiBI4)
                        Case Is = "6"
                            SimpanData(cKode5, cKeterangan5, optJenis5, cSandiBI5)
                        Case Else
                            Exit Select
                    End Select
                    GetEdit(False)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit, _
                           ByVal optJenisTemp As RadioGroup, ByVal cSandiBITemp As TextEdit)
        Dim cJenisTemp As String = GetOpt(optJenisTemp)
        Dim cKodeTemp1 As String = GetInduk(cKodeTemp.Text)
        Dim cKodeBI As String = Trim(GetInduk(cSandiBITemp.Text))
        Dim cWhere As String = String.Format("kode = '{0}'", cKodeTemp1)
        Dim vaField() As Object = {"kode", "keterangan", "jenis", "sandiBI"}
        Dim vaValue() As Object = {cKodeTemp1, cKeteranganTemp.Text, cJenisTemp, cKodeBI}
        objData.Update(GetDSN, "rekening", cWhere, vaField, vaValue)
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            GetDataKode(cKode, cKeterangan, optJenis, cSandiBI)
        End If
    End Sub

    Private Sub GetDataKode(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit, _
                            ByVal optJenisTemp As RadioGroup, ByVal cSandiBITemp As TextEdit)
        Try
            If cKodeTemp.Text <> "" Then
                Using cDataTable = objData.Browse(GetDSN, "rekening", , "kode", data.myOperator.Assign, cKodeTemp.Text)
                    If cDataTable.Rows.Count > 0 Then
                        If nPos = myPos.add Then
                            MessageBox.Show("Kode sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                        GetMemory(cKodeTemp, cKeteranganTemp, optJenisTemp, cSandiBITemp)
                        If nPos = myPos.delete Then
                            DeleteData()
                        End If
                    ElseIf nPos <> myPos.add Then
                        MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub GetDataGrid(ByVal gView As DevExpress.XtraGrid.Views.Grid.GridView, ByVal cKodeTemp As TextEdit, _
                            ByVal cKeteranganTemp As TextEdit, ByVal opt As RadioGroup, ByVal cSandiBITemp As TextEdit)
        Try
            Dim row As DataRow = gView.GetDataRow(gView.FocusedRowHandle)
            cKodeTemp.Text = row(0).ToString
            cKeteranganTemp.Text = row(1).ToString
            SetOpt(opt, row(2).ToString)
            cSandiBITemp.Text = row(3).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        If nPos = myPos.delete Then
            DeleteData()
        End If
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        GetDataGrid(GridView1, cKode, cKeterangan, optJenis, cSandiBI)
    End Sub

    Private Sub GridView2_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView2.FocusedRowChanged
        GetDataGrid(GridView2, cKode1, cKeterangan1, optJenis1, cSandiBI1)
    End Sub

    Private Sub GridView3_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView3.FocusedRowChanged
        GetDataGrid(GridView3, cKode2, cKeterangan2, optJenis2, cSandiBI2)
    End Sub

    Private Sub GridView4_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView4.FocusedRowChanged
        GetDataGrid(GridView4, cKode3, cKeterangan3, optJenis3, cSandiBI3)
    End Sub

    Private Sub GridView5_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView5.FocusedRowChanged
        GetDataGrid(GridView5, cKode4, cKeterangan4, optJenis4, cSandiBI4)
    End Sub

    Private Sub GridView6_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView6.FocusedRowChanged
        GetDataGrid(GridView6, cKode5, cKeterangan5, optJenis5, cSandiBI5)
    End Sub
End Class