﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstGolonganDeposito
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String
    Dim dtData As New DataTable
    Dim dtRekening As New DataTable

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cRekening.EditValue = ""
        cNamaRekening.Text = ""
        cRekeningBunga.EditValue = ""
        cNamaRekeningBunga.Text = ""
        cRekeningPajakBunga.EditValue = ""
        cNamaRekeningPajakBunga.Text = ""
        cRekeningJatuhTempo.EditValue = ""
        cNamaRekeningJatuhTempo.Text = ""
        cRekeningPinalti.EditValue = ""
        cNamaRekeningPinalti.Text = ""
        cRekeningAntarBankPasiva.EditValue = ""
        cNamaRekeningAntarBankPasiva.Text = ""
        cRekeningPajakBMHD.EditValue = ""
        cNamaRekeningPajakBMHD.Text = ""
        cRekeningAccrual.EditValue = ""
        cNamaRekeningAccrual.Text = ""
        nPajak.Text = "0"
        optWajibPajak.SelectedIndex = 0
    End Sub

    Private Sub InitLookUp()
        GetDataLookup("rekening", cRekening)
        GetDataLookup("rekening", cRekeningBunga)
        GetDataLookup("rekening", cRekeningPajakBunga)
        GetDataLookup("rekening", cRekeningJatuhTempo)
        GetDataLookup("rekening", cRekeningPinalti)
        GetDataLookup("rekening", cRekeningAntarBankPasiva)
        GetDataLookup("rekening", cRekeningPajakBMHD)
        GetDataLookup("rekening", cRekeningAccrual)
    End Sub

    Private Sub SetingTabIndex()
        Dim n As Integer
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cRekeningBunga.TabIndex, n)
        SetTabIndex(cRekeningPajakBunga.TabIndex, n)
        SetTabIndex(cRekeningJatuhTempo.TabIndex, n)
        SetTabIndex(cRekeningPinalti.TabIndex, n)
        SetTabIndex(cRekeningAntarBankPasiva.TabIndex, n)
        SetTabIndex(cRekeningPajakBMHD.TabIndex, n)
        SetTabIndex(cRekeningAccrual.TabIndex, n)
        SetTabIndex(nPajak.TabIndex, n)
        SetTabIndex(optWajibPajak.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cRekeningBunga.EnterMoveNextControl = True
        cRekeningPajakBunga.EnterMoveNextControl = True
        cRekeningJatuhTempo.EnterMoveNextControl = True
        cRekeningPinalti.EnterMoveNextControl = True
        cRekeningAntarBankPasiva.EnterMoveNextControl = True
        cRekeningPajakBMHD.EnterMoveNextControl = True
        cRekeningAccrual.EnterMoveNextControl = True
        nPajak.EnterMoveNextControl = True
        optWajibPajak.EnterMoveNextControl = True

        FormatTextBox(nPajak, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Akan Dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            objData.Delete(GetDSN, "golongandeposito", "kode", data.myOperator.Assign, cKode.Text, cWhere)
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Not CheckData(cKode, "Kode Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cKeterangan, "Nama Bank Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKeterangan.Focus()
            Exit Function
        End If
        If Not CheckData(cRekening, "Rekening Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim cWajibPajak As String = GetOpt(optWajibPajak)
                Dim vaField() As Object = {"Kode", "Keterangan", _
                                           "Rekeningakuntansi", "RekeningBunga", "RekeningPajakBunga", _
                                           "PajakBunga", "RekeningJatuhTempo", "WajibPajak", _
                                           "RekeningPinalti", "RekeningAntarBankPasiva", "RekeningPajakRRP", _
                                           "RekeningAccrual"}
                Dim vaValue() As Object = {cKode.Text, cKeterangan.Text, _
                                           cRekening.Text, cRekeningBunga.Text, cRekeningPajakBunga.Text, _
                                           Val(nPajak.Text), cRekeningJatuhTempo.Text, cWajibPajak, _
                                           cRekeningPinalti.Text, cRekeningAntarBankPasiva.Text, cRekeningPajakBMHD.Text, _
                                           cRekeningAccrual.Text}
                Dim cWhere As String = String.Format("kode = '{0}' and status = '{1}'", cKode.Text, cStatus)
                objData.Update(GetDSN, "golongandeposito", cWhere, vaField, vaValue)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Function GetNamaRekening(cRekening As String) As String
        Dim nBaris As DataRow() = dtRekening.Select(String.Format("kode = {0} ", cRekening))
        If nBaris.Count > 0 Then
            GetNamaRekening = nBaris(0).Item("rekening").ToString
        Else
            GetNamaRekening = ""
        End If
    End Function

    Private Sub GetMemory()
        dtData = objData.Browse(GetDSN, "GolonganDeposito", , "Kode", , cKode.Text)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cKode.Text = .Item("Kode").ToString
                cKeterangan.Text = .Item("Keterangan").ToString
                cRekening.Text = .Item("RekeningAkuntansi").ToString
                cNamaRekening.Text = GetNamaRekening(cRekening.Text)
                cRekeningBunga.Text = .Item("RekeningBunga").ToString
                cNamaRekeningBunga.Text = GetNamaRekening(cRekeningBunga.Text)
                cRekeningPajakBunga.Text = .Item("RekeningPajakBunga").ToString
                cNamaRekeningPajakBunga.Text = GetNamaRekening(cRekeningPajakBunga.Text)
                cRekeningJatuhTempo.Text = .Item("rekeningJatuhTempo").ToString
                cNamaRekeningJatuhTempo.Text = GetNamaRekening(cRekeningJatuhTempo.Text)
                cRekeningPinalti.Text = .Item("RekeningPinalti").ToString
                cNamaRekeningPinalti.Text = GetNamaRekening(cRekeningPinalti.Text)
                cRekeningAntarBankPasiva.Text = .Item("RekeningAntarBankPasiva").ToString
                cNamaRekeningAntarBankPasiva.Text = GetNamaRekening(cRekeningAntarBankPasiva.Text)
                cRekeningPajakBMHD.Text = .Item("RekeningPajakRRP").ToString
                cNamaRekeningPajakBMHD.Text = GetNamaRekening(cRekeningPajakBMHD.Text)
                cRekeningAccrual.Text = .Item("RekeningAccrual").ToString
                cNamaRekeningAccrual.Text = GetNamaRekening(cRekeningAccrual.Text)
                nPajak.Text = .Item("PajakBunga").ToString
                SetOpt(optWajibPajak, .Item("WajibPajak").ToString)
            End With
        End If
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Dim cField As String = "Kode,Keterangan,RekeningAkuntansi,RekeningBunga,RekeningPajakBunga,RekeningJatuhTempo,"
            cField = cField & "RekeningPinalti,RekeningAntarBankPasiva,RekeningPajakRRP,RekeningAccrual,PajakBunga,WajibPajak"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "golongandeposito", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("golongandeposito")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 50)
            GridColumnFormat(GridView1, "Keterangan")
            GridColumnFormat(GridView1, "Rekening", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningBunga", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningPajakBunga", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningJatuhTempo", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningPinalti", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningAntarBankPasiva", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningPajakRRP", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningAccrual", , , 80, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "PajakBunga", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "WajibPajak", , , 50, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetDataGrid()
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
            cRekening.EditValue = row(2).ToString
            cNamaRekening.Text = GetNamaRekening(cRekening.Text)
            cRekeningBunga.EditValue = row(3).ToString
            cNamaRekeningBunga.Text = GetNamaRekening(cRekeningBunga.Text)
            cRekeningPajakBunga.Text = row(4).ToString
            cNamaRekeningPajakBunga.Text = GetNamaRekening(cRekeningPajakBunga.Text)
            cRekeningJatuhTempo.Text = row(5).ToString
            cNamaRekeningJatuhTempo.Text = GetNamaRekening(cRekeningJatuhTempo.Text)
            cRekeningPinalti.Text = row(6).ToString
            cNamaRekeningPinalti.Text = GetNamaRekening(cRekeningPinalti.Text)
            cRekeningAntarBankPasiva.Text = row(7).ToString
            cNamaRekeningAntarBankPasiva.Text = GetNamaRekening(cRekeningAntarBankPasiva.Text)
            cRekeningPajakBMHD.Text = row(8).ToString
            cNamaRekeningPajakBMHD.Text = GetNamaRekening(cRekeningPajakBMHD.Text)
            cRekeningAccrual.Text = row(9).ToString
            cNamaRekeningAccrual.Text = GetNamaRekening(cRekeningAccrual.Text)
            nPajak.Text = row(10).ToString
            SetOpt(optWajibPajak, row(11).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            Dim cDataTable = objData.Browse(GetDSN, "golongantabungan", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, cWhere, "kode")
            With cDataTable
                If .Rows.Count > 0 Then
                    If nPos = myPos.add Then
                        MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                Else
                    MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        GetKeteranganLookUp(cRekening, cNamaRekening)
    End Sub

    Private Sub cRekeningBunga_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningBunga.Closed
        GetKeteranganLookUp(cRekeningBunga, cNamaRekeningBunga)
    End Sub

    Private Sub MstGolonganDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        CenterFormManual(Me)
        dtRekening = objData.Browse(GetDSN, "rekening", "Kode,Keterangan", , , , , "Kode")

        InitValue()
        InitLookUp()
        SetingTabIndex()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        GetSQL()
    End Sub

    Private Sub GridControl1_EditorKeyDown(sender As Object, e As KeyEventArgs) Handles GridControl1.EditorKeyDown
        GetDataGrid()
    End Sub

    Private Sub cRekeningAccrual_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAccrual.Closed
        GetKeteranganLookUp(cRekeningAccrual, cNamaRekeningAccrual)
    End Sub

    Private Sub cRekeningAntarBankPasiva_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAntarBankPasiva.Closed
        GetKeteranganLookUp(cRekeningAntarBankPasiva, cNamaRekeningAntarBankPasiva)
    End Sub

    Private Sub cRekeningJatuhTempo_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningJatuhTempo.Closed
        GetKeteranganLookUp(cRekeningJatuhTempo, cNamaRekeningJatuhTempo)
    End Sub

    Private Sub cRekeningPajakBMHD_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningPajakBMHD.Closed
        GetKeteranganLookUp(cRekeningPajakBMHD, cNamaRekeningPajakBMHD)
    End Sub

    Private Sub cRekeningPajakBunga_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningPajakBunga.Closed
        GetKeteranganLookUp(cRekeningPajakBunga, cNamaRekeningPajakBunga)
    End Sub

    Private Sub cRekeningPinalti_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningPinalti.Closed
        GetKeteranganLookUp(cRekeningPinalti, cNamaRekeningPinalti)
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        GetDataGrid()
    End Sub
End Class