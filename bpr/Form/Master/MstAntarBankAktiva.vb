﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstAntarBankAktiva
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cRekening.EditValue = ""
        cNamaRekening.Text = ""
        cJenisBank.EditValue = ""
        cNamaJenisBank.Text = ""
        cSandiBank.Text = ""
        cJenis.Text = ""
        cLokasi.EditValue = ""
        cNamaLokasi.Text = ""
        cKeterkaitan.Text = ""
        cHari.Text = ""
        cBulan.Text = ""
        cTahun.Text = ""
        dTglMulai.DateTime = Date.Today
        cTglAkhir.Text = ""
        cKualitas.Text = ""
        cSukuBunga.Text = ""
        cAlamat.Text = ""
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Akan Dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            objData.Delete(GetDSN, "bank", "kode", data.myOperator.Assign, cKode.Text, cWhere)
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Not CheckData(cKode, "Kode Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cKeterangan, "Nama Bank Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKeterangan.Focus()
            Exit Function
        End If
        If Not CheckData(cRekening, "Rekening Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim vaField() As Object = {"kode", "keterangan", "rekening", "sandibank", "jenis", _
                                           "terkait", "jw1", "jw2", "jw3", "kualitas", "sukubunga", _
                                           "status", "tglmulai", "tglakhir", "jenisbank", "lokasibank", _
                                           "alamat"}
                Dim vaValue() As Object = {cKode.Text, cKeterangan.Text, cRekening.Text, cSandiBank.Text, cJenis.Text, _
                                           cKeterkaitan.Text, cHari.Text, cBulan.Text, cTahun.Text, cKualitas.Text, cSukuBunga.Text, _
                                           cStatus, CDate(dTglMulai.DateTime), cTglAkhir.Text, cJenisBank.Text, cLokasi.Text, _
                                           cLokasi.Text}
                Dim cWhere As String = String.Format("kode = '{0}' and status = '{1}'", cKode.Text, cStatus)
                objData.Update(GetDSN, "bank", cWhere, vaField, vaValue)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub GetMemory()
        Dim cField As String = "b.*,r.keterangan as KeteranganRekening,w.keterangan as KeteranganWilayah,"
        cField = cField & "j.keterangan as KeteranganJenisBank"
        Dim cWhere As String = String.Format("and b.status = '{0}'", cStatus)
        Dim vaJoin() As Object = {"Left Join Rekening r on r.kode = b.rekening", _
                                  "Left Join Wilayah w on w.kode = b.wilayah", _
                                  "Left Join JenisBank j on j.kode = b.JenisBank"}
        Dim cDataTable = objData.Browse(GetDSN, "bank b", cField, "b.kode", data.myOperator.Assign, cKode.Text, cWhere, , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cKeterangan.Text = .Item("Keterangan").ToString
                cRekening.Text = .Item("Rekening").ToString
                cNamaRekening.Text = .Item("KeteranganRekening").ToString
                cJenisBank.Text = .Item("JenisBank").ToString
                cNamaJenisBank.Text = .Item("KeteranganJenisBank").ToString
                cSandiBank.Text = .Item("SandiBank").ToString
                cJenis.Text = .Item("Jenis").ToString
                cKeterkaitan.Text = .Item("Terkait").ToString
                cHari.Text = .Item("jw1").ToString
                cBulan.Text = .Item("jw2").ToString
                cTahun.Text = .Item("jw3").ToString
                cKualitas.Text = .Item("Kualitas").ToString
                cSukuBunga.Text = .Item("SukuBunga").ToString
                dTglMulai.DateTime = CDate(.Item("TglMulai").ToString)
                cTglAkhir.Text = .Item("TglAkhir").ToString
                cLokasi.Text = .Item("LokasiBank").ToString
                cNamaLokasi.Text = .Item("KeteranganWilayah").ToString
                cAlamat.Text = .Item("Alamat").ToString
            End With
        End If
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Dim cField As String = "Kode,Keterangan,Rekening,JenisBank,SandiBank,Jenis,LokasiBank,Terkait,jw1,jw2,jw3,TglMulai,TglAkhir,"
            cField = cField & "Kualitas,SukuBunga,Alamat"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "bank", cField, "status", data.myOperator.Assign, cStatus, , "kode")
            GridControl1.DataSource = cDataSet.Tables("bank")
            cDataSet.Dispose()
            InitGrid(GridView1, False)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 50)
            GridColumnFormat(GridView1, "Keterangan")
            GridColumnFormat(GridView1, "Rekening", , , 80)
            GridColumnFormat(GridView1, "JenisBank", , , 60, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "SandiBank", , , 90, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "Jenis", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "LokasiBank", , , 50, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "Terkait", , , 60, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "jw3", , , 50)
            GridColumnFormat(GridView1, "jw2", , , 50)
            GridColumnFormat(GridView1, "jw1", , , 50)
            GridColumnFormat(GridView1, "TglMulai", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "TglAkhir", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "Kualitas", , , 50, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "SukuBunga", , , 60, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Alamat", , , -2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MstAntarBankAktiva_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("rekening", cRekening)
        GetDataLookup("jenisbank", cJenisBank)
        GetDataLookup("wilayah", cLokasi)
    End Sub

    Private Sub MstAntarBankAktiva_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        getsql()
        InitValue()
        GetEdit(False)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cJenisBank.TabIndex, n)
        SetTabIndex(cSandiBank.TabIndex, n)
        SetTabIndex(cJenis.TabIndex, n)
        SetTabIndex(cLokasi.TabIndex, n)
        SetTabIndex(cKeterkaitan.TabIndex, n)
        SetTabIndex(cHari.TabIndex, n)
        SetTabIndex(cBulan.TabIndex, n)
        SetTabIndex(cTahun.TabIndex, n)
        SetTabIndex(dTglMulai.TabIndex, n)
        SetTabIndex(cTglAkhir.TabIndex, n)
        SetTabIndex(cKualitas.TabIndex, n)
        SetTabIndex(cSukuBunga.TabIndex, n)
        SetTabIndex(cAlamat.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cJenisBank.EnterMoveNextControl = True
        cSandiBank.EnterMoveNextControl = True
        cJenis.EnterMoveNextControl = True
        cLokasi.EnterMoveNextControl = True
        cKeterkaitan.EnterMoveNextControl = True
        cHari.EnterMoveNextControl = True
        cBulan.EnterMoveNextControl = True
        cTahun.EnterMoveNextControl = True
        dTglMulai.EnterMoveNextControl = True
        cTglAkhir.EnterMoveNextControl = True
        cKualitas.EnterMoveNextControl = True
        cSukuBunga.EnterMoveNextControl = True
        cAlamat.EnterMoveNextControl = True

        FormatTextBox(cTglAkhir, formatType.dd_MM_yyyy, DevExpress.Utils.HorzAlignment.Default, Mask.MaskType.DateTime, "99-99-9999")
    End Sub

    Sub GetStatus(ByVal cStat As String)
        cStatus = cStat
        If cStatus = "1" Then
            Text = "Antar Bank Aktiva"
        Else
            Text = "Antar Bank Pasiva"
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            Dim cDataTable = objData.Browse(GetDSN, "bank", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, cWhere, "kode")
            With cDataTable
                If .Rows.Count > 0 Then
                    If nPos = myPos.add Then
                        MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                Else
                    MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        GetKeteranganLookUp(cRekening, cNamaRekening)
    End Sub

    Private Sub cJenisBank_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cJenisBank.Closed
        GetKeteranganLookUp(cJenisBank, cNamaJenisBank)
    End Sub

    Private Sub cLokasi_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cLokasi.Closed
        GetKeteranganLookUp(cLokasi, cNamaLokasi)
    End Sub

    Private Function GetKeterangan(ByVal cKodeTemp As String, ByVal cTable As String) As String
        Dim cDataTable = objData.Browse(GetDSN, cTable, "keterangan", "kode", data.myOperator.Assign, cKodeTemp)
        If cDataTable.Rows.Count > 0 Then
            GetKeterangan = cDataTable.Rows(0).Item("keterangan").ToString
        Else
            GetKeterangan = ""
        End If
    End Function

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
            cRekening.EditValue = row(2).ToString
            cNamaRekening.Text = GetKeterangan(cRekening.Text, "rekening")
            cJenisBank.EditValue = row(3).ToString
            cNamaJenisBank.Text = GetKeterangan(cJenisBank.Text, "jenisbank")
            cSandiBank.Text = row(4).ToString
            cJenis.Text = row(5).ToString
            cLokasi.EditValue = row(6).ToString
            cNamaLokasi.Text = GetKeterangan(cLokasi.Text, "wilayah")
            cKeterkaitan.Text = row(7).ToString
            cHari.Text = row(8).ToString
            cBulan.Text = row(9).ToString
            cTahun.Text = row(10).ToString
            dTglMulai.DateTime = CDate(row(11).ToString)
            cTglAkhir.Text = row(12).ToString
            cKualitas.Text = row(13).ToString
            cSukuBunga.Text = row(14).ToString
            cAlamat.Text = row(15).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class