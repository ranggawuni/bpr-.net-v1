﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors

Public Class MstSuratPerjanjian
    ReadOnly objData As New data()
    Dim nPos As myPos
    Dim lEdit As Boolean

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "Kode,File"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "SuratPerjanjian", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("SuratPerjanjian")
            cDataSet.Dispose()
            InitGrid(GridView1, False)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 10, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "Keterangan")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Trim(cKode.Text) = "" Then
            MessageBox.Show("Kode Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cFile, "Nama File Tidak Boleh Kosong.") Then
            Return False
            cFile.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cKode.Text = ""
        cFile.Text = ""
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl2.Enabled = lPar
        'GridControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
    End Sub

    Private Sub GetMemory()
        Try
            Using cDataTable = objData.Browse(GetDSN, "SuratPerjanjian", , "kode", data.myOperator.Assign, cKode.Text)
                With cDataTable
                    If .Rows.Count > 0 Then
                        cKode.Text = .Rows(0).Item("kode").ToString
                        cFile.Text = .Rows(0).Item("file").ToString
                    End If
                End With
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                objData.Delete(GetDSN, "SuratPerjanjian", "kode", data.myOperator.Assign, cKode.Text)
                GetData()
                GetEdit(False)
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cWhere As String = String.Format("kode = '{0}'", cKode.Text)
                    Dim vaField() As Object = {"kode", "file"}
                    Dim vaValue() As Object = {cKode.Text, cFile.Text}
                    objData.Update(GetDSN, "SuratPerjanjian", cWhere, vaField, vaValue)
                    GetEdit(False)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Try
                If cKode.Text <> "" Then
                    Using cDataTable = objData.Browse(GetDSN, "SuratPerjanjian", , "kode", data.myOperator.Assign, cKode.Text)
                        If cDataTable.Rows.Count > 0 Then
                            If nPos = myPos.add Then
                                MessageBox.Show("Kode sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Exit Sub
                            End If
                            GetMemory()
                            If nPos = myPos.delete Then
                                DeleteData()
                            End If
                        ElseIf nPos <> myPos.add Then
                            MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                    End Using
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If
    End Sub

    Private Sub OpenFile()
        Using openDialog1 As New OpenFileDialog()
            With openDialog1
                .InitialDirectory = "c:\"
                .Filter = "Microsoft Word (*.docx)|*.docx|(*.doc)|*.doc"
                .FilterIndex = 2
                .RestoreDirectory = True
                If .ShowDialog = Windows.Forms.DialogResult.OK Then
                    cFile.Text = .FileName
                End If
            End With
        End Using
    End Sub

    Private Sub cFile_ButtonClick(sender As Object, e As ButtonPressedEventArgs) Handles cFile.ButtonClick
        OpenFile()
    End Sub

    Private Sub cFile_ButtonPressed(sender As Object, e As ButtonPressedEventArgs) Handles cFile.ButtonPressed
        OpenFile()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cFile.Text = row(1).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class