﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class cfgTabungan
    ReadOnly objData As New data
    Dim dtData As New DataTable

    Private Sub InitValue()
        cPenarikanPB.EditValue = ""
        cNamaPenarikanPB.Text = ""
        cSetoranPB.EditValue = ""
        cNamaSetoranPB.Text = ""
        cBunga.EditValue = ""
        cNamaBunga.Text = ""
        cPajak.EditValue = ""
        cNamaPajak.Text = ""
        cPenutupan.EditValue = ""
        cNamaPenutupan.Text = ""
        cAdmPenutupan.EditValue = ""
        cNamaAdmPenutupan.Text = ""
        cAdmPemeliharaan.EditValue = ""
        cNamaAdmPemeliharaan.Text = ""
        cPenarikanTunai.EditValue = ""
        cNamaPenarikanTunai.Text = ""
        cSetoranTunai.EditValue = ""
        cNamaSetoranTunai.Text = ""
    End Sub

    Private Sub SimpanData()
        If MessageBox.Show("Data sudah valid?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            UpdCfg(eCfg.msKodePenarikanPemindahBukuan, cPenarikanPB.EditValue)
            UpdCfg(eCfg.msKodePenyetoranPemindahbukuan, cSetoranPB.EditValue)
            UpdCfg(eCfg.msKodeBunga, cBunga.EditValue)
            UpdCfg(eCfg.msKodePajakBunga, cPajak.EditValue)
            UpdCfg(eCfg.msKodePenutupanTabungan, cPenutupan.EditValue)
            UpdCfg(eCfg.msKodeAdministrasi, cAdmPenutupan.EditValue)
            UpdCfg(eCfg.msKodeAdmPemeliharaan, cAdmPemeliharaan.EditValue)
            UpdCfg(eCfg.msKodePenarikanTunai, cPenarikanTunai.EditValue)
            UpdCfg(eCfg.msKodeSetoranTunai, cSetoranTunai.EditValue)
            MessageBox.Show("Data Sudah Disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub GetData()
        cPenarikanPB.EditValue = aCfg(eCfg.msKodePenarikanPemindahBukuan)
        cNamaPenarikanPB.Text = GetKeterangan(cPenarikanPB.EditValue.ToString)
        cSetoranPB.EditValue = aCfg(eCfg.msKodePenyetoranPemindahbukuan)
        cNamaSetoranPB.Text = GetKeterangan(cSetoranPB.EditValue.ToString)
        cBunga.EditValue = aCfg(eCfg.msKodeBunga)
        cNamaBunga.Text = GetKeterangan(cBunga.EditValue.ToString)
        cPajak.EditValue = aCfg(eCfg.msKodePajakBunga)
        cNamaPajak.Text = GetKeterangan(cPajak.EditValue.ToString)
        cPenutupan.EditValue = aCfg(eCfg.msKodePenutupanTabungan)
        cNamaPenutupan.Text = GetKeterangan(cPenutupan.EditValue.ToString)
        cAdmPenutupan.EditValue = aCfg(eCfg.msKodeAdministrasi)
        cNamaAdmPenutupan.Text = GetKeterangan(cAdmPenutupan.EditValue.ToString)
        cAdmPemeliharaan.EditValue = aCfg(eCfg.msKodeAdmPemeliharaan)
        cNamaAdmPemeliharaan.Text = GetKeterangan(cAdmPemeliharaan.EditValue.ToString)
        cSetoranTunai.EditValue = aCfg(eCfg.msKodeSetoranTunai)
        cNamaSetoranTunai.Text = GetKeterangan(cSetoranTunai.EditValue.ToString)
        cPenarikanTunai.EditValue = aCfg(eCfg.msKodePenarikanTunai)
        cNamaPenarikanPB.Text = GetKeterangan(cPenarikanTunai.EditValue.ToString)
    End Sub

    Private Function GetKeterangan(ByVal cKodeTransaksi As String) As String
        dtData = objData.Browse(GetDSN, "KodeTransaksi", "Keterangan", "Kode", , cKodeTransaksi)
        If dtData.Rows.Count > 0 Then
            GetKeterangan = dtData.Rows(0).Item("Keterangan").ToString
        Else
            GetKeterangan = ""
        End If
    End Function

    Private Sub InitLookUp()
        GetDataLookup("KodeTransaksi", cPenarikanPB)
        GetDataLookup("KodeTransaksi", cSetoranPB)
        GetDataLookup("KodeTransaksi", cBunga)
        GetDataLookup("KodeTransaksi", cPajak)
        GetDataLookup("KodeTransaksi", cPenutupan)
        GetDataLookup("KodeTransaksi", cAdmPenutupan)
        GetDataLookup("KodeTransaksi", cAdmPemeliharaan)
        GetDataLookup("KodeTransaksi", cPenarikanTunai)
        GetDataLookup("KodeTransaksi", cSetoranTunai)
    End Sub

    Private Sub SetingTabIndex()
        Dim n As Integer

        SetTabIndex(cPenarikanPB.TabIndex, n)
        SetTabIndex(cSetoranPB.TabIndex, n)
        SetTabIndex(cBunga.TabIndex, n)
        SetTabIndex(cPajak.TabIndex, n)
        SetTabIndex(cPenutupan.TabIndex, n)
        SetTabIndex(cAdmPenutupan.TabIndex, n)
        SetTabIndex(cAdmPemeliharaan.TabIndex, n)
        SetTabIndex(cPenarikanTunai.TabIndex, n)
        SetTabIndex(cSetoranTunai.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cPenarikanPB.EnterMoveNextControl = True
        cSetoranPB.EnterMoveNextControl = True
        cBunga.EnterMoveNextControl = True
        cPajak.EnterMoveNextControl = True
        cPenutupan.EnterMoveNextControl = True
        cAdmPenutupan.EnterMoveNextControl = True
        cAdmPemeliharaan.EnterMoveNextControl = True
        cPenarikanTunai.EnterMoveNextControl = True
        cSetoranTunai.EnterMoveNextControl = True
    End Sub

    Private Sub cfgTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        CenterFormManual(Me)
        InitValue()
        InitLookUp()
        SetingTabIndex()
        GetData()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cAdmPemeliharaan_Closed(sender As Object, e As ClosedEventArgs) Handles cAdmPemeliharaan.Closed
        GetKeteranganLookUp(cAdmPemeliharaan, cNamaAdmPemeliharaan)
    End Sub

    Private Sub cAdmPenutupan_Closed(sender As Object, e As ClosedEventArgs) Handles cAdmPenutupan.Closed
        GetKeteranganLookUp(cAdmPenutupan, cNamaAdmPenutupan)
    End Sub

    Private Sub cBunga_Click(sender As Object, e As EventArgs) Handles cBunga.Click
        GetKeteranganLookUp(cBunga, cNamaBunga)
    End Sub

    Private Sub cPajak_Click(sender As Object, e As EventArgs) Handles cPajak.Click
        GetKeteranganLookUp(cPajak, cNamaPajak)
    End Sub

    Private Sub cPenarikanPB_Click(sender As Object, e As EventArgs) Handles cPenarikanPB.Click
        GetKeteranganLookUp(cPenarikanPB, cNamaPenarikanPB)
    End Sub

    Private Sub cPenarikanTunai_Click(sender As Object, e As EventArgs) Handles cPenarikanTunai.Click
        GetKeteranganLookUp(cPenarikanTunai, cNamaPenarikanTunai)
    End Sub

    Private Sub cPenutupan_Closed(sender As Object, e As ClosedEventArgs) Handles cPenutupan.Closed
        GetKeteranganLookUp(cPenutupan, cNamaPenutupan)
    End Sub

    Private Sub cSetoranPB_Click(sender As Object, e As EventArgs) Handles cSetoranPB.Click
        GetKeteranganLookUp(cSetoranPB, cNamaSetoranPB)
    End Sub

    Private Sub cSetoranTunai_Click(sender As Object, e As EventArgs) Handles cSetoranTunai.Click
        GetKeteranganLookUp(cSetoranTunai, cNamaSetoranTunai)
    End Sub
End Class