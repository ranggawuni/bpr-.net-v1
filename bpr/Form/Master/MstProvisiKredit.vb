﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstProvisiKredit
    ReadOnly objData As New data
    Dim dtData As New DataTable
    Dim dtArray As New DataTable

    Private Sub InitTable()
        AddColumn(dtArray, "dTgl", System.Type.GetType("System.Date"), True)
        AddColumn(dtArray, "nLama", System.Type.GetType("System.Double"), True)
        AddColumn(dtArray, "nPersentase", System.Type.GetType("System.Double"), True)
    End Sub

    Private Sub InitValue()
        cKode.EditValue = ""
        cKeterangan.Text = ""
        dTgl.DateTime = Now.Date
        nLama.Text = "0"
        nPersentase.Text = "0"
    End Sub

    Private Sub MstProvisiKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , cmdHapus, cmdSimpan, cmdKeluar, , , cmdOK)
        InitValue()
        InitTable()
        GetDataLookup("golongankredit", cKode)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nPersentase.TabIndex, n)
        SetTabIndex(cmdOK.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cKode.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nPersentase.EnterMoveNextControl = True

        FormatTextBox(nLama, formatType.BilRpPict, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nPersentase, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub GetSQL()
        Dim n As Integer
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "tgl,lama,persen"
            Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "detailprovisi", cField, "kode", , cKode.Text, cWhere, "tgl desc")
            GridControl1.DataSource = cDataSet.Tables("detailprovisi")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Tgl", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, 50)
            GridColumnFormat(GridView1, "Lama", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "persen", , , 50, DevExpress.Utils.HorzAlignment.Far)

            dtData = objData.Browse(GetDSN, "detailprovisi", cField, "kode", , cKode.Text, cWhere, "tgl desc")
            For n = 0 To dtData.Rows.Count - 1
                With dtData.Rows(n)
                    dtArray.Rows.Add(New Object() {.Item("tgl"), .Item("lama"), .Item("persen")})
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_Closed(sender As Object, e As ClosedEventArgs) Handles cKode.Closed
        GetKeteranganLookUp(cKode, cKeterangan)
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetSQL()
        End If
    End Sub

    Private Sub cKode_LostFocus(sender As Object, e As EventArgs) Handles cKode.LostFocus
        GetSQL()
    End Sub

    Private Sub HapusData()
        If GridView1.RowCount >= 1 Then
            If MessageBox.Show("Data Benar-benar Dihapus ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                Dim cWhere As String = String.Format("and tgl='{0}' and persen = {1} ", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd), Val(nPersentase.Text))
                cWhere = String.Format("{0} and lama = {1}", cWhere, Val(nLama.Text))
                objData.Delete(GetDSN, "detailprovisi", "kode", , cKode.Text, cWhere)
                GetSQL()
                dTgl.DateTime = Now.Date
                nLama.Text = "0"
                nPersentase.Text = "0"
            End If
        End If
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        HapusData()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        dtArray.Rows.Add(New Object() {dTgl.DateTime.Date, Val(nLama.Text), Val(nPersentase.Text)})
        nLama.Focus()
    End Sub

    Private Sub SimpanData()
        Dim n As Integer
        Dim cWhere As String = String.Format(" and Tgl = '{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
        objData.Delete(GetDSN, "detailprovisi", "Kode", , cKode.Text, cWhere)

        Dim vaField() As Object = {"Kode", "Tgl", "lama", "Persen"}
        Dim vaValue() As Object
        For n = 0 To dtArray.Rows.Count - 1
            With dtArray.Rows(n)
                vaValue = {cKode.Text, .Item(0), .Item(1), .Item(2), .Item(3)}
                objData.Add(GetDSN, "detailprovisi", vaField, vaValue)
            End With
        Next
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            dTgl.DateTime = CDate(row(0).ToString)
            nLama.Text = row(1).ToString
            nPersentase.Text = row(2).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class