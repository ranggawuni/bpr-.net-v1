﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MstDaerah
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
            If dtKecamatan IsNot Nothing Then
                dtKecamatan.Dispose()
                dtKecamatan = Nothing
            End If
            If dtKelurahan IsNot Nothing Then
                dtKelurahan.Dispose()
                dtKelurahan = Nothing
            End If
            If dtKota IsNot Nothing Then
                dtKota.Dispose()
                dtKota = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MstDaerah))
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem5 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem6 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdPreview = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cKodePos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cKecamatan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKecamatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cKota1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKota1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode2 = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cKota = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKota = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode1 = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.tabControl1 = New DevExpress.XtraTab.XtraTabControl()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cKodePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKecamatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKecamatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKota1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKota1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdPreview)
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(2, 362)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(650, 33)
        Me.PanelControl7.TabIndex = 13
        '
        'cmdPreview
        '
        Me.cmdPreview.Location = New System.Drawing.Point(5, 5)
        Me.cmdPreview.Name = "cmdPreview"
        Me.cmdPreview.Size = New System.Drawing.Size(27, 24)
        Me.cmdPreview.TabIndex = 12
        Me.cmdPreview.Visible = False
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(37, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Image = CType(resources.GetObject("cmdKeluar.Image"), System.Drawing.Image)
        Me.cmdKeluar.Location = New System.Drawing.Point(565, 4)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem5.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem5.Appearance.Options.UseImage = True
        ToolTipTitleItem5.Image = CType(resources.GetObject("ToolTipTitleItem5.Image"), System.Drawing.Image)
        ToolTipTitleItem5.Text = "Batal / Keluar"
        ToolTipItem5.LeftIndent = 6
        ToolTipItem5.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        SuperToolTip5.Items.Add(ToolTipItem5)
        Me.cmdKeluar.SuperTip = SuperToolTip5
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(484, 4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem6.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem6.Appearance.Options.UseImage = True
        ToolTipTitleItem6.Image = CType(resources.GetObject("ToolTipTitleItem6.Image"), System.Drawing.Image)
        ToolTipTitleItem6.Text = "Preview Data"
        ToolTipItem6.LeftIndent = 6
        ToolTipItem6.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        SuperToolTip6.Items.Add(ToolTipItem6)
        Me.cmdSimpan.SuperTip = SuperToolTip6
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(222, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(146, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(70, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridControl1
        '
        Me.GridControl1.Enabled = False
        Me.GridControl1.Location = New System.Drawing.Point(5, 70)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(640, 257)
        Me.GridControl1.TabIndex = 11
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.GridControl = Me.GridControl1
        Me.GridView7.Name = "GridView7"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(5, 92)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(640, 235)
        Me.GridControl2.TabIndex = 11
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl2
        Me.GridView4.Name = "GridView4"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage3.Controls.Add(Me.GridControl3)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage3.Text = "3. Kelurahan"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cKodePos)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.cKecamatan)
        Me.PanelControl3.Controls.Add(Me.cNamaKecamatan)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.cKota1)
        Me.PanelControl3.Controls.Add(Me.cNamaKota1)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.cKeterangan2)
        Me.PanelControl3.Controls.Add(Me.LabelControl11)
        Me.PanelControl3.Controls.Add(Me.LabelControl12)
        Me.PanelControl3.Controls.Add(Me.cKode2)
        Me.PanelControl3.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(640, 127)
        Me.PanelControl3.TabIndex = 14
        '
        'cKodePos
        '
        Me.cKodePos.Location = New System.Drawing.Point(80, 102)
        Me.cKodePos.Name = "cKodePos"
        Me.cKodePos.Size = New System.Drawing.Size(44, 20)
        Me.cKodePos.TabIndex = 26
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(7, 105)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl6.TabIndex = 25
        Me.LabelControl6.Text = "Kode Pos"
        '
        'cKecamatan
        '
        Me.cKecamatan.Location = New System.Drawing.Point(80, 32)
        Me.cKecamatan.Name = "cKecamatan"
        Me.cKecamatan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKecamatan.Size = New System.Drawing.Size(44, 20)
        Me.cKecamatan.TabIndex = 24
        '
        'cNamaKecamatan
        '
        Me.cNamaKecamatan.Enabled = False
        Me.cNamaKecamatan.Location = New System.Drawing.Point(130, 32)
        Me.cNamaKecamatan.Name = "cNamaKecamatan"
        Me.cNamaKecamatan.Size = New System.Drawing.Size(191, 20)
        Me.cNamaKecamatan.TabIndex = 23
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(7, 35)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl5.TabIndex = 22
        Me.LabelControl5.Text = "Kecamatan"
        '
        'cKota1
        '
        Me.cKota1.Location = New System.Drawing.Point(80, 8)
        Me.cKota1.Name = "cKota1"
        Me.cKota1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKota1.Size = New System.Drawing.Size(44, 20)
        Me.cKota1.TabIndex = 21
        '
        'cNamaKota1
        '
        Me.cNamaKota1.Enabled = False
        Me.cNamaKota1.Location = New System.Drawing.Point(130, 8)
        Me.cNamaKota1.Name = "cNamaKota1"
        Me.cNamaKota1.Size = New System.Drawing.Size(191, 20)
        Me.cNamaKota1.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Kota / Kab."
        '
        'cKeterangan2
        '
        Me.cKeterangan2.Location = New System.Drawing.Point(80, 79)
        Me.cKeterangan2.Name = "cKeterangan2"
        Me.cKeterangan2.Size = New System.Drawing.Size(191, 20)
        Me.cKeterangan2.TabIndex = 3
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(7, 82)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl11.TabIndex = 2
        Me.LabelControl11.Text = "Keterangan"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(7, 59)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl12.TabIndex = 1
        Me.LabelControl12.Text = "Kode"
        '
        'cKode2
        '
        Me.cKode2.EditValue = "1234567890"
        Me.cKode2.Location = New System.Drawing.Point(80, 56)
        Me.cKode2.Name = "cKode2"
        Me.cKode2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode2.Properties.MaxLength = 15
        Me.cKode2.Size = New System.Drawing.Size(44, 20)
        Me.cKode2.TabIndex = 0
        '
        'GridControl3
        '
        Me.GridControl3.Enabled = False
        Me.GridControl3.Location = New System.Drawing.Point(5, 137)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(640, 190)
        Me.GridControl3.TabIndex = 13
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage2.Text = "2. Kecamatan"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cKota)
        Me.PanelControl2.Controls.Add(Me.cNamaKota)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.cKeterangan1)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.cKode1)
        Me.PanelControl2.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(640, 81)
        Me.PanelControl2.TabIndex = 12
        '
        'cKota
        '
        Me.cKota.Location = New System.Drawing.Point(80, 8)
        Me.cKota.Name = "cKota"
        Me.cKota.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKota.Size = New System.Drawing.Size(44, 20)
        Me.cKota.TabIndex = 18
        '
        'cNamaKota
        '
        Me.cNamaKota.Enabled = False
        Me.cNamaKota.Location = New System.Drawing.Point(140, 8)
        Me.cNamaKota.Name = "cNamaKota"
        Me.cNamaKota.Size = New System.Drawing.Size(191, 20)
        Me.cNamaKota.TabIndex = 17
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl1.TabIndex = 16
        Me.LabelControl1.Text = "Kota / Kab."
        '
        'cKeterangan1
        '
        Me.cKeterangan1.Location = New System.Drawing.Point(80, 55)
        Me.cKeterangan1.Name = "cKeterangan1"
        Me.cKeterangan1.Size = New System.Drawing.Size(191, 20)
        Me.cKeterangan1.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 58)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Keterangan"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(7, 35)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl8.TabIndex = 1
        Me.LabelControl8.Text = "Kode"
        '
        'cKode1
        '
        Me.cKode1.EditValue = "1234567890"
        Me.cKode1.Location = New System.Drawing.Point(80, 32)
        Me.cKode1.Name = "cKode1"
        Me.cKode1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode1.Properties.MaxLength = 15
        Me.cKode1.Size = New System.Drawing.Size(44, 20)
        Me.cKode1.TabIndex = 0
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GridControl1)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(649, 331)
        Me.XtraTabPage1.Text = "1. Kota / Kabupaten"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(5, 5)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(640, 59)
        Me.PanelControl1.TabIndex = 10
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(80, 31)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(191, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(80, 8)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(44, 20)
        Me.cKode.TabIndex = 0
        '
        'tabControl1
        '
        Me.tabControl1.Location = New System.Drawing.Point(2, 2)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.tabControl1.Size = New System.Drawing.Size(655, 359)
        Me.tabControl1.TabIndex = 12
        Me.tabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'MstDaerah
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 399)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.tabControl1)
        Me.Name = "MstDaerah"
        Me.Text = "MstDaerah"
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cKodePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKecamatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKecamatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKota1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKota1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdPreview As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cKeterangan2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cKeterangan1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents cKodePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKecamatan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKecamatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKota1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKota1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKota As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKota As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
