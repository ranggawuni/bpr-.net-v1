﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cfgTabungan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtData IsNot Nothing Then
                    dtData.Dispose()
                    dtData = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cfgTabungan))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.cSetoranPB = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaSetoranPB = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cPenarikanPB = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaPenarikanPB = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.cPajak = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cBunga = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.cAdmPenutupan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAdmPenutupan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cPenutupan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaPenutupan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.cAdmPemeliharaan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAdmPemeliharaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.cSetoranTunai = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaSetoranTunai = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cPenarikanTunai = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaPenarikanTunai = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cSetoranPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSetoranPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cPenarikanPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPenarikanPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cAdmPenutupan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAdmPenutupan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cPenutupan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPenutupan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cAdmPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAdmPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cSetoranTunai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSetoranTunai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cPenarikanTunai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPenarikanTunai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Location = New System.Drawing.Point(2, 405)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(537, 33)
        Me.PanelControl7.TabIndex = 16
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(453, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(372, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl5)
        Me.PanelControl1.Controls.Add(Me.GroupControl4)
        Me.PanelControl1.Controls.Add(Me.GroupControl3)
        Me.PanelControl1.Controls.Add(Me.GroupControl2)
        Me.PanelControl1.Controls.Add(Me.GroupControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(537, 397)
        Me.PanelControl1.TabIndex = 17
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cSetoranPB)
        Me.GroupControl1.Controls.Add(Me.cNamaSetoranPB)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.cPenarikanPB)
        Me.GroupControl1.Controls.Add(Me.cNamaPenarikanPB)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(5, 5)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(527, 78)
        Me.GroupControl1.TabIndex = 41
        Me.GroupControl1.Text = "Pemindah Bukuan"
        '
        'cSetoranPB
        '
        Me.cSetoranPB.Location = New System.Drawing.Point(88, 50)
        Me.cSetoranPB.Name = "cSetoranPB"
        Me.cSetoranPB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSetoranPB.Size = New System.Drawing.Size(120, 20)
        Me.cSetoranPB.TabIndex = 30
        '
        'cNamaSetoranPB
        '
        Me.cNamaSetoranPB.Enabled = False
        Me.cNamaSetoranPB.Location = New System.Drawing.Point(214, 50)
        Me.cNamaSetoranPB.Name = "cNamaSetoranPB"
        Me.cNamaSetoranPB.Size = New System.Drawing.Size(308, 20)
        Me.cNamaSetoranPB.TabIndex = 29
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(6, 53)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl1.TabIndex = 28
        Me.LabelControl1.Text = "Penyetoran"
        '
        'cPenarikanPB
        '
        Me.cPenarikanPB.Location = New System.Drawing.Point(88, 24)
        Me.cPenarikanPB.Name = "cPenarikanPB"
        Me.cPenarikanPB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPenarikanPB.Size = New System.Drawing.Size(120, 20)
        Me.cPenarikanPB.TabIndex = 27
        '
        'cNamaPenarikanPB
        '
        Me.cNamaPenarikanPB.Enabled = False
        Me.cNamaPenarikanPB.Location = New System.Drawing.Point(214, 24)
        Me.cNamaPenarikanPB.Name = "cNamaPenarikanPB"
        Me.cNamaPenarikanPB.Size = New System.Drawing.Size(308, 20)
        Me.cNamaPenarikanPB.TabIndex = 26
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(6, 27)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl2.TabIndex = 25
        Me.LabelControl2.Text = "Penarikan"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.cPajak)
        Me.GroupControl2.Controls.Add(Me.cNamaPajak)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.cBunga)
        Me.GroupControl2.Controls.Add(Me.cNamaBunga)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Location = New System.Drawing.Point(5, 89)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(527, 78)
        Me.GroupControl2.TabIndex = 42
        Me.GroupControl2.Text = "Bunga Tabungan"
        '
        'cPajak
        '
        Me.cPajak.Location = New System.Drawing.Point(88, 50)
        Me.cPajak.Name = "cPajak"
        Me.cPajak.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPajak.Size = New System.Drawing.Size(120, 20)
        Me.cPajak.TabIndex = 30
        '
        'cNamaPajak
        '
        Me.cNamaPajak.Enabled = False
        Me.cNamaPajak.Location = New System.Drawing.Point(214, 50)
        Me.cNamaPajak.Name = "cNamaPajak"
        Me.cNamaPajak.Size = New System.Drawing.Size(308, 20)
        Me.cNamaPajak.TabIndex = 29
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(6, 53)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl3.TabIndex = 28
        Me.LabelControl3.Text = "Pajak"
        '
        'cBunga
        '
        Me.cBunga.Location = New System.Drawing.Point(88, 24)
        Me.cBunga.Name = "cBunga"
        Me.cBunga.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cBunga.Size = New System.Drawing.Size(120, 20)
        Me.cBunga.TabIndex = 27
        '
        'cNamaBunga
        '
        Me.cNamaBunga.Enabled = False
        Me.cNamaBunga.Location = New System.Drawing.Point(214, 24)
        Me.cNamaBunga.Name = "cNamaBunga"
        Me.cNamaBunga.Size = New System.Drawing.Size(308, 20)
        Me.cNamaBunga.TabIndex = 26
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(6, 27)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl4.TabIndex = 25
        Me.LabelControl4.Text = "Bunga"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.cAdmPenutupan)
        Me.GroupControl3.Controls.Add(Me.cNamaAdmPenutupan)
        Me.GroupControl3.Controls.Add(Me.LabelControl5)
        Me.GroupControl3.Controls.Add(Me.cPenutupan)
        Me.GroupControl3.Controls.Add(Me.cNamaPenutupan)
        Me.GroupControl3.Controls.Add(Me.LabelControl6)
        Me.GroupControl3.Location = New System.Drawing.Point(5, 173)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(527, 78)
        Me.GroupControl3.TabIndex = 43
        Me.GroupControl3.Text = "Penutupan Rekening"
        '
        'cAdmPenutupan
        '
        Me.cAdmPenutupan.Location = New System.Drawing.Point(88, 50)
        Me.cAdmPenutupan.Name = "cAdmPenutupan"
        Me.cAdmPenutupan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAdmPenutupan.Size = New System.Drawing.Size(120, 20)
        Me.cAdmPenutupan.TabIndex = 30
        '
        'cNamaAdmPenutupan
        '
        Me.cNamaAdmPenutupan.Enabled = False
        Me.cNamaAdmPenutupan.Location = New System.Drawing.Point(214, 50)
        Me.cNamaAdmPenutupan.Name = "cNamaAdmPenutupan"
        Me.cNamaAdmPenutupan.Size = New System.Drawing.Size(308, 20)
        Me.cNamaAdmPenutupan.TabIndex = 29
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(6, 53)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl5.TabIndex = 28
        Me.LabelControl5.Text = "Administrasi"
        '
        'cPenutupan
        '
        Me.cPenutupan.Location = New System.Drawing.Point(88, 24)
        Me.cPenutupan.Name = "cPenutupan"
        Me.cPenutupan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPenutupan.Size = New System.Drawing.Size(120, 20)
        Me.cPenutupan.TabIndex = 27
        '
        'cNamaPenutupan
        '
        Me.cNamaPenutupan.Enabled = False
        Me.cNamaPenutupan.Location = New System.Drawing.Point(214, 24)
        Me.cNamaPenutupan.Name = "cNamaPenutupan"
        Me.cNamaPenutupan.Size = New System.Drawing.Size(308, 20)
        Me.cNamaPenutupan.TabIndex = 26
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 27)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl6.TabIndex = 25
        Me.LabelControl6.Text = "Penutupan"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.cAdmPemeliharaan)
        Me.GroupControl4.Controls.Add(Me.cNamaAdmPemeliharaan)
        Me.GroupControl4.Controls.Add(Me.LabelControl8)
        Me.GroupControl4.Location = New System.Drawing.Point(5, 257)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(527, 50)
        Me.GroupControl4.TabIndex = 44
        Me.GroupControl4.Text = "Administrasi"
        '
        'cAdmPemeliharaan
        '
        Me.cAdmPemeliharaan.Location = New System.Drawing.Point(88, 24)
        Me.cAdmPemeliharaan.Name = "cAdmPemeliharaan"
        Me.cAdmPemeliharaan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAdmPemeliharaan.Size = New System.Drawing.Size(120, 20)
        Me.cAdmPemeliharaan.TabIndex = 27
        '
        'cNamaAdmPemeliharaan
        '
        Me.cNamaAdmPemeliharaan.Enabled = False
        Me.cNamaAdmPemeliharaan.Location = New System.Drawing.Point(214, 24)
        Me.cNamaAdmPemeliharaan.Name = "cNamaAdmPemeliharaan"
        Me.cNamaAdmPemeliharaan.Size = New System.Drawing.Size(308, 20)
        Me.cNamaAdmPemeliharaan.TabIndex = 26
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(6, 27)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl8.TabIndex = 25
        Me.LabelControl8.Text = "Pemeliharaan"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.cSetoranTunai)
        Me.GroupControl5.Controls.Add(Me.cNamaSetoranTunai)
        Me.GroupControl5.Controls.Add(Me.LabelControl7)
        Me.GroupControl5.Controls.Add(Me.cPenarikanTunai)
        Me.GroupControl5.Controls.Add(Me.cNamaPenarikanTunai)
        Me.GroupControl5.Controls.Add(Me.LabelControl9)
        Me.GroupControl5.Location = New System.Drawing.Point(5, 313)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(527, 78)
        Me.GroupControl5.TabIndex = 45
        Me.GroupControl5.Text = "Tunai"
        '
        'cSetoranTunai
        '
        Me.cSetoranTunai.Location = New System.Drawing.Point(88, 50)
        Me.cSetoranTunai.Name = "cSetoranTunai"
        Me.cSetoranTunai.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSetoranTunai.Size = New System.Drawing.Size(120, 20)
        Me.cSetoranTunai.TabIndex = 30
        '
        'cNamaSetoranTunai
        '
        Me.cNamaSetoranTunai.Enabled = False
        Me.cNamaSetoranTunai.Location = New System.Drawing.Point(214, 50)
        Me.cNamaSetoranTunai.Name = "cNamaSetoranTunai"
        Me.cNamaSetoranTunai.Size = New System.Drawing.Size(308, 20)
        Me.cNamaSetoranTunai.TabIndex = 29
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(6, 53)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl7.TabIndex = 28
        Me.LabelControl7.Text = "Penyetoran"
        '
        'cPenarikanTunai
        '
        Me.cPenarikanTunai.Location = New System.Drawing.Point(88, 24)
        Me.cPenarikanTunai.Name = "cPenarikanTunai"
        Me.cPenarikanTunai.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPenarikanTunai.Size = New System.Drawing.Size(120, 20)
        Me.cPenarikanTunai.TabIndex = 27
        '
        'cNamaPenarikanTunai
        '
        Me.cNamaPenarikanTunai.Enabled = False
        Me.cNamaPenarikanTunai.Location = New System.Drawing.Point(214, 24)
        Me.cNamaPenarikanTunai.Name = "cNamaPenarikanTunai"
        Me.cNamaPenarikanTunai.Size = New System.Drawing.Size(308, 20)
        Me.cNamaPenarikanTunai.TabIndex = 26
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(6, 27)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl9.TabIndex = 25
        Me.LabelControl9.Text = "Penarikan"
        '
        'cfgTabungan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 439)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl7)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "cfgTabungan"
        Me.Text = "Konfigurasi Tabungan"
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cSetoranPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSetoranPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cPenarikanPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPenarikanPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cAdmPenutupan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAdmPenutupan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cPenutupan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPenutupan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.cAdmPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAdmPemeliharaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cSetoranTunai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSetoranTunai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cPenarikanTunai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPenarikanTunai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cSetoranTunai As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaSetoranTunai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cPenarikanTunai As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaPenarikanTunai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cAdmPemeliharaan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAdmPemeliharaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cAdmPenutupan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAdmPenutupan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cPenutupan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaPenutupan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cPajak As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cBunga As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cSetoranPB As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaSetoranPB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cPenarikanPB As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaPenarikanPB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
End Class
