﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MstGolonganKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtData IsNot Nothing Then
                    dtData.Dispose()
                    dtData = Nothing
                End If
                If dtRekening IsNot Nothing Then
                    dtRekening.Dispose()
                    dtRekening = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MstGolonganKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekeningAccrual = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAccrual = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTitipanAngsuran = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningTitipanAngsuran = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningPendapatanProvisi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningPendapatanProvisi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningProvisi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningProvisi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAdministrasi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAdministrasi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningDenda = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningDenda = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.optAmortisasi = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.nToleransiDenda = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningBunga = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.cRekeningAsuransi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAsuransi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningMaterai = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningMaterai = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningNotaris = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningNotaris = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTitipanAngsuran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningTitipanAngsuran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningPendapatanProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningPendapatanProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optAmortisasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nToleransiDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAsuransi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAsuransi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningMaterai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningMaterai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningNotaris.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningNotaris.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl7.Controls.Add(Me.cmdKeluar)
        Me.PanelControl7.Controls.Add(Me.cmdSimpan)
        Me.PanelControl7.Controls.Add(Me.cmdHapus)
        Me.PanelControl7.Controls.Add(Me.cmdEdit)
        Me.PanelControl7.Controls.Add(Me.cmdAdd)
        Me.PanelControl7.Location = New System.Drawing.Point(2, 455)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(945, 33)
        Me.PanelControl7.TabIndex = 21
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 5)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 11
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(849, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(768, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(192, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(116, 5)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(40, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(2, 249)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(945, 200)
        Me.GridControl1.TabIndex = 20
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekeningAsuransi)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAsuransi)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cRekeningMaterai)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningMaterai)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cRekeningNotaris)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningNotaris)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cRekeningAccrual)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAccrual)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekeningTitipanAngsuran)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningTitipanAngsuran)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cRekeningPendapatanProvisi)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningPendapatanProvisi)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.cRekeningProvisi)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningProvisi)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.cRekeningAdministrasi)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAdministrasi)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.cRekeningDenda)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningDenda)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.optAmortisasi)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.nToleransiDenda)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cNamaRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 1)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(945, 242)
        Me.PanelControl1.TabIndex = 19
        '
        'cRekeningAccrual
        '
        Me.cRekeningAccrual.Location = New System.Drawing.Point(587, 88)
        Me.cRekeningAccrual.Name = "cRekeningAccrual"
        Me.cRekeningAccrual.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAccrual.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningAccrual.TabIndex = 125
        '
        'cNamaRekeningAccrual
        '
        Me.cNamaRekeningAccrual.Enabled = False
        Me.cNamaRekeningAccrual.Location = New System.Drawing.Point(711, 88)
        Me.cNamaRekeningAccrual.Name = "cNamaRekeningAccrual"
        Me.cNamaRekeningAccrual.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningAccrual.TabIndex = 124
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(469, 91)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl10.TabIndex = 123
        Me.LabelControl10.Text = "Rek. Accrual"
        '
        'cRekeningTitipanAngsuran
        '
        Me.cRekeningTitipanAngsuran.Location = New System.Drawing.Point(587, 62)
        Me.cRekeningTitipanAngsuran.Name = "cRekeningTitipanAngsuran"
        Me.cRekeningTitipanAngsuran.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTitipanAngsuran.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningTitipanAngsuran.TabIndex = 122
        '
        'cNamaRekeningTitipanAngsuran
        '
        Me.cNamaRekeningTitipanAngsuran.Enabled = False
        Me.cNamaRekeningTitipanAngsuran.Location = New System.Drawing.Point(711, 62)
        Me.cNamaRekeningTitipanAngsuran.Name = "cNamaRekeningTitipanAngsuran"
        Me.cNamaRekeningTitipanAngsuran.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningTitipanAngsuran.TabIndex = 121
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(469, 65)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl11.TabIndex = 120
        Me.LabelControl11.Text = "Rek. Titipan Angsuran"
        '
        'cRekeningPendapatanProvisi
        '
        Me.cRekeningPendapatanProvisi.Location = New System.Drawing.Point(587, 36)
        Me.cRekeningPendapatanProvisi.Name = "cRekeningPendapatanProvisi"
        Me.cRekeningPendapatanProvisi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningPendapatanProvisi.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningPendapatanProvisi.TabIndex = 119
        '
        'cNamaRekeningPendapatanProvisi
        '
        Me.cNamaRekeningPendapatanProvisi.Enabled = False
        Me.cNamaRekeningPendapatanProvisi.Location = New System.Drawing.Point(711, 36)
        Me.cNamaRekeningPendapatanProvisi.Name = "cNamaRekeningPendapatanProvisi"
        Me.cNamaRekeningPendapatanProvisi.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningPendapatanProvisi.TabIndex = 118
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(469, 38)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl8.TabIndex = 117
        Me.LabelControl8.Text = "Rek. Pend. Provisi"
        '
        'cRekeningProvisi
        '
        Me.cRekeningProvisi.Location = New System.Drawing.Point(587, 9)
        Me.cRekeningProvisi.Name = "cRekeningProvisi"
        Me.cRekeningProvisi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningProvisi.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningProvisi.TabIndex = 116
        '
        'cNamaRekeningProvisi
        '
        Me.cNamaRekeningProvisi.Enabled = False
        Me.cNamaRekeningProvisi.Location = New System.Drawing.Point(711, 9)
        Me.cNamaRekeningProvisi.Name = "cNamaRekeningProvisi"
        Me.cNamaRekeningProvisi.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningProvisi.TabIndex = 115
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(469, 12)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl9.TabIndex = 114
        Me.LabelControl9.Text = "Rek. Provisi"
        '
        'cRekeningAdministrasi
        '
        Me.cRekeningAdministrasi.Location = New System.Drawing.Point(105, 137)
        Me.cRekeningAdministrasi.Name = "cRekeningAdministrasi"
        Me.cRekeningAdministrasi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAdministrasi.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningAdministrasi.TabIndex = 113
        '
        'cNamaRekeningAdministrasi
        '
        Me.cNamaRekeningAdministrasi.Enabled = False
        Me.cNamaRekeningAdministrasi.Location = New System.Drawing.Point(231, 137)
        Me.cNamaRekeningAdministrasi.Name = "cNamaRekeningAdministrasi"
        Me.cNamaRekeningAdministrasi.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningAdministrasi.TabIndex = 112
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(7, 140)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl5.TabIndex = 111
        Me.LabelControl5.Text = "Rek. Administrasi"
        '
        'cRekeningDenda
        '
        Me.cRekeningDenda.Location = New System.Drawing.Point(105, 111)
        Me.cRekeningDenda.Name = "cRekeningDenda"
        Me.cRekeningDenda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningDenda.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningDenda.TabIndex = 110
        '
        'cNamaRekeningDenda
        '
        Me.cNamaRekeningDenda.Enabled = False
        Me.cNamaRekeningDenda.Location = New System.Drawing.Point(231, 111)
        Me.cNamaRekeningDenda.Name = "cNamaRekeningDenda"
        Me.cNamaRekeningDenda.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningDenda.TabIndex = 109
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(7, 114)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl6.TabIndex = 108
        Me.LabelControl6.Text = "Rek. Denda"
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(647, 117)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl18.TabIndex = 107
        Me.LabelControl18.Text = "Hari"
        '
        'optAmortisasi
        '
        Me.optAmortisasi.Location = New System.Drawing.Point(587, 140)
        Me.optAmortisasi.Name = "optAmortisasi"
        Me.optAmortisasi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optAmortisasi.Size = New System.Drawing.Size(120, 31)
        Me.optAmortisasi.TabIndex = 14
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(469, 117)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl1.TabIndex = 99
        Me.LabelControl1.Text = "Toleransi Denda"
        '
        'nToleransiDenda
        '
        Me.nToleransiDenda.EditValue = "123456"
        Me.nToleransiDenda.Location = New System.Drawing.Point(587, 114)
        Me.nToleransiDenda.Name = "nToleransiDenda"
        Me.nToleransiDenda.Properties.Mask.EditMask = "1.000.00.00.00"
        Me.nToleransiDenda.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.nToleransiDenda.Properties.MaxLength = 6
        Me.nToleransiDenda.Size = New System.Drawing.Size(54, 20)
        Me.nToleransiDenda.TabIndex = 98
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(469, 143)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl13.TabIndex = 93
        Me.LabelControl13.Text = "Amortisasi"
        '
        'cRekeningBunga
        '
        Me.cRekeningBunga.Location = New System.Drawing.Point(105, 85)
        Me.cRekeningBunga.Name = "cRekeningBunga"
        Me.cRekeningBunga.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningBunga.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningBunga.TabIndex = 21
        '
        'cNamaRekeningBunga
        '
        Me.cNamaRekeningBunga.Enabled = False
        Me.cNamaRekeningBunga.Location = New System.Drawing.Point(231, 85)
        Me.cNamaRekeningBunga.Name = "cNamaRekeningBunga"
        Me.cNamaRekeningBunga.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningBunga.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(7, 88)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Rek. Bunga"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(105, 59)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(120, 20)
        Me.cRekening.TabIndex = 18
        '
        'cNamaRekening
        '
        Me.cNamaRekening.Enabled = False
        Me.cNamaRekening.Location = New System.Drawing.Point(231, 59)
        Me.cNamaRekening.Name = "cNamaRekening"
        Me.cNamaRekening.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekening.TabIndex = 17
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 62)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Rek. Perkiraan"
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(105, 33)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(266, 20)
        Me.cKeterangan.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 36)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "1234567890"
        Me.cKode.Location = New System.Drawing.Point(105, 9)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "1.###.##.##.##"
        Me.cKode.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(32)
        Me.cKode.Properties.MaxLength = 15
        Me.cKode.Size = New System.Drawing.Size(34, 20)
        Me.cKode.TabIndex = 0
        '
        'cRekeningAsuransi
        '
        Me.cRekeningAsuransi.Location = New System.Drawing.Point(105, 215)
        Me.cRekeningAsuransi.Name = "cRekeningAsuransi"
        Me.cRekeningAsuransi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAsuransi.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningAsuransi.TabIndex = 134
        '
        'cNamaRekeningAsuransi
        '
        Me.cNamaRekeningAsuransi.Enabled = False
        Me.cNamaRekeningAsuransi.Location = New System.Drawing.Point(231, 215)
        Me.cNamaRekeningAsuransi.Name = "cNamaRekeningAsuransi"
        Me.cNamaRekeningAsuransi.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningAsuransi.TabIndex = 133
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(7, 218)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl12.TabIndex = 132
        Me.LabelControl12.Text = "Rek. Asuransi"
        '
        'cRekeningMaterai
        '
        Me.cRekeningMaterai.Location = New System.Drawing.Point(105, 189)
        Me.cRekeningMaterai.Name = "cRekeningMaterai"
        Me.cRekeningMaterai.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningMaterai.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningMaterai.TabIndex = 131
        '
        'cNamaRekeningMaterai
        '
        Me.cNamaRekeningMaterai.Enabled = False
        Me.cNamaRekeningMaterai.Location = New System.Drawing.Point(231, 189)
        Me.cNamaRekeningMaterai.Name = "cNamaRekeningMaterai"
        Me.cNamaRekeningMaterai.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningMaterai.TabIndex = 130
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(7, 192)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl14.TabIndex = 129
        Me.LabelControl14.Text = "Rek. Materai"
        '
        'cRekeningNotaris
        '
        Me.cRekeningNotaris.Location = New System.Drawing.Point(105, 163)
        Me.cRekeningNotaris.Name = "cRekeningNotaris"
        Me.cRekeningNotaris.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningNotaris.Size = New System.Drawing.Size(120, 20)
        Me.cRekeningNotaris.TabIndex = 128
        '
        'cNamaRekeningNotaris
        '
        Me.cNamaRekeningNotaris.Enabled = False
        Me.cNamaRekeningNotaris.Location = New System.Drawing.Point(231, 163)
        Me.cNamaRekeningNotaris.Name = "cNamaRekeningNotaris"
        Me.cNamaRekeningNotaris.Size = New System.Drawing.Size(226, 20)
        Me.cNamaRekeningNotaris.TabIndex = 127
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(7, 166)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl15.TabIndex = 126
        Me.LabelControl15.Text = "Rek. Notaris"
        '
        'MstGolonganKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(950, 492)
        Me.Controls.Add(Me.PanelControl7)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MstGolonganKredit"
        Me.Text = "Golongan Kredit"
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAccrual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTitipanAngsuran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningTitipanAngsuran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningPendapatanProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningPendapatanProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optAmortisasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nToleransiDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAsuransi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAsuransi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningMaterai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningMaterai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningNotaris.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningNotaris.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekeningAsuransi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAsuransi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningMaterai As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningMaterai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningNotaris As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningNotaris As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAccrual As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAccrual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTitipanAngsuran As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningTitipanAngsuran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningPendapatanProvisi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningPendapatanProvisi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningProvisi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningProvisi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAdministrasi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAdministrasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningDenda As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningDenda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optAmortisasi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nToleransiDenda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningBunga As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
End Class
