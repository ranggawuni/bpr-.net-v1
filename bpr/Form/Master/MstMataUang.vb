﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstMataUang
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String
    Dim dtData As New DataTable

    Private Sub InitValue()
        optJenis.SelectedIndex = 0
        cKode.Text = ""
        nMataUang.Text = "0"
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "Kode,Nominal,Status"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "UangPecahan", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("UangPecahan")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 50)
            GridColumnFormat(GridView1, "Nominal", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, 90, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status", , , 60, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub MstMataUang_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetSQL()
        InitValue()
        GetEdit(False)

        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(nMataUang.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        optJenis.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        nMataUang.EnterMoveNextControl = True
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(cKode.Text, "Kode KodeTransaksi Harus Diisi, Silahkan Mengulangi Pengisian") Then
            ValidSaving = False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(nMataUang.Text, "Nominal Mata Uang Harus Diisi, Silahkan Mengulangi Pengisian") Then
            ValidSaving = False
            nMataUang.Focus()
            Exit Function
        End If
    End Function

    Private Sub DeleteData()
        If MsgBox("Data Benar-benar Dihapus ?", vbYesNo) = vbYes Then
            objData.Delete(GetDSN, "UangPecahan", "kode", , cKode.Text)
            GetSQL()
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data benar-benar sudah VALID ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                Dim vaField() As Object = {"kode", "Nominal", "Status"}
                Dim vaValue() As Object = {cKode.Text, Val(nMataUang.Text), cKode.Text.Substring(0, 1)}
                objData.Update(GetDSN, "UangPecahan", String.Format("kode = '{0}'", cKode.Text), vaField, vaValue)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(sender As Object, e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        Try
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
                If cKode.Text.Trim <> "" Then
                    cKode.Text = Padl(cKode.Text, 2, "0")
                    Dim cDataTable = objData.Browse(GetDSN, "UangPecahan", "kode", "kode", data.myOperator.Assign, cKode.Text, , "kode")
                    With cDataTable
                        If .Rows.Count > 0 Then
                            If nPos = myPos.add Then
                                MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                Exit Sub
                            End If
                            GetMemory()
                            If nPos = myPos.delete Then
                                DeleteData()
                            End If
                        Else
                            MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            cKode.Focus()
                        End If
                    End With
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetMemory()
        Try
            Const cField As String = "kode,nominal,status"
            dtData = objData.Browse(GetDSN, "UangPecahan", cField, "kode", , cKode.Text)
            If dtData.Rows.Count > 0 Then
                With dtData.Rows(0)
                    cKode.Text = .Item("Kode").ToString
                    nMataUang.Text = .Item("nominal").ToString
                    If .Item("status").ToString = "L" Then
                        optJenis.SelectedIndex = 1
                    Else
                        optJenis.SelectedIndex = 0
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GetDataGrid()
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            nMataUang.Text = row(1).ToString
            SetOpt(optJenis, row(2).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub optJenis_KeyPress(sender As Object, e As KeyPressEventArgs) Handles optJenis.KeyPress
        SendKeys.Send("{TAB}")
    End Sub

    Private Function GetKode() As Integer
        Dim nNomor As Integer = 0
        Dim cJenis As String = GetOpt(optJenis)
        Const cField As String = "Max(kode) as kode"
        dtData = objData.Browse(GetDSN, "UangPecahan", cField, "left(kode,1)", , cJenis, , "Kode")
        If dtData.Rows.Count > 0 Then
            nNomor = CInt(GetNull(Microsoft.VisualBasic.Right(dtData.Rows(0).Item("Kode").ToString, 3))) + 1
        Else
            nNomor = 1
        End If
        GetKode = nNomor
    End Function

    Private Sub optJenis_LostFocus(sender As Object, e As EventArgs) Handles optJenis.LostFocus
        cKode.Text = GetOpt(optJenis) & Padl(GetKode.ToString, 3, "0")
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        GetDataGrid()
    End Sub
End Class