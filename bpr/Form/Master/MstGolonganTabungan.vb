﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstGolonganTabungan
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Public cStatus As String
    Dim dtData As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cRekening.EditValue = ""
        cNamaRekening.Text = ""
        cRekeningBunga.EditValue = ""
        cNamaRekeningBunga.Text = ""
        nPajak.Text = "0"
        nLamaPasif.Text = "0"
        nSaldoMinimum.Text = "0"
        nSetoranMinimum.Text = "0"
        nSaldoDapatBunga.Text = "0"
        nAdministrasi.Text = "0"
        nPemeliharaan.Text = "0"
        nAdminPasif.Text = "0"
        nSaldoKenaPajak.Text = "0"
        optWajibPajak.SelectedIndex = 0
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Akan Dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            objData.Delete(GetDSN, "golongantabungan", "kode", data.myOperator.Assign, cKode.Text, cWhere)
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Not CheckData(cKode, "Kode Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If
        If Not CheckData(cKeterangan, "Nama Bank Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cKeterangan.Focus()
            Exit Function
        End If
        If Not CheckData(cRekening, "Rekening Harus Diisi. Ulangi Pengisian.") Then
            Return False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim cWajibPajak As String = GetOpt(optWajibPajak)
                Dim vaField() As Object = {"Kode", "Keterangan", "Rekening", _
                                           "SaldoMinimum", "SetoranMinimum", "SaldominimumDapatBunga", "AdministrasiTutup", _
                                           "AdministrasiBulanan", "AdminPasif", "WajibPajak", "RekeningBunga", _
                                           "PajakBunga", "SaldoMinimumKenaPajak", "LamaPasif"}
                Dim vaValue() As Object = {cKode.Text, cKeterangan.Text, cRekening.Text, _
                                           Val(nSaldoMinimum.Text), Val(nSetoranMinimum.Text), Val(nSaldoDapatBunga.Text), Val(nAdministrasi.Text), _
                                           Val(nPemeliharaan.Text), Val(nAdminPasif.Text), cWajibPajak, cRekeningBunga.Text, _
                                           Val(nPajak.Text), Val(nSaldoKenaPajak.Text), Val(nLamaPasif.Text)}
                Dim cWhere As String = String.Format("kode = '{0}' and status = '{1}'", cKode.Text, cStatus)
                objData.Update(GetDSN, "golongantabungan", cWhere, vaField, vaValue)
                GetSQL()
            Else
                cKode.Focus()
                Exit Sub
            End If
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub GetMemory()
        Const cField As String = "k.*, i.Keterangan as NamaRekeningBunga,r.Keterangan as KeteranganRekening"
        Dim vaJoin() As String = {"Left Join Rekening r on r.Kode=k.Rekening", _
                                  "Left Join Rekening i on i.Kode = k.RekeningBunga"}
        dtData = objData.Browse(GetDSN, "GolonganTabungan k", cField, "k.Kode", data.myOperator.Assign, cKode.Text, , "k.Kode", vaJoin)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cKeterangan.Text = .Item("Keterangan").ToString
                cRekening.Text = .Item("Rekening").ToString
                cNamaRekening.Text = .Item("KeteranganRekening").ToString
                nPajak.Text = .Item("PajakBunga").ToString
                nSaldoMinimum.Text = .Item("SaldoMinimum").ToString
                nSetoranMinimum.Text = .Item("SetoranMinimum").ToString
                nSaldoDapatBunga.Text = .Item("SaldoMinimumDapatBunga").ToString
                nAdministrasi.Text = .Item("AdministrasiTutup").ToString
                nPemeliharaan.Text = .Item("AdministrasiBulanan").ToString
                nAdminPasif.Text = .Item("AdminPasif").ToString
                SetOpt(optWajibPajak, .Item("WajibPajak").ToString)
                cRekeningBunga.Text = .Item("RekeningBunga").ToString
                cNamaRekeningBunga.Text = .Item("NamaRekeningbunga").ToString
                nSaldoKenaPajak.Text = .Item("SaldoMinimumKenaPajak").ToString
                nLamaPasif.Text = .Item("LamaPasif").ToString
            End With
        End If
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Dim cField As String = "Kode,Keterangan,Rekening,RekeningBunga,PajakBunga,LamaPasif,SaldoMinimum,"
            cField = cField & "SetoranMinimum,SaldoMinimumDapatBunga,AdministrasiTutup,AdministrasiBulanan,"
            cField = cField & "AdminPasif,SaldoMinimumKenaPajak,WajibPajak"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "golongantabungan", cField, , , , , "kode")
            GridControl1.DataSource = cDataSet.Tables("golongantabungan")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", DevExpress.Utils.FormatType.None, , 50)
            GridColumnFormat(GridView1, "Keterangan")
            GridColumnFormat(GridView1, "Rekening", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningBunga", , , 80, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "PajakBunga", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "LamaPasif", , , 40, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "SaldoMinimum", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "SetoranMinimum", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "SaldoMinimumDapatBunga", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "AdministrasiTutup", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "AdministrasiBulanan", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "AdminPasif", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "SaldoMinimumKenaPajak", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "WajibPajak", , , 50, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim cWhere As String = String.Format(" and status = '{0}'", cStatus)
            Dim cDataTable = objData.Browse(GetDSN, "golongantabungan", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, cWhere, "kode")
            With cDataTable
                If .Rows.Count > 0 Then
                    If nPos = myPos.add Then
                        MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                Else
                    MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        GetKeteranganLookUp(cRekening, cNamaRekening)
    End Sub

    Private Function GetKeterangan(ByVal cKodeTemp As String, ByVal cTable As String) As String
        Dim cDataTable = objData.Browse(GetDSN, cTable, "keterangan", "kode", data.myOperator.Assign, cKodeTemp)
        If cDataTable.Rows.Count > 0 Then
            GetKeterangan = cDataTable.Rows(0).Item("keterangan").ToString
        Else
            GetKeterangan = ""
        End If
    End Function

    Private Sub MstGolonganTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetSQL()
        InitValue()
        GetEdit(False)

        GetDataLookup("rekening", cRekening)
        GetDataLookup("rekening", cRekeningBunga)


        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cRekeningBunga.TabIndex, n)
        SetTabIndex(nPajak.TabIndex, n)
        SetTabIndex(nLamaPasif.TabIndex, n)
        SetTabIndex(nSaldoMinimum.TabIndex, n)
        SetTabIndex(nSetoranMinimum.TabIndex, n)
        SetTabIndex(nSaldoDapatBunga.TabIndex, n)
        SetTabIndex(nAdministrasi.TabIndex, n)
        SetTabIndex(nPemeliharaan.TabIndex, n)
        SetTabIndex(nAdminPasif.TabIndex, n)
        SetTabIndex(nSaldoKenaPajak.TabIndex, n)
        SetTabIndex(optWajibPajak.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cRekeningBunga.EnterMoveNextControl = True
        nPajak.EnterMoveNextControl = True
        nLamaPasif.EnterMoveNextControl = True
        nSaldoMinimum.EnterMoveNextControl = True
        nSaldoDapatBunga.EnterMoveNextControl = True
        nAdministrasi.EnterMoveNextControl = True
        nPemeliharaan.EnterMoveNextControl = True
        nAdminPasif.EnterMoveNextControl = True
        nSaldoKenaPajak.EnterMoveNextControl = True
        optWajibPajak.EnterMoveNextControl = True

        FormatTextBox(nPajak, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nLamaPasif, formatType.BilRpPict, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nSaldoMinimum, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nSetoranMinimum, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nSaldoDapatBunga, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nPemeliharaan, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nAdminPasif, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nSaldoKenaPajak, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub cRekeningBunga_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningBunga.Closed
        GetKeteranganLookUp(cRekeningBunga, cNamaRekeningBunga)
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            cKeterangan.Text = row(1).ToString
            cRekening.EditValue = row(2).ToString
            cNamaRekening.Text = GetKeterangan(cRekening.Text, "rekening")
            cRekeningBunga.EditValue = row(3).ToString
            cNamaRekeningBunga.Text = GetKeterangan(cRekeningBunga.Text, "rekening")
            nPajak.Text = row(4).ToString
            nLamaPasif.Text = row(5).ToString
            nSaldoMinimum.Text = row(6).ToString
            nSetoranMinimum.Text = row(7).ToString
            nSaldoMinimum.Text = row(8).ToString
            nAdministrasi.Text = row(9).ToString
            nPemeliharaan.Text = row(10).ToString
            nAdminPasif.Text = row(11).ToString
            nSaldoKenaPajak.Text = row(12).ToString
            SetOpt(optWajibPajak, row(13).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class