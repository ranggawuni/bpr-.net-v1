﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraTab
Imports bpr.MySQL_Data_Library

Public Class MstDaerah
    ReadOnly objData As New data()
    Dim nPos As myPos
    Dim lEdit As Boolean
    Dim dtKecamatan As New DataTable
    Dim dtKelurahan As New DataTable
    Dim dtKota As New DataTable
    Dim nBaris As DataRow()

    Private Sub InitData()
        dtKota = objData.SQL(GetDSN, "Select kode,keterangan from kodya")
        dtKecamatan = objData.SQL(GetDSN, "select kode,kodya,keterangan from kecamatan")
        dtKelurahan = objData.SQL(GetDSN, "select kode,kecamatan,kodya,keterangan,kodepos from kelurahan")
    End Sub

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Dim cField As String = "kode,keterangan"
            Dim cWhere As String = "left(kode,1)='1'"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "kodya", cField, , , , cWhere, "kode")
            GridControl1.DataSource = cDataSet.Tables("kodya")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "kode", , , 80)
            GridColumnFormat(GridView1, "keterangan", , , -2)

            Dim cDataSet2 As New DataSet
            cField = "kode,kodya,keterangan"
            cWhere = "left(kode,1)='2'"
            cDataSet2.Clear()
            cDataSet2 = objData.BrowseDataSet(GetDSN, "kecamatan", cField, , , , cWhere, "kode")
            GridControl2.DataSource = cDataSet2.Tables("kecamatan")
            cDataSet2.Dispose()
            InitGrid(GridView2)
            GridColumnFormat(GridView2, "kode", , , 80)
            GridColumnFormat(GridView2, "kodya", , , 80)
            GridColumnFormat(GridView2, "keterangan", , , -2)

            Dim cDataSet3 As New DataSet
            cField = "kode,kecamatan,kodya,keterangan,kodepos"
            cWhere = "left(kode,1)='3'"
            cDataSet3.Clear()
            cDataSet3 = objData.BrowseDataSet(GetDSN, "kelurahan", cField, , , , cWhere, "kode")
            GridControl3.DataSource = cDataSet3.Tables("kelurahan")
            cDataSet3.Dispose()
            InitGrid(GridView3)
            GridColumnFormat(GridView3, "kode", , , 80)
            GridColumnFormat(GridView3, "kecamatan", , , 80)
            GridColumnFormat(GridView3, "kodya", , , 80)
            GridColumnFormat(GridView3, "keterangan", , , -2)
            GridColumnFormat(GridView3, "kodepos", , , 80)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        Select Case tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
            Case Is = "1"
                Return ValidDetail(cKode, cKeterangan)
            Case Is = "2"
                Return ValidDetail(cKode1, cKeterangan1)
            Case Is = "3"
                Return ValidDetail(cKode2, cKeterangan2)
            Case Else
                Exit Select
        End Select
    End Function

    Private Function ValidDetail(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit) As Boolean
        Return True
        If Trim(cKodeTemp.Text) = "" Then
            MessageBox.Show("Kode Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cKodeTemp.Focus()
            Exit Function
        End If
        If Not CheckData(cKeteranganTemp, "Keterangan Tidak Boleh Kosong.") Then
            Return False
            cKeteranganTemp.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        cKode1.Text = ""
        cKeterangan1.Text = ""
        cKota.EditValue = ""
        cNamaKota.Text = ""
        cKode2.Text = ""
        cKeterangan2.Text = ""
        cKecamatan.EditValue = ""
        cNamaKecamatan.Text = ""
        cKota1.EditValue = ""
        cNamaKota1.Text = ""
        cKodePos.Text = ""
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        GridControl1.Enabled = lPar
        PanelControl2.Enabled = lPar
        GridControl2.Enabled = lPar
        PanelControl3.Enabled = lPar
        GridControl3.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
    End Sub

    Private Sub GetMemory(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit, ByVal cTable As String)
        Try
            Dim cField As String = ""
            If cTable = "kodya" Then
                cField = "kode,keterangan"
            ElseIf cTable = "kecamatan" Then
                cField = "kode,kodya,keterangan"
            ElseIf cTable = "kelurahan" Then
                cField = "kode,kecamatan,kodya,keterangan,kodepos"
            End If
            Using cDataTable = objData.Browse(GetDSN, cTable, cField, "kode", data.myOperator.Assign, cKodeTemp.Text)
                If cDataTable.Rows.Count > 0 Then
                    With cDataTable.Rows(0)
                        cKodeTemp.Text = .Item("kode").ToString
                        cKeteranganTemp.Text = .Item("keterangan").ToString
                        If cTable = "kecamatan" Then
                            cKota.EditValue = .Item("kodya").ToString
                            nBaris = dtKota.Select(String.Format("kode = {0} ", cKota.Text))
                            If nBaris.Count > 0 Then
                                cNamaKota.Text = nBaris(0).Item("keterangan").ToString
                            End If
                        ElseIf cTable = "kelurahan" Then
                            cKota1.EditValue = .Item("kodya").ToString
                            nBaris = dtKota.Select(String.Format("kode = {0}", cKota1.Text))
                            If nBaris.Count > 0 Then
                                cNamaKota1.Text = nBaris(0).Item("Keterangan").ToString
                            End If
                            cKecamatan.EditValue = .Item("kecamatan").ToString
                            nBaris = dtKecamatan.Select(String.Format("kode = {0} ", cKecamatan.Text))
                            If nBaris.Count > 0 Then
                                cNamaKecamatan.Text = nBaris(0).Item("keterangan").ToString
                            End If
                        End If
                    End With
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1)
                Select Case cTemp
                    Case Is = "1"
                        HapusData(cKode)
                    Case Is = "2"
                        HapusData(cKode1)
                    Case Is = "3"
                        HapusData(cKode2)
                    Case Else
                        Exit Select
                End Select
                GetData()
                GetEdit(False)
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub HapusData(ByVal cKodeTemp As TextEdit)
        objData.Delete(GetDSN, "rekening", "kode", MySQL_Data_Library.data.myOperator.Assign, cKodeTemp.Text)
    End Sub

    Shared Sub InitForm(ByVal cForm As Form, Optional ByVal lMaximize As Boolean = False, Optional ByVal lMinimize As Boolean = False, _
                 Optional ByVal cmdBatal As SimpleButton = Nothing)
        With cForm
            .KeyPreview = True
            .MaximizeBox = lMaximize
            .MinimizeBox = lMinimize
            .CancelButton = cmdBatal
        End With
    End Sub

    Private Sub mstCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, , , cmdKeluar)
        InitData()
        GetEdit(False)
        GetData()
        InitValue()
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi, cmdPreview)
        GetDataLookup("kodya", cKota)
        GetDataLookup("kodya", cKota1)
        GetDataLookup("kecamatan", cKecamatan)

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cKode1.TabIndex, n)
        SetTabIndex(cKeterangan1.TabIndex, n)
        SetTabIndex(cKode2.TabIndex, n)
        SetTabIndex(cKeterangan2.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdPreview.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        cKode1.EnterMoveNextControl = True
        cKeterangan1.EnterMoveNextControl = True
        cKode2.EnterMoveNextControl = True
        cKeterangan2.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
                    Select Case cTemp
                        Case Is = "1"
                            SimpanData(cKode, cKeterangan)
                        Case Is = "2"
                            SimpanData(cKode1, cKeterangan1)
                        Case Is = "3"
                            SimpanData(cKode2, cKeterangan2)
                        Case Else
                            Exit Select
                    End Select
                    GetEdit(False)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit)
        Dim cKodeTemp1 As String = GetInduk(cKodeTemp.Text)
        Dim cWhere As String = String.Format("kode = '{0}'", cKodeTemp1)
        Dim vaField() As Object = {"kode", "keterangan"}
        Dim vaValue() As Object = {cKodeTemp1, cKeteranganTemp.Text}
        objData.Update(GetDSN, "rekening", cWhere, vaField, vaValue)
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            GetDataKode(cKode, cKeterangan, "kodya")
        End If
    End Sub

    Private Sub GetDataKode(ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit, ByVal cTable As String)
        Try
            If cKodeTemp.Text <> "" Then
                Using cDataTable = objData.Browse(GetDSN, cTable, , "kode", data.myOperator.Assign, cKodeTemp.Text)
                    If cDataTable.Rows.Count > 0 Then
                        If nPos = myPos.add Then
                            MessageBox.Show("Kode sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                        GetMemory(cKodeTemp, cKeteranganTemp, cTable)
                        If nPos = myPos.delete Then
                            DeleteData()
                        End If
                    ElseIf nPos <> myPos.add Then
                        MessageBox.Show("Data tidak ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        If nPos = myPos.delete Then
            DeleteData()
        End If
    End Sub

    Private Sub cKode1_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode1.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            GetDataKode(cKode, cKeterangan, "kecamatan")
        End If
    End Sub

    Private Sub cKode2_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode2.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            GetDataKode(cKode, cKeterangan, "kelurahan")
        End If
    End Sub

    Private Sub GetDataGrid(ByVal gView As DevExpress.XtraGrid.Views.Grid.GridView, ByVal nTipe As Integer, _
                            ByVal cKodeTemp As TextEdit, ByVal cKeteranganTemp As TextEdit)
        Try
            Dim row As DataRow = gView.GetDataRow(gView.FocusedRowHandle)
            cKodeTemp.Text = row(0).ToString
            cKeteranganTemp.Text = row(1).ToString
            Select Case nTipe
                Case 2
                    cKota.EditValue = row(1).ToString
                    nBaris = dtKota.Select(String.Format("kode = '{0}'", cKota.EditValue))
                    If nBaris.Count > 0 Then
                        cNamaKota.Text = nBaris(0).Item("keterangan").ToString
                    End If
                    cKeteranganTemp.Text = row(2).ToString
                Case 3
                    cKecamatan.EditValue = row(1).ToString
                    nBaris = dtKecamatan.Select(String.Format("kode = '{0}' ", cKecamatan.EditValue))
                    If nBaris.Count > 0 Then
                        cNamaKecamatan.Text = nBaris(0).Item("keterangan").ToString
                    End If
                    cKota1.EditValue = row(2).ToString
                    nBaris = dtKota.Select(String.Format("kode = '{0}'", cKota1.EditValue))
                    If nBaris.Count > 0 Then
                        cNamaKota1.Text = nBaris(0).Item("keterangan").ToString
                    End If
                    cKeteranganTemp.Text = row(3).ToString
                    cKodePos.Text = row(4).ToString
                Case Else
                    Exit Select
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
        GetDataGrid(GridView1, CInt(cTemp), cKode, cKeterangan)
    End Sub

    Private Sub GridView2_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView2.FocusedRowChanged
        Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
        GetDataGrid(GridView2, CInt(cTemp), cKode1, cKeterangan1)
    End Sub

    Private Sub GridView3_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView3.FocusedRowChanged
        Dim cTemp As String = tabControl1.SelectedTabPage.Text.Substring(0, 1).ToString
        GetDataGrid(GridView3, CInt(cTemp), cKode2, cKeterangan2)
    End Sub
End Class