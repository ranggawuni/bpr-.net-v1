﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstJaminan
    ReadOnly objData As New data
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim lEditGrid As Boolean
    Dim dtData As New DataTable
    Dim dtArray As New DataTable

    Private Sub InitValue()
        cKode.Text = ""
        cKeterangan.Text = ""
        nPersen.Text = "0"
        cJaminanBI.EditValue = ""
        cNamaJaminanBI.Text = ""
        cKeteranganTambahan.Text = ""
    End Sub

    Private Sub SetingTabIndex()
        Dim n As Integer
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(nPersen.TabIndex, n)
        SetTabIndex(cJaminanBI.TabIndex, n)
        SetTabIndex(cKeteranganTambahan.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        nPersen.EnterMoveNextControl = True
        cJaminanBI.EnterMoveNextControl = True
        cKeteranganTambahan.EnterMoveNextControl = True

        FormatTextBox(nPersen, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub DeleteData()
        If MsgBox("Data Dihapus ?", vbYesNo) = vbYes Then
            objData.Delete(GetDSN(), "gAgunan", "kode", , cKode.Text)
        End If
        InitValue()
        GetEdit(False)
        GetSQL()
    End Sub

    Private Sub SimpanData()
        Dim cKolom As String = ""
        Dim n As Integer = 0
        For n = 0 To GridView1.RowCount - 1
            Dim cTemp As String = GridView1.GetRowCellValue(n, "cKolom").ToString
            If cTemp <> "" Then
                If n = 0 Then
                    cKolom = cTemp.Trim
                Else
                    cKolom = cKolom & cTemp.Trim
                End If
            End If
        Next

        Dim vaField() As Object = {"kode", "keterangan", "KeteranganTambahan", _
                                   "SandiBI", "Kolom", "Diperhitungkan"}
        Dim vaValue() As Object = {cKode.Text, cKeterangan.Text, cKeteranganTambahan.Text, _
                                   cJaminanBI.Text, cKolom, Val(nPersen.EditValue)}

        objData.Update(GetDSN, "gagunan", String.Format("kode='{0}'", cKode.Text), vaField, vaValue)

        InitValue()
        GetEdit(False)
        GetSQL()
    End Sub

    Private Sub InitTable()
        AddColumn(dtArray, "cKolom", System.Type.GetType("System.String"), True)
        AddColumn(dtArray, "cKeterangan", System.Type.GetType("System.String"), True)
    End Sub

    Private Sub GetMemory()
        Dim n As Integer
        Dim cSQL As String = "select g.kode,g.keterangan,g.sandibi,g.kolom,g.keterangantambahan,g.diperhitungkan,"
        cSQL = cSQL & " j.keterangan as NamaJaminanBI"
        cSQL = cSQL & " from gagunan g"
        cSQL = cSQL & " left join jaminanBI j on j.kode = g.sandiBI"
        cSQL = String.Format("{0} where g.kode='{1}'", cSQL, cKode.Text)
        dtData = objData.SQL(GetDSN, cSQL)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cKeterangan.Text = .Item("Keterangan").ToString
                cKeteranganTambahan.Text = .Item("KeteranganTambahan").ToString
                nPersen.Text = .Item("Diperhitungkan").ToString
                cJaminanBI.Text = .Item("SandiBI").ToString
                cNamaJaminanBI.Text = GetNull(.Item("NamaJaminanBI").ToString, "").ToString
                Dim cKolom() As Object = Split(GetNull(.Item("Kolom").ToString, "").ToString, "~")
                For n = 0 To UBound(cKolom)
                    dtArray.Rows.Add(New Object() {cKolom(n), ""})
                Next
            End With
        End If
        GridControl1.DataSource = dtArray
    End Sub

    Function Gabung(Teks1 As String, Teks2 As String) As String
        Dim S1 As Integer
        If Teks1 = "" And Teks2 = "" Then
            Gabung = ""
        Else
            S1 = 47 - Len(RTrim(Teks1))
            Gabung = RTrim(Teks1) + Replicate(" ", S1) + Trim(Teks2)
        End If
    End Function

    Private Sub GetSQL()
        Dim dsData As New DataSet
        dsData = objData.BrowseDataSet(GetDSN, "Gagunan", "Kode,Keterangan", , , , , "Kode")
        GridControl2.DataSource = dsData.Tables("Gagunan")
        dsData.Dispose()
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Kode", , , 50)
        GridColumnFormat(GridView1, "Keterangan")
    End Sub

    Private Sub GetEdit(lPar As Boolean)
        lEdit = lPar
        lEditGrid = Not lPar
        PanelControl1.Enabled = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub MstJaminan_Load(sender As Object, e As EventArgs) Handles Me.Load
        CenterFormManual(Me)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        InitValue()
        InitTable()
        SetingTabIndex()
        GetSQL()
        GetEdit(False)
        GetDataLookup("jaminanBI", cJaminanBI)
    End Sub

    Private Sub cJaminanBI_Closed(sender As Object, e As ClosedEventArgs) Handles cJaminanBI.Closed
        GetKeteranganLookUp(cJaminanBI, cNamaJaminanBI)
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim cDataTable = objData.Browse(GetDSN, "gagunan", "kode,keterangan", "kode", data.myOperator.Assign, cKode.Text, , "kode")
            With cDataTable
                If .Rows.Count > 0 Then
                    If nPos = myPos.add Then
                        MessageBox.Show("Kode Sudah Dipakai, Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                Else
                    MessageBox.Show("Kode Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        GetEdit(True)
        InitValue()
        cKode.Focus()
        nPos = myPos.add
    End Sub

    Private Sub cmdEdit_Click(sender As Object, e As EventArgs) Handles cmdEdit.Click
        GetEdit(True)
        nPos = myPos.edit
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub
End Class