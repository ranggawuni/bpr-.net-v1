﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class MstAdmKredit
    ReadOnly objData As New data
    Dim dtData As New DataTable
    Dim dtArray As New DataTable

    Private Sub InitTable()
        AddColumn(dtArray, "dTgl", System.Type.GetType("System.Date"), True)
        AddColumn(dtArray, "nLama", System.Type.GetType("System.Double"), True)
        AddColumn(dtArray, "nMinimum", System.Type.GetType("System.Double"), True)
        AddColumn(dtArray, "nMaksimum", System.Type.GetType("System.Double"), True)
        AddColumn(dtArray, "nNominal", System.Type.GetType("System.Double"), True)
        AddColumn(dtArray, "nPersentase", System.Type.GetType("System.Double"), True)
    End Sub

    Private Sub InitValue()
        cKode.EditValue = ""
        cKeterangan.Text = ""
        dTgl.DateTime = Now.Date
        nLama.Text = "0"
        nMinimum.Text = "0"
        nMaksimum.Text = "0"
        nNominal.Text = "0"
        nPersentase.Text = "0"
    End Sub

    Private Sub MstAdmKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("golongankredit", cKode)
    End Sub

    Private Sub MstAdmKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , cmdHapus, cmdSimpan, cmdKeluar, , , cmdOK)
        InitValue()
        InitTable()

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nMinimum.TabIndex, n)
        SetTabIndex(nMaksimum.TabIndex, n)
        SetTabIndex(nNominal.TabIndex, n)
        SetTabIndex(nPersentase.TabIndex, n)
        SetTabIndex(cmdOK.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cKode.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nMinimum.EnterMoveNextControl = True
        nMaksimum.EnterMoveNextControl = True
        nNominal.EnterMoveNextControl = True
        nPersentase.EnterMoveNextControl = True

        FormatTextBox(nLama, formatType.BilRpPict, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nMinimum, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nMaksimum, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nNominal, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
        FormatTextBox(nPersentase, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub GetSQL()
        Dim n As Integer
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "tgl,lama,minimum,maximum,nominal,persen"
            Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "detailadm", cField, "kode", , cKode.Text, cWhere, "tgl desc")
            GridControl1.DataSource = cDataSet.Tables("detailadm")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Tgl", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, 50)
            GridColumnFormat(GridView1, "Lama", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Minimum", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Maximum", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "Nominal", , , 50, DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView1, "persen", , , 50, DevExpress.Utils.HorzAlignment.Far)

            dtData = objData.Browse(GetDSN, "detailadm", cField, "kode", , cKode.Text, cWhere, "tgl desc")
            For n = 0 To dtData.Rows.Count - 1
                With dtData.Rows(n)
                    dtArray.Rows.Add(New Object() {.Item("tgl"), .Item("lama"), .Item("minimum"), .Item("maximum"),
                                                   .Item("nominal"), .Item("persen")})
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cKode_Closed(sender As Object, e As ClosedEventArgs) Handles cKode.Closed
        GetKeteranganLookUp(cKode, cKeterangan)
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetSQL()
        End If
    End Sub

    Private Sub HapusData()
        If GridView1.RowCount >= 1 Then
            If MessageBox.Show("Data Benar-benar Dihapus ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                Dim cWhere As String = String.Format("and tgl='{0}' and minimum= {1} and maximum ={2}", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd), Val(nMinimum.Text), Val(nMaksimum.Text))
                cWhere = String.Format("{0} and lama = {1}", cWhere, Val(nLama.Text))
                cWhere = String.Format("{0} and Nominal = {1}", cWhere, Val(nNominal.Text))
                cWhere = String.Format("{0} and persen = {1}", cWhere, Val(nPersentase.Text))
                objData.Delete(GetDSN, "detailadm", "kode", , cKode.Text, cWhere)
                GetSQL()
                dTgl.DateTime = Now.Date
                nLama.Text = "0"
                nMinimum.Text = "0"
                nMaksimum.Text = "0"
                nNominal.Text = "0"
                nPersentase.Text = "0"
            End If
        End If
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        HapusData()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        dtArray.Rows.Add(New Object() {dTgl.DateTime.Date, Val(nLama.Text), Val(nMinimum.Text), Val(nMaksimum.Text), Val(nNominal.Text), Val(nPersentase.Text)})
        nMinimum.Focus()
    End Sub

    Private Sub SimpanData()
        Dim n As Integer
        Dim cWhere As String = String.Format(" and Tgl = '{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
        objData.Delete(GetDSN, "detailadm", "Kode", , cKode.Text, cWhere)

        Dim vaField() As Object = {"Kode", "Tgl", "lama", "Minimum", "Maximum", "Nominal", "Persen"}
        Dim vaValue() As Object
        For n = 0 To dtArray.Rows.Count - 1
            With dtArray.Rows(n)
                vaValue = {cKode.Text, .Item(0), .Item(1), .Item(2), .Item(3), .Item(4), .Item(5)}
                objData.Add(GetDSN, "DetailAdm", vaField, vaValue)
            End With
        Next
        InitValue()
        cKode.Focus()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            dTgl.DateTime = CDate(row(0).ToString)
            nLama.Text = row(1).ToString
            nMinimum.Text = row(2).ToString
            nMaksimum.Text = row(3).ToString
            nNominal.Text = row(4).ToString
            nPersentase.Text = row(5).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class