﻿Imports bpr.MySQL_Data_Library

Public Class cfgSystem
    Dim objData As New data

    Private Sub InitValue()
        cCabang.EditValue = CStr(aCfg(eCfg.msKodeCabang))
        cGolonganNasabah.EditValue = aCfg(eCfg.msKodeGolonganNasabahTerkait).ToString
        cRekeningKas.EditValue = aCfg(eCfg.msKodeKas).ToString
        cRekeningPB.EditValue = aCfg(eCfg.msRekeningPemindahBukuan).ToString
        cRekeningLaba.EditValue = aCfg(eCfg.msKodeLaba).ToString
        cRekeningLabaTahunLalu.EditValue = aCfg(eCfg.msKodeLabaTahunLalu).ToString
        cRekeningLabaPenjualanAktiva.EditValue = aCfg(eCfg.msRekPenjualanAktiva).ToString
        cRekeningTaksiranPajak.EditValue = aCfg(eCfg.msRekeningJurnalTaksiranPajak).ToString
        cRekeningTaksiranPPh25.EditValue = aCfg(eCfg.msKodeTaksiranPphPasal25).ToString
        cRekeningAntarKantorAktiva.EditValue = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString
        cRekeningAntarKantorPasiva.EditValue = aCfg(eCfg.msRekeningAntarKantorPasiva).ToString

        GetKeteranganLookUp(cCabang, cNamaCabang)
        GetKeteranganLookUp(cGolonganNasabah, cNamaGolonganNasabah)
        GetKeteranganLookUp(cRekeningKas, cNamaRekeningKas)
        GetKeteranganLookUp(cRekeningPB, cNamaRekeningPB)
        GetKeteranganLookUp(cRekeningLaba, cNamaRekeningLaba)
        GetKeteranganLookUp(cRekeningLabaTahunLalu, cNamaRekeningLabaTahunLalu)
        GetKeteranganLookUp(cRekeningLabaPenjualanAktiva, cNamaRekeningLabaPenjualanAktiva)
        GetKeteranganLookUp(cRekeningTaksiranPajak, cNamaRekeningTaksiranPajak)
        GetKeteranganLookUp(cRekeningTaksiranPPh25, cNamaRekeningTaksiranPPh25)
        GetKeteranganLookUp(cRekeningAntarKantorAktiva, cNamaRekeningAntarKantorAktiva)
        GetKeteranganLookUp(cRekeningAntarKantorPasiva, cNamaRekeningAntarKantorPasiva)
    End Sub

    Private Sub cfgSystem_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("cabang", cCabang)
        GetDataLookup("golonganNasabah", cGolonganNasabah)
        GetDataLookup("rekening", cRekeningKas, "left(kode,1)='1'")
        GetDataLookup("rekening", cRekeningPB, "left(kode,1)<3")
        GetDataLookup("rekening", cRekeningLaba, "left(kode,1)='3'")
        GetDataLookup("rekening", cRekeningLabaTahunLalu, "left(kode,1)='3'")
        GetDataLookup("rekening", cRekeningLabaPenjualanAktiva)
        GetDataLookup("rekening", cRekeningTaksiranPPh25)
        GetDataLookup("rekening", cRekeningTaksiranPajak)
        GetDataLookup("rekening", cRekeningAntarKantorAktiva)
        GetDataLookup("rekening", cRekeningAntarKantorPasiva)
    End Sub

    Private Sub cfgSystem_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim n As Integer

        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me)
        InitValue()

        SetTabIndex(cCabang.TabIndex, n)
        SetTabIndex(cGolonganNasabah.TabIndex, n)
        SetTabIndex(cRekeningKas.TabIndex, n)
        SetTabIndex(cRekeningPB.TabIndex, n)
        SetTabIndex(cRekeningLaba.TabIndex, n)
        SetTabIndex(cRekeningLabaTahunLalu.TabIndex, n)
        SetTabIndex(cRekeningLabaPenjualanAktiva.TabIndex, n)
        SetTabIndex(cRekeningTaksiranPPh25.TabIndex, n)
        SetTabIndex(cRekeningTaksiranPajak.TabIndex, n)
        SetTabIndex(cRekeningAntarKantorAktiva.TabIndex, n)
        SetTabIndex(cRekeningAntarKantorPasiva.TabIndex, n)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cCabang.EnterMoveNextControl = True
        cGolonganNasabah.EnterMoveNextControl = True
        cRekeningKas.EnterMoveNextControl = True
        cRekeningPB.EnterMoveNextControl = True
        cRekeningLaba.EnterMoveNextControl = True
        cRekeningLabaTahunLalu.EnterMoveNextControl = True
        cRekeningLabaPenjualanAktiva.EnterMoveNextControl = True
        cRekeningTaksiranPPh25.EnterMoveNextControl = True
        cRekeningTaksiranPajak.EnterMoveNextControl = True
        cRekeningAntarKantorAktiva.EnterMoveNextControl = True
        cRekeningAntarKantorPasiva.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        UpdCfg(eCfg.msKodeCabang, cCabang.Text, cCabang.Text)
        UpdCfg(eCfg.msKodeGolonganNasabahTerkait, cGolonganNasabah.Text, cCabang.Text)
        UpdCfg(eCfg.msKodeKas, cRekeningKas.Text, cCabang.Text)
        UpdCfg(eCfg.msRekeningPemindahBukuan, cRekeningPB.Text, cCabang.Text)
        UpdCfg(eCfg.msKodeLaba, cRekeningLaba.Text, cCabang.Text)
        UpdCfg(eCfg.msKodeLabaTahunLalu, cRekeningLabaTahunLalu.Text, cCabang.Text)
        UpdCfg(eCfg.msRekPenjualanAktiva, cRekeningLabaPenjualanAktiva.Text, cCabang.Text)
        UpdCfg(eCfg.msKodeTaksiranPphPasal25, cRekeningTaksiranPPh25.Text, cCabang.Text)
        UpdCfg(eCfg.msRekeningJurnalTaksiranPajak, cRekeningTaksiranPajak.Text, cCabang.Text)
        UpdCfg(eCfg.msRekeningAntarKantorAktiva, cRekeningAntarKantorAktiva.Text, cCabang.Text)
        UpdCfg(eCfg.msRekeningAntarKantorPasiva, cRekeningAntarKantorPasiva.Text, cCabang.Text)
    End Sub

    Private Sub cCabang_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cCabang.Closed
        GetKeteranganLookUp(cCabang, cNamaCabang)
    End Sub

    Private Sub cCabang_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cCabang.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cCabang, cNamaCabang)
        End If
    End Sub

    Private Sub cGolonganNasabah_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cGolonganNasabah.Closed
        GetKeteranganLookUp(cGolonganNasabah, cNamaGolonganNasabah)
    End Sub

    Private Sub cGolonganNasabah_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganNasabah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganNasabah, cNamaGolonganNasabah)
        End If
    End Sub

    Private Sub cRekeningAntarKantorAktiva_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningAntarKantorAktiva.Closed
        GetKeteranganLookUp(cRekeningAntarKantorAktiva, cNamaRekeningAntarKantorAktiva)
    End Sub

    Private Sub cRekeningAntarKantorAktiva_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningAntarKantorAktiva.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningAntarKantorAktiva, cNamaRekeningAntarKantorAktiva)
        End If
    End Sub

    Private Sub cRekeningAntarKantorPasiva_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningAntarKantorPasiva.Closed
        GetKeteranganLookUp(cRekeningAntarKantorPasiva, cNamaRekeningAntarKantorPasiva)
    End Sub

    Private Sub cRekeningAntarKantorPasiva_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningAntarKantorPasiva.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningAntarKantorPasiva, cNamaRekeningAntarKantorPasiva)
        End If
    End Sub

    Private Sub cRekeningKas_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningKas.Closed
        GetKeteranganLookUp(cRekeningKas, cNamaRekeningKas)
    End Sub

    Private Sub cRekeningKas_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningKas.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningKas, cNamaRekeningKas)
        End If
    End Sub

    Private Sub cRekeningLaba_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningLaba.Closed
        GetKeteranganLookUp(cRekeningLaba, cNamaRekeningLaba)
    End Sub

    Private Sub cRekeningLaba_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningLaba.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningLaba, cNamaRekeningLaba)
        End If
    End Sub

    Private Sub cRekeningLabaPenjualanAktiva_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningLabaPenjualanAktiva.Closed
        GetKeteranganLookUp(cRekeningLabaPenjualanAktiva, cNamaRekeningLabaPenjualanAktiva)
    End Sub

    Private Sub cRekeningLabaPenjualanAktiva_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningLabaPenjualanAktiva.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningLabaPenjualanAktiva, cNamaRekeningLabaPenjualanAktiva)
        End If
    End Sub

    Private Sub cRekeningLabaTahunLalu_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningLabaTahunLalu.Closed
        GetKeteranganLookUp(cRekeningLabaTahunLalu, cNamaRekeningLabaTahunLalu)
    End Sub

    Private Sub cRekeningLabaTahunLalu_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningLabaTahunLalu.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningLabaTahunLalu, cNamaRekeningLabaTahunLalu)
        End If
    End Sub

    Private Sub cRekeningPB_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningPB.Closed
        GetKeteranganLookUp(cRekeningPB, cNamaRekeningPB)
    End Sub

    Private Sub cRekeningPB_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningPB.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningPB, cNamaRekeningPB)
        End If
    End Sub

    Private Sub cRekeningTaksiranPajak_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningTaksiranPajak.Closed
        GetKeteranganLookUp(cRekeningTaksiranPajak, cNamaRekeningTaksiranPajak)
    End Sub

    Private Sub cRekeningTaksiranPajak_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningTaksiranPajak.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningTaksiranPajak, cNamaRekeningTaksiranPajak)
        End If
    End Sub

    Private Sub cRekeningTaksiranPPh25_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekeningTaksiranPPh25.Closed
        GetKeteranganLookUp(cRekeningTaksiranPPh25, cNamaRekeningTaksiranPPh25)
    End Sub

    Private Sub cRekeningTaksiranPPh25_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningTaksiranPPh25.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cRekeningTaksiranPPh25, cNamaRekeningTaksiranPPh25)
        End If
    End Sub
End Class