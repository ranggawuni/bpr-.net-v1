﻿Imports bpr.MySQL_Data_Library
Imports bpr.clsMD5

Public Class frmGantiPassword
    ReadOnly objData As New data()
    Private ReadOnly md5k As New clsMD5()

    Private Shared Sub GS(obj As DevExpress.XtraEditors.TextEdit)
        obj.SelectionStart = 0
        obj.SelectionLength = Len(obj.Text)
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        Dim dbData As New DataTable
        Dim cPassBaru As String = md5k.MD5(cNewPassword.Text)
        Dim cPassLama As String = md5k.MD5(cOldPassword.Text)
        dbData = objData.SQL(GetDSN, String.Format("select userpassword from username where username = '{0}'", cUserName))
        If dbData.Rows.Count > 0 Then
            Dim cPassAsal As String = dbData.Rows(0).Item("userpassword").ToString
            If cPassLama <> cPassAsal Then
                MessageBox.Show("Password Tidak Sama Dengan Password Asal !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cOldPassword.Focus()
            ElseIf cNewPassword.Text = "" Or (cPassBaru = "" And Trim(cKonfirmasi.Text) = "") Then
                MessageBox.Show("Password Tidak Boleh Kosong !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cNewPassword.Focus()
            ElseIf Trim(cNewPassword.Text) <> Trim(cKonfirmasi.Text) Then
                MessageBox.Show("Password Tidak sama Dengan Konfirmasi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cNewPassword.Focus()
            End If
            If cNewPassword.Text = cOldPassword.Text Then
                MsgBox("Password Tidak boleh sama Dengan password sebelumnya!", vbCritical)
                ValidSaving = False
                cNewPassword.Focus()
            End If
        End If
    End Function

    Private Sub InitValue()
        cOldPassword.Text = ""
        cNewPassword.Text = ""
        cKonfirmasi.Text = ""
    End Sub

    Private Sub frmGantiPassword_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        InitForm(Me)
        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)

        SetTabIndex(cOldPassword.TabIndex, n)
        SetTabIndex(cNewPassword.TabIndex, n)
        SetTabIndex(cKonfirmasi.TabIndex, n)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cOldPassword.EnterMoveNextControl = True
        cNewPassword.EnterMoveNextControl = True
        cKonfirmasi.EnterMoveNextControl = True
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        Dim dbData As New DataTable
        Try
            Dim cNew As String
            If ValidSaving() Then
                cNew = md5k.MD5(cNewPassword.Text)
                nBulanExpired = 2
                dbData = objData.Browse(GetDSN, "username", "bulanexpired", "username", , GetUserName(1))
                If dbData.Rows.Count > 0 Then
                    nBulanExpired = CInt(GetNull(dbData.Rows(0).Item("bulanexpired"), 2))
                End If
                Dim vaField() As Object = {"UserPassword", "expired"}
                Dim vaValue() As Object = {cNew, formatValue(DateAdd(DateInterval.Month, nBulanExpired, Date.Today), formatType.yyyy_MM_dd)}
                Dim cWhere As String = String.Format("UserName = '{0}'", GetUserName(1))
                objData.Edit(GetDSN, "UserName", cWhere, vaField, vaValue)
                MessageBox.Show("Password Baru Anda Telah Aktif" & vbCrLf & "Untuk Login Lagi Gunakan Password Baru Anda", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                GetUserName(2, cNewPassword.Text, True)
                InitValue()
                cOldPassword.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        dbData.Dispose()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cNewPassword_GotFocus(sender As Object, e As EventArgs) Handles cNewPassword.GotFocus
        GS(cNewPassword)
    End Sub

    Private Sub cNewPassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cNewPassword.KeyPress
        GK(Microsoft.VisualBasic.AscW(e.KeyChar))
    End Sub

    Private Sub cOldPassword_GotFocus(sender As Object, e As EventArgs) Handles cOldPassword.GotFocus
        GS(cOldPassword)
    End Sub

    Private Sub cOldPassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cOldPassword.KeyPress
        GK(Microsoft.VisualBasic.AscW(e.KeyChar))
    End Sub

    Private Shared Sub GK(KeyAscii As Integer)
        KeyAscii = Asc(LCase(Chr(KeyAscii)))
        If KeyAscii = 13 Then
            SendKeys.Send("{tab}")
        End If
    End Sub
End Class