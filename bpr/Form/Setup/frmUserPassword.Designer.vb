﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtTable IsNot Nothing Then
                    dtTable.Dispose()
                    dtTable = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserPassword))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cPassword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cUserID = New DevExpress.XtraEditors.TextEdit()
        Me.cFullName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nUserLevel = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cUserID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFullName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nUserLevel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.cmdKeluar)
        Me.PanelControl5.Controls.Add(Me.cmdSimpan)
        Me.PanelControl5.Controls.Add(Me.cmdHapus)
        Me.PanelControl5.Controls.Add(Me.cmdAdd)
        Me.PanelControl5.Location = New System.Drawing.Point(2, 342)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(365, 33)
        Me.PanelControl5.TabIndex = 16
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Image = CType(resources.GetObject("cmdKeluar.Image"), System.Drawing.Image)
        Me.cmdKeluar.Location = New System.Drawing.Point(285, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 24)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(80, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 24)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(209, 5)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(5, 5)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(2, 117)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(365, 219)
        Me.GridControl1.TabIndex = 15
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nUserLevel)
        Me.PanelControl2.Controls.Add(Me.cFullName)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.cPassword)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.cUserID)
        Me.PanelControl2.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(365, 108)
        Me.PanelControl2.TabIndex = 14
        '
        'cPassword
        '
        Me.cPassword.Location = New System.Drawing.Point(80, 31)
        Me.cPassword.Name = "cPassword"
        Me.cPassword.Size = New System.Drawing.Size(102, 20)
        Me.cPassword.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Password"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "User ID"
        '
        'cUserID
        '
        Me.cUserID.EditValue = "12"
        Me.cUserID.Location = New System.Drawing.Point(80, 8)
        Me.cUserID.Name = "cUserID"
        Me.cUserID.Properties.MaxLength = 2
        Me.cUserID.Size = New System.Drawing.Size(102, 20)
        Me.cUserID.TabIndex = 0
        '
        'cFullName
        '
        Me.cFullName.Location = New System.Drawing.Point(80, 57)
        Me.cFullName.Name = "cFullName"
        Me.cFullName.Size = New System.Drawing.Size(275, 20)
        Me.cFullName.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 60)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Full Name"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(7, 86)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl2.TabIndex = 7
        Me.LabelControl2.Text = "Menu Level"
        '
        'nUserLevel
        '
        Me.nUserLevel.EditValue = "12"
        Me.nUserLevel.Location = New System.Drawing.Point(80, 83)
        Me.nUserLevel.Name = "nUserLevel"
        Me.nUserLevel.Properties.MaxLength = 2
        Me.nUserLevel.Size = New System.Drawing.Size(23, 20)
        Me.nUserLevel.TabIndex = 6
        '
        'frmUserPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(369, 376)
        Me.Controls.Add(Me.PanelControl5)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "frmUserPassword"
        Me.Text = "User Password"
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cUserID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFullName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nUserLevel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cUserID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nUserLevel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cFullName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
