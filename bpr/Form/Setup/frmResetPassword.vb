﻿Imports bpr.MySQL_Data_Library
Imports bpr.clsMD5

Public Class frmResetPassword
    ReadOnly objData As New data()
    Dim dtTable As New DataTable
    Private ReadOnly md5k As New clsMD5()

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "username,fullname"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "username", cField, "kode", , cKodeAplikasi, , "level,username")
            GridControl1.DataSource = cDataSet.Tables("username")
            cDataSet.Dispose()
            InitGrid(GridView1, False, , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "username")
            GridColumnFormat(GridView1, "fullname")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub InitValue()
        dtTable.Clear()
        GridControl1.DataSource = dtTable
    End Sub

    Private Sub frmResetPassword_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetData()
        InitValue()
        InitForm(Me)
        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        Dim nRow As DataRow
        Dim vaValue() As Object
        Dim vaField() As Object
        Dim cWhere As String
        Try
            If MessageBox.Show("Proses dilanjutkan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                    If GridView1.GetSelectedRows()(n) >= 0 Then
                        nRow = GridView1.GetDataRow(n)
                        Dim cNew As String = md5k.MD5(nRow(0).ToString)
                        vaField = {"UserPassword"}
                        vaValue = {cNew}
                        cWhere = String.Format("UserName = '{0}' and Kode = 'BPR'", nRow(0))
                        objData.Edit(GetDSN, "UserName", cWhere, vaField, vaValue)
                    End If
                Next
                MessageBox.Show("Proses Selesai.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                InitValue()
                GetData()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub
End Class