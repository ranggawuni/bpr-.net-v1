﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cfgSystem
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
                objData = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cfgSystem))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cNamaCabang = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cCabang = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cNamaGolonganNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganNasabah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningKas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningKas = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningPB = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningPB = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningLaba = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningLaba = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningLabaTahunLalu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningLabaTahunLalu = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningLabaPenjualanAktiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningLabaPenjualanAktiva = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningTaksiranPPh25 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTaksiranPPh25 = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningTaksiranPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTaksiranPajak = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAntarKantorAktiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAntarKantorAktiva = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningAntarKantorPasiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAntarKantorPasiva = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cNamaCabang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cNamaGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningPB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningLaba.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningLaba.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningLabaTahunLalu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningLabaTahunLalu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningLabaPenjualanAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningLabaPenjualanAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningTaksiranPPh25.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTaksiranPPh25.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningTaksiranPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTaksiranPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAntarKantorAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAntarKantorAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAntarKantorPasiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAntarKantorPasiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAntarKantorPasiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cRekeningAntarKantorPasiva)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAntarKantorAktiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.cRekeningAntarKantorAktiva)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningTaksiranPajak)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.cRekeningTaksiranPajak)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningTaksiranPPh25)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.cRekeningTaksiranPPh25)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningLabaPenjualanAktiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.cRekeningLabaPenjualanAktiva)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningLabaTahunLalu)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.cRekeningLabaTahunLalu)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningLaba)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.cRekeningLaba)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningPB)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cRekeningPB)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningKas)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekeningKas)
        Me.PanelControl1.Controls.Add(Me.cNamaGolonganNasabah)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cGolonganNasabah)
        Me.PanelControl1.Controls.Add(Me.cNamaCabang)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cCabang)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 5)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(517, 295)
        Me.PanelControl1.TabIndex = 16
        '
        'cNamaCabang
        '
        Me.cNamaCabang.Enabled = False
        Me.cNamaCabang.Location = New System.Drawing.Point(211, 7)
        Me.cNamaCabang.Name = "cNamaCabang"
        Me.cNamaCabang.Size = New System.Drawing.Size(141, 20)
        Me.cNamaCabang.TabIndex = 110
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 10)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl10.TabIndex = 106
        Me.LabelControl10.Text = "Kode Cabang"
        '
        'cCabang
        '
        Me.cCabang.Location = New System.Drawing.Point(161, 7)
        Me.cCabang.Name = "cCabang"
        Me.cCabang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cCabang.Size = New System.Drawing.Size(44, 20)
        Me.cCabang.TabIndex = 111
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 306)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(517, 33)
        Me.PanelControl2.TabIndex = 15
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(435, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(354, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cNamaGolonganNasabah
        '
        Me.cNamaGolonganNasabah.Enabled = False
        Me.cNamaGolonganNasabah.Location = New System.Drawing.Point(211, 33)
        Me.cNamaGolonganNasabah.Name = "cNamaGolonganNasabah"
        Me.cNamaGolonganNasabah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganNasabah.TabIndex = 113
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 36)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(100, 13)
        Me.LabelControl1.TabIndex = 112
        Me.LabelControl1.Text = "Gol. Nasabah Terkait"
        '
        'cGolonganNasabah
        '
        Me.cGolonganNasabah.Location = New System.Drawing.Point(161, 33)
        Me.cGolonganNasabah.Name = "cGolonganNasabah"
        Me.cGolonganNasabah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganNasabah.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganNasabah.TabIndex = 114
        '
        'cNamaRekeningKas
        '
        Me.cNamaRekeningKas.Enabled = False
        Me.cNamaRekeningKas.Location = New System.Drawing.Point(274, 59)
        Me.cNamaRekeningKas.Name = "cNamaRekeningKas"
        Me.cNamaRekeningKas.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningKas.TabIndex = 116
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 62)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 115
        Me.LabelControl2.Text = "Rekening Kas"
        '
        'cRekeningKas
        '
        Me.cRekeningKas.Location = New System.Drawing.Point(161, 59)
        Me.cRekeningKas.Name = "cRekeningKas"
        Me.cRekeningKas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningKas.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningKas.TabIndex = 117
        '
        'cNamaRekeningPB
        '
        Me.cNamaRekeningPB.Enabled = False
        Me.cNamaRekeningPB.Location = New System.Drawing.Point(274, 85)
        Me.cNamaRekeningPB.Name = "cNamaRekeningPB"
        Me.cNamaRekeningPB.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningPB.TabIndex = 119
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 88)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl3.TabIndex = 118
        Me.LabelControl3.Text = "Rekening PB"
        '
        'cRekeningPB
        '
        Me.cRekeningPB.Location = New System.Drawing.Point(161, 85)
        Me.cRekeningPB.Name = "cRekeningPB"
        Me.cRekeningPB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningPB.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningPB.TabIndex = 120
        '
        'cNamaRekeningLaba
        '
        Me.cNamaRekeningLaba.Enabled = False
        Me.cNamaRekeningLaba.Location = New System.Drawing.Point(274, 111)
        Me.cNamaRekeningLaba.Name = "cNamaRekeningLaba"
        Me.cNamaRekeningLaba.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningLaba.TabIndex = 122
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(9, 114)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl4.TabIndex = 121
        Me.LabelControl4.Text = "Rekening Laba"
        '
        'cRekeningLaba
        '
        Me.cRekeningLaba.Location = New System.Drawing.Point(161, 111)
        Me.cRekeningLaba.Name = "cRekeningLaba"
        Me.cRekeningLaba.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLaba.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningLaba.TabIndex = 123
        '
        'cNamaRekeningLabaTahunLalu
        '
        Me.cNamaRekeningLabaTahunLalu.Enabled = False
        Me.cNamaRekeningLabaTahunLalu.Location = New System.Drawing.Point(274, 137)
        Me.cNamaRekeningLabaTahunLalu.Name = "cNamaRekeningLabaTahunLalu"
        Me.cNamaRekeningLabaTahunLalu.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningLabaTahunLalu.TabIndex = 125
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 140)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl5.TabIndex = 124
        Me.LabelControl5.Text = "Rekening Laba Tahun Lalu"
        '
        'cRekeningLabaTahunLalu
        '
        Me.cRekeningLabaTahunLalu.Location = New System.Drawing.Point(161, 137)
        Me.cRekeningLabaTahunLalu.Name = "cRekeningLabaTahunLalu"
        Me.cRekeningLabaTahunLalu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLabaTahunLalu.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningLabaTahunLalu.TabIndex = 126
        '
        'cNamaRekeningLabaPenjualanAktiva
        '
        Me.cNamaRekeningLabaPenjualanAktiva.Enabled = False
        Me.cNamaRekeningLabaPenjualanAktiva.Location = New System.Drawing.Point(274, 163)
        Me.cNamaRekeningLabaPenjualanAktiva.Name = "cNamaRekeningLabaPenjualanAktiva"
        Me.cNamaRekeningLabaPenjualanAktiva.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningLabaPenjualanAktiva.TabIndex = 128
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 166)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(109, 13)
        Me.LabelControl6.TabIndex = 127
        Me.LabelControl6.Text = "Rek. Laba Penj. Aktiva"
        '
        'cRekeningLabaPenjualanAktiva
        '
        Me.cRekeningLabaPenjualanAktiva.Location = New System.Drawing.Point(161, 163)
        Me.cRekeningLabaPenjualanAktiva.Name = "cRekeningLabaPenjualanAktiva"
        Me.cRekeningLabaPenjualanAktiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLabaPenjualanAktiva.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningLabaPenjualanAktiva.TabIndex = 129
        '
        'cNamaRekeningTaksiranPPh25
        '
        Me.cNamaRekeningTaksiranPPh25.Enabled = False
        Me.cNamaRekeningTaksiranPPh25.Location = New System.Drawing.Point(274, 189)
        Me.cNamaRekeningTaksiranPPh25.Name = "cNamaRekeningTaksiranPPh25"
        Me.cNamaRekeningTaksiranPPh25.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningTaksiranPPh25.TabIndex = 131
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 192)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl7.TabIndex = 130
        Me.LabelControl7.Text = "Rek. Taksiran PPh 25"
        '
        'cRekeningTaksiranPPh25
        '
        Me.cRekeningTaksiranPPh25.Location = New System.Drawing.Point(161, 189)
        Me.cRekeningTaksiranPPh25.Name = "cRekeningTaksiranPPh25"
        Me.cRekeningTaksiranPPh25.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTaksiranPPh25.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningTaksiranPPh25.TabIndex = 132
        '
        'cNamaRekeningTaksiranPajak
        '
        Me.cNamaRekeningTaksiranPajak.Enabled = False
        Me.cNamaRekeningTaksiranPajak.Location = New System.Drawing.Point(274, 215)
        Me.cNamaRekeningTaksiranPajak.Name = "cNamaRekeningTaksiranPajak"
        Me.cNamaRekeningTaksiranPajak.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningTaksiranPajak.TabIndex = 134
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(9, 218)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl8.TabIndex = 133
        Me.LabelControl8.Text = "Rekening Taksiran Pajak"
        '
        'cRekeningTaksiranPajak
        '
        Me.cRekeningTaksiranPajak.Location = New System.Drawing.Point(161, 215)
        Me.cRekeningTaksiranPajak.Name = "cRekeningTaksiranPajak"
        Me.cRekeningTaksiranPajak.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTaksiranPajak.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningTaksiranPajak.TabIndex = 135
        '
        'cNamaRekeningAntarKantorAktiva
        '
        Me.cNamaRekeningAntarKantorAktiva.Enabled = False
        Me.cNamaRekeningAntarKantorAktiva.Location = New System.Drawing.Point(274, 241)
        Me.cNamaRekeningAntarKantorAktiva.Name = "cNamaRekeningAntarKantorAktiva"
        Me.cNamaRekeningAntarKantorAktiva.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningAntarKantorAktiva.TabIndex = 137
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 244)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl9.TabIndex = 136
        Me.LabelControl9.Text = "Rek. Antar Kantor Aktiva"
        '
        'cRekeningAntarKantorAktiva
        '
        Me.cRekeningAntarKantorAktiva.Location = New System.Drawing.Point(161, 241)
        Me.cRekeningAntarKantorAktiva.Name = "cRekeningAntarKantorAktiva"
        Me.cRekeningAntarKantorAktiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAntarKantorAktiva.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningAntarKantorAktiva.TabIndex = 138
        '
        'cNamaRekeningAntarKantorPasiva
        '
        Me.cNamaRekeningAntarKantorPasiva.Enabled = False
        Me.cNamaRekeningAntarKantorPasiva.Location = New System.Drawing.Point(274, 267)
        Me.cNamaRekeningAntarKantorPasiva.Name = "cNamaRekeningAntarKantorPasiva"
        Me.cNamaRekeningAntarKantorPasiva.Size = New System.Drawing.Size(238, 20)
        Me.cNamaRekeningAntarKantorPasiva.TabIndex = 140
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(9, 270)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl11.TabIndex = 139
        Me.LabelControl11.Text = "Rek. Antar Kantor Pasiva"
        '
        'cRekeningAntarKantorPasiva
        '
        Me.cRekeningAntarKantorPasiva.Location = New System.Drawing.Point(161, 267)
        Me.cRekeningAntarKantorPasiva.Name = "cRekeningAntarKantorPasiva"
        Me.cRekeningAntarKantorPasiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAntarKantorPasiva.Size = New System.Drawing.Size(107, 20)
        Me.cRekeningAntarKantorPasiva.TabIndex = 141
        '
        'cfgSystem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(523, 343)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "cfgSystem"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "System Configuration"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cNamaCabang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.cNamaGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningPB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningLaba.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningLaba.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningLabaTahunLalu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningLabaTahunLalu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningLabaPenjualanAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningLabaPenjualanAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningTaksiranPPh25.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTaksiranPPh25.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningTaksiranPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTaksiranPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAntarKantorAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAntarKantorAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAntarKantorPasiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAntarKantorPasiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cNamaCabang As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cCabang As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cNamaGolonganNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganNasabah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAntarKantorPasiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAntarKantorPasiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningAntarKantorAktiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAntarKantorAktiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningTaksiranPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTaksiranPajak As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningTaksiranPPh25 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTaksiranPPh25 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningLabaPenjualanAktiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningLabaPenjualanAktiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningLabaTahunLalu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningLabaTahunLalu As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningLaba As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningLaba As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningPB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningPB As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningKas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningKas As DevExpress.XtraEditors.LookUpEdit
End Class
