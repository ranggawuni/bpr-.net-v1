﻿Imports bpr.MySQL_Data_Library

Public Class frmSetupPlafond
    ReadOnly objData As New data()
    Dim dtTable As New DataTable

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "s.User,s.Maximum"
            Const cWhere As String = "where not isnull(user) Order by s.User"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "username u", cField, , , , cWhere)
            GridControl1.DataSource = cDataSet.Tables("username")
            cDataSet.Dispose()
            InitGrid(GridView1, False, , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "user")
            GridColumnFormat(GridView1, "maximum")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub InitValue()
        cUsername.EditValue = ""
        nMax.Text = ""

        dtTable.Clear()
        GridControl1.DataSource = dtTable
    End Sub

    Private Sub DeleteData()
        Dim nRow As DataRow
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                    If GridView1.GetSelectedRows()(n) >= 0 Then
                        nRow = GridView1.GetDataRow(n)
                        objData.Delete(GetDSN, "setupplafond", "user", data.myOperator.Assign, nRow(0).ToString)
                        GetData()
                        InitValue()
                    End If
                Next
                MessageBox.Show("Data sudah dihapus.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        DeleteData()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Dim dtData As New DataTable
        Try
            If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim vaField() As Object = {"User", "Maximum"}
                Dim vaValue() As Object = {cUsername.Text, CDbl(nMax.Text)}
                Dim cWhere As String = String.Format("user = '{0}'", cUsername.Text)
                objData.Update(GetDSN, "setupplafond", cWhere, vaField, vaValue)
                MessageBox.Show("Data Tersimpan.", "Notifikasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                GetData()
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        dtData.Dispose()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cUsername.EditValue = row(0).ToString
            nMax.Text = row(1).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub frmSetupPlafond_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("username", cUsername, , "username,fullname")
    End Sub

    Private Sub frmSetupPlafond_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetData()
        InitValue()
        InitForm(Me)
        SetButtonPicture(Me, , , cmdHapus, cmdSimpan, cmdKeluar)

        SetTabIndex(cUsername.TabIndex, n)
        SetTabIndex(nMax.TabIndex, n)

        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cUsername.EnterMoveNextControl = True
        nMax.EnterMoveNextControl = True
        FormatTextBox(nMax, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far, DevExpress.XtraEditors.Mask.MaskType.Numeric)
    End Sub
End Class