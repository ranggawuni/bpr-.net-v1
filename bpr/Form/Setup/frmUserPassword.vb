﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System.Data
Imports bpr.clsMD5

Public Class frmUserPassword
    ReadOnly objData As New data()
    Dim dtTable As New DataTable
    Private ReadOnly md5k As New clsMD5()

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "username,fullname,level"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "username", cField, "kode", , cKodeAplikasi, , "level,username")
            GridControl1.DataSource = cDataSet.Tables("username")
            cDataSet.Dispose()
            InitGrid(GridView1, False)
            GridColumnFormat(GridView1, "username")
            GridColumnFormat(GridView1, "fullname")
            GridColumnFormat(GridView1, "level", DevExpress.Utils.FormatType.None, , 10, DevExpress.Utils.HorzAlignment.Center)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Trim(cUserID.Text) = "" Then
            MessageBox.Show("User ID Tidak Boleh Kosong", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            cUserID.Focus()
            Exit Function
        End If
        If Not CheckData(cPassword, "Password Tidak Boleh Kosong.") Then
            Return False
            cPassword.Focus()
            Exit Function
        End If
        If Not CheckData(cFullName, "Full Name Tidak Boleh Kosong.") Then
            Return False
            cFullName.Focus()
            Exit Function
        End If
        If Not CheckData(nUserLevel, "User Level Tidak Boleh Kosong.") Then
            Return False
            nUserLevel.Focus()
            Exit Function
        End If
    End Function

    Private Sub InitValue()
        cUserID.Text = ""
        cPassword.Text = ""
        cFullName.Text = ""
        nUserLevel.Text = ""
        dtTable.Clear()
        GridControl1.DataSource = dtTable
    End Sub

    Private Sub DeleteData()
        Try
            If MessageBox.Show("Data akan dihapus?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim cWhere As String = String.Format("and kode = '{0}'", cKodeAplikasi)
                objData.Delete(GetDSN, "username", "username", data.myOperator.Assign, cUserID.Text, cWhere)
                GetData()
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub frmUserPassword_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetData()
        InitValue()
        InitForm(Me)
        SetButtonPicture(Me, cmdAdd, , cmdHapus, cmdSimpan, cmdKeluar)

        SetTabIndex(cUserID.TabIndex, n)
        SetTabIndex(cPassword.TabIndex, n)
        SetTabIndex(cFullName.TabIndex, n)
        SetTabIndex(nUserLevel.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cUserID.EnterMoveNextControl = True
        cPassword.EnterMoveNextControl = True
        cFullName.EnterMoveNextControl = True
        nUserLevel.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        InitValue()
        cUserID.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        If cUserID.Text <> "" Then
            DeleteData()
        End If
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Dim dtData As New DataTable
        Try
            If ValidSaving() Then
                If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim nID As Integer
                    dtData = objData.Browse(GetDSN, "username", "ifnull(max(ID),0) as ID")
                    If dtData.Rows.Count > 0 Then
                        nID = CInt(dtData.Rows(0).Item("ID")) + 1
                    Else
                        nID = 1
                    End If
                    Dim cID As String = Padl(nID.ToString, 3, "0")
                    Dim cPass As String = md5k.MD5(cPassword.Text)
                    Dim vaField() As Object = {"id", "Kode", "UserName",
                                               "Userpassword", "fullname", "level", "expired"}
                    Dim vaValue() As Object = {cID, cKodeAplikasi, cUserID.Text,
                                               cPass, cFullName.Text, Val(nUserLevel.Text), formatValue(DateAdd(DateInterval.Month, nBulanExpired, Date.Today), formatType.yyyy_MM_dd)}
                    Dim cWhere As String = String.Format("username = '{0}'", cUserID.Text)
                    objData.Update(GetDSN, "username", cWhere, vaField, vaValue)
                    GetData()
                    InitValue()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        dtData.Dispose()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cUserID.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Down Then
            Dim dtData As New DataTable
            Try
                If cUserID.Text <> "" Then
                    dtData = objData.Browse(GetDSN, "username", , "username", data.myOperator.Assign, cUserID.Text)
                    If dtData.Rows.Count > 0 Then
                        MessageBox.Show("UserID sudah ada,silahkan ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            dtData.Dispose()
        End If
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cUserID.Text = row(0).ToString
            cFullName.Text = row(1).ToString
            nUserLevel.Text = row(2).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class