﻿Imports bpr.MySQL_Data_Library

Public Class frmResetMenuLevel
    ReadOnly objData As New data()
    Dim dtTable As New DataTable

    Private Sub GetData()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "username,fullname,level,status"
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "username", cField, "kode", , cKodeAplikasi, , "level,username")
            GridControl1.DataSource = cDataSet.Tables("username")
            cDataSet.Dispose()
            InitGrid(GridView1, False)
            GridColumnFormat(GridView1, "username")
            GridColumnFormat(GridView1, "fullname")
            GridColumnFormat(GridView1, "level", DevExpress.Utils.FormatType.None, , 10, DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView1, "status")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub InitValue()
        cUserID.Text = ""
        cFullName.Text = ""
        nUserLevel.Text = ""
        chkAktif.Checked = False
        dtTable.Clear()
        GridControl1.DataSource = dtTable
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        Dim dtData As New DataTable
        Try
            If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim nStatus As Integer = CInt(IIf(chkAktif.Checked = True, 1, 0))
                Dim vaField() As Object = {"level", "Status"}
                Dim vaValue() As Object = {nUserLevel.Text, nStatus}
                Dim cWhere As String = String.Format("username = '{0}' and kode='BPR'", cUserID.Text)
                objData.Update(GetDSN, "username", cWhere, vaField, vaValue)
                GetData()
                InitValue()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        dtData.Dispose()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cUserID.Text = row(0).ToString
            cFullName.Text = row(1).ToString
            nUserLevel.Text = row(2).ToString
            If row(3).ToString = "0" Then
                chkAktif.Checked = False
            Else
                chkAktif.Checked = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub frmResetMenuLevel_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        GetData()
        InitValue()
        InitForm(Me)
        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)

        SetTabIndex(cUserID.TabIndex, n)
        SetTabIndex(cFullName.TabIndex, n)
        SetTabIndex(nUserLevel.TabIndex, n)
        SetTabIndex(chkAktif.TabIndex, n)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cUserID.EnterMoveNextControl = True
        cFullName.EnterMoveNextControl = True
        nUserLevel.EnterMoveNextControl = True
        chkAktif.EnterMoveNextControl = True
    End Sub
End Class