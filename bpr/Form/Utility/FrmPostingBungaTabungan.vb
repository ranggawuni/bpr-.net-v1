﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class FrmPostingBungaTabungan
    ReadOnly objData As New data()
    Dim dbData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim nBaris As DataRow()

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        dTgl1.DateTime = Date.Today
        dTglPosting.DateTime = Date.Today
    End Sub

    Private Sub PostingBunga()
        Dim nTotalBunga As Double
        Dim nTotalPajak As Double
        Dim n As Integer
        Dim cFaktur As String
        Dim cWhere As String
        Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
        Dim cKodeBunga As String
        Dim cKodePajak As String
        Dim cF As String = String.Format("BT{0}{1}{2}", cKodeKantorInduk, Format(EOM(dTglPosting.DateTime), "yyMMdd"), cUserID)
        Dim vaGolonganTabungan As New DataTable
        Dim db As New DataTable
        Dim m As Integer
        Dim cRekeningJurnalPajak As String
        Dim vaJoin() As Object
        Dim cField As String
        Dim vaArray As New DataTable
        Dim cFTemp As String = String.Format("BT{0}{1}", cKodeKantorInduk, Format(EOM(dTglPosting.DateTime), "yyMMdd"))
        Dim vaSukuBunga As New DataTable
        Dim nSaldoAkhir As Double
        Dim dbKas As New DataTable
        Dim cRekening As String = ""
        Dim cKodeTemp As String = ""
        Dim cGolonganTabungan As String = ""
        Dim cKeteranganPosting As String = ""

        cFaktur = cF & Padl("1", 4, "0")

        objData.Delete(GetDSN, "mutasitabungan", "faktur", data.myOperator.Prefix, cFTemp)
        objData.Delete(GetDSN, "bukubesar", "faktur", data.myOperator.Prefix, cFTemp)
        dbKas = objData.Browse(GetDSN, "cabang", "Kode", "Induk", , cKodeCabang)
        If dbKas.Rows.Count > 0 Then
            For n = 0 To dbKas.Rows.Count - 1
                cWhere = String.Format(" tglpenutupan >= '{0}' and Left(rekening,2)='{1}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd), GetNull(dbKas.Rows(n).Item("Kode")))
                '  cWhere = cWhere & " and rekening = '01.11.000486'"
                dbData = objData.Browse(GetDSN, "Tabungan", "Rekening,GolonganTabungan,Kode,Close", , , , cWhere, "Rekening")
                If dbData.Rows.Count > 0 Then
                    n = 0
                    db = objData.Browse(GetDSN, "golongantabungan", "kode,rekening,rekeningbunga", , , , , "kode")
                    If db.Rows.Count > 0 Then
                        vaGolonganTabungan = db.Copy
                    Else
                        vaGolonganTabungan.Reset()
                    End If
                    cKodeBunga = aCfg(eCfg.msKodeBunga).ToString
                    cKodePajak = aCfg(eCfg.msKodePajakBunga).ToString

                    db = objData.Browse(GetDSN, "detailSukubunga", "kode,max(sukubunga) as sukubunga", , , , , , , "kode having sukubunga>0")
                    If db.Rows.Count > 0 Then
                        vaSukuBunga = db.Copy
                    Else
                        vaSukuBunga.Reset()
                    End If

                    For m = 0 To dbData.Rows.Count - 1
                        '      If App.LogMode = 0 And dbData!Rekening = "01.11.000093" Then Stop
                        With dbData.Rows(m)
                            If .Item("Close").ToString = "0" Then
                                nBaris = dbData.Select(String.Format("kode = {0}", .Item("GolonganTabungan")))
                                If nBaris.Count > 0 Then
                                    cRekening = .Item("Rekening").ToString
                                    cKodeTemp = .Item("Kode").ToString
                                    cGolonganTabungan = .Item("GolonganTabungan").ToString
                                    vaArray = GetRptBungaHarian(cRekening, dTgl.DateTime, dTgl1.DateTime, nTotalBunga, nTotalPajak, ,
                                                                nSaldoAkhir, , , , , , , cKodeTemp, cGolonganTabungan, True)
                                    If nTotalBunga + nTotalPajak > 0 And cGolonganTabungan <> "17" Then
                                        n = n + 1
                                        lblKeterangan.Text = String.Format("Posting Bunga Tabungan {0} Rekening", n)
                                        ' Update Bunga Tabungan
                                        cKeteranganPosting = String.Format("Bunga Tgl {0} {1}", Format(dTglPosting.DateTime, "MM-yy"), cRekening)
                                        UpdMutasiTabungan(cKodeCabang, cKodeBunga, cFaktur, dTglPosting.DateTime, cRekening, nTotalBunga, ,
                                                          cKeteranganPosting, False, , , , , , , False)
                                        cField = "id,username,keterangan,cabangentry,keterangan,datetime"
                                        cWhere = String.Format("and tgl='{0}' and kodetransaksi='{1}'", formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), cKodeBunga)
                                        db = objData.Browse(GetDSN, "mutasitabungan", cField, "rekening", , cRekening, cWhere)
                                        If db.Rows.Count > 0 Then
                                            nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                                            If nBaris.Count > 0 Then
                                                UpdBukuBesar(CInt(db.Rows(0).Item("Id")), db.Rows(0).Item("CabangEntry").ToString, cFaktur, dTglPosting.DateTime,
                                                             nBaris(0).Item(1).ToString, db.Rows(0).Item("Keterangan").ToString, nTotalBunga, ,
                                                             Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"), db.Rows(0).Item("UserName").ToString)
                                                UpdBukuBesar(CInt(db.Rows(0).Item("Id")), db.Rows(0).Item("CabangEntry").ToString, cFaktur, dTglPosting.DateTime,
                                                             nBaris(1).ToString, db.Rows(0).Item("Keterangan").ToString, , nTotalBunga,
                                                             Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"), db.Rows(0).Item("UserName").ToString)
                                            End If
                                        End If

                                        ' Update Pajak Tabungan
                                        If nTotalPajak > 0 And .Item("GolonganTabungan").ToString <> "17" Then
                                            UpdMutasiTabungan(cKodeCabang, cKodePajak, cFaktur, dTglPosting.DateTime, .Item("Rekening").ToString,
                                                              nTotalPajak, , String.Format("Pajak Bunga Tgl {0} {1}", Format(dTglPosting.DateTime, "MM-yyyy"), .Item("Rekening")),
                                                              False, , , , , , , False)
                                            cField = "m.id,m.username,m.keterangan,m.cabangentry,m.keterangan,m.datetime,k.rekening as RekeningKodeTransaksi"
                                            cWhere = String.Format("and m.tgl='{0}' and m.kodetransaksi='{1}'", formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), cKodePajak)
                                            vaJoin = {"Left Join KodeTransaksi k on k.kode = m.kodetransaksi"}
                                            db = objData.Browse(GetDSN, "mutasitabungan m", cField, "m.rekening", , .Item("Rekening").ToString,
                                                                cWhere, , vaJoin)
                                            If db.Rows.Count > 0 Then
                                                cRekeningJurnalPajak = GetNull(db.Rows(0).Item("RekeningKodeTransaksi"), "").ToString
                                                nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                                                If nBaris.Count > 0 Then
                                                    UpdBukuBesar(CInt(db.Rows(0).Item("Id")), db.Rows(0).Item("CabangEntry").ToString, cFaktur,
                                                                 dTglPosting.DateTime, nBaris(0).Item(1).ToString, db.Rows(0).Item("Keterangan").ToString,
                                                                 nTotalPajak, , Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"),
                                                                 db.Rows(0).Item("UserName").ToString)
                                                    UpdBukuBesar(CInt(db.Rows(0).Item("Id")), db.Rows(0).Item("CabangEntry").ToString, cFaktur,
                                                                 dTglPosting.DateTime, cRekeningJurnalPajak, db.Rows(0).Item("Keterangan").ToString,
                                                                 , nTotalPajak, Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"), db.Rows(0).Item("UserName").ToString)
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End With
                    Next
                End If
            Next
        End If
        dbData.Dispose()
        db.Dispose()
        dbKas.Dispose()
        vaArray.Dispose()
        vaSukuBunga.Dispose()
    End Sub

    Private Sub PostingAdm()
        Dim lUpdateBukuBesar As Boolean
        Dim n As Integer
        Dim cFaktur As String
        Dim nSaldoAkhir As Double
        Dim cF As String = String.Format("TA{0}{1}{2}", cKodeKantorInduk, Format(EOM(dTglPosting.DateTime), "yyMMdd"), cUserID)
        Dim nAdminPasif As Double
        Dim lAdminPasif As Boolean
        Dim cWhere As String
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cField As String
        Dim nSaldoMinimum As Double
        Dim vaJoin() As Object
        Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
        Dim vaGolonganTabungan As New DataTable
        Dim db As New DataTable
        Dim cRekeningJurnal As String
        Dim cKodeTransaksi As String
        Dim cFakturTemp As String = ""
        Dim i As Integer
        Dim cFTemp As String = String.Format("TA{0}{1}", cKodeKantorInduk, Format(EOM(dTglPosting.DateTime), "yyMMdd"))
        Dim dbKas As New DataTable

        cFaktur = cF & Padl("1", 4, "0")

        Dim cSQL As String = String.Format("select rekening from mutasitabungan where keterangan like 'Tutup %' and tgl='{0}' and faktur like '{1}%'", formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), cFTemp)
        dbData = objData.SQL(GetDSN, cSQL)
        For n = 0 To dbData.Rows.Count - 1
            '    If App.LogMode = 0 And dbData!Rekening = "01.11.000523" Then Stop
            cWhere = String.Format("Rekening = '{0}'", dbData.Rows(0).Item("Rekening"))
            vaField = {"Close", "TglPenutupan"}
            vaValue = {"0", "9999-12-31"}
            objData.Edit(GetDSN, "Tabungan", cWhere, vaField, vaValue)
        Next

        objData.Delete(GetDSN, "mutasitabungan", "faktur", , cFTemp)
        objData.Delete(GetDSN, "bukubesar", "faktur", , cFTemp)
        dbKas = objData.Browse(GetDSN, "cabang", "Kode", "Induk", , cKodeCabang)
        If dbKas.rows.count > 0 Then
            For n = 0 To dbKas.Rows.Count - 1
                cField = ""
                cField = "t.Rekening,g.AdministrasiBulanan,g.AdminPasif,t.lastUpdate,r.nama,"
                cField = cField & "g.lamapasif,t.golongantabungan,g.saldominimum,g.administrasitutup"
                cWhere = String.Format("and t.tglpenutupan >= '{0}' ", formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0}and Left(t.rekening,2)='{1}'", cWhere, GetNull(dbKas.Rows(0).Item("Kode")))
                '  If App.LogMode = 0 Then cWhere = cWhere & " and t.rekening='01.11.008931'"
                vaJoin = {"Left Join GolonganTabungan g on t.GolonganTabungan = g.Kode",
                          "Left Join RegisterNasabah r on r.kode = t.kode"}
                dbData = objData.Browse(GetDSN, "Tabungan t", cField, "GolonganTabungan", , "11", cWhere, , vaJoin)
                If dbData.Rows.Count > 0 Then
                    db = objData.Browse(GetDSN, "golongantabungan", "kode,rekening,rekeningbunga", , , , , "kode")
                    If db.Rows.Count > 0 Then
                        vaGolonganTabungan = db.Copy
                    Else
                        vaGolonganTabungan.Reset()
                    End If
                    cKodeTransaksi = aCfg(eCfg.msKodeAdmPemeliharaan).ToString
                    n = 0
                    i = 1
                    lUpdateBukuBesar = False
                    For i = 0 To dbData.Rows.Count - 1
                        lAdminPasif = GetRekeningPassif(dbData.Rows(i).item("Rekening").ToString, dTglPosting.DateTime)
                        nSaldoAkhir = GetSaldoTabungan(dbData.Rows(i).Item("Rekening").ToString, dTglPosting.DateTime)
                        nSaldoMinimum = CDbl(GetNull(dbData.Rows(i).Item("AdministrasiTutup")))
                        nAdminPasif = CDbl(GetNull(dbData.Rows(i).Item("AdministrasiBulanan")))
                        ' Update Administrasi Tabungan
                        If nSaldoAkhir > nSaldoMinimum And dbData.Rows(i).Item("GolonganTabungan").ToString = "11" Then
                            If nAdminPasif > 0 Then
                                n = n + 1
                                lblKeterangan.Text = String.Format("Posting Administrasi Tabungan {0} Rekening", n)
                                UpdMutasiTabungan(cKodeCabang, cKodeTransaksi, cFaktur, dTglPosting.DateTime, dbData.Rows(i).Item("Rekening").ToString,
                                                  nAdminPasif, , String.Format("Biaya Adm. {0} - {1}", Format(dTglPosting.DateTime, "dd-MM-yyyy"), dbData.Rows(i).Item("Rekening")),
                                                  False, , , , , , , False)
                                cField = "m.id,m.username,m.keterangan,m.cabangentry,m.keterangan,m.datetime,k.rekening as RekeningKodeTransaksi"
                                cWhere = String.Format("and m.tgl='{0}' and m.kodetransaksi='{1}'", formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), cKodeTransaksi)
                                vaJoin = {"Left Join KodeTransaksi k on k.kode = m.kodetransaksi"}
                                db = objData.Browse(GetDSN, "mutasitabungan m", cField, "m.rekening", , dbData.Rows(i).Item("Rekening").ToString, cWhere, , vaJoin)
                                If db.Rows.Count > 0 Then
                                    cRekeningJurnal = GetNull(db.Rows(0).Item("RekeningKodeTransaksi"), "").ToString
                                    nBaris = dbData.Select(String.Format("kode = {0}", dbData.Rows(0).Item("GolonganTabungan")))
                                    If nBaris.Count > 0 Then
                                        UpdBukuBesar(CInt(db.Rows(0).Item("ID")), db.Rows(0).Item("CabangEntry").ToString, cFaktur, dTglPosting.DateTime,
                                                     nBaris(0).Item(1).ToString, db.Rows(0).Item("Keterangan").ToString, nAdminPasif, ,
                                                     Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"), db.Rows(0).Item("UserName").ToString)
                                        UpdBukuBesar(CInt(db.Rows(0).Item("Id")), db.Rows(0).Item("CabangEntry").ToString, cFaktur, dTglPosting.DateTime,
                                                     cRekeningJurnal, db.Rows(0).Item("Keterangan").ToString, , nAdminPasif,
                                                     Format(db.Rows(0).Item("DateTime"), "yyyy-mm-dd hh:mm:ss"), db.Rows(0).Item("UserName").ToString)
                                    End If
                                End If
                            End If
                        ElseIf nSaldoAkhir > 0 Then
                            If nSaldoAkhir < nSaldoMinimum Then
                                i = i + 1
                                ' Edit Tabungan
                                objData.Edit(GetDSN, "Tabungan", String.Format("Rekening = '{0}'", dbData.Rows(i).Item("Rekening")), _
                                             {"Close", "TglPenutupan"}, {"1", dTglPosting.DateTime})

                                ' Tutup tabungan ( saldo ditarik tunai )
                                cFakturTemp = cF & Padl(i.ToString, 4, "0")
                                UpdMutasiTabungan(cKodeCabang, aCfg(eCfg.msKodePenarikanTunai).ToString, cFakturTemp, dTglPosting.DateTime, _
                                                  dbData.Rows(i).Item("Rekening").ToString, nSaldoAkhir, , "Tutup Rekening Tabungan an. " & dbData.Rows(i).Item("Nama").ToString)
                            End If
                        End If
                    Next
                End If
            Next
        End If
        vaGolonganTabungan.Dispose()
    End Sub

    Private Sub FrmPostingBungaTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        SetButtonPicture(, , , , cmdProses, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dTgl1.TabIndex, n)
        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        dTgl1.EnterMoveNextControl = True
        dTglPosting.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        cmdKeluar.Focus()
        If MessageBox.Show("Proses Benar-benar Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            lblKeterangan.Visible = True
            lblKeterangan.Text = "Posting Administrasi Tabungan"
            PostingAdm()
            lblKeterangan.Text = "Posting Bunga Tabungan"
            PostingBunga()
            lblKeterangan.Visible = False
            MsgBox("Finish Successfully!! ", vbInformation)
        End If
    End Sub
End Class