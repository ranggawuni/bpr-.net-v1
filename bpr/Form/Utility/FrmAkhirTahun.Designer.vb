﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAkhirTahun
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If vaArray IsNot Nothing Then
                    vaArray.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAkhirTahun))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.pb = New DevExpress.XtraEditors.ProgressBarControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdProses = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cTahun = New DevExpress.XtraEditors.TextEdit()
        Me.lblJumlahRekening = New DevExpress.XtraEditors.LabelControl()
        Me.lblKeterangan = New DevExpress.XtraEditors.LabelControl()
        Me.dTglPosting = New DevExpress.XtraEditors.DateEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.pb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cTahun.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPosting.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPosting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.pb)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdProses)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 88)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(605, 33)
        Me.PanelControl4.TabIndex = 36
        '
        'pb
        '
        Me.pb.EditValue = "60"
        Me.pb.Location = New System.Drawing.Point(10, 5)
        Me.pb.Name = "pb"
        Me.pb.ShowProgressInTaskBar = True
        Me.pb.Size = New System.Drawing.Size(424, 23)
        Me.pb.TabIndex = 11
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(520, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdProses
        '
        Me.cmdProses.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdProses.Location = New System.Drawing.Point(439, 5)
        Me.cmdProses.Name = "cmdProses"
        Me.cmdProses.Size = New System.Drawing.Size(75, 23)
        Me.cmdProses.TabIndex = 8
        Me.cmdProses.Text = "&Proses"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cTahun)
        Me.PanelControl1.Controls.Add(Me.lblJumlahRekening)
        Me.PanelControl1.Controls.Add(Me.lblKeterangan)
        Me.PanelControl1.Controls.Add(Me.dTglPosting)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(605, 79)
        Me.PanelControl1.TabIndex = 35
        '
        'cTahun
        '
        Me.cTahun.Location = New System.Drawing.Point(114, 13)
        Me.cTahun.Name = "cTahun"
        Me.cTahun.Size = New System.Drawing.Size(71, 20)
        Me.cTahun.TabIndex = 121
        '
        'lblJumlahRekening
        '
        Me.lblJumlahRekening.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJumlahRekening.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblJumlahRekening.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblJumlahRekening.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lblJumlahRekening.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblJumlahRekening.Location = New System.Drawing.Point(211, 39)
        Me.lblJumlahRekening.Name = "lblJumlahRekening"
        Me.lblJumlahRekening.Size = New System.Drawing.Size(384, 28)
        Me.lblJumlahRekening.TabIndex = 120
        Me.lblJumlahRekening.Text = "Posting Bunga Tabungan"
        '
        'lblKeterangan
        '
        Me.lblKeterangan.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKeterangan.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblKeterangan.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblKeterangan.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lblKeterangan.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblKeterangan.Location = New System.Drawing.Point(211, 5)
        Me.lblKeterangan.Name = "lblKeterangan"
        Me.lblKeterangan.Size = New System.Drawing.Size(384, 28)
        Me.lblKeterangan.TabIndex = 119
        Me.lblKeterangan.Text = "Posting Bunga Tabungan"
        '
        'dTglPosting
        '
        Me.dTglPosting.EditValue = Nothing
        Me.dTglPosting.Location = New System.Drawing.Point(114, 39)
        Me.dTglPosting.Name = "dTglPosting"
        Me.dTglPosting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglPosting.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglPosting.Size = New System.Drawing.Size(82, 20)
        Me.dTglPosting.TabIndex = 118
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(16, 42)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(76, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Tanggal Posting"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(16, 16)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(30, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tahun"
        '
        'FrmAkhirTahun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(610, 124)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "FrmAkhirTahun"
        Me.Text = "Posting Akhir Tahun"
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.pb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cTahun.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPosting.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPosting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pb As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdProses As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblJumlahRekening As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblKeterangan As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglPosting As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTahun As DevExpress.XtraEditors.TextEdit
End Class
