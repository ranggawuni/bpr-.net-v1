﻿Imports bpr.MySQL_Data_Library

Public Class FrmAkhirTahun
    Dim dbData As New DataTable
    ReadOnly objData As New data()
    Private ReadOnly vaArray As New DataTable()
    Dim cFaktur As String

    Private Sub FrmAkhirTahun_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        SetButtonPicture(, , , , cmdProses, cmdKeluar)
        CenterFormManual(Me, , True)

        cTahun.Text = ""
        dTglPosting.DateTime = Date.Today
        lblJumlahRekening.Text = ""
        lblKeterangan.Text = ""

        SetTabIndex(cTahun.TabIndex, n)
        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cTahun.EnterMoveNextControl = True
        dTglPosting.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub simpanData()
        Dim nID As Double
        Dim nDebet As Double
        Dim nKredit As Double
        Dim nLaba As Double
        Dim dTanggal As Date
        Dim nSaldo As Double = 0
        Dim cRekening As String = ""

        If MessageBox.Show("Proses Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            If ValidProses() Then
                cFaktur = String.Format("TH{0}{1}", cKodeKantorInduk, Format(EOM(dTglPosting.DateTime), "yyMMdd"))

                ' Hapus Pada Buku Besar
                DelBukuBesar(cFaktur)

                nLaba = 0
                dTanggal = dTglPosting.DateTime 'DateAdd("d", 1, BOM(DateAdd("m", 1, dTglPosting.DateTime)))
                nID = GetID(dTanggal)
                lblKeterangan.Text = String.Format("Posting Jurnal Penutup Tahun {0}.", cTahun.Text)

                dbData = objData.Browse(GetDSN, "BukuBesar", "Rekening,Sum(Debet-Kredit) as Saldo", "Tgl", data.myOperator.LowerThanEqual, formatValue(EOM(dTglPosting.DateTime), formatType.yyyy_MM_dd), " and Rekening >= '4' and Rekening < '6' Group by Rekening")
                For n = 0 To dbData.Rows.Count - 1
                    nSaldo = CDbl(dbData.Rows(n).Item("Saldo"))
                    cRekening = dbData.Rows(n).Item("rekening").ToString
                    If nSaldo <> 0 Then
                        nDebet = 0
                        nKredit = 0
                        If nSaldo > 0 Then      ' Debet
                            nKredit = nSaldo
                        Else                          ' Kredit
                            nDebet = -nSaldo
                        End If
                        nLaba = nLaba + nSaldo

                        UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTanggal, cRekening, "Tutup Tahun " & cTahun.Text,
                                     nDebet, nKredit)
                    End If
                Next

                If nLaba <> 0 Then
                    nDebet = CDbl(IIf(nLaba > 0, nLaba, 0))
                    nKredit = CDbl(IIf(nLaba > 0, 0, -nLaba))

                    UpdBukuBesar(nID, aCfg(eCfg.msKodeCabang).ToString, cFaktur, dTanggal, aCfg(eCfg.msKodeLabaTahunLalu).ToString,
                                 "Tutup Tahun " & cTahun.Text, nDebet, nKredit)
                End If

                lblKeterangan.Text = String.Format("Rekap Buku Besar Tahun {0}.", cTahun.Text)
                PostingRekapBukuBesar(dTanggal, lblJumlahRekening)

                MsgBox("Proses Akhir Tahun Sudah Selesai", vbInformation)
            End If
        End If
        lblKeterangan.Text = ""
    End Sub

    Private Function ValidProses() As Boolean
        ValidProses = True
        If cTahun.Text & 12 <> Format(dTglPosting.DateTime, "yyyyMM") Then
            MsgBox("Proses Akhir Tahun Harus Dilakukan Pada Bulan Desember Dan Tahun Yang Sama ...!", vbExclamation)
            ValidProses = False
            Exit Function
        End If
    End Function


    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        simpanData()
    End Sub
End Class