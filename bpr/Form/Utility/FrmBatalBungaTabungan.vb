﻿Imports bpr.MySQL_Data_Library

Public Class FrmBatalBungaTabungan
    Dim objData As New data

    Private Sub BatalPostingBunga()
        objData.SQL(GetDSN, String.Format("Delete from bukubesar where keterangan like 'Bunga Tgl {0}%' or keterangan like 'Pajak Bunga Tgl {1}%'", Format(dTglPosting.DateTime, "MM-yy"), Format(dTglPosting.DateTime, "MM-yyyy")))
        objData.SQL(GetDSN, String.Format("Delete from MutasiTabungan where keterangan like 'Bunga Tgl {0}%' or keterangan like 'Pajak Bunga Tgl {1}%'", Format(dTglPosting.DateTime, "MM-yy"), Format(dTglPosting.DateTime, "MM-yyyy")))
    End Sub

    Private Sub BatalPostingAdm()
        Dim cF As String = "TA" & Format(dTglPosting.DateTime, "yyyyMMdd") '& "0000000001"
        objData.Delete(GetDSN, "mutasitabungan", "faktur", data.myOperator.Prefix, cF)
        objData.Delete(GetDSN, "bukubesar", "faktur", data.myOperator.Prefix, cF)
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        cmdKeluar.Focus()
        If MessageBox.Show("Proses Benar-benar Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            BatalPostingAdm()
            BatalPostingBunga()
            MsgBox("Finish Successfully !! ", vbInformation)
        End If
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub FrmBatalBungaTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        SetButtonPicture(, , , , cmdProses, cmdKeluar)
        CenterFormManual(Me, , True)

        dTglPosting.DateTime = Date.Today

        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTglPosting.EnterMoveNextControl = True
    End Sub
End Class