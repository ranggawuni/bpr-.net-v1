﻿Imports bpr.MySQL_Data_Library

Public Class frmAkhirBulan
    Dim dbData As New DataTable
    ReadOnly objData As New data()
    Dim cFakturAmortisasiProvisi As String
    Private ReadOnly vaArray As New DataTable()
    Dim nBaris As DataRow

    Private Enum enAccrualKredit
        enNomor = 0
        enRekening = 1
        enNama = 2
        enPlafond = 3
        enBakiDebet = 4
        enBungaAccrualHarian = 5
        enTunggakanBunga = 6
        enGolonganKredit = 7
    End Enum

    Private Sub frmAkhirBulan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        SetButtonPicture(, , , , cmdProses, cmdKeluar)
        CenterFormManual(Me, , True)

        dTglPosting.DateTime = Date.Today
        lblJumlahRekening.Text = ""
        lblKeterangan.Text = ""

        chkProses.CheckState = CheckState.Checked
        chkProses1.CheckState = CheckState.Checked
        chkProses2.CheckState = CheckState.Checked

        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(chkProses.TabIndex, n)
        SetTabIndex(chkProses1.TabIndex, n)
        SetTabIndex(chkProses2.TabIndex, n)
        SetTabIndex(cCabang.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTglPosting.EnterMoveNextControl = True
        chkProses.EnterMoveNextControl = True
        chkProses1.EnterMoveNextControl = True
        chkProses2.EnterMoveNextControl = True
        cCabang.EnterMoveNextControl = True

        cCabang.Text = aCfg(eCfg.msKodeCabang).ToString
        If cCabang.Text <> "01" Then
            cCabang.Enabled = False
        End If
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        If MessageBox.Show("Proses Dilanjutkan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cKey As String = cCabang.Text & Format(EOM(dTglPosting.DateTime), "yyMMdd") & cUserID
            cFakturAmortisasiProvisi = "PV" & cKey

            If chkProses.CheckState = CheckState.Checked Then
                PostingAccrualDeposito()
            End If
            If chkProses1.CheckState = CheckState.Checked Then
                PostingAccrualKredit()
            End If
            If chkProses2.CheckState = CheckState.Checked Then
                PostingAmortisasiProvisi()
            End If

            SetLabel(False)
        End If
        MessageBox.Show("Proses Akhir Bulan Selesai.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub SetLabel(ByVal lAwal As Boolean, Optional ByVal cText As String = "")
        If lAwal Then
            lblKeterangan.Text = cText
            lblKeterangan.Visible = lAwal
        Else
            lblKeterangan.Text = cText
            lblKeterangan.Visible = Not lAwal
        End If
    End Sub

    Private Sub PostingAccrualKredit()
        Dim cFaktur As String
        Dim nTotal As Double
        Dim vaKredit As New DataTable
        Dim n As Integer
        Dim cKeterangan As String
        Dim X As TypeKolektibilitas = Nothing
        Dim Y As TypeAccrualKredit
        Dim nTotalBakiDebet As Double
        Dim nTotalTunggakanBunga As Double
        Dim nID As Double
        Dim cTemp As String
        Dim dTglTemp As Date
        Dim nDebet As Double, nKredit As Double
        Dim nDebetBunga As Double
        Dim nKreditBunga As Double
        Dim dEOM As Date = EOM(dTglPosting.DateTime)

        vaKredit.Reset()
        AddColumn(vaKredit, "kode", System.Type.GetType("system.String"))
        AddColumn(vaKredit, "rekeningaccrual", System.Type.GetType("system.String"))
        AddColumn(vaKredit, "rekeningbunga", System.Type.GetType("system.String"))
        AddColumn(vaKredit, "accrual", System.Type.GetType("system.Double"))
        AddColumn(vaKredit, "tunggakanbunga", System.Type.GetType("system.Double"))
        AddColumn(vaKredit, "saldorekening", System.Type.GetType("system.Double"))
        vaArray.Reset()
        AddColumn(vaArray, "nomor", System.Type.GetType("system.Int32"))
        AddColumn(vaArray, "rekening", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nama", System.Type.GetType("system.String"))
        AddColumn(vaArray, "plafond", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "bakidebet", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "accrual", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "tunggakanbunga", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "golongankredit", System.Type.GetType("system.String"))
        If formatValue(dEOM, formatType.yyyy_MM_dd) < "2014-03-01" Then
            dTglTemp = EOM(DateAdd("m", -1, dEOM))
        Else
            dTglTemp = dEOM
        End If

        Dim cField As String = "d.GolonganKredit,g.Keterangan as NamaGolongan,d.Rekening,d.kode,r.nama,"
        cField = cField & "d.Tgl,d.Plafond,left(d.rekening,2) as cabang,"
        '  cField = cField & "d.plafond - ifnull((select SUM(KPokok-DPokok) from Angsuran a where a.rekening = d.rekening and (a.status=" & eAngsuran.ags_Angsuran & " or status = " & eAngsuran.ags_Koreksi & ") and a.tgl <= '" & FormatValue(dEOM, formatType.yyyy_MM_dd ) & "'),0) AS SaldoPokok"
        cField = String.Format("{0}if(d.caraperhitungan<>'7', d.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = d.rekening AND (a.status = {1} or status = {2}) AND a.tgl <= '{3}'),0),", cField, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dEOM, formatType.yyyy_MM_dd))
        cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = d.rekening and a.tgl <= '{1}'),0)) as SaldoPokok", cField, formatValue(dEOM, formatType.yyyy_MM_dd))
        Dim cWhere As String = "and statuspencairan<>0 "
        cWhere = String.Format("{0} and left(d.rekening,2) in {1}", cWhere, GetBranch(dEOM, cCabang.Text))
        Dim vaJoin() As Object = {"Left Join GolonganKredit g on g.kode = d.golongankredit",
                                  "Left Join RegisterNasabah r on r.kode = d.kode"}
        dbData = objData.Browse(GetDSN, "Debitur d", cField, "d.Tgl", data.myOperator.LowerThanEqual, formatValue(dEOM, formatType.yyyy_MM_dd),
                                cWhere & " having saldopokok > 0", "d.GolonganKredit,d.tgl,d.rekening ", vaJoin)
        For n = 0 To dbData.Rows.Count - 1
            With dbData.Rows(n)
                GetTunggakan(.Item("Rekening").ToString, dEOM, X, , , True, , .Item("Kode").ToString)
                '    X.nKolek = IIf(X.nBulanTunggakan = 0 And X.nTunggakanBunga = 0 And X.nTunggakanPokok = 0, 0, X.nKolek)
                If X.nKolek <= 1 Then
                    GetAccrualKredit(.Item("Rekening").ToString, dTglTemp, Y)
                    nBaris = vaArray.NewRow
                    nBaris("nomor") = (n + 1)
                    nBaris("rekening") = .Item("Rekening").ToString
                    nBaris("nama") = .Item("Nama").ToString
                    nBaris("plafond") = CDbl(X.nPlafond)
                    nBaris("bakidebet") = CDbl(X.nBakiDebet)
                    nBaris("accrual") = CDbl(Y.nBungaAccrualHarian)
                    nBaris("tunggakanbunga") = CDbl(X.nTunggakanBunga)
                    nBaris("golongankredit") = .Item("GolonganKredit").ToString
                    vaArray.Rows.Add(nBaris)
                    nTotalBakiDebet = nTotalBakiDebet + X.nBakiDebet
                    nTotalTunggakanBunga = nTotalTunggakanBunga + X.nTunggakanBunga
                End If
            End With
        Next

        dbData = objData.Browse(GetDSN, "golongankredit", "kode,rekeningAccrual,Rekeningbunga,0,0,0")
        If dbData.Rows.Count > 0 Then
            vaKredit = dbData.Copy
        End If

        If vaArray.Rows.Count > 0 Then
            Dim nBarisTemp As DataRow()
            For n = 0 To vaArray.Rows.Count - 1
                nBarisTemp = vaKredit.Select(String.Format("kode = '{0}'", vaArray.Rows(n).Item("golongankredit").ToString))
                If nBarisTemp.Count > 0 Then
                    nBarisTemp(0).Item("accrual") = CDbl(nBarisTemp(0).Item("accrual")) + CDbl(vaArray.Rows(n).Item("accrual"))
                    nBarisTemp(0).Item("tunggakanbunga") = CDbl(nBarisTemp(0).Item("tunggakanbunga")) + CDbl(vaArray.Rows(n).Item("TunggakanBunga"))
                End If
            Next

            cTemp = String.Format("AC{0}{1}", aCfg(eCfg.msKodeCabang), Format(dTglPosting.DateTime, "yyMM"))
            objData.Delete(GetDSN, "bukubesar", "faktur", data.myOperator.Prefix, cTemp)

            For n = 0 To vaKredit.Rows.Count - 1
                vaKredit.Rows(n).Item("saldorekening") = GetSaldoRekening(vaKredit.Rows(n).Item(1).ToString, dEOM, cCabang.Text)
            Next

            cKeterangan = String.Format("Accrual Kredit Periode {0} {1}", GetMonth(Month(dTglPosting.DateTime)), Year(dTglPosting.DateTime))
            For n = 0 To vaKredit.Rows.Count - 1
                With vaKredit.Rows(n)
                    nTotal = CDbl(.Item("accrual")) + CDbl(.Item("tunggakanbunga"))
                    ' membuat nominal akun yang sudah ada sebelumnya menjadi 0
                    nID = GetID(dTglPosting.DateTime)
                    cFaktur = GetLastFaktur(eFaktur.fkt_AccrualKredit, dTglPosting.DateTime, , True)
                    nKredit = 0 : nDebetBunga = 0 : nDebet = 0 : nKreditBunga = 0
                    If CDbl(.Item("saldorekening")) > 0 Then
                        nKredit = CDbl(.Item("saldorekening"))
                        nDebetBunga = CDbl(.Item("saldorekening"))
                    Else
                        nDebet = Math.Abs(CDbl(.Item("saldorekening")))
                        nKreditBunga = Math.Abs(CDbl(.Item("saldorekening")))
                    End If
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, .Item("rekeningaccrual").ToString, cKeterangan, nDebet, nKredit)
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, .Item("rekeningbunga").ToString, cKeterangan, nDebetBunga, nKreditBunga)

                    ' jurnal accrual bulan parameter
                    nID = GetID(dTglPosting.DateTime)
                    cFaktur = GetLastFaktur(eFaktur.fkt_AccrualKredit, dTglPosting.DateTime, , True)
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, .Item("rekeningaccrual").ToString, cKeterangan, nTotal)
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, .Item("rekeningbunga").ToString, cKeterangan, , nTotal)
                End With
            Next
        End If
    End Sub

    Private Sub PostingAccrualDeposito()
        Dim n As Integer
        Dim dTanggalcair As Date
        Dim cWhere As String
        Dim X As TypeDeposito = Nothing
        Dim vaArray As New DataTable
        Dim dbData As New DataTable
        Dim cFaktur As String
        Dim cTemp As String
        Dim nID As Double
        Dim vaGolongan As New DataTable
        Dim cKeterangan As String

        vaArray.Reset()
        AddColumn(vaArray, "jenis", System.Type.GetType("system.String"))
        AddColumn(vaArray, "rekening", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nomorbilyet", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nama", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nominalaktual", System.Type.GetType("system.String"))
        AddColumn(vaArray, "tglpembukaan", System.Type.GetType("system.String"))
        AddColumn(vaArray, "sukubunga", System.Type.GetType("system.String"))
        AddColumn(vaArray, "hariaccrual", System.Type.GetType("system.String"))
        AddColumn(vaArray, "bunga", System.Type.GetType("system.String"))
        AddColumn(vaArray, "pajak", System.Type.GetType("system.String"))
        AddColumn(vaArray, "accrual", System.Type.GetType("system.String"))
        AddColumn(vaArray, "cabang", System.Type.GetType("system.String"))
        AddColumn(vaArray, "haripembukaan", System.Type.GetType("system.String"))
        AddColumn(vaArray, "rekeningbunga", System.Type.GetType("system.String"))
        AddColumn(vaArray, "rekeningaccrual", System.Type.GetType("system.String"))
        AddColumn(vaArray, "golongandeposito", System.Type.GetType("system.String"))

        Dim dTgl As Date = EOM(dTglPosting.DateTime)
        Const cField As String = "d.Rekening,d.Nominal,g.RekeningBunga,g.RekeningAccrual,g.RekeningPajakBunga"
        cWhere = String.Format("Tgl <='{0}'", Format(dTgl, "yyyy-MM-dd"))
        cWhere = String.Format("{0} and left(d.rekening,2) in ( select kode from cabang where induk = '{1}')", cWhere, cCabang.Text)
        Dim vaJoin() As Object = {"left join golongandeposito g on g.kode = d.golongandeposito"}
        dbData = objData.Browse(GetDSN, "Deposito d", cField, , , , cWhere, " day(d.tgl),d.tgl,d.rekening", vaJoin)
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    GetDeposito(.Item("Rekening").ToString, dTgl, X)
                    dTanggalcair = X.dTglCair
                    If Not X.lTutup Then
                        nBaris = vaArray.NewRow
                        nBaris(0) = "1"
                        nBaris(1) = X.cRekening
                        nBaris(2) = X.cNomorBilyet
                        nBaris(3) = X.cNama
                        nBaris(4) = X.nNominalAktual
                        nBaris(5) = X.dTglPembukaan
                        nBaris(6) = X.nSukuBunga
                        nBaris(7) = X.nHariAccrual1
                        nBaris(8) = X.nBunga
                        nBaris(9) = X.nPajak
                        nBaris(10) = X.nBungaAccrual1
                        nBaris(11) = X.cCabang
                        nBaris(12) = X.dTglPembukaan.Day
                        nBaris(13) = GetNull(.Item("RekeningBunga"), "").ToString
                        nBaris(14) = GetNull(.Item("RekeningAccrual"), "").ToString
                        nBaris(15) = X.cGolonganDeposito
                        vaArray.Rows.Add(nBaris)
                    End If
                End With
            Next
            vaGolongan.Reset()
            dbData = objData.Browse(GetDSN, "golongandeposito", "kode,rekeningbunga,rekeningaccrual,0 as saldo")
            If dbData.Rows.Count > 0 Then
                vaGolongan = dbData.Copy
            End If

            Dim nRow As DataRow()
            For n = 0 To vaArray.Rows.Count - 1
                nRow = vaArray.Select(String.Format("kode = '{0}'", vaArray.Rows(n).Item("golongandeposito").ToString))
                If nRow.Count > 0 Then
                    nRow(0).Item("saldo") = CDbl(nRow(0).Item("saldo")) + CDbl(vaArray.Rows(n).Item("accrual"))
                End If
            Next

            cTemp = String.Format("AD{0}{1}", cCabang.Text, Format(dTglPosting.DateTime, "yyMMdd"))
            '    cTemp = "AD01130830001"
            '    cTemp = "AD01130831"
            objData.Delete(GetDSN, "bukubesar", "faktur", data.myOperator.Prefix, cTemp)
            For n = 0 To vaGolongan.Rows.Count - 1
                If CDbl(vaGolongan.Rows(n).Item(3)) > 0 Then
                    cFaktur = GetLastFaktur(eFaktur.fkt_AccrualDeposito, dTglPosting.DateTime, , True)
                    nID = GetID(dTgl)
                    cKeterangan = String.Format("Accrual Bunga Deposito Periode {0} {1}", GetMonth(Month(dTglPosting.DateTime)), Year(dTglPosting.DateTime))
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, vaGolongan.Rows(n).Item(1).ToString, cKeterangan, CDbl(vaGolongan.Rows(n).Item(3)), , "N")
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, vaGolongan.Rows(n).Item(2).ToString, cKeterangan, , CDbl(vaGolongan.Rows(n).Item(3)), "N")
                End If
            Next
        End If
        vaGolongan.Dispose()
    End Sub

    Private Sub PostingAmortisasiProvisi()
        Dim cKeterangan As String
        Dim n As Integer
        Dim cRekeningProvisi As String
        Dim cRekeningPendapatanProvisi As String
        Dim cFaktur As String
        Dim nID As Double
        Dim vaGolongan As New DataTable
        Dim cTemp As String = String.Format("PV{0}{1}", cCabang.Text, Format(dTglPosting.DateTime, "yyMMdd"))

        SetLabel(True, "Posting Amortisasi Provisi")
        vaGolongan.Reset()
        Const cField As String = "kode,rekeningprovisi,rekeningpendapatanprovisi,0 as saldo"
        dbData = objData.Browse(GetDSN, "GolonganKredit", cField)
        If dbData.Rows.Count > 0 Then
            vaGolongan = dbData.Copy
        End If
        '    cTemp = "AD01130830001"
        '    cTemp = "AD01130831"
        objData.Delete(GetDSN, "bukubesar", "faktur", data.myOperator.Prefix, cTemp)

        For n = 0 To vaGolongan.Rows.Count - 1
            With vaGolongan.Rows(n)
                .Item("saldo") = GetAmortisasiProvisiAdm(dTglPosting.DateTime, .Item("kode").ToString)
                If CDbl(.Item("saldo")) > 0 Then
                    cRekeningProvisi = .Item("rekeningprovisi").ToString
                    cRekeningPendapatanProvisi = .Item("rekeningpendapatanprovisi").ToString
                    cKeterangan = String.Format("Amortisasi Provisi Untuk Bulan {0} {1}", GetNamaBulan(dTglPosting.DateTime), Year(dTglPosting.DateTime))
                    cFaktur = GetLastFaktur(eFaktur.fkt_Amortisasi_Provisi, dTglPosting.DateTime, , True)
                    nID = GetID(dTglPosting.DateTime)

                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, cRekeningProvisi, cKeterangan, CDbl(.Item("saldo")))
                    UpdBukuBesar(nID, cCabang.Text, cFaktur, dTglPosting.DateTime, cRekeningPendapatanProvisi, cKeterangan, , CDbl(.Item("saldo")))
                End If
            End With
        Next
        SetLabel(False)
        lblJumlahRekening.Text = ""
        vaGolongan.Dispose()
    End Sub

    Private Function GetAmortisasiProvisiAdm(ByVal dTgl As Date, ByVal cGolonganKredit As String) As Double
        Dim n As Integer
        Dim cBulan As String
        Dim nPerBulan As Double
        Dim nBakiDebet As Double
        Dim dJthTmp As Date
        Dim cTable As String
        Dim dPelunasan As Date
        Dim nSisa As Double
        Dim db As New datatable
        Dim nKe As Integer
        Dim nTotal As Double
        Dim nProvisi As Double
        Dim nAdm As Double
        Dim nTemp As Double

        vaArray.Reset()
        AddColumn(vaArray, "golongankredit", System.Type.GetType("system.String"))
        AddColumn(vaArray, "keterangangolongan", System.Type.GetType("system.String"))
        AddColumn(vaArray, "rekening", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nama", System.Type.GetType("system.String"))
        AddColumn(vaArray, "tgl", System.Type.GetType("system.DateTime"))
        AddColumn(vaArray, "lama", System.Type.GetType("system.Int32"))
        AddColumn(vaArray, "plafond", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "administrasi", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "provisi", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "total", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "perbulan", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "bulanlalu", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "bulanini", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "akumulasi", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "sisa", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "bakidebet", System.Type.GetType("system.Double"))
        AddColumn(vaArray, "caraperhitungan", System.Type.GetType("system.String"))
        AddColumn(vaArray, "jthtmp", System.Type.GetType("system.DateTime"))
        AddColumn(vaArray, "tglpelunasan", System.Type.GetType("system.DateTime"))
        AddColumn(vaArray, "ke", System.Type.GetType("system.Int32"))
        AddColumn(vaArray, "tglperpanjangan", System.Type.GetType("system.DateTime"))
        AddColumn(vaArray, "cabang", System.Type.GetType("system.String"))
        AddColumn(vaArray, "rekeninglama", System.Type.GetType("system.String"))
        AddColumn(vaArray, "nospk", System.Type.GetType("system.String"))

        dTgl = EOM(dTgl)
        cBulan = Year(dTgl) & Padl(Month(dTgl).ToString, 2, "0")
        Dim cField As String = "d.golongankredit,g.keterangan as KeteranganGolongan,d.Rekening,r.Nama,d.tgl,d.lama,d.Plafond,d.Provisi,"
        cField = cField & "d.administrasi,d.CaraPerhitungan,left(d.rekening,2) as cabang,d.rekeninglama,d.nospk, "
        '  cField = cField & "d.plafond - ifnull((select SUM(KPokok-DPokok) from Angsuran a where a.rekening = d.rekening and (a.status=" & eAngsuran.ags_Angsuran & " or status = " & eAngsuran.ags_Koreksi & ") and a.tgl <= '" & FormatValue(dTgl, formatType.yyyy_MM_dd ) & "'),0) AS SaldoPokok"
        cField = String.Format("{0}if(d.caraperhitungan<>'7', d.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = d.rekening AND (a.status = {1} or status = {2}) AND a.tgl <= '{3}'),0),", cField, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl, formatType.yyyy_MM_dd))
        cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = d.rekening and a.tgl <= '{1}'),0)) as SaldoPokok", cField, formatValue(dTgl, formatType.yyyy_MM_dd))
        Dim cWhere As String = String.Format(" and d.golongankredit ='{0}' ", cGolonganKredit)
        cWhere = String.Format("{0}and statuspencairan<>0 and date_format(date_add(d.tgl,interval d.lama month),'%Y%m')>'{1}'", cWhere, cBulan)
        cWhere = String.Format("{0} and left(d.rekening,2) in {1}", cWhere, GetBranch(dTglPosting.DateTime, cCabang.Text))
        '  If App.LogMode = 0 Then cWhere = cWhere & " and d.rekening='11.73.110706'"
        Dim vaJoin() As Object = {"left join registernasabah r on d.kode = r.kode",
                                  "left join golongankredit g on d.golongankredit = g.kode"}
        dbData = objData.Browse(GetDSN, "debitur d", cField, "date_format(d.tgl,'%Y%m')", data.myOperator.LowerThanEqual, cBulan,
                                cWhere, , vaJoin)
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    nBakiDebet = CDbl(GetNull(.Item("saldopokok")))
                    If CDbl(.Item("Lama")) > 0 Then
                        nProvisi = Math.Round(CDbl(.Item("Provisi")) / CDbl(.Item("Lama")), 2)
                        nAdm = Math.Round(CDbl(.Item("Administrasi")) / CDbl(.Item("Lama")), 2)
                    End If
                    nTotal = CDbl(.Item("Provisi"))
                    nPerBulan = nProvisi + nAdm
                    dJthTmp = DateAdd("m", CDbl(.Item("Lama")), CDate(.Item("Tgl")))

                    nBaris = vaArray.NewRow
                    nBaris(0) = GetNull(.Item("GolonganKredit"))
                    nBaris(1) = GetNull(.Item("Keterangangolongan"))
                    nBaris(2) = GetNull(.Item("Rekening"))
                    nBaris(3) = GetNull(.Item("Nama"))
                    nBaris(4) = GetNull(.Item("Tgl"), Date.Today)
                    nBaris(5) = GetNull(.Item("Lama"))
                    nBaris(6) = GetNull(.Item("Plafond"))
                    nBaris(7) = GetNull(.Item("Administrasi")) ' adm
                    nBaris(8) = GetNull(.Item("Provisi")) ' provisi
                    nBaris(9) = nTotal ' adm+provisi
                    nBaris(10) = nPerBulan ' per bulan
                    nBaris(11) = 0 ' awal (sebelum bulan paramater)
                    nBaris(12) = 0 ' bulan ini
                    nBaris(13) = CDbl(nBaris(11)) + CDbl(nBaris(12)) ' awal + bulan ini
                    nBaris(14) = CDbl(nBaris(9)) - CDbl(nBaris(13)) ' adm + provisi - (awal + bulan ini)
                    nBaris(15) = nBakiDebet
                    nBaris(16) = .Item("CaraPerhitungan").ToString
                    nBaris(17) = dJthTmp ' jatuh tempo
                    nBaris(18) = 0 ' tanggal pelunasan
                    nBaris(19) = 0
                    nBaris(20) = 0 ' tanggal mulai di amortisasi untuk perpanjangan kredit
                    nBaris(21) = .Item("Cabang").ToString
                    nBaris(22) = .Item("Rekeninglama").ToString
                    nBaris(23) = .Item("NoSPK").ToString
                End With
            Next
        End If
        If vaArray.Rows.Count < 0 Then
            GetAmortisasiProvisiAdm = 0
            Exit Function
        End If

        Dim nBarisTemp As DataRow()

        ' isi data awal dan bulan ini tanpa ada perpanjangan kredit
        cField = "rekening,sum(provisi) as provisiawal,sum(adm) as admawal"
        cWhere = ""
        '  If App.LogMode = 0 Then cWhere = "and rekening='01.73.004297'"
        cWhere = String.Format(" and left(rekening,2) in ( select kode from cabang where induk = '{0}')", cCabang.Text)
        dbData = objData.Browse(GetDSN, "Provisi_adm", cField, "date_format(tgl,'%Y%m')", data.myOperator.LowerThan, cBulan, cWhere,
                                "rekening", , "rekening")
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                nBarisTemp = vaArray.Select(String.Format("rekening = '{0}'", dbData.Rows(n).Item("rekening")))
                If nBarisTemp.Count > 0 Then
                    nTotal = CDbl(GetNull(dbData.Rows(n).Item("provisiawal")))
                    nBarisTemp(0).Item("bulanlalu") = nTotal
                End If
            Next
        End If

        cField = "rekening,provisi,adm"
        cWhere = String.Format(" and left(rekening,2) in ( select kode from cabang where induk = '{0}')", cCabang.Text)
        dbData = objData.Browse(GetDSN, "Provisi_adm", cField, "date_format(tgl,'%Y%m')", , cBulan, cWhere, "rekening", , "rekening")
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                nBarisTemp = vaArray.Select(String.Format("rekening = '{0}'", dbData.Rows(n).Item("rekening")))
                If nBarisTemp.Count > 0 Then
                    nTotal = CDbl(GetNull(dbData.Rows(n).Item("provisi")))
                    nBarisTemp(0).Item("bulanini") = nTotal
                End If
            Next
        End If

        ' jika kredit lunas sebelum jatuh tempo maka sisa provisi yang belum diamortisasi langsung
        ' diakui sebagai pendapatan
        Dim m As Integer
        For n = 0 To vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                .Item(13) = CDbl(.Item(11)) + CDbl(.Item(12))  ' akumulasi = awal + bulan ini
                .Item(14) = CDbl(.Item(9)) - CDbl(.Item(13)) ' sisa = adm + provisi - (awal + bulan ini)
                If CDbl(.Item(20)) = 0 Then
                    m = CInt(DateDiff("m", .Item(4), dTgl))
                    If m = CDbl(.Item(5)) And CDbl(.Item(14)) = 0 Then
                        nKe = m
                    Else
                        nKe = m + 1
                    End If
                Else
                    m = CInt(DateDiff("m", .Item(20), dTgl))
                    nKe = m + 1
                End If
                .Item(19) = nKe
                If CDbl(.Item(15)) = 0 Then
                    If .Item(16).ToString <> "7" Then
                        cTable = "Angsuran"
                    Else
                        cTable = "MutasiTabungan"
                    End If
                    db = objData.Browse(GetDSN, cTable, "max(tgl) as tgl", "Rekening", , .Item(2).ToString, "and status = " & eAngsuran.ags_Angsuran)
                    If db.Rows.Count > 0 Then
                        dPelunasan = CDate(GetNull(db.Rows(0).Item("Tgl"), .Item(17)))
                    End If
                    .Item(18) = dPelunasan
                End If
                If CDbl(.Item(15)) = 0 Then
                    nSisa = CDbl(.Item(9)) - CDbl(.Item(11))
                    If CDbl(.Item(18)) < CDbl(.Item(17)) Then
                        .Item(12) = nSisa
                        .Item(13) = CDbl(.Item(12)) + CDbl(.Item(11))
                        .Item(14) = CDbl(.Item(9)) - CDbl(.Item(13))
                    End If
                End If
            End With
        Next

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                nKe = CInt(DateDiff("m", CDate(.Item(4)), dTgl))
                If CDbl(.Item(19)) > CDbl(.Item(5)) And CDbl(.Item(11)) = CDbl(.Item(9)) And CDbl(.Item(13)) = CDbl(.Item(9)) Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                nKe = CInt(DateDiff("m", CDate(.Item(4)), dTgl))
                If CDbl(.Item(11)) = 0 And CDbl(.Item(12)) = 0 And CDbl(.Item(13)) = 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                If CDbl(.Item(14)) = 0 And CDbl(.Item(19)) < CDbl(.Item(5)) Then
                    If CDbl(.Item(18)) > 0 Then
                        If Format(.Item(18), "yyyymm") < Format(dTgl, "yyyymm") Then
                            vaArray.Rows.Remove(dbData.Rows(n))
                            n = n - 1
                        End If
                    End If
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                nKe = CInt(DateDiff("m", CDate(.Item(4)), dTgl))
                If CDbl(.Item(19)) = CDbl(.Item(5)) And CDbl(.Item(12)) = 0 And CDbl(.Item(14)) = 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                Dim nSelisihBulan As Integer = CInt(DateDiff("m", CDate(.Item(4)), dTgl))
                If nSelisihBulan >= 1 And CDbl(.Item(13)) = CDbl(.Item(9)) And CDbl(.Item(12)) = 0 And CDbl(.Item(14)) = 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                If CDbl(.Item(12)) < 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        'vaArray.QuickSort(0, vaArray.Rows.Count - 1, 0, XORDER_ASCEND, XTYPE_DEFAULT, 2, XORDER_ASCEND, XTYPE_DEFAULT)

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                If CDbl(.Item(19)) > CDbl(.Item(5)) And CDbl(.Item(14)) < 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With vaArray.Rows(n)
                Dim dTglLunasTemp As Date = EOM(DateAdd("m", -1, dTgl))
                If CDate(.Item(18)) <= dTglLunasTemp And CDbl(.Item(15)) = 0 And CDbl(.Item(14)) = 0 Then
                    vaArray.Rows.Remove(dbData.Rows(n))
                    n = n - 1
                End If
                n = n + 1
            End With
        Loop

        n = 0
        Do While Not n > vaArray.Rows.Count - 1
            With dbData.Rows(n)
                Dim dTglLunasTemp1 As Date
                dbData = objData.Browse(GetDSN, "debitur", "tglpelunasan", "rekening", , .Item(2).ToString)
                If dbData.Rows.Count > 0 Then
                    dTglLunasTemp1 = EOM(DateAdd("m", -1, dTgl))
                    If formatValue(dbData.Rows(0).Item("TglPelunasan"), formatType.yyyy_MM_dd) <= formatValue(dTglLunasTemp1, formatType.yyyy_MM_dd) And CDbl(.Item(15)) = 0 Then
                        vaArray.Rows.Remove(dbData.Rows(n))
                        n = n - 1
                    End If
                End If
                n = n + 1
            End With
        Loop

        For n = 0 To vaArray.Rows.Count - 1
            nTemp = nTemp + CDbl(vaArray.Rows(n).Item(12))
        Next
        GetAmortisasiProvisiAdm = nTemp
    End Function

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub
End Class