﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class frmPostingBungaAntarKantor
    ReadOnly objData As New data()
    Dim dbData As New DataTable
    Dim lFormLoad As Boolean = False
    Private ReadOnly nBaris As DataRow()
    Dim vaArray As New DataTable

    Private Sub InitTable()
        vaArray.Reset()
        AddColumn(vaArray, "dTgl", System.Type.GetType("System.DateTime"), True)
        AddColumn(vaArray, "nAktiva", System.Type.GetType("System.Double"), True)
        AddColumn(vaArray, "nPasiva", System.Type.GetType("System.Double"), True)
        AddColumn(vaArray, "nSaldo", System.Type.GetType("System.Double"), True)
        AddColumn(vaArray, "cPersen", System.Type.GetType("System.String"), True)
        AddColumn(vaArray, "nBunga", System.Type.GetType("System.Double"), True)
        GridControl1.DataSource = vaArray
    End Sub

    Private Sub InitValue()
        dAwal.DateTime = Date.Today
        dAkhir.DateTime = Date.Today
        dTglPosting.DateTime = Date.Today
        cRekeningDebet.Text = ""
        cRekeningKredit.Text = ""
        nTotal.Text = "0"
    End Sub

    Private Sub GetData()
        Dim d As Integer
        Dim n As Integer = CInt(DateDiff(DateInterval.Day, dAwal.DateTime, dAkhir.DateTime))
        Dim dDateTemp As Date
        Dim cWhere2 As String
        Dim nTotSaldo As Double = 0
        Dim nBaris As DataRow
        Dim nBunga As Double
        For d = 0 To n
            dDateTemp = DateAdd(DateInterval.Day, d, dAwal.DateTime)
            nBaris = vaArray.NewRow
            nBaris("dTgl") = dDateTemp.ToString
            cWhere2 = " and keterangan not like 'Posting Total%Antar Kantor%'"
            nBaris("nAktiva") = GetSaldoRekening("1.191.10", dDateTemp, cCabang.Text, , cWhere2)
            nBaris("nPasiva") = GetSaldoRekening("2.261.10", dDateTemp, cCabang.Text, , cWhere2)
            nBaris("nSaldo") = CDbl(nBaris("nAktiva")) - CDbl(nBaris("nPasiva"))
            nBunga = Math.Round((12.5 / 100) * Devide(CDbl(nBaris("nSaldo")), 360), 2)
            nTotSaldo = nTotSaldo + nBunga
            nBaris("cPersen") = "12.5 %"
            nBaris("nBunga") = nBunga
            vaArray.Rows.Add(nBaris)
        Next
        If nTotSaldo > 0 Then
            lblTotal.Text = "Total Pendapatan Antar Kantor"
        Else
            lblTotal.Text = "Total Biaya Antar Kantor"
        End If
        nTotal.Text = Math.Abs(nTotSaldo).ToString
        GridControl1.DataSource = vaArray
    End Sub

    Private Sub frmPostingBungaAntarKantor_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("cabang", cCabang, "jeniskantor<>'K'")
    End Sub

    Private Sub frmPostingBungaAntarKantor_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdProses, cmdKeluar, , , , , , , , , cmdRefresh)
        CenterFormManual(Me, , True)

        SetTabIndex(dAwal.TabIndex, n)
        SetTabIndex(dAkhir.TabIndex, n)
        SetTabIndex(cCabang.TabIndex, n)
        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(cRekeningDebet.TabIndex, n)
        SetTabIndex(cRekeningKredit.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dAwal.EnterMoveNextControl = True
        dAkhir.EnterMoveNextControl = True
        cCabang.EnterMoveNextControl = True
        dTglPosting.EnterMoveNextControl = True
        cRekeningDebet.EnterMoveNextControl = True
        cRekeningKredit.EnterMoveNextControl = True

        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        SimpanData()
    End Sub

    Private Sub SimpanData()
        Dim cFaktur As String
        Dim cKeterangan As String
        Dim cKodeCabang As String

        If ValidSaving() Then
            If MsgBox("Anda yakin ingin melanjutkan Posting Bunga Antar Kantor ?", vbYesNo) = vbNo Then
                Exit Sub
            End If
            'Dr   Antar Bank Aktiva
            ' Cr    Pendapatan Non Operasional
            'Rekening Debet
            cKeterangan = String.Format("Posting {0} - {1}", lblTotal.Text, GetLocalDate(dTglPosting.DateTime, False))
            objData.Delete(GetDSN, "bukubesar", "tgl", , formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), String.Format(" and keterangan = '{0}'", cKeterangan))
            objData.Delete(GetDSN, "totjurnal", "tgl", , formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), String.Format(" and keterangan = '{0}'", cKeterangan))
            objData.Delete(GetDSN, "jurnal", "tgl", , formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd), String.Format(" and keterangan = '{0}'", cKeterangan))

            cKodeCabang = cCabang.Text ' aCfg(msKodeCabang)
            cFaktur = GetLastFaktur(eFaktur.fkt_Jurnal, dTglPosting.DateTime, , True)
            '    cKeterangan = "Posting " & nTotal.Caption & " - " & GetLocalDate(dTgl.Value, False)
            UpdTotJurnalLainLain(cFaktur, dTglPosting.DateTime, cUserName, SNow, CDbl(nTotal.Text), _
                                 CDbl(nTotal.Text), cCabang.Text, cKeterangan)

            UpdJurnalLainLain(cFaktur, dTglPosting.DateTime, cRekeningDebet.Text, cKeterangan, CDbl(nTotal.Text), 0, cCabang.Text)
            UpdJurnalLainLain(cFaktur, dTglPosting.DateTime, cRekeningKredit.Text, cKeterangan, 0, CDbl(nTotal.Text), cCabang.Text)

            UpdRekJurnal(cFaktur)
            MsgBox("Finish", vbInformation)
            InitValue()
            InitTable()
        End If
    End Sub

    Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(cRekeningDebet.Text, "Rekening masih kosong, Ulangi pengisisan!") Then
            ValidSaving = False
            cRekeningDebet.Focus()
            Exit Function
        End If
        If Not CheckData(cRekeningKredit.Text, "Rekening masih kosong, Ulangi pengisisan!") Then
            ValidSaving = False
            cRekeningKredit.Focus()
            Exit Function
        End If
    End Function

    Private Sub cCabang_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cCabang.Closed
        GetKeteranganLookUp(cCabang, cNamaCabang)
    End Sub

    Private Sub cCabang_KeyDown(sender As Object, e As KeyEventArgs) Handles cCabang.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cCabang, cNamaCabang)
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetData()
    End Sub
End Class