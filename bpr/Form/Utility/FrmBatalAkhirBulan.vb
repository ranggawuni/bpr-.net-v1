﻿Imports bpr.MySQL_Data_Library

Public Class FrmBatalAkhirBulan
    Dim objData As New data
    Dim dbData As New DataTable
    Dim cFakturAmortisasiProvisi As String
    Dim cFakturAccrualDeposito As String
    Dim cFakturAktivaTetap As String
    Dim cKey As String

    Private Sub cmdProses_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        Dim cWhere As String

        If MessageBox.Show("Proses Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            cKey = cKodeKantorInduk & Format(EOM(dTglPosting.DateTime), "yyMMdd") & cUserID
            cFakturAmortisasiProvisi = "PV" & cKey
            cFakturAccrualDeposito = "AD" & cKey
            cFakturAktivaTetap = "AT" & cKey

            ' hapus data deposito
            DelBukuBesar(cFakturAccrualDeposito)

            ' hapus data kredit
            DelBukuBesar(cFakturAmortisasiProvisi)

            ' hapus penyusutan aktiva
            DelBukuBesar(cFakturAktivaTetap)
            '      objData.Edit GetDSN, "akhirbulan", "periode = '" & GetPeriode & "'", Array("Tgl"), Array("1900-01-01")

            If Month(dTglPosting.DateTime) = 12 Then ' jika akhir tahun
                cWhere = String.Format(" and tgl= '{0}'", formatValue(dTglPosting.DateTime, formatType.yyyy_MM_dd))
                objData.Delete(GetDSN, "bukubesar", "left(faktur,2)", , "TH", cWhere)
            End If
            MessageBox.Show("Proses Sudah Selesai", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub FrmBatalAkhirBulan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        SetButtonPicture(, , , , cmdProses, cmdKeluar)
        CenterFormManual(Me, , True)

        dTglPosting.DateTime = Date.Today

        SetTabIndex(dTglPosting.TabIndex, n)
        SetTabIndex(cmdProses.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTglPosting.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub
End Class