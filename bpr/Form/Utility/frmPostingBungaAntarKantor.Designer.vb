﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostingBungaAntarKantor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
                If vaArray IsNot Nothing Then
                    vaArray.Dispose()
                    vaArray = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPostingBungaAntarKantor))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.pb = New DevExpress.XtraEditors.ProgressBarControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdProses = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.lblTotal = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaCabang = New DevExpress.XtraEditors.TextEdit()
        Me.cCabang = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningDebet = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglPosting = New DevExpress.XtraEditors.DateEdit()
        Me.dAkhir = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dAwal = New DevExpress.XtraEditors.DateEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.pb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaCabang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPosting.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPosting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAkhir.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAkhir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAwal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAwal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdRefresh)
        Me.PanelControl4.Controls.Add(Me.pb)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdProses)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 344)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(882, 33)
        Me.PanelControl4.TabIndex = 34
        '
        'cmdRefresh
        '
        Me.cmdRefresh.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdRefresh.Location = New System.Drawing.Point(640, 5)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 12
        Me.cmdRefresh.Text = "&Refresh"
        '
        'pb
        '
        Me.pb.EditValue = "60"
        Me.pb.Location = New System.Drawing.Point(10, 5)
        Me.pb.Name = "pb"
        Me.pb.ShowProgressInTaskBar = True
        Me.pb.Size = New System.Drawing.Size(624, 23)
        Me.pb.TabIndex = 11
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(802, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdProses
        '
        Me.cmdProses.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdProses.Location = New System.Drawing.Point(721, 5)
        Me.cmdProses.Name = "cmdProses"
        Me.cmdProses.Size = New System.Drawing.Size(75, 23)
        Me.cmdProses.TabIndex = 8
        Me.cmdProses.Text = "&Proses"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.nTotal)
        Me.PanelControl1.Controls.Add(Me.lblTotal)
        Me.PanelControl1.Controls.Add(Me.cNamaCabang)
        Me.PanelControl1.Controls.Add(Me.cCabang)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekeningKredit)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cRekeningDebet)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.dTglPosting)
        Me.PanelControl1.Controls.Add(Me.dAkhir)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dAwal)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(882, 88)
        Me.PanelControl1.TabIndex = 33
        '
        'nTotal
        '
        Me.nTotal.Enabled = False
        Me.nTotal.Location = New System.Drawing.Point(754, 11)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(123, 20)
        Me.nTotal.TabIndex = 127
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(589, 14)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(159, 13)
        Me.lblTotal.TabIndex = 126
        Me.lblTotal.Text = "Biaya / Pendapatan Antar Kantor"
        '
        'cNamaCabang
        '
        Me.cNamaCabang.Enabled = False
        Me.cNamaCabang.Location = New System.Drawing.Point(163, 37)
        Me.cNamaCabang.Name = "cNamaCabang"
        Me.cNamaCabang.Size = New System.Drawing.Size(143, 20)
        Me.cNamaCabang.TabIndex = 125
        '
        'cCabang
        '
        Me.cCabang.Location = New System.Drawing.Point(115, 37)
        Me.cCabang.Name = "cCabang"
        Me.cCabang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cCabang.Size = New System.Drawing.Size(42, 20)
        Me.cCabang.TabIndex = 124
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 40)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl2.TabIndex = 123
        Me.LabelControl2.Text = "Rekening Debet"
        '
        'cRekeningKredit
        '
        Me.cRekeningKredit.Location = New System.Drawing.Point(433, 63)
        Me.cRekeningKredit.Name = "cRekeningKredit"
        Me.cRekeningKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningKredit.Size = New System.Drawing.Size(135, 20)
        Me.cRekeningKredit.TabIndex = 122
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(327, 66)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl1.TabIndex = 121
        Me.LabelControl1.Text = "Rekening Kredit"
        '
        'cRekeningDebet
        '
        Me.cRekeningDebet.Location = New System.Drawing.Point(433, 37)
        Me.cRekeningDebet.Name = "cRekeningDebet"
        Me.cRekeningDebet.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningDebet.Size = New System.Drawing.Size(135, 20)
        Me.cRekeningDebet.TabIndex = 120
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(327, 40)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 119
        Me.LabelControl7.Text = "Rekening Debet"
        '
        'dTglPosting
        '
        Me.dTglPosting.EditValue = Nothing
        Me.dTglPosting.Location = New System.Drawing.Point(433, 11)
        Me.dTglPosting.Name = "dTglPosting"
        Me.dTglPosting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglPosting.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglPosting.Size = New System.Drawing.Size(82, 20)
        Me.dTglPosting.TabIndex = 118
        '
        'dAkhir
        '
        Me.dAkhir.EditValue = Nothing
        Me.dAkhir.Location = New System.Drawing.Point(224, 11)
        Me.dAkhir.Name = "dAkhir"
        Me.dAkhir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dAkhir.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dAkhir.Size = New System.Drawing.Size(82, 20)
        Me.dAkhir.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(203, 14)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "s/d"
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(327, 15)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(76, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Tanggal Posting"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 14)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(77, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Periode Tanggal"
        '
        'dAwal
        '
        Me.dAwal.EditValue = Nothing
        Me.dAwal.Location = New System.Drawing.Point(115, 11)
        Me.dAwal.Name = "dAwal"
        Me.dAwal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dAwal.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dAwal.Size = New System.Drawing.Size(82, 20)
        Me.dAwal.TabIndex = 82
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(3, 97)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(882, 241)
        Me.GridControl1.TabIndex = 35
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'frmPostingBungaAntarKantor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(887, 379)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "frmPostingBungaAntarKantor"
        Me.Text = "Posting Bunga Antar Kantor"
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.pb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaCabang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPosting.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPosting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAkhir.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAkhir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAwal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAwal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pb As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdProses As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dTglPosting As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dAkhir As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dAwal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cCabang As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningDebet As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmdRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaCabang As DevExpress.XtraEditors.TextEdit
End Class
