﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trAngsuranInstansiTitipan
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim dtTable As New DataTable
    Dim dtExcel As New DataTable
    Private ReadOnly x As TypeKolektibilitas

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        optKas.SelectedIndex = 1
        cInstansi.EditValue = ""
        cNamaInstansi.Text = ""
        cBendahara.EditValue = ""
        cNamaBendahara.Text = ""
        cFile.EditValue = ""
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nama", System.Type.GetType("System.String"))
        AddColumn(dtTable, "TunggakanPokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TunggakanBunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TotalTunggakan", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TotalAngsuran", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "SaldoPokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(dtTable, "Status", System.Type.GetType("System.String"))
        AddColumn(dtTable, "RekeningTabungan", System.Type.GetType("System.String"))
        AddColumn(dtTable, "SaldoTabungan", System.Type.GetType("System.Double"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim row As DataRow = dtTable.NewRow()
            Dim nKe As Double = 0
            Dim nPokok As Double = 0
            Dim nBunga As Double = 0
            Dim cWhere As String = ""
            Dim vaJoin() As Object = Nothing
            Dim cField As String = String.Format("t.rekening,if(t.caraperhitungan<>'7', t.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = t.rekening AND (a.status = {0} or status = {1}) AND a.tgl <= '{2}'),0),", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = t.rekening and a.tgl <= '{1}'),0)) as SaldoPokok,", cField, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = cField & "r.Nama,t.kode,t.rekeningtabungan"
            If cBendahara.Text = "" Then
                cWhere = String.Format("and r.instansi ='{0}'", cInstansi.Text)
                vaJoin = {"left join registernasabah r on t.kode=r.kode",
                          "Left Join Bendahara b on b.kode = t.bendahara"}
            Else
                cWhere = String.Format("and r.instansi ='{0}' and t.bendahara = '{1}'", cInstansi.Text, cBendahara.Text)
                vaJoin = {"left join registernasabah r on t.kode=r.kode",
                          "Left Join Bendahara b on b.kode = t.bendahara"}
            End If
            cWhere = String.Format("{0} and t.Tgl <= '{1}' and t.statuspencairan <> 0 ", cWhere, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = cWhere & " and (t.bendahara <> '' and t.bendahara <> '999') "
            dtData = objData.Browse(GetDSN, "debitur t", cField, "t.instansi", , "Y", cWhere & " having saldopokok>0", "t.Tgl,t.Rekening", vaJoin)
            If Not dtData.Rows.Count > 0 Then
                dTglTemp = DateAdd(DateInterval.Month, 0, dTgl.DateTime)
                For n As Integer = 0 To dtData.Rows.Count - 1
                    nPokok = 0
                    nBunga = 0
                    With dtData.Rows(n)
                        row("Rekening") = .Item("Rekening").ToString
                        row("Nama") = .Item("Nama").ToString
                        GetTunggakan(.Item("Rekening").ToString, dTgl.DateTime, x, , , True, , .Item("Kode").ToString)
                        row("TunggakanPokok") = x.nTunggakanPokok
                        row("TunggakanBunga") = x.nTunggakanBunga
                        row("TotalTunggakan") = x.nTunggakanPokok + x.nTunggakanBunga
                        GetKewajibanPokokBunga(.Item("Rekening").ToString, dTgl.DateTime, nBunga, nPokok, True, nKe)
                        row("Pokok") = nPokok
                        row("Bunga") = nBunga
                        row("TotalAngsuran") = nPokok + nBunga
                        row("SaldoPokok") = CDbl(.Item("SaldoPokok"))
                        row("Ke") = nKe
                        row("Status") = ""
                        If optKas.SelectedIndex = 3 Then
                            row("RekeningTabungan") = x.cRekeningTabungan
                            row("SaldoTabungan") = GetSaldoTabungan(x.cRekeningTabungan, dTgl.DateTime)
                        End If
                        dtTable.Rows.Add(row)
                    End With
                Next
            End If
            GridControl1.DataSource = dtTable
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "TunggakanPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TunggakanBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TotalTunggakan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TotalAngsuran", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "SaldoPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Ke", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status")
            GridColumnFormat(GridView1, "RekeningTabungan")
            GridColumnFormat(GridView1, "SaldoTabungan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim lTabungan As Boolean = False
        Dim cJenisKas As String = GetOpt(optKas)
        Dim lSaldoTabungan As Boolean = False
        Dim cFaktur As String = ""
        Dim cKeterangan As String = ""
        Dim cKodeCabang As String = ""

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    DelMutasiTabungan(nRow(0).ToString, nRow(2).ToString, True)
                    lTabungan = False
                    If optKas.SelectedIndex = 2 Then
                        If nRow(11).ToString.Trim.Length = 12 Then
                            lTabungan = True
                        End If
                    Else
                        lTabungan = True
                    End If
                    If lTabungan Then
                        ' cek saldo tabungan
                        If CDbl(nRow(12)) >= CDbl(nRow(5)) + CDbl(nRow(6)) Then
                            lSaldoTabungan = True
                        Else
                            lSaldoTabungan = False
                        End If
                        If lSaldoTabungan Then
                            cFaktur = GetLastFaktur(eFaktur.fkt_Angsuran_Instansi, dTgl.DateTime, , True)
                            cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                            cKeterangan = "Angsuran Kredit Instansi an. " & nRow(1).ToString

                            UpdAngsuranInstansi(cKodeCabang, cFaktur, dTgl.DateTime, nRow(0).ToString, CDbl(nRow(9)), CDbl(nRow(6)),
                                                CDbl(nRow(7)), cKeterangan, cInstansi.Text, , , , cJenisKas)
                            GridView1.SetRowCellValue(n, "Status", "Berhasil")
                        Else
                            If lTabungan Then
                                cFaktur = GetLastFaktur(eFaktur.fkt_Angsuran_Instansi, dTgl.DateTime, , True)
                                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                                cKeterangan = "Angsuran Kredit Instansi an. " & nRow(1).ToString

                                UpdAngsuranInstansi(cKodeCabang, cFaktur, dTgl.DateTime, nRow(0).ToString, CDbl(nRow(9)),
                                                    CDbl(nRow(5)), CDbl(nRow(6)), cKeterangan, cInstansi.Text, , , , cJenisKas)
                                GridView1.SetRowCellValue(n, "Status", "Berhasil")
                            Else
                                GridView1.SetRowCellValue(n, "Status", "Gagal - Saldo Tabungan Tidak Cukup")
                            End If
                        End If
                    Else
                        GridView1.SetRowCellValue(n, "Status", "Gagal - Rek. Tabungan Tidak Lengkap")
                    End If
                Else
                    GridView1.SetRowCellValue(n, "Status", "Gagal")
                End If
            Next
            MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            cInstansi.Focus()
        End If
    End Sub

    Private Sub trAngsuranInstansiTitipan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("Instansi", cInstansi)
        GetDataLookup("bendahara", cBendahara, , "kode,nama")
    End Sub

    Private Sub trAngsuranInstansiTitipan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar, , , , , , , , , cmdRefresh)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cInstansi.TabIndex, n)
        SetTabIndex(cBendahara.TabIndex, n)
        SetTabIndex(cFile.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cInstansi.EnterMoveNextControl = True
        cBendahara.EnterMoveNextControl = True
        cFile.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        If cFile.Text = "" Then
            GetSQL()
        Else
            read_excel()
        End If
    End Sub

    Private Sub Read_Excel()
        Dim db As New DataTable
        Dim n As Integer
        Dim vaJoin() As Object
        Dim cField As String, cWhere As String
        Dim dtTemp As New DataTable
        Dim cRekening As String
        Dim row As DataRow = dtTable.NewRow()
        Dim nPokok As Double = 0
        Dim nBunga As Double = 0
        Dim nKe As Double = 0

        Dim sconn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection(String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0;", cFile.Text))
        Dim cDataSet As DataSet = New System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", sconn)
        MyCommand.Fill(cDataSet)
        dtTemp = cDataSet.Tables(0)

        For n = 0 To dtTemp.Rows.Count - 1
            With dtTemp.Rows(n)
                cRekening = GetNull(.Item(0), "").ToString
                If cRekening <> "" Then
                    vaJoin = {"Left Join RegisterNasabah r on d.Kode = r.Kode"}
                    cField = ""
                    cField = "d.GolonganKredit,d.Rekening,d.Lama, d.SukuBunga,"
                    cField = cField & "d.Tgl,r.Nama,d.Plafond,d.NoSPK,"
                    cField = String.Format("{0}if(d.caraperhitungan<>'7', d.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = d.rekening AND (a.status = {1} or status = {2}) AND a.tgl <= '{3}'),0),", cField, eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
                    cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = d.rekening and a.tgl <= '{1}'),0)) as SaldoPokok,", cField, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
                    cField = cField & "d.CaraPerhitungan,left(d.rekening,2) as cabang"
                    cWhere = "and d.statuspencairan <> 0"
                    '    If App.LogMode = 0 Then cWhere = " and d.rekening='18.73.000058'"
                    db = objData.Browse(GetDSN, "Debitur d", cField, "d.Rekening", , cRekening, cWhere, , vaJoin)
                    If db.Rows.Count > 0 Then
                        row("Rekening") = cRekening  ' rekening
                        row("Nama") = db.Rows(0).Item("Nama").ToString  ' nama
                        GetTunggakan(.Item("Rekening").ToString, dTgl.DateTime, x, , , True, , .Item("Kode").ToString)
                        row("TunggakanPokok") = x.nTunggakanPokok
                        row("TunggakanBunga") = x.nTunggakanBunga
                        row("TotalTunggakan") = x.nTunggakanPokok + x.nTunggakanBunga
                        nPokok = CDbl(GetNull(.Item(1))) ' pokok
                        nBunga = CDbl(GetNull(.Item(2))) ' Bunga
                        row("Pokok") = nPokok
                        row("Bunga") = nBunga
                        row("TotalAngsuran") = nPokok + nBunga
                        row("SaldoPokok") = CDbl(.Item("SaldoPokok"))
                        GetKewajibanPokokBunga(cRekening, dTgl.DateTime, nBunga, nPokok, True, nKe)
                        row("Ke") = nKe
                        row("Status") = ""
                        If optKas.SelectedIndex = 3 Then
                            row("RekeningTabungan") = x.cRekeningTabungan
                            row("SaldoTabungan") = GetSaldoTabungan(x.cRekeningTabungan, dTgl.DateTime)
                        End If
                        dtTable.Rows.Add(row)
                    End If
                End If
            End With
        Next
        GridControl1.DataSource = dtTable
        InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
        GridColumnFormat(GridView1, "Rekening")
        GridColumnFormat(GridView1, "Nama")
        GridColumnFormat(GridView1, "TunggakanPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "TunggakanBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "TotalTunggakan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "TotalAngsuran", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "SaldoPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Ke", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Status")
        GridColumnFormat(GridView1, "RekeningTabungan")
        GridColumnFormat(GridView1, "SaldoTabungan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        cDataSet.Dispose()
        dtTemp.Dispose()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub trAngsuranInstansiTitipan_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Me.WindowState = FormWindowState.Maximized
        PanelControl1.Width = Me.Width - 20
        GridControl1.Width = PanelControl1.Width '- 100
        PanelControl4.Width = PanelControl1.Width
        GridControl1.Height = Me.Height - PanelControl1.Height - PanelControl4.Height - 60
        PanelControl4.Top = GridControl1.Height + PanelControl1.Height + 15
        cmdSimpan.Left = PanelControl4.Width - cmdSimpan.Width - cmdKeluar.Width - 20
        cmdKeluar.Left = PanelControl4.Width - cmdKeluar.Width - 10
    End Sub
End Class