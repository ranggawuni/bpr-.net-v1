﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trPenolakanPengajuanKredit
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dbData As New DataTable
    Private ReadOnly dTglTemp As Date = #1/1/1900#

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cWilayah.EditValue = aCfg(eCfg.msKodeDefaultWilayah)
        If nPos = myPos.add Then
            If dTglTemp = #1/1/1900# Then
                dTanggal.DateTime = Date.Today
            Else
                dTanggal.DateTime = dTglTemp
            End If
        End If
    End Sub

    Private Sub Koreksi(ByVal lKoreksi As Boolean)
        cNoPengajuan.Enabled = lKoreksi
    End Sub

    Private Sub InitValue()
        dTanggal.DateTime = Date.Today
        dTgl.DateTime = Date.Today
        cNoPengajuan.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cWilayah.Text = ""
        cNamaWilayah.Text = ""
        cAO.Text = ""
        cNamaAO.Text = ""
        nPlafond.Text = ""
        nLama.Text = ""
        cKeterangan.Text = ""
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Benar-benar akan Dihapus", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            Dim vaField() As Object = {"StatusPengajuan", "Keterangan", "TglPenolakan"}
            Dim vaValue() As Object = {"0", "", CDate("1900-01-01")}
            Dim cWhere As String = String.Format("rekening='{0}'", cNoPengajuan.EditValue)
            objData.Edit(GetDSN, "PengajuanKredit", cWhere, vaField, vaValue)
            InitValue()
            MessageBox.Show("Data Sudah Dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        GetEdit(False)
        InitValue()
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If cNoPengajuan.Text.Length < 9 Then
            Return False
            cNoPengajuan.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim vaField() As Object = {"StatusPengajuan", "Keterangan", "TglPenolakan"}
                    Dim vaValue() As Object = {"2", cKeterangan.Text, dTanggal.datetime}
                    objData.Edit(GetDSN, "PengajuanKredit", String.Format("Rekening = '{0}'", cNoPengajuan.EditValue), vaField, vaValue)
                Else
                    cNoPengajuan.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub trPenolakanPengajuanKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("pengajuankredit t", cNoPengajuan, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trPenolakanPengajuanKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        SetTabIndex(dTanggal.TabIndex, n)
        SetTabIndex(cNoPengajuan.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(nPlafond.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTanggal.EnterMoveNextControl = True
        cNoPengajuan.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        nPlafond.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cNoPengajuan.Focus()
        Koreksi(True)
        cNoPengajuan.Enabled = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cNoPengajuan.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cNoPengajuan.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cNoPengajuan_Closed(sender As Object, e As ClosedEventArgs) Handles cNoPengajuan.Closed
        KeteranganLookupSearch(cNoPengajuan, cNama, "rekening", "nama")
        KeteranganLookupSearch(cNoPengajuan, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cNoPengajuan_KeyDown(sender As Object, e As KeyEventArgs) Handles cNoPengajuan.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cNoPengajuan, cNama, "rekening", "nama")
            KeteranganLookupSearch(cNoPengajuan, cAlamat, "rekening", "alamat")
            If Len(cNoPengajuan.Text) = 12 Then
                Const cField As String = "p.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,p.ao,a.nama as NamaAO"
                Dim vaJoin() As Object = {"Left Join RegisterNasabah r on r.Kode = p.Kode",
                                           "Left join AO a on a.kode=p.ao"}
                dbData = objData.Browse(GetDSN, "PengajuanKredit p", cField, "p.Rekening", , cNoPengajuan.Text, , , vaJoin)
                If dbData.Rows.Count > 0 Then
                    With dbData.Rows(0)
                        cNama.Text = GetNull(.Item("NamaDebitur"), "").ToString
                        cAlamat.Text = GetNull(.Item("AlamatDebitur"), "").ToString
                        dTgl.DateTime = CDate(GetNull(.Item("Tgl"), Date.Today))
                        dTanggal.DateTime = CDate(GetNull(.Item("TglPenolakan"), Date.Today))
                        nPlafond.Text = GetNull(.Item("Plafond")).ToString
                        nLama.Text = GetNull(.Item("Lama")).ToString
                        cAO.Text = GetNull(.Item("AO"), "").ToString
                        cNamaAO.Text = GetNull(.Item("NamaAO"), "").ToString
                        cKeterangan.Text = GetNull(.Item("Keterangan"), "").ToString
                        If nPos <> myPos.edit Then
                            If nPos = myPos.add Then
                                If .Item("StatusPengajuan").ToString = "2" Then
                                    MessageBox.Show(String.Format("Pengajuan Untuk No. Rekening {0} Sudah Pernah Ditolak.", cNoPengajuan.Text), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    cNoPengajuan.Text = ""
                                    cNoPengajuan.Focus()
                                    Exit Sub
                                End If
                            End If
                            If .Item("StatusPengajuan").ToString = "1" Then
                                MessageBox.Show(String.Format("Pengajuan Kredit Untuk No. Rekening {0} Sudah Dicairkan.", cNoPengajuan.Text), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                cNoPengajuan.Text = ""
                                cNoPengajuan.Focus()
                                Exit Sub
                            End If
                            If nPos = myPos.delete Then
                                DeleteData()
                                GetEdit(False)
                            End If
                        End If
                    End With
                Else
                    MessageBox.Show("Data Tidak Ditemukan, Ulangi Pengisian ...!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cNoPengajuan.Focus()
                    cNoPengajuan.Text = ""
                End If
            End If
        End If
    End Sub
End Class