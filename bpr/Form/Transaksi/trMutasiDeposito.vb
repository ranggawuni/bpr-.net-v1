﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trMutasiDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Private ReadOnly x As TypeDeposito
    Dim lStatusTabungan As Boolean
    Dim lBungaCair As Boolean
    Dim nTotDeposito As Double
    Dim nBungaTemp As Double
    Dim lHitungPajak As Boolean
    Dim nSaldoMinimumKenaPajak As Double
    Dim nTarifPajak As Single
    Dim nSisaBunga As Double
    Dim nSisaPajak As Double
    Dim nPokokTemp As Double
    Dim dtJadwal As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekeningLama.EditValue = ""
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        cGolonganDeposito.EditValue = ""
        cKeteranganGolonganDeposito.Text = ""
        cGolonganDeposan.EditValue = ""
        cKeteranganGolonganDeposan.Text = ""
        nPenempatan.Text = ""
        nSukuBunga.Text = ""
        nLama.Text = ""
        dTglPenempatan.DateTime = Date.Today
        dTempo.DateTime = Date.Today
        dTglValutaBulanIni.DateTime = Date.Today
        dJthTmpBulanIni.DateTime = Date.Today
        cNoBilyet.Text = ""

        nJumlahHari.Text = ""
        ckPokok.Checked = False
        ckBunga.Checked = False
        nPokok.Text = ""
        nBunga.Text = ""
        nPajak.Text = ""
        nPinalti.Text = ""
        nTotal.Text = ""
        cKeterangan.Text = ""
        optKas.SelectedIndex = 1
        cRekeningTabungan.Text = ""
        cNamaTabungan.Text = ""
        cAlamatTabungan.Text = ""
        nSaldoTabungan.Text = ""
    End Sub

    Private Sub trMutasiDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Dim cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
        cField = "t.rekeninglama,t.rekening,r.nama,r.alamat"
        LookupSearch("deposito t", cRekeningLama, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trMutasiDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cRekeningLama.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(ckPokok.TabIndex, n)
        SetTabIndex(ckBunga.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(nPajak.TabIndex, n)
        SetTabIndex(nPinalti.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(optKas.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekeningLama.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        ckPokok.EnterMoveNextControl = True
        ckBunga.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        nPajak.EnterMoveNextControl = True
        nPinalti.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        optKas.EnterMoveNextControl = True

        FormatTextBox(nPenempatan, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSukuBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nJumlahHari, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPokok, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPajak, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPinalti, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "nominal,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("nominal")) = 0 Then
                        MessageBox.Show("Pembukaan Deposito Belum Dilakukan, Data Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        If CInt(cDataTable.Rows(0).Item("Status")) <> 0 Then
                            MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            InitValue()
                            cRekening.Focus()
                        Else
                            GetMemory()
                        End If
                        If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                            MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.Focus()
                            Exit Sub
                        End If
                        GetMemory()
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        Dim db As New DataTable
        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , False).ToString
        Const cField As String = "Rekening,status"
        Dim cDataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", data.myOperator.Assign, cRekening.Text)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                If CInt(.Item("Status")) <> 0 Then
                    MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cRekening.Focus()
                    Exit Sub
                End If
                GetDeposito(cRekening.Text, dTgl.DateTime, x)
                cNama.Text = x.cNama
                cAlamat.Text = x.cAlamat
                cGolonganDeposan.Text = x.cGolonganDeposan
                cKeteranganGolonganDeposan.Text = x.cNamaGolonganDeposan
                cGolonganDeposito.Text = x.cGolonganDeposito
                cKeteranganGolonganDeposito.Text = x.cNamaGolonganDeposito
                nSaldoMinimumKenaPajak = Val(aCfg(eCfg.msSaldoMinimumKenaPajak))
                nLama.Text = .Item("Lama").ToString
                Timer2.Enabled = False
                lblBlokir.Visible = False
                If x.lStatusBlokir Then
                    Timer2.Enabled = True
                    lblBlokir.Visible = True
                End If
                nBunga.Text = x.nBunga.ToString
                nPajak.Text = x.nPajak.ToString
                nPokok.Text = "0"
                nLama.Text = x.nLama.ToString
                nSukuBunga.Text = x.nSukuBunga.ToString
                nTotDeposito = x.nTotalNominalDeposito
                nBungaTemp = x.nBunga
                nPokokTemp = x.nNominal

                cNoBilyet.Text = x.cNomorBilyet
                lHitungPajak = x.lHitungPajak
                nJumlahHari.Text = x.nJumlahHari.ToString
                nPenempatan.Text = x.nNominal.ToString
                dTempo.DateTime = x.dJthTmpPeriode
                dTglPenempatan.DateTime = x.dTglPembukaan
                dTglValutaBulanIni.DateTime = DateAdd(DateInterval.Month, -1, x.dTgl)
                dJthTmpBulanIni.DateTime = DateAdd(DateInterval.Month, -1, x.dJthTmpBulanIni)
                nTarifPajak = x.nTarifPajak
                nSisaBunga = x.nBungaAccrual2
                nSisaPajak = x.nPajakAccrual2
                cKeterangan.Text = String.Format("Pencairan Bunga Deposito Periode {0} an. {1}", GetLocalDate(x.dTgl, False), cNama.Text)

                If dTgl.DateTime < dJthTmpBulanIni.DateTime Then
                    lblBungaJatuhTempo.Visible = True
                    nPinalti.Enabled = True
                    Timer1.Enabled = True
                Else
                    Timer1.Enabled = False
                    lblBungaJatuhTempo.Visible = False
                End If

                cRekeningTabungan.Text = x.cRekeningTabungan
                cNamaTabungan.Text = x.cNamaNasabahTabungan
                cAlamatTabungan.Text = x.cAlamatNasabahTabungan
                If x.cRekeningTabungan <> "" Then
                    optKas.Properties.Items(2).Enabled = True
                    nSaldoTabungan.Text = GetSaldoTabungan(x.cRekeningTabungan, dTgl.DateTime).ToString
                Else
                    optKas.Properties.Items(2).Enabled = False
                    optKas.SelectedIndex = 1
                    nSaldoTabungan.Text = ""
                End If

                db = objData.Browse(GetDSN, "Tabungan", "close", "Rekening", , cRekeningTabungan.Text)
                If db.Rows.Count > 0 Then
                    If db.Rows(0).Item("Close").ToString = "1" Then
                        lblTabunganTutup.Visible = True
                        Timer3.Enabled = True
                        lStatusTabungan = True
                    Else
                        lblTabunganTutup.Visible = False
                        Timer3.Enabled = False
                        lStatusTabungan = False
                    End If
                End If

                If lBungaCair Then
                    ckBunga.Checked = True
                Else
                    ckPokok.Checked = True
                    nBunga.Text = ""
                    nPajak.Text = ""
                    cKeterangan.Text = "Pencairan Pokok Deposito an. " & cNama.Text
                End If
                SumTotal()
            End With
        End If
        db.Dispose()
    End Sub

    Private Sub SumTotal()
        Dim nTemp As Double = Val(nPokok.Text) + Val(nBunga.Text) - Val(nPinalti.Text) - Val(nPajak.Text)
        nTotal.Text = nTemp.ToString
    End Sub

    Private Function ValidSaving() As Boolean
        Dim nJumlahTemp As Double = 0
        Dim cWhere As String
        Dim nSaldoKasTeller As Double
        Dim db As New DataTable
        Dim cField As String = ""
        Dim vaJoin() As Object = Nothing

        ValidSaving = True
        If Not CheckData(cRekening, "Rekening tidak lengkap. Ulangi pengisian !") Then
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        db = objData.Browse(GetDSN, "Deposito", "Nominal,Status", "Rekening", , cRekening.Text)
        If db.Rows.Count > 0 Then
            If db.Rows(0).Item("Nominal").ToString = "0" Then
                MessageBox.Show("Pembukaan Deposito Belum Dilakukan, Transaksi Tidak Bisa Dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            Else
                If Val(db.Rows(0).Item("Status")) <> 0 Then
                    MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ValidSaving = False
                    cRekening.Focus()
                    Exit Function
                End If
            End If
        Else
            MessageBox.Show("Rekening Tidak Ditemukan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If Val(nBunga.Text) > 0 Then
            cWhere = String.Format(" and date_format(tgl,'%Y%m') = date_format('{0}','%Y%m') and kas <> 'E'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            db = objData.Browse(GetDSN, "mutasideposito", "bunga", "rekening", , cRekening.Text, cWhere)
            If db.Rows.Count > 0 Then
                If Not lBungaCair Then
                    If MessageBox.Show("Bunga Sudah Dicairkan Bulan Ini. " & vbCrLf & "Data Tetap Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbNo Then
                        ValidSaving = False
                        cRekening.Focus()
                        Exit Function
                    End If
                Else
                    MsgBox("Bunga sudah dicairkan.", vbInformation)
                    ValidSaving = False
                    cRekening.Focus()
                    Exit Function
                End If
            End If
        End If

        If Val(nBunga.Text) < 0 Then
            MsgBox("Bunga tidak boleh minus ( - )", vbInformation)
            nBunga.Focus()
            ValidSaving = False
            Exit Function
        End If

        If Val(nPajak.Text) < 0 Then
            MsgBox("Pajak tidak boleh minus ( - )", vbInformation)
            nBunga.Focus()
            ValidSaving = False
            Exit Function
        End If

        If ckPokok.Checked = True Then
            If Val(nBunga.Text) = 0 Then
                MsgBox("Bunga tidak boleh 0", vbInformation)
                nBunga.Focus()
                ValidSaving = False
                Exit Function
            End If
        Else
            If Val(nPokok.Text) = 0 Then
                MsgBox("Pokok tidak boleh 0", vbInformation)
                nPokok.Focus()
                ValidSaving = False
                Exit Function
            End If
        End If
        If ckPokok.Checked = True Then
            nJumlahTemp = Val(nPokok.Text)
        ElseIf ckBunga.Checked = True Then
            nJumlahTemp = Val(nBunga.Text) - Val(nPajak.Text) - Val(nPinalti.Text)
        End If
        If ckPokok.Checked = True And ckBunga.Checked = True Then
            nJumlahTemp = Val(nBunga.Text) - Val(nPajak.Text) - Val(nPinalti.Text) + Val(nPokok.Text)
        End If

        If optKas.SelectedIndex = 2 And lStatusTabungan Then
            MessageBox .Show ("Rekening Tabungan Sudah Ditutup, Transaksi Tidak Bisa Dilanjutkan.", "Error",MessageBoxButtons.OK,MessageBoxIcon.Error )
            ValidSaving = False
            optKas.SelectedIndex = 2
            Exit Function
        End If

        If GetOpt(optKas) = "K" Then
            cField = "u.UserName,u.FullName,u.KasTeller,r.keterangan as NamaRekening"
            cWhere = " and u.KasTeller <> ''"
            vaJoin = {"left join rekening r on r.kode = u.kasteller"}
            db = objData.Browse(GetDSN, "UserName u", cField, "u.UserName", data.myOperator.Content, cUserName, cWhere, , vajoin)
            If db.Rows.Count > 0 Then
                cKasTeller = GetNull(db.Rows(0).Item("KasTeller"), "").ToString
            End If
            nSaldoKasTeller = GetSaldoRekening(cKasTeller, dTgl.DateTime)
            If nSaldoKasTeller < 0 Then
                MsgBox("Saldo Kas Minus!!!  " & formatValue(nSaldoKasTeller, formatType.BilRpPict), vbInformation)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
            If nSaldoKasTeller - Val(nTotal.Text) < 0 Then
                MsgBox("Mutasi Melebihi Saldo Kas!!!  " & formatValue(nSaldoKasTeller, formatType.BilRpPict), vbInformation)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim dJthTmp As Date
        Dim cKodeCabang As String
        Dim cJenisKas As String
        Dim vaField() As Object
        Dim vaValue() As Object

        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , True)
        If ValidSaving() Then
            dJthTmp = DateAdd("m", 1, dTgl.DateTime)
            DelBukuBesar(cFaktur.Text)

            If ckPokok.Checked = True Then
                vaField = {"Status", "TglCair"}
                vaValue = {"1", dTgl.DateTime}
                objData.Edit(GetDSN, "Deposito", String.Format("Rekening = '{0}'", cRekening.Text), vaField, vaValue)
            End If

            cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
            cJenisKas = GetOpt(optKas)
            UpdMutasiDeposito(cKodeCabang, cFaktur.Text, cRekening.Text, dTgl.DateTime, dJthTmp, , Val(nPokok.Text), Val(nBunga.Text), _
                              Val(nPajak.Text), Val(nPinalti.Text), True, True, , , cJenisKas, , , nSisaBunga, nSisaPajak, cKeterangan.Text)

            PerpanjanganDeposito()

            If ckBunga.Checked = True Then
                If MessageBox.Show("Cetak Validasi Bunga ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                    CetakValidasiDepositoDetail(cFaktur.Text)
                End If
                If MessageBox.Show("Cetak Tanda Terima Bunga Deposito ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                    'With trCetakTandaTerimaPembayaranBungaDeposito
                    '    .dTgl.Value = dTgl.Value
                    'End With
                    'trCetakTandaTerimaPembayaranBungaDeposito.Action Me
                End If
            End If

            If ckPokok.Checked = True Then
                If MessageBox.Show("Cetak Validasi Pokok ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                    CetakValidasiDepositoDetail(cFaktur.Text)
                End If
            End If
            InitValue()
        End If
    End Sub

    Function GetTglDeposito(ByVal dTglProses As Date, ByVal dTglPenempatan As Date) As Date
        If dTglProses.Day = EOM(dTglPenempatan).Day And dTglProses.Day < dTglPenempatan.Day Then
            GetTglDeposito = dTglProses
        Else
            GetTglDeposito = DateSerial(dTglProses.Year, dTglProses.Month, dTglPenempatan.Day)
        End If
    End Function

    Private Sub PerpanjanganDeposito()
        Dim cFakturPerpanjangan As String
        Dim dTglPerpanjangan As Date
        Dim dJthTmpPerpanjangan As Date
        Dim db As New DataTable
        Dim nID As Double
        Dim n As Double
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim dTglValutaTemp As Date
        Const cField As String = "m.rekening,m.tgl,g.lama,date_add(m.tgl,interval g.lama month) as jatuhtempo"
        Dim cWhere As String = "and m.kas='E'"
        Dim vaJoin() As Object = {"left join deposito d on d.rekening = m.rekening",
                                  "left join golongandeposito g on g.kode = d.golongandeposito"}
        db = objData.Browse(GetDSN, "mutasideposito m", cField, "m.rekening", , cRekening.Text, cWhere,
                            "m.tgl desc", vaJoin)
        If db.Rows.Count > 0 Then
            With db.Rows(0)
                If dTgl.DateTime >= CDate(.Item("jatuhtempo")) Then
                    dTglValutaTemp = GetTglDeposito(dTgl.DateTime, dTglPenempatan.DateTime)
                    HitungJadwal(dTglValutaTemp)

                    cFakturPerpanjangan = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , True)
                    nID = GetID(dTgl.DateTime)

                    dTglPerpanjangan = CDate(dtJadwal.Rows(0).Item("Valuta"))
                    dJthTmpPerpanjangan = CDate(dtJadwal.Rows(dtJadwal.Rows.Count - 1).Item("JthTmp"))
                    Dim cKodeCabang As String = cRekening.Text.Substring(0, 2)
                    UpdMutasiDeposito(cKodeCabang, cFakturPerpanjangan, cRekening.Text, dTglPerpanjangan, _
                                      dJthTmpPerpanjangan, , , , , , True, , , , "E", Val(nSukuBunga.Text))

                    For n = 0 To dtJadwal.Rows.Count - 1
                        Dim nKe As Double = Val(dtJadwal.Rows(CInt(n)).Item("Ke"))
                        Dim dValutaTemp As Date = CDate(dtJadwal.Rows(CInt(n)).Item("Valuta"))
                        Dim dTempoTemp As Date = CDate(dtJadwal.Rows(CInt(n)).Item("JthTmp"))
                        cWhere = ""
                        cWhere = " and bulanke = " & nKe
                        cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(dValutaTemp, formatType.yyyy_MM_dd))
                        cWhere = String.Format("{0} and date_format(jthtmp,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(dTempoTemp, formatType.yyyy_MM_dd))
                        cWhere = cWhere & " and status=0"
                        objData.Delete(GetDSN, "jadwalbungadeposito", "rekening", , cRekening.Text, cWhere)

                        vaField = {"cabangentry", "rekening", "bulanke", "tgl",
                                   "jthtmp", "status", "tglpembukaan", "datetime", "sukubunga",
                                   "PerpanjanganKe", "faktur"}
                        vaValue = {cKodeCabang, cRekening.Text, nKe, formatValue(dValutaTemp, formatType.yyyy_MM_dd),
                                   formatValue(dTempoTemp, formatType.yyyy_MM_dd), 0, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd), SNow(), Val(nSukuBunga.Text),
                                   0, cFakturPerpanjangan}
                        objData.Add(GetDSN, "jadwalbungadeposito", vaField, vaValue)
                    Next
                End If
            End With
        End If
        db.Dispose()
    End Sub

    Private Sub HitungJadwal(ByVal dTanggal As Date)
        Dim dTemp As Date = dTglPenempatan.DateTime
        Dim dValuta As Date
        Dim dJatuhTempo1 As Date
        Dim n As Integer = CInt(nLama.Text)
        Dim i As Integer = 1
        Dim m As Double = 0

        dtJadwal.Clear()
        dJatuhTempo1 = DateAdd("m", Val(nLama.Text), dTanggal)
        Do Until i > Val(nLama.Text)
            dTemp = DateAdd("m", -(n - i), dJatuhTempo1)
            dValuta = DateAdd("m", -1, dTemp)
            m = m + 1
            dtJadwal.Rows.Add(New Object() {m, dValuta, dTemp, GetLocalDate(dValuta, False)})
            i = i + 1
        Loop
    End Sub

    Private Sub InitTable()
        AddColumn(dtJadwal, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(dtJadwal, "Valuta", System.Type.GetType("System.Date"))
        AddColumn(dtJadwal, "JthTmp", System.Type.GetType("System.Date"))
        AddColumn(dtJadwal, "Keterangan", System.Type.GetType("System.String"))
    End Sub

    Private Sub dTgl_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dTgl.LostFocus
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

    Private Sub ckBunga_Click(sender As Object, e As EventArgs) Handles ckBunga.Click
        If ckBunga.Checked = True Then
            InitBunga(True)
            CekStatusBunga()
        Else
            InitBunga(False)
        End If
        setBungaPajak()
        SumTotal()
        SetKeterangan()
    End Sub

    Private Sub InitBunga(Optional ByVal lPar As Boolean = True)
        Dim nTemp As Double = 0
        If lPar Then
            nBunga.Text = ""
            nPajak.Text = ""
            nPinalti.Text = ""
        Else
            nBunga.Text = nBungaTemp.ToString
            If nTotDeposito > Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Then
                ntemp = Math.Round(Val(nBunga.Text) * nTarifPajak / 100)
                nPajak.Text = nTemp.ToString
            End If
            nTemp = Val(nPokok.Text) + Val(nBunga.Text) - Val(nPinalti.Text) - Val(nPajak.Text)
            nTotal.Text = nTemp.ToString
        End If
    End Sub

    Private Sub CekStatusBunga()
        Dim db As New DataTable
        Dim cWhere As String = String.Format(" and date_format(tgl,'%Y%m') = date_format('{0}','%Y%m') and kas <> 'E' ", formatValue(dTgl.Text, formatType.yyyy_MM_dd))
        db = objData.Browse(GetDSN, "mutasideposito", "bunga", "rekening", , cRekening.Text, cWhere)
        If db.Rows.Count > 0 Then
            If MessageBox.Show("Bunga sudah dicairkan." & vbCrLf & "Transaksi Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                If nUserLevel <= 1 Then
                    lBungaCair = False
                Else
                    MsgBox("Anda Tidak Memiliki Hak Akses." & vbCrLf & "Hubungi Administrator Untuk Melanjutkan Proses.", vbInformation)
                    lBungaCair = True
                    cRekeningLama.Focus()
                    Exit Sub
                End If
            Else
                lBungaCair = True
                cRekeningLama.Focus()
            End If
        End If
        db.Dispose()
    End Sub

    Private Sub setBungaPajak()
        If ckPokok.Checked = True Then
            nPokok.Text = x.nNominal.ToString
            nPinalti.Enabled = True
        Else
            nPokok.Text = ""
            nPinalti.Text = ""
        End If
        If ckBunga.Checked = True Then
            InitBunga(False)
        Else
            InitBunga(True)
        End If
    End Sub

    Private Sub SetKeterangan()
        If ckPokok.Checked = True Then
            cKeterangan.Text = String.Format("Pencairan Pokok Deposito No. Rekening {0} an. {1}", cRekening.Text, cNama.Text)
        End If
        If ckBunga.Checked = True Then
            cKeterangan.Text = String.Format("Pencairan Bunga Deposito No. Rekening {0} an. {1}", cRekening.Text, cNama.Text)
        End If
        If ckPokok.Checked = True And ckBunga.Checked = True Then
            cKeterangan.Text = String.Format("Pencairan Pokok Dan Bunga Deposito No. Rekening {0} an. {1}", cRekening.Text, cNama.Text)
        End If
    End Sub

    Private Sub ckBunga_KeyDown(sender As Object, e As KeyEventArgs) Handles ckBunga.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub ckPokok_Click(sender As Object, e As EventArgs) Handles ckPokok.Click
        If ckPokok.Checked = True Then
            nPokok.Text = nPokokTemp.ToString
        Else
            nPokok.Text = ""
        End If
        SumTotal()
        SetKeterangan()
    End Sub

    Private Sub ckPokok_KeyDown(sender As Object, e As KeyEventArgs) Handles ckPokok.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If
    End Sub
End Class