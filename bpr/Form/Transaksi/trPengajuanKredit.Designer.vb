﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trPengajuanKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trPengajuanKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.cNoPengajuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nPlafond = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaAO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cAO = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaWilayah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cWilayah = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cCabang1 = New DevExpress.XtraEditors.TextEdit()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNoPengajuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.cNoPengajuan)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.nLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.nPlafond)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.cNamaAO)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.cAO)
        Me.PanelControl1.Controls.Add(Me.cNamaWilayah)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cWilayah)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cCabang1)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(1, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(573, 268)
        Me.PanelControl1.TabIndex = 17
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 34)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 202
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(103, 31)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 201
        '
        'cNoPengajuan
        '
        Me.cNoPengajuan.Location = New System.Drawing.Point(103, 135)
        Me.cNoPengajuan.Name = "cNoPengajuan"
        Me.cNoPengajuan.Size = New System.Drawing.Size(105, 20)
        Me.cNoPengajuan.TabIndex = 200
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(139, 242)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl6.TabIndex = 199
        Me.LabelControl6.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.EditValue = "12"
        Me.nLama.Location = New System.Drawing.Point(103, 239)
        Me.nLama.Name = "nLama"
        Me.nLama.Properties.Mask.EditMask = "##.######"
        Me.nLama.Properties.MaxLength = 2
        Me.nLama.Size = New System.Drawing.Size(30, 20)
        Me.nLama.TabIndex = 198
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(8, 242)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl5.TabIndex = 197
        Me.LabelControl5.Text = "Lama Kredit"
        '
        'nPlafond
        '
        Me.nPlafond.Location = New System.Drawing.Point(103, 213)
        Me.nPlafond.Name = "nPlafond"
        Me.nPlafond.Size = New System.Drawing.Size(105, 20)
        Me.nPlafond.TabIndex = 196
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(8, 217)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl4.TabIndex = 195
        Me.LabelControl4.Text = "Jumlah Pengajuan"
        '
        'cNamaAO
        '
        Me.cNamaAO.Enabled = False
        Me.cNamaAO.Location = New System.Drawing.Point(153, 187)
        Me.cNamaAO.Name = "cNamaAO"
        Me.cNamaAO.Size = New System.Drawing.Size(244, 20)
        Me.cNamaAO.TabIndex = 193
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(8, 190)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl3.TabIndex = 192
        Me.LabelControl3.Text = "AO"
        '
        'cAO
        '
        Me.cAO.Location = New System.Drawing.Point(103, 187)
        Me.cAO.Name = "cAO"
        Me.cAO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAO.Size = New System.Drawing.Size(44, 20)
        Me.cAO.TabIndex = 194
        '
        'cNamaWilayah
        '
        Me.cNamaWilayah.Enabled = False
        Me.cNamaWilayah.Location = New System.Drawing.Point(153, 161)
        Me.cNamaWilayah.Name = "cNamaWilayah"
        Me.cNamaWilayah.Size = New System.Drawing.Size(244, 20)
        Me.cNamaWilayah.TabIndex = 190
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 164)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl2.TabIndex = 189
        Me.LabelControl2.Text = "Wilayah"
        '
        'cWilayah
        '
        Me.cWilayah.Location = New System.Drawing.Point(103, 161)
        Me.cWilayah.Name = "cWilayah"
        Me.cWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cWilayah.Size = New System.Drawing.Size(44, 20)
        Me.cWilayah.TabIndex = 191
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 138)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 137
        Me.LabelControl1.Text = "No. Pengajuan"
        '
        'cCabang1
        '
        Me.cCabang1.EditValue = "12"
        Me.cCabang1.Location = New System.Drawing.Point(103, 5)
        Me.cCabang1.Name = "cCabang1"
        Me.cCabang1.Properties.Mask.EditMask = "##.######"
        Me.cCabang1.Properties.MaxLength = 2
        Me.cCabang1.Size = New System.Drawing.Size(30, 20)
        Me.cCabang1.TabIndex = 135
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(103, 109)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(8, 113)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(103, 83)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(456, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 86)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 57)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 60)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(8, 8)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(60, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Register"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(139, 5)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(1, 276)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(573, 33)
        Me.PanelControl2.TabIndex = 19
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(484, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(403, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'trPengajuanKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 312)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trPengajuanKredit"
        Me.Text = "Pengajuan Kredit"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNoPengajuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cCabang1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPlafond As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaAO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAO As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaWilayah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cWilayah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNoPengajuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
End Class
