﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trKoreksiDeposan
    ReadOnly objData As New data()
    Private ReadOnly x As TypeDeposito
    Dim nPlafond As Double
    Dim dtJadwal As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cGolonganDeposan.EditValue = ""
        cNamaGolonganDeposan.Text = ""
        nLama.Text = ""
        nBunga.Text = ""
        optDeposan.SelectedIndex = 1
        optARO.SelectedIndex = 2
        optPajak.SelectedIndex = 1
        dTgl.DateTime = Date.Today
        dJthTmp.DateTime = Date.Today
        nNominal.Text = ""
        cNasabah.EditValue = ""
        cKeteranganNasabah.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        cAlamatNasabah.Text = ""
        nSaldoTabungan.Text = ""
    End Sub

    Private Sub trKoreksiDeposan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trKoreksiDeposan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cGolonganDeposan.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(optDeposan.TabIndex, n)
        SetTabIndex(optARO.TabIndex, n)
        SetTabIndex(optPajak.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dJthTmp.TabIndex, n)
        SetTabIndex(cNasabah.TabIndex, n)
        SetTabIndex(cRekeningTabungan.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        cGolonganDeposan.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        optDeposan.EnterMoveNextControl = True
        optARO.EnterMoveNextControl = True
        optPajak.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        dJthTmp.EnterMoveNextControl = True
        cNasabah.EnterMoveNextControl = True
        cRekeningTabungan.EnterMoveNextControl = True

        FormatTextBox(nBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNominal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "nominal,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    GetMemory()
                End If
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.EditValue = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub GetMemory()
        Dim db As New DataTable
        Const cField As String = "d.*,c.Keterangan as NasabahKantor"
        Dim vaJoin() As Object = {"Left join cabang c on c.kode = d.nasabah"}
        Dim cDataTable = objData.Browse(GetDSN, "deposito d", cField, "d.rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                If CInt(.Item("Status")) <> 0 Then
                    MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cRekening.Focus()
                    Exit Sub
                End If
                GetDeposito(cRekening.Text, dTgl.DateTime, x)
                cNama.Text = x.cNama
                cAlamat.Text = x.cAlamat
                cGolonganDeposan.Text = x.cGolonganDeposan
                cNamaGolonganDeposan.Text = x.cNamaGolonganDeposan
                nLama.Text = .Item("Lama").ToString
                nBunga.Text = x.nSukuBunga.ToString
                dTgl.DateTime = x.dTgl
                dJthTmp.DateTime = x.dJthTmpPeriode
                nNominal.Text = x.nNominalSetoranDeposito.ToString
                nPlafond = x.nNominalSetoranDeposito
                cNasabah.Text = .Item("Nasabah").ToString
                cKeteranganNasabah.Text = .Item("NasabahKantor").ToString
                SetOpt(optDeposan, .Item("JenisDeposan").ToString)
                SetOpt(optPajak, .Item("StatusPajak").ToString)
                SetOpt(optARO, .Item("ARO").ToString)

                If cRekeningTabungan.Text.Length = 12 Then
                    cRekeningTabungan.Text = x.cRekeningTabungan
                    cNamaNasabah.Text = x.cNamaNasabahTabungan
                    cAlamatNasabah.Text = x.cAlamatNasabahTabungan
                    nSaldoTabungan.Text = GetSaldoTabungan(cRekeningTabungan.Text, dTgl.DateTime).ToString
                End If
            End With
        End If
        db.Dispose()
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(cRekening, "Rekening tidak lengkap. Ulangi pengisian !") Then
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim vaField() As Object
        Dim vaValue() As Object

        If ValidSaving() Then
            vaField = {"Rekening", "GolonganDeposan", "ARO",
                       "SukuBunga", "RekeningTabungan", "Tgl",
                       "JTHTMP", "StatusPajak", "JenisDeposan",
                       "Nominal", "Nasabah"}
            vaValue = {cRekening, cGolonganDeposan.Text, GetOpt(optARO),
                       Val(nBunga.Text), cRekeningTabungan, dTgl.DateTime,
                       dJthTmp.DateTime, GetOpt(optPajak), GetOpt(optDeposan),
                       Val(nNominal.Text), cNasabah.Text}
            Dim cWhere As String = String.Format("rekening ='{0}'", cRekening.Text)
            objData.Edit(GetDSN, "Deposito", cWhere, vaField, vaValue)

            vaField = {"Tgl", "JTHTMP"}
            vaValue = {dTgl.DateTime, dJthTmp.DateTime}
            cWhere = String.Format("Jenis = '1' and Rekening = '{0}' and setoranplafond={1}", cRekening.Text, nPlafond)
            objData.Edit(GetDSN, "MutasiDeposito", cWhere, vaField, vaValue)
            InitValue()
        End If
    End Sub

    Function GetTglDeposito(ByVal dTglProses As Date, ByVal dTglPenempatan As Date) As Date
        If dTglProses.Day = EOM(dTglPenempatan).Day And dTglProses.Day < dTglPenempatan.Day Then
            GetTglDeposito = dTglProses
        Else
            GetTglDeposito = DateSerial(dTglProses.Year, dTglProses.Month, dTglPenempatan.Day)
        End If
    End Function

    Private Sub cRekeningTabungan_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningTabungan.KeyDown
        If e.KeyCode = Keys.Enter Then
            If e.KeyCode = Keys.Enter Then
                KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
                KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
                If Len(cRekening.Text) = 12 Then
                    Const cField As String = "r.nama,r.alamat"
                    Dim vaJoin() As Object = {"left join registernasabah r on r.kode = t.kode"}
                    Dim cDataTable As DataTable = objData.Browse(GetDSN, "tabungan t", cField, "t.rekening", , cRekeningTabungan.Text,
                                                                 , , vaJoin)
                    If cDataTable.Rows.Count > 0 Then
                        With cDataTable.Rows(0)
                            cNamaNasabah.Text = .Item("Nama").ToString
                            cAlamatNasabah.Text = .Item("Alamat").ToString
                            nSaldoTabungan.Text = GetSaldoTabungan(cRekeningTabungan.Text, dTgl.DateTime).ToString
                        End With
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub
End Class