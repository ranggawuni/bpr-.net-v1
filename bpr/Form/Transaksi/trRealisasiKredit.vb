﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trRealisasiKredit
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim cKodeRegister As String
    Private ReadOnly dTglTemp As Date = #1/1/1900#
    Dim dtJadwal As New DataTable
    Dim lNoDelete As Boolean
    Dim cKodeRegisterLama As String
    Private ReadOnly nCaraPerhitungan As Integer
    Dim nBungaPenyesuaianTemp As Double
    Dim nPokokPenyesuaianTemp As Double
    Dim cRekeningTemp As String
    Dim dTanggalRealisasi As Date
    Dim nPlafondLama As Double
    Dim cCaraPencairan As String
    Dim nStatusKredit As JenisStatusKredit
    Dim cWhereSifatKredit As String
    Dim cWhereSektorEkonomi As String
    Dim cWhereGolonganPenjamin As String
    Dim vaSifatKredit As New DataTable
    Dim vaSektorEkonomi As New DataTable
    Dim vaJenisPenggunaan As New DataTable
    Dim vaGolonganDebitur As New DataTable
    Dim vaGolonganPenjamin As New DataTable
    Dim vaWilayah As New DataTable
    Dim vaJadwal As New DataTable

    Private Sub HitungAdm()
        nAdministrasi.Text = (CDbl(nPersenAdm.Text) / 100 * CDbl(nPlafond.Text)).ToString
    End Sub

    Private Sub HitungProvisi()
        nProvisi.Text = (CDbl(nPersenProvisi.Text) / 100 * CDbl(nPlafond.Text)).ToString
    End Sub

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cWilayah.EditValue = aCfg(eCfg.msKodeDefaultWilayah)
        If nPos = myPos.add Then
            If dTglTemp = #1/1/1900# Then
                dTgl.DateTime = Date.Today
            Else
                dTgl.DateTime = dTglTemp
            End If
        End If
    End Sub

    Private Sub Koreksi(ByVal lKoreksi As Boolean)
        cCabang1.Enabled = lKoreksi
        cRekening.Enabled = lKoreksi
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCabang1.Text = CStr(aCfg(eCfg.msKodeCabang))
        cKode.Text = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cRekening.EditValue = ""
        dTgl.DateTime = Now.Date
        cPengajuan.EditValue = ""
        cNamaPengajuan.Text = ""
        nPlafondPengajuan.Text = ""
        cNoSPK.Text = ""
        cGolonganKredit.EditValue = ""
        cNamaGolonganKredit.Text = ""
        cSifatKredit.EditValue = ""
        cNamaSifatKredit.Text = ""
        cJenisPenggunaan.EditValue = ""
        cNamaJenisPenggunaan.Text = ""
        cGolonganDebitur.EditValue = ""
        cNamaGolonganDebitur.Text = ""
        cSektorEkonomi.EditValue = ""
        cNamaSektorEkonomi.Text = ""
        cWilayah.EditValue = ""
        cNamaWilayah.Text = ""
        cBendahara.EditValue = ""
        cNamaBendahara.Text = ""
        cAO.EditValue = ""
        cNamaAO.Text = ""
        cGolonganPenjamin.EditValue = ""
        cNamaGolonganPenjamin.Text = ""
        optAsuransi.SelectedIndex = 1
        cBagianYangDijamin.Text = ""
        cbKeterkaitan.EditValue = ""
        cbSumberDana.EditValue = ""
        cbPeriodePembayaran.EditValue = ""
        cbJenisUsaha.EditValue = ""
        optAutoDebet.SelectedIndex = 1
        nLama.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        cAlamatNasabah.Text = ""
        nNomor.Text = ""
        cJaminan.Text = ""
        cKeteranganJaminan.Text = ""
        nNilaiPengikatan.Text = ""
        nNilaiJaminan.Text = ""
        nNilaiNJOP.Text = ""
        nPersBunga.Text = ""
        nPersBungaBerikutnya.Text = ""
        nBungaEfektif.Text = ""
        nLama.Text = ""
        nGraceBunga.Text = ""
        nGracePokok.Text = ""
        cCaraPerhitungan.Text = ""
        cNamaCaraPerhitungan.Text = ""
        nPlafond.Text = ""
        nBunga.Text = ""
        optDenda.SelectedIndex = 1
        optJenis.SelectedIndex = 1
        optKaryawan.SelectedIndex = 2
        optInstansi.SelectedIndex = 2
        cAdministrasi.EditValue = ""
        nPersenAdm.Text = ""
        nAdministrasi.Text = ""
        nNotaris.Text = ""
        nMaterai.Text = ""
        nAsuransi.Text = ""
        optProvisi.SelectedIndex = 1
        cProvisi.EditValue = ""
        nPersenProvisi.Text = ""
        nProvisi.Text = ""
        nTotal.Text = ""
        nPembulatan.Text = ""
        nAngsuran.Text = ""
        nBungaPenyesuaian.Text = ""
        nPokokPenyesuaian.Text = ""
    End Sub

    Private Function ValidDeleted() As Boolean
        ValidDeleted = True
        Dim db As New DataTable
        ' Cek apakah Pernah ada angsuran
        db = objData.Browse(GetDSN, "Debitur", "statuspencairan", "Rekening", , cRekening.Text)
        If db.Rows.Count > 0 Then
            If db.Rows(0).Item("statuspencairan").ToString <> "0" Then
                MsgBox("Rekening Sudah Di Cairkan, Batalkan Terlebih dahulu Pencairan Kredit ...!", vbExclamation)
                ValidDeleted = False
                db.Dispose()
                Exit Function
            End If
        Else
            MsgBox("Rekening Tidak Ditemukan, Penghapusan Tidak bisa Dilanjutkan ...!", vbExclamation)
            ValidDeleted = False
            db.Dispose()
            Exit Function
        End If
    End Function

    Private Sub DeleteData()
        Dim db As New DataTable
        If ValidDeleted() Then
            If MessageBox.Show("Data Benar-benar akan Dihapus", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                Dim cWhere As String = String.Format("and (status <> {0} and status <> {1})", eAngsuran.ags_jadwal, eAngsuran.ags_Realisasi)
                db = objData.Browse(GetDSN, "angsuran", "rekening,kpokok,kbunga", "rekening", , cRekening.Text, cWhere)
                If db.Rows.Count > 0 Then
                    MsgBox("Rekening Tidak Bisa Dihapus." & vbCrLf & "Nasabah Masih Memiliki Data Mutasi.", vbExclamation)
                    lNoDelete = True
                Else
                    objData.Delete(GetDSN, "debitur", "Rekening", , cRekening.Text)
                    cWhere = "and status=" & eAngsuran.ags_jadwal
                    objData.Delete(GetDSN, "angsuran", "rekening", , cRekening.Text, cWhere)
                    lNoDelete = False
                    InitValue()
                    MessageBox.Show("Data Sudah Dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                GetEdit(False)
                InitValue()
            End If
            db.Dispose()
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As DataTable
        ValidSaving = True

        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(cNoSPK.Text, "Nomor PK harus diisi. Ulangi pengisian.") Then
            Return False
            cNoSPK.Focus()
            Exit Function
        End If

        If Not CheckData(cSifatKredit.Text, "Sifat Kredit harus diisi. Ulangi pengisian.") Then
            Return False
            cSifatKredit.Focus()
            Exit Function
        End If

        If Not CheckData(cJenisPenggunaan.Text, "Jenis Penggunaan harus diisi. Ulangi pengisian.") Then
            Return False
            cJenisPenggunaan.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganDebitur.Text, "Golongan Debitur harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganDebitur.Focus()
            Exit Function
        End If

        If Not CheckData(cSektorEkonomi.Text, "Sektor Ekonomi harus diisi. Ulangi pengisian.") Then
            Return False
            cSektorEkonomi.Focus()
            Exit Function
        End If

        If Not CheckData(cWilayah.Text, "Wilayah harus diisi. Ulangi pengisian.") Then
            Return False
            cWilayah.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganPenjamin.Text, "Golongan penjamin harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganPenjamin.Focus()
            Exit Function
        End If

        If Not CheckData(cAO.Text, "AO harus diisi. Ulangi pengisian.") Then
            Return False
            cAO.Focus()
            Exit Function
        End If

        If Not CheckData(nPersBunga.Text, "Suku Bunga harus diisi. Ulangi pengisian.") Then
            Return False
            nPersBunga.Focus()
            Exit Function
        End If

        If Len(cRekening.Text) < 10 Then
            Return False
            cRekening.Focus()
            Exit Function
        End If

        If nPos = myPos.add Then
            cDataTable = objData.Browse(GetDSN, "Debitur", "rekening", "rekening", data.myOperator.Assign, cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                MessageBox.Show("Nomor rekening sudah digunakan. Ulangi pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
                cRekening.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub IsiJadwal()
        Dim n As Integer
        Dim nTotalPokok As Double = 0
        Dim nTotalBunga As Double = 0
        Dim nBaris As Integer
        '  If nPokokPenyesuaian.Value + nBungaPenyesuaian.Value = 0 Then GetDataJadwal: Exit Sub
        vaJadwal.Clear()
        AddColumn(vaJadwal, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(vaJadwal, "JthTmp", System.Type.GetType("System.DateTime"))
        AddColumn(vaJadwal, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(vaJadwal, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(vaJadwal, "Jumlah", System.Type.GetType("System.Double"))
        AddColumn(vaJadwal, "SisaBunga", System.Type.GetType("System.Double"))
        AddColumn(vaJadwal, "SisaPokok", System.Type.GetType("System.Double"))
        For n = 0 To CInt(nLama.Text)
            If n = 0 Then
                vaJadwal.Rows.Add(New Object() {Nothing, Nothing, Nothing, Nothing, Nothing, CDbl(nBunga.Text), CDbl(nPlafond.Text)})
            Else
                vaJadwal.Rows.Add(New Object() {0, 0, 0, 0, 0, 0, 0})
            End If
        Next
        For n = 1 To vaJadwal.Rows.Count - 1
            With vaJadwal
                .Rows(n).Item(0) = n
                .Rows(n).Item(1) = DateAdd("m", n, dTgl.DateTime)
                .Rows(n).Item(2) = CDbl(nBungaPenyesuaian.Text)
                .Rows(n).Item(3) = CDbl(nPokokPenyesuaian.Text)
                .Rows(n).Item(4) = CDbl(nPokokPenyesuaian.Text) + CDbl(nBungaPenyesuaian.Text)
                .Rows(n).Item(5) = Val(.Rows(n - 1).Item(5)) - Val(.Rows(n).Item(2))
                .Rows(n).Item(6) = Val(.Rows(n - 1).Item(6)) - Val(.Rows(n).Item(3))
                nTotalPokok = nTotalPokok + CDbl(nPokokPenyesuaian.Text)
                nTotalBunga = nTotalBunga + CDbl(nBungaPenyesuaian.Text)
            End With
        Next
        nTotalPokok = CDbl(nPlafond.Text) - nTotalPokok
        nTotalBunga = CDbl(nBunga.Text) - nTotalBunga
        If nTotalPokok + nTotalBunga <> 0 Then
            With vaJadwal
                nBaris = .Rows.Count - 1
                .Rows(nBaris).Item(2) = Val(.Rows(nBaris).Item(2)) + nTotalBunga
                .Rows(nBaris).Item(3) = Val(.Rows(nBaris).Item(3)) + nTotalPokok
                .Rows(nBaris).Item(4) = Val(.Rows(nBaris).Item(2)) + Val(.Rows(nBaris).Item(3))
                .Rows(nBaris).Item(5) = Val(.Rows(nBaris - 1).Item(5)) - Val(.Rows(nBaris).Item(2))
                .Rows(nBaris).Item(6) = Val(.Rows(nBaris - 1).Item(6)) - Val(.Rows(nBaris).Item(3))
            End With
        End If
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView3)
        GridColumnFormat(GridView3, "Ke")
        GridColumnFormat(GridView3, "JthTmp", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, , DevExpress.Utils.HorzAlignment.Center)
        GridColumnFormat(GridView3, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView3, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView3, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView3, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView3, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cWhere As String = ""
                    Dim cKeteranganTemp As String = ""
                    Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
                    Dim cJenisProvisi As String = GetOpt(optProvisi)
                    Dim cAutoDebet As String = GetOpt(optAutoDebet)
                    Dim cKeterkaitanTemp As String = cbKeterkaitan.Text.Substring(0, 1)
                    Dim cSumberDanaTemp As String = cbSumberDana.Text.Substring(0, 2)
                    Dim cPeriodePembayaranTemp As String = cbPeriodePembayaran.Text.Substring(0, 1)
                    Dim cJenisUsahaTemp As String = cbJenisUsaha.Text.Substring(0, 1)
                    Dim cKaryawan As String = GetOpt(optKaryawan)
                    Dim cInstansi As String = GetOpt(optInstansi)
                    Dim cKodeTemp As String = String.Format("{0}.{1}", cCabang1.Text, cKode.Text)
                    Dim cJenisRealisasi As String = GetOpt(optJenis)
                    Dim cHitungDenda As String = GetOpt(optDenda)
                    UpdRealisasi(cRekening.Text, cWilayah.Text, cKodeTemp, cGolonganKredit.Text,
                                 cGolonganDebitur.Text, cSektorEkonomi.Text, cSifatKredit.Text, cJenisPenggunaan.Text,
                                 cGolonganPenjamin.Text, cNoSPK.Text, CDbl(nPersBunga.Text), CDbl(nBungaEfektif.Text), dTgl.DateTime,
                                 cCaraPerhitungan.Text, CDbl(nPlafond.Text), CDbl(nLama.Text), cAO.Text, CDbl(nAdministrasi.Text),
                                 CDbl(nAsuransi.Text), CDbl(nMaterai.Text), CDbl(nNotaris.Text), CDbl(nProvisi.Text), cPengajuan.Text,
                                 CDbl(nBunga.Text), cRekeningTabungan.Text, CDbl(nGracePokok.Text), CDbl(nGraceBunga.Text),
                                 CDbl(nPembulatan.Text), CDbl(nAngsuran.Text), cHitungDenda, cJenisRealisasi, cBagianYangDijamin.Text, ,
                                 cJenisProvisi, cAutoDebet, cKodeCabang, cKeterkaitanTemp, cSumberDanaTemp, cPeriodePembayaranTemp,
                                 cJenisUsahaTemp, cKaryawan, cInstansi, cBendahara.Text, CSng(nPersBungaBerikutnya.Text), nPos)
                    ' kalau kredit RK tidak perlu update jadwal karena bunga dihitung harian berdasarkan saldo
                    If cCaraPerhitungan.Text <> "7" Then
                        ' update jadwal angsuran
                        If nPos = myPos.add Then
                            cWhere = String.Format(" status = {0} and Rekening = '{1}'", eAngsuran.ags_jadwal, cRekening.Text)
                            objData.Delete(GetDSN, "Angsuran", , , , cWhere)
                            Dim cFaktur As String = GetLastFaktur(eFaktur.fkt_Jadwal_Angsuran, dTgl.DateTime, , True)
                            For n = 1 To vaJadwal.Rows.Count - 1
                                With vaJadwal.Rows(n)
                                    cKeteranganTemp = "Jadwal Angsuran Ke. " & n
                                    UpdAngsuranPembiayaan(cRekening.Text.Substring(0, 2), cFaktur, CDate(.Item(1)), cRekening.Text, eAngsuran.ags_jadwal,
                                                          CDbl(.Item(3)), CDbl(.Item(2)), 0, cKeteranganTemp, False, False, , , , "")
                                End With
                            Next
                        End If
                    End If

                    ' update data table provisi_adm
                    Dim nProvisiPerBulan As Double = Math.Round(CDbl(nProvisi.Text) / CDbl(nLama.Text), 2)
                    Dim nAdmPerBulan As Double = Math.Round(CDbl(nAdministrasi.Text) / CDbl(nLama.Text), 2)
                    Dim nTotalProvisi As Double = 0
                    Dim nTotalAdm As Double = 0
                    Dim dTanggal As Date
                    Dim vaField1() As Object
                    Dim vaValue1() As Object
                    objData.Delete(GetDSN, "provisi_adm", "rekening", , cRekening.Text)
                    If CDbl(nProvisi.Text) > 0 Or CDbl(nAdministrasi.Text) > 0 Then
                        '        cWhere = "and date_format(tglmutasi,'%Y%m') = date_format('" & SisFormat(dTgl.Value, Sis_yyyy_MM_dd) & "','%Y%m') "
                        '        objdata.Delete GetDSN, "provisi_adm", "rekening", sisAssign, cRek, cWhere
                        For n = 0 To CDbl(nLama.Text) - 1
                            If n = CDbl(nLama.Text) - 1 Then
                                nProvisiPerBulan = Math.Round(CDbl(nProvisi.Text) - nTotalProvisi, 2)
                                nAdmPerBulan = Math.Round(CDbl(nAdministrasi.Text) - nTotalAdm, 2)
                            Else
                                nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                                nTotalAdm = nTotalAdm + nAdmPerBulan
                            End If
                            dTanggal = DateAdd("m", n, dTgl.DateTime)
                            vaField1 = {"rekening", "tgl", "provisi", "adm", "tglMutasi", "cabangentry"}
                            vaValue1 = {cRekening.Text, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.DateTime, cKodeCabang}
                            '          cWhere = "rekening='" & cRek & "' and tgl='" & SisFormat(dTanggal, Sis_yyyy_MM_dd) & "'"
                            objData.Add(GetDSN, "provisi_adm", vafield1, vavalue1)
                        Next
                    End If

                    '      'update table jaminan
                    '      vaField = Array("rekening")
                    '      vaValue = Array(cRek)
                    '      cWhere = "NoPengajuan = '" & cPengajuan.Text & "'"
                    '      objdata.Edit GetDSN, "Agunan", cWhere, vaField, vaValue
                    '

                    cKeteranganTemp = String.Format("Informasi Rekening Debitur : {0}    Rekening     : {1} {0}    No. PK         : {2} {0}    Atas Nama  : {3} {0}    Alamat        : {4} ", Chr(13), cRekening.Text, cNoSPK.Text, cNama.Text, cAlamat.Text)
                    MessageBox.Show(cKeteranganTemp, "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    If MessageBox.Show("Cetak Kartu Angsuran ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                        'CetakKartuAngsuran()
                    End If
                    If MessageBox.Show("Cetak Slip Penerimaan Pinjaman ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                        'CetakSlipPenerimaan()
                    End If
                    InitValue()
                    GetEdit(False)

                Else
                    cRekening.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Function GetFrekuensi() As String
        Dim cTemp As String = ""
        Dim nFrekuensi As Integer = 1
        Dim cCabangTemp As String
        If Len(cRekening.Text) = 10 Then
            cCabangTemp = cRekening.Text.Substring(1, 3)
        Else
            cCabangTemp = CStr(aCfg(eCfg.msKodeCabang))
        End If
        Dim cDataTable = objData.Browse(GetDSN, "debitur", "max(rekening) as kodeKantor", "left(rekening,2)", data.myOperator.Assign, cCabangTemp)
        With cDataTable
            If .Rows.Count > 0 Then
                cTemp = .Rows(0).Item("kodeKantor").ToString.Substring(4)
                nFrekuensi = CInt(cTemp) + 1
            End If
        End With
        Dim cKodeCabangTemp As String = CStr(aCfg(eCfg.msKodeCabang))
        cTemp = String.Format("{0}1{1}", Padl(cKodeCabangTemp, 3, "0"), Padl(CStr(nFrekuensi), 6, "0"))
        Return cTemp
    End Function

    Private Sub GetMemory()
        Dim cField As String = "d.*,d.Kode as KodeDebitur,r.Nama, r.Alamat,w.Keterangan as NamaWilayah,"
        cField = cField & "a.Keterangan as NamaGolonganKredit,c.Keterangan as NamaGolonganDebitur,"
        cField = cField & "e.Keterangan as NamaSektorEkonomi,f.Keterangan as NamaSifatKredit,"
        cField = cField & "l.Keterangan as NamaGolonganPenjamin,"
        cField = cField & "g.Keterangan as NamaJenisPenggunaan,h.Nama as NamaAO,j.Nama as NamaNasabah,j.Alamat as AlamatNasabah,"
        cField = cField & "t.Rekening as RekeningTab,t.JumlahBlokir, t.Kode,d.Rekening as RekTemp,"
        cField = cField & "p.Jaminan as JaminanPengajuan,p.Plafond as PlafondPengajuan,b.nama as NamaBendahara,ch.keterangan as NamaCaraHitung"
        Dim vaJoin() As Object = {"Left Join Wilayah w on w.Kode = d.Wilayah",
                                  "Left Join RegisterNasabah r on r.Kode = d.Kode",
                                  "Left Join GolonganKredit a on a.Kode = d.GolonganKredit",
                                  "Left Join GolonganDebitur c on c.Kode = d.GolonganDebitur",
                                  "Left Join SektorEkonomi e on e.Kode = d.SektorEkonomi",
                                  "Left Join SifatKredit f on f.Kode = d.SifatKredit",
                                  "Left Join JenisPenggunaan g on g.Kode = d.JenisPenggunaan",
                                  "Left Join GolonganPenjamin l on l.Kode = d.GolonganPenjamin",
                                  "Left Join AO h on h.Kode = d.AO",
                                  "Left Join Tabungan t on t.Rekening = d.RekeningTabungan",
                                  "Left Join RegisterNasabah j on j.Kode = t.Kode",
                                  "Left Join PengajuanKredit p on p.Rekening = d.NoPengajuan",
                                  "Left Join Bendahara b on b.kode = d.bendahara",
                                  "Left Join CaraHitung ch on ch.kode = d.caraperhitungan"}
        Dim cDataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cKodeRegisterLama = .Item("KodeDebitur").ToString
                cRekeningTemp = .Item("RekTemp").ToString
                dTanggalRealisasi = CDate(.Item("Tgl"))
                nPlafondLama = CDbl(.Item("Plafond"))
                cCabang1.Text = .Item("kode").ToString.Substring(0, 2)
                cKode.Text = .Item("kode").ToString.Substring(4)
                cNama.Text = .Item("nama").ToString
                cAlamat.Text = .Item("alamat").ToString
                cTelepon.Text = .Item("telepon").ToString
                dTgl.DateTime = CDate(.Item("tgl").ToString)
                cPengajuan.Text = .Item("NoPengajuan").ToString
                cNamaPengajuan.Text = .Item("Nama").ToString
                nPlafondPengajuan.Text = .Item("PlafondPengajuan").ToString
                cJaminan.Text = .Item("JaminanPengajuan").ToString
                cSifatKredit.EditValue = .Item("SifatKredit").ToString
                cNamaSifatKredit.Text = .Item("NamaSifatKredit").ToString
                cJenisPenggunaan.EditValue = .Item("JenisPenggunaan").ToString
                cNamaJenisPenggunaan.Text = .Item("NamaJenisPenggunaan").ToString
                cGolonganDebitur.EditValue = .Item("GolonganDebitur").ToString
                cNamaGolonganDebitur.Text = .Item("NamaGolonganDebitur").ToString
                cSektorEkonomi.EditValue = .Item("SektorEkonomi").ToString
                cNamaSektorEkonomi.Text = .Item("NamaSektorEkonomi").ToString
                cWilayah.EditValue = .Item("wilayah").ToString
                cNamaWilayah.Text = .Item("NamaWilayah").ToString
                cGolonganKredit.EditValue = .Item("GolonganKredit").ToString
                cNamaGolonganKredit.Text = .Item("NamaGolonganKredit").ToString
                cBendahara.EditValue = .Item("Bendahara").ToString
                cNamaBendahara.Text = .Item("NamaBendahara").ToString
                cAO.EditValue = .Item("AO").ToString
                cNamaAO.Text = .Item("NamaAO").ToString
                cGolonganPenjamin.Text = .Item("GolonganPenjamin").ToString
                cNamaGolonganPenjamin.Text = .Item("NamaGolonganPenjamin").ToString
                cBagianYangDijamin.Text = .Item("BagianYangDijamin").ToString

                For n = 0 To cbKeterkaitan.Properties.Items.Count
                    If cbKeterkaitan.Properties.Items(n).ToString.Substring(1, 1) = .Item("Keterkaitan").ToString Then
                        cbKeterkaitan.EditValue = cbKeterkaitan.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbSumberDana.Properties.Items.Count - 1
                    If cbSumberDana.Properties.Items(n).ToString = .Item("SumberDana").ToString Then
                        cbSumberDana.Text = cbSumberDana.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbPeriodePembayaran.Properties.Items.Count - 1
                    If cbPeriodePembayaran.Properties.Items(n).ToString = .Item("PeriodePembayaran").ToString Then
                        cbPeriodePembayaran.Text = cbPeriodePembayaran.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbJenisUsaha.Properties.Items.Count - 1
                    If cbJenisUsaha.Properties.Items(n).ToString = .Item("JenisUsaha").ToString Then
                        cbJenisUsaha.Text = cbJenisUsaha.Properties.Items(n).ToString
                    End If
                Next

                Dim db As New DataTable
                If nPos = myPos.add Then
                    cField = "t.rekening,r.nama,r.alamat"
                    vaJoin = {"Left Join RegisterNasabah r on r.kode = t.kode"}
                    db = objData.Browse(GetDSN, "tabungan t", cField, "t.kode", , .Item("Kode").ToString, , , vaJoin)
                    If db.Rows.Count > 0 Then
                        cRekeningTabungan.EditValue = db.Rows(0).Item("Rekening").ToString
                        cNamaNasabah.Text = db.Rows(0).Item("Nama").ToString
                        cAlamatNasabah.Text = db.Rows(0).Item("Alamat").ToString
                    End If
                Else
                    If .Item("RekeningTabungan").ToString.Length > 8 Then
                        cRekeningTabungan.EditValue = .Item("RekeningTabungan").ToString
                        cNamaNasabah.Text = .Item("NamaNasabah").ToString
                        cAlamatNasabah.Text = .Item("AlamatNasabah").ToString
                    Else
                        cRekeningTabungan.EditValue = ""
                    End If
                End If
                db.Dispose()
                nPersBunga.Text = .Item("SukuBunga").ToString
                nPersBungaBerikutnya.Text = .Item("SukuBungaBerikutnya").ToString
                nBungaEfektif.Text = .Item("BungaEfektif").ToString
                nLama.Text = .Item("Lama").ToString
                nGracePokok.Text = .Item("GracePeriodePokok").ToString
                nGraceBunga.Text = .Item("GracePeriodeBunga").ToString
                cCaraPerhitungan.EditValue = .Item("CaraPerhitungan").ToString
                cNamaCaraPerhitungan.Text = .Item("NamaCaraHitung").ToString
                nPlafond.Text = .Item("Plafond").ToString
                SetOpt(optDenda, .Item("HitungDenda").ToString)
                SetOpt(optJenis, .Item("StatusRealisasi").ToString)
                SetOpt(optKaryawan, .Item("karyawan").ToString)
                SetOpt(optInstansi, .Item("Instansi").ToString)
                nAdministrasi.Text = .Item("Administrasi").ToString
                nNotaris.Text = .Item("Notaris").ToString
                nMaterai.Text = .Item("Materai").ToString
                nAsuransi.Text = .Item("Asuransi").ToString
                SetOpt(optProvisi, .Item("JenisProvisi").ToString)
                nProvisi.Text = .Item("Provisi").ToString
                nPembulatan.Text = .Item("Pembulatan").ToString
                nAngsuran.Text = .Item("AngsuranPerBulan").ToString

                SumTotal()
                IsiJadwal()
            End With
        End If
    End Sub

    Private Sub SumTotal()
        Dim nTotalTemp As Double = CDbl(nPlafond.Text) - CDbl(nAdministrasi.Text) - CDbl(nNotaris.Text) - CDbl(nMaterai.Text) - CDbl(nAsuransi.Text) - CDbl(nProvisi.Text)
        nTotal.Text = nTotalTemp.ToString
        If cCaraPerhitungan.Text = "1" Or cCaraPerhitungan.Text = "8" Or cCaraPerhitungan.Text = "5" Then
            nBunga.Text = Math.Round(CDbl(nPlafond.Text) * CDbl(nPersBunga.Text) / 100 / 12 * CDbl(nLama.Text), 0).ToString
        ElseIf nCaraPerhitungan = 2 Or nCaraPerhitungan = 9 Then
            nBunga.Text = "0"
            nAngsuran.Text = GetPembulatan(Math.Round(CDbl(nPlafond.Text) * CDbl(nPersBunga.Text) / 3600, 0), CDbl(nPembulatan.Text)).ToString
        End If
        ' kalau kredit RK, tidak perlu generate jadwal karena akan membingungkan user
        If cCaraPerhitungan.Text <> "7" Then
            GetDataJadwal()
            'Else
            'objGrid.ClearGrid itGrid4
        End If
    End Sub

    Private Sub GetDataJadwal()
        Dim nTotBunga As Double
        If nPos = myPos.add Then
            vaJadwal = GetJadwal(dTgl.DateTime, CDbl(nPlafond.Text), CInt(nLama.Text), CSng(nPersBunga.Text), cCaraPerhitungan.Text, CDbl(nGracePokok.Text), CDbl(nGraceBunga.Text), CDbl(nPembulatan.Text), , CSng(nPersBungaBerikutnya.Text), nTotBunga)
            GridControl3.DataSource = vaJadwal
            InitGrid(GridView3)
            GridColumnFormat(GridView3, "Ke")
            GridColumnFormat(GridView3, "JthTmp", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, , DevExpress.Utils.HorzAlignment.Center)
            GridColumnFormat(GridView3, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView3, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView3, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView3, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
            GridColumnFormat(GridView3, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)

            nPokokPenyesuaian.Text = vaJadwal.Rows(1).Item(3).ToString
            nBungaPenyesuaian.Text = vaJadwal.Rows(1).Item(2).ToString
            nPokokPenyesuaianTemp = CDbl(nPokokPenyesuaian.Text)
            nBungaPenyesuaianTemp = CDbl(nBungaPenyesuaian.Text)
            If cCaraPerhitungan.Text = "4" Then
                nBunga.Text = nTotBunga.ToString
            End If
        End If
    End Sub

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cbKeterkaitan.Properties
        With properties
            .Items.AddRange(New String() {"1 - Terkait", "2 - Tidak Terkait"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbKeterkaitan.SelectedIndex = 1

        properties = cbSumberDana.Properties
        With properties
            .Items.AddRange(New String() {"10 - Gaji/Honor", "21 - Usaha Subdisi", "22 - Usaha Non Subsidi", "31 - Lainnya Subsidi", "32 - Lainnya Non Subsidi"})
        End With
        cbSumberDana.SelectedIndex = 1

        properties = cbPeriodePembayaran.Properties
        With properties
            .Items.AddRange(New String() {"1 - Harian", "2 - Mingguan", "3 - Bulanan", "4 - Triwulanan", "5 - Semesteran",
                                          "6 - Tahunan", "7 - Sekaligus", "8 - Setiap saat"})
        End With
        cbPeriodePembayaran.SelectedIndex = 1

        properties = cbJenisUsaha.Properties
        With properties
            .Items.AddRange(New String() {"1 - Mikro", "2 - Kecil", "3 - Menengah", "4 - Selain Usaha Mikro, Kecil dan Menengah"})
        End With
        cbJenisUsaha.SelectedIndex = 1

    End Sub

    Private Sub trRealisasiKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Dim cField As String = "kode,lama,minimum,maximum,nominal,persen,id"
        Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and ({1} between minimum and maximum or (lama <= {2} and lama <> 0)) ", cWhere, CDbl(nPlafond.Text), CDbl(nLama.Text))
        GetDataLookup("detailadm", cAdministrasi, cWhere, cField)
        cField = "kode,lama,persen,id"
        cWhere = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and ({1} or (lama <= {2} and lama <> 0)) ", cWhere, CDbl(nPlafond.Text), CDbl(nLama.Text))
        GetDataLookup("detailprovisi", cAdministrasi, cWhere, cField)
        GetDataLookup("PengajuanKredit", cPengajuan, , "rekening,nominal")
        GetDataLookup("GolonganKredit", cGolonganKredit)
        GetDataLookup("SifatKredit", cSifatKredit)
        GetDataLookup("JenisPenggunaan", cJenisPenggunaan)
        GetDataLookup("GolonganDebitur", cGolonganDebitur)
        GetDataLookup("SektorEkonomi", cSektorEkonomi)
        GetDataLookup("Wilayah", cWilayah)
        GetDataLookup("Bendahara", cBendahara, "kode,nama")
        GetDataLookup("AO", cAO, , "kode,nama")
        GetDataLookup("GolonganPenjamin", cGolonganPenjamin)
        GetDataLookup("CaraPerhitungan", cCaraPerhitungan)
    End Sub

    Private Sub trRealisasiKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        IsiCombo()
        ArrayNilaiDefault()

        SetTabIndex(cCabang1.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cPengajuan.TabIndex, n)
        SetTabIndex(cNoSPK.TabIndex, n)
        SetTabIndex(cGolonganKredit.TabIndex, n)
        SetTabIndex(cSifatKredit.TabIndex, n)
        SetTabIndex(cJenisPenggunaan.TabIndex, n)
        SetTabIndex(cGolonganDebitur.TabIndex, n)
        SetTabIndex(cSektorEkonomi.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cBendahara.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(cGolonganPenjamin.TabIndex, n)
        SetTabIndex(optAsuransi.TabIndex, n)
        SetTabIndex(cBagianYangDijamin.TabIndex, n)
        SetTabIndex(cbKeterkaitan.TabIndex, n)
        SetTabIndex(cbSumberDana.TabIndex, n)
        SetTabIndex(cbPeriodePembayaran.TabIndex, n)
        SetTabIndex(cbJenisUsaha.TabIndex, n)
        SetTabIndex(optAutoDebet.TabIndex, n)
        SetTabIndex(cRekeningTabungan.TabIndex, n)

        SetTabIndex(nPersBunga.TabIndex, n)
        SetTabIndex(nPersBungaBerikutnya.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nGracePokok.TabIndex, n)
        SetTabIndex(nGraceBunga.TabIndex, n)
        SetTabIndex(cCaraPerhitungan.TabIndex, n)
        SetTabIndex(nPlafond.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(optDenda.TabIndex, n)
        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(optKaryawan.TabIndex, n)
        SetTabIndex(optInstansi.TabIndex, n)
        SetTabIndex(cAdministrasi.TabIndex, n)
        SetTabIndex(nPersenAdm.TabIndex, n)
        SetTabIndex(nAdministrasi.TabIndex, n)
        SetTabIndex(nNotaris.TabIndex, n)
        SetTabIndex(nMaterai.TabIndex, n)
        SetTabIndex(nAsuransi.TabIndex, n)
        SetTabIndex(optProvisi.TabIndex, n)
        SetTabIndex(cProvisi.TabIndex, n)
        SetTabIndex(nPersenProvisi.TabIndex, n)
        SetTabIndex(nProvisi.TabIndex, n)
        SetTabIndex(nPembulatan.TabIndex, n)
        SetTabIndex(nAngsuran.TabIndex, n)

        SetTabIndex(nBungaPenyesuaian.TabIndex, n)
        SetTabIndex(nPokokPenyesuaian.TabIndex, n)
        SetTabIndex(cmdOK.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cCabang1.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cPengajuan.EnterMoveNextControl = True
        cNoSPK.EnterMoveNextControl = True
        cGolonganKredit.EnterMoveNextControl = True
        cSifatKredit.EnterMoveNextControl = True
        cJenisPenggunaan.EnterMoveNextControl = True
        cGolonganDebitur.EnterMoveNextControl = True
        cSektorEkonomi.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cBendahara.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        cGolonganPenjamin.EnterMoveNextControl = True
        optAsuransi.EnterMoveNextControl = True
        cBagianYangDijamin.EnterMoveNextControl = True
        cbKeterkaitan.EnterMoveNextControl = True
        cbSumberDana.EnterMoveNextControl = True
        cbPeriodePembayaran.EnterMoveNextControl = True
        cbJenisUsaha.EnterMoveNextControl = True
        optAutoDebet.EnterMoveNextControl = True
        cRekeningTabungan.EnterMoveNextControl = True

        nPersBunga.EnterMoveNextControl = True
        nPersBungaBerikutnya.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nGracePokok.EnterMoveNextControl = True
        nGraceBunga.EnterMoveNextControl = True
        cCaraPerhitungan.EnterMoveNextControl = True
        nPlafond.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        optDenda.EnterMoveNextControl = True
        optJenis.EnterMoveNextControl = True
        optKaryawan.EnterMoveNextControl = True
        optInstansi.EnterMoveNextControl = True
        cAdministrasi.EnterMoveNextControl = True
        nPersenAdm.EnterMoveNextControl = True
        nAdministrasi.EnterMoveNextControl = True
        nNotaris.EnterMoveNextControl = True
        nMaterai.EnterMoveNextControl = True
        nAsuransi.EnterMoveNextControl = True
        optProvisi.EnterMoveNextControl = True
        cProvisi.EnterMoveNextControl = True
        nPersenProvisi.EnterMoveNextControl = True
        nProvisi.EnterMoveNextControl = True
        nPembulatan.EnterMoveNextControl = True
        nAngsuran.EnterMoveNextControl = True

        nBungaPenyesuaian.EnterMoveNextControl = True
        nPokokPenyesuaian.EnterMoveNextControl = True

        FormatTextBox(nPlafondPengajuan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNomor, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiJaminan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiNJOP, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiPengikatan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPersBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPersBungaBerikutnya, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nBungaEfektif, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nGracePokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nGraceBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPersenAdm, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNotaris, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nMaterai, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAsuransi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPersenProvisi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nProvisi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPembulatan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAngsuran, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBungaPenyesuaian, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPokokPenyesuaian, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cKode.Focus()
        Koreksi(True)
        cCabang1.Enabled = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
            cKodeRegister = setKode(cCabang1.Text, cKode.Text)
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "nama,alamat,telepon", "kode", data.myOperator.Assign, cKodeRegister)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cTelepon.Text = .Item("Telepon").ToString
                End With
            Else
                MessageBox.Show("No. Register tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                cAlamat.Text = ""
                cTelepon.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cRekening_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cRekening.GotFocus
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cJenisPenggunaan.Closed
        GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
    End Sub

    Private Sub cJenisPenggunaan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cJenisPenggunaan.Closed
        GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cJenisPenggunaan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
        End If
    End Sub

    Private Sub cJenisPenggunaan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cJenisPenggunaan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
            If nPos = myPos.add Then
                cRekening.Text = GetFrekuensi()
            End If
        End If
    End Sub

    Private Sub cAO_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cAO.Closed
        GetKeteranganLookUp(cAO, cNamaAO, , "nama")
    End Sub

    Private Sub cAO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cAO.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cAO, cNamaAO, , "nama")
        End If
    End Sub

    Private Sub cWilayah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cWilayah.Closed
        GetKeteranganLookUp(cWilayah, cNamaWilayah)
    End Sub

    Private Sub cWilayah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cWilayah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cWilayah, cNamaWilayah)
        End If
    End Sub

    Private Sub cRekening_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim dbData As New DataTable
            dbData = objData.Browse(GetDSN, "Debitur", "statusPencairan", "Rekening", , cRekening.Text)
            If dbData.Rows.Count > 0 Then
                cCaraPencairan = dbData.Rows(0).Item("CaraPencairan").ToString
                If nPos <> myPos.add Then
                    nStatusKredit = JenisStatusKredit.Belum_Cair
                    GetMemory()
                Else
                    cNoSPK.Text = cRekening.Text
                End If
                EditControl(True)
                If cCaraPencairan="1" Then
                    EditControl(False)
                Else
                    If nPos = myPos.delete Then
                        DeleteData()
                        If lNoDelete Then
                            cRekening.Focus()
                        End If
                    End If
                End If
                dbData.Dispose()
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Data Tidak Ada. Silahkan mengulang pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cRekening.EditValue = ""
                cRekening.Focus()
                dbData.Dispose()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub EditControl(ByVal lValue As Boolean)
        nPlafond.Enabled = lValue
        nPersBunga.Enabled = lValue
        nPersBungaBerikutnya.Enabled = lValue
        nLama.Enabled = lValue
        nGracePokok.Enabled = lValue
        nGraceBunga.Enabled = lValue
        nBunga.Enabled = lValue
    End Sub

    Private Sub cRekeningTabungan_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningTabungan.Closed
        GetDataTabungan()
    End Sub

    Private Sub cRekeningTabungan_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningTabungan.KeyDown
        If e.KeyCode = Keys.Enter Then
            getdataTabungan()
        End If
    End Sub

    Private Sub GetDataTabungan()
        Dim dbData As New DataTable
        Const cField As String = "t.Rekening,r.Nama,r.Alamat,g.Keterangan as NamaGolongan,t.close"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on t.Kode = r.Kode", _
                                  "Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan"}
        dbData = objData.Browse(GetDSN, "Tabungan t", cField, "t.Rekening", , cRekeningTabungan.Text, , , vaJoin)
        If dbData.Rows.Count > 0 Then
            With dbData.Rows(0)
                cNamaNasabah.Text = .Item("Nama").ToString
                cAlamatNasabah.Text = .Item("Alamat").ToString
                If GetNull(.Item("Close").ToString).ToString = "1" Then
                    MessageBox.Show("Rekening tabungan sudah ditutup,transaksi tidak bisa dilanjutkan...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cRekeningTabungan.EditValue = ""
                    cRekeningTabungan.Focus()
                    Exit Sub
                End If
            End With
        Else
            MessageBox.Show("Rekening Tabungan, Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cRekeningTabungan.EditValue = ""
            cRekeningTabungan.Focus()
        End If
        dbData.Dispose()
    End Sub

    Private Sub cAdministrasi_Closed(sender As Object, e As ClosedEventArgs) Handles cAdministrasi.Closed
        getketeranganAdm()
    End Sub

    Private Sub GetKeteranganAdm()
        If cAdministrasi.Text <> "" Then
            Dim r As Integer = cAdministrasi.Properties.GetDataSourceRowIndex(cAdministrasi.Properties.Columns("kode"), cAdministrasi.Text)
            If r >= 0 Then
                Const cField As String = "kode,lama,minimum,maximum,nominal,persen,id"
                Dim cValue As String = cAdministrasi.Properties.GetDataSourceValue("id", r).ToString
                Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0} and ({1} between minimum and maximum or (lama <= {2} and lama <> 0)) ", cWhere, CDbl(nPlafond.Text), CDbl(nLama.Text))
                cWhere = String.Format("{0} and id={1}", cWhere, CDbl(cValue))
                Dim db As New DataTable
                db = objData.Browse(GetDSN, "detailadm", cField, "kode", data.myOperator.Content, cAdministrasi.Text, cWhere)
                If db.Rows.Count > 0 Then
                    If CDbl(db.Rows(0).Item("Nominal")) = 0 Then
                        nPersenAdm.Text = db.Rows(0).Item("Persen").ToString
                        HitungAdm()
                    Else
                        nAdministrasi.Text = db.Rows(0).Item("Nominal").ToString
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cProvisi_Closed(sender As Object, e As ClosedEventArgs) Handles cProvisi.Closed
        getketeranganprovisi()
    End Sub

    Private Sub GetKeteranganProvisi()
        If cProvisi.Text <> "" Then
            Dim r As Integer = cProvisi.Properties.GetDataSourceRowIndex(cProvisi.Properties.Columns("kode"), cProvisi.Text)
            If r >= 0 Then
                Const cField As String = "kode,lama,persen,id"
                Dim cValue As String = cProvisi.Properties.GetDataSourceValue("id", r).ToString
                Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0} and ({1} or (lama <= {2} and lama <> 0)) ", cWhere, CDbl(nPlafond.Text), CDbl(nLama.Text))
                cWhere = String.Format("{0} and id={1}", cWhere, CDbl(cValue))
                Dim db As New DataTable
                db = objData.Browse(GetDSN, "detailprovisi", cField, "kode", data.myOperator.Content, cProvisi.Text, cWhere)
                If db.Rows.Count > 0 Then
                    nPersenProvisi.Text = db.Rows(0).Item("Persen").ToString
                    HitungProvisi()
                End If
            End If
        End If
    End Sub

    Private Sub cAdministrasi_KeyDown(sender As Object, e As KeyEventArgs) Handles cAdministrasi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganAdm()
        End If
    End Sub

    Private Sub cProvisi_KeyDown(sender As Object, e As KeyEventArgs) Handles cProvisi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganProvisi()
        End If
    End Sub

    Private Sub cSifatKredit_Closed(sender As Object, e As ClosedEventArgs) Handles cSifatKredit.Closed
        GetKeteranganLookUp(cSifatKredit, cNamaSifatKredit)
    End Sub

    Private Sub cSifatKredit_KeyDown(sender As Object, e As KeyEventArgs) Handles cSifatKredit.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cSifatKredit, cNamaSifatKredit)
        End If
    End Sub

    Private Sub cGolonganDebitur_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganDebitur.Closed
        GetKeteranganLookUp(cGolonganDebitur, cNamaGolonganDebitur)
    End Sub

    Private Sub cGolonganDebitur_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganDebitur.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganDebitur, cNamaGolonganDebitur)
        End If
    End Sub

    Private Sub cSektorEkonomi_Closed(sender As Object, e As ClosedEventArgs) Handles cSektorEkonomi.Closed
        GetKeteranganLookUp(cSektorEkonomi, cNamaSektorEkonomi)
    End Sub

    Private Sub cSektorEkonomi_KeyDown(sender As Object, e As KeyEventArgs) Handles cSektorEkonomi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cSektorEkonomi, cNamaSektorEkonomi)
        End If
    End Sub

    Private Sub cBendahara_Closed(sender As Object, e As ClosedEventArgs) Handles cBendahara.Closed
        GetKeteranganLookUp(cBendahara, cNamaBendahara)
    End Sub

    Private Sub cBendahara_KeyDown(sender As Object, e As KeyEventArgs) Handles cBendahara.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cBendahara, cNamaBendahara)
        End If
    End Sub

    Private Sub cGolonganPenjamin_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganPenjamin.Closed
        GetKeteranganLookUp(cGolonganPenjamin, cNamaGolonganPenjamin)
    End Sub

    Private Sub cGolonganPenjamin_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganPenjamin.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganPenjamin, cNamaGolonganPenjamin)
        End If
    End Sub

    Private Sub cCaraPerhitungan_Closed(sender As Object, e As ClosedEventArgs) Handles cCaraPerhitungan.Closed
        GetKeteranganLookUp(cCaraPerhitungan, cNamaCaraPerhitungan)
    End Sub

    Private Sub cCaraPerhitungan_KeyDown(sender As Object, e As KeyEventArgs) Handles cCaraPerhitungan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cCaraPerhitungan, cNamaCaraPerhitungan)
        End If
    End Sub

    Private Sub cPengajuan_Closed(sender As Object, e As ClosedEventArgs) Handles cPengajuan.Closed
        GetDataPengajuan()
    End Sub

    Private Sub cPengajuan_KeyDown(sender As Object, e As KeyEventArgs) Handles cPengajuan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetDataPengajuan()
        End If
    End Sub

    Private Sub GetDataPengajuan()
        Dim dbData As New DataTable
        Dim cKodeTemp As String = String.Format("{0}.{1}", cCabang1.Text, cKode.Text)
        Const cField As String = "p.Rekening,r.Nama,p.Plafond,p.Lama,p.Jaminan"
        Dim cWhere As String = String.Format(" and Rekening Like '{0}%' and statuspengajuan='0'", cPengajuan.Text)
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on p.Kode = r.Kode"}
        dbData = objData.Browse(GetDSN, "pengajuankredit p", cField, "p.kode", , cKodeTemp, cWhere, , vaJoin)
        If dbData.Rows.Count > 0 Then
            With dbData.Rows(0)
                cNamaPengajuan.Text = .Item("Nama").ToString
                nPlafondPengajuan.Text = .Item("Plafond").ToString
                SeekPengajuan()
            End With
        Else
            MessageBox.Show("Nomor Pengajuan Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cPengajuan.EditValue = ""
            cPengajuan.Focus()
        End If
        dbData.Dispose()
    End Sub

    Private Sub SeekPengajuan()
        Dim dbData As New DataTable
        Dim cField As String = "p.*,"
        cField = cField & "r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,"
        cField = cField & "s.Keterangan as NamaSifatKredit,"
        cField = cField & "j.Keterangan as NamaJenisPenggunaan,"
        cField = cField & "gd.Keterangan as NamaGolonganDebitur,"
        cField = cField & "se.Keterangan as NamaSektorEkonomi,"
        cField = cField & "w.Keterangan as NamaWilayah,"
        cField = cField & "a.Nama as NamaAO,"
        cField = cField & "gp.Keterangan as NamaGolonganPenjamin"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on r.Kode = p.Kode",
                                  "Left Join SifatKredit s on p.SifatKredit = s.Kode",
                                  "Left Join JenisPenggunaan j on p.JenisPenggunaan = j.Kode",
                                  "Left Join GolonganDebitur gd on p.GolonganDebitur = gd.Kode",
                                  "Left Join SektorEkonomi se on p.SektorEkonomi = se.Kode",
                                  "Left Join Wilayah w on p.Wilayah = w.Kode",
                                  "Left Join AO a on p.AO = a.Kode",
                                  "Left Join GolonganPenjamin gp on p.GolonganPenjamin = gp.Kode"}
        dbData = objData.Browse(GetDSN, "PengajuanKredit p", cField, "p.Rekening", , cPengajuan.Text, , , vaJoin)
        If dbData.rows.count > 0 Then
            If nPos = myPos.add Then
                With dbData.Rows(0)
                    cSifatKredit.Text = .Item("SifatKredit").ToString
                    cNamaSifatKredit.Text = .Item("NamaSifatKredit").ToString
                    cJenisPenggunaan.Text = .Item("JenisPenggunaan").ToString
                    cNamaJenisPenggunaan.Text = .Item("NamaJenisPenggunaan").ToString
                    cGolonganDebitur.Text = .Item("GolonganDebitur").ToString
                    cNamaGolonganDebitur.Text = .Item("NamaGolonganDebitur").ToString
                    cSektorEkonomi.Text = .Item("SektorEkonomi").ToString
                    cNamaSektorEkonomi.Text = .Item("NamaSektorEkonomi").ToString
                    cWilayah.Text = .Item("Wilayah").ToString
                    cNamaWilayah.Text = .Item("NamaWilayah").ToString
                    cAO.Text = .Item("AO").ToString
                    cNamaAO.Text = .Item("NamaAO").ToString
                    cGolonganPenjamin.Text = .Item("GolonganPenjamin").ToString
                    cNamaGolonganPenjamin.Text = .Item("NamaGolonganPenjamin").ToString
                    cBagianYangDijamin.Text = .Item("BagianYangDijamin").ToString
                End With
            End If
            'SeekDetailJaminan False
        End If
        dbData.Dispose()
    End Sub

    Private Sub ArrayNilaiDefault()
        Dim dbData As New DataTable
        AddColumn(vaWilayah, "Kode", System.Type.GetType("System.String"))
        AddColumn(vaWilayah, "Keterangan", System.Type.GetType("System.String"))

        dbData = objData.Browse(GetDSN, "SifatKredit", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaSifatKredit = dbData.Copy
        End If
        dbData = objData.Browse(GetDSN, "JenisPenggunaan", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaJenisPenggunaan = dbData.Copy
        End If
        dbData = objData.Browse(GetDSN, "GolonganDebitur", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaGolonganDebitur = dbData.Copy
        End If
        dbData = objData.Browse(GetDSN, "SektorEkonomi", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaSektorEkonomi = dbData.Copy
        End If
        dbData = objData.Browse(GetDSN, "wilayah", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaWilayah = dbData.Copy
        End If
        dbData = objData.Browse(GetDSN, "GolonganPenjamin", "kode,keterangan")
        If dbData.Rows.Count > 0 Then
            vaGolonganPenjamin = dbData.Copy
        End If
    End Sub

    Private Sub InitNilaiDefault()
        cSifatKredit.Text = ""
        cNamaSifatKredit.Text = ""
        cJenisPenggunaan.Text = ""
        cNamaJenisPenggunaan.Text = ""
        cGolonganDebitur.Text = ""
        cNamaGolonganDebitur.Text = ""
        cSektorEkonomi.Text = ""
        cNamaSektorEkonomi.Text = ""
        cWilayah.Text = ""
        cNamaWilayah.Text = ""
        cGolonganPenjamin.Text = ""
        cNamaGolonganPenjamin.Text = ""
        cBagianYangDijamin.Text = ""
        cbKeterkaitan.EditValue = ""
        cbSumberDana.EditValue = ""
        cbPeriodePembayaran.EditValue = ""
        cbJenisUsaha.EditValue = ""
    End Sub

    Private Sub NilaiDefault()
        Dim n As Integer
        Dim cTemp As String
        Dim nRow As DataRow()

        IsiCombo()
        cWhereSifatKredit = ""
        cWhereSektorEkonomi = ""
        cWhereGolonganPenjamin = ""
        InitNilaiDefault()
        Select Case cGolonganKredit.Text
            Case Is = "71"
                cWilayah.Text = "1204"
                nRow = vaWilayah.Select(String.Format("kode='{0}'", cWilayah.Text))
                If nRow.Count > 0 Then
                    cNamaWilayah.Text = nRow(1).ToString
                End If
                cbKeterkaitan.Text = cbKeterkaitan.Properties.Items(1).ToString
            Case Is = "72"
                cWhereSifatKredit = "and kode = '3' or kode='6'"
                cJenisPenggunaan.Text = "39"
                nRow = vaJenisPenggunaan.Select(String.Format("kode='{0}'", cJenisPenggunaan.Text))
                If nRow.Count > 0 Then
                    cNamaJenisPenggunaan.Text = nRow(1).ToString
                End If
                cGolonganDebitur.Text = "874"
                nRow = vaGolonganDebitur.Select(String.Format("kode='{0}'", cGolonganDebitur.Text))
                If nRow.Count > 0 Then
                    cNamaGolonganDebitur.Text = nRow(1).ToString
                End If
                cSektorEkonomi.Text = "1020"
                nRow = vaSektorEkonomi.Select(String.Format("kode='{0}'", cSektorEkonomi.Text))
                If nRow.Count > 0 Then
                    cNamaSektorEkonomi.Text = nRow(1).ToString
                End If
                cWilayah.Text = "1204"
                nRow = vaWilayah.Select(String.Format("kode='{0}'", cWilayah.Text))
                If nRow.Count > 0 Then
                    cNamaWilayah.Text = nRow(1).ToString
                End If
                cWhereGolonganPenjamin = "and kode = '880' or kode = '890'"
                cbKeterkaitan.Text = cbKeterkaitan.Properties.Items(1).ToString
                cbSumberDana.Text = cbSumberDana.Properties.Items(0).ToString
                cbPeriodePembayaran.Text = cbPeriodePembayaran.Properties.Items(2).ToString
                cbJenisUsaha.Text = cbJenisUsaha.Properties.Items(3).ToString
            Case Is = "73"
                cWhereSifatKredit = "and kode = '3' or kode='6'"
                cJenisPenggunaan.Text = "10"
                nRow = vaJenisPenggunaan.Select(String.Format("kode='{0}'", cJenisPenggunaan.Text))
                If nRow.Count > 0 Then
                    cNamaJenisPenggunaan.Text = nRow(1).ToString
                End If
                cGolonganDebitur.Text = "875"
                nRow = vaGolonganDebitur.Select(String.Format("kode='{0}'", cGolonganDebitur.Text))
                If nRow.Count > 0 Then
                    cNamaGolonganDebitur.Text = nRow(1).ToString
                End If
                '      cWhereSektorEkonomi = "and kode <> '1020'"
                cWilayah.Text = "1204"
                nRow = vaWilayah.Select(String.Format("kode='{0}'", cWilayah.Text))
                If nRow.Count > 0 Then
                    cNamaWilayah.Text = nRow(1).ToString
                End If
                cWhereGolonganPenjamin = "and kode = '880' or kode = '890'"
                cbKeterkaitan.Text = cbKeterkaitan.Properties.Items(1).ToString
                cbSumberDana.Text = cbSumberDana.Properties.Items(2).ToString
                For n = 0 To cbPeriodePembayaran.Properties.Items.Count
                    cTemp = cbPeriodePembayaran.Properties.Items(n).ToString.Substring(0, 1)
                    If cTemp <> "" Then
                        If cTemp <> "3" And cTemp <> "5" And cTemp <> "6" And cTemp <> "7" Then
                            cbPeriodePembayaran.Properties.Items.Remove(n)
                        End If
                    End If
                Next
                For n = 0 To cbJenisUsaha.Properties.Items.Count
                    cTemp = cbJenisUsaha.Properties.Items(n).ToString.Substring(0, 1)
                    If cTemp <> "" Then
                        If cTemp <> "1" Or cTemp <> "2" Then
                            cbJenisUsaha.Properties.Items.Remove(n)
                        End If
                    End If
                Next
            Case Is = "74"
                cWilayah.Text = "1204"
                nRow = vaWilayah.Select(String.Format("kode='{0}'", cWilayah.Text))
                If nRow.Count > 0 Then
                    cNamaWilayah.Text = nRow(1).ToString
                End If
                cbKeterkaitan.Text = cbKeterkaitan.Properties.Items(1).ToString
            Case Is = "75"
                cWhereSifatKredit = "and kode = '3' or kode='6'"
                cJenisPenggunaan.Text = "35"
                nRow = vaJenisPenggunaan.Select(String.Format("kode='{0}'", cJenisPenggunaan.Text))
                If nRow.Count > 0 Then
                    cNamaJenisPenggunaan.Text = nRow(1).ToString
                End If
                cGolonganDebitur.Text = "875"
                nRow = vaGolonganDebitur.Select(String.Format("kode='{0}'", cGolonganDebitur.Text))
                If nRow.Count > 0 Then
                    cNamaGolonganDebitur.Text = nRow(1).ToString
                End If
                '      cWhereSektorEkonomi = "and kode <> '1020'"
                cGolonganPenjamin.Text = "000"
                nRow = vaGolonganPenjamin.Select(String.Format("kode='{0}'", cGolonganPenjamin.Text))
                If nRow.Count > 0 Then
                    cNamaGolonganPenjamin.Text = nRow(1).ToString
                End If
                cWilayah.Text = "1204"
                nRow = vaWilayah.Select(String.Format("kode='{0}'", cWilayah.Text))
                If nRow.Count > 0 Then
                    cNamaWilayah.Text = nRow(1).ToString
                End If
                cBagianYangDijamin.Text = "0"
                cbKeterkaitan.Text = cbKeterkaitan.Properties.Items(1).ToString
                cbSumberDana.Text = cbSumberDana.Properties.Items(0).ToString
                cbPeriodePembayaran.Text = cbPeriodePembayaran.Properties.Items(3).ToString
                cbJenisUsaha.Text = cbJenisUsaha.Properties.Items(3).ToString
            Case Else
                Exit Select
        End Select
    End Sub

    Private Sub cGolonganKredit_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganKredit.Closed
        GetKeteranganLookUp(cGolonganKredit, cNamaGolonganKredit)
    End Sub

    Private Sub cGolonganKredit_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganKredit.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganKredit, cNamaGolonganKredit)
        End If
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        If CDbl(nPokokPenyesuaian.Text) <> nPokokPenyesuaianTemp And CDbl(nBungaPenyesuaian.Text) <> nBungaPenyesuaianTemp Then
            IsiJadwal()
        End If
    End Sub
End Class