﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trSetorDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        cGolonganDeposito.EditValue = ""
        cKeteranganGolonganDeposito.Text = ""
        cGolonganDeposan.EditValue = ""
        cKeteranganGolonganDeposan.Text = ""
        nLama.Text = ""
        dJthTmp.DateTime = Date.Today
        cRekeningTabungan.Text = ""
        optDeposan.SelectedIndex = 1
        nNominal.Text = ""
        optKas.SelectedIndex = 1
    End Sub

    Private Sub trSetorDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trSetorDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nNominal.TabIndex, n)
        SetTabIndex(optKas.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nNominal.EnterMoveNextControl = True
        optKas.EnterMoveNextControl = True

        FormatTextBox(nLama, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(cRekeningTabungan, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nNominal, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "nominal,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("nominal")) <> 0 Then
                        MessageBox.Show("Pembukaan Deposito Telah Dilakukan, Data Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                    If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                    GetMemory()
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , False).ToString
        Dim cField As String = "t.Rekening,t.Kode,t.GolonganDeposan,t.GolonganDeposito,t.Tgl,t.RekeningTabungan,"
        cField = cField & "t.JenisDeposan,r.Nama as NamaDeposan,r.Alamat as AlamatDeposan,g.Keterangan as NamaGolonganDeposan,"
        cField = cField & "o.Keterangan as NamaGolonganDeposito,t.Lama,rt.Nama as NamaNasabah"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on t.Kode = r.Kode",
                                  "Left Join GolonganDeposan g on g.Kode = t.GolonganDeposan",
                                  "Left Join GolonganDeposito o on t.GolonganDeposito = o.Kode",
                                  "Left Join Tabungan tb on t.RekeningTabungan = tb.Rekening",
                                  "Left Join RegisterNasabah rt on tb.Kode = rt.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "deposito t", cField, "t.rekening", data.myOperator.Assign, cRekening.Text, _
                                        , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("NamaDeposan").ToString
                cAlamat.Text = .Item("AlamatDeposan").ToString
                cGolonganDeposan.Text = .Item("GolonganDeposan").ToString
                cKeteranganGolonganDeposan.Text = .Item("NamaGolonganDeposan").ToString
                cGolonganDeposito.Text = .Item("GolonganDeposito").ToString
                cKeteranganGolonganDeposito.Text = .Item("NamaGolonganDeposito").ToString
                nLama.Text = .Item("Lama").ToString
                dJthTmp.DateTime = DateAdd("m", Val(.Item("Lama")), CDate(.Item("Tgl")))
                cRekeningTabungan.Text = GetNull(.Item("RekeningTabungan").ToString, "").ToString
                nSaldoTabungan.Text = GetSaldoTabungan(.Item("RekeningTabungan").ToString, dTgl.DateTime).ToString
                SetOpt(optDeposan, .Item("JenisDeposan").ToString)
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(nNominal.Text, "Nominal Tidak Boleh Kosong,Ulangi Pengisian...") Then
            ValidSaving = False
            nNominal.Focus()
            Exit Function
        End If

        If Len(cRekening) < 12 Then
            MsgBox("Nomor Rekening Tidak Lengkap. Transaksi Tidak Bisa Dilanjutkan.", vbExclamation)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If optKas.SelectedIndex = 2 Then
            If cRekeningTabungan.Text = "" Then
                MessageBox.Show("Rekening Tabungan Tidak Ada." & vbCrLf & "Anda Tidak Dapat Menyimpan Data Ini", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
            If Val(nNominal.Text) > Val(nSaldoTabungan.Text) Then
                MessageBox.Show("Saldo Tabungan Tidak Mencukupi.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                nNominal.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim cKodeCabang As String

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , True)
            If ValidSaving() Then
                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                Dim cKas As String = GetOpt(optKas).ToString
                UpdSetorDeposito(cRekening.Text, Val(nNominal.Text), cKodeCabang, cFaktur.Text, dTgl.DateTime, dJthTmp.DateTime, cKas,True)
                If MessageBox.Show("Cetak Validasi ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                    'CetakValidasiPokokDepositoDetail cFaktur.Text
                End If
                MsgBox("Data sudah disimpan.", vbInformation)
                InitValue()
            End If
        End If
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub
End Class