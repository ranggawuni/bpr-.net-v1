﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraBars
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class trCariRegister
    Dim dbData As New DataTable
    Dim dbSearch As New DataTable
    Dim objData As New data
    Dim vaMutasi As New DataTable
    Dim vaArray As New DataTable
    Dim lClickCancel As Boolean
    Dim nNomorTemp As Integer
    Dim cKodeTemp As String
    Dim cRekeningKredit As String

    Dim vaTabungan As New DataTable
    Dim vaDeposito As New DataTable
    Dim vaKredit As New DataTable
    Dim vaPengajuan As New DataTable
    Dim vaSearch As New DataTable
    Dim vaRekening As New DataTable
    Dim vaKantor As New DataTable
    Dim vaRekTabungan As New DataTable
    Dim vaRekDeposito As New DataTable
    Dim vaRekKredit As New DataTable
    Dim vaReport As New DataTable
    Dim vaTabunganTemp As New DataTable
    Dim vaDepositoTemp As New DataTable
    Dim vaKreditTemp As New DataTable

    Private Sub cbJenis_LostFocus(sender As Object, e As EventArgs) Handles cbJenis.LostFocus
        Dim n As Integer
        Dim cOld As String = cbJenis.Text
        cbJenis.Text = cbJenis.Properties.Items(0).ToString
        For n = 0 To cbJenis.Properties.Items.Count - 1
            If UCase(cbJenis.Properties.Items(n).ToString.Substring(0, cOld.Length)) = UCase(cOld) Then
                cbJenis.Text = cbJenis.Properties.Items(n).ToString
            End If
        Next
    End Sub

    Private Sub cbKantor_LostFocus(sender As Object, e As EventArgs) Handles cbKantor.LostFocus
        Dim n As Integer
        Dim cOld As String = cbKantor.Text
        cbKantor.Text = cbKantor.Properties.Items(0).ToString
        For n = 0 To cbKantor.Properties.Items.Count - 1
            If UCase(cbKantor.Properties.Items(n).ToString.Substring(0, cOld.Length)) = UCase(cOld) Then
                cbKantor.Text = cbKantor.Properties.Items(n).ToString
            End If
        Next
    End Sub

    Private Sub InitTableTabungan()
        vaTabunganTemp.Reset()
        AddColumn(vaTabunganTemp, "Kode", System.Type.GetType("System.String"))
        AddColumn(vaTabunganTemp, "Rekening", System.Type.GetType("System.String"))
        AddColumn(vaTabunganTemp, "Jumlah", System.Type.GetType("System.String"))
        AddColumn(vaTabunganTemp, "StatusPenutupan", System.Type.GetType("System.String"))
        AddColumn(vaTabunganTemp, "TglPenutupan", System.Type.GetType("System.DateTime"))
    End Sub

    Private Sub InitTableDeposito()
        vaDepositoTemp.Reset()
        AddColumn(vaDepositoTemp, "Kode", System.Type.GetType("System.String"))
        AddColumn(vaDepositoTemp, "Rekening", System.Type.GetType("System.String"))
        AddColumn(vaDepositoTemp, "Jumlah", System.Type.GetType("System.String"))
        AddColumn(vaDepositoTemp, "TglCair", System.Type.GetType("System.DateTime"))
    End Sub

    Private Sub initTableReport()
        vaReport.Reset()
        AddColumn(vaReport, "Tabungan", System.Type.GetType("System.String"))
        AddColumn(vaReport, "Deposito", System.Type.GetType("System.String"))
        AddColumn(vaReport, "Kredit", System.Type.GetType("System.String"))
    End Sub

    Private Sub GetSQL()
        Dim cWhere As String = ""
        Dim n As Integer
        Dim i As Integer
        Dim nJumlahRecord As Integer
        Dim a As Integer, b As Integer
        Dim cNamaTemp As String
        Dim cAlamatTemp As String
        Dim dTglRegisterTemp As String
        Dim cKodeInduk As String
        Dim vaReportTemp As New DataTable
        Dim cTemp As String
        Dim cTglTemp As String
        Dim nBakiDebet As Double
        Dim dbAngsur As New DataTable
        Dim lAngsur As Boolean
        Dim row As DataRow
        Dim baris As DataRow

        InitTableTabungan()
        InitTableDeposito()
        initTableReport()
        Dim cField As String = "kode,rekening,0,Close,tglpenutupan"
        dbData = objData.Browse(GetDSN, "tabungan", cField, "kode", , cKodeTemp, , "kode,rekening")
        If dbData.Rows.Count > 0 Then
            vaTabunganTemp = dbData.Copy
            With vaTabunganTemp
                For n = 0 To .Rows.Count - 1
                    .Rows(n).Item("Jumlah") = 1
                    If n > 0 Then
                        If .Rows(n - 1).Item("kode").ToString = .Rows(n).Item("Kode").ToString Then
                            .Rows(n).Item("Jumlah") = CDbl(.Rows(n - 1).Item("Jumlah")) + 1
                        End If
                    End If
                Next
            End With
        End If

        cField = "kode,rekening,0,tglcair"
        dbData = objData.Browse(GetDSN, "deposito", cField, "Kode", , cKodeTemp, , "kode,rekening")
        If dbData.Rows.Count > 0 Then
            vaDepositoTemp = dbData.Copy
            With vaDepositoTemp
                For n = 0 To .Rows.Count - 1
                    .Rows(n).Item("Jumlah") = 1
                    If n > 0 Then
                        If .Rows(n - 1).Item("Kode").ToString = .Rows(n).Item("Kode").ToString Then
                            .Rows(n).Item("Jumlah") = CDbl(.Rows(n - 1).Item("Jumlah")) + 1
                        End If
                        cTglTemp = GetNull(dbData.Rows(n).Item("TglCair"), "9999-12-31").ToString
                        If cTglTemp.Substring(0, 4) <> "0000" And cTglTemp.Substring(0, 4) <> "9999" Then
                            .Rows(n).Item("Jumlah") = "C A I R"
                            .Rows(n).Item("TglCair") = CDate(GetNull(dbData.Rows(n).Item("TglCair"), "9999-12-31"))
                        End If
                    End If
                Next
            End With
        End If

        dbData = objData.Browse(GetDSN, "debitur", "kode,rekening,0,0", "Kode", , cKodeTemp, , "kode,rekening")
        If dbData.Rows.Count > 0 Then
            vaKreditTemp = dbData.Copy
            With vaKreditTemp
                For n = 0 To .Rows.Count - 1
                    .Rows(n).Item("Jumlah") = 1
                    If n > 0 Then
                        If .Rows(n - 1).Item("Kode").ToString = .Rows(n).Item("Kode").ToString Then
                            .Rows(n).Item("Jumlah") = CDbl(.Rows(n - 1).Item("Jumlah")) + 1
                        End If
                    End If
                Next
            End With
        End If

        n = 0
        cField = "0,kodeinduk,kode,nama,alamat,TglRegister,0,0,0,left(kode,2) as Cabang"
        '  cWhere = cWhere & " and kode ='01.011101'"
        'Dim vaJoin() As Object = {"left join tabungan t on t.kode=r.kode", "left join deposito d on d.kode=r.kode", "left join debitur k on k.kode = r.kode"}
        dbData = objData.Browse(GetDSN, "RegisterNasabah", cField, "Kode", , cKodeTemp, cWhere, "KodeInduk,Kode")
        If dbData.Rows.Count > 0 Then
            cKodeInduk = GetNull(dbData.Rows(0).Item("KodeInduk"), "").ToString
            cNamaTemp = GetNull(dbData.Rows(0).Item("Nama"), "").ToString
            cAlamatTemp = GetNull(dbData.Rows(0).Item("Alamat"), "").ToString
            dTglRegisterTemp = GetNull(dbData.Rows(0).Item("TglRegister"), "1900-01-01").ToString
            nJumlahRecord = 0
            a = 0
            vaReportTemp.Reset()
            vaReportTemp = dbData.Copy
            For n = 0 To vaReportTemp.Rows.Count - 1
                vaReportTemp.Rows(n).Item(0) = n + 1
            Next
            For i = 0 To vaTabunganTemp.Rows.Count - 1
                row = vaReportTemp.Select(String.Format("kode = '{0}", vaTabunganTemp.Rows(i).Item("Kode").ToString)).FirstOrDefault
                a = vaReportTemp.Rows.IndexOf(row)
                If Not row Is Nothing Then
                    If CDbl(row.Item(6)) = 0 Then
                        row.Item(6) = vaTabunganTemp.Rows(i).Item("Rekening").ToString
                    Else
                        b = a + CInt(vaTabunganTemp.Rows(i).Item(2)) - 1
                        baris = vaReportTemp.NewRow
                        vaReportTemp.Rows.InsertAt(baris, a)
                        vaReportTemp.Rows(a).Item(6) = vaTabunganTemp.Rows(i).Item("Rekening").ToString
                    End If
                    If vaTabunganTemp.Rows(i).Item("Jumlah").ToString = "1" Then
                        cTemp = " - T U T U P"
                    Else
                        cTemp = ""
                    End If
                    vaReportTemp.Rows(b).Item(6) = vaReportTemp.Rows(b).Item(6).ToString & cTemp
                End If
            Next

            For i = 0 To vaDepositoTemp.Rows.Count - 1
                row = vaReportTemp.Select(String.Format("kode = '{0}", vaDepositoTemp.Rows(i).Item("Kode").ToString)).FirstOrDefault
                a = vaReportTemp.Rows.IndexOf(row)
                If Not row Is Nothing Then
                    If CDbl(row.Item(7)) = 0 Then
                        row.Item(7) = vaDepositoTemp.Rows(i).Item("Rekening").ToString
                    Else
                        b = a + CInt(vaDepositoTemp.Rows(i).Item(2)) - 1
                        If vaReportTemp.Rows.Count < b Then
                            baris = vaReportTemp.NewRow
                            vaReportTemp.Rows.InsertAt(baris, a)
                        End If
                        vaReportTemp.Rows(a).Item(7) = vaDepositoTemp.Rows(i).Item("Rekening").ToString
                    End If
                    cTglTemp = GetNull(vaDepositoTemp.Rows(i).Item(3), "9999-12-31").ToString
                    If cTglTemp.Substring(0, 4) <> "0000" And cTglTemp.Substring(0, 4) <> "9999" Then
                        cTemp = " - C A I R"
                    Else
                        cTemp = ""
                    End If
                    If cTemp <> "" Then
                        vaReportTemp.Rows(b).Item(7) = vaReportTemp.Rows(b).Item(7).ToString & cTemp
                    End If
                End If
            Next

            For i = 0 To vaKreditTemp.Rows.Count - 1
                row = vaReportTemp.Select(String.Format("kode = '{0}", vaKreditTemp.Rows(i).Item("Kode").ToString)).FirstOrDefault
                a = vaReportTemp.Rows.IndexOf(row)
                If Not row Is Nothing Then
                    If CDbl(row.Item(8)) = 0 Then
                        row.Item(8) = vaKreditTemp.Rows(i).Item("Rekening").ToString
                    Else
                        b = a + CInt(vaKreditTemp.Rows(i).Item(2)) - 1
                        If vaReportTemp.Rows.Count < b Then
                            baris = vaReportTemp.NewRow
                            vaReportTemp.Rows.InsertAt(baris, a)
                        End If
                        vaReportTemp.Rows(a).Item(8) = vaKreditTemp.Rows(i).Item("Rekening").ToString
                    End If
                    nBakiDebet = GetBakiDebet(vaKreditTemp.Rows(i).Item(1).ToString, GetTglTransaksi)
                    cWhere = String.Format(" and status = '{0}'", eAngsuran.ags_Angsuran)
                    dbAngsur = objData.Browse(GetDSN, "Angsuran", "Rekening", "Rekening", , vaKreditTemp.Rows(i).Item("Rekening").ToString, cWhere)
                    If dbAngsur.Rows.Count > 0 Then
                        lAngsur = True
                    Else
                        lAngsur = False
                    End If
                    If nBakiDebet = 0 And lAngsur Then
                        cTemp = " - L U N A S"
                    Else
                        cTemp = ""
                    End If
                    vaReportTemp.Rows(b).Item(8) = vaReportTemp.Rows(b).Item(8).ToString & cTemp
                End If
            Next
            dbAngsur.Dispose()

            For n = 0 To vaReportTemp.Rows.Count - 1
                vaReport.Rows(n).Item(0) = vaReportTemp.Rows(n).Item(6).ToString
                vaReport.Rows(n).Item(1) = vaReportTemp.Rows(n).Item(7).ToString
                vaReport.Rows(n).Item(2) = vaReportTemp.Rows(n).Item(8).ToString
            Next
        End If
    End Sub

    Private Sub CariData()
        Dim cWhere As String = ""
        Dim cParameter As String
        Dim cText As String
        Dim vaJoin() As Object = Nothing
        Dim cTable As String = ""
        Dim cField As String = ""
        Dim cOrder As String = ""
        Dim cWhere1 As String = ""
        Dim cSortType As String = ""
        Dim cUrutTemp As String = ""

        If cSearch.Text <> "" Then
            If optCara.SelectedIndex = 0 Then
                cParameter = "Like '%"
            Else
                cParameter = "Like '"
            End If
            cText = Replace(cSearch.Text, "'", "''")
            If cbUrut.Text = "No. Register" Then
                cUrutTemp = "Kode"
            Else
                cUrutTemp = cbUrut.Text
            End If
            Select Case Trim(cbKategori.Text)
                Case "Nama"
                    cWhere = String.Format(" Nama {0}{1}%' ", cParameter, cText)
                Case "Alamat"
                    cWhere = String.Format(" Alamat {0}{1}%' ", cParameter, cSearch.Text)
                Case "Kode Induk"
                    cWhere = String.Format(" KodeInduk {0}{1}%' ", cParameter, cSearch.Text)
                Case Else
                    Exit Select
            End Select
            Select Case Trim(cbJenis.Text)
                Case "Semua"
                    cTable = "RegisterNasabah r"
                    cField = "r.Kode,r.Nama,r.Alamat,r.KodeInduk,''"
                    cOrder = Replace(cUrutTemp, " ", "")
                    vaJoin = Nothing
                Case "Tabungan"
                    cTable = "tabungan t"
                    cField = "r.Kode,r.Nama,r.Alamat,r.KodeInduk,''"
                    cOrder = "r." & Replace(cUrutTemp, " ", "")
                    vaJoin = {"left join registernasabah r on t.kode = r.kode"}
                Case "Deposito"
                    cTable = "deposito t"
                    cField = "r.Kode,r.Nama,r.Alamat,r.KodeInduk,''"
                    cOrder = "r." & Replace(cUrutTemp, " ", "")
                    vaJoin = {"left join registernasabah r on t.kode = r.kode"}
                Case "Kredit"
                    cTable = "debitur t"
                    cField = "r.Kode,r.Nama,r.Alamat,r.KodeInduk,''"
                    cOrder = "r." & Replace(cUrutTemp, " ", "")
                    vaJoin = {"left join registernasabah r on t.kode = r.kode"}
                Case Else
                    Exit Select
            End Select
            If cbJenisUrut.Text = "A-Z" Then
                cSortType = "Asc"
            Else
                cSortType = "Desc"
            End If
            If optKantor.SelectedIndex <> 0 Then
                cWhere1 = String.Format(" and left(r.kode,2)='{0}'", cbKantor.Text.Substring(0, 2))
            Else
                cWhere1 = ""
            End If
            dbSearch = objData.Browse(GetDSN, cTable, cField, , , , String.Format("{0}{1} group by r.kode", cWhere, cWhere1), String.Format("{0} {1}", cOrder, cSortType), vaJoin)
            If dbSearch.Rows.Count > 0 Then
                vaSearch.Reset()
                vaSearch = dbSearch.Copy
            Else
                vaSearch.Reset()
            End If
            gRegister.DataSource = vaSearch
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Kode", , , , , , "No. Register")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "Alamat")
            GridColumnFormat(GridView1, "KodeInduk")
            tabData.SelectedTabPage = XtraTabPage1
        Else
            ClearGrid()
        End If
    End Sub

    Private Sub ClearGrid()
        Dim vaClear As New DataTable
        vaClear.Reset()
        gRegister.DataSource = vaClear
        gTabungan.DataSource = vaClear
        gTabunganDetail.DataSource = vaClear
        gDeposito.DataSource = vaClear
        gDepositoDetail.DataSource = vaClear
        gPengajuanKredit.DataSource = vaClear
        gDataDetailTabungan.DataSource = vaClear
        gDataDetailTabungan.DataSource = vaClear
        gDataDetailKredit.DataSource = vaClear
        gSaldoInduk.DataSource = vaClear
    End Sub

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cbKategori.Properties
        With properties
            .Items.AddRange(New String() {"Nama", "Alamat", "Kode Induk"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbKategori.SelectedIndex = 0

        properties = cbUrut.Properties
        With properties
            .Items.AddRange(New String() {"No. Register", "Nama", "Alamat", "Kode Induk"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbUrut.SelectedIndex = 0

        properties = cbJenisUrut.Properties
        With properties
            .Items.AddRange(New String() {"A-Z", "Z-A"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbJenisUrut.SelectedIndex = 0

        properties = cbJenis.Properties
        With properties
            .Items.AddRange(New String() {"Semua", "Tabungan", "Deposito", "Kredit"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbJenis.SelectedIndex = 0
    End Sub

    Private Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        CariData()
    End Sub

    Private Sub InitValue()
        optCara.SelectedIndex = 0
        cSearch.Text = ""
        optKantor.SelectedIndex = 0
    End Sub

    Private Sub trCariRegister_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        lClickCancel = False
        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , , , cmdKeluar, , cmdPreview, , , , cmdDetailNasabah, cmdSearch)

        InitValue()
        InitTableKantor()
        IsiCombo()
        ClearGrid()
        SetKantor()

        SetTabIndex(optKantor.TabIndex, n)
        SetTabIndex(cbKantor.TabIndex, n)
        SetTabIndex(cbJenis.TabIndex, n)
        SetTabIndex(optCara.TabIndex, n)
        SetTabIndex(cSearch.TabIndex, n)
        SetTabIndex(cbKategori.TabIndex, n)
        SetTabIndex(cbUrut.TabIndex, n)
        SetTabIndex(cbJenisUrut.TabIndex, n)
        SetTabIndex(cmdSearch.TabIndex, n)
        SetTabIndex(cmdPreview.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        optKantor.EnterMoveNextControl = True
        cbKantor.EnterMoveNextControl = True
        cbJenis.EnterMoveNextControl = True
        optCara.EnterMoveNextControl = True
        cSearch.EnterMoveNextControl = True
        cbKategori.EnterMoveNextControl = True
        cbUrut.EnterMoveNextControl = True
        cbJenisUrut.EnterMoveNextControl = True
    End Sub

    Private Sub InitTableKantor()
        cbKantor.Properties.Items.Clear()
        vaKantor.Reset()
        Dim i As Integer = 0
        Dim cTempKantor As String = ""
        nNomorTemp = 0
        dbData = objData.Browse(GetDSN, "Cabang", "kode,Keterangan")
        If dbData.Rows.Count > 0 Then
            cbKantor.Text = String.Format("{0}.{1}", dbData.Rows(0).Item("Kode"), UCase(dbData.Rows(0).Item("Keterangan").ToString))
            i = i + 1
            For i = 0 To dbData.Rows.Count - 1
                cTempKantor = String.Format("{0}. {1}", dbData.Rows(i).Item("Kode"), UCase(dbData.Rows(i).Item("Keterangan").ToString))
                cbKantor.Properties.Items.Add(cTempKantor)
                If aCfg(eCfg.msKodeCabang).ToString = dbData.Rows(i).Item("Kode").ToString Then
                    nNomorTemp = i
                End If
            Next
            vaKantor = dbData.Copy
        End If
    End Sub

    Private Sub SetKantor()
        Dim va() As String
        Dim i As Integer
        Dim cTemp As String
        Dim nTemp As Integer

        For i = 0 To cbKantor.Properties.Items.Count - 1
            cTemp = Trim(cbKantor.Properties.Items(i).ToString)
            va = Split(cTemp, ".")
            nTemp = CInt(va(0))
            If nNomorTemp = nTemp Then
                cbKantor.Text = cbKantor.Properties.Items(i).ToString
                Exit For
            End If
        Next
    End Sub

    Private Sub GetDataSaldo(ByVal cKodeInduk As String)
        Dim n As Integer
        Dim dbInduk As New DataTable
        Dim nSaldoTotal As Double

        gSaldoInduk.DataSource = Nothing
        Const cField As String = "kode,nama,0"
        dbInduk = objData.Browse(GetDSN, "RegisterNasabah", cField, "KodeInduk", , cKodeInduk, , "Kode")
        If dbInduk.Rows.Count > 0 Then
            nSaldoTotal = 0
            For n = 0 To dbInduk.Rows.Count - 1
                nSaldoTotal = nSaldoTotal + GetSaldoRegister(dbInduk.Rows(n).Item(0).ToString, Date.Today)
                dbInduk.Rows(n).Item(2) = nSaldoTotal
            Next
            gSaldoInduk.DataSource = dbInduk
        End If
        dbInduk.Dispose()
    End Sub

    Private Sub GetMemory()
        Dim n As Integer
        Dim cField As String = ""

        ' Ambil Data Tabungan
        gTabungan.DataSource = Nothing
        dbData = objData.Browse(GetDSN, "Tabungan", "Rekening,Tgl,0", "Kode", , cKodeTemp, , "Rekening")
        If dbData.Rows.Count > 0 Then
            vaTabungan.Reset()
            For n = 0 To dbData.Rows.Count - 1
                dbData.Rows(n).Item(2) = GetSaldoTabungan(dbData.Rows(n).Item("Rekening").ToString, Date.Today)
            Next
            gTabungan.DataSource = dbData
        End If

        ' Ambil Data Deposito
        gDeposito.DataSource = Nothing
        dbData = objData.Browse(GetDSN, "Deposito", "Rekening,Tgl,Nominal", "Kode", , cKodeTemp, , "Rekening")
        If dbData.Rows.Count > 0 Then
            gDeposito.DataSource = dbData
        End If

        ' Ambil Data Kredit
        gKredit.DataSource = Nothing
        dbData = objData.Browse(GetDSN, "Debitur", "Rekening,Tgl,0", "Kode", , cKodeTemp, , "Rekening")
        If dbData.Rows.Count > 0 Then
            For n = 0 To dbData.Rows.Count - 1
                dbData.Rows(n).Item(2) = GetBakiDebet(dbData.Rows(n).Item("Rekening").ToString, Date.Today)
            Next
            gKredit.DataSource = dbData
        Else
            gDataDetailKredit.DataSource = Nothing
        End If

        ' Ambil Data Pengajuan Kredit
        gPengajuanKredit.DataSource = Nothing
        cField = "Rekening,Tgl,Plafond,Lama,if(statuspengajuan=0,'Pending',if(statuspengajuan=1,'Acc','Reject')) as status"
        dbData = objData.Browse(GetDSN, "PengajuanKredit", cField, "Kode", , cKodeTemp, , "Rekening")
        If dbData.Rows.Count > 0 Then
            gPengajuanKredit.DataSource = dbData
        End If
    End Sub

    Private Sub GetMemoryRekening()
        Try
            Dim n As Integer
            Dim nRec As Integer
            Dim cWhere As String
            Dim nBakiDebet As Double
            Dim nRecTab As Integer = 0
            Dim nRecDep As Integer = 0
            Dim nRecKredit As Integer = 0
            Dim lAngsur As Boolean
            Dim dbAngsur As New DataTable
            Dim cTemp As String
            Dim cTglTemp As String
            Dim row As DataRow

            ' Ambil Data Tabungan
            vaRekTabungan.Reset()
            AddColumn(vaRekTabungan, "Rekening", System.Type.GetType("System.String"))
            AddColumn(vaRekTabungan, "RekeningLama", System.Type.GetType("System.String"))
            AddColumn(vaRekTabungan, "Status", System.Type.GetType("System.String"))
            AddColumn(vaRekTabungan, "TglTutup", System.Type.GetType("System.String"))
            AddColumn(vaRekTabungan, "Kode", System.Type.GetType("System.String"))
            vaRekTabungan.Rows.Add(New Object() {"Tabungan :", "", "", "", ""})
            Dim cField As String = "Rekening,RekeningLama,Close,tglpenutupan,kode"
            dbData = objData.Browse(GetDSN, "Tabungan", cField, "Kode", , cKodeTemp, , "Rekening")
            If dbData.Rows.Count > 0 Then
                For n = 0 To dbData.Rows.Count - 1
                    With dbData.Rows(n)
                        row = vaRekTabungan.NewRow
                        row("Rekening") = .Item("Rekening").ToString
                        row("RekeningLama") = .Item("Rekeninglama").ToString
                        If .Item("Close").ToString = "1" Then
                            row("Status") = "T U T U P"
                            row("TglTutup") = GetNull(.Item("TglPenutupan"), "9999-12-31").ToString
                        Else
                            row("Status") = ""
                            row("TglTutup") = ""
                        End If
                        row("Kode") = .Item("Kode").ToString
                        vaRekTabungan.Rows.Add(row)
                    End With
                    nRecTab = nRecTab + 1
                Next
            End If

            ' Ambil Data Deposito
            vaRekDeposito.Reset()
            AddColumn(vaRekDeposito, "Rekening", System.Type.GetType("System.String"))
            AddColumn(vaRekDeposito, "RekeningLama", System.Type.GetType("System.String"))
            AddColumn(vaRekDeposito, "Status", System.Type.GetType("System.String"))
            AddColumn(vaRekDeposito, "TglCair", System.Type.GetType("System.String"))
            AddColumn(vaRekDeposito, "Kode", System.Type.GetType("System.String"))
            vaRekTabungan.Rows.Add(New Object() {"Deposito :", "", "", "", ""})
            cField = "Rekening,RekeningLama,TglCair,kode"
            dbData = objData.Browse(GetDSN, "Deposito", cField, "Kode", , cKodeTemp, , "Rekening")
            If dbData.Rows.Count > 0 Then
                For n = 0 To dbData.Rows.Count - 1
                    With dbData.Rows(n)
                        row = vaRekDeposito.NewRow
                        row("Rekening") = .Item("Rekening").ToString
                        row("RekeningLama") = .Item("Rekeninglama").ToString
                        cTglTemp = GetNull(.Item("TglCair"), "9999-12-31").ToString
                        If cTglTemp.Substring(0, 4) <> "0000" And cTglTemp.Substring(0, 4) <> "9999" Then
                            row("Status") = "C A I R"
                            row("TglCair") = GetNull(.Item("TglCair"), "9999-12-31").ToString
                        End If
                        row("Kode") = .Item("Kode").ToString
                        vaRekDeposito.Rows.Add(row)
                    End With
                    nRecDep = nRecDep + 1
                Next
            End If

            ' Ambil Data Kredit
            vaRekKredit.Reset()
            AddColumn(vaRekKredit, "Rekening", System.Type.GetType("System.String"))
            AddColumn(vaRekKredit, "RekeningLama", System.Type.GetType("System.String"))
            AddColumn(vaRekKredit, "Status", System.Type.GetType("System.String"))
            AddColumn(vaRekKredit, "Kode", System.Type.GetType("System.String"))
            vaRekKredit.Rows.Add(New Object() {"Kredit :", "", "", ""})
            cField = "Rekening,RekeningLama,NoSPK,kode"
            dbData = objData.Browse(GetDSN, "Debitur", cField, "Kode", , cKodeTemp, , "Rekening")
            If dbData.Rows.Count > 0 Then
                For n = 0 To dbData.Rows.Count - 1
                    With dbData.Rows(n)
                        row = vaRekKredit.NewRow
                        row("Rekening") = .Item("Rekening").ToString
                        row("RekeningLama") = .Item("NoSPK").ToString
                        nBakiDebet = GetBakiDebet(.Item("Rekening").ToString, GetTglTransaksi)
                        cWhere = String.Format(" and status = '{0}'", eAngsuran.ags_Angsuran)
                        dbAngsur = objData.Browse(GetDSN, "Angsuran", "Rekening", "Rekening", , .Item("Rekening").ToString, cWhere)
                        If dbAngsur.Rows.Count > 0 Then
                            lAngsur = True
                        Else
                            lAngsur = False
                        End If
                        dbAngsur.Dispose()
                        If nBakiDebet = 0 And lAngsur Then
                            row("Status") = "L U N A S"
                        End If
                        row("Kode") = .Item("Kode").ToString
                        vaRekKredit.Rows.Add(row)
                    End With
                    nRecKredit = nRecKredit + 1
                Next
            End If

            vaRekening.Reset()
            AddColumn(vaRekening, "Keterangan", System.Type.GetType("System.String"))
            For n = 0 To vaRekTabungan.Rows.Count - 1
                With vaRekTabungan.Rows(n)
                    row = vaRekening.NewRow
                    If n = 0 Then
                        row(0) = .Item(0)
                        If .Item(1).ToString <> "" Then
                            row(0) = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                    Else
                        cTemp = .Item(0).ToString
                        If .Item(1).ToString <> "" Then
                            cTemp = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                        If .Item(2).ToString <> "" Then
                            cTemp = String.Format("{0} - {1} - {2}", cTemp, .Item(2), .Item(3))
                        End If
                        row(0) = cTemp
                    End If
                    vaRekening.Rows.Add(row)
                End With
            Next
            row = vaRekening.NewRow
            vaRekening.Rows.Add(row)
            vaRekening.AcceptChanges()

            If nRecTab > 0 Then
                nRec = nRecTab + 2
            Else
                nRec = 0
            End If
            Dim nBarisTemp As Integer = 0
            For n = 0 To vaRekDeposito.Rows.Count - 1
                With vaRekDeposito.Rows(n)
                    row = vaRekening.NewRow
                    nBarisTemp = n + nRec
                    If n = 0 Then
                        row(0) = .Item(0)
                        If .Item(1).ToString <> "" Then
                            row(0) = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                    Else
                        cTemp = .Item(0).ToString
                        If .Item(1).ToString <> "" Then
                            cTemp = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                        If .Item(2).ToString <> "" Then
                            cTemp = String.Format("{0} - {1} - {2}", cTemp, .Item(2), .Item(3))
                        End If
                        row(0) = cTemp
                    End If
                End With
                vaRekening.Rows.InsertAt(row, nBarisTemp)
                vaRekening.AcceptChanges()
            Next

            If nRecTab > 0 And nRecDep > 0 Then
                nRec = nRecTab + nRecDep + 4
            ElseIf nRecTab > 0 And nRecDep = 0 Then
                nRec = nRecTab + 2
            ElseIf nRecDep > 0 And nRecTab = 0 Then
                nRec = nRecDep + 2
            Else
                nRec = 0
            End If
            nBarisTemp = 0
            For n = 0 To vaRekKredit.Rows.Count - 1
                row = vaRekening.NewRow
                nBarisTemp = n + nRec
                With vaRekKredit.Rows(n)
                    If n = 0 Then
                        row(0) = .Item(0)
                        If .Item(1).ToString <> "" Then
                            row(0) = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                    Else
                        cTemp = .Item(0).ToString
                        If .Item(1).ToString <> "" Then
                            cTemp = String.Format("{0} - {1}", .Item(0), .Item(1))
                        End If
                        If .Item(2).ToString <> "" Then
                            cTemp = String.Format("{0} - {1}", cTemp, .Item(2))
                        End If
                        row(0) = cTemp
                    End If
                    vaRekening.Rows.InsertAt(row, nBarisTemp)
                    vaRekening.AcceptChanges()
                End With
            Next
            gRegisterDetail.DataSource = vaRekening
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GetMutasiHarian(ByVal nPar As eModule, ByVal cRekening As String)
        Dim db As New DataTable
        Dim cTable As String = ""
        Dim n As Integer
        Dim dTgl As Date
        Dim nSaldo As Double
        Dim row As DataRow

        gTabunganDetail.DataSource = Nothing
        gDepositoDetail.DataSource = Nothing
        gKreditDetail.DataSource = Nothing
        vaMutasi.Reset()
        'vaMutasi.Columns.Add("Tgl", System.Type.GetType("System.String"))
        'vaMutasi.Columns.Add("Mutasi1", System.Type.GetType("System.String"))
        'vaMutasi.Columns.Add("Mutasi2", System.Type.GetType("System.String"))
        'vaMutasi.Columns.Add("Saldo", System.Type.GetType("System.String"))
        Dim cField As String = ""
        Dim cOrder As String = ""
        Dim cWhere As String = ""
        Select Case nPar
            Case Is = eModule.mTabungan
                cTable = "MutasiTabungan"
                cField = "tgl,ifnull(debet,0) as debet,ifnull(kredit,0) as kredit,0"
                cOrder = "tgl,ID"
                dTgl = DateAdd("m", -12, Date.Today)
                cWhere = String.Format(" and Tgl >= '{0}' ", formatValue(dTgl, formatType.yyyy_MM_dd))
            Case Is = eModule.mDeposito
                cTable = "MutasiDeposito"
                cField = "tgl,if(setoranplafond>0,setoranplafond,-pencairanplafond) as pokok,bunga,pajak"
                cOrder = "Tgl,ID"
                dTgl = DateAdd("m", -12, Date.Today)
                cWhere = ""
            Case Is = eModule.mKredit
                cTable = "Angsuran"
                cField = "tgl,if(status=2,dpokok,kpokok) as pokok,if(status=2,dbunga,kbunga) as bunga,denda,0"
                cOrder = "tgl"
                cWhere = String.Format(" and status<>'{0}' ", eAngsuran.ags_jadwal)
            Case Else
                Exit Select
        End Select

        db = objData.Browse(GetDSN, cTable, cField, "Rekening", , cRekening, cWhere, cOrder)
        If db.Rows.Count > 0 Then
            Select Case nPar
                Case Is = eModule.mTabungan
                    ' masukkan data ke grid
                    nSaldo = GetSaldoTabungan(cRekening, DateAdd("d", -1, dTgl))
                    vaMutasi = db.Copy
                    row = vaMutasi.NewRow
                    row(3) = nSaldo
                    vaMutasi.Rows.InsertAt(row, 0)
                    For n = 1 To db.Rows.Count - 1
                        If n = 1 Then
                            vaMutasi.Rows(n).Item(3) = CDbl(vaMutasi.Rows(n).Item(2)) - CDbl(vaMutasi.Rows(n).Item(1))
                        Else
                            vaMutasi.Rows(n).Item(3) = CDbl(vaMutasi.Rows(n - 1).Item(3)) + CDbl(vaMutasi.Rows(n).Item(2)) - CDbl(vaMutasi.Rows(n).Item(1))
                        End If
                    Next
                    gTabunganDetail.DataSource = vaMutasi
                    GetDetail(eModule.mTabungan)

                Case Is = eModule.mDeposito
                    ' masukkan data ke grid
                    vaMutasi = db.Copy
                    gDepositoDetail.DataSource = vaMutasi
                    GetDetail(eModule.mDeposito)

                Case Is = eModule.mKredit
                    ' masukkan data ke grid
                    vaMutasi = db.Copy
                    For n = 0 To vaMutasi.Rows.Count - 1
                        vaMutasi.Rows(n).Item(4) = CDbl(vaMutasi.Rows(n).Item(1)) + CDbl(vaMutasi.Rows(n).Item(2)) + CDbl(vaMutasi.Rows(n).Item(3))
                    Next
                    gKreditDetail.DataSource = vaMutasi
                    GetDetail(eModule.mKredit)
            End Select
        End If
    End Sub

    Private Sub GetDetail(ByVal nPar As eModule)
        Dim db As New DataTable
        Dim cField As String
        Dim vaDetailTemp As New DataTable

        Select Case nPar
            Case Is = eModule.mTabungan
                cField = "Rekening,RekeningLama,Tgl,GolonganNasabah,AO"
                db = objData.Browse(GetDSN, "Tabungan", cField, "Kode", , cKodeTemp)
                If db.Rows.Count > 0 Then
                    vaDetailTemp = db.Copy
                Else
                    vaDetailTemp = Nothing
                End If
                gDataDetailTabungan.DataSource = vaDetailTemp

            Case Is = eModule.mDeposito
                cField = "Rekening,NoBilyet,Tgl,GolonganDeposan,if(ARO='Y','Ya','Tidak'),RekeningTabungan"
                db = objData.Browse(GetDSN, "Deposito", cField, "Kode", , cKodeTemp)
                If db.Rows.Count > 0 Then
                    vaDetailTemp = db.Copy
                Else
                    vaDetailTemp = Nothing
                End If
                gDataDetailDeposito.DataSource = vaDetailTemp

            Case Is = eModule.mKredit
                GetDetailKredit()
        End Select
        db.Dispose()
        vaDetailTemp.Dispose()
    End Sub

    Private Sub GetDetailKredit()
        Dim n As Integer
        Dim db As New DataTable
        Dim dbDetail As New DataTable

        Dim cField As String = "d.tgl,d.rekeninglama as rekening_lama,d.nospk as no_spk,"
        cField = cField & "concat(d.golongankredit,' - ',a.Keterangan as NamaGolonganKredit) as golongan_kredit,"
        cField = cField & "concat(d.sifatkredit,' - ', f.Keterangan as NamaSifatKredit) as sifat_kredit,"
        cField = cField & "concat(d.jenispenggunaan,' - ', g.Keterangan as NamaJenisPenggunaan) as jenis_penggunaan,"
        cField = cField & "concat(d.GolonganDebitur,' - ',c.Keterangan as NamaGolonganDebitur) as golongan_debitur, "
        cField = cField & "concat(d.sektorekonomi,' - ',e.Keterangan as NamaSektorEkonomi) as sektor_ekonomi,"
        cField = cField & "concat(d.wilayah,' - ',w.Keterangan as NamaWilayah) as wilayah, concat(d.ao,' - ',h.Nama as NamaAO) as AO,"
        cField = cField & "concat(d.golonganpenjamin,' - ', l.Keterangan as NamaGolonganPenjamin) as golongan_penjamin,"
        cField = cField & "concat(d.caraperhitungan,' - ',ch.keterangan as KeteranganCaraHitung) as cara_perhitungan,"
        cField = cField & "d.sukubunga as suku_bunga,d.lama,d.graceperiodepokok as grace_periode_pokok,d.graceperiodebunga as grace_periode_bunga,"
        cField = cField & "d.plafond, d.totalbunga as jumlah_bunga, d.angsuranperbulan as angsuran_per_bulan,"
        cField = cField & "d.rekeningtabungan as rekening_tabungan,j.Nama as nama_tabungan,j.Alamat as alamat_tabungan"
        Dim vaJoin() As Object = {"Left Join Wilayah w on w.Kode = d.Wilayah",
                                  "Left Join RegisterNasabah r on r.Kode = d.Kode",
                                  "Left Join GolonganKredit a on a.Kode = d.GolonganKredit",
                                  "Left Join GolonganDebitur c on c.Kode = d.GolonganDebitur",
                                  "Left Join SektorEkonomi e on e.Kode = d.SektorEkonomi",
                                  "Left Join SifatKredit f on f.Kode = d.SifatKredit",
                                  "Left Join JenisPenggunaan g on g.Kode = d.JenisPenggunaan",
                                  "Left Join GolonganPenjamin l on l.Kode = d.GolonganPenjamin",
                                  "Left Join AO h on h.Kode = d.AO",
                                  "Left Join Tabungan t on t.Rekening = d.RekeningTabungan",
                                  "Left Join RegisterNasabah j on j.Kode = t.Kode",
                                  "Left Join CaraHitung ch on ch.kode = d.caraperhitungan"}
        dbDetail = objData.Browse(GetDSN, "Debitur d", cField, "d.Rekening", , cRekeningKredit, , , vaJoin)
        If dbDetail.Rows.Count > 0 Then
            db.Reset()
            db.Columns.Add("field")
            db.Columns.Add("keterangan")
            Dim row As DataRow
            Dim cColumnName As String
            Dim cColumnName1() As String
            Dim cColumnName2 As String = ""
            For n = 0 To dbDetail.Columns.Count - 1
                row = db.NewRow
                cColumnName = dbDetail.Columns(n).ColumnName
                cColumnName1 = cColumnName.Split(CChar("_"))
                For i As Integer = 0 To cColumnName1.GetUpperBound(0)
                    If i > 0 Then
                        cColumnName2 = String.Format("{0} {1}", cColumnName2, UCase(cColumnName1(i)))
                    Else
                        cColumnName2 = UCase(cColumnName1(i))
                    End If
                Next
                row(0) = cColumnName2
                row(1) = dbDetail.Rows(0).Item(n).ToString
                dbDetail.Rows.Add(row)
            Next
        End If
    End Sub


    Private Sub cSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles cSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            CariData()
        End If
    End Sub

    Private Sub GridView2_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView2.FocusedRowChanged
        Try
            Dim cTable As String = ""
            Dim cField As String = ""
            Dim nTypeModul As eModule
            Dim db As New DataTable
            Dim row As DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim cData As String = row(0).ToString
            Dim cGolongan As String = ""
            If Trim(cData).Length = 15 Then
                cGolongan = cData.Substring(4, 2)
                If cGolongan <= "19" Then
                    cTable = "Tabungan"
                    cField = "GolonganTabungan"
                    nTypeModul = eModule.mTabungan
                ElseIf cGolongan <= "29" And cGolongan >= "20" Then
                    cTable = "Deposito"
                    cField = "GolonganDeposito"
                    nTypeModul = eModule.mDeposito
                ElseIf cGolongan >= "30" Then
                    cTable = "Debitur"
                    cField = "GolonganKredit"
                    nTypeModul = eModule.mKredit
                End If

                db = objData.Browse(GetDSN, cTable, cField, "rekening", , cData)
                If db.Rows.Count > 0 Then
                    Select Case nTypeModul
                        Case eModule.mTabungan
                            'trCetakTabungan.Action c
                        Case eModule.mDeposito
                            'trCetakDeposito.Action c
                        Case eModule.mKredit
                            'trCetakKredit.Action c
                    End Select
                End If
                db.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub GridView3_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView3.FocusedRowChanged
        If GridView3.RowCount > 0 Then
            Dim row As DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)
            Dim cKeterangan As String = row(0).ToString
            GetMutasiHarian(eModule.mTabungan, cKeterangan)
        End If
    End Sub

    Private Sub GridView5_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView5.FocusedRowChanged
        With GridView4
            If .RowCount > 0 Then
                Dim row As DataRow = .GetDataRow(.FocusedRowHandle)
                Dim cKeterangan As String = row(0).ToString
                GetMutasiHarian(eModule.mDeposito, cKeterangan)
            End If
        End With
    End Sub

    Private Sub GridView7_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView7.FocusedRowChanged
        With GridView5
            If .RowCount > 0 Then
                Dim row As DataRow = .GetDataRow(.FocusedRowHandle)
                cRekeningKredit = row(0).ToString
                GetMutasiHarian(eModule.mDeposito, cRekeningKredit)
            End If
        End With
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        With GridView1
            If .RowCount > 0 Then
                Dim row As DataRow = .GetDataRow(.FocusedRowHandle)
                cKodeTemp = row(0).ToString
                GetMemory()
                GetMemoryRekening()
                GetDataSaldo(row(3).ToString)
            End If
        End With
    End Sub
End Class