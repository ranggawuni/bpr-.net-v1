﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trUpdateJadwalKredit
    ReadOnly objData As New data()
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim cFaktur As String = ""
    Dim nTotalBunga As Double = 0
    Dim dtJadwal As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cKode.Text = ""
        nPlafond.Text = ""
        cCaraPerhitungan.Text = ""
        nLama.Text = ""
        nSukuBunga.Text = ""
        nSukuBungaBerikutnya.Text = ""
        nGracePeriodePokok.Text = ""
        nGracePeriodeBunga.Text = ""
        nPembulatan.Text = ""
        optJadwal.SelectedIndex = 2
        nAngsuranPokok.Text = ""
        nAngsuranBunga.Text = ""
        nTotalAngsuran.Text = ""
        nPokokPenyesuaian.Text = ""
        nBungaPenyesuaian.Text = ""
        nTotalPenyesuaian.Text = ""
    End Sub

    Private Sub InitTable()
        AddColumn(dtJadwal, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(dtJadwal, "JthTmp", System.Type.GetType("System.DateTime"))
        AddColumn(dtJadwal, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtJadwal, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(dtJadwal, "Angsuran", System.Type.GetType("System.Double"))
        AddColumn(dtJadwal, "SisaBunga", System.Type.GetType("System.Double"))
        AddColumn(dtJadwal, "SisaPokok", System.Type.GetType("System.Double"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim n As Integer
            dtJadwal.Reset()
            InitTable()
            Dim nRow As DataRow = dtJadwal.NewRow()
            nRow("Ke") = 0
            nRow("JthTmp") = #1/1/1900#
            nRow("Bunga") = 0
            nRow("Pokok") = 0
            nRow("Angsuran") = 0
            nRow("SisaBunga") = nTotalBunga
            nRow("SisaPokok") = CDbl(nPlafond.Text)
            dtJadwal.Rows.Add(nRow)

            Const cField As String = "Tgl,DBunga,DPokok"
            Dim cWhere As String = " and status = " & eAngsuran.ags_jadwal
            dtData = objData.Browse(GetDSN, "angsuran", cField, "rekening", , cRekening.Text, cWhere, "tgl,id")
            If dtData.Rows.Count > 0 Then
                For n = 0 To dtData.Rows.Count - 1
                    nRow("Ke") = n + 1
                    nRow("JthTmp") = CDate(dtData.Rows(n).Item("Tgl"))
                    nRow("Bunga") = CDbl(dtData.Rows(n).Item("DBunga"))
                    nRow("Pokok") = CDbl(dtData.Rows(n).Item("DPokok"))
                    nRow("Angsuran") = CDbl(dtData.Rows(n).Item("DPokok")) + CDbl(dtData.Rows(n).Item("DBunga"))
                    dtJadwal.Rows.Add(nRow)
                Next
            End If
            For n = 1 To dtJadwal.Rows.Count - 1
                With dtJadwal
                    .Rows(n).Item("SisaBunga") = CDbl(.Rows(n - 1).Item("SisaBunga")) - CDbl(.Rows(n).Item("Bunga"))
                    .Rows(n).Item("SisaPokok") = CDbl(.Rows(n - 1).Item("SisaPokok")) - CDbl(.Rows(n).Item("Pokok"))
                End With
            Next
            nAngsuranPokok.Text = dtJadwal.Rows(1).Item("Pokok").ToString
            nAngsuranPokok.Text = dtJadwal.Rows(1).Item("Bunga").ToString
            GridControl1.DataSource = dtJadwal
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Ke", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "JthTmp")
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub IsiJadwalPenyesuaian()
        Dim nRow As DataRow = dtJadwal.NewRow()
        Dim nTotBunga As Double = Math.Round(CDbl(nPlafond.Text) * CDbl(nSukuBunga.Text) / 100 / 12 * CDbl(nLama.Text), 0)
        nTotalBunga = 0
        Dim nTotalPokok As Double = 0
        dtJadwal.Reset()
        InitTable()
        nRow("Ke") = 0
        nRow("JthTmp") = #1/1/1900#
        nRow("Bunga") = 0
        nRow("Pokok") = 0
        nRow("Angsuran") = 0
        nRow("SisaBunga") = nTotBunga
        nRow("SisaPokok") = CDbl(nPlafond.Text)
        dtJadwal.Rows.Add(nRow)
        For n As Integer = 1 To CInt(nLama.Text)
            nRow("Ke") = n
            nRow("JthTmp") = DateAdd(DateInterval.Month, n, dTgl.DateTime)
            nRow("Bunga") = CDbl(nBungaPenyesuaian.Text)
            nRow("Pokok") = CDbl(nPokokPenyesuaian.Text)
            nRow("Angsuran") = CDbl(nPokokPenyesuaian.Text) + CDbl(nBungaPenyesuaian.Text)
            nTotalPokok = nTotalPokok + CDbl(nPokokPenyesuaian.Text)
            nTotalBunga = nTotalBunga + CDbl(nBungaPenyesuaian.Text)
            dtJadwal.Rows.Add(nRow)
        Next
        With dtJadwal
            For n As Integer = 1 To .Rows.Count - 1
                .Rows(n).Item("SisaBunga") = CDbl(.Rows(n - 1).Item("SisaBunga")) - CDbl(.Rows(n).Item("Bunga"))
                .Rows(n).Item("SisaPokok") = CDbl(.Rows(n - 1).Item("SisaPokok")) - CDbl(.Rows(n).Item("Pokok"))
            Next
            nTotalPokok = CDbl(nPlafond.Text) - nTotalPokok
            nTotalBunga = nTotBunga - nTotalBunga
            If nTotalPokok + nTotalBunga > 0 Then
                Dim nBaris As Integer = dtJadwal.Rows.Count - 1
                .Rows(nBaris).Item("Bunga") = CDbl(.Rows(nBaris).Item("Bunga")) + nTotalBunga
                .Rows(nBaris).Item("Pokok") = CDbl(.Rows(nBaris).Item("Pokok")) + nTotalPokok
                .Rows(nBaris).Item("Angsuran") = CDbl(.Rows(nBaris).Item("Pokok")) + CDbl(.Rows(nBaris).Item("Bunga"))
                .Rows(nBaris).Item("SisaBunga") = CDbl(.Rows(nBaris - 1).Item("SisaBunga")) + CDbl(.Rows(nBaris).Item("Bunga"))
                .Rows(nBaris).Item("SisaPokok") = CDbl(.Rows(nBaris - 1).Item("SisaPokok")) + CDbl(.Rows(nBaris).Item("Pokok"))
            End If
        End With
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Ke", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "JthTmp")
        GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If dtJadwal.Rows.Count <= 0 Then
            ValidSaving = False
            MessageBox.Show("Jadwal Angsuran Tidak Ada.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cFaktur As String = ""
        Dim cKeteranganTemp As String = ""
        If ValidSaving() Then
            If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim cKodeCabang As String = cRekening.Text.Substring(0, 2)
                cWhere = String.Format(" and Rekening = '{0}'", cRekening.Text)
                objData.Delete(GetDSN, "Angsuran", "Status", , eAngsuran.ags_jadwal.ToString, cWhere)
                cFaktur = GetLastFaktur(eFaktur.fkt_Jadwal_Angsuran, dTgl.DateTime, , True)
                For n As Integer = 0 To dtJadwal.Rows.Count - 1
                    With dtJadwal.Rows(n)
                        nTotalBunga = nTotalBunga + CDbl(.Item("Bunga"))
                        cKeteranganTemp = "Jadwal Angsuran Ke " & n
                        UpdAngsuranPembiayaan(cKodeCabang, cFaktur, dTgl.DateTime, cRekening.Text, eAngsuran.ags_jadwal,
                                              CDbl(.Item("Pokok")), CDbl(.Item("Bunga")), 0, cKeteranganTemp, False, False, , , , "")
                    End With
                Next
                vaField = {"pembulatan", "lama", "sukuBunga", "SukuBungaBerikutnya",
                           "totalbunga", "GracePeriodePokok", "GracePeriodeBunga"}
                vaValue = {CDbl(nPembulatan.Text), CDbl(nLama.Text), CDbl(nSukuBunga.Text), CDbl(nSukuBungaBerikutnya.Text),
                           nTotalBunga, CDbl(nGracePeriodePokok.Text), CDbl(nGracePeriodeBunga.Text)}
                cWhere = String.Format("rekening='{0}'", cRekening.Text)
                objData.Edit(GetDSN, "debitur", cWhere, vaField, vaValue)
                MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                InitValue()
                GetSQL()
            End If
        End If
    End Sub

    Private Sub trUpdateJadwalKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trUpdateJadwalKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nSukuBunga.TabIndex, n)
        SetTabIndex(nSukuBungaBerikutnya.TabIndex, n)
        SetTabIndex(nGracePeriodePokok.TabIndex, n)
        SetTabIndex(nGracePeriodeBunga.TabIndex, n)
        SetTabIndex(nPembulatan.TabIndex, n)
        SetTabIndex(optJadwal.TabIndex, n)
        SetTabIndex(nPokokPenyesuaian.TabIndex, n)
        SetTabIndex(nBungaPenyesuaian.TabIndex, n)
        SetTabIndex(cmdOK.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nSukuBunga.EnterMoveNextControl = True
        nSukuBungaBerikutnya.EnterMoveNextControl = True
        nGracePeriodePokok.EnterMoveNextControl = True
        nGracePeriodeBunga.EnterMoveNextControl = True
        nPembulatan.EnterMoveNextControl = True
        optJadwal.EnterMoveNextControl = True
        nPokokPenyesuaian.EnterMoveNextControl = True
        nBungaPenyesuaian.EnterMoveNextControl = True

        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSukuBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSukuBungaBerikutnya, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nGracePeriodePokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nGracePeriodeBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPembulatan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPokokPenyesuaian, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBungaPenyesuaian, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cField As String = "d.kode,d.plafond,d.tgl,d.pembulatan,d.lama,d.TotalBunga,d.GracePeriodePokok,d.GracePeriodeBunga,"
            cField = cField & "d.sukubunga,d.sukubungaberikutnya,r.nama,r.alamat,d.faktur,d.caraperhitungan,"
            cField = cField & "c.keterangan as NamaCaraPerhitungan"
            Dim vaJoin() As Object = {"Left Join registernasabah r on r.kode = d.kode",
                                      "Left Join CaraHitung c on c.kode = d.CaraPerhitungan"}
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", , cRekening.Text, , , vaJoin)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cKode.Text = .Item("Kode").ToString
                    dTgl.DateTime = CDate(.Item("Tgl"))
                    nPlafond.Text = .Item("Plafond").ToString
                    nLama.Text = .Item("Lama").ToString
                    nSukuBunga.Text = .Item("SukuBunga").ToString
                    nSukuBungaBerikutnya.Text = .Item("SukuBungaBerikutnya").ToString
                    nGracePeriodePokok.Text = .Item("GracePeriodePokok").ToString
                    nGracePeriodeBunga.Text = .Item("GracePeriodeBunga").ToString
                    nPembulatan.Text = .Item("Pembulatan").ToString
                    cFaktur = .Item("Faktur").ToString
                    cCaraPerhitungan.Text = .Item("CaraPerhitungan").ToString
                    lblCaraHitung.Text = .Item("NamaCaraPerhitungan").ToString
                    nTotalBunga = CDbl(.Item("TotalBunga"))
                    GetSQL()
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        IsiJadwalPenyesuaian()
    End Sub
End Class