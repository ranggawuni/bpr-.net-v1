﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trCetakSuratPerjanjian
    ReadOnly objData As New data()
    Dim cBulan As String
    Dim cJaminan() As String
    Dim cJaminanTanah() As String
    Dim cJaminanKendaraan() As String
    Dim cJaminanDeposito() As String
    Dim cJaminanPotongGaji() As String
    Dim lEmpty As Boolean
    Dim dTglTemp As Date
    Dim lOldRekening As Boolean
    Dim cOldJenis As String
    Dim vaJaminan As New DataTable
    Dim vaArray As New DataTable
    Dim vaInput As New DataTable
    Dim lEmptyHarga As Boolean
    Dim nGracePeriodPokok As Double

    Private Sub InitValue()
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cNoSPK.Text = ""
        cCaraPerhitungan.EditValue = ""
        cNamaCaraPerhitungan.Text = ""
        cKode.EditValue = ""
        cKeterangan.Text = ""
        dTglCetak.DateTime = Date.Today
    End Sub

    Private Sub trCetakSuratPerjanjian_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
        GetDataLookup("suratperjanjian", cKode, , "kode,file")
    End Sub

    Private Sub trCetakSuratPerjanjian_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , , cmdKeluar, , , , , , , , cmdCetak)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cCaraPerhitungan.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(dTglCetak.TabIndex, n)
        SetTabIndex(cmdCetak.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        cCaraPerhitungan.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        dTglCetak.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Dim cField As String = "d.kode,r.nama,r.alamat,d.nospk,d.CaraPerhitungan,d.NoPengajuan,p.Jaminan,d.graceperiodepokok,"
                cField = cField & "c.keterangan as NamaCaraHitung"
                Dim vaJoin() As Object = {"left join registernasabah r on d.kode=r.kode",
                                          "Left join Pengajuankredit p on p.rekening=d.nopengajuan",
                                          "Left Join CaraHitung c on c.kode = d.CaraPerhitungan"}
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur d", cField, "rekening", , cRekening.Text, , , vaJoin)
                If cDataTable.Rows.Count > 0 Then
                    With cDataTable.Rows(0)
                        nGracePeriodPokok = CDbl(.Item("GracePeriodePokok"))
                        cNama.Text = .Item("Nama").ToString
                        cAlamat.Text = .Item("Alamat").ToString
                        cCaraPerhitungan.Text = .Item("CaraPerhitungan").ToString
                        cNamaCaraPerhitungan.Text = .Item("NamaCaraHitung").ToString

                    End With
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.Text = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

    End Function


    Private Sub cKode_Closed(sender As Object, e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKode.Closed
        GetKeteranganLookUp(cKode, cKeterangan, , "file")
    End Sub

    Private Sub cKode_KeyDown(sender As Object, e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cKode, cKeterangan, , "file")
        End If
    End Sub
End Class