﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trAngsuranKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
                If xArray IsNot Nothing Then
                    xArray.Dispose()
                    xArray = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trAngsuranKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem5 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem6 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningLama = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.optKas = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nSisaPembayaranTotal = New DevExpress.XtraEditors.TextEdit()
        Me.nSisaPembayaranDenda = New DevExpress.XtraEditors.TextEdit()
        Me.nSisaPembayaranBunga = New DevExpress.XtraEditors.TextEdit()
        Me.nPotonganTotal = New DevExpress.XtraEditors.TextEdit()
        Me.nPotonganDenda = New DevExpress.XtraEditors.TextEdit()
        Me.nPembulatanBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.nSisaPembayaranPokok = New DevExpress.XtraEditors.TextEdit()
        Me.nTotalBayar = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.nPotonganBunga = New DevExpress.XtraEditors.TextEdit()
        Me.nDenda = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.nPokok = New DevExpress.XtraEditors.TextEdit()
        Me.nPembulatanPokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdDenda = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdCekList = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdJadwal = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKartu = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cNamaGolonganKredit = New DevExpress.XtraEditors.TextEdit()
        Me.nSaldoTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.cGolonganKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lblJthTmp = New DevExpress.XtraEditors.LabelControl()
        Me.dJthTmp = New DevExpress.XtraEditors.DateEdit()
        Me.nTunggakanBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.nTunggakanPokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.nBakiDebet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.lblKolek = New DevExpress.XtraEditors.LabelControl()
        Me.nKolektibilitas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.nBungaPelunasan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaPembayaranTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaPembayaranDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaPembayaranBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPotonganTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPotonganDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPembulatanBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaPembayaranPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotalBayar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPotonganBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPembulatanPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTunggakanBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTunggakanPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBakiDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nKolektibilitas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBungaPelunasan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cFaktur)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cRekeningLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(1, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(925, 134)
        Me.PanelControl1.TabIndex = 21
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(104, 105)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(227, 20)
        Me.cFaktur.TabIndex = 134
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 109)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 133
        Me.LabelControl15.Text = "No. Faktur"
        '
        'cRekeningLama
        '
        Me.cRekeningLama.Location = New System.Drawing.Point(312, 9)
        Me.cRekeningLama.Name = "cRekeningLama"
        Me.cRekeningLama.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLama.Size = New System.Drawing.Size(158, 20)
        Me.cRekeningLama.TabIndex = 131
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(217, 13)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl10.TabIndex = 130
        Me.LabelControl10.Text = "Rekening Lama"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 33)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 81)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(366, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 84)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 37)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 57)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 60)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cKeterangan)
        Me.PanelControl2.Controls.Add(Me.LabelControl26)
        Me.PanelControl2.Controls.Add(Me.LabelControl20)
        Me.PanelControl2.Controls.Add(Me.optKas)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nSisaPembayaranTotal)
        Me.PanelControl2.Controls.Add(Me.nSisaPembayaranDenda)
        Me.PanelControl2.Controls.Add(Me.nSisaPembayaranBunga)
        Me.PanelControl2.Controls.Add(Me.nPotonganTotal)
        Me.PanelControl2.Controls.Add(Me.nPotonganDenda)
        Me.PanelControl2.Controls.Add(Me.nPembulatanBunga)
        Me.PanelControl2.Controls.Add(Me.LabelControl35)
        Me.PanelControl2.Controls.Add(Me.LabelControl34)
        Me.PanelControl2.Controls.Add(Me.LabelControl33)
        Me.PanelControl2.Controls.Add(Me.LabelControl32)
        Me.PanelControl2.Controls.Add(Me.LabelControl31)
        Me.PanelControl2.Controls.Add(Me.nSisaPembayaranPokok)
        Me.PanelControl2.Controls.Add(Me.nTotalBayar)
        Me.PanelControl2.Controls.Add(Me.LabelControl16)
        Me.PanelControl2.Controls.Add(Me.nPotonganBunga)
        Me.PanelControl2.Controls.Add(Me.nDenda)
        Me.PanelControl2.Controls.Add(Me.LabelControl11)
        Me.PanelControl2.Controls.Add(Me.nBunga)
        Me.PanelControl2.Controls.Add(Me.nTotal)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.nPokok)
        Me.PanelControl2.Controls.Add(Me.nPembulatanPokok)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.ShapeContainer1)
        Me.PanelControl2.Location = New System.Drawing.Point(1, 140)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(633, 255)
        Me.PanelControl2.TabIndex = 26
        '
        'cKeterangan
        '
        Me.cKeterangan.Enabled = False
        Me.cKeterangan.Location = New System.Drawing.Point(109, 225)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(512, 20)
        Me.cKeterangan.TabIndex = 241
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(15, 228)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl26.TabIndex = 240
        Me.LabelControl26.Text = "Keterangan"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(15, 198)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl20.TabIndex = 239
        Me.LabelControl20.Text = "Jumlah Hari"
        '
        'optKas
        '
        Me.optKas.Location = New System.Drawing.Point(110, 190)
        Me.optKas.Name = "optKas"
        Me.optKas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Kas / Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Pemindah Bukuan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tabungan")})
        Me.optKas.Size = New System.Drawing.Size(354, 29)
        Me.optKas.TabIndex = 238
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(15, 118)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl2.TabIndex = 236
        Me.LabelControl2.Text = "Total"
        '
        'nSisaPembayaranTotal
        '
        Me.nSisaPembayaranTotal.Enabled = False
        Me.nSisaPembayaranTotal.Location = New System.Drawing.Point(473, 115)
        Me.nSisaPembayaranTotal.Name = "nSisaPembayaranTotal"
        Me.nSisaPembayaranTotal.Size = New System.Drawing.Size(148, 20)
        Me.nSisaPembayaranTotal.TabIndex = 235
        '
        'nSisaPembayaranDenda
        '
        Me.nSisaPembayaranDenda.Enabled = False
        Me.nSisaPembayaranDenda.Location = New System.Drawing.Point(473, 91)
        Me.nSisaPembayaranDenda.Name = "nSisaPembayaranDenda"
        Me.nSisaPembayaranDenda.Size = New System.Drawing.Size(148, 20)
        Me.nSisaPembayaranDenda.TabIndex = 234
        '
        'nSisaPembayaranBunga
        '
        Me.nSisaPembayaranBunga.Enabled = False
        Me.nSisaPembayaranBunga.Location = New System.Drawing.Point(473, 68)
        Me.nSisaPembayaranBunga.Name = "nSisaPembayaranBunga"
        Me.nSisaPembayaranBunga.Size = New System.Drawing.Size(148, 20)
        Me.nSisaPembayaranBunga.TabIndex = 233
        '
        'nPotonganTotal
        '
        Me.nPotonganTotal.Enabled = False
        Me.nPotonganTotal.Location = New System.Drawing.Point(365, 115)
        Me.nPotonganTotal.Name = "nPotonganTotal"
        Me.nPotonganTotal.Properties.Mask.EditMask = "n"
        Me.nPotonganTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nPotonganTotal.Size = New System.Drawing.Size(99, 20)
        Me.nPotonganTotal.TabIndex = 232
        '
        'nPotonganDenda
        '
        Me.nPotonganDenda.Enabled = False
        Me.nPotonganDenda.Location = New System.Drawing.Point(365, 91)
        Me.nPotonganDenda.Name = "nPotonganDenda"
        Me.nPotonganDenda.Properties.Mask.EditMask = "n"
        Me.nPotonganDenda.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nPotonganDenda.Size = New System.Drawing.Size(99, 20)
        Me.nPotonganDenda.TabIndex = 231
        '
        'nPembulatanBunga
        '
        Me.nPembulatanBunga.Enabled = False
        Me.nPembulatanBunga.Location = New System.Drawing.Point(266, 68)
        Me.nPembulatanBunga.Name = "nPembulatanBunga"
        Me.nPembulatanBunga.Size = New System.Drawing.Size(88, 20)
        Me.nPembulatanBunga.TabIndex = 230
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.BackColor = System.Drawing.Color.CornflowerBlue
        Me.LabelControl35.Appearance.BorderColor = System.Drawing.Color.Black
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl35.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LabelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.LabelControl35.Location = New System.Drawing.Point(468, 5)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(158, 30)
        Me.LabelControl35.TabIndex = 229
        Me.LabelControl35.Text = "Sisa Pembayaran"
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.BackColor = System.Drawing.Color.CornflowerBlue
        Me.LabelControl34.Appearance.BorderColor = System.Drawing.Color.Black
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl34.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LabelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl34.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.LabelControl34.Location = New System.Drawing.Point(358, 5)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(112, 30)
        Me.LabelControl34.TabIndex = 228
        Me.LabelControl34.Text = "Potongan"
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.BackColor = System.Drawing.Color.CornflowerBlue
        Me.LabelControl33.Appearance.BorderColor = System.Drawing.Color.Black
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl33.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LabelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.LabelControl33.Location = New System.Drawing.Point(260, 5)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(101, 30)
        Me.LabelControl33.TabIndex = 227
        Me.LabelControl33.Text = "Pembulatan"
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.BackColor = System.Drawing.Color.CornflowerBlue
        Me.LabelControl32.Appearance.BorderColor = System.Drawing.Color.Black
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl32.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LabelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl32.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.LabelControl32.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(101, 30)
        Me.LabelControl32.TabIndex = 226
        Me.LabelControl32.Text = "Keterangan"
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.BackColor = System.Drawing.Color.CornflowerBlue
        Me.LabelControl31.Appearance.BorderColor = System.Drawing.Color.Black
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl31.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LabelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat
        Me.LabelControl31.Location = New System.Drawing.Point(104, 5)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(158, 30)
        Me.LabelControl31.TabIndex = 225
        Me.LabelControl31.Text = "Pembayaran"
        '
        'nSisaPembayaranPokok
        '
        Me.nSisaPembayaranPokok.Enabled = False
        Me.nSisaPembayaranPokok.Location = New System.Drawing.Point(473, 45)
        Me.nSisaPembayaranPokok.Name = "nSisaPembayaranPokok"
        Me.nSisaPembayaranPokok.Size = New System.Drawing.Size(148, 20)
        Me.nSisaPembayaranPokok.TabIndex = 224
        '
        'nTotalBayar
        '
        Me.nTotalBayar.Enabled = False
        Me.nTotalBayar.Location = New System.Drawing.Point(110, 152)
        Me.nTotalBayar.Name = "nTotalBayar"
        Me.nTotalBayar.Properties.Mask.EditMask = "n"
        Me.nTotalBayar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nTotalBayar.Size = New System.Drawing.Size(146, 20)
        Me.nTotalBayar.TabIndex = 149
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(15, 155)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl16.TabIndex = 148
        Me.LabelControl16.Text = "Total Bayar"
        '
        'nPotonganBunga
        '
        Me.nPotonganBunga.Enabled = False
        Me.nPotonganBunga.Location = New System.Drawing.Point(365, 68)
        Me.nPotonganBunga.Name = "nPotonganBunga"
        Me.nPotonganBunga.Properties.Mask.EditMask = "n"
        Me.nPotonganBunga.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nPotonganBunga.Size = New System.Drawing.Size(99, 20)
        Me.nPotonganBunga.TabIndex = 146
        '
        'nDenda
        '
        Me.nDenda.Enabled = False
        Me.nDenda.Location = New System.Drawing.Point(110, 91)
        Me.nDenda.Name = "nDenda"
        Me.nDenda.Size = New System.Drawing.Size(146, 20)
        Me.nDenda.TabIndex = 144
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(15, 94)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl11.TabIndex = 143
        Me.LabelControl11.Text = "Denda"
        '
        'nBunga
        '
        Me.nBunga.Enabled = False
        Me.nBunga.Location = New System.Drawing.Point(110, 68)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Properties.Mask.EditMask = "n"
        Me.nBunga.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nBunga.Size = New System.Drawing.Size(146, 20)
        Me.nBunga.TabIndex = 142
        '
        'nTotal
        '
        Me.nTotal.Enabled = False
        Me.nTotal.Location = New System.Drawing.Point(110, 115)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(146, 20)
        Me.nTotal.TabIndex = 141
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(15, 71)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl1.TabIndex = 140
        Me.LabelControl1.Text = "Bunga"
        '
        'nPokok
        '
        Me.nPokok.Enabled = False
        Me.nPokok.Location = New System.Drawing.Point(110, 45)
        Me.nPokok.Name = "nPokok"
        Me.nPokok.Properties.Mask.EditMask = "n"
        Me.nPokok.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nPokok.Size = New System.Drawing.Size(146, 20)
        Me.nPokok.TabIndex = 139
        '
        'nPembulatanPokok
        '
        Me.nPembulatanPokok.Enabled = False
        Me.nPembulatanPokok.Location = New System.Drawing.Point(266, 45)
        Me.nPembulatanPokok.Name = "nPembulatanPokok"
        Me.nPembulatanPokok.Size = New System.Drawing.Size(88, 20)
        Me.nPembulatanPokok.TabIndex = 138
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(15, 48)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl8.TabIndex = 137
        Me.LabelControl8.Text = "Pokok"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(629, 251)
        Me.ShapeContainer1.TabIndex = 237
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderWidth = 2
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 4
        Me.LineShape1.X2 = 623
        Me.LineShape1.Y1 = 140
        Me.LineShape1.Y2 = 140
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BorderWidth = 2
        Me.RectangleShape1.Location = New System.Drawing.Point(4, 32)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(619, 146)
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdDenda)
        Me.PanelControl4.Controls.Add(Me.cmdCekList)
        Me.PanelControl4.Controls.Add(Me.cmdJadwal)
        Me.PanelControl4.Controls.Add(Me.cmdKartu)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(1, 400)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(925, 33)
        Me.PanelControl4.TabIndex = 25
        '
        'cmdDenda
        '
        Me.cmdDenda.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdDenda.Location = New System.Drawing.Point(354, 5)
        Me.cmdDenda.Name = "cmdDenda"
        Me.cmdDenda.Size = New System.Drawing.Size(109, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdDenda.SuperTip = SuperToolTip1
        Me.cmdDenda.TabIndex = 13
        Me.cmdDenda.Text = "&Denda"
        '
        'cmdCekList
        '
        Me.cmdCekList.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCekList.Location = New System.Drawing.Point(239, 5)
        Me.cmdCekList.Name = "cmdCekList"
        Me.cmdCekList.Size = New System.Drawing.Size(109, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdCekList.SuperTip = SuperToolTip2
        Me.cmdCekList.TabIndex = 12
        Me.cmdCekList.Text = "&Cek List Angsuran"
        '
        'cmdJadwal
        '
        Me.cmdJadwal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdJadwal.Location = New System.Drawing.Point(124, 5)
        Me.cmdJadwal.Name = "cmdJadwal"
        Me.cmdJadwal.Size = New System.Drawing.Size(109, 23)
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image2"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Batal / Keluar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.cmdJadwal.SuperTip = SuperToolTip3
        Me.cmdJadwal.TabIndex = 11
        Me.cmdJadwal.Text = "&Jadwal Angsuran"
        '
        'cmdKartu
        '
        Me.cmdKartu.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKartu.Location = New System.Drawing.Point(9, 5)
        Me.cmdKartu.Name = "cmdKartu"
        Me.cmdKartu.Size = New System.Drawing.Size(109, 23)
        ToolTipTitleItem4.Appearance.Image = CType(resources.GetObject("resource.Image3"), System.Drawing.Image)
        ToolTipTitleItem4.Appearance.Options.UseImage = True
        ToolTipTitleItem4.Image = CType(resources.GetObject("ToolTipTitleItem4.Image"), System.Drawing.Image)
        ToolTipTitleItem4.Text = "Preview Data"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.cmdKartu.SuperTip = SuperToolTip4
        Me.cmdKartu.TabIndex = 10
        Me.cmdKartu.Text = "&Kartu Angsuran"
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(840, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem5.Appearance.Image = CType(resources.GetObject("resource.Image4"), System.Drawing.Image)
        ToolTipTitleItem5.Appearance.Options.UseImage = True
        ToolTipTitleItem5.Image = CType(resources.GetObject("ToolTipTitleItem5.Image"), System.Drawing.Image)
        ToolTipTitleItem5.Text = "Batal / Keluar"
        ToolTipItem5.LeftIndent = 6
        ToolTipItem5.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        SuperToolTip5.Items.Add(ToolTipItem5)
        Me.cmdKeluar.SuperTip = SuperToolTip5
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(759, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem6.Appearance.Image = CType(resources.GetObject("resource.Image5"), System.Drawing.Image)
        ToolTipTitleItem6.Appearance.Options.UseImage = True
        ToolTipTitleItem6.Image = CType(resources.GetObject("ToolTipTitleItem6.Image"), System.Drawing.Image)
        ToolTipTitleItem6.Text = "Preview Data"
        ToolTipItem6.LeftIndent = 6
        ToolTipItem6.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        SuperToolTip6.Items.Add(ToolTipItem6)
        Me.cmdSimpan.SuperTip = SuperToolTip6
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cNamaGolonganKredit)
        Me.PanelControl3.Controls.Add(Me.nSaldoTabungan)
        Me.PanelControl3.Controls.Add(Me.cGolonganKredit)
        Me.PanelControl3.Controls.Add(Me.LabelControl29)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.lblJthTmp)
        Me.PanelControl3.Controls.Add(Me.dJthTmp)
        Me.PanelControl3.Controls.Add(Me.nTunggakanBunga)
        Me.PanelControl3.Controls.Add(Me.LabelControl25)
        Me.PanelControl3.Controls.Add(Me.nTunggakanPokok)
        Me.PanelControl3.Controls.Add(Me.LabelControl24)
        Me.PanelControl3.Controls.Add(Me.nBakiDebet)
        Me.PanelControl3.Controls.Add(Me.LabelControl23)
        Me.PanelControl3.Controls.Add(Me.LabelControl22)
        Me.PanelControl3.Controls.Add(Me.lblKolek)
        Me.PanelControl3.Controls.Add(Me.nKolektibilitas)
        Me.PanelControl3.Controls.Add(Me.LabelControl27)
        Me.PanelControl3.Controls.Add(Me.nBungaPelunasan)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Location = New System.Drawing.Point(638, 140)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(288, 255)
        Me.PanelControl3.TabIndex = 24
        '
        'cNamaGolonganKredit
        '
        Me.cNamaGolonganKredit.Enabled = False
        Me.cNamaGolonganKredit.Location = New System.Drawing.Point(142, 195)
        Me.cNamaGolonganKredit.Name = "cNamaGolonganKredit"
        Me.cNamaGolonganKredit.Size = New System.Drawing.Size(136, 20)
        Me.cNamaGolonganKredit.TabIndex = 226
        '
        'nSaldoTabungan
        '
        Me.nSaldoTabungan.Enabled = False
        Me.nSaldoTabungan.Location = New System.Drawing.Point(104, 171)
        Me.nSaldoTabungan.Name = "nSaldoTabungan"
        Me.nSaldoTabungan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoTabungan.TabIndex = 228
        '
        'cGolonganKredit
        '
        Me.cGolonganKredit.Enabled = False
        Me.cGolonganKredit.Location = New System.Drawing.Point(104, 195)
        Me.cGolonganKredit.Name = "cGolonganKredit"
        Me.cGolonganKredit.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganKredit.TabIndex = 225
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(9, 198)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl29.TabIndex = 224
        Me.LabelControl29.Text = "Golongan Kredit"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 174)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl5.TabIndex = 227
        Me.LabelControl5.Text = "Saldo Tabungan"
        '
        'lblJthTmp
        '
        Me.lblJthTmp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblJthTmp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJthTmp.Location = New System.Drawing.Point(192, 6)
        Me.lblJthTmp.Name = "lblJthTmp"
        Me.lblJthTmp.Size = New System.Drawing.Size(87, 20)
        Me.lblJthTmp.TabIndex = 222
        Me.lblJthTmp.Text = "Hari"
        '
        'dJthTmp
        '
        Me.dJthTmp.EditValue = Nothing
        Me.dJthTmp.Location = New System.Drawing.Point(104, 6)
        Me.dJthTmp.Name = "dJthTmp"
        Me.dJthTmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJthTmp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJthTmp.Size = New System.Drawing.Size(82, 20)
        Me.dJthTmp.TabIndex = 221
        '
        'nTunggakanBunga
        '
        Me.nTunggakanBunga.Enabled = False
        Me.nTunggakanBunga.Location = New System.Drawing.Point(104, 101)
        Me.nTunggakanBunga.Name = "nTunggakanBunga"
        Me.nTunggakanBunga.Size = New System.Drawing.Size(121, 20)
        Me.nTunggakanBunga.TabIndex = 214
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(9, 104)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl25.TabIndex = 213
        Me.LabelControl25.Text = "Tunggakan Bunga"
        '
        'nTunggakanPokok
        '
        Me.nTunggakanPokok.Enabled = False
        Me.nTunggakanPokok.Location = New System.Drawing.Point(104, 77)
        Me.nTunggakanPokok.Name = "nTunggakanPokok"
        Me.nTunggakanPokok.Size = New System.Drawing.Size(121, 20)
        Me.nTunggakanPokok.TabIndex = 212
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(9, 80)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl24.TabIndex = 211
        Me.LabelControl24.Text = "Tunggakan Pokok"
        '
        'nBakiDebet
        '
        Me.nBakiDebet.Enabled = False
        Me.nBakiDebet.Location = New System.Drawing.Point(104, 29)
        Me.nBakiDebet.Name = "nBakiDebet"
        Me.nBakiDebet.Size = New System.Drawing.Size(121, 20)
        Me.nBakiDebet.TabIndex = 210
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(9, 32)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl23.TabIndex = 209
        Me.LabelControl23.Text = "Baki Debet"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(9, 9)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl22.TabIndex = 207
        Me.LabelControl22.Text = "Jatuh Tempo"
        '
        'lblKolek
        '
        Me.lblKolek.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblKolek.Location = New System.Drawing.Point(143, 128)
        Me.lblKolek.Name = "lblKolek"
        Me.lblKolek.Size = New System.Drawing.Size(19, 13)
        Me.lblKolek.TabIndex = 203
        Me.lblKolek.Text = "Hari"
        '
        'nKolektibilitas
        '
        Me.nKolektibilitas.Enabled = False
        Me.nKolektibilitas.Location = New System.Drawing.Point(104, 124)
        Me.nKolektibilitas.Name = "nKolektibilitas"
        Me.nKolektibilitas.Size = New System.Drawing.Size(32, 20)
        Me.nKolektibilitas.TabIndex = 202
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(9, 127)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl27.TabIndex = 166
        Me.LabelControl27.Text = "Kolektibilitas"
        '
        'nBungaPelunasan
        '
        Me.nBungaPelunasan.Location = New System.Drawing.Point(104, 53)
        Me.nBungaPelunasan.Name = "nBungaPelunasan"
        Me.nBungaPelunasan.Size = New System.Drawing.Size(121, 20)
        Me.nBungaPelunasan.TabIndex = 130
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 56)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl9.TabIndex = 129
        Me.LabelControl9.Text = "Bunga Lunas"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(104, 147)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 125
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 150)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl6.TabIndex = 124
        Me.LabelControl6.Text = "Rek. Tabungan"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(1, 436)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(925, 135)
        Me.GridControl1.TabIndex = 27
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'trAngsuranKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 576)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trAngsuranKredit"
        Me.Text = "Angsuran Kredit"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaPembayaranTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaPembayaranDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaPembayaranBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPotonganTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPotonganDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPembulatanBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaPembayaranPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotalBayar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPotonganBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPembulatanPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTunggakanBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTunggakanPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBakiDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nKolektibilitas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBungaPelunasan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningLama As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nSisaPembayaranPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nTotalBayar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPotonganBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nDenda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPembulatanPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nTunggakanBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTunggakanPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBakiDebet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblKolek As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nKolektibilitas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBungaPelunasan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSisaPembayaranTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nSisaPembayaranDenda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nSisaPembayaranBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPotonganTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPotonganDenda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPembulatanBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optKas As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents cNamaGolonganKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nSaldoTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cGolonganKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblJthTmp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJthTmp As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents cmdDenda As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCekList As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdJadwal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKartu As DevExpress.XtraEditors.SimpleButton
End Class
