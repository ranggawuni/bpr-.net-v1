﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trPembatalanPencairanKredit
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Private ReadOnly x As TypeDeposito

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nPlafond.Text = ""
        nPencairanPokok.Text = ""
        nAdministrasi.Text = ""
        nNotaris.Text = ""
        nMaterai.Text = ""
        nAsuransi.Text = ""
        nProvisi.Text = ""
        nTotal.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        nSaldoTabungan.Text = ""
        cKunci.Text = ""
    End Sub

    Private Sub trPembatalanPencairanKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trPembatalanPencairanKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cKunci.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cKunci.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPencairanPokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNotaris, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nMaterai, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAsuransi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nProvisi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "statuspencairan,plafond"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    Dim lCair As Boolean = .Item("StatusPencairan").ToString <> "0"
                    If lCair Then
                        GetMemory()
                    Else
                        If dTgl.DateTime < CDate(.Item("Tgl")) Then
                            MessageBox.Show("Tanggal Realisasi Tidak Sama Dengan Tanggal Transaksi, Tidak Bisa Dibatalkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Else
                            Dim nBakiDebet As Double = GetBakiDebet(cRekening.Text, dTgl.DateTime)
                            If nBakiDebet < CDbl(.Item("Plafond")) Then
                                MessageBox.Show("Kredit Sudah Diangsur, Tidak Bisa Dibatalkan." & vbCrLf & "Hapus Mutasi Angsuran Terlebih Dahulu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Else
                                MessageBox.Show("Kredit Belum Dicairkan, Tidak Bisa Dibatalkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            End If
                        End If
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                    If CDate(.Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub GetMemory()
        Dim cField As String = "d.faktur,d.plafond,d.Administrasi,d.Notaris,d.Materai,d.Asuransi,d.Provisi,d.RekeningTabungan,"
        cField = cField & "d.caraperhitungan,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,rn.Nama as NamaNasabah"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join Tabungan t on d.RekeningTabungan = t.Rekening",
                                  "Left Join RegisterNasabah rn on rn.Kode = t.Kode"}
        dtData = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", , cRekening.Text, , , vaJoin)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cFaktur.Text = .Item("faktur").ToString
                cNama.Text = .Item("Nama").ToString
                cAlamat.Text = .Item("Alamat").ToString
                nPlafond.Text = .Item("Plafond").ToString
                nAdministrasi.Text = .Item("Administrasi").ToString
                nNotaris.Text = .Item("Notaris").ToString
                nMaterai.Text = .Item("Materai").ToString
                nAsuransi.Text = .Item("Asuransi").ToString
                nProvisi.Text = .Item("Provisi").ToString
                If .Item("CaraPencairan").ToString = "1" Or .Item("CaraPencairan").ToString = "3" Then
                    nPencairanPokok.Text = nPlafond.Text
                End If
                cRekeningTabungan.Text = x.cRekeningTabungan
                cNamaNasabah.Text = x.cNamaNasabahTabungan
                nSaldoTabungan.Text = GetSaldoTabungan(cRekening.Text, dTgl.DateTime).ToString
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim db As New DataTable
        ValidSaving = True

        Dim cWhere As String = String.Format("and (status = {0} or status = {1})", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
        db = objData.Browse(GetDSN, "angsuran", "rekening,kpokok,kbunga", "rekening", , cRekening.Text, cWhere)
        If db.Rows.Count > 0 Then
            MsgBox("Kredit Sudah Diangsur, Tidak Bisa Dibatalkan." & vbCrLf & "Hapus Mutasi Angsuran Terlebih Dahulu.", vbExclamation)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If cKunci.Text <> "PEMBATALAN PENCAIRAN KREDIT" Then
            MsgBox("Kata Kunci Salah, Transaksi Tidak Bisa di Lanjutkan ..!", vbExclamation)
            ValidSaving = False
            cKunci.Focus()
            cKunci.Text = ""
            Exit Function
        End If

        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        db = objData.Browse(GetDSN, "Debitur", "StatusPencairan", "Rekening", , cRekening.Text)
        If db.Rows.Count > 0 Then
            If db.Rows(0).Item("statuspencairan").ToString = "0" Then
                MessageBox.Show("Kredit Belum Dicairkan, Transaksi Tidak bisa di lanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        Else
            MessageBox.Show("Rekening Tidak Ditemukan, Transaksi tidak bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
        db.Dispose()
    End Function

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        simpanData()
    End Sub

    Private Sub simpanData()
        If ValidSaving() Then
            DelPencairanDeposito(cRekening.Text, dTgl.DateTime)
            InitValue()
            cRekening.Focus()
        End If
        MessageBox.Show("Pembatalan selesai.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class