﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trKoreksiPerpanjanganDeposito
    ReadOnly objData As New data()
    Dim dtData As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "m.faktur,m.Rekening,r.nama,m.tgl,m.jthtmp"
            Dim cWhere As String = String.Format(" and tgl = '{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "mutasideposito", cField, "Kas", , "E", cWhere, "rekening")
            GridControl1.DataSource = cDataSet.Tables("mutasideposito")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "Tgl", , , , , True)
            GridColumnFormat(GridView1, "JthTmp", , , , , True)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                nRow = GridView1.GetDataRow(n)
                If nRow(0).ToString <> "" Then
                    cWhere = String.Format("faktur = '{0}' and rekening = '{1}'", nRow(0).ToString, nRow(1).ToString)
                    vaField = {"Tgl", "JthTmp"}
                    vaValue = {nRow(2).ToString, nRow(3).ToString}
                    objData.Edit(GetDSN, "mutasideposito", cWhere, vaField, vaValue)
                End If
            Next
            MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trKoreksiPerpanjanganDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, , cmdKeluar, , , , , cmdPerNasabah, , , cmdRefresh)
        SetButtonPicture(, , , , , , , , , , , cmdJadwal)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdPerNasabah.TabIndex, n)
        SetTabIndex(cmdJadwal.TabIndex, n)

        dTgl.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdPerNasabah_Click(sender As Object, e As EventArgs) Handles cmdPerNasabah.Click
        trKoreksiPerpanjanganDepositoPerNasabah.MdiParent = aMainMenu
        trKoreksiPerpanjanganDepositoPerNasabah.Show()
    End Sub

    Private Sub cmdJadwal_Click(sender As Object, e As EventArgs) Handles cmdJadwal.Click
        trKoreksiJadwalBungaDeposito.MdiParent = aMainMenu
        trKoreksiJadwalBungaDeposito.Show()
    End Sub
End Class