﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trSetorDeposito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trSetorDeposito))
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.optKas = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.optDeposan = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.dJthTmp = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.cKeteranganGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.nNominal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeteranganGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(517, 118)
        Me.PanelControl1.TabIndex = 16
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 9)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 87)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 13)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 61)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 38)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 35)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.optKas)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.optDeposan)
        Me.PanelControl3.Controls.Add(Me.LabelControl27)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Controls.Add(Me.dJthTmp)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.cGolonganDeposito)
        Me.PanelControl3.Controls.Add(Me.cKeteranganGolonganDeposito)
        Me.PanelControl3.Controls.Add(Me.LabelControl1)
        Me.PanelControl3.Controls.Add(Me.cGolonganDeposan)
        Me.PanelControl3.Controls.Add(Me.cFaktur)
        Me.PanelControl3.Controls.Add(Me.LabelControl15)
        Me.PanelControl3.Controls.Add(Me.nNominal)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.nSaldoTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.nLama)
        Me.PanelControl3.Controls.Add(Me.LabelControl7)
        Me.PanelControl3.Controls.Add(Me.cKeteranganGolonganDeposan)
        Me.PanelControl3.Controls.Add(Me.LabelControl8)
        Me.PanelControl3.Location = New System.Drawing.Point(2, 127)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(517, 290)
        Me.PanelControl3.TabIndex = 18
        '
        'optKas
        '
        Me.optKas.Location = New System.Drawing.Point(105, 254)
        Me.optKas.Name = "optKas"
        Me.optKas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. PB")})
        Me.optKas.Size = New System.Drawing.Size(269, 29)
        Me.optKas.TabIndex = 200
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(10, 262)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl4.TabIndex = 199
        Me.LabelControl4.Text = "Cara Penyetoran"
        '
        'optDeposan
        '
        Me.optDeposan.Location = New System.Drawing.Point(105, 193)
        Me.optDeposan.Name = "optDeposan"
        Me.optDeposan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Non Instansi"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Instansi")})
        Me.optDeposan.Size = New System.Drawing.Size(177, 29)
        Me.optDeposan.TabIndex = 167
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(10, 201)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl27.TabIndex = 166
        Me.LabelControl27.Text = "Jenis Deposan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(10, 118)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl3.TabIndex = 139
        Me.LabelControl3.Text = "Jatuh Tempo"
        '
        'dJthTmp
        '
        Me.dJthTmp.EditValue = Nothing
        Me.dJthTmp.Location = New System.Drawing.Point(105, 115)
        Me.dJthTmp.Name = "dJthTmp"
        Me.dJthTmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJthTmp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJthTmp.Size = New System.Drawing.Size(82, 20)
        Me.dJthTmp.TabIndex = 138
        '
        'LabelControl2
        '
        Me.LabelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl2.Location = New System.Drawing.Point(143, 93)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl2.TabIndex = 137
        Me.LabelControl2.Text = "Bulan"
        '
        'cGolonganDeposito
        '
        Me.cGolonganDeposito.Enabled = False
        Me.cGolonganDeposito.Location = New System.Drawing.Point(105, 63)
        Me.cGolonganDeposito.Name = "cGolonganDeposito"
        Me.cGolonganDeposito.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposito.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposito.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposito.TabIndex = 136
        '
        'cKeteranganGolonganDeposito
        '
        Me.cKeteranganGolonganDeposito.Enabled = False
        Me.cKeteranganGolonganDeposito.Location = New System.Drawing.Point(143, 63)
        Me.cKeteranganGolonganDeposito.Name = "cKeteranganGolonganDeposito"
        Me.cKeteranganGolonganDeposito.Size = New System.Drawing.Size(311, 20)
        Me.cKeteranganGolonganDeposito.TabIndex = 135
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 66)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl1.TabIndex = 134
        Me.LabelControl1.Text = "Gol. Deposito"
        '
        'cGolonganDeposan
        '
        Me.cGolonganDeposan.Enabled = False
        Me.cGolonganDeposan.Location = New System.Drawing.Point(105, 37)
        Me.cGolonganDeposan.Name = "cGolonganDeposan"
        Me.cGolonganDeposan.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposan.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposan.TabIndex = 133
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(105, 11)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(227, 20)
        Me.cFaktur.TabIndex = 132
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(10, 15)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 131
        Me.LabelControl15.Text = "No. Faktur"
        '
        'nNominal
        '
        Me.nNominal.Location = New System.Drawing.Point(105, 228)
        Me.nNominal.Name = "nNominal"
        Me.nNominal.Size = New System.Drawing.Size(105, 20)
        Me.nNominal.TabIndex = 130
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(10, 231)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl9.TabIndex = 129
        Me.LabelControl9.Text = "Nominal"
        '
        'nSaldoTabungan
        '
        Me.nSaldoTabungan.Enabled = False
        Me.nSaldoTabungan.Location = New System.Drawing.Point(105, 167)
        Me.nSaldoTabungan.Name = "nSaldoTabungan"
        Me.nSaldoTabungan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoTabungan.TabIndex = 127
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(10, 171)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl5.TabIndex = 126
        Me.LabelControl5.Text = "Saldo Tabungan"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(105, 141)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 125
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(10, 145)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl6.TabIndex = 124
        Me.LabelControl6.Text = "Rek. Tabungan"
        '
        'nLama
        '
        Me.nLama.Enabled = False
        Me.nLama.Location = New System.Drawing.Point(105, 89)
        Me.nLama.Name = "nLama"
        Me.nLama.Properties.Mask.EditMask = "n"
        Me.nLama.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nLama.Size = New System.Drawing.Size(32, 20)
        Me.nLama.TabIndex = 123
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(10, 93)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl7.TabIndex = 122
        Me.LabelControl7.Text = "Jangka Waktu"
        '
        'cKeteranganGolonganDeposan
        '
        Me.cKeteranganGolonganDeposan.Enabled = False
        Me.cKeteranganGolonganDeposan.Location = New System.Drawing.Point(143, 37)
        Me.cKeteranganGolonganDeposan.Name = "cKeteranganGolonganDeposan"
        Me.cKeteranganGolonganDeposan.Size = New System.Drawing.Size(311, 20)
        Me.cKeteranganGolonganDeposan.TabIndex = 121
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(10, 40)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl8.TabIndex = 120
        Me.LabelControl8.Text = "Gol. Deposan"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(2, 423)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(517, 33)
        Me.PanelControl4.TabIndex = 19
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(434, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Batal / Keluar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.cmdKeluar.SuperTip = SuperToolTip3
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(353, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem4.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem4.Appearance.Options.UseImage = True
        ToolTipTitleItem4.Image = CType(resources.GetObject("ToolTipTitleItem4.Image"), System.Drawing.Image)
        ToolTipTitleItem4.Text = "Preview Data"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.cmdSimpan.SuperTip = SuperToolTip4
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'trSetorDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 459)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trSetorDeposito"
        Me.Text = "Penyetoran Deposito"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJthTmp As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeteranganGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNominal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeteranganGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optDeposan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optKas As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
End Class
