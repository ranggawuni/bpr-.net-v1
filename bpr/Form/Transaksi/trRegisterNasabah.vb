﻿Imports bpr.MySQL_Data_Library
Imports VB = Microsoft.VisualBasic
Imports DevExpress.XtraEditors

Public Class trRegisterNasabah
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim lKoreksi As Boolean
    ReadOnly RegNas As New IniTool()

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar)
        If lEdit Then
            If nPos = myPos.add Then
                cKode.Enabled = False
                'cKode.BackColor = BackColor
                GetRegister()
                cKTP.Focus()
            Else
                cKode.Enabled = True
                'cKode.BackColor = Color.White
                cKode.Focus()
            End If
        End If
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCabang.Text = CStr(aCfg(eCfg.msKodeCabang, "01"))
        cKode.Text = ""
        cKTP.Text = ""
        cNama.Text = ""
        cNamaIbu.Text = ""
        cTempatLahir.Text = ""
        dTglLahir.DateTime = Date.Today
        optJenisKelamin.SelectedIndex = 0
        optGolonganDarah.SelectedIndex = 0
        cAlamat.Text = ""
        cRTRW.Text = ""
        cKota.EditValue = ""
        cNamaKota.Text = ""
        cKecamatan.EditValue = ""
        cNamaKecamatan.Text = ""
        cKelurahan.EditValue = ""
        cNamaKelurahan.Text = ""
        cKodePos.Text = ""
        cAgama.EditValue = ""
        cNamaAgama.Text = ""
        optPerkawinan.SelectedIndex = 0
        cCabangInduk.Text = ""
        cKodeInduk.Text = ""
        cPekerjaan.EditValue = ""
        cNamaPekerjaan.Text = ""
        dTglKTP.DateTime = Date.Today
        cTelepon.Text = ""
        cFax.Text = ""
        cInstansi.EditValue = ""
        cNamaInstansi.Text = ""
        cNIP.Text = ""
        cNPWP.Text = ""
    End Sub

    Private Sub SeekKodePos()
        Const cField As String = "k.kode,k.keterangan,k.kecamatan,c.keterangan as NamaKecamatan,k.kodya,d.keterangan as namakota"
        Dim vaJoin() As String = {"Left Join Kecamatan c on k.kecamatan = c.Kode and k.kodya = c.kodya", _
                                  "Left Join Kodya k on k.kodya = d.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "kelurahan k", cField, "k.kodepos", data.myOperator.Assign, cKodePos.Text, , , vaJoin)
        With cDataTable
            If .Rows.Count > 0 Then
                cKota.Text = .Rows(0).Item("kode").ToString
                cNamaKota.Text = .Rows(0).Item("keterangan").ToString
                cKecamatan.Text = .Rows(0).Item("kecamatan").ToString
                cNamaKecamatan.Text = .Rows(0).Item("NamaKecamatan").ToString
                cKelurahan.Text = .Rows(0).Item("kelurahan").ToString
                cNamaKelurahan.Text = .Rows(0).Item("NamaKelurahan").ToString
            End If
        End With
    End Sub

    Private Sub GetRegister()
        Dim cKodeTemp As String = "", cKodeTemp1 As String = ""
        'Dim cCabangTemp As String = cKode.Text.Substring(0, 2)
        If nPos = myPos.add Then
            Const nJumlahRegister As Double = 1
            Const cField As String = "Max(Kode) as Kode"
            Dim cDataTable = objData.Browse(GetDSN, "RegisterNasabah", cField, "kode", data.myOperator.Prefix, cCabang.Text)
            With cDataTable
                If .Rows.Count = 0 Then
                    cKodeTemp = CStr(nJumlahRegister)
                Else
                    cKodeTemp = CStr(Val(Mid(.Rows(0).Item("kode").ToString, 4)) + 1)
                End If
            End With
            cKodeTemp1 = Padl(Trim(cKodeTemp), 6, "0")
            cKode.Text = cKodeTemp1
            cKodeInduk.Text = cKode.Text
            cCabangInduk.Text = cCabang.Text
        Else
            cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
        End If
    End Sub

    Private Sub GetMemory()
        Try

            Dim cKodeTemp As String = setKode(cCabang.Text, cKode.Text)
            Dim cfield As String = "r.*,a.Keterangan as NamaAgama,p.Keterangan as NamaPekerjaan,"
            cfield = cfield & " k.Keterangan as NamaKodya,c.Keterangan as NamaKecamatan,l.Keterangan as NamaKelurahan,"
            cfield = cfield & " i.Keterangan as NamaInstansi"
            Dim vaJoin() As Object = {"left join agama a on a.kode = r.agama", _
                                      "Left Join Pekerjaan p on p.Kode=r.Pekerjaan", _
                                      "Left Join Kodya k on k.Kode = r.Kodya", _
                                      "Left Join Kecamatan c on c.Kodya = r.Kodya and c.Kode = r.Kecamatan", _
                                      "Left Join Kelurahan l on l.Kodya = r.Kodya and l.Kecamatan = r.Kecamatan and l.Kode = r.Kelurahan", _
                                      "Left Join Kodya k1 on k1.Kode = r.KodyaTinggal", _
                                      "Left Join instansi i on i.kode = r.instansi"}
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah r", cfield, "r.kode", data.myOperator.Assign, cKodeTemp, , , vaJoin)
            If Not IsNothing(cDataTable) Then
                With cDataTable
                    If .Rows.Count > 0 Then
                        GetDataLookup("kelurahan", cKelurahan)
                        dTgl.DateTime = CDate(.Rows(0).Item("tglregister").ToString)
                        cKTP.Text = .Rows(0).Item("KTP").ToString
                        cNama.Text = .Rows(0).Item("nama").ToString
                        cNamaIbu.Text = .Rows(0).Item("namaibu").ToString
                        cTempatLahir.Text = .Rows(0).Item("TempatLahir").ToString
                        dTglLahir.DateTime = CDate(.Rows(0).Item("TglLahir").ToString)
                        SetOpt(optJenisKelamin, .Rows(0).Item("Kelamin").ToString)
                        SetOpt(optGolonganDarah, .Rows(0).Item("GolonganDarah").ToString)
                        cAlamat.Text = .Rows(0).Item("Alamat").ToString
                        cRTRW.Text = .Rows(0).Item("RTRW").ToString
                        cKota.EditValue = .Rows(0).Item("kodya").ToString
                        cNamaKota.Text = .Rows(0).Item("NamaKodya").ToString
                        GetDataLookup("kecamatan", cKecamatan, "kodya = '" & cKota.Text & "'")
                        cKecamatan.EditValue = .Rows(0).Item("Kecamatan").ToString
                        cNamaKecamatan.Text = .Rows(0).Item("namaKecamatan").ToString
                        GetDataLookup("kelurahan", cKelurahan, "kodya = '" & cKota.Text & "' and kecamatan = '" & cKecamatan.Text & "'")
                        cKelurahan.EditValue = .Rows(0).Item("kelurahan").ToString
                        cNamaKelurahan.Text = .Rows(0).Item("Namakelurahan").ToString
                        cKodePos.Text = .Rows(0).Item("KodePos").ToString
                        cAgama.EditValue = .Rows(0).Item("Agama").ToString
                        cNamaAgama.Text = .Rows(0).Item("NamaAgama").ToString
                        SetOpt(optPerkawinan, .Rows(0).Item("StatusPerkawinan").ToString)
                        cCabangInduk.Text = VB.Left(.Rows(0).Item("kodeinduk").ToString, 2)
                        cKodeInduk.Text = Mid(.Rows(0).Item("KodeInduk").ToString, 4)
                        cPekerjaan.EditValue = .Rows(0).Item("Pekerjaan").ToString
                        cNamaPekerjaan.Text = .Rows(0).Item("NamaPekerjaan").ToString
                        dTglKTP.DateTime = CDate(.Rows(0).Item("TglKtp").ToString)
                        cTelepon.Text = .Rows(0).Item("Telepon").ToString
                        cFax.Text = .Rows(0).Item("Fax").ToString
                        cInstansi.EditValue = .Rows(0).Item("Instansi").ToString
                        cNamaInstansi.Text = .Rows(0).Item("NamaInstansi").ToString
                        cNIP.Text = .Rows(0).Item("NIP").ToString
                        cNPWP.Text = .Rows(0).Item("NPWP").ToString
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function ValidSaving() As Boolean
        Return True
        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cKode.Focus()
            Return False
            Exit Function
        End If
        If Not CheckData(cNama.Text, "Nama Tidak Boleh Kosong") Then
            cNama.Focus()
            Return False
            Exit Function
        End If
        If Len(cKodeInduk.Text) < 9 Then
            MessageBox.Show("Kode Induk Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cKode.Focus()
            Return False
            Exit Function
        End If
        If Not CheckData(cTempatLahir.Text, "Tempat Lahir Tidak Boleh Kosong.") Then
            cTempatLahir.Focus()
            Return False
            Exit Function
        End If
        If Not CheckData(cPekerjaan.Text, "Pekerjaan Tidak Boleh Kosong.") Then
            cPekerjaan.Focus()
            Return False
            Exit Function
        End If
        If Not CheckData(cKTP.Text, "No. KTP Tidak Boleh Kosong.") Then
            cKTP.Focus()
            Return False
            Exit Function
        End If
        If Not CheckData(cTelepon.Text, "No. Telepon Tidak Boleh Kosong.") Then
            cTelepon.Focus()
            Return False
            Exit Function
        End If
    End Function

    Private Sub DeleteData()
        If MessageBox.Show("Data Akan Dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim cKodeTemp As String = setKode(cCabang.Text, cKode.Text)
            objData.Delete(GetDSN, "RegisterNasabah", "kode", data.myOperator.Assign, cKodeTemp)
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim cNoRegisterNasabah As String = setKode(cCabang.Text, cKode.Text)
                If nPos = myPos.add Then
                    Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "kode", "kode", data.myOperator.Assign, cNoRegisterNasabah)
                    With cDataTable
                        If .Rows.Count > 0 Then
                            MessageBox.Show("No. Register Terpakai Bersamaan. Silahkan Diganti.")
                            cKode.Focus()
                            Exit Sub
                        End If
                    End With
                End If
                Dim cJenisKelamin As String = GetOpt(optJenisKelamin)
                Dim cStatusPerkawinan As String = GetOpt(optPerkawinan)
                Dim cCabangEntry As String = CStr(aCfg(eCfg.msKodeCabang))
                Dim cKodeTemp As String = setKode(cCabang.Text, cKode.Text)
                Dim vaField() As Object = {"kode", "tglregister", "nama", "kelamin", "tempatlahir", _
                                           "tglLahir", "StatusPerkawinan", "KTP", "Agama", "Pekerjaan", _
                                           "alamat", "telepon", "fax", "kodya", "kecamatan", _
                                           "kelurahan", "kodepos", "RTRW", "TglKTP", "NamaIbu", _
                                           "UserName", "KodeInduk", "NPWP", "NIP", "Instansi", _
                                           "CabangEntry"}
                Dim vaValue() As Object = {cKodeTemp, dTgl.DateTime, cNama.Text, cJenisKelamin, cTempatLahir.Text, _
                                           dTglLahir.DateTime, cStatusPerkawinan, cKTP.Text, cAgama.Text, cPekerjaan.Text, _
                                           cAlamat.Text, cTelepon.Text, cFax.Text, cKota.Text, cKecamatan.Text, _
                                           cKelurahan.Text, cKodePos.Text, cRTRW.Text, dTglKTP.DateTime, cNamaIbu.Text, _
                                           cUserName, cKodeInduk.Text, cNPWP.Text, cNIP.Text, cInstansi.Text, _
                                           cCabangEntry}
                If Not lKoreksi Then
                    Do While Not objData.Add(GetDSN, "registernasabah", vaField, vaValue)
                        GetRegister()
                        cNoRegisterNasabah = setKode(cCabang.Text, cKode.Text)
                        vaValue = {cNoRegisterNasabah, dTgl.DateTime, cNama.Text, cJenisKelamin, cTempatLahir.Text, _
                                   dTglLahir.DateTime, cStatusPerkawinan, cKTP.Text, cAgama.Text, cPekerjaan.Text, _
                                   cAlamat.Text, cTelepon.Text, cFax.Text, cKota.Text, cKecamatan.Text, _
                                   cKelurahan.Text, cKodePos.Text, cRTRW.Text, dTglKTP.DateTime, cNamaIbu.Text, _
                                   cUserName, cKodeInduk.Text, cNPWP.Text, cNIP.Text, cInstansi.Text, _
                                   cCabangEntry}
                    Loop
                    'RegNas.WriteValue(Name, "Last Kode", Mid(cNoRegisterNasabah, 4, 7))
                Else
                    Dim cWhere As String = String.Format("kode = '{0}'", cNoRegisterNasabah)
                    objData.Edit(GetDSN, "registernasabah", cWhere, vaField, vaValue)
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub trRegisterNasabah_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar)
        GetEdit(False)
        InitValue()

        GetDataLookup("kodya", cKota)
        GetDataLookup("pekerjaan", cPekerjaan)
        GetDataLookup("instansi", cInstansi)
        GetDataLookup("agama", cAgama)
        cKecamatan.Properties.NullText = ""
        cKelurahan.Properties.NullText = ""

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cCabang.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cKTP.TabIndex, n)
        SetTabIndex(cNama.TabIndex, n)
        SetTabIndex(cNamaIbu.TabIndex, n)
        SetTabIndex(cTempatLahir.TabIndex, n)
        SetTabIndex(dTglLahir.TabIndex, n)
        SetTabIndex(optJenisKelamin.TabIndex, n)
        SetTabIndex(optGolonganDarah.TabIndex, n)
        SetTabIndex(cAlamat.TabIndex, n)
        SetTabIndex(cRTRW.TabIndex, n)
        SetTabIndex(cKota.TabIndex, n)
        SetTabIndex(cKecamatan.TabIndex, n)
        SetTabIndex(cKelurahan.TabIndex, n)
        SetTabIndex(cKodePos.TabIndex, n)
        SetTabIndex(cAgama.TabIndex, n)
        SetTabIndex(optPerkawinan.TabIndex, n)
        SetTabIndex(cCabangInduk.TabIndex, n)
        SetTabIndex(cKodeInduk.TabIndex, n)
        SetTabIndex(cPekerjaan.TabIndex, n)
        SetTabIndex(dTglKTP.TabIndex, n)
        SetTabIndex(cTelepon.TabIndex, n)
        SetTabIndex(cFax.TabIndex, n)
        SetTabIndex(cInstansi.TabIndex, n)
        SetTabIndex(cNIP.TabIndex, n)
        SetTabIndex(cNPWP.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        cKTP.EnterMoveNextControl = True
        cNama.EnterMoveNextControl = True
        cNamaIbu.EnterMoveNextControl = True
        cTempatLahir.EnterMoveNextControl = True
        dTglLahir.EnterMoveNextControl = True
        optJenisKelamin.EnterMoveNextControl = True
        optGolonganDarah.EnterMoveNextControl = True
        cAlamat.EnterMoveNextControl = True
        cRTRW.EnterMoveNextControl = True
        cKota.EnterMoveNextControl = True
        cKecamatan.EnterMoveNextControl = True
        cKelurahan.EnterMoveNextControl = True
        cKodePos.EnterMoveNextControl = True
        cAgama.EnterMoveNextControl = True
        optPerkawinan.EnterMoveNextControl = True
        cKodeInduk.EnterMoveNextControl = True
        cPekerjaan.EnterMoveNextControl = True
        dTglKTP.EnterMoveNextControl = True
        cTelepon.EnterMoveNextControl = True
        cFax.EnterMoveNextControl = True
        cInstansi.EnterMoveNextControl = True
        cNIP.EnterMoveNextControl = True
        cNPWP.EnterMoveNextControl = True
    End Sub

    Private Sub cKota_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKota.Closed
        GetKeteranganLookUp(cKota, cNamaKota)
        GetDataLookup("kecamatan", cKecamatan, "kodya = '" & cKota.Text & "'")
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        InitValue()
        lKoreksi = True
        GetEdit(True)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        InitValue()
        lKoreksi = False
        GetEdit(True)
        cCabang.Enabled = False
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        InitValue()
        GetEdit(True)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            InitValue()
            GetEdit(False)
        End If
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            Const cKodeTemp As String = "" ' RegNas.GetValue(Name, "Last Kode", "")
            If cKodeTemp <> "" Then
                If nPos = myPos.add Then
                    cKode.Text = CStr(CDbl(cKodeTemp) + 1)
                End If
                cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
            Else
                GetRegister()
            End If
            Dim cKodeTemp1 As String = setKode(cCabang.Text, cKode.Text)
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "Kode", "kode", data.myOperator.Assign, cKodeTemp1)
            With cDataTable
                If .Rows.Count > 0 Then
                    cKode.EnterMoveNextControl = True
                    If nPos = myPos.add Then
                        MessageBox.Show("Nomor Register Sudah Ada. Silahkan Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cKode.Focus()
                        cKode.Text = ""
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                ElseIf nPos <> myPos.add Then
                    MessageBox.Show("Data Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.EnterMoveNextControl = False
                    cKode.Text = ""
                    cKode.Focus()
                End If
            End With
        End If
    End Sub

    Private Sub cKecamatan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKecamatan.Closed
        GetKeteranganLookUp(cKecamatan, cNamaKecamatan)
        GetDataLookup("kelurahan", cKelurahan, "kodya = '" & cKota.Text & "' and kecamatan = '" & cKecamatan.Text & "'")
    End Sub

    Private Sub cKelurahan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKelurahan.Closed
        GetKeteranganLookUp(cKelurahan, cNamaKelurahan)
    End Sub

    Private Sub cPekerjaan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cPekerjaan.Closed
        GetKeteranganLookUp(cPekerjaan, cNamaPekerjaan)
    End Sub

    Private Sub cInstansi_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cInstansi.Closed
        GetKeteranganLookUp(cInstansi, cNamaInstansi)
    End Sub

    Private Sub cKota_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cKota.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cKota, cNamaKota)
            GetDataLookup("kecamatan", cKecamatan, "kodya = '" & cKota.Text & "'")
        End If
        LookupEditClear(sender, e)
    End Sub

    Private Sub cPekerjaan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cPekerjaan.KeyDown
        GetKeteranganLookUp(cPekerjaan, cNamaPekerjaan)
    End Sub

    Private Sub cInstansi_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cInstansi.KeyDown
        GetKeteranganLookUp(cInstansi, cNamaInstansi)
    End Sub

    Private Sub cKelurahan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKelurahan.KeyDown
        GetKeteranganLookUp(cKelurahan, cNamaKelurahan)
    End Sub

    Private Sub cKecamatan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKecamatan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cKecamatan, cNamaKecamatan)
            GetDataLookup("kelurahan", cKelurahan, "kodya = '" & cKota.Text & "' and kecamatan = '" & cKecamatan.Text & "'")
        End If
    End Sub

    Friend Shared Sub LookupEditClear(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyData = Keys.Back OrElse e.KeyData = Keys.Delete Then
            Dim luE As LookUpEdit = CType(sender, LookUpEdit)
            If luE.SelectionLength = luE.Text.Length Then
                luE.EditValue = Nothing
                e.Handled = True
            End If
        End If
    End Sub
End Class