﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trCariRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
                If dbSearch IsNot Nothing Then
                    dbSearch.Dispose()
                    dbSearch = Nothing
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                    objData = Nothing
                End If
                If vaMutasi IsNot Nothing Then
                    vaMutasi.Dispose()
                    vaMutasi = Nothing
                End If
                If vaArray IsNot Nothing Then
                    vaArray.Dispose()
                    vaArray = Nothing
                End If
                If vaTabungan IsNot Nothing Then
                    vaTabungan.Dispose()
                    vaTabungan = Nothing
                End If
                If vaDeposito IsNot Nothing Then
                    vaDeposito.Dispose()
                    vaDeposito = Nothing
                End If
                If vaKredit IsNot Nothing Then
                    vaKredit.Dispose()
                    vaKredit = Nothing
                End If
                If vaPengajuan IsNot Nothing Then
                    vaPengajuan.Dispose()
                    vaPengajuan = Nothing
                End If
                If vaSearch IsNot Nothing Then
                    vaSearch.Dispose()
                    vaSearch = Nothing
                End If
                If vaRekening IsNot Nothing Then
                    vaRekening.Dispose()
                    vaRekening = Nothing
                End If
                If vaKantor IsNot Nothing Then
                    vaKantor.Dispose()
                    vaKantor = Nothing
                End If
                If vaRekTabungan IsNot Nothing Then
                    vaRekTabungan.Dispose()
                    vaRekTabungan = Nothing
                End If
                If vaRekDeposito IsNot Nothing Then
                    vaRekDeposito.Dispose()
                    vaRekDeposito = Nothing
                End If
                If vaRekKredit IsNot Nothing Then
                    vaRekKredit.Dispose()
                    vaRekKredit = Nothing
                End If
                If vaReport IsNot Nothing Then
                    vaReport.Dispose()
                    vaReport = Nothing
                End If
                If vaTabunganTemp IsNot Nothing Then
                    vaTabunganTemp.Dispose()
                    vaTabunganTemp = Nothing
                End If
                If vaDepositoTemp IsNot Nothing Then
                    vaDepositoTemp.Dispose()
                    vaDepositoTemp = Nothing
                End If
                If vaKreditTemp IsNot Nothing Then
                    vaKreditTemp.Dispose()
                    vaKreditTemp = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trCariRegister))
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdDetailNasabah = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdPreview = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.cbKantor = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbJenisUrut = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cbUrut = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cbKategori = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbJenis = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.optCara = New DevExpress.XtraEditors.RadioGroup()
        Me.optKantor = New DevExpress.XtraEditors.RadioGroup()
        Me.cSearch = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.gRegisterDetail = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gRegister = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.groupDeposito = New DevExpress.XtraEditors.GroupControl()
        Me.gDeposito = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gDepositoDetail = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.groupTabungan = New DevExpress.XtraEditors.GroupControl()
        Me.gTabungan = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gTabunganDetail = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.gKredit = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gKreditDetail = New DevExpress.XtraGrid.GridControl()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit8 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.gPengajuanKredit = New DevExpress.XtraGrid.GridControl()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit9 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.gDataDetailTabungan = New DevExpress.XtraGrid.GridControl()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit10 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.gDataDetailDeposito = New DevExpress.XtraGrid.GridControl()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit11 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage7 = New DevExpress.XtraTab.XtraTabPage()
        Me.gDataDetailKredit = New DevExpress.XtraGrid.GridControl()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit12 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.XtraTabPage8 = New DevExpress.XtraTab.XtraTabPage()
        Me.gSaldoInduk = New DevExpress.XtraGrid.GridControl()
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit13 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cbKantor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbJenisUrut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbUrut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbKategori.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optCara.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKantor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSearch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.gRegisterDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gRegister, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.groupDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupDeposito.SuspendLayout()
        CType(Me.gDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gDepositoDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.groupTabungan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupTabungan.SuspendLayout()
        CType(Me.gTabungan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gTabunganDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.gKredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gKreditDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.gPengajuanKredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.gDataDetailTabungan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.gDataDetailDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage7.SuspendLayout()
        CType(Me.gDataDetailKredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage8.SuspendLayout()
        CType(Me.gSaldoInduk, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdDetailNasabah)
        Me.PanelControl4.Controls.Add(Me.cmdPreview)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 559)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(881, 33)
        Me.PanelControl4.TabIndex = 24
        '
        'cmdDetailNasabah
        '
        Me.cmdDetailNasabah.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdDetailNasabah.Location = New System.Drawing.Point(5, 5)
        Me.cmdDetailNasabah.Name = "cmdDetailNasabah"
        Me.cmdDetailNasabah.Size = New System.Drawing.Size(106, 23)
        Me.cmdDetailNasabah.TabIndex = 11
        Me.cmdDetailNasabah.Text = "&Detail Nasabah"
        '
        'cmdPreview
        '
        Me.cmdPreview.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdPreview.Location = New System.Drawing.Point(716, 5)
        Me.cmdPreview.Name = "cmdPreview"
        Me.cmdPreview.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.cmdPreview.SuperTip = SuperToolTip1
        Me.cmdPreview.TabIndex = 10
        Me.cmdPreview.Text = "&Preview"
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(797, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip2
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdSearch)
        Me.PanelControl1.Controls.Add(Me.cbKantor)
        Me.PanelControl1.Controls.Add(Me.cbJenisUrut)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.cbUrut)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.cbKategori)
        Me.PanelControl1.Controls.Add(Me.cbJenis)
        Me.PanelControl1.Controls.Add(Me.optCara)
        Me.PanelControl1.Controls.Add(Me.optKantor)
        Me.PanelControl1.Controls.Add(Me.cSearch)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(881, 135)
        Me.PanelControl1.TabIndex = 23
        '
        'cmdSearch
        '
        Me.cmdSearch.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSearch.Location = New System.Drawing.Point(610, 104)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(23, 23)
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image2"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Batal / Keluar"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem2)
        Me.cmdSearch.SuperTip = SuperToolTip3
        Me.cmdSearch.TabIndex = 274
        '
        'cbKantor
        '
        Me.cbKantor.Location = New System.Drawing.Point(275, 8)
        Me.cbKantor.Name = "cbKantor"
        Me.cbKantor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbKantor.Size = New System.Drawing.Size(329, 20)
        Me.cbKantor.TabIndex = 273
        '
        'cbJenisUrut
        '
        Me.cbJenisUrut.Location = New System.Drawing.Point(499, 81)
        Me.cbJenisUrut.Name = "cbJenisUrut"
        Me.cbJenisUrut.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenisUrut.Size = New System.Drawing.Size(105, 20)
        Me.cbJenisUrut.TabIndex = 271
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(430, 85)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl8.TabIndex = 270
        Me.LabelControl8.Text = "Jenis Sorting"
        '
        'cbUrut
        '
        Me.cbUrut.Location = New System.Drawing.Point(300, 81)
        Me.cbUrut.Name = "cbUrut"
        Me.cbUrut.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbUrut.Size = New System.Drawing.Size(105, 20)
        Me.cbUrut.TabIndex = 269
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(236, 85)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl7.TabIndex = 268
        Me.LabelControl7.Text = "Sorting Data "
        '
        'cbKategori
        '
        Me.cbKategori.Location = New System.Drawing.Point(104, 81)
        Me.cbKategori.Name = "cbKategori"
        Me.cbKategori.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbKategori.Size = New System.Drawing.Size(105, 20)
        Me.cbKategori.TabIndex = 267
        '
        'cbJenis
        '
        Me.cbJenis.Location = New System.Drawing.Point(104, 32)
        Me.cbJenis.Name = "cbJenis"
        Me.cbJenis.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenis.Size = New System.Drawing.Size(105, 20)
        Me.cbJenis.TabIndex = 266
        '
        'optCara
        '
        Me.optCara.Location = New System.Drawing.Point(104, 55)
        Me.optCara.Name = "optCara"
        Me.optCara.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Acak"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Dari Huruf Pertama")})
        Me.optCara.Size = New System.Drawing.Size(276, 23)
        Me.optCara.TabIndex = 139
        '
        'optKantor
        '
        Me.optKantor.Location = New System.Drawing.Point(104, 6)
        Me.optKantor.Name = "optKantor"
        Me.optKantor.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Semua"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Per Kantor")})
        Me.optKantor.Size = New System.Drawing.Size(165, 23)
        Me.optKantor.TabIndex = 137
        '
        'cSearch
        '
        Me.cSearch.Location = New System.Drawing.Point(104, 105)
        Me.cSearch.Name = "cSearch"
        Me.cSearch.Size = New System.Drawing.Size(500, 20)
        Me.cSearch.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 109)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Cari Data"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 85)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Kategori"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 36)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "Jenis Data"
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 60)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(73, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Cara pencarian"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(59, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Jenis Kantor"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(2, 143)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(881, 415)
        Me.tabData.TabIndex = 25
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6, Me.XtraTabPage7, Me.XtraTabPage8})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.gRegisterDetail)
        Me.XtraTabPage1.Controls.Add(Me.gRegister)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage1.Text = "Data Register"
        '
        'gRegisterDetail
        '
        Me.gRegisterDetail.Location = New System.Drawing.Point(3, 207)
        Me.gRegisterDetail.MainView = Me.GridView2
        Me.gRegisterDetail.Name = "gRegisterDetail"
        Me.gRegisterDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit2})
        Me.gRegisterDetail.Size = New System.Drawing.Size(869, 177)
        Me.gRegisterDetail.TabIndex = 33
        Me.gRegisterDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gRegisterDetail
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Caption = "Check"
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'gRegister
        '
        Me.gRegister.Location = New System.Drawing.Point(3, 4)
        Me.gRegister.MainView = Me.GridView1
        Me.gRegister.Name = "gRegister"
        Me.gRegister.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.gRegister.Size = New System.Drawing.Size(869, 197)
        Me.gRegister.TabIndex = 32
        Me.gRegister.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.gRegister
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.groupDeposito)
        Me.XtraTabPage2.Controls.Add(Me.groupTabungan)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage2.Text = "Data Tabungan && Deposito"
        '
        'groupDeposito
        '
        Me.groupDeposito.Controls.Add(Me.gDeposito)
        Me.groupDeposito.Controls.Add(Me.gDepositoDetail)
        Me.groupDeposito.Location = New System.Drawing.Point(4, 196)
        Me.groupDeposito.Name = "groupDeposito"
        Me.groupDeposito.Size = New System.Drawing.Size(868, 186)
        Me.groupDeposito.TabIndex = 1
        Me.groupDeposito.Text = "Data Deposito"
        '
        'gDeposito
        '
        Me.gDeposito.Location = New System.Drawing.Point(4, 24)
        Me.gDeposito.MainView = Me.GridView5
        Me.gDeposito.Name = "gDeposito"
        Me.gDeposito.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit5})
        Me.gDeposito.Size = New System.Drawing.Size(366, 157)
        Me.gDeposito.TabIndex = 34
        Me.gDeposito.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.gDeposito
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView5.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit5
        '
        Me.RepositoryItemCheckEdit5.AutoHeight = False
        Me.RepositoryItemCheckEdit5.Caption = "Check"
        Me.RepositoryItemCheckEdit5.Name = "RepositoryItemCheckEdit5"
        '
        'gDepositoDetail
        '
        Me.gDepositoDetail.Location = New System.Drawing.Point(376, 24)
        Me.gDepositoDetail.MainView = Me.GridView6
        Me.gDepositoDetail.Name = "gDepositoDetail"
        Me.gDepositoDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit6})
        Me.gDepositoDetail.Size = New System.Drawing.Size(487, 157)
        Me.gDepositoDetail.TabIndex = 33
        Me.gDepositoDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.GridControl = Me.gDepositoDetail
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView6.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit6
        '
        Me.RepositoryItemCheckEdit6.AutoHeight = False
        Me.RepositoryItemCheckEdit6.Caption = "Check"
        Me.RepositoryItemCheckEdit6.Name = "RepositoryItemCheckEdit6"
        '
        'groupTabungan
        '
        Me.groupTabungan.Controls.Add(Me.gTabungan)
        Me.groupTabungan.Controls.Add(Me.gTabunganDetail)
        Me.groupTabungan.Location = New System.Drawing.Point(4, 4)
        Me.groupTabungan.Name = "groupTabungan"
        Me.groupTabungan.Size = New System.Drawing.Size(868, 186)
        Me.groupTabungan.TabIndex = 0
        Me.groupTabungan.Text = "Data Tabungan"
        '
        'gTabungan
        '
        Me.gTabungan.Location = New System.Drawing.Point(4, 24)
        Me.gTabungan.MainView = Me.GridView3
        Me.gTabungan.Name = "gTabungan"
        Me.gTabungan.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit4})
        Me.gTabungan.Size = New System.Drawing.Size(366, 157)
        Me.gTabungan.TabIndex = 34
        Me.gTabungan.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.gTabungan
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit4
        '
        Me.RepositoryItemCheckEdit4.AutoHeight = False
        Me.RepositoryItemCheckEdit4.Caption = "Check"
        Me.RepositoryItemCheckEdit4.Name = "RepositoryItemCheckEdit4"
        '
        'gTabunganDetail
        '
        Me.gTabunganDetail.Location = New System.Drawing.Point(376, 24)
        Me.gTabunganDetail.MainView = Me.GridView4
        Me.gTabunganDetail.Name = "gTabunganDetail"
        Me.gTabunganDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit3})
        Me.gTabunganDetail.Size = New System.Drawing.Size(487, 157)
        Me.gTabunganDetail.TabIndex = 33
        Me.gTabunganDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.gTabunganDetail
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit3
        '
        Me.RepositoryItemCheckEdit3.AutoHeight = False
        Me.RepositoryItemCheckEdit3.Caption = "Check"
        Me.RepositoryItemCheckEdit3.Name = "RepositoryItemCheckEdit3"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.gKredit)
        Me.XtraTabPage3.Controls.Add(Me.gKreditDetail)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage3.Text = "Data Kredit"
        '
        'gKredit
        '
        Me.gKredit.Location = New System.Drawing.Point(3, 4)
        Me.gKredit.MainView = Me.GridView7
        Me.gKredit.Name = "gKredit"
        Me.gKredit.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit7})
        Me.gKredit.Size = New System.Drawing.Size(366, 380)
        Me.gKredit.TabIndex = 36
        Me.gKredit.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.GridControl = Me.gKredit
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView7.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit7
        '
        Me.RepositoryItemCheckEdit7.AutoHeight = False
        Me.RepositoryItemCheckEdit7.Caption = "Check"
        Me.RepositoryItemCheckEdit7.Name = "RepositoryItemCheckEdit7"
        '
        'gKreditDetail
        '
        Me.gKreditDetail.Location = New System.Drawing.Point(375, 4)
        Me.gKreditDetail.MainView = Me.GridView8
        Me.gKreditDetail.Name = "gKreditDetail"
        Me.gKreditDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit8})
        Me.gKreditDetail.Size = New System.Drawing.Size(497, 380)
        Me.gKreditDetail.TabIndex = 35
        Me.gKreditDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView8})
        '
        'GridView8
        '
        Me.GridView8.GridControl = Me.gKreditDetail
        Me.GridView8.Name = "GridView8"
        Me.GridView8.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView8.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit8
        '
        Me.RepositoryItemCheckEdit8.AutoHeight = False
        Me.RepositoryItemCheckEdit8.Caption = "Check"
        Me.RepositoryItemCheckEdit8.Name = "RepositoryItemCheckEdit8"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.gPengajuanKredit)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage4.Text = "Data Pengajuan Kredit"
        '
        'gPengajuanKredit
        '
        Me.gPengajuanKredit.Location = New System.Drawing.Point(3, 3)
        Me.gPengajuanKredit.MainView = Me.GridView9
        Me.gPengajuanKredit.Name = "gPengajuanKredit"
        Me.gPengajuanKredit.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit9})
        Me.gPengajuanKredit.Size = New System.Drawing.Size(869, 381)
        Me.gPengajuanKredit.TabIndex = 33
        Me.gPengajuanKredit.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView9})
        '
        'GridView9
        '
        Me.GridView9.GridControl = Me.gPengajuanKredit
        Me.GridView9.Name = "GridView9"
        Me.GridView9.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView9.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit9
        '
        Me.RepositoryItemCheckEdit9.AutoHeight = False
        Me.RepositoryItemCheckEdit9.Caption = "Check"
        Me.RepositoryItemCheckEdit9.Name = "RepositoryItemCheckEdit9"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.gDataDetailTabungan)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage5.Text = "Detail Tabungan"
        '
        'gDataDetailTabungan
        '
        Me.gDataDetailTabungan.Location = New System.Drawing.Point(3, 3)
        Me.gDataDetailTabungan.MainView = Me.GridView10
        Me.gDataDetailTabungan.Name = "gDataDetailTabungan"
        Me.gDataDetailTabungan.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit10})
        Me.gDataDetailTabungan.Size = New System.Drawing.Size(869, 381)
        Me.gDataDetailTabungan.TabIndex = 33
        Me.gDataDetailTabungan.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView10})
        '
        'GridView10
        '
        Me.GridView10.GridControl = Me.gDataDetailTabungan
        Me.GridView10.Name = "GridView10"
        Me.GridView10.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView10.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit10
        '
        Me.RepositoryItemCheckEdit10.AutoHeight = False
        Me.RepositoryItemCheckEdit10.Caption = "Check"
        Me.RepositoryItemCheckEdit10.Name = "RepositoryItemCheckEdit10"
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.gDataDetailDeposito)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage6.Text = "Detail Deposito"
        '
        'gDataDetailDeposito
        '
        Me.gDataDetailDeposito.Location = New System.Drawing.Point(3, 3)
        Me.gDataDetailDeposito.MainView = Me.GridView11
        Me.gDataDetailDeposito.Name = "gDataDetailDeposito"
        Me.gDataDetailDeposito.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit11})
        Me.gDataDetailDeposito.Size = New System.Drawing.Size(869, 381)
        Me.gDataDetailDeposito.TabIndex = 33
        Me.gDataDetailDeposito.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView11})
        '
        'GridView11
        '
        Me.GridView11.GridControl = Me.gDataDetailDeposito
        Me.GridView11.Name = "GridView11"
        Me.GridView11.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView11.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit11
        '
        Me.RepositoryItemCheckEdit11.AutoHeight = False
        Me.RepositoryItemCheckEdit11.Caption = "Check"
        Me.RepositoryItemCheckEdit11.Name = "RepositoryItemCheckEdit11"
        '
        'XtraTabPage7
        '
        Me.XtraTabPage7.Controls.Add(Me.gDataDetailKredit)
        Me.XtraTabPage7.Name = "XtraTabPage7"
        Me.XtraTabPage7.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage7.Text = "Detail Kredit"
        '
        'gDataDetailKredit
        '
        Me.gDataDetailKredit.Location = New System.Drawing.Point(3, 3)
        Me.gDataDetailKredit.MainView = Me.GridView12
        Me.gDataDetailKredit.Name = "gDataDetailKredit"
        Me.gDataDetailKredit.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit12})
        Me.gDataDetailKredit.Size = New System.Drawing.Size(869, 381)
        Me.gDataDetailKredit.TabIndex = 33
        Me.gDataDetailKredit.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView12})
        '
        'GridView12
        '
        Me.GridView12.GridControl = Me.gDataDetailKredit
        Me.GridView12.Name = "GridView12"
        Me.GridView12.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView12.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit12
        '
        Me.RepositoryItemCheckEdit12.AutoHeight = False
        Me.RepositoryItemCheckEdit12.Caption = "Check"
        Me.RepositoryItemCheckEdit12.Name = "RepositoryItemCheckEdit12"
        '
        'XtraTabPage8
        '
        Me.XtraTabPage8.Controls.Add(Me.gSaldoInduk)
        Me.XtraTabPage8.Name = "XtraTabPage8"
        Me.XtraTabPage8.Size = New System.Drawing.Size(875, 387)
        Me.XtraTabPage8.Text = "Saldo Induk"
        '
        'gSaldoInduk
        '
        Me.gSaldoInduk.Location = New System.Drawing.Point(3, 3)
        Me.gSaldoInduk.MainView = Me.GridView13
        Me.gSaldoInduk.Name = "gSaldoInduk"
        Me.gSaldoInduk.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit13})
        Me.gSaldoInduk.Size = New System.Drawing.Size(869, 381)
        Me.gSaldoInduk.TabIndex = 33
        Me.gSaldoInduk.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView13})
        '
        'GridView13
        '
        Me.GridView13.GridControl = Me.gSaldoInduk
        Me.GridView13.Name = "GridView13"
        Me.GridView13.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView13.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit13
        '
        Me.RepositoryItemCheckEdit13.AutoHeight = False
        Me.RepositoryItemCheckEdit13.Caption = "Check"
        Me.RepositoryItemCheckEdit13.Name = "RepositoryItemCheckEdit13"
        '
        'trCariRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(895, 596)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trCariRegister"
        Me.Text = "Cari Register"
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cbKantor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbJenisUrut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbUrut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbKategori.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optCara.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKantor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSearch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.gRegisterDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gRegister, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.groupDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupDeposito.ResumeLayout(False)
        CType(Me.gDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gDepositoDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.groupTabungan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupTabungan.ResumeLayout(False)
        CType(Me.gTabungan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gTabunganDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.gKredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gKreditDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.gPengajuanKredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.gDataDetailTabungan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage6.ResumeLayout(False)
        CType(Me.gDataDetailDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage7.ResumeLayout(False)
        CType(Me.gDataDetailKredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage8.ResumeLayout(False)
        CType(Me.gSaldoInduk, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents optCara As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optKantor As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents cSearch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbJenisUrut As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbUrut As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbKategori As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbJenis As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gRegisterDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gRegister As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents groupDeposito As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gDeposito As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gDepositoDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents groupTabungan As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gTabungan As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gTabunganDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gKredit As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gKreditDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit8 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gPengajuanKredit As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit9 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gDataDetailTabungan As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit10 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gDataDetailDeposito As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit11 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage7 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gDataDetailKredit As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit12 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents XtraTabPage8 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gSaldoInduk As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit13 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents cbKantor As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cmdSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDetailNasabah As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdPreview As DevExpress.XtraEditors.SimpleButton
End Class
