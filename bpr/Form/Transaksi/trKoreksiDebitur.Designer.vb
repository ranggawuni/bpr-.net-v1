﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trKoreksiDebitur
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trKoreksiDebitur))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cbJenisUsaha = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.cbPeriodePembayaran = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cbSumberDana = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cbKeterkaitan = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nPlafond = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.cBagianYangDijamin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganPenjamin = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganPenjamin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cAO = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAO = New DevExpress.XtraEditors.TextEdit()
        Me.cBendahara = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaBendahara = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cWilayah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaWilayah = New DevExpress.XtraEditors.TextEdit()
        Me.cNoSPK = New DevExpress.XtraEditors.TextEdit()
        Me.cNamaSifatKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cSifatKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaJenisPenggunaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cSektorEkonomi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaSektorEkonomi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDebitur = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganDebitur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.cJenisPenggunaan = New DevExpress.XtraEditors.LookUpEdit()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.optAutoDebet = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.optInstansi = New DevExpress.XtraEditors.RadioGroup()
        Me.optKaryawan = New DevExpress.XtraEditors.RadioGroup()
        Me.optDenda = New DevExpress.XtraEditors.RadioGroup()
        Me.optJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.optProvisi = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cNoPengajuan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cKode = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cbJenisUsaha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodePembayaran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbSumberDana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBagianYangDijamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBendahara.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaBendahara.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNoSPK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSifatKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSifatKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optAutoDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optInstansi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cNoPengajuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 1)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(778, 123)
        Me.PanelControl1.TabIndex = 18
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(103, 4)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(139, 20)
        Me.cRekening.TabIndex = 190
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 99)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(58, 13)
        Me.lblTglRegister.TabIndex = 189
        Me.lblTglRegister.Text = "Tgl Realisasi"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(103, 95)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 188
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 9)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 186
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(103, 72)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(8, 76)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(103, 50)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(294, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 54)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 27)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 31)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(2, 130)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(778, 268)
        Me.XtraTabControl1.TabIndex = 192
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PanelControl4)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(772, 240)
        Me.XtraTabPage1.Text = "Data Kredit 1"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cKode)
        Me.PanelControl4.Controls.Add(Me.cNoPengajuan)
        Me.PanelControl4.Controls.Add(Me.cbJenisUsaha)
        Me.PanelControl4.Controls.Add(Me.LabelControl22)
        Me.PanelControl4.Controls.Add(Me.cbPeriodePembayaran)
        Me.PanelControl4.Controls.Add(Me.LabelControl5)
        Me.PanelControl4.Controls.Add(Me.cbSumberDana)
        Me.PanelControl4.Controls.Add(Me.LabelControl4)
        Me.PanelControl4.Controls.Add(Me.cbKeterkaitan)
        Me.PanelControl4.Controls.Add(Me.LabelControl17)
        Me.PanelControl4.Controls.Add(Me.LabelControl3)
        Me.PanelControl4.Controls.Add(Me.LabelControl2)
        Me.PanelControl4.Controls.Add(Me.nPlafond)
        Me.PanelControl4.Controls.Add(Me.LabelControl42)
        Me.PanelControl4.Controls.Add(Me.cGolonganKredit)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganKredit)
        Me.PanelControl4.Controls.Add(Me.LabelControl19)
        Me.PanelControl4.Controls.Add(Me.cBagianYangDijamin)
        Me.PanelControl4.Controls.Add(Me.LabelControl21)
        Me.PanelControl4.Controls.Add(Me.LabelControl1)
        Me.PanelControl4.Controls.Add(Me.cGolonganPenjamin)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganPenjamin)
        Me.PanelControl4.Controls.Add(Me.LabelControl7)
        Me.PanelControl4.Controls.Add(Me.cAO)
        Me.PanelControl4.Controls.Add(Me.cNamaAO)
        Me.PanelControl4.Controls.Add(Me.cBendahara)
        Me.PanelControl4.Controls.Add(Me.cNamaBendahara)
        Me.PanelControl4.Controls.Add(Me.LabelControl9)
        Me.PanelControl4.Controls.Add(Me.LabelControl8)
        Me.PanelControl4.Controls.Add(Me.cWilayah)
        Me.PanelControl4.Controls.Add(Me.cNamaWilayah)
        Me.PanelControl4.Controls.Add(Me.cNoSPK)
        Me.PanelControl4.Controls.Add(Me.cNamaSifatKredit)
        Me.PanelControl4.Controls.Add(Me.LabelControl6)
        Me.PanelControl4.Controls.Add(Me.cSifatKredit)
        Me.PanelControl4.Controls.Add(Me.LabelControl10)
        Me.PanelControl4.Controls.Add(Me.cNamaJenisPenggunaan)
        Me.PanelControl4.Controls.Add(Me.LabelControl11)
        Me.PanelControl4.Controls.Add(Me.cSektorEkonomi)
        Me.PanelControl4.Controls.Add(Me.cNamaSektorEkonomi)
        Me.PanelControl4.Controls.Add(Me.LabelControl13)
        Me.PanelControl4.Controls.Add(Me.cGolonganDebitur)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganDebitur)
        Me.PanelControl4.Controls.Add(Me.LabelControl16)
        Me.PanelControl4.Controls.Add(Me.cJenisPenggunaan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(766, 234)
        Me.PanelControl4.TabIndex = 18
        '
        'cbJenisUsaha
        '
        Me.cbJenisUsaha.Location = New System.Drawing.Point(488, 160)
        Me.cbJenisUsaha.Name = "cbJenisUsaha"
        Me.cbJenisUsaha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenisUsaha.Size = New System.Drawing.Size(138, 20)
        Me.cbJenisUsaha.TabIndex = 271
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(365, 164)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl22.TabIndex = 270
        Me.LabelControl22.Text = "Jenis Usaha"
        '
        'cbPeriodePembayaran
        '
        Me.cbPeriodePembayaran.Location = New System.Drawing.Point(488, 137)
        Me.cbPeriodePembayaran.Name = "cbPeriodePembayaran"
        Me.cbPeriodePembayaran.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodePembayaran.Size = New System.Drawing.Size(138, 20)
        Me.cbPeriodePembayaran.TabIndex = 269
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(365, 141)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl5.TabIndex = 268
        Me.LabelControl5.Text = "Periode Pembayaran"
        '
        'cbSumberDana
        '
        Me.cbSumberDana.Location = New System.Drawing.Point(488, 115)
        Me.cbSumberDana.Name = "cbSumberDana"
        Me.cbSumberDana.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbSumberDana.Size = New System.Drawing.Size(138, 20)
        Me.cbSumberDana.TabIndex = 267
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(365, 119)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl4.TabIndex = 266
        Me.LabelControl4.Text = "Sumber Dana Pelunasan"
        '
        'cbKeterkaitan
        '
        Me.cbKeterkaitan.Location = New System.Drawing.Point(488, 93)
        Me.cbKeterkaitan.Name = "cbKeterkaitan"
        Me.cbKeterkaitan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbKeterkaitan.Size = New System.Drawing.Size(138, 20)
        Me.cbKeterkaitan.TabIndex = 265
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(365, 97)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl17.TabIndex = 264
        Me.LabelControl17.Text = "Keterkaitan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(365, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl3.TabIndex = 234
        Me.LabelControl3.Text = "No. Register"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(365, 52)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl2.TabIndex = 232
        Me.LabelControl2.Text = "No. Pengajuan"
        '
        'nPlafond
        '
        Me.nPlafond.Enabled = False
        Me.nPlafond.Location = New System.Drawing.Point(128, 5)
        Me.nPlafond.Name = "nPlafond"
        Me.nPlafond.Size = New System.Drawing.Size(141, 20)
        Me.nPlafond.TabIndex = 231
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl42.TabIndex = 230
        Me.LabelControl42.Text = "Plafond"
        '
        'cGolonganKredit
        '
        Me.cGolonganKredit.Location = New System.Drawing.Point(128, 49)
        Me.cGolonganKredit.Name = "cGolonganKredit"
        Me.cGolonganKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganKredit.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganKredit.TabIndex = 229
        '
        'cNamaGolonganKredit
        '
        Me.cNamaGolonganKredit.Enabled = False
        Me.cNamaGolonganKredit.Location = New System.Drawing.Point(178, 48)
        Me.cNamaGolonganKredit.Name = "cNamaGolonganKredit"
        Me.cNamaGolonganKredit.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganKredit.TabIndex = 228
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(5, 52)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl19.TabIndex = 226
        Me.LabelControl19.Text = "Golongan Kredit"
        '
        'cBagianYangDijamin
        '
        Me.cBagianYangDijamin.Location = New System.Drawing.Point(488, 27)
        Me.cBagianYangDijamin.Name = "cBagianYangDijamin"
        Me.cBagianYangDijamin.Size = New System.Drawing.Size(44, 20)
        Me.cBagianYangDijamin.TabIndex = 218
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(365, 31)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl21.TabIndex = 217
        Me.LabelControl21.Text = "Bagian Yg Dijamin"
        '
        'LabelControl1
        '
        Me.LabelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl1.Location = New System.Drawing.Point(365, 9)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl1.TabIndex = 214
        Me.LabelControl1.Text = "Golongan Penjamin"
        '
        'cGolonganPenjamin
        '
        Me.cGolonganPenjamin.Location = New System.Drawing.Point(488, 5)
        Me.cGolonganPenjamin.Name = "cGolonganPenjamin"
        Me.cGolonganPenjamin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganPenjamin.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganPenjamin.TabIndex = 213
        '
        'cNamaGolonganPenjamin
        '
        Me.cNamaGolonganPenjamin.Enabled = False
        Me.cNamaGolonganPenjamin.Location = New System.Drawing.Point(538, 5)
        Me.cNamaGolonganPenjamin.Name = "cNamaGolonganPenjamin"
        Me.cNamaGolonganPenjamin.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganPenjamin.TabIndex = 212
        '
        'LabelControl7
        '
        Me.LabelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl7.Location = New System.Drawing.Point(5, 210)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl7.TabIndex = 211
        Me.LabelControl7.Text = "Account Officer"
        '
        'cAO
        '
        Me.cAO.Location = New System.Drawing.Point(128, 206)
        Me.cAO.Name = "cAO"
        Me.cAO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAO.Size = New System.Drawing.Size(44, 20)
        Me.cAO.TabIndex = 210
        '
        'cNamaAO
        '
        Me.cNamaAO.Enabled = False
        Me.cNamaAO.Location = New System.Drawing.Point(178, 206)
        Me.cNamaAO.Name = "cNamaAO"
        Me.cNamaAO.Size = New System.Drawing.Size(141, 20)
        Me.cNamaAO.TabIndex = 209
        '
        'cBendahara
        '
        Me.cBendahara.Location = New System.Drawing.Point(128, 183)
        Me.cBendahara.Name = "cBendahara"
        Me.cBendahara.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cBendahara.Size = New System.Drawing.Size(44, 20)
        Me.cBendahara.TabIndex = 208
        '
        'cNamaBendahara
        '
        Me.cNamaBendahara.Enabled = False
        Me.cNamaBendahara.Location = New System.Drawing.Point(178, 183)
        Me.cNamaBendahara.Name = "cNamaBendahara"
        Me.cNamaBendahara.Size = New System.Drawing.Size(141, 20)
        Me.cNamaBendahara.TabIndex = 207
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(5, 187)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl9.TabIndex = 206
        Me.LabelControl9.Text = "Bendahara"
        '
        'LabelControl8
        '
        Me.LabelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl8.Location = New System.Drawing.Point(5, 164)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl8.TabIndex = 202
        Me.LabelControl8.Text = "Wilayah"
        '
        'cWilayah
        '
        Me.cWilayah.Location = New System.Drawing.Point(128, 160)
        Me.cWilayah.Name = "cWilayah"
        Me.cWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cWilayah.Size = New System.Drawing.Size(44, 20)
        Me.cWilayah.TabIndex = 201
        '
        'cNamaWilayah
        '
        Me.cNamaWilayah.Enabled = False
        Me.cNamaWilayah.Location = New System.Drawing.Point(178, 160)
        Me.cNamaWilayah.Name = "cNamaWilayah"
        Me.cNamaWilayah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaWilayah.TabIndex = 200
        '
        'cNoSPK
        '
        Me.cNoSPK.Enabled = False
        Me.cNoSPK.Location = New System.Drawing.Point(128, 27)
        Me.cNoSPK.Name = "cNoSPK"
        Me.cNoSPK.Size = New System.Drawing.Size(191, 20)
        Me.cNoSPK.TabIndex = 199
        '
        'cNamaSifatKredit
        '
        Me.cNamaSifatKredit.Enabled = False
        Me.cNamaSifatKredit.Location = New System.Drawing.Point(178, 70)
        Me.cNamaSifatKredit.Name = "cNamaSifatKredit"
        Me.cNamaSifatKredit.Size = New System.Drawing.Size(141, 20)
        Me.cNamaSifatKredit.TabIndex = 187
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(5, 74)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl6.TabIndex = 186
        Me.LabelControl6.Text = "Sifat Kredit"
        '
        'cSifatKredit
        '
        Me.cSifatKredit.Location = New System.Drawing.Point(128, 70)
        Me.cSifatKredit.Name = "cSifatKredit"
        Me.cSifatKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSifatKredit.Size = New System.Drawing.Size(44, 20)
        Me.cSifatKredit.TabIndex = 188
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(5, 31)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl10.TabIndex = 175
        Me.LabelControl10.Text = "No. PK"
        '
        'cNamaJenisPenggunaan
        '
        Me.cNamaJenisPenggunaan.Enabled = False
        Me.cNamaJenisPenggunaan.Location = New System.Drawing.Point(178, 93)
        Me.cNamaJenisPenggunaan.Name = "cNamaJenisPenggunaan"
        Me.cNamaJenisPenggunaan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaJenisPenggunaan.TabIndex = 173
        '
        'LabelControl11
        '
        Me.LabelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl11.Location = New System.Drawing.Point(5, 141)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl11.TabIndex = 172
        Me.LabelControl11.Text = "Sektor Ekonomi"
        '
        'cSektorEkonomi
        '
        Me.cSektorEkonomi.Location = New System.Drawing.Point(128, 137)
        Me.cSektorEkonomi.Name = "cSektorEkonomi"
        Me.cSektorEkonomi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSektorEkonomi.Size = New System.Drawing.Size(44, 20)
        Me.cSektorEkonomi.TabIndex = 171
        '
        'cNamaSektorEkonomi
        '
        Me.cNamaSektorEkonomi.Enabled = False
        Me.cNamaSektorEkonomi.Location = New System.Drawing.Point(178, 137)
        Me.cNamaSektorEkonomi.Name = "cNamaSektorEkonomi"
        Me.cNamaSektorEkonomi.Size = New System.Drawing.Size(141, 20)
        Me.cNamaSektorEkonomi.TabIndex = 170
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(5, 97)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl13.TabIndex = 169
        Me.LabelControl13.Text = "Jenis Penggunaan"
        '
        'cGolonganDebitur
        '
        Me.cGolonganDebitur.Location = New System.Drawing.Point(128, 115)
        Me.cGolonganDebitur.Name = "cGolonganDebitur"
        Me.cGolonganDebitur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganDebitur.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganDebitur.TabIndex = 168
        '
        'cNamaGolonganDebitur
        '
        Me.cNamaGolonganDebitur.Enabled = False
        Me.cNamaGolonganDebitur.Location = New System.Drawing.Point(178, 115)
        Me.cNamaGolonganDebitur.Name = "cNamaGolonganDebitur"
        Me.cNamaGolonganDebitur.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganDebitur.TabIndex = 167
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(5, 119)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl16.TabIndex = 166
        Me.LabelControl16.Text = "Golongan Debitur"
        '
        'cJenisPenggunaan
        '
        Me.cJenisPenggunaan.Location = New System.Drawing.Point(128, 93)
        Me.cJenisPenggunaan.Name = "cJenisPenggunaan"
        Me.cJenisPenggunaan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cJenisPenggunaan.Size = New System.Drawing.Size(44, 20)
        Me.cJenisPenggunaan.TabIndex = 174
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.PanelControl7)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(772, 240)
        Me.XtraTabPage2.Text = "Data Kredit 2"
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl7.Controls.Add(Me.LabelControl23)
        Me.PanelControl7.Controls.Add(Me.optAutoDebet)
        Me.PanelControl7.Controls.Add(Me.LabelControl18)
        Me.PanelControl7.Controls.Add(Me.LabelControl34)
        Me.PanelControl7.Controls.Add(Me.optInstansi)
        Me.PanelControl7.Controls.Add(Me.optKaryawan)
        Me.PanelControl7.Controls.Add(Me.optDenda)
        Me.PanelControl7.Controls.Add(Me.optJenis)
        Me.PanelControl7.Controls.Add(Me.LabelControl38)
        Me.PanelControl7.Controls.Add(Me.LabelControl39)
        Me.PanelControl7.Controls.Add(Me.LabelControl40)
        Me.PanelControl7.Controls.Add(Me.optProvisi)
        Me.PanelControl7.Controls.Add(Me.LabelControl51)
        Me.PanelControl7.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(766, 234)
        Me.PanelControl7.TabIndex = 20
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(153, 35)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTabungan.Size = New System.Drawing.Size(138, 20)
        Me.cRekeningTabungan.TabIndex = 255
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(15, 39)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl23.TabIndex = 254
        Me.LabelControl23.Text = "Rekening Tabungan"
        '
        'optAutoDebet
        '
        Me.optAutoDebet.Location = New System.Drawing.Point(153, 4)
        Me.optAutoDebet.Name = "optAutoDebet"
        Me.optAutoDebet.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optAutoDebet.Size = New System.Drawing.Size(138, 29)
        Me.optAutoDebet.TabIndex = 252
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(15, 12)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl18.TabIndex = 251
        Me.LabelControl18.Text = "Auto Debet Tabungan"
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(15, 100)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl34.TabIndex = 244
        Me.LabelControl34.Text = "Angsuran Per Bulan"
        '
        'optInstansi
        '
        Me.optInstansi.Location = New System.Drawing.Point(153, 158)
        Me.optInstansi.Name = "optInstansi"
        Me.optInstansi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optInstansi.Size = New System.Drawing.Size(138, 29)
        Me.optInstansi.TabIndex = 230
        '
        'optKaryawan
        '
        Me.optKaryawan.Location = New System.Drawing.Point(153, 126)
        Me.optKaryawan.Name = "optKaryawan"
        Me.optKaryawan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optKaryawan.Size = New System.Drawing.Size(138, 29)
        Me.optKaryawan.TabIndex = 229
        '
        'optDenda
        '
        Me.optDenda.Location = New System.Drawing.Point(153, 58)
        Me.optDenda.Name = "optDenda"
        Me.optDenda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optDenda.Size = New System.Drawing.Size(138, 29)
        Me.optDenda.TabIndex = 228
        '
        'optJenis
        '
        Me.optJenis.Location = New System.Drawing.Point(153, 91)
        Me.optJenis.Name = "optJenis"
        Me.optJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Baru"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Ulangan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. Perpanjangan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&4. Tambahan")})
        Me.optJenis.Size = New System.Drawing.Size(455, 32)
        Me.optJenis.TabIndex = 227
        '
        'LabelControl38
        '
        Me.LabelControl38.Location = New System.Drawing.Point(15, 166)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl38.TabIndex = 217
        Me.LabelControl38.Text = "Kredit Instansi"
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(15, 134)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl39.TabIndex = 215
        Me.LabelControl39.Text = "Kredit Karyawan"
        '
        'LabelControl40
        '
        Me.LabelControl40.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl40.Location = New System.Drawing.Point(15, 66)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl40.TabIndex = 214
        Me.LabelControl40.Text = "Hitung Denda"
        '
        'optProvisi
        '
        Me.optProvisi.Location = New System.Drawing.Point(153, 190)
        Me.optProvisi.Name = "optProvisi"
        Me.optProvisi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optProvisi.Size = New System.Drawing.Size(138, 29)
        Me.optProvisi.TabIndex = 198
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(15, 198)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl51.TabIndex = 164
        Me.LabelControl51.Text = "Pendapatan Provisi Diakui"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Location = New System.Drawing.Point(2, 399)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(777, 33)
        Me.PanelControl2.TabIndex = 193
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(695, 4)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(614, 4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cNoPengajuan
        '
        Me.cNoPengajuan.Location = New System.Drawing.Point(488, 49)
        Me.cNoPengajuan.Name = "cNoPengajuan"
        Me.cNoPengajuan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cNoPengajuan.Size = New System.Drawing.Size(107, 20)
        Me.cNoPengajuan.TabIndex = 272
        '
        'cKode
        '
        Me.cKode.Location = New System.Drawing.Point(488, 71)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKode.Size = New System.Drawing.Size(107, 20)
        Me.cKode.TabIndex = 273
        '
        'trKoreksiDebitur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 434)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trKoreksiDebitur"
        Me.Text = "Koreksi Data Debitur"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.cbJenisUsaha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodePembayaran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbSumberDana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBagianYangDijamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBendahara.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaBendahara.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNoSPK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSifatKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSifatKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.PanelControl7.PerformLayout()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optAutoDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optInstansi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.cNoPengajuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cGolonganKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cBagianYangDijamin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganPenjamin As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganPenjamin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAO As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cBendahara As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaBendahara As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cWilayah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaWilayah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cNoSPK As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cNamaSifatKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSifatKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaJenisPenggunaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSektorEkonomi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaSektorEkonomi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDebitur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganDebitur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cJenisPenggunaan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nPlafond As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbJenisUsaha As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbPeriodePembayaran As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbSumberDana As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbKeterkaitan As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optAutoDebet As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optInstansi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optKaryawan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optDenda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optProvisi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNoPengajuan As DevExpress.XtraEditors.LookUpEdit
End Class
