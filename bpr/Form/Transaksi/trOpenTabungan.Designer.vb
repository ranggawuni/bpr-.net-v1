﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trOpenTabungan
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trOpenTabungan))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cGolonganTabungan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cmbKeterkaitan = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.optKaryawan = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cCabang1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cAO = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cWilayah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaWilayah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.optJenisNasabah = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.cGolonganNasabah = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cGolonganTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenisNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cGolonganTabungan)
        Me.PanelControl1.Controls.Add(Me.cNamaGolonganTabungan)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cmbKeterkaitan)
        Me.PanelControl1.Controls.Add(Me.optKaryawan)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.cCabang1)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNamaGolonganNasabah)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cAO)
        Me.PanelControl1.Controls.Add(Me.cNamaAO)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cWilayah)
        Me.PanelControl1.Controls.Add(Me.cNamaWilayah)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.optJenisNasabah)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Controls.Add(Me.cGolonganNasabah)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(517, 368)
        Me.PanelControl1.TabIndex = 14
        '
        'cGolonganTabungan
        '
        Me.cGolonganTabungan.Location = New System.Drawing.Point(104, 338)
        Me.cGolonganTabungan.Name = "cGolonganTabungan"
        Me.cGolonganTabungan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganTabungan.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganTabungan.TabIndex = 143
        '
        'cNamaGolonganTabungan
        '
        Me.cNamaGolonganTabungan.Enabled = False
        Me.cNamaGolonganTabungan.Location = New System.Drawing.Point(155, 338)
        Me.cNamaGolonganTabungan.Name = "cNamaGolonganTabungan"
        Me.cNamaGolonganTabungan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganTabungan.TabIndex = 142
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 341)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl1.TabIndex = 141
        Me.LabelControl1.Text = "Gol. Tabungan"
        '
        'cmbKeterkaitan
        '
        Me.cmbKeterkaitan.Location = New System.Drawing.Point(104, 286)
        Me.cmbKeterkaitan.Name = "cmbKeterkaitan"
        Me.cmbKeterkaitan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbKeterkaitan.Size = New System.Drawing.Size(138, 20)
        Me.cmbKeterkaitan.TabIndex = 140
        '
        'optKaryawan
        '
        Me.optKaryawan.Location = New System.Drawing.Point(104, 225)
        Me.optKaryawan.Name = "optKaryawan"
        Me.optKaryawan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya")})
        Me.optKaryawan.Size = New System.Drawing.Size(177, 29)
        Me.optKaryawan.TabIndex = 139
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 232)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl6.TabIndex = 138
        Me.LabelControl6.Text = "Karyawan"
        '
        'cCabang1
        '
        Me.cCabang1.EditValue = "12"
        Me.cCabang1.Location = New System.Drawing.Point(104, 35)
        Me.cCabang1.Name = "cCabang1"
        Me.cCabang1.Properties.Mask.EditMask = "##.######"
        Me.cCabang1.Properties.MaxLength = 2
        Me.cCabang1.Size = New System.Drawing.Size(30, 20)
        Me.cCabang1.TabIndex = 135
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(9, 289)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl17.TabIndex = 122
        Me.LabelControl17.Text = "Keterkaitan"
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(104, 113)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 117)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(104, 87)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 139)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Size = New System.Drawing.Size(105, 20)
        Me.cRekening.TabIndex = 113
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 142)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNamaGolonganNasabah
        '
        Me.cNamaGolonganNasabah.Enabled = False
        Me.cNamaGolonganNasabah.Location = New System.Drawing.Point(154, 165)
        Me.cNamaGolonganNasabah.Name = "cNamaGolonganNasabah"
        Me.cNamaGolonganNasabah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganNasabah.TabIndex = 110
        '
        'LabelControl11
        '
        Me.LabelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl11.Location = New System.Drawing.Point(9, 263)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl11.TabIndex = 109
        Me.LabelControl11.Text = "AO"
        '
        'cAO
        '
        Me.cAO.Location = New System.Drawing.Point(104, 260)
        Me.cAO.Name = "cAO"
        Me.cAO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAO.Size = New System.Drawing.Size(44, 20)
        Me.cAO.TabIndex = 108
        '
        'cNamaAO
        '
        Me.cNamaAO.Enabled = False
        Me.cNamaAO.Location = New System.Drawing.Point(155, 260)
        Me.cNamaAO.Name = "cNamaAO"
        Me.cNamaAO.Size = New System.Drawing.Size(141, 20)
        Me.cNamaAO.TabIndex = 107
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 168)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl10.TabIndex = 106
        Me.LabelControl10.Text = "Gol. Nasabah"
        '
        'cWilayah
        '
        Me.cWilayah.Location = New System.Drawing.Point(104, 312)
        Me.cWilayah.Name = "cWilayah"
        Me.cWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cWilayah.Size = New System.Drawing.Size(44, 20)
        Me.cWilayah.TabIndex = 105
        '
        'cNamaWilayah
        '
        Me.cNamaWilayah.Enabled = False
        Me.cNamaWilayah.Location = New System.Drawing.Point(155, 312)
        Me.cNamaWilayah.Name = "cNamaWilayah"
        Me.cNamaWilayah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaWilayah.TabIndex = 104
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 315)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl9.TabIndex = 103
        Me.LabelControl9.Text = "Wilayah"
        '
        'optJenisNasabah
        '
        Me.optJenisNasabah.Location = New System.Drawing.Point(104, 190)
        Me.optJenisNasabah.Name = "optJenisNasabah"
        Me.optJenisNasabah.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Non Instansi"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Instansi")})
        Me.optJenisNasabah.Size = New System.Drawing.Size(177, 29)
        Me.optJenisNasabah.TabIndex = 98
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 198)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl5.TabIndex = 97
        Me.LabelControl5.Text = "Jenis Nasabah"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(104, 61)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(9, 38)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(60, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Register"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(140, 35)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'cGolonganNasabah
        '
        Me.cGolonganNasabah.Location = New System.Drawing.Point(104, 165)
        Me.cGolonganNasabah.Name = "cGolonganNasabah"
        Me.cGolonganNasabah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganNasabah.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganNasabah.TabIndex = 111
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 377)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(517, 33)
        Me.PanelControl2.TabIndex = 13
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(421, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(340, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'trOpenTabungan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 413)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "trOpenTabungan"
        Me.Text = "Pembukaan Tabungan"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cGolonganTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenisNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cCabang1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAO As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cWilayah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaWilayah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optJenisNasabah As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cGolonganNasabah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbKeterkaitan As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents optKaryawan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganTabungan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
End Class
