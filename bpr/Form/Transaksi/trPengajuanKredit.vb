﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trPengajuanKredit
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim cKodeRegister As String
    Private ReadOnly dTglTemp As Date = #1/1/1900#
    Dim dtJadwal As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cWilayah.EditValue = aCfg(eCfg.msKodeDefaultWilayah)
        If nPos = myPos.add Then
            If dTglTemp = #1/1/1900# Then
                dTgl.DateTime = Date.Today
            Else
                dTgl.DateTime = dTglTemp
            End If
        End If
    End Sub

    Private Sub Koreksi(ByVal lKoreksi As Boolean)
        cCabang1.Enabled = lKoreksi
        cNoPengajuan.Enabled = lKoreksi
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCabang1.Text = CStr(aCfg(eCfg.msKodeCabang))
        cKode.Text = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cNoPengajuan.EditValue = ""
        cWilayah.Text = ""
        cNamaWilayah.Text = ""
        cAO.EditValue = ""
        cNamaAO.Text = ""
        nPlafond.Text = ""
        nLama.Text = ""
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Benar-benar akan Dihapus", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            objData.Delete(GetDSN, "pengajuankredit", "Rekening", , cNoPengajuan.Text)
            InitValue()
            MessageBox.Show("Data Sudah Dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        GetEdit(False)
        InitValue()
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
            cKode.Focus()
            Exit Function
        End If

        If cNoPengajuan.Text.Length < 9 Then
            Return False
            cNoPengajuan.Focus()
            Exit Function
        End If

        If Not CheckData(cWilayah.Text, "Wilayah tidak boleh kosong...") Then
            ValidSaving = False
            cWilayah.Focus()
            Exit Function
        End If
        If Not CheckData(cAO.Text, "Account officer tidak boleh kosong...") Then
            ValidSaving = False
            cAO.Focus()
            Exit Function
        End If
        If Not CheckData(nPlafond.Text, "Plafond tidak boleh 0...") Then
            ValidSaving = False
            nPlafond.Focus()
            Exit Function
        End If
        If Not CheckData(nLama.Text, "Lama kredit tidak boleh 0...") Then
            ValidSaving = False
            nLama.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cKodeTemp As String = String.Format("{0}.{1}", cCabang1.Text, cKode.Text)
                    UpdPengajuanKredit(cNoPengajuan.Text, dTgl.DateTime, cKodeTemp, cWilayah.Text, cAO.Text, CDbl(nPlafond.Text),
                                       CDbl(nLama.Text), cUserName, CDate(SNow()))
                Else
                    cNoPengajuan.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Function GetFrekuensi() As String
        Dim cTemp As String = ""
        Dim nFrekuensi As Integer = 1
        Dim cCabangTemp As String
        If Len(cNoPengajuan.Text) = 9 Then
            cCabangTemp = cNoPengajuan.Text.Substring(0, 2)
        Else
            cCabangTemp = CStr(aCfg(eCfg.msKodeCabang))
        End If
        Dim cDataTable = objData.Browse(GetDSN, "pengajuankredit", "max(rekening) as kodeKantor", "left(rekening,2)", data.myOperator.Assign, cCabangTemp)
        With cDataTable
            If .Rows.Count > 0 Then
                cTemp = .Rows(0).Item("kodeKantor").ToString.Substring(4)
                nFrekuensi = CInt(cTemp) + 1
            End If
        End With
        Dim cKodeCabangTemp As String = CStr(aCfg(eCfg.msKodeCabang))
        cTemp = String.Format("{0}.{1}", Padl(cKodeCabangTemp, 2, "0"), Padl(CStr(nFrekuensi), 6, "0"))
        Return cTemp
    End Function

    Private Sub GetMemory()
        Dim cField As String = "p.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,"
        cField = cField & "w.Keterangan as NamaWilayah,"
        cField = cField & "a.Nama as NamaAO"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on r.Kode = p.Kode",
                                  "Left Join Wilayah w on p.Wilayah = w.Kode",
                                  "Left Join AO a on p.AO = a.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "Deposito d", cField, "d.rekening", data.myOperator.Assign, cNoPengajuan.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cCabang1.Text = .Item("kode").ToString.Substring(1, 2)
                cKode.Text = .Item("kode").ToString.Substring(4)
                cNama.Text = .Item("nama").ToString
                cAlamat.Text = .Item("alamat").ToString
                cTelepon.Text = .Item("telepon").ToString
                dTgl.DateTime = CDate(.Item("tgl").ToString)
                cWilayah.EditValue = .Item("wilayah").ToString
                cNamaWilayah.Text = .Item("NamaWilayah").ToString
                cAO.EditValue = .Item("AO").ToString
                cNamaAO.Text = .Item("NamaAO").ToString
                nPlafond.Text = .Item("Plafond").ToString
                nLama.Text = .Item("Lama").ToString
            End With
        End If
    End Sub

    Private Sub trPengajuanKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("AO", cAO, , "kode,nama")
        GetDataLookup("Wilayah", cWilayah)
    End Sub

    Private Sub trPengajuanKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cNoPengajuan.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(nPlafond.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cCabang1.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cNoPengajuan.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        nPlafond.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cKode.Focus()
        Koreksi(True)
        cCabang1.Enabled = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cNoPengajuan.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cNoPengajuan.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
            cKodeRegister = setKode(cCabang1.Text, cKode.Text)
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "nama,alamat,telepon", "kode", data.myOperator.Assign, cKodeRegister)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cTelepon.Text = .Item("Telepon").ToString
                End With
            Else
                MessageBox.Show("No. Register tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                cAlamat.Text = ""
                cTelepon.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cNoPengajuan_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cNoPengajuan.GotFocus
        If nPos = myPos.add Then
            cNoPengajuan.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cAO_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cAO.Closed
        GetKeteranganLookUp(cAO, cNamaAO, , "nama")
    End Sub

    Private Sub cAO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cAO.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cAO, cNamaAO, , "nama")
        End If
    End Sub

    Private Sub cWilayah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cWilayah.Closed
        GetKeteranganLookUp(cWilayah, cNamaWilayah)
    End Sub

    Private Sub cWilayah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cWilayah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cWilayah, cNamaWilayah)
        End If
    End Sub

    Private Sub cNoPengajuan_KeyDown(sender As Object, e As KeyEventArgs) Handles cNoPengajuan.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim dbData As New DataTable
            Dim cAOTemp As String = ""
            Dim cNamaAOTemp As String = ""
            dbData = objData.Browse(GetDSN, "PengajuanKredit", "Rekening,kode", "Rekening", , cNoPengajuan.Text)
            If dbData.Rows.Count > 0 Then
                If GetStatusPengajuan(dbData.Rows(0).Item("Kode").ToString, cAOTemp, cNamaAOTemp) Then
                    Dim cKet As String = String.Format("Calon Debitur SUDAH PERNAH DITOLAK Sebelumnya.{0}Dan Sudah DISURVEY OLEH {1} ( {2} ) {0}Anda Yakin Akan Melanjutkan ?", vbCrLf, cAOTemp, cNamaAOTemp)
                    If MessageBox.Show(cKet, "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbNo Then
                        cmdKeluar_Click(sender, e)
                    End If
                Else

                    If nPos = myPos.add Then
                        MessageBox.Show("Nomor Pengajuan sudah ada. Silahkan Ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        cNoPengajuan.Text = ""
                        cNoPengajuan.Focus()
                        dbData.Dispose()
                        Exit Sub
                    End If
                    GetMemory()
                    If nPos = myPos.delete Then
                        DeleteData()
                    End If
                End If
                dbData.Dispose()
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Data Tidak Ada. Silahkan mengulang pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cNoPengajuan.Text = ""
                cNoPengajuan.Focus()
                dbData.Dispose()
                Exit Sub
            End If
        End If
    End Sub

    Private Function GetStatusPengajuan(ByVal cKode As String, ByRef cKodeAO As String, ByRef cNamaAOTemp As String) As Boolean
        Dim db As New DataTable
        GetStatusPengajuan = False
        Const cField As String = "p.ao,a.Nama"
        Const cWhere As String = " and statuspengajuan='2'"
        Dim vaJoin() As Object = {"left join ao a on a.kode=p.ao"}
        db = objData.Browse(GetDSN, "PengajuanKredit p", cField, "p.kode", , cKode, cWhere, "p.Rekening desc limit 1", vaJoin)
        If db.Rows.Count > 0 Then
            GetStatusPengajuan = True
            cKodeAO = GetNull(db.Rows(0).Item("AO"), "").ToString
            cNamaAOTemp = GetNull(db.Rows(0).Item("Nama"), "").ToString
        End If
    End Function
End Class