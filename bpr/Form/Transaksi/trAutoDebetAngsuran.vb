﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trAutoDebetAngsuran
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim dtTable As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        dDate.DateTime = Date.Today
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nama", System.Type.GetType("System.String"))
        AddColumn(dtTable, "TglRealisasi", System.Type.GetType("System.DateTime"))
        AddColumn(dtTable, "BakiDebet", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "RekeningTabungan", System.Type.GetType("System.String"))
        AddColumn(dtTable, "SaldoTabungan", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Total", System.Type.GetType("System.Double"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim dbAgs As New DataTable
            Dim row As DataRow = dtTable.NewRow()
            Dim nPokok As Double = 0
            Dim nBunga As Double = 0
            Dim cWhere As String = ""
            Dim nBakiDebet As Double = 0
            Dim lPosted As Boolean
            Dim nSaldoTabungan As Double = 0
            Dim cField As String = "d.*,g.SaldoMinimum,r.Nama as NamaDebitur"
            cWhere = String.Format(" and date_format(d.tgl,'%d') = '{0}' ", Format(dDate.DateTime, "dd"))
            Dim vaJoin() As Object = {"Left Join Tabungan t on d.RekeningTabungan = t.Rekening",
                                      "Left Join GolonganTabungan g on g.Kode = Mid(d.RekeningTabungan,4,2)",
                                      "Left Join RegisterNasabah r on r.Kode = d.Kode"}
            dtData = objData.Browse(GetDSN, "debitur t", cField, "t.instansi", , "Y", cWhere & " having saldopokok>0", "t.Tgl,t.Rekening", vaJoin)
            If Not dtData.Rows.Count > 0 Then
                dTglTemp = DateAdd(DateInterval.Month, 0, dTgl.DateTime)
                For n As Integer = 0 To dtData.Rows.Count - 1
                    nPokok = 0
                    nBunga = 0
                    With dtData.Rows(n)
                        nBakiDebet = GetBakiDebet(.Item("Rekening").ToString, dTgl.DateTime)
                        If nBakiDebet > 0 Then
                            lPosted = False
                            cField = "kpokok,kbunga"
                            dbAgs = objData.Browse(GetDSN, "angsuran", cField, "Rekening", , .Item("Rekening").ToString, " and AutoDebet='Y'")
                            If dbAgs.Rows.Count > 0 Then
                                If CDbl(GetNull(dbAgs.Rows(0).Item("KPokok"))) + CDbl(GetNull(dbAgs.Rows(0).Item("KBunga"))) > 0 Then
                                    lPosted = True
                                End If
                            End If
                            nSaldoTabungan = GetSaldoTabungan(.Item("RekeningTabungan").ToString, dTgl.DateTime)
                            GetKewajibanPokokBunga(.Item("Rekening").ToString, dDate.DateTime, nBunga, nPokok, True)
                            If nPokok + nBunga > 0 And Not lPosted Then
                                If nPokok + nBunga > 0 And nSaldoTabungan >= nPokok + nBunga + CDbl(GetNull(.Item("SaldoMinimum"), 0)) Then
                                    row("Rekening") = .Item("Rekening").ToString
                                    row("Nama") = .Item("Nama").ToString
                                    row("TglRealisasi") = CDate(.Item("Tgl"))
                                    row("BakiDebet") = nBakiDebet
                                    row("RekeningTabungan") = .Item("RekeningTabungan").ToString
                                    row("SaldoTabungan") = nSaldoTabungan
                                    row("Pokok") = nPokok
                                    row("Bunga") = nBunga
                                    row("Total") = nPokok + nBunga
                                    dtTable.Rows.Add(row)
                                End If
                            End If
                        End If
                    End With
                Next
            End If
            GridControl1.DataSource = dtTable
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "TglRealisasi", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, , HorzAlignment.Center)
            GridColumnFormat(GridView1, "BakiDebet", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "RekeningTabungan")
            GridColumnFormat(GridView1, "SaldoTabungan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Total", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            dbAgs.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim cFaktur As String = ""
        Dim cKeterangan As String = ""
        Dim cKodeCabang As String = ""
        Dim cStatusPembayaran As String = ""

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    cFaktur = GetLastFaktur(eFaktur.fkt_Angsuran, dTgl.DateTime, , True)
                    cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                    If CDbl(nRow("Pokok")) = CDbl(nRow("BakiDebet")) Then
                        cStatusPembayaran = "3"
                    Else
                        cStatusPembayaran = "2"
                    End If
                    cKeterangan = "Auto Debet Angsuran an. " & nRow("Rekening").ToString
                    UpdAngsuranPembiayaan(cKodeCabang, cFaktur, dTgl.DateTime, nRow("Rekening").ToString, eAngsuran.ags_Angsuran,
                                          CDbl(nRow("Pokok")), CDbl(nRow("Bunga")), 0, cKeterangan, , , , 0, 0, , , cStatusPembayaran,
                                          , , , , , True)
                End If
            Next
            MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
        End If
    End Sub

    Private Sub trAutoDebetAngsuran_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar, , cmdPreview, , , , , , , cmdRefresh)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dDate.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdPreview.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        dDate.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub trAngsuranInstansi_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        'Me.WindowState = FormWindowState.Maximized
        PanelControl1.Width = Width - 20
        GridControl1.Width = PanelControl1.Width '- 100
        PanelControl4.Width = PanelControl1.Width
        GridControl1.Height = Height - PanelControl1.Height - PanelControl4.Height - 60
        PanelControl4.Top = GridControl1.Height + PanelControl1.Height + 15
        cmdSimpan.Left = PanelControl4.Width - cmdSimpan.Width - cmdKeluar.Width - 20
        cmdKeluar.Left = PanelControl4.Width - cmdKeluar.Width - 10
    End Sub
End Class