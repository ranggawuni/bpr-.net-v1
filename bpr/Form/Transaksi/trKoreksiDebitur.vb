﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trKoreksiDebitur
    ReadOnly objData As New data()
    Dim dtJadwal As New DataTable
    Dim cKodeRegisterLama As String
    Dim cRekeningTemp As String
    Dim dTanggalRealisasi As Date
    Dim nPlafondLama As Double

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        nPlafond.Text = ""
        cNoSPK.Text = ""
        cGolonganKredit.EditValue = ""
        cNamaGolonganKredit.Text = ""
        cSifatKredit.EditValue = ""
        cNamaSifatKredit.Text = ""
        cJenisPenggunaan.EditValue = ""
        cNamaJenisPenggunaan.Text = ""
        cGolonganDebitur.EditValue = ""
        cNamaGolonganDebitur.Text = ""
        cSektorEkonomi.EditValue = ""
        cNamaSektorEkonomi.Text = ""
        cWilayah.EditValue = ""
        cNamaWilayah.Text = ""
        cBendahara.EditValue = ""
        cNamaBendahara.Text = ""
        cAO.EditValue = ""
        cNamaAO.Text = ""
        cGolonganPenjamin.EditValue = ""
        cNamaGolonganPenjamin.Text = ""
        cBagianYangDijamin.Text = ""
        cNoPengajuan.EditValue = ""
        cKode.EditValue = ""
        cbKeterkaitan.EditValue = ""
        cbSumberDana.EditValue = ""
        cbPeriodePembayaran.EditValue = ""
        cbJenisUsaha.EditValue = ""
        optAutoDebet.SelectedIndex = 1
        cRekeningTabungan.EditValue = ""
        optDenda.SelectedIndex = 1
        optJenis.SelectedIndex = 1
        optKaryawan.SelectedIndex = 2
        optInstansi.SelectedIndex = 2
        optProvisi.SelectedIndex = 1
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(cNoSPK.Text, "Nomor PK harus diisi. Ulangi pengisian.") Then
            Return False
            cNoSPK.Focus()
            Exit Function
        End If

        If Not CheckData(cSifatKredit.Text, "Sifat Kredit harus diisi. Ulangi pengisian.") Then
            Return False
            cSifatKredit.Focus()
            Exit Function
        End If

        If Not CheckData(cJenisPenggunaan.Text, "Jenis Penggunaan harus diisi. Ulangi pengisian.") Then
            Return False
            cJenisPenggunaan.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganDebitur.Text, "Golongan Debitur harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganDebitur.Focus()
            Exit Function
        End If

        If Not CheckData(cSektorEkonomi.Text, "Sektor Ekonomi harus diisi. Ulangi pengisian.") Then
            Return False
            cSektorEkonomi.Focus()
            Exit Function
        End If

        If Not CheckData(cWilayah.Text, "Wilayah harus diisi. Ulangi pengisian.") Then
            Return False
            cWilayah.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganPenjamin.Text, "Golongan penjamin harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganPenjamin.Focus()
            Exit Function
        End If

        If Not CheckData(cAO.Text, "AO harus diisi. Ulangi pengisian.") Then
            Return False
            cAO.Focus()
            Exit Function
        End If

        'If Debugger.IsAttached Then
        'Dim nJumlahTemp As Double = 999999999999
        'Dim cTemp As String = ""
        'If cKodeRegisterLama <> cKode.Text Then
        '    cTemp = String.Format("{0}~{1}", cKode.Text, cKodeRegisterLama)
        '    ValidSaving = SendRequest(acGantiRegisterKredit, cTemp, nJumlahTemp, dTgl.DateTime, cRekening, "")
        'End If
        'If dTanggalRealisasi <> dTgl.DateTime Then
        '    cTemp = String.Format("{0}~{1}", dTanggalRealisasi, dTgl.DateTime)
        '    ValidSaving = SendRequest(acGantiRegisterKredit, cTemp, nJumlahTemp, dTgl.DateTime, cRekening, "")
        'End If
        'End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim cJenisProvisi As String = GetOpt(optProvisi)
                Dim cAutoDebet As String = GetOpt(optAutoDebet)
                Dim cKeterkaitanTemp As String = cbKeterkaitan.Text.Substring(0, 1)
                Dim cSumberDanaTemp As String = cbSumberDana.Text.Substring(0, 2)
                Dim cPeriodePembayaranTemp As String = cbPeriodePembayaran.Text.Substring(0, 1)
                Dim cJenisUsahaTemp As String = cbJenisUsaha.Text.Substring(0, 1)
                Dim cKaryawan As String = GetOpt(optKaryawan)
                Dim cInstansi As String = GetOpt(optInstansi)
                Dim cJenisRealisasi As String = GetOpt(optJenis)
                Dim cHitungDenda As String = GetOpt(optDenda)
                Dim vaField() As Object = {"NOSPK", "SifatKredit", "JenisPenggunaan",
                                           "GolonganDebitur", "SektorEkonomi", "Wilayah",
                                           "AO", "GolonganPenjamin", "Tgl",
                                           "BagianYangDijamin", "GolonganKredit", "RekeningTabungan",
                                           "Kode", "StatusRealisasi", "Bendahara",
                                           "NoPengajuan", "Karyawan", "Instansi",
                                           "JenisProvisi", "AutoDebet", "SumberDana",
                                           "PeriodePembayaran", "JenisUsaha", "Keterkaitan",
                                           "HitungDenda"}
                Dim vavalue() As Object = {cNoSPK.Text, cSifatKredit.Text, cJenisPenggunaan.Text,
                                           cGolonganDebitur.Text, cSektorEkonomi.Text, cWilayah.Text,
                                           cAO.Text, cGolonganPenjamin.Text, dTgl.DateTime,
                                           cBagianYangDijamin.Text, cGolonganKredit.Text, cRekeningTabungan.Text,
                                           cKode.Text, cJenisRealisasi, cBendahara.Text,
                                           cNoPengajuan.Text, cKaryawan, cInstansi,
                                           cJenisProvisi, cAutoDebet, cSumberDanaTemp,
                                           cPeriodePembayaranTemp, cJenisUsahaTemp, cKeterkaitanTemp,
                                           cHitungDenda}
                Dim cWhere As String = String.Format("Rekening = '{0}'", cRekening.Text)
                objData.Edit(GetDSN, "Debitur", cWhere, vaField, vavalue)
                InitValue()
            Else
                cRekening.Focus()
                Exit Sub
            End If
            InitValue()
        End If
    End Sub

    Private Sub GetMemory()
        Dim cField As String = "d.*,d.Kode as KodeDebitur,r.Nama, r.Alamat,w.Keterangan as NamaWilayah,"
        cField = cField & "a.Keterangan as NamaGolonganKredit,c.Keterangan as NamaGolonganDebitur,"
        cField = cField & "e.Keterangan as NamaSektorEkonomi,f.Keterangan as NamaSifatKredit,"
        cField = cField & "l.Keterangan as NamaGolonganPenjamin,"
        cField = cField & "g.Keterangan as NamaJenisPenggunaan,h.Nama as NamaAO,j.Nama as NamaNasabah,j.Alamat as AlamatNasabah,"
        cField = cField & "t.Rekening as RekeningTab,t.JumlahBlokir, t.Kode,d.Rekening as RekTemp,"
        cField = cField & "p.Jaminan as JaminanPengajuan,p.Plafond as PlafondPengajuan,b.nama as NamaBendahara,ch.keterangan as NamaCaraHitung"
        Dim vaJoin() As Object = {"Left Join Wilayah w on w.Kode = d.Wilayah",
                                  "Left Join RegisterNasabah r on r.Kode = d.Kode",
                                  "Left Join GolonganKredit a on a.Kode = d.GolonganKredit",
                                  "Left Join GolonganDebitur c on c.Kode = d.GolonganDebitur",
                                  "Left Join SektorEkonomi e on e.Kode = d.SektorEkonomi",
                                  "Left Join SifatKredit f on f.Kode = d.SifatKredit",
                                  "Left Join JenisPenggunaan g on g.Kode = d.JenisPenggunaan",
                                  "Left Join GolonganPenjamin l on l.Kode = d.GolonganPenjamin",
                                  "Left Join AO h on h.Kode = d.AO",
                                  "Left Join Tabungan t on t.Rekening = d.RekeningTabungan",
                                  "Left Join RegisterNasabah j on j.Kode = t.Kode",
                                  "Left Join PengajuanKredit p on p.Rekening = d.NoPengajuan",
                                  "Left Join Bendahara b on b.kode = d.bendahara",
                                  "Left Join CaraHitung ch on ch.kode = d.caraperhitungan"}
        Dim cDataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cKodeRegisterLama = .Item("KodeDebitur").ToString
                cRekeningTemp = .Item("RekTemp").ToString
                dTanggalRealisasi = CDate(.Item("Tgl"))
                nPlafondLama = CDbl(.Item("Plafond"))
                cKode.EditValue = .Item("KodeDebitur").ToString
                cNama.Text = .Item("nama").ToString
                cAlamat.Text = .Item("alamat").ToString
                cTelepon.Text = .Item("telepon").ToString
                dTgl.DateTime = CDate(.Item("tgl").ToString)
                cNoPengajuan.EditValue = .Item("NoPengajuan").ToString
                cSifatKredit.EditValue = .Item("SifatKredit").ToString
                cNamaSifatKredit.Text = .Item("NamaSifatKredit").ToString
                cJenisPenggunaan.EditValue = .Item("JenisPenggunaan").ToString
                cNamaJenisPenggunaan.Text = .Item("NamaJenisPenggunaan").ToString
                cGolonganDebitur.EditValue = .Item("GolonganDebitur").ToString
                cNamaGolonganDebitur.Text = .Item("NamaGolonganDebitur").ToString
                cSektorEkonomi.EditValue = .Item("SektorEkonomi").ToString
                cNamaSektorEkonomi.Text = .Item("NamaSektorEkonomi").ToString
                cWilayah.EditValue = .Item("wilayah").ToString
                cNamaWilayah.Text = .Item("NamaWilayah").ToString
                cGolonganKredit.EditValue = .Item("GolonganKredit").ToString
                cNamaGolonganKredit.Text = .Item("NamaGolonganKredit").ToString
                cBendahara.EditValue = .Item("Bendahara").ToString
                cNamaBendahara.Text = .Item("NamaBendahara").ToString
                cAO.EditValue = .Item("AO").ToString
                cNamaAO.Text = .Item("NamaAO").ToString
                cGolonganPenjamin.Text = .Item("GolonganPenjamin").ToString
                cNamaGolonganPenjamin.Text = .Item("NamaGolonganPenjamin").ToString
                cBagianYangDijamin.Text = .Item("BagianYangDijamin").ToString
                cRekeningTabungan.EditValue = .Item("RekeningTabungan").ToString

                For n = 0 To cbKeterkaitan.Properties.Items.Count
                    If cbKeterkaitan.Properties.Items(n).ToString.Substring(1, 1) = .Item("Keterkaitan").ToString Then
                        cbKeterkaitan.EditValue = cbKeterkaitan.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbSumberDana.Properties.Items.Count - 1
                    If cbSumberDana.Properties.Items(n).ToString = .Item("SumberDana").ToString Then
                        cbSumberDana.Text = cbSumberDana.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbPeriodePembayaran.Properties.Items.Count - 1
                    If cbPeriodePembayaran.Properties.Items(n).ToString = .Item("PeriodePembayaran").ToString Then
                        cbPeriodePembayaran.Text = cbPeriodePembayaran.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbJenisUsaha.Properties.Items.Count - 1
                    If cbJenisUsaha.Properties.Items(n).ToString = .Item("JenisUsaha").ToString Then
                        cbJenisUsaha.Text = cbJenisUsaha.Properties.Items(n).ToString
                    End If
                Next

                nPlafond.Text = .Item("Plafond").ToString
                SetOpt(optDenda, .Item("HitungDenda").ToString)
                SetOpt(optJenis, .Item("StatusRealisasi").ToString)
                SetOpt(optKaryawan, .Item("karyawan").ToString)
                SetOpt(optInstansi, .Item("Instansi").ToString)
                SetOpt(optProvisi, .Item("JenisProvisi").ToString)
            End With
        End If
    End Sub

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cbKeterkaitan.Properties
        With properties
            .Items.AddRange(New String() {"1 - Terkait", "2 - Tidak Terkait"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbKeterkaitan.SelectedIndex = 1

        properties = cbSumberDana.Properties
        With properties
            .Items.AddRange(New String() {"10 - Gaji/Honor", "21 - Usaha Subdisi", "22 - Usaha Non Subsidi", "31 - Lainnya Subsidi", "32 - Lainnya Non Subsidi"})
        End With
        cbSumberDana.SelectedIndex = 1

        properties = cbPeriodePembayaran.Properties
        With properties
            .Items.AddRange(New String() {"1 - Harian", "2 - Mingguan", "3 - Bulanan", "4 - Triwulanan", "5 - Semesteran",
                                          "6 - Tahunan", "7 - Sekaligus", "8 - Setiap saat"})
        End With
        cbPeriodePembayaran.SelectedIndex = 1

        properties = cbJenisUsaha.Properties
        With properties
            .Items.AddRange(New String() {"1 - Mikro", "2 - Kecil", "3 - Menengah", "4 - Selain Usaha Mikro, Kecil dan Menengah"})
        End With
        cbJenisUsaha.SelectedIndex = 1

    End Sub

    Private Sub trKoreksiDebitur_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("PengajuanKredit", cNoPengajuan, , "rekening,nominal")
        GetDataLookup("RegisterNasabah", cKode, , "kode,nama,alamat,noktp")
        GetDataLookup("GolonganKredit", cGolonganKredit)
        GetDataLookup("SifatKredit", cSifatKredit)
        GetDataLookup("JenisPenggunaan", cJenisPenggunaan)
        GetDataLookup("GolonganDebitur", cGolonganDebitur)
        GetDataLookup("SektorEkonomi", cSektorEkonomi)
        GetDataLookup("Wilayah", cWilayah)
        GetDataLookup("Bendahara", cBendahara, "kode,nama")
        GetDataLookup("AO", cAO, , "kode,nama")
        GetDataLookup("GolonganPenjamin", cGolonganPenjamin)
    End Sub

    Private Sub trKoreksiDebitur_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)
        InitValue()
        IsiCombo()

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nPlafond.TabIndex, n)
        SetTabIndex(cNoSPK.TabIndex, n)
        SetTabIndex(cGolonganKredit.TabIndex, n)
        SetTabIndex(cSifatKredit.TabIndex, n)
        SetTabIndex(cJenisPenggunaan.TabIndex, n)
        SetTabIndex(cGolonganDebitur.TabIndex, n)
        SetTabIndex(cSektorEkonomi.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cBendahara.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(cGolonganPenjamin.TabIndex, n)
        SetTabIndex(cBagianYangDijamin.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cNoPengajuan.TabIndex, n)
        SetTabIndex(cbKeterkaitan.TabIndex, n)
        SetTabIndex(cbSumberDana.TabIndex, n)
        SetTabIndex(cbPeriodePembayaran.TabIndex, n)
        SetTabIndex(cbJenisUsaha.TabIndex, n)
        SetTabIndex(optAutoDebet.TabIndex, n)
        SetTabIndex(cRekeningTabungan.TabIndex, n)
        SetTabIndex(optDenda.TabIndex, n)
        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(optKaryawan.TabIndex, n)
        SetTabIndex(optInstansi.TabIndex, n)
        SetTabIndex(optProvisi.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        nPlafond.EnterMoveNextControl = True
        cNoSPK.EnterMoveNextControl = True
        cGolonganKredit.EnterMoveNextControl = True
        cSifatKredit.EnterMoveNextControl = True
        cJenisPenggunaan.EnterMoveNextControl = True
        cGolonganDebitur.EnterMoveNextControl = True
        cSektorEkonomi.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cBendahara.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        cGolonganPenjamin.EnterMoveNextControl = True
        cBagianYangDijamin.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        cNoPengajuan.EnterMoveNextControl = True
        cbKeterkaitan.EnterMoveNextControl = True
        cbSumberDana.EnterMoveNextControl = True
        cbPeriodePembayaran.EnterMoveNextControl = True
        cbJenisUsaha.EnterMoveNextControl = True
        optAutoDebet.EnterMoveNextControl = True
        cRekeningTabungan.EnterMoveNextControl = True
        optDenda.EnterMoveNextControl = True
        optJenis.EnterMoveNextControl = True
        optKaryawan.EnterMoveNextControl = True
        optInstansi.EnterMoveNextControl = True
        optProvisi.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cGolonganNasabah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cJenisPenggunaan.Closed
        GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
    End Sub

    Private Sub cJenisPenggunaan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cJenisPenggunaan.Closed
        GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
    End Sub

    Private Sub cGolonganNasabah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cJenisPenggunaan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
        End If
    End Sub

    Private Sub cJenisPenggunaan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cJenisPenggunaan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cJenisPenggunaan, cNamaJenisPenggunaan)
        End If
    End Sub

    Private Sub cAO_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cAO.Closed
        GetKeteranganLookUp(cAO, cNamaAO, , "nama")
    End Sub

    Private Sub cAO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cAO.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cAO, cNamaAO, , "nama")
        End If
    End Sub

    Private Sub cWilayah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cWilayah.Closed
        GetKeteranganLookUp(cWilayah, cNamaWilayah)
    End Sub

    Private Sub cWilayah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cWilayah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cWilayah, cNamaWilayah)
        End If
    End Sub

    Private Sub cRekening_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim dbData As New DataTable
            dbData = objData.Browse(GetDSN, "Debitur", "statusPencairan", "Rekening", , cRekening.Text)
            If dbData.Rows.Count > 0 Then
                GetMemory()
                dbData.Dispose()
            ElseIf dbData.Rows.Count = 0 Then
                MessageBox.Show("Data Tidak Ada. Silahkan mengulang pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cRekening.EditValue = ""
                cRekening.Focus()
                dbData.Dispose()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cSifatKredit_Closed(sender As Object, e As ClosedEventArgs) Handles cSifatKredit.Closed
        GetKeteranganLookUp(cSifatKredit, cNamaSifatKredit)
    End Sub

    Private Sub cSifatKredit_KeyDown(sender As Object, e As KeyEventArgs) Handles cSifatKredit.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cSifatKredit, cNamaSifatKredit)
        End If
    End Sub

    Private Sub cGolonganDebitur_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganDebitur.Closed
        GetKeteranganLookUp(cGolonganDebitur, cNamaGolonganDebitur)
    End Sub

    Private Sub cGolonganDebitur_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganDebitur.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganDebitur, cNamaGolonganDebitur)
        End If
    End Sub

    Private Sub cSektorEkonomi_Closed(sender As Object, e As ClosedEventArgs) Handles cSektorEkonomi.Closed
        GetKeteranganLookUp(cSektorEkonomi, cNamaSektorEkonomi)
    End Sub

    Private Sub cSektorEkonomi_KeyDown(sender As Object, e As KeyEventArgs) Handles cSektorEkonomi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cSektorEkonomi, cNamaSektorEkonomi)
        End If
    End Sub

    Private Sub cBendahara_Closed(sender As Object, e As ClosedEventArgs) Handles cBendahara.Closed
        GetKeteranganLookUp(cBendahara, cNamaBendahara)
    End Sub

    Private Sub cBendahara_KeyDown(sender As Object, e As KeyEventArgs) Handles cBendahara.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cBendahara, cNamaBendahara)
        End If
    End Sub

    Private Sub cGolonganPenjamin_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganPenjamin.Closed
        GetKeteranganLookUp(cGolonganPenjamin, cNamaGolonganPenjamin)
    End Sub

    Private Sub cGolonganPenjamin_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganPenjamin.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganPenjamin, cNamaGolonganPenjamin)
        End If
    End Sub

    Private Sub cGolonganKredit_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganKredit.Closed
        GetKeteranganLookUp(cGolonganKredit, cNamaGolonganKredit)
    End Sub

    Private Sub cGolonganKredit_KeyDown(sender As Object, e As KeyEventArgs) Handles cGolonganKredit.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganKredit, cNamaGolonganKredit)
        End If
    End Sub
End Class