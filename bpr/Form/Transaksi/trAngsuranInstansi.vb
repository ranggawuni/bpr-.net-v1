﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trAngsuranInstansi
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim dtTable As New DataTable
    Private ReadOnly x As TypeKolektibilitas

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cInstansi.EditValue = ""
        cNamaInstansi.Text = ""
        cBendahara.EditValue = ""
        cNamaBendahara.Text = ""
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nama", System.Type.GetType("System.String"))
        AddColumn(dtTable, "TunggakanPokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TunggakanBunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TotalTunggakan", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TotalAngsuran", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "SaldoPokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(dtTable, "Status", System.Type.GetType("System.String"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim row As DataRow = dtTable.NewRow()
            Dim nKe As Double = 0
            Dim nPokok As Double = 0
            Dim nBunga As Double = 0
            Dim cWhere As String = ""
            Dim vaJoin() As Object = Nothing
            Dim cField As String = String.Format("t.rekening,if(t.caraperhitungan<>'7', t.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = t.rekening AND (a.status = {0} or status = {1}) AND a.tgl <= '{2}'),0),", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = t.rekening and a.tgl <= '{1}'),0)) as SaldoPokok,", cField, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = cField & "r.Nama,t.kode,t.rekeningtabungan"
            If cBendahara.Text = "" Then
                cWhere = String.Format("and r.instansi ='{0}'", cInstansi.Text)
                vaJoin = {"left join registernasabah r on t.kode=r.kode",
                          "Left Join Bendahara b on b.kode = t.bendahara"}
            Else
                cWhere = String.Format("and r.instansi ='{0}' and t.bendahara = '{1}'", cInstansi.Text, cBendahara.Text)
                vaJoin = {"left join registernasabah r on t.kode=r.kode",
                          "Left Join Bendahara b on b.kode = t.bendahara"}
            End If
            cWhere = String.Format("{0} and t.Tgl <= '{1}' and t.statuspencairan <> 0 ", cWhere, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = cWhere & " and (t.bendahara <> '' and t.bendahara <> '999') "
            dtData = objData.Browse(GetDSN, "debitur t", cField, "t.instansi", , "Y", cWhere & " having saldopokok>0", "t.Tgl,t.Rekening", vaJoin)
            If Not dtData.Rows.Count > 0 Then
                dTglTemp = DateAdd(DateInterval.Month, 0, dTgl.DateTime)
                For n As Integer = 0 To dtData.Rows.Count - 1
                    nPokok = 0
                    nBunga = 0
                    With dtData.Rows(n)
                        row("Rekening") = .Item("Rekening").ToString
                        row("Nama") = .Item("Nama").ToString
                        GetTunggakan(.Item("Rekening").ToString, dTgl.DateTime, x, , , True, , .Item("Kode").ToString)
                        row("TunggakanPokok") = x.nTunggakanPokok
                        row("TunggakanBunga") = x.nTunggakanBunga
                        row("TotalTunggakan") = x.nTunggakanPokok + x.nTunggakanBunga
                        GetKewajibanPokokBunga(.Item("Rekening").ToString, dTgl.DateTime, nBunga, nPokok, True, nKe)
                        row("Pokok") = nPokok
                        row("Bunga") = nBunga
                        row("TotalAngsuran") = nPokok + nBunga
                        row("SaldoPokok") = CDbl(.Item("SaldoPokok"))
                        row("Ke") = nKe
                        row("Status") = ""
                        dtTable.Rows.Add(row)
                    End With
                Next
            End If
            GridControl1.DataSource = dtTable
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "TunggakanPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TunggakanBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TotalTunggakan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TotalAngsuran", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "SaldoPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Ke", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status")
            GridColumnFormat(GridView1, "RekeningTabungan")
            GridColumnFormat(GridView1, "SaldoTabungan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim cFaktur As String = ""
        Dim cKeterangan As String = ""
        Dim cKodeCabang As String = ""
        Dim lPass1 As Boolean, lPass2 As Boolean
        Dim cKet1 As String, cKet2 As String
        Dim nBakiDebet As Double = 0
        Dim cStatusPembayaran As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cWhere As String

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    lPass1 = True : lPass2 = True : cKet1 = "" : cKet2 = ""
                    nBakiDebet = GetBakiDebet(nRow(0).ToString, dTgl.DateTime)
                    If nBakiDebet <= 0 Then
                        lPass1 = False
                        cKet1 = "Bakidebet Sudah 0"
                    End If
                    If CDbl(nRow(5)) > nBakiDebet Then 'adjie@03-04-2014 jika angsuran pokok lebih dari bakidebet, maka tidak boleh
                        lPass2 = False
                        cKet2 = "Ags Pokok > Baki Debet"
                    End If
                    If lPass1 And lPass2 Then
                        cFaktur = GetLastFaktur(eFaktur.fkt_Angsuran, dTgl.DateTime, , True)
                        cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                        If CDbl(nRow(5)) = CDbl(nRow(8)) Then
                            cStatusPembayaran = "3"
                        Else
                            cStatusPembayaran = "2"
                        End If
                        cKeterangan = "Angsuran Kredit Instansi an. " & nRow(1).ToString
                        UpdAngsuranPembiayaan(cKodeCabang, cFaktur, dTgl.DateTime, nRow(0).ToString, eAngsuran.ags_Angsuran,
                                              CDbl(nRow(5)), CDbl(nRow(6)), 0, cKeterangan, , , , 0, 0, , , cStatusPembayaran,
                                              , , , , , True)
                        vaField = {"fakturAngsuran"}
                        vaValue = {cFaktur}
                        cWhere = String.Format("rekening = '{0}' and faktur = '{1}'", nRow(0), nRow(10))
                        objData.Edit(GetDSN, "angsurantemp", cWhere, vaField, vaValue)
                        GridView1.SetRowCellValue(n, "Status", "Berhasil")
                    Else
                        GridView1.SetRowCellValue(n, "Status", String.Format("{0} - {1}", cKet1, cKet2))
                    End If
                Else
                    GridView1.SetRowCellValue(n, "Status", "Gagal")
                End If
            Next
            MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            cInstansi.Focus()
        End If
    End Sub

    Private Sub trAngsuranInstansi_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("Instansi", cInstansi)
        GetDataLookup("bendahara", cBendahara, , "kode,nama")
    End Sub

    Private Sub trAngsuranInstansi_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar, , , , , , , , , cmdRefresh)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cInstansi.TabIndex, n)
        SetTabIndex(cBendahara.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cInstansi.EnterMoveNextControl = True
        cBendahara.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub trAngsuranInstansi_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        'Me.WindowState = FormWindowState.Maximized
        PanelControl1.Width = Me.Width - 20
        GridControl1.Width = PanelControl1.Width '- 100
        PanelControl4.Width = PanelControl1.Width
        GridControl1.Height = Me.Height - PanelControl1.Height - PanelControl4.Height - 60
        PanelControl4.Top = GridControl1.Height + PanelControl1.Height + 15
        cmdSimpan.Left = PanelControl4.Width - cmdSimpan.Width - cmdKeluar.Width - 20
        cmdKeluar.Left = PanelControl4.Width - cmdKeluar.Width - 10
    End Sub
End Class