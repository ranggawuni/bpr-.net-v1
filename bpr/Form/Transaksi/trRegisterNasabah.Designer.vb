﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trRegisterNasabah
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trRegisterNasabah))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cCabangInduk = New DevExpress.XtraEditors.TextEdit()
        Me.cKodeInduk = New DevExpress.XtraEditors.TextEdit()
        Me.cCabang = New DevExpress.XtraEditors.TextEdit()
        Me.cRTRW = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.cKelurahan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKelurahan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cKecamatan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKecamatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.cKota = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaKota = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.cKodePos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.cFax = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNPWP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cNIP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaPekerjaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cInstansi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaInstansi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cAgama = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAgama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglKTP = New DevExpress.XtraEditors.DateEdit()
        Me.cKTP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.optPerkawinan = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglLahir = New DevExpress.XtraEditors.DateEdit()
        Me.cTempatLahir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.optGolonganDarah = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.optJenisKelamin = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaIbu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.cPekerjaan = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cCabangInduk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKodeInduk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRTRW.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKelurahan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKelurahan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKecamatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKecamatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKodePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNPWP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNIP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPekerjaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cInstansi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaInstansi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAgama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAgama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglKTP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglKTP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKTP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPerkawinan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTempatLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optGolonganDarah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenisKelamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaIbu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cPekerjaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(1, 459)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(653, 33)
        Me.PanelControl2.TabIndex = 11
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(571, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(490, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cCabangInduk)
        Me.PanelControl1.Controls.Add(Me.cKodeInduk)
        Me.PanelControl1.Controls.Add(Me.cCabang)
        Me.PanelControl1.Controls.Add(Me.cRTRW)
        Me.PanelControl1.Controls.Add(Me.LabelControl21)
        Me.PanelControl1.Controls.Add(Me.cKelurahan)
        Me.PanelControl1.Controls.Add(Me.cNamaKelurahan)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.cKecamatan)
        Me.PanelControl1.Controls.Add(Me.cNamaKecamatan)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.cKota)
        Me.PanelControl1.Controls.Add(Me.cNamaKota)
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Controls.Add(Me.cKodePos)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.cFax)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNPWP)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cNIP)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNamaPekerjaan)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.cInstansi)
        Me.PanelControl1.Controls.Add(Me.cNamaInstansi)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cAgama)
        Me.PanelControl1.Controls.Add(Me.cNamaAgama)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.dTglKTP)
        Me.PanelControl1.Controls.Add(Me.cKTP)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.optPerkawinan)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.dTglLahir)
        Me.PanelControl1.Controls.Add(Me.cTempatLahir)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.optGolonganDarah)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.optJenisKelamin)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cNamaIbu)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Controls.Add(Me.cPekerjaan)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(653, 452)
        Me.PanelControl1.TabIndex = 12
        '
        'cCabangInduk
        '
        Me.cCabangInduk.EditValue = "12"
        Me.cCabangInduk.Location = New System.Drawing.Point(449, 9)
        Me.cCabangInduk.Name = "cCabangInduk"
        Me.cCabangInduk.Properties.Mask.EditMask = "##.######"
        Me.cCabangInduk.Properties.MaxLength = 2
        Me.cCabangInduk.Size = New System.Drawing.Size(30, 20)
        Me.cCabangInduk.TabIndex = 137
        '
        'cKodeInduk
        '
        Me.cKodeInduk.EditValue = "123456"
        Me.cKodeInduk.Location = New System.Drawing.Point(485, 9)
        Me.cKodeInduk.Name = "cKodeInduk"
        Me.cKodeInduk.Properties.Mask.EditMask = "##.######"
        Me.cKodeInduk.Properties.MaxLength = 6
        Me.cKodeInduk.Size = New System.Drawing.Size(46, 20)
        Me.cKodeInduk.TabIndex = 136
        '
        'cCabang
        '
        Me.cCabang.EditValue = "12"
        Me.cCabang.Location = New System.Drawing.Point(104, 35)
        Me.cCabang.Name = "cCabang"
        Me.cCabang.Properties.Mask.EditMask = "##.######"
        Me.cCabang.Properties.MaxLength = 2
        Me.cCabang.Size = New System.Drawing.Size(30, 20)
        Me.cCabang.TabIndex = 135
        '
        'cRTRW
        '
        Me.cRTRW.Location = New System.Drawing.Point(104, 260)
        Me.cRTRW.Name = "cRTRW"
        Me.cRTRW.Properties.Mask.EditMask = "###/###"
        Me.cRTRW.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.cRTRW.Size = New System.Drawing.Size(61, 20)
        Me.cRTRW.TabIndex = 134
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(11, 263)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl21.TabIndex = 133
        Me.LabelControl21.Text = "RT / RW"
        '
        'cKelurahan
        '
        Me.cKelurahan.Location = New System.Drawing.Point(104, 338)
        Me.cKelurahan.Name = "cKelurahan"
        Me.cKelurahan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKelurahan.Size = New System.Drawing.Size(61, 20)
        Me.cKelurahan.TabIndex = 132
        '
        'cNamaKelurahan
        '
        Me.cNamaKelurahan.Enabled = False
        Me.cNamaKelurahan.Location = New System.Drawing.Point(171, 338)
        Me.cNamaKelurahan.Name = "cNamaKelurahan"
        Me.cNamaKelurahan.Size = New System.Drawing.Size(160, 20)
        Me.cNamaKelurahan.TabIndex = 131
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(11, 341)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl18.TabIndex = 130
        Me.LabelControl18.Text = "Kelurahan"
        '
        'cKecamatan
        '
        Me.cKecamatan.Location = New System.Drawing.Point(104, 312)
        Me.cKecamatan.Name = "cKecamatan"
        Me.cKecamatan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKecamatan.Size = New System.Drawing.Size(61, 20)
        Me.cKecamatan.TabIndex = 129
        '
        'cNamaKecamatan
        '
        Me.cNamaKecamatan.Enabled = False
        Me.cNamaKecamatan.Location = New System.Drawing.Point(171, 312)
        Me.cNamaKecamatan.Name = "cNamaKecamatan"
        Me.cNamaKecamatan.Size = New System.Drawing.Size(160, 20)
        Me.cNamaKecamatan.TabIndex = 128
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(11, 315)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl19.TabIndex = 127
        Me.LabelControl19.Text = "Kecamatan"
        '
        'cKota
        '
        Me.cKota.EditValue = " "
        Me.cKota.Location = New System.Drawing.Point(104, 286)
        Me.cKota.Name = "cKota"
        Me.cKota.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKota.Properties.NullText = ""
        Me.cKota.Properties.ValidateOnEnterKey = True
        Me.cKota.Size = New System.Drawing.Size(61, 20)
        Me.cKota.TabIndex = 126
        '
        'cNamaKota
        '
        Me.cNamaKota.Enabled = False
        Me.cNamaKota.Location = New System.Drawing.Point(171, 286)
        Me.cNamaKota.Name = "cNamaKota"
        Me.cNamaKota.Size = New System.Drawing.Size(160, 20)
        Me.cNamaKota.TabIndex = 125
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(11, 289)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl20.TabIndex = 124
        Me.LabelControl20.Text = "Kota / Kabupaten"
        '
        'cKodePos
        '
        Me.cKodePos.Location = New System.Drawing.Point(104, 364)
        Me.cKodePos.Name = "cKodePos"
        Me.cKodePos.Properties.Mask.EditMask = "d"
        Me.cKodePos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cKodePos.Size = New System.Drawing.Size(61, 20)
        Me.cKodePos.TabIndex = 123
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(11, 367)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl17.TabIndex = 122
        Me.LabelControl17.Text = "Kode Pos"
        '
        'cFax
        '
        Me.cFax.Location = New System.Drawing.Point(449, 112)
        Me.cFax.Name = "cFax"
        Me.cFax.Size = New System.Drawing.Size(105, 20)
        Me.cFax.TabIndex = 121
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(356, 115)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(18, 13)
        Me.LabelControl16.TabIndex = 120
        Me.LabelControl16.Text = "Fax"
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(449, 87)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(356, 90)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(104, 234)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(227, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(11, 237)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNPWP
        '
        Me.cNPWP.Location = New System.Drawing.Point(449, 191)
        Me.cNPWP.Name = "cNPWP"
        Me.cNPWP.Size = New System.Drawing.Size(191, 20)
        Me.cNPWP.TabIndex = 115
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(357, 194)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl13.TabIndex = 114
        Me.LabelControl13.Text = "NPWP"
        '
        'cNIP
        '
        Me.cNIP.Location = New System.Drawing.Point(449, 165)
        Me.cNIP.Name = "cNIP"
        Me.cNIP.Size = New System.Drawing.Size(191, 20)
        Me.cNIP.TabIndex = 113
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(356, 168)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(17, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "NIP"
        '
        'cNamaPekerjaan
        '
        Me.cNamaPekerjaan.Enabled = False
        Me.cNamaPekerjaan.Location = New System.Drawing.Point(499, 35)
        Me.cNamaPekerjaan.Name = "cNamaPekerjaan"
        Me.cNamaPekerjaan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaPekerjaan.TabIndex = 110
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(356, 142)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl11.TabIndex = 109
        Me.LabelControl11.Text = "Instansi"
        '
        'cInstansi
        '
        Me.cInstansi.Location = New System.Drawing.Point(449, 139)
        Me.cInstansi.Name = "cInstansi"
        Me.cInstansi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cInstansi.Size = New System.Drawing.Size(44, 20)
        Me.cInstansi.TabIndex = 108
        '
        'cNamaInstansi
        '
        Me.cNamaInstansi.Enabled = False
        Me.cNamaInstansi.Location = New System.Drawing.Point(499, 139)
        Me.cNamaInstansi.Name = "cNamaInstansi"
        Me.cNamaInstansi.Size = New System.Drawing.Size(141, 20)
        Me.cNamaInstansi.TabIndex = 107
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(356, 38)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl10.TabIndex = 106
        Me.LabelControl10.Text = "Pekerjaan"
        '
        'cAgama
        '
        Me.cAgama.Location = New System.Drawing.Point(104, 390)
        Me.cAgama.Name = "cAgama"
        Me.cAgama.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAgama.Size = New System.Drawing.Size(44, 20)
        Me.cAgama.TabIndex = 105
        '
        'cNamaAgama
        '
        Me.cNamaAgama.Enabled = False
        Me.cNamaAgama.Location = New System.Drawing.Point(154, 390)
        Me.cNamaAgama.Name = "cNamaAgama"
        Me.cNamaAgama.Size = New System.Drawing.Size(141, 20)
        Me.cNamaAgama.TabIndex = 104
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(11, 393)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl9.TabIndex = 103
        Me.LabelControl9.Text = "Agama"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(356, 64)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl8.TabIndex = 102
        Me.LabelControl8.Text = "Tanggal KTP"
        '
        'dTglKTP
        '
        Me.dTglKTP.EditValue = Nothing
        Me.dTglKTP.Location = New System.Drawing.Point(449, 61)
        Me.dTglKTP.Name = "dTglKTP"
        Me.dTglKTP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglKTP.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglKTP.Size = New System.Drawing.Size(82, 20)
        Me.dTglKTP.TabIndex = 101
        '
        'cKTP
        '
        Me.cKTP.Location = New System.Drawing.Point(104, 61)
        Me.cKTP.Name = "cKTP"
        Me.cKTP.Size = New System.Drawing.Size(191, 20)
        Me.cKTP.TabIndex = 100
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(11, 64)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl7.TabIndex = 99
        Me.LabelControl7.Text = "No. KTP"
        '
        'optPerkawinan
        '
        Me.optPerkawinan.Location = New System.Drawing.Point(104, 416)
        Me.optPerkawinan.Name = "optPerkawinan"
        Me.optPerkawinan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Kawin"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Belum")})
        Me.optPerkawinan.Size = New System.Drawing.Size(191, 29)
        Me.optPerkawinan.TabIndex = 98
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 424)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl5.TabIndex = 97
        Me.LabelControl5.Text = "Status Perkawinan"
        '
        'dTglLahir
        '
        Me.dTglLahir.EditValue = Nothing
        Me.dTglLahir.Location = New System.Drawing.Point(213, 138)
        Me.dTglLahir.Name = "dTglLahir"
        Me.dTglLahir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglLahir.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglLahir.Size = New System.Drawing.Size(82, 20)
        Me.dTglLahir.TabIndex = 96
        '
        'cTempatLahir
        '
        Me.cTempatLahir.Location = New System.Drawing.Point(104, 138)
        Me.cTempatLahir.Name = "cTempatLahir"
        Me.cTempatLahir.Size = New System.Drawing.Size(103, 20)
        Me.cTempatLahir.TabIndex = 95
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(9, 142)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 94
        Me.LabelControl4.Text = "Tempat, Tgl Lahir"
        '
        'optGolonganDarah
        '
        Me.optGolonganDarah.Location = New System.Drawing.Point(104, 199)
        Me.optGolonganDarah.Name = "optGolonganDarah"
        Me.optGolonganDarah.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "A"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "B"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "AB"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "O")})
        Me.optGolonganDarah.Size = New System.Drawing.Size(191, 29)
        Me.optGolonganDarah.TabIndex = 93
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(11, 206)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl3.TabIndex = 92
        Me.LabelControl3.Text = "Golongan Darah"
        '
        'optJenisKelamin
        '
        Me.optJenisKelamin.Location = New System.Drawing.Point(104, 164)
        Me.optJenisKelamin.Name = "optJenisKelamin"
        Me.optJenisKelamin.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Laki - Laki"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Perempuan")})
        Me.optJenisKelamin.Size = New System.Drawing.Size(191, 29)
        Me.optJenisKelamin.TabIndex = 91
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(11, 171)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl6.TabIndex = 90
        Me.LabelControl6.Text = "Jenis Kelamin"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(356, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl2.TabIndex = 89
        Me.LabelControl2.Text = "Kode Induk"
        '
        'cNamaIbu
        '
        Me.cNamaIbu.Location = New System.Drawing.Point(104, 112)
        Me.cNamaIbu.Name = "cNamaIbu"
        Me.cNamaIbu.Size = New System.Drawing.Size(227, 20)
        Me.cNamaIbu.TabIndex = 87
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(11, 115)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl1.TabIndex = 86
        Me.LabelControl1.Text = "Nama Ibu"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(104, 87)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(11, 90)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(70, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama Lengkap"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(11, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(11, 38)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(60, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Register"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(140, 35)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'cPekerjaan
        '
        Me.cPekerjaan.Location = New System.Drawing.Point(449, 35)
        Me.cPekerjaan.Name = "cPekerjaan"
        Me.cPekerjaan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPekerjaan.Size = New System.Drawing.Size(44, 20)
        Me.cPekerjaan.TabIndex = 111
        '
        'trRegisterNasabah
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(657, 494)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "trRegisterNasabah"
        Me.Text = "Register Nasabah"
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cCabangInduk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKodeInduk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRTRW.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKelurahan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKelurahan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKecamatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKecamatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKodePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNPWP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNIP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPekerjaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cInstansi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaInstansi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAgama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAgama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglKTP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglKTP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKTP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPerkawinan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTempatLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optGolonganDarah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenisKelamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaIbu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cPekerjaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRTRW As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKelurahan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKelurahan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKecamatan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKecamatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKota As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaKota As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKodePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNPWP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNIP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaPekerjaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cInstansi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaInstansi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAgama As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAgama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglKTP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cKTP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optPerkawinan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglLahir As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cTempatLahir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optGolonganDarah As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optJenisKelamin As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaIbu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cCabang As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cCabangInduk As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKodeInduk As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cPekerjaan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
End Class
