﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trMutasiTabungan
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trMutasiTabungan))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.nSaldoEfektif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.nBlokir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.nSetoranMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cKodeTransaksi = New DevExpress.XtraEditors.LookUpEdit()
        Me.nSaldoAkhir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nMutasi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoAwal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeteranganKodeTransaksi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.nSaldoEfektif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBlokir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKodeTransaksi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoAkhir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nMutasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoAwal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganKodeTransaksi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cFaktur)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(711, 144)
        Me.PanelControl1.TabIndex = 15
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 35)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(104, 113)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(227, 20)
        Me.cFaktur.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 117)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "No. Faktur"
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 87)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 39)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 61)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.nSaldoEfektif)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.nBlokir)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.nSetoranMinimum)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nSaldoMinimum)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 149)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(221, 138)
        Me.PanelControl2.TabIndex = 16
        '
        'nSaldoEfektif
        '
        Me.nSaldoEfektif.EditValue = ""
        Me.nSaldoEfektif.Enabled = False
        Me.nSaldoEfektif.Location = New System.Drawing.Point(104, 83)
        Me.nSaldoEfektif.Name = "nSaldoEfektif"
        Me.nSaldoEfektif.Properties.Mask.EditMask = "n"
        Me.nSaldoEfektif.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoEfektif.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoEfektif.TabIndex = 127
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(9, 87)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl4.TabIndex = 126
        Me.LabelControl4.Text = "Saldo Efektif"
        '
        'nBlokir
        '
        Me.nBlokir.Enabled = False
        Me.nBlokir.Location = New System.Drawing.Point(104, 57)
        Me.nBlokir.Name = "nBlokir"
        Me.nBlokir.Size = New System.Drawing.Size(105, 20)
        Me.nBlokir.TabIndex = 125
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 61)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl3.TabIndex = 124
        Me.LabelControl3.Text = "Blokir"
        '
        'nSetoranMinimum
        '
        Me.nSetoranMinimum.Enabled = False
        Me.nSetoranMinimum.Location = New System.Drawing.Point(104, 31)
        Me.nSetoranMinimum.Name = "nSetoranMinimum"
        Me.nSetoranMinimum.Size = New System.Drawing.Size(105, 20)
        Me.nSetoranMinimum.TabIndex = 123
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 35)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl2.TabIndex = 122
        Me.LabelControl2.Text = "Setoran Minimum"
        '
        'nSaldoMinimum
        '
        Me.nSaldoMinimum.Enabled = False
        Me.nSaldoMinimum.Location = New System.Drawing.Point(104, 5)
        Me.nSaldoMinimum.Name = "nSaldoMinimum"
        Me.nSaldoMinimum.Properties.Mask.EditMask = "n"
        Me.nSaldoMinimum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoMinimum.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoMinimum.TabIndex = 121
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 9)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl1.TabIndex = 120
        Me.LabelControl1.Text = "Saldo Minimum"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cKeterangan)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.cKodeTransaksi)
        Me.PanelControl3.Controls.Add(Me.nSaldoAkhir)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.nMutasi)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.nSaldoAwal)
        Me.PanelControl3.Controls.Add(Me.LabelControl7)
        Me.PanelControl3.Controls.Add(Me.cKeteranganKodeTransaksi)
        Me.PanelControl3.Controls.Add(Me.LabelControl8)
        Me.PanelControl3.Location = New System.Drawing.Point(228, 149)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(486, 138)
        Me.PanelControl3.TabIndex = 17
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(104, 109)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(360, 20)
        Me.cKeterangan.TabIndex = 130
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 112)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl9.TabIndex = 129
        Me.LabelControl9.Text = "Keterangan"
        '
        'cKodeTransaksi
        '
        Me.cKodeTransaksi.Location = New System.Drawing.Point(104, 5)
        Me.cKodeTransaksi.Name = "cKodeTransaksi"
        Me.cKodeTransaksi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKodeTransaksi.Size = New System.Drawing.Size(44, 20)
        Me.cKodeTransaksi.TabIndex = 128
        '
        'nSaldoAkhir
        '
        Me.nSaldoAkhir.Enabled = False
        Me.nSaldoAkhir.Location = New System.Drawing.Point(104, 83)
        Me.nSaldoAkhir.Name = "nSaldoAkhir"
        Me.nSaldoAkhir.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAkhir.TabIndex = 127
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 87)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl5.TabIndex = 126
        Me.LabelControl5.Text = "Saldo Akhir"
        '
        'nMutasi
        '
        Me.nMutasi.Location = New System.Drawing.Point(104, 57)
        Me.nMutasi.Name = "nMutasi"
        Me.nMutasi.Size = New System.Drawing.Size(105, 20)
        Me.nMutasi.TabIndex = 125
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 61)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl6.TabIndex = 124
        Me.LabelControl6.Text = "Mutasi"
        '
        'nSaldoAwal
        '
        Me.nSaldoAwal.Enabled = False
        Me.nSaldoAwal.Location = New System.Drawing.Point(104, 31)
        Me.nSaldoAwal.Name = "nSaldoAwal"
        Me.nSaldoAwal.Properties.Mask.EditMask = "n"
        Me.nSaldoAwal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoAwal.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAwal.TabIndex = 123
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 35)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl7.TabIndex = 122
        Me.LabelControl7.Text = "Saldo Awal"
        '
        'cKeteranganKodeTransaksi
        '
        Me.cKeteranganKodeTransaksi.Enabled = False
        Me.cKeteranganKodeTransaksi.Location = New System.Drawing.Point(155, 5)
        Me.cKeteranganKodeTransaksi.Name = "cKeteranganKodeTransaksi"
        Me.cKeteranganKodeTransaksi.Size = New System.Drawing.Size(311, 20)
        Me.cKeteranganKodeTransaksi.TabIndex = 121
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(9, 9)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl8.TabIndex = 120
        Me.LabelControl8.Text = "Kode Transaksi"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 290)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(711, 33)
        Me.PanelControl4.TabIndex = 18
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(631, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(550, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'trMutasiTabungan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdKeluar
        Me.ClientSize = New System.Drawing.Size(715, 324)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trMutasiTabungan"
        Me.Text = "Mutasi Tabungan"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.nSaldoEfektif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBlokir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKodeTransaksi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoAkhir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nMutasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoAwal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganKodeTransaksi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nSaldoMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoEfektif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBlokir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSetoranMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nSaldoAkhir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nMutasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoAwal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeteranganKodeTransaksi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKodeTransaksi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
End Class
