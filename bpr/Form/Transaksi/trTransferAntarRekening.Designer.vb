﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trTransferAntarRekening
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
            If objData IsNot Nothing Then
                objData.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trTransferAntarRekening))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.nSaldoEfektif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.nBlokir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.nSetoranMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoMinimum = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cKodeTransaksiAsal = New DevExpress.XtraEditors.LookUpEdit()
        Me.cKeteranganKodeTransaksiAsal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoAkhir = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nMutasi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoAwal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeteranganKodeTransaksiTujuan = New DevExpress.XtraEditors.TextEdit()
        Me.cKodeTransaksiTujuan = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.nSaldoAkhirTujuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoAwalTujuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTujuan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamatTujuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaTujuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.nSaldoEfektif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBlokir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKodeTransaksiAsal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganKodeTransaksiAsal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoAkhir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nMutasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoAwal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cKeteranganKodeTransaksiTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKodeTransaksiTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.nSaldoAkhirTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoAwalTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamatTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaTujuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.nSaldoEfektif)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.nBlokir)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.nSetoranMinimum)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.nSaldoMinimum)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cKodeTransaksiAsal)
        Me.PanelControl1.Controls.Add(Me.cKeteranganKodeTransaksiAsal)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cFaktur)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(898, 209)
        Me.PanelControl1.TabIndex = 15
        '
        'nSaldoEfektif
        '
        Me.nSaldoEfektif.EditValue = ""
        Me.nSaldoEfektif.Enabled = False
        Me.nSaldoEfektif.Location = New System.Drawing.Point(329, 184)
        Me.nSaldoEfektif.Name = "nSaldoEfektif"
        Me.nSaldoEfektif.Properties.Mask.EditMask = "n"
        Me.nSaldoEfektif.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoEfektif.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoEfektif.TabIndex = 140
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(234, 188)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl4.TabIndex = 139
        Me.LabelControl4.Text = "Saldo Efektif"
        '
        'nBlokir
        '
        Me.nBlokir.Enabled = False
        Me.nBlokir.Location = New System.Drawing.Point(329, 158)
        Me.nBlokir.Name = "nBlokir"
        Me.nBlokir.Size = New System.Drawing.Size(105, 20)
        Me.nBlokir.TabIndex = 138
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(234, 162)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl3.TabIndex = 137
        Me.LabelControl3.Text = "Blokir"
        '
        'nSetoranMinimum
        '
        Me.nSetoranMinimum.Enabled = False
        Me.nSetoranMinimum.Location = New System.Drawing.Point(104, 184)
        Me.nSetoranMinimum.Name = "nSetoranMinimum"
        Me.nSetoranMinimum.Size = New System.Drawing.Size(105, 20)
        Me.nSetoranMinimum.TabIndex = 136
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 188)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl2.TabIndex = 135
        Me.LabelControl2.Text = "Setoran Minimum"
        '
        'nSaldoMinimum
        '
        Me.nSaldoMinimum.Enabled = False
        Me.nSaldoMinimum.Location = New System.Drawing.Point(104, 158)
        Me.nSaldoMinimum.Name = "nSaldoMinimum"
        Me.nSaldoMinimum.Properties.Mask.EditMask = "n"
        Me.nSaldoMinimum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoMinimum.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoMinimum.TabIndex = 134
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 162)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl1.TabIndex = 133
        Me.LabelControl1.Text = "Saldo Minimum"
        '
        'cKodeTransaksiAsal
        '
        Me.cKodeTransaksiAsal.Location = New System.Drawing.Point(104, 5)
        Me.cKodeTransaksiAsal.Name = "cKodeTransaksiAsal"
        Me.cKodeTransaksiAsal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKodeTransaksiAsal.Size = New System.Drawing.Size(44, 20)
        Me.cKodeTransaksiAsal.TabIndex = 132
        '
        'cKeteranganKodeTransaksiAsal
        '
        Me.cKeteranganKodeTransaksiAsal.Enabled = False
        Me.cKeteranganKodeTransaksiAsal.Location = New System.Drawing.Point(154, 5)
        Me.cKeteranganKodeTransaksiAsal.Name = "cKeteranganKodeTransaksiAsal"
        Me.cKeteranganKodeTransaksiAsal.Size = New System.Drawing.Size(280, 20)
        Me.cKeteranganKodeTransaksiAsal.TabIndex = 131
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(10, 9)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl10.TabIndex = 130
        Me.LabelControl10.Text = "Kode Transaksi"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 56)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(104, 134)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(227, 20)
        Me.cFaktur.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 138)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "No. Faktur"
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 108)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(330, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 111)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 60)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "Rekening Asal"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 82)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 85)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(10, 34)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 30)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cKeterangan)
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Controls.Add(Me.nSaldoAkhir)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.nMutasi)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.nSaldoAwal)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 218)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(448, 159)
        Me.PanelControl2.TabIndex = 16
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(105, 87)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(329, 20)
        Me.cKeterangan.TabIndex = 138
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(10, 90)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl9.TabIndex = 137
        Me.LabelControl9.Text = "Keterangan"
        '
        'nSaldoAkhir
        '
        Me.nSaldoAkhir.Enabled = False
        Me.nSaldoAkhir.Location = New System.Drawing.Point(105, 61)
        Me.nSaldoAkhir.Name = "nSaldoAkhir"
        Me.nSaldoAkhir.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAkhir.TabIndex = 136
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(10, 65)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl5.TabIndex = 135
        Me.LabelControl5.Text = "Saldo Akhir"
        '
        'nMutasi
        '
        Me.nMutasi.Location = New System.Drawing.Point(105, 35)
        Me.nMutasi.Name = "nMutasi"
        Me.nMutasi.Size = New System.Drawing.Size(105, 20)
        Me.nMutasi.TabIndex = 134
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(10, 39)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl6.TabIndex = 133
        Me.LabelControl6.Text = "Mutasi"
        '
        'nSaldoAwal
        '
        Me.nSaldoAwal.Enabled = False
        Me.nSaldoAwal.Location = New System.Drawing.Point(105, 9)
        Me.nSaldoAwal.Name = "nSaldoAwal"
        Me.nSaldoAwal.Properties.Mask.EditMask = "n"
        Me.nSaldoAwal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoAwal.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAwal.TabIndex = 132
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(10, 13)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl7.TabIndex = 131
        Me.LabelControl7.Text = "Saldo Awal"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 383)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(898, 33)
        Me.PanelControl4.TabIndex = 18
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(631, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(550, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(11, 9)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl8.TabIndex = 120
        Me.LabelControl8.Text = "Kode Transaksi"
        '
        'cKeteranganKodeTransaksiTujuan
        '
        Me.cKeteranganKodeTransaksiTujuan.Enabled = False
        Me.cKeteranganKodeTransaksiTujuan.Location = New System.Drawing.Point(155, 5)
        Me.cKeteranganKodeTransaksiTujuan.Name = "cKeteranganKodeTransaksiTujuan"
        Me.cKeteranganKodeTransaksiTujuan.Size = New System.Drawing.Size(281, 20)
        Me.cKeteranganKodeTransaksiTujuan.TabIndex = 121
        '
        'cKodeTransaksiTujuan
        '
        Me.cKodeTransaksiTujuan.Location = New System.Drawing.Point(104, 5)
        Me.cKodeTransaksiTujuan.Name = "cKodeTransaksiTujuan"
        Me.cKodeTransaksiTujuan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKodeTransaksiTujuan.Size = New System.Drawing.Size(44, 20)
        Me.cKodeTransaksiTujuan.TabIndex = 128
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.nSaldoAkhirTujuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl18)
        Me.PanelControl3.Controls.Add(Me.nSaldoAwalTujuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl17)
        Me.PanelControl3.Controls.Add(Me.cRekeningTujuan)
        Me.PanelControl3.Controls.Add(Me.cAlamatTujuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl11)
        Me.PanelControl3.Controls.Add(Me.LabelControl13)
        Me.PanelControl3.Controls.Add(Me.cNamaTujuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl16)
        Me.PanelControl3.Controls.Add(Me.cKodeTransaksiTujuan)
        Me.PanelControl3.Controls.Add(Me.cKeteranganKodeTransaksiTujuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl8)
        Me.PanelControl3.Location = New System.Drawing.Point(457, 218)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(444, 159)
        Me.PanelControl3.TabIndex = 17
        '
        'nSaldoAkhirTujuan
        '
        Me.nSaldoAkhirTujuan.Enabled = False
        Me.nSaldoAkhirTujuan.Location = New System.Drawing.Point(106, 132)
        Me.nSaldoAkhirTujuan.Name = "nSaldoAkhirTujuan"
        Me.nSaldoAkhirTujuan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAkhirTujuan.TabIndex = 139
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(11, 136)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl18.TabIndex = 138
        Me.LabelControl18.Text = "Saldo Akhir"
        '
        'nSaldoAwalTujuan
        '
        Me.nSaldoAwalTujuan.Enabled = False
        Me.nSaldoAwalTujuan.Location = New System.Drawing.Point(106, 106)
        Me.nSaldoAwalTujuan.Name = "nSaldoAwalTujuan"
        Me.nSaldoAwalTujuan.Properties.Mask.EditMask = "n"
        Me.nSaldoAwalTujuan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSaldoAwalTujuan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoAwalTujuan.TabIndex = 137
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(11, 110)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl17.TabIndex = 136
        Me.LabelControl17.Text = "Saldo Awal"
        '
        'cRekeningTujuan
        '
        Me.cRekeningTujuan.Location = New System.Drawing.Point(106, 28)
        Me.cRekeningTujuan.Name = "cRekeningTujuan"
        Me.cRekeningTujuan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTujuan.Size = New System.Drawing.Size(158, 20)
        Me.cRekeningTujuan.TabIndex = 135
        '
        'cAlamatTujuan
        '
        Me.cAlamatTujuan.Enabled = False
        Me.cAlamatTujuan.Location = New System.Drawing.Point(106, 80)
        Me.cAlamatTujuan.Name = "cAlamatTujuan"
        Me.cAlamatTujuan.Size = New System.Drawing.Size(330, 20)
        Me.cAlamatTujuan.TabIndex = 134
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(11, 83)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl11.TabIndex = 133
        Me.LabelControl11.Text = "Alamat"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(11, 32)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl13.TabIndex = 132
        Me.LabelControl13.Text = "Rekening Tujuan"
        '
        'cNamaTujuan
        '
        Me.cNamaTujuan.Enabled = False
        Me.cNamaTujuan.Location = New System.Drawing.Point(106, 54)
        Me.cNamaTujuan.Name = "cNamaTujuan"
        Me.cNamaTujuan.Size = New System.Drawing.Size(227, 20)
        Me.cNamaTujuan.TabIndex = 131
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(11, 57)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl16.TabIndex = 130
        Me.LabelControl16.Text = "Nama"
        '
        'trTransferAntarRekening
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdKeluar
        Me.ClientSize = New System.Drawing.Size(904, 420)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trTransferAntarRekening"
        Me.Text = "Mutasi Transfer Antar Rekening"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.nSaldoEfektif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBlokir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSetoranMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoMinimum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKodeTransaksiAsal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganKodeTransaksiAsal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoAkhir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nMutasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoAwal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.cKeteranganKodeTransaksiTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKodeTransaksiTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.nSaldoAkhirTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoAwalTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamatTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaTujuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nSaldoEfektif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBlokir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSetoranMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoMinimum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKodeTransaksiAsal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cKeteranganKodeTransaksiAsal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoAkhir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nMutasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoAwal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeteranganKodeTransaksiTujuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKodeTransaksiTujuan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nSaldoAkhirTujuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoAwalTujuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTujuan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamatTujuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaTujuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
