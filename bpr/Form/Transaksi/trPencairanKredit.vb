﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trPencairanKredit
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim cCaraPerhitungan As String
    Dim nLama As Integer

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nPlafond.Text = ""
        nAdministrasi.Text = ""
        nNotaris.Text = ""
        nMaterai.Text = ""
        nAsuransi.Text = ""
        nProvisi.Text = ""
        nTotal.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        optKas.SelectedIndex = 1
    End Sub

    Private Sub trPencairanKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trPencairanKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(optKas.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        optKas.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNotaris, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nMaterai, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAsuransi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nProvisi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "statuspencairan,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("statuspencairan")) = 0 Then
                        Dim lCair As Boolean = cDataTable.Rows(0).Item("StatusPencairan").ToString <> "0"
                        If Not lCair Then
                            GetMemory()
                        Else
                            MessageBox.Show("Kredit sudah dicairkan, proses tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.EditValue = ""
                            cRekening.Focus()
                            Exit Sub
                        End If
                    Else
                        MessageBox.Show("Kredit sudah dicairkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , False).ToString
        Dim cField As String = "d.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,"
        cField = cField & "rn.Nama as NamaNasabah,g.Keterangan as NamaGolonganKredit"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join Tabungan t on d.RekeningTabungan = t.Rekening",
                                  "Left Join RegisterNasabah rn on rn.Kode = t.Kode",
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "debitur d", cField, "t.rekening", , cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("NamaDeposan").ToString
                cAlamat.Text = .Item("AlamatDeposan").ToString
                nPlafond.Text = .Item("GolonganDeposan").ToString
                cCaraPerhitungan = .Item("CaraPerhitungan").ToString
                If .Item("StatusPencairan").ToString = "0" Then
                    nAdministrasi.Text = .Item("Administrasi").ToString
                    nNotaris.Text = .Item("Notaris").ToString
                    nMaterai.Text = .Item("Materai").ToString
                    nAsuransi.Text = .Item("Asuransi").ToString
                    nProvisi.Text = .Item("Provisi").ToString
                    nLama = CInt(.Item("Lama").ToString)
                Else
                    nAdministrasi.Text = ""
                    nNotaris.Text = ""
                    nMaterai.Text = ""
                    nAsuransi.Text = ""
                    nProvisi.Text = ""
                    nLama = 0
                End If
                getpencairan()
                If .Item("RekeningTabungan").ToString.Length <> 12 Then
                    cRekeningTabungan.Text = .Item("RekeningTabungan").ToString
                    cNamaNasabah.Text = .Item("NamaNasabah").ToString
                Else
                    cRekeningTabungan.EditValue = ""
                    cNamaNasabah.Text = ""
                End If
            End With
        End If
    End Sub

    Private Sub GetPencairan()
        Dim nTemp As Double = CDbl(nPlafond.Text) - CDbl(nAdministrasi.Text) - CDbl(nNotaris.Text) - CDbl(nMaterai.Text) - CDbl(nAsuransi.Text) - CDbl(nProvisi.Text)
        nTotal.Text = nTemp.ToString
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(nPlafond.Text, "Plafond Tidak Boleh Kosong,Ulangi Pengisian...") Then
            ValidSaving = False
            cmdKeluar.Focus()
            Exit Function
        End If

        If Len(cRekening) < 12 Then
            MsgBox("Nomor Rekening Tidak Lengkap. Transaksi Tidak Bisa Dilanjutkan.", vbExclamation)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If optKas.SelectedIndex = 2 Then
            If cRekeningTabungan.Text = "" Then
                MessageBox.Show("Rekening Tabungan Tidak Ada." & vbCrLf & "Anda Tidak Dapat Menyimpan Data Ini", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
            If cRekeningTabungan.Text.Length < 12 Then
                MsgBox("Nomor Rekening Tabungan Tidak Lengkap. Transaksi Tidak Bisa Dilanjutkan.", vbExclamation)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        End If
        If optKas.SelectedIndex = 1 Then
            Dim db As New DataTable
            Const cField As String = "u.UserName,u.FullName,u.KasTeller,r.keterangan as NamaRekening"
            Const cWhere As String = " and u.KasTeller <> ''"
            Dim vaJoin() As Object = {"left join rekening r on r.kode = u.kasteller"}
            db = objData.Browse(GetDSN, "username u", cField, "u.username", data.myOperator.Content, cUserName, cWhere, , vaJoin)
            If db.Rows.Count > 0 Then
                cKasTeller = GetNull(db.Rows(0).Item("KasTeller")).ToString
            End If
            db.Dispose()
            Dim nSaldoKasTeller = GetSaldoRekening(cKasTeller, dTgl.DateTime)
            If nSaldoKasTeller < 0 Then
                MessageBox.Show("Saldo Kas Tidak Mencukupi!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
            If nSaldoKasTeller - CDbl(nTotal.Text) < 0 Then
                MessageBox.Show("Mutasi Melebihi Saldo Kas!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
        End If
        If Val(nTotal.Text) > Val(nAsuransi.Text) Then
            MessageBox.Show("Saldo Tabungan Tidak Mencukupi.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ValidSaving = False
            nTotal.Focus()
            Exit Function
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim cKodeCabang As String
        Dim vaJadwal As New DataTable
        Dim cWhere As String

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_Relisasi, dTgl.DateTime, , True)
            If ValidSaving() Then
                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                Dim cKas As String = GetOpt(optKas).ToString
                Dim db As New DataTable
                Const cField As String = "SukuBunga,CaraPerhitungan,GracePeriodePokok,GracePeriodeBunga,Pembulatan,SukuBungaBerikutnya"
                db = objData.Browse(GetDSN, "debitur", cField, "rekening", , cRekening.Text)
                If db.Rows.Count > 0 Then
                    With db.Rows(0)
                        vaJadwal = GetJadwal(dTgl.DateTime, CDbl(nPlafond.Text), nLama, CSng(.Item("SukuBunga")), cCaraPerhitungan,
                                             CDbl(.Item("GracePeriodePokok")), CDbl(.Item("GracePeriodeBunga")), CDbl(.Item("Pembulatan")),
                                             , CSng(.Item("SukuBungaBerikutnya")))
                    End With
                    cwhere = String.Format("rekening = '{0}'", cRekening.Text)
                    objData.Delete(GetDSN, "angsuran", "status", , eAngsuran.ags_jadwal.ToString, cWhere)
                    Dim cFakturTemp As String = GetLastFaktur(eFaktur.fkt_Jadwal_Angsuran, dTgl.DateTime, , True)
                    Dim cKeteranganTemp As String = ""
                    For n As Integer = 1 To vaJadwal.Rows.Count - 1
                        cKeteranganTemp = "Jadwal Angsuran Ke. " & n + 1
                        With vaJadwal.Rows(n)
                            UpdAngsuranPembiayaan(cKodeCabang, cFakturTemp, CDate(.Item(1)), cRekening.Text, eAngsuran.ags_jadwal,
                                                  CDbl(.Item(3)), CDbl(.Item(2)), 0, cKeteranganTemp, False, False, , , , , , , cUserName)
                        End With
                    Next
                Else
                    vaJadwal = Nothing
                End If
                db.Dispose()
                vaJadwal.Dispose()

                Dim vaField() As Object = {"tgl"}
                Dim vaValue() As Object = {dTgl.DateTime}
                cWhere = String.Format("rekening = '{0}'", cRekening.Text)
                objData.Edit(GetDSN, "debitur", cWhere, vaField, vaValue)

                Dim nProvisiPerBulan As Double = Math.Round(CDbl(nProvisi.Text) / nLama, 2)
                Dim nAdmPerBulan As Double = Math.Round(CDbl(nAdministrasi.Text) / nLama, 2)
                Dim nTotalProvisi As Double = 0
                Dim nTotalAdm As Double = 0
                Dim dTanggal As Date = Date.Today
                objData.Delete(GetDSN, "provisi_adm", "rekening", , cRekening.Text)
                If CDbl(nProvisi.Text) > 0 Or CDbl(nAdministrasi.Text) > 0 Then
                    For n = 0 To nLama - 1
                        If n = nLama - 1 Then
                            nProvisiPerBulan = Math.Round(CDbl(nProvisi.Text) - nTotalProvisi, 2)
                            nAdmPerBulan = Math.Round(CDbl(nAdministrasi.Text) - nTotalAdm, 2)
                        Else
                            nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                            nTotalAdm = nTotalAdm + nAdmPerBulan
                        End If
                        dTanggal = DateAdd("m", n, dTgl.DateTime)
                        vaField = {"rekening", "tgl", "provisi", "adm", "tglMutasi"}
                        vaValue = {cRekening.Text, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.DateTime}
                        cWhere = String.Format("rekening='{0}' ", cRekening.Text)
                        objData.Add(GetDSN, "provisi_adm", vaField, vaValue)
                    Next
                End If

                UpdPencairanKredit(cRekening.Text, cFaktur.Text, CDbl(nPlafond.Text), dTgl.DateTime, cKas)

                'If MessageBox.Show("Cetak Validasi Pencairan Kredit ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                'CetakValidasiPencairanKreditDetail cFaktur.Text
                'End If

                'UpdActivity(Me, GetActivity)
                MsgBox("Rekening Kredit Telah Dicairkan.", vbInformation)
            End If

            If MessageBox.Show("Cetak Validasi ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                'CetakValidasiPokokDepositoDetail cFaktur.Text
            End If
            MsgBox("Data sudah disimpan.", vbInformation)
            InitValue()
        End If
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

End Class