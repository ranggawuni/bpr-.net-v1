﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trPerpanjanganDeposito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trPerpanjanganDeposito))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.cbJenisDeposito = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.nPerpanjanganKe = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.nNominalBaru = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.nTotBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.nNominal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.nPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.dJatuhTempo = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.nBungaBaru = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.cKeteranganGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.cKeteranganGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekeningLama = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTanggal = New DevExpress.XtraEditors.DateEdit()
        Me.lblTglPenempatan = New DevExpress.XtraEditors.LabelControl()
        Me.lblJatuhTempo = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbJenisDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPerpanjanganKe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNominalBaru.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJatuhTempo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBungaBaru.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTanggal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTanggal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.lblJatuhTempo)
        Me.PanelControl2.Controls.Add(Me.lblTglPenempatan)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Controls.Add(Me.cbJenisDeposito)
        Me.PanelControl2.Controls.Add(Me.LabelControl15)
        Me.PanelControl2.Controls.Add(Me.cNamaTabungan)
        Me.PanelControl2.Controls.Add(Me.LabelControl29)
        Me.PanelControl2.Controls.Add(Me.nPerpanjanganKe)
        Me.PanelControl2.Controls.Add(Me.LabelControl25)
        Me.PanelControl2.Controls.Add(Me.nNominalBaru)
        Me.PanelControl2.Controls.Add(Me.LabelControl24)
        Me.PanelControl2.Controls.Add(Me.nTotBunga)
        Me.PanelControl2.Controls.Add(Me.LabelControl23)
        Me.PanelControl2.Controls.Add(Me.nNominal)
        Me.PanelControl2.Controls.Add(Me.LabelControl22)
        Me.PanelControl2.Controls.Add(Me.nPajak)
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl17)
        Me.PanelControl2.Controls.Add(Me.dJatuhTempo)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.dTgl)
        Me.PanelControl2.Controls.Add(Me.LabelControl13)
        Me.PanelControl2.Controls.Add(Me.nBungaBaru)
        Me.PanelControl2.Controls.Add(Me.LabelControl16)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nLama)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.nBunga)
        Me.PanelControl2.Controls.Add(Me.LabelControl11)
        Me.PanelControl2.Controls.Add(Me.cGolonganDeposito)
        Me.PanelControl2.Controls.Add(Me.cKeteranganGolonganDeposito)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.cGolonganDeposan)
        Me.PanelControl2.Controls.Add(Me.cKeteranganGolonganDeposan)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Location = New System.Drawing.Point(2, 152)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(825, 366)
        Me.PanelControl2.TabIndex = 27
        '
        'LabelControl4
        '
        Me.LabelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl4.Location = New System.Drawing.Point(142, 113)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl4.TabIndex = 246
        Me.LabelControl4.Text = "%"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(348, 8)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(468, 303)
        Me.GridControl1.TabIndex = 245
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'cbJenisDeposito
        '
        Me.cbJenisDeposito.Location = New System.Drawing.Point(105, 239)
        Me.cbJenisDeposito.Name = "cbJenisDeposito"
        Me.cbJenisDeposito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenisDeposito.Size = New System.Drawing.Size(121, 20)
        Me.cbJenisDeposito.TabIndex = 244
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(10, 242)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl15.TabIndex = 243
        Me.LabelControl15.Text = "Jenis Deposito"
        '
        'cNamaTabungan
        '
        Me.cNamaTabungan.Enabled = False
        Me.cNamaTabungan.Location = New System.Drawing.Point(105, 31)
        Me.cNamaTabungan.Name = "cNamaTabungan"
        Me.cNamaTabungan.Size = New System.Drawing.Size(227, 20)
        Me.cNamaTabungan.TabIndex = 240
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(10, 34)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl29.TabIndex = 239
        Me.LabelControl29.Text = "Nama"
        '
        'nPerpanjanganKe
        '
        Me.nPerpanjanganKe.Enabled = False
        Me.nPerpanjanganKe.Location = New System.Drawing.Point(335, 343)
        Me.nPerpanjanganKe.Name = "nPerpanjanganKe"
        Me.nPerpanjanganKe.Size = New System.Drawing.Size(27, 20)
        Me.nPerpanjanganKe.TabIndex = 236
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(240, 347)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl25.TabIndex = 235
        Me.LabelControl25.Text = "Perpanjangan Ke"
        '
        'nNominalBaru
        '
        Me.nNominalBaru.Enabled = False
        Me.nNominalBaru.Location = New System.Drawing.Point(105, 343)
        Me.nNominalBaru.Name = "nNominalBaru"
        Me.nNominalBaru.Size = New System.Drawing.Size(121, 20)
        Me.nNominalBaru.TabIndex = 234
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(10, 347)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl24.TabIndex = 233
        Me.LabelControl24.Text = "Nominal Baru"
        '
        'nTotBunga
        '
        Me.nTotBunga.Enabled = False
        Me.nTotBunga.Location = New System.Drawing.Point(105, 291)
        Me.nTotBunga.Name = "nTotBunga"
        Me.nTotBunga.Size = New System.Drawing.Size(121, 20)
        Me.nTotBunga.TabIndex = 232
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(10, 295)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl23.TabIndex = 231
        Me.LabelControl23.Text = "Bunga"
        '
        'nNominal
        '
        Me.nNominal.Enabled = False
        Me.nNominal.Location = New System.Drawing.Point(105, 265)
        Me.nNominal.Name = "nNominal"
        Me.nNominal.Size = New System.Drawing.Size(121, 20)
        Me.nNominal.TabIndex = 230
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(10, 269)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl22.TabIndex = 229
        Me.LabelControl22.Text = "Nominal"
        '
        'nPajak
        '
        Me.nPajak.Location = New System.Drawing.Point(105, 317)
        Me.nPajak.Name = "nPajak"
        Me.nPajak.Size = New System.Drawing.Size(121, 20)
        Me.nPajak.TabIndex = 226
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(10, 320)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl9.TabIndex = 225
        Me.LabelControl9.Text = "Pajak"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(105, 5)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 222
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(10, 9)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl6.TabIndex = 221
        Me.LabelControl6.Text = "Rek. Tabungan"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(10, 216)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl17.TabIndex = 154
        Me.LabelControl17.Text = "Jatuh Tempo"
        '
        'dJatuhTempo
        '
        Me.dJatuhTempo.EditValue = Nothing
        Me.dJatuhTempo.Location = New System.Drawing.Point(105, 213)
        Me.dJatuhTempo.Name = "dJatuhTempo"
        Me.dJatuhTempo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJatuhTempo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJatuhTempo.Size = New System.Drawing.Size(82, 20)
        Me.dJatuhTempo.TabIndex = 153
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(10, 190)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl3.TabIndex = 152
        Me.LabelControl3.Text = "Tgl Penempatan"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(105, 187)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 151
        '
        'LabelControl13
        '
        Me.LabelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl13.Location = New System.Drawing.Point(142, 139)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl13.TabIndex = 150
        Me.LabelControl13.Text = "%"
        '
        'nBungaBaru
        '
        Me.nBungaBaru.Enabled = False
        Me.nBungaBaru.Location = New System.Drawing.Point(105, 135)
        Me.nBungaBaru.Name = "nBungaBaru"
        Me.nBungaBaru.Properties.Mask.EditMask = "n"
        Me.nBungaBaru.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nBungaBaru.Size = New System.Drawing.Size(32, 20)
        Me.nBungaBaru.TabIndex = 149
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(10, 138)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl16.TabIndex = 148
        Me.LabelControl16.Text = "Suku Bunga Baru"
        '
        'LabelControl2
        '
        Me.LabelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl2.Location = New System.Drawing.Point(143, 164)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl2.TabIndex = 147
        Me.LabelControl2.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.Enabled = False
        Me.nLama.Location = New System.Drawing.Point(105, 161)
        Me.nLama.Name = "nLama"
        Me.nLama.Properties.Mask.EditMask = "n"
        Me.nLama.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nLama.Size = New System.Drawing.Size(32, 20)
        Me.nLama.TabIndex = 146
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 164)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl7.TabIndex = 145
        Me.LabelControl7.Text = "Jangka Waktu"
        '
        'nBunga
        '
        Me.nBunga.Enabled = False
        Me.nBunga.Location = New System.Drawing.Point(105, 109)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Size = New System.Drawing.Size(32, 20)
        Me.nBunga.TabIndex = 144
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(10, 113)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl11.TabIndex = 143
        Me.LabelControl11.Text = "Suku Bunga Lama"
        '
        'cGolonganDeposito
        '
        Me.cGolonganDeposito.Enabled = False
        Me.cGolonganDeposito.Location = New System.Drawing.Point(105, 83)
        Me.cGolonganDeposito.Name = "cGolonganDeposito"
        Me.cGolonganDeposito.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposito.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposito.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposito.TabIndex = 142
        '
        'cKeteranganGolonganDeposito
        '
        Me.cKeteranganGolonganDeposito.Enabled = False
        Me.cKeteranganGolonganDeposito.Location = New System.Drawing.Point(143, 83)
        Me.cKeteranganGolonganDeposito.Name = "cKeteranganGolonganDeposito"
        Me.cKeteranganGolonganDeposito.Size = New System.Drawing.Size(189, 20)
        Me.cKeteranganGolonganDeposito.TabIndex = 141
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 86)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl1.TabIndex = 140
        Me.LabelControl1.Text = "Gol. Deposito"
        '
        'cGolonganDeposan
        '
        Me.cGolonganDeposan.Enabled = False
        Me.cGolonganDeposan.Location = New System.Drawing.Point(105, 57)
        Me.cGolonganDeposan.Name = "cGolonganDeposan"
        Me.cGolonganDeposan.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposan.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposan.TabIndex = 139
        '
        'cKeteranganGolonganDeposan
        '
        Me.cKeteranganGolonganDeposan.Enabled = False
        Me.cKeteranganGolonganDeposan.Location = New System.Drawing.Point(143, 57)
        Me.cKeteranganGolonganDeposan.Name = "cKeteranganGolonganDeposan"
        Me.cKeteranganGolonganDeposan.Size = New System.Drawing.Size(189, 20)
        Me.cKeteranganGolonganDeposan.TabIndex = 138
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(10, 60)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl8.TabIndex = 137
        Me.LabelControl8.Text = "Gol. Deposan"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(2, 524)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(825, 33)
        Me.PanelControl4.TabIndex = 26
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(739, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(658, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekeningLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTanggal)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(825, 144)
        Me.PanelControl1.TabIndex = 24
        '
        'cRekeningLama
        '
        Me.cRekeningLama.Location = New System.Drawing.Point(104, 35)
        Me.cRekeningLama.Name = "cRekeningLama"
        Me.cRekeningLama.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLama.Size = New System.Drawing.Size(158, 20)
        Me.cRekeningLama.TabIndex = 131
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 39)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl10.TabIndex = 130
        Me.LabelControl10.Text = "Rekening Lama"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 61)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 113)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 116)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 65)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 87)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 90)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTanggal
        '
        Me.dTanggal.EditValue = Nothing
        Me.dTanggal.Location = New System.Drawing.Point(104, 9)
        Me.dTanggal.Name = "dTanggal"
        Me.dTanggal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTanggal.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTanggal.Size = New System.Drawing.Size(82, 20)
        Me.dTanggal.TabIndex = 82
        '
        'lblTglPenempatan
        '
        Me.lblTglPenempatan.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblTglPenempatan.Location = New System.Drawing.Point(193, 190)
        Me.lblTglPenempatan.Name = "lblTglPenempatan"
        Me.lblTglPenempatan.Size = New System.Drawing.Size(26, 13)
        Me.lblTglPenempatan.TabIndex = 247
        Me.lblTglPenempatan.Text = "Bulan"
        '
        'lblJatuhTempo
        '
        Me.lblJatuhTempo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJatuhTempo.Location = New System.Drawing.Point(193, 216)
        Me.lblJatuhTempo.Name = "lblJatuhTempo"
        Me.lblJatuhTempo.Size = New System.Drawing.Size(26, 13)
        Me.lblJatuhTempo.TabIndex = 248
        Me.lblJatuhTempo.Text = "Bulan"
        '
        'trPerpanjanganDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 558)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trPerpanjanganDeposito"
        Me.Text = "Perpanjangan Deposito"
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbJenisDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPerpanjanganKe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNominalBaru.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJatuhTempo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJatuhTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBungaBaru.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTanggal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTanggal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cNamaTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPerpanjanganKe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNominalBaru As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTotBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNominal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJatuhTempo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBungaBaru As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeteranganGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeteranganGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekeningLama As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTanggal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbJenisDeposito As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblJatuhTempo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglPenempatan As DevExpress.XtraEditors.LabelControl
End Class
