﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trBatalTutupTabungan
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        cKunci.Text = ""
    End Sub

    Private Sub trBatalTutupTabungan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("tabungan t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trBatalTutupTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cKunci.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cKunci.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "close,tgl"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "tabungan", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    If .Item("close").ToString = "1" Then
                        MessageBox.Show("Rekening sudah ditutup. Lakukan proses PEMBATALAN PENUTUPAN REKENING TABUNGAN terlebih dahulu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        getmemory()
                    End If
                    If CDate(.Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub GetMemory()
        Const cField As String = "d.*,r.Nama,r.Alamat,m.faktur"
        Dim cWhere As String = String.Format(" and d.TglPenutupan='{0}' and d.Close='1' and m.tgl=d.tglpenutupan ", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
        Dim vaJoin() As Object = {"Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan", _
                                  "Left Join RegisterNasabah r on t.Kode = r.Kode"}
        dtData = objData.Browse(GetDSN, "Tabungan t", cField, "t.Rekening", , cRekening.Text, cWhere, , vaJoin)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cFaktur.Text = .Item("faktur").ToString
                cNama.Text = .Item("NamaNasabah").ToString
                cAlamat.Text = .Item("AlamatNasabah").ToString
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As New DataTable
        ValidSaving = True

        If cKunci.Text <> "PEMBATALAN TUTUP TABUNGAN" Then
            MsgBox("Kata Kunci Salah, Transaksi Tidak Bisa di Lanjutkan ..!", vbExclamation)
            ValidSaving = False
            cKunci.Focus()
            cKunci.Text = ""
            Exit Function
        End If

        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        cDataTable = objData.Browse(GetDSN, "tabungan", "close", "rekening", , cRekening.Text)
        If cDataTable.Rows.Count > 0 Then
            If cDataTable.Rows(0).Item("close").ToString = "0" Then
                MessageBox.Show("Rekening tabungan belum ditutup. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        Else
            MessageBox.Show("Rekening tabungan tidak ditemukan. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
        cDataTable.Dispose()
    End Function

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        simpanData()
    End Sub

    Private Sub simpanData()
        If MessageBox.Show("Proses dilanjutkan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            If ValidSaving() Then
                Dim cWhere As String = String.Format("and tgl = '{0}' and faktur = '{1}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd), cFaktur.Text)
                dtData = objData.Browse(GetDSN, "mutasitabungan", "kodetransaksi", "rekening", , cRekening.Text, cWhere)
                If dtData.Rows.Count > 0 Then
                    With dtData.Rows(0)
                        If .Item("KodeTransaksi").ToString <> aCfg(eCfg.msKodeSetoranTunai).ToString And .Item("KodeTransaksi").ToString <> aCfg(eCfg.msKodePenyetoranPemindahbukuan).ToString Then
                            objData.Delete(GetDSN, "MutasiTabungan", "Faktur", , cFaktur.Text)
                            objData.Delete(GetDSN, "TotJurnal", "Faktur", , cFaktur.Text)
                            objData.Delete(GetDSN, "Jurnal", "faktur", , cFaktur.Text)

                            ' hapus data buku besar
                            DelBukuBesar(cFaktur.Text)
                        End If
                    End With
                End If

                ' Edit status tabungan di tabel tabungan
                cWhere = String.Format("Rekening='{0}", cRekening.Text) & "'"
                Dim vaField() As Object = {"Close", "TglPenutupan"}
                Dim vaValue() As Object = {"0", "9999-12-31"}
                objData.Edit(GetDSN, "Tabungan", cWhere, vaField, vaValue)
            End If
            MessageBox.Show("Pembatalan selesai.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class