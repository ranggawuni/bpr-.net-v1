﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trPenerimaanKasBesar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dbData IsNot Nothing Then
                    dbData.Dispose()
                    dbData = Nothing
                End If
                If dtTable IsNot Nothing Then
                    dtTable.Dispose()
                    dtTable = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trPenerimaanKasBesar))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.ckFaktur = New DevExpress.XtraEditors.CheckEdit()
        Me.cNamaDebet = New DevExpress.XtraEditors.TextEdit()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.nJumlah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cDebet = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.cKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ckFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nJumlah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.ckFaktur)
        Me.PanelControl1.Controls.Add(Me.cNamaDebet)
        Me.PanelControl1.Controls.Add(Me.nTotal)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.cKeterangan)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cNamaKredit)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.nJumlah)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.cDebet)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cFaktur)
        Me.PanelControl1.Controls.Add(Me.cKredit)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(467, 173)
        Me.PanelControl1.TabIndex = 16
        '
        'ckFaktur
        '
        Me.ckFaktur.Location = New System.Drawing.Point(345, 10)
        Me.ckFaktur.Name = "ckFaktur"
        Me.ckFaktur.Properties.Caption = "Faktur Sebagian"
        Me.ckFaktur.Size = New System.Drawing.Size(108, 19)
        Me.ckFaktur.TabIndex = 206
        '
        'cNamaDebet
        '
        Me.cNamaDebet.Location = New System.Drawing.Point(215, 61)
        Me.cNamaDebet.Name = "cNamaDebet"
        Me.cNamaDebet.Size = New System.Drawing.Size(238, 20)
        Me.cNamaDebet.TabIndex = 144
        '
        'nTotal
        '
        Me.nTotal.Enabled = False
        Me.nTotal.Location = New System.Drawing.Point(312, 139)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(141, 20)
        Me.nTotal.TabIndex = 142
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(282, 142)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl1.TabIndex = 141
        Me.LabelControl1.Text = "Total"
        '
        'cKeterangan
        '
        Me.cKeterangan.Location = New System.Drawing.Point(104, 113)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(349, 20)
        Me.cKeterangan.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 117)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Keterangan"
        '
        'cNamaKredit
        '
        Me.cNamaKredit.Enabled = False
        Me.cNamaKredit.Location = New System.Drawing.Point(215, 87)
        Me.cNamaKredit.Name = "cNamaKredit"
        Me.cNamaKredit.Size = New System.Drawing.Size(238, 20)
        Me.cNamaKredit.TabIndex = 110
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl10.TabIndex = 106
        Me.LabelControl10.Text = "Kredit"
        '
        'nJumlah
        '
        Me.nJumlah.Enabled = False
        Me.nJumlah.Location = New System.Drawing.Point(104, 139)
        Me.nJumlah.Name = "nJumlah"
        Me.nJumlah.Size = New System.Drawing.Size(141, 20)
        Me.nJumlah.TabIndex = 104
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 146)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl9.TabIndex = 103
        Me.LabelControl9.Text = "Mutasi"
        '
        'cDebet
        '
        Me.cDebet.Location = New System.Drawing.Point(104, 61)
        Me.cDebet.Name = "cDebet"
        Me.cDebet.Size = New System.Drawing.Size(105, 20)
        Me.cDebet.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(29, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Debet"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(9, 38)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(51, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Faktur"
        '
        'cFaktur
        '
        Me.cFaktur.EditValue = "123456"
        Me.cFaktur.Location = New System.Drawing.Point(104, 35)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Properties.Mask.EditMask = "##.######"
        Me.cFaktur.Properties.MaxLength = 6
        Me.cFaktur.Size = New System.Drawing.Size(208, 20)
        Me.cFaktur.TabIndex = 80
        '
        'cKredit
        '
        Me.cKredit.Location = New System.Drawing.Point(104, 87)
        Me.cKredit.Name = "cKredit"
        Me.cKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cKredit.Size = New System.Drawing.Size(106, 20)
        Me.cKredit.TabIndex = 111
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(2, 428)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(467, 33)
        Me.PanelControl2.TabIndex = 15
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(378, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Location = New System.Drawing.Point(297, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(2, 178)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(467, 246)
        Me.GridControl1.TabIndex = 31
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'trPenerimaanKasBesar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 464)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "trPenerimaanKasBesar"
        Me.Text = "Penerimaan Kas Besar"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ckFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nJumlah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nJumlah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cDebet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cNamaDebet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents ckFaktur As DevExpress.XtraEditors.CheckEdit
End Class
