﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trBlokirDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.Text = ""
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cGolonganDeposito.Text = ""
        cNamaGolonganDeposito.Text = ""
        nSaldoAwal.Text = ""
        optBlokir.SelectedIndex = 0
        optSemua.SelectedIndex = 0
        nBlokir.Text = ""
        cKeterangan.Text = ""
    End Sub

    Private Sub trBlokirDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trBlokirDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitForm(Me, True, True, cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(optBlokir.TabIndex, n)
        SetTabIndex(optSemua.TabIndex, n)
        SetTabIndex(nBlokir.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        optBlokir.EnterMoveNextControl = True
        optSemua.EnterMoveNextControl = True
        nBlokir.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True

        FormatTextBox(nBlokir, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoAwal, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "close,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("nominal")) = 0 Then
                        MessageBox.Show("Pembukaan Deposito Belum Dilakukan, Data Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        If CInt(cDataTable.Rows(0).Item("Status")) <> 0 Then
                            MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            InitValue()
                            cRekening.Focus()
                        Else
                            GetMemory()
                        End If
                        If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                            MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.Focus()
                            Exit Sub
                        End If
                        GetMemory()
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.Text = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        Dim cField As String = ""
        cField = "t.rekening,t.jumlahblokir,r.Nama as NamaNasabah,r.Alamat as AlamatNasabah,r.telepon,g.Keterangan as NamaGolonganDeposito,"
        cField = cField & "t.KeteranganBlokir,t.JenisBlokir,t.golonganDeposito"
        Dim vaJoin() As Object = {"Left Join GolonganDeposito g on g.Kode = t.GolonganTabungan", _
                                  "Left Join RegisterNasabah r on t.Kode = r.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "deposito t", cField, "t.rekening", data.myOperator.Assign, cRekening.Text, _
                                        , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("NamaNasabah").ToString
                cAlamat.Text = .Item("AlamatNasabah").ToString
                cTelepon.Text = .Item("Telepon").ToString
                cGolonganDeposito.Text = .Item("GolonganDeposito").ToString
                cNamaGolonganDeposito.Text = .Item("NamaGolonganDeposito").ToString
                nSaldoAwal.Text = GetSaldoTabungan(cRekening.Text, CDate(dTgl.Text)).ToString
                nBlokir.Text = .Item("JumlahBlokir").ToString
                If Val(nBlokir.Text) > 0 Then
                    optBlokir.SelectedIndex = 0
                Else
                    optBlokir.SelectedIndex = 1
                End If
                SetOpt(optSemua, .Item("JenisBlokir").ToString)
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim cStatusBlokir As String
        Dim Keterangan As String
        Dim cJenisBlokir As String
        Dim dTglTemp As Date

        If ValidSaving() Then
            If optBlokir.SelectedIndex = 0 Then
                cStatusBlokir = "1"
                Keterangan = cKeterangan.Text
                dTglTemp = dTgl.DateTime.Date
            Else
                cStatusBlokir = "0"
                Keterangan = ""
                nBlokir.Text = "0"
                dTglTemp = #12/31/9999#
            End If
            cJenisBlokir = GetOpt(optSemua)
            If MessageBox.Show("Data benar-benar akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                Dim vaField() As Object = {"TglBlokir", "StatusBlokir", "JumlahBlokir", "KeteranganBlokir", "JenisBlokir"}
                Dim vaValue() As Object = {dTglTemp, cStatusBlokir, Val(nBlokir.Text), Keterangan, cJenisBlokir}
                objData.Edit(GetDSN(), "Deposito", String.Format("Rekening = '{0}'", cRekening.Text), vaField, vaValue)
                MessageBox.Show("Data sudah disimpan.", "Notifikasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cRekening.SelectedText = ""
                cRekening.EditValue = ""
                InitValue()
                cRekening.Focus()
            End If
        End If
    End Sub

    Private Sub dTgl_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dTgl.LostFocus
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

    Private Sub nBlokir_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles nBlokir.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Val(nBlokir.Text) > Val(nSaldoAwal.Text) Then
                MessageBox.Show("Nominal yang akan diblokir tidak boleh melebihi saldo awal.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                nBlokir.Focus()
            End If
        End If
    End Sub
End Class