﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trHapusMutasiTabungan
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        dTgl1.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        nSaldoAkhir.Text = ""
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "Faktur,Tgl,KodeTransaksi as SD,Jumlah,UserName,ID"
            Dim cWhere As String = String.Format(" and tgl >= '{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = String.Format(" {0} and tgl <= '{1}'", cWhere, formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "mutasitabungan", cField, "rekening", , cRekening.Text, cWhere, "tgl,id")
            GridControl1.DataSource = cDataSet.Tables("mutasitabungan")
            cDataSet.Dispose()
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "Tgl")
            GridColumnFormat(GridView1, "SD")
            GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "UserName")
            GridColumnFormat(GridView1, "ID", , , 0)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub HapusMutasi()
        Dim nRow As DataRow
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    DelMutasiTabungan(nRow(0).ToString, nRow(2).ToString, True)
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trHapusMutasiTabungan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("tabungan t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trHapusMutasiTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , cmdHapus, cmdRefresh, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dTgl1.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True

        FormatTextBox(nSaldoAkhir, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "t.close,t.tgl,r.nama,r.alamat"
            Dim vaJoin() As Object = {"left join registernasabah r on r.kode = t.kode"}
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "tabungan t", cField, "t.rekening", , cRekening.Text, , , vaJoin)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    If .Item("close").ToString = "1" Then
                        MessageBox.Show("Rekening sudah ditutup. Lakukan proses PEMBATALAN PENUTUPAN REKENING TABUNGAN terlebih dahulu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        cNama.Text = .Item("nama").ToString
                        cAlamat.Text = .Item("alamat").ToString
                        nSaldoAkhir.Text = GetSaldoTabungan(cRekening.Text, dTgl1.DateTime).ToString
                        GetSQL()
                    End If
                    If CDate(.Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As New DataTable
        ValidSaving = True
        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        cDataTable = objData.Browse(GetDSN, "tabungan", "close", "rekening", , cRekening.Text)
        If cDataTable.Rows.Count > 0 Then
            If cDataTable.Rows(0).Item("close").ToString = "1" Then
                MessageBox.Show("Rekening tabungan sudah ditutup. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        Else
            MessageBox.Show("Rekening tabungan tidak ditemukan. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
        cDataTable.Dispose()
    End Function

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        HapusMutasi()
    End Sub
End Class