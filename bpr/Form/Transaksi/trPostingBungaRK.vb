﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trPostingBungaRK
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim dtTable As New DataTable
    Private ReadOnly x As TypeKolektibilitas = Nothing

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
    End Sub

    Private Sub InitTable()
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nama", System.Type.GetType("System.String"))
        AddColumn(dtTable, "BakiDebet", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "SukuBunga", System.Type.GetType("System.Single"))
        AddColumn(dtTable, "Lama", System.Type.GetType("System.Int32"))
        AddColumn(dtTable, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TglRealisasi", System.Type.GetType("System.DateTime"))
        AddColumn(dtTable, "RekeningTabungan", System.Type.GetType("System.String"))
        AddColumn(dtTable, "SaldoTabungan", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Status", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Plafond", System.Type.GetType("System.Double"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim n As Integer
            Dim nBakiDebet As Double
            Dim dTanggal As Date
            Dim nHari As Double
            Dim nHari1 As Double
            Dim lBelumLunas As Boolean
            Dim nStatus As Integer
            Dim nRow As DataRow = dtTable.NewRow()

            If dTgl.DateTime = EOM(dTgl.DateTime) Then
                dTanggal = EOM(dTgl.DateTime)
                nHari = dTanggal.Day
                nHari1 = nHari
                If dTanggal.Day = 30 Then
                    nHari1 = nHari + 1
                ElseIf dTanggal.Day = 28 Or dTanggal.Day = 29 Then
                    If dTanggal.Day = 28 Then
                        nHari1 = nHari + 3
                    Else
                        nHari1 = nHari + 2
                    End If
                End If
            Else
                Dim a As Integer
                Dim dTemp As Date
                nHari = dTgl.DateTime.Day
                nHari1 = nHari
                dTemp = EOM(dTgl.DateTime)
                ' jika tgl posting adalah hari jumat dan tgl akhir bulan adalah hari sabtu atau minggu maka
                ' hari sabtu dan minggu itu digabungkan jadi satu dihari jumat
                If Weekday(dTgl.DateTime) = 6 And (Weekday(dTemp) = 7 Or Weekday(dTemp) = 1) Then
                    a = EOM(dTgl.DateTime).Day
                    For n = a To 1 Step -1
                        If IsHoliday(dTemp) Then
                            dTemp = DateAdd("d", -1, dTemp)
                            If dTemp.Day = 30 Then
                                nHari1 = nHari + 1
                            ElseIf dTemp.Day = 28 Or dTemp.Day = 29 Then
                                nHari1 = 31
                            End If
                            Exit For
                        End If
                    Next
                End If
            End If
            Dim cField As String = "d.rekening,r.nama,d.plafond,d.sukubunga,d.lama,d.Tgl,d.tglPelunasan,d.CaraPerhitungan,r.kode,"
            cField = cField & "d.rekeningtabungan"
            '  cWhere = " and day(d.tgl) >= " & nHari & " and day(d.Tgl) <= " & nHari1 & " and d.caraperhitungan = '7'"
            Const cWhere As String = " and d.caraperhitungan = '7'"
            Dim vaJoin() As Object = {"left join registernasabah r on r.kode = d.kode"}
            dtData = objData.Browse(GetDSN, "debitur d", cField, "d.tgl", data.myOperator.LowerThanEqual, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd),
                                    cWhere, "d.Rekening", vaJoin)
            If dtData.Rows.Count > 0 Then
                For n = 0 To dtData.Rows.Count - 1
                    With dtData.Rows(n)
                        nBakiDebet = GetBakiDebet(.Item("Rekening").ToString, dTgl.DateTime)
                        lBelumLunas = False
                        If nBakiDebet <> 0 Then
                            lBelumLunas = True
                        Else
                            If .Item("CaraPerhitungan").ToString = "7" Then
                                If formatValue(CDate(.Item("TglPelunasan")), formatType.yyyy_MM_dd) >= "9999-99-99" And nBakiDebet = 0 Then
                                    lBelumLunas = True
                                End If
                            End If
                        End If
                        If lBelumLunas Then
                            GetTunggakan(.Item("Rekening").ToString, dTgl.DateTime, x, , , True, , .Item("Kode").ToString)
                            If x.dJthTmp >= dTgl.DateTime Then
                                If CekDataMutasi(.Item("Rekening").ToString, dTgl.DateTime) Then
                                    nStatus = 1
                                Else
                                    nStatus = 0
                                End If
                                nRow("Rekening") = .Item("Rekening").ToString
                                nRow("Nama") = .Item("Nama").ToString
                                nRow("BakiDebet") = nBakiDebet
                                nRow("SukuBunga") = CSng(.Item("SukuBunga"))
                                nRow("Lama") = CDbl(.Item("Lama"))
                                nRow("Bunga") = getDataBungaRK(.Item("Rekening").ToString, CSng(.Item("SukuBunga")), dTgl.DateTime, CDate(.Item("Tgl")))
                                nRow("TglRealisasi") = CDate(.Item("Tgl"))
                                nRow("RekeningTabungan") = .Item("RekeningTabungan").ToString
                                nRow("SaldoTabungan") = GetSaldoTabungan(.Item("RekeningTabungan").ToString, dTgl.DateTime)
                                nRow("Status") = ""
                                nRow("Plafond") = CDbl(.Item("Plafond"))
                                dtTable.Rows.Add(nRow)
                            End If
                        End If
                    End With
                Next
            End If
            GridControl1.DataSource = dtData
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "BakiDebet", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "SukuBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Lama", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TglRealisasi", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, , HorzAlignment.Center)
            GridColumnFormat(GridView1, "RekeningTabungan")
            GridColumnFormat(GridView1, "SaldoTabungan", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status")
            GridColumnFormat(GridView1, "Plafond", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Function CekDataMutasi(ByVal cRekening As String, ByVal dTanggal As Date) As Boolean
        Dim db As New DataTable

        Dim cWhere As String = String.Format(" and date_format(tgl,'%Y%m') = '{0}' and dbunga>0", Format(dTanggal, "yyyymm"))
        db = objData.Browse(GetDSN, "angsuran", , "rekening", , cRekening, cWhere)
        If db.Rows.Count > 0 Then
            CekDataMutasi = True
        Else
            CekDataMutasi = False
        End If
        db.Dispose()
    End Function

    Private Sub SimpanMutasi()
        Dim nRow As DataRow
        Dim cKodeCabang As String = ""
        Dim cStatusPembayaran As String = ""
        Dim dTanggal As Date
        Dim cFaktur As String = ""
        Dim cKeterangan As String

        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    If nRow("RekeningTabungan").ToString.Length = 12 Then
                        cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                        cStatusPembayaran = "2"
                        '## proses hanya jika ada jadwalnya saja ##
                        If CDbl(nRow("Bunga")) > 0 Then
                            ' update data jadwal. Dibentuk duluan meskipun ada saldo tabungan atau tidak
                            ' karena telah dimungkinkan untuk melakukan pembayaran via angsuran kasir
                            dTanggal = dTgl.DateTime
                            cFaktur = GetLastFaktur(eFaktur.fkt_Jadwal_Angsuran, dTanggal, , True)
                            cKeterangan = "Bunga RK " & GetLocalDate(dTanggal, False, , , True)
                            UpdAngsuranPembiayaan(cKodeCabang, cFaktur, dTanggal, nRow("Rekening").ToString, eAngsuran.ags_jadwal, 0,
                                                  CDbl(nRow("Bunga")), 0, cKeterangan, , , , , , "T", , cStatusPembayaran)
                            If Val(nRow("SaldoTabungan")) >= Val(nRow("Bunga")) Then
                                ' update data angsuran
                                cFaktur = GetLastFaktur(eFaktur.fkt_Angsuran, dTanggal, , True)
                                UpdAngsuranPembiayaan(cKodeCabang, cFaktur, dTanggal, nRow("Rekening").ToString, eAngsuran.ags_Angsuran, 0,
                                                      CDbl(nRow("Bunga")), 0, cKeterangan, , , , , , "T", , cStatusPembayaran)
                                nRow("Status") = "Berhasil"
                                GridView1.SetRowCellValue(n, "Status", "Berhasil")
                            Else
                                nRow("Status") = "Saldo Tab. Tidak Cukup"
                                GridView1.SetRowCellValue(n, "Status", "Saldo Tab. Tidak Cukup")
                            End If
                        Else
                            nRow("Status") = "Rek. Tabungan Tidak Ada"
                            GridView1.SetRowCellValue(n, "Status", "Rek. Tabungan Tidak Ada")
                        End If
                    End If
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trPostingBungaRK_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        InitTable()
        SetButtonPicture(, , , cmdSimpan, cmdRefresh, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanMutasi()
    End Sub
End Class