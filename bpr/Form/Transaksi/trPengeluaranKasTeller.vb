﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trPengeluaranKasTeller
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dbData As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        If Not lEdit Or nPos = myPos.add Then
            InitValue()
        End If
        If lEdit Then
            If nPos = myPos.add Then
                cFaktur.Enabled = False
                cDebet.Focus()
                ckFaktur.Visible = False
            Else
                cFaktur.Enabled = True
                ckFaktur.Visible = True
                cFaktur.Focus()
            End If
            GetFaktur(False)
        End If
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cFaktur.Text = ""
        cDebet.EditValue = ""
        cNamaDebet.Text = ""
        cKredit.Text = ""
        cNamaKredit.Text = ""
        cKeterangan.Text = ""
        nJumlah.Text = "0"

        cKredit.Text = cKasTeller
        dbData = objData.Browse(GetDSN, "Rekening", , "Kode", , cKredit.Text)
        If dbData.Rows.Count > 0 Then
            cNamaKredit.Text = dbData.Rows(0).Item("Keterangan").ToString
        End If
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            DelBukuBesar(cFaktur.Text)

            objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur.Text)

            objData.Delete(GetDSN, "totjurnal", "Faktur", , cFaktur.Text)
            'UpdActivity(Me, GetActivity)
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cFaktur.Text, "Nomor faktur tidak boleh kosong. Ulangi pengisian.") Then
            Return False
            cFaktur.Focus()
            Exit Function
        End If

        If Not SeekData("Rekening", "Kode", cKredit.Text) Then
            ValidSaving = False
            MsgBox("Rekening Debet Tidak Ditemukan, Transaksi tidak bisa disimpan ...!", vbExclamation)
            Exit Function
        End If

        If Not SeekData("Rekening", "Kode", cDebet.Text) Then
            ValidSaving = False
            MsgBox("Rekening Kredit Tidak Ditemukan, Transaksi tidak bisa disimpan ...!", vbExclamation)
            Exit Function
        End If

        If CDbl(nJumlah.Text) = 0 Then
            MsgBox("Jumlah Tidak Boleh Kosong, Transaksi tidak bisa disimpan ...!", vbExclamation)
            ValidSaving = False
            Exit Function
        End If

        If IsHoliday(dTgl.DateTime) Then
            MsgBox("Proses Tidak Boleh Dilakukan Pada Hari Libur!", vbExclamation)
            ValidSaving = False
            dTgl.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    GetFaktur(True)
                    Dim cCabang As String = aCfg(eCfg.msKodeCabang).ToString

                    UpdTotJurnalLainLain(cFaktur.Text, dTgl.DateTime, cUserName, SNow, CDbl(nJumlah.Text), CDbl(nJumlah.Text),
                                         cCabang, cKeterangan.Text)

                    UpdJurnalLainLain(cFaktur.Text, dTgl.DateTime, cDebet.Text, cKeterangan.Text, CDbl(nJumlah.Text), 0, cCabang)

                    UpdJurnalLainLain(cFaktur.Text, dTgl.DateTime, cKredit.Text, cKeterangan.Text, 0, CDbl(nJumlah.Text), cCabang)

                    UpdRekJurnal(cFaktur.Text)

                    'If MsgBox("Apakah anda akan mencetak bukti kas masuk...?", vbQuestion + vbYesNo) = vbYes Then
                    '    CetakSlipKas()
                    'End If
                    'If MsgBox("Cetak Validasi Kas?", vbQuestion + vbYesNo) = vbYes Then
                    '    '        CetakValidasiKas cFaktur.Text
                    '    CetakValidasiKasDetail cFaktur.Text
                    'End If

                    'UpdActivity(Me, GetActivity)
                    MsgBox("Data sudah disimpan.", vbInformation)
                Else
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub GetFaktur(ByVal lUpdate As Boolean)
        If nPos = myPos.add Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_KasKeluar, dTgl.DateTime, , lUpdate)
        End If
    End Sub

    Private Sub GetMemory()
        Const cField As String = "j.*,r.Keterangan as NamaRekening"
        Dim vaJoin() As Object = {"Left Join Rekening r on r.Kode = j.Rekening",
                                  "left join TotJurnal t on j.faktur=t.faktur"}
        Dim cDataTable = objData.Browse(GetDSN, "jurnal j", cField, "j.faktur", data.myOperator.Assign, cFaktur.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            For n As Integer = 0 To cDataTable.Rows.Count - 1
                With cDataTable.Rows(n)
                    dTgl.DateTime = CDate(.Item("tgl").ToString)
                    If CDbl(.Item("Debet")) <> 0 Then
                        cDebet.Text = .Item("Rekening").ToString
                        cNamaDebet.Text = .Item("NamaRekening").ToString
                        nJumlah.Text = .Item("Debet").ToString
                        cKeterangan.Text = .Item("Keterangan").ToString
                    Else
                        cKredit.Text = .Item("rekening").ToString
                        cNamaKredit.Text = .Item("NamaRekening").ToString
                    End If
                End With
            Next
        End If
    End Sub

    Private Sub trPengeluaranKasTeller_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("Rekening", cDebet, "and jenis ='D'")
    End Sub

    Private Sub trPengeluaranKasTeller_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cFaktur.TabIndex, n)
        SetTabIndex(cDebet.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(nJumlah.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cFaktur.EnterMoveNextControl = True
        cDebet.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        nJumlah.EnterMoveNextControl = True

        FormatTextBox(nJumlah, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cFaktur.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKredit_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cDebet.Closed
        GetKeteranganLookUp(cDebet, cNamaKredit)
    End Sub

    Private Sub cKredit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cDebet.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cDebet, cNamaKredit)
        End If
    End Sub

    Private Sub cFaktur_KeyDown(sender As Object, e As KeyEventArgs) Handles cFaktur.KeyDown
        If e.KeyCode = Keys.Enter Then
            If cFaktur.Text.Trim = "" Then
                cFaktur.Focus()
                Exit Sub
            End If
            If ckFaktur.Checked Then
                cFaktur.Text = GetLastFaktur(eFaktur.fkt_KasKeluar, dTgl.DateTime, cFaktur.Text)
            End If
            Const cField As String = "j.*,r.Keterangan as NamaRekening"
            Dim cWhere As String = String.Format("and left(faktur,2)='TM' and tgl='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            Dim vaJoin() As Object = {"Left Join Rekening r on r.Kode = j.Rekening",
                                       "left join TotJurnal t on j.faktur=t.faktur"}
            dbData = objData.Browse(GetDSN, "jurnal j", cField, "j.Faktur", , cFaktur.Text, cWhere, , vaJoin)
            If dbData.Rows.Count > 0 Then
                cFaktur.Text = dbData.Rows(0).Item("faktur").ToString
                If nPos = myPos.add Then  ' Tambah
                    MessageBox.Show("Nomor Faktur Sudah Ada, Data Tidak bisa Ditambah", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cFaktur.Focus()
                    Exit Sub
                End If
                GetMemory()
                If nPos = myPos.delete Then
                    DeleteData()
                End If
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Nomor Faktur Tidak Ada, Ulangi Pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cFaktur.Focus()
            End If
        End If
    End Sub
End Class