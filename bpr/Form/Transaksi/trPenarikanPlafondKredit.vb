﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trPenarikanPlafondKredit
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim cCaraPerhitungan As String
    Dim nLama As Integer

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nPlafond.Text = ""
        nbakidebet.text = ""
        nPenarikan.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        optKas.SelectedIndex = 1
    End Sub

    Private Sub trPenarikanPlafondKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trPenarikanPlafondKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nPenarikan.TabIndex, n)
        SetTabIndex(optKas.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        optKas.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBakiDebet, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPenarikan, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "statuspencairan,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("statuspencairan")) = 0 Then
                        Dim lCair As Boolean = cDataTable.Rows(0).Item("StatusPencairan").ToString <> "0"
                        If Not lCair Then
                            GetMemory()
                        Else
                            MessageBox.Show("Kredit sudah dicairkan, proses tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.EditValue = ""
                            cRekening.Focus()
                            Exit Sub
                        End If
                    Else
                        MessageBox.Show("Kredit sudah dicairkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , False).ToString
        Dim cField As String = "d.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,"
        cField = cField & "rn.Nama as NamaNasabah,g.Keterangan as NamaGolonganKredit"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join Tabungan t on d.RekeningTabungan = t.Rekening",
                                  "Left Join RegisterNasabah rn on rn.Kode = t.Kode",
                                  "Left Join GolonganKredit g on d.GolonganKredit = g.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "debitur d", cField, "t.rekening", , cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("NamaDeposan").ToString
                cAlamat.Text = .Item("AlamatDeposan").ToString
                nPlafond.Text = .Item("GolonganDeposan").ToString
                cCaraPerhitungan = .Item("CaraPerhitungan").ToString
                nBakiDebet.Text = .Item("Administrasi").ToString
                Dim nTemp As Double = CDbl(nPlafond.Text) - CDbl(nBakiDebet.Text)
                nPenarikan.Text = nTemp.ToString
                nLama = CInt(.Item("Lama").ToString)
                If .Item("RekeningTabungan").ToString.Length <> 12 Then
                    cRekeningTabungan.Text = .Item("RekeningTabungan").ToString
                    cNamaNasabah.Text = .Item("NamaNasabah").ToString
                Else
                    cRekeningTabungan.EditValue = ""
                    cNamaNasabah.Text = ""
                End If
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True
        If Not CheckData(nPlafond.Text, "Plafond Tidak Boleh Kosong,Ulangi Pengisian...") Then
            ValidSaving = False
            cmdKeluar.Focus()
            Exit Function
        End If

        If Len(cRekening) < 12 Then
            MsgBox("Nomor Rekening Tidak Lengkap. Transaksi Tidak Bisa Dilanjutkan.", vbExclamation)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If optKas.SelectedIndex = 2 Then
            If cRekeningTabungan.Text = "" Then
                MessageBox.Show("Rekening Tabungan Tidak Ada." & vbCrLf & "Anda Tidak Dapat Menyimpan Data Ini", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
            If cRekeningTabungan.Text.Length < 12 Then
                MsgBox("Nomor Rekening Tabungan Tidak Lengkap. Transaksi Tidak Bisa Dilanjutkan.", vbExclamation)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        End If

        If CDbl(nPenarikan.Text) > CDbl(nPlafond.Text) - CDbl(nBakiDebet.Text) Then
            MessageBox.Show("Penarikan Tidak Boleh Melebihi Rp. " & formatValue(CDbl(nPlafond.Text) - CDbl(nBakiDebet.Text), formatType.BilRpPict), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            nPenarikan.Focus()
            Exit Function
        End If

        If optKas.SelectedIndex = 1 Then
            Dim db As New DataTable
            Const cField As String = "u.UserName,u.FullName,u.KasTeller,r.keterangan as NamaRekening"
            Const cWhere As String = " and u.KasTeller <> ''"
            Dim vaJoin() As Object = {"left join rekening r on r.kode = u.kasteller"}
            db = objData.Browse(GetDSN, "username u", cField, "u.username", data.myOperator.Content, cUserName, cWhere, , vaJoin)
            If db.Rows.Count > 0 Then
                cKasTeller = GetNull(db.Rows(0).Item("KasTeller")).ToString
            End If
            db.Dispose()
            Dim nSaldoKasTeller = GetSaldoRekening(cKasTeller, dTgl.DateTime)
            If nSaldoKasTeller < 0 Then
                MessageBox.Show("Saldo Kas Tidak Mencukupi!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
            If nSaldoKasTeller - CDbl(nPenarikan.Text) < 0 Then
                MessageBox.Show("Mutasi Melebihi Saldo Kas!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cmdKeluar.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim cKodeCabang As String

        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_Relisasi, dTgl.DateTime, , True)
            If ValidSaving() Then
                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                Dim cKas As String = GetOpt(optKas).ToString
                UpdPencairanKredit(cRekening.Text, cFaktur.Text, CDbl(nPenarikan.Text), dTgl.DateTime, cKas, True)

                'If MessageBox.Show("Cetak Validasi Pencairan Kredit ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                'CetakValidasiPencairanKreditDetail cFaktur.Text
                'End If

                'UpdActivity(Me, GetActivity)
                MsgBox("Rekening Kredit Telah Dicairkan.", vbInformation)
            End If

            If MessageBox.Show("Cetak Validasi ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                'CetakValidasiPokokDepositoDetail cFaktur.Text
            End If
            MsgBox("Data sudah disimpan.", vbInformation)
            InitValue()
        End If
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

End Class