﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trKoreksiStatusPengajuan
    ReadOnly objData As New data()
    Dim dbData As New DataTable

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cbStatus.Properties
        With properties
            .Items.AddRange(New String() {"1. Belum Realisasi", "2. Disetujui", "3. Ditolak"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cbStatus.SelectedIndex = 1
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cNoPengajuan.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cbStatus.Text = ""
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If cNoPengajuan.Text.Length < 9 Then
            Return False
            cNoPengajuan.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If ValidSaving() Then
            If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim vaField() As Object
                Dim vaValue() As Object
                Dim nStatus As Integer = CInt(cbStatus.Text.Substring(0, 1))
                nStatus = nStatus - 1
                If nStatus = 2 Then
                    vaField = {"StatusPengajuan"}
                    vaValue = {nStatus}
                Else
                    vaField = {"StatusPengajuan", "Keterangan", "TglPenolakan"}
                    vaValue = {nStatus, "", dTgl.DateTime}
                End If
                objData.Edit(GetDSN, "PengajuanKredit", String.Format("Rekening = '{0}'", cNoPengajuan.EditValue), vaField, vaValue)
            Else
                cNoPengajuan.Focus()
                Exit Sub
            End If
            InitValue()
        End If
    End Sub

    Private Sub trKoreksiStatusPengajuan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("pengajuankredit t", cNoPengajuan, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trKoreksiStatusPengajuan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , , cmdSimpan, cmdKeluar)
        InitValue()
        SetTabIndex(cNoPengajuan.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cbStatus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cNoPengajuan.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cbStatus.EnterMoveNextControl = True
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cNoPengajuan_Closed(sender As Object, e As ClosedEventArgs) Handles cNoPengajuan.Closed
        KeteranganLookupSearch(cNoPengajuan, cNama, "rekening", "nama")
        KeteranganLookupSearch(cNoPengajuan, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cNoPengajuan_KeyDown(sender As Object, e As KeyEventArgs) Handles cNoPengajuan.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cNoPengajuan, cNama, "rekening", "nama")
            KeteranganLookupSearch(cNoPengajuan, cAlamat, "rekening", "alamat")
            If Len(cNoPengajuan.Text) = 12 Then
                Const cField As String = "p.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,p.ao,a.nama as NamaAO"
                Dim vaJoin() As Object = {"Left Join RegisterNasabah r on r.Kode = p.Kode",
                                           "Left join AO a on a.kode=p.ao"}
                dbData = objData.Browse(GetDSN, "PengajuanKredit p", cField, "p.Rekening", , cNoPengajuan.Text, , , vaJoin)
                If dbData.Rows.Count > 0 Then
                    With dbData.Rows(0)
                        cNama.Text = GetNull(.Item("NamaDebitur"), "").ToString
                        cAlamat.Text = GetNull(.Item("AlamatDebitur"), "").ToString
                        dTgl.DateTime = CDate(GetNull(.Item("Tgl"), Date.Today))
                        For n = 0 To cbStatus.Properties.Items.Count
                            If cbStatus.Properties.Items(n).ToString.Substring(1, 1) = .Item("Keterkaitan").ToString Then
                                cbStatus.EditValue = cbStatus.Properties.Items(n).ToString
                            End If
                        Next
                    End With
                Else
                    MessageBox.Show("Data Tidak Ditemukan, Ulangi Pengisian ...!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cNoPengajuan.Focus()
                    cNoPengajuan.Text = ""
                End If
            End If
        End If
    End Sub
End Class