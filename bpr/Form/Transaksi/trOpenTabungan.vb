﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class trOpenTabungan
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim cKodeRegister As String
    Private ReadOnly dTglTemp As Date = #1/1/1900#

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cWilayah.EditValue = aCfg(eCfg.msKodeDefaultWilayah)
        If nPos = myPos.add Then
            If dTglTemp = #1/1/1900# Then
                dTgl.DateTime = Date.Today
            Else
                dTgl.DateTime = dTglTemp
            End If
        End If
    End Sub

    Private Sub Koreksi(ByVal lKoreksi As Boolean)
        cCabang1.Enabled = lKoreksi
        cRekening.Enabled = lKoreksi
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCabang1.Text = CStr(aCfg(eCfg.msKodeCabang))
        cKode.Text = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cGolonganNasabah.EditValue = ""
        cNamaGolonganNasabah.Text = ""
        optJenisNasabah.SelectedIndex = 0
        optKaryawan.SelectedIndex = 0
        cAO.EditValue = ""
        cNamaAO.Text = ""
        cmbKeterkaitan.EditValue = ""
        cWilayah.EditValue = ""
        cNamaWilayah.Text = ""
        cGolonganTabungan.EditValue = ""
        cNamaGolonganTabungan.Text = ""
        cRekening.Text = ""
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            objData.Delete(GetDSN, "Tabungan", "rekening", data.myOperator.Assign, cRekening.Text)
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As DataTable
        ValidSaving = True

        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganNasabah.Text, "Golongan harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganNasabah.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganTabungan.Text, "Golongan tabungan harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganTabungan.Focus()
            Exit Function
        End If

        If Len(cRekening.Text) < 10 Then
            Return False
            cRekening.Focus()
            Exit Function
        End If

        If nPos = myPos.add Then
            cDataTable = objData.Browse(GetDSN, "tabungan", "rekening", "rekening", data.myOperator.Assign, cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                MessageBox.Show("Rekening tabungan sudah digunakan. Ulangi pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
                cRekening.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cJenisNasabah As String = GetOpt(optJenisNasabah)
                    Dim cKaryawan As String = GetOpt(optKaryawan)
                    Dim cKeterkaitan As String = cmbKeterkaitan.Text.Substring(1, 1)
                    UpdPembukaanTabungan(cRekening.Text, dTgl.DateTime, cGolonganNasabah.Text, cGolonganTabungan.Text, cKodeRegister, cAO.Text, cWilayah.Text, _
                                         cKeterkaitan, , cJenisNasabah, cKaryawan)
                Else
                    cRekening.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Function GetFrekuensi() As String
        Dim cTemp As String = ""
        Dim nFrekuensi As Integer = 1
        Dim cCabangTemp As String
        If Len(cRekening.Text) = 10 Then
            cCabangTemp = cRekening.Text.Substring(1, 3)
        Else
            cCabangTemp = CStr(aCfg(eCfg.msKodeCabang))
        End If
        Dim cDataTable = objData.Browse(GetDSN, "tabungan", "max(rekening) as kodeKantor", "left(rekening,2)", data.myOperator.Assign, cCabangTemp)
        With cDataTable
            If .Rows.Count > 0 Then
                cTemp = .Rows(0).Item("kodeKantor").ToString.Substring(4)
                nFrekuensi = CInt(cTemp) + 1
            End If
        End With
        Dim cKodeCabangTemp As String = CStr(aCfg(eCfg.msKodeCabang))
        cTemp = String.Format("{0}1{1}", Padl(cKodeCabangTemp, 3, "0"), Padl(CStr(nFrekuensi), 6, "0"))
        Return cTemp
    End Function

    Private Sub GetMemory()
        Dim cField As String = ""
        cField = "d.Rekening,d.Tgl,d.GolonganNasabah,d.GolonganTabungan, d.Kode,d.Karyawan,"
        cField = cField & "d.Close,d.JumlahBlokir,d.AO,d.JenisNasabah,d.wilayah,d.keterkaitan,"
        cField = cField & " r.Nama, r.Alamat, r.telepon,r.fax,r.pekerjaan,r.KTP,"
        cField = cField & " a.Keterangan as KetGolonganNasabah,w.keterangan as NamaWilayah,"
        cField = cField & " b.Keterangan as KetGolonganTabungan, b.SetoranMinimum,"
        cField = cField & " b.SaldoMinimum,b.SaldoMinimumDapatBunga,"
        cField = cField & " o.Nama as NamaAO"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=d.Kode", _
                                  " Left Join GolonganNasabah a on a.Kode=d.GolonganNasabah", _
                                  " Left Join GolonganTabungan b on b.Kode=d.GolonganTabungan", _
                                  " Left Join AO o on o.Kode = d.AO", _
                                  " Left Join Wilayah w on w.kode = d.Wilayah"}
        Dim cDataTable = objData.Browse(GetDSN, "tabungan", cField, "rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                dTgl.DateTime = CDate(.Item("tgl").ToString)
                cCabang1.Text = .Item("kode").ToString.Substring(1, 2)
                cKode.Text = .Item("kode").ToString.Substring(4)
                cNama.Text = .Item("nama").ToString
                cAlamat.Text = .Item("alamat").ToString
                cTelepon.Text = .Item("telepon").ToString
                cGolonganNasabah.EditValue = .Item("golongannasabah").ToString
                cNamaGolonganNasabah.Text = .Item("KetGolonganNasabah").ToString
                SetOpt(optJenisNasabah, .Item("JenisNasabah").ToString)
                SetOpt(optKaryawan, .Item("Karyawan").ToString)
                cAO.EditValue = .Item("AO").ToString
                cNamaAO.Text = .Item("NamaAO").ToString
                For n = 0 To cmbKeterkaitan.Properties.Items.Count
                    If cmbKeterkaitan.Properties.Items(n).ToString.Substring(1, 1) = .Item("Keterkaitan").ToString Then
                        cmbKeterkaitan.EditValue = cmbKeterkaitan.Properties.Items(n).ToString
                    End If
                Next
                cWilayah.EditValue = .Item("wilayah").ToString
                cNamaWilayah.Text = .Item("NamaWilayah").ToString
                cGolonganTabungan.EditValue = .Item("GolonganTabungan").ToString
                cNamaGolonganTabungan.Text = .Item("KetGolonganTabungan").ToString
            End With
        End If
    End Sub

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cmbKeterkaitan.Properties
        With properties
            .Items.AddRange(New String() {"1 - Terkait", "2 - Tidak Terkait"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cmbKeterkaitan.SelectedIndex = 1
    End Sub

    Private Sub trOpenTabungan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("GolonganNasabah", cGolonganNasabah)
        GetDataLookup("AO", cAO, , "kode,nama")
        GetDataLookup("Wilayah", cWilayah)
        GetDataLookup("GolonganTabungan", cGolonganTabungan)
    End Sub

    Private Sub trOpenTabungan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        IsiCombo()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cGolonganNasabah.TabIndex, n)
        SetTabIndex(optJenisNasabah.TabIndex, n)
        SetTabIndex(optKaryawan.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(cmbKeterkaitan.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cGolonganTabungan.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cGolonganNasabah.EnterMoveNextControl = True
        optJenisNasabah.EnterMoveNextControl = True
        optKaryawan.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        cmbKeterkaitan.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cGolonganTabungan.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cKode.Focus()
        Koreksi(True)
        cCabang1.Enabled = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
            cKodeRegister = setKode(cCabang1.Text, cKode.Text)
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "nama,alamat,telepon", "kode", data.myOperator.Assign, cKodeRegister)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cTelepon.Text = .Item("Telepon").ToString
                End With
            Else
                MessageBox.Show("No. Register tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                cAlamat.Text = ""
                cTelepon.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cRekening_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cRekening.GotFocus
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cGolonganNasabah.Closed
        GetKeteranganLookUp(cGolonganNasabah, cNamaGolonganNasabah)
    End Sub

    Private Sub cGolonganTabungan_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cGolonganTabungan.Closed
        GetKeteranganLookUp(cGolonganTabungan, cNamaGolonganTabungan)
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cGolonganNasabah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganNasabah, cNamaGolonganNasabah)
        End If
    End Sub

    Private Sub cGolonganTabungan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cGolonganTabungan.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganTabungan, cNamaGolonganTabungan)
            If nPos = myPos.add Then
                cRekening.Text = GetFrekuensi()
            End If
        End If
    End Sub

    Private Sub cAO_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cAO.Closed
        GetKeteranganLookUp(cAO, cNamaAO, , "nama")
    End Sub

    Private Sub cAO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cAO.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cAO, cNamaAO, , "nama")
        End If
    End Sub

    Private Sub cWilayah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cWilayah.Closed
        GetKeteranganLookUp(cWilayah, cNamaWilayah)
    End Sub

    Private Sub cWilayah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cWilayah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cWilayah, cNamaWilayah)
        End If
    End Sub

    Private Sub cRekening_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            If nPos = myPos.edit Then
                GetMemory()
            ElseIf nPos = myPos.delete Then
                DeleteData()
            End If
        End If
    End Sub
End Class