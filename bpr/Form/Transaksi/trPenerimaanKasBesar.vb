﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trPenerimaanKasBesar
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dbData As New DataTable
    Dim dtTable As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        If Not lEdit Or nPos = myPos.add Then
            InitValue()
        End If
        If lEdit Then
            If nPos = myPos.add Then
                cFaktur.Enabled = False
                cKredit.Focus()
                ckFaktur.Visible = False
            Else
                cFaktur.Enabled = True
                ckFaktur.Visible = True
                cFaktur.Focus()
            End If
            GetFaktur(False)
        End If
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cFaktur.Text = ""
        cDebet.Text = ""
        cNamaDebet.Text = ""
        cKredit.EditValue = ""
        cNamaKredit.Text = ""
        cKeterangan.Text = ""
        nJumlah.Text = "0"
        nTotal.Text = "0"

        cDebet.Text = cKasTeller
        dbData = objData.Browse(GetDSN, "Rekening", , "Kode", , cDebet.Text)
        If dbData.Rows.Count > 0 Then
            cNamaDebet.Text = dbData.Rows(0).Item("Keterangan").ToString
        End If

        If cKodeKantorInduk = "01" Then
            cKredit.Text = "1.110.01
        Else
            cKredit.Text = "1.110.02"
        End If
        dbData = objData.Browse(GetDSN, "Rekening", , "Kode", , cKredit.Text)
        If dbData.Rows.Count > 0 Then
            cNamaKredit.Text = dbData.Rows(0).Item("Keterangan").ToString
        End If

        initDetail()
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            DelBukuBesar(cFaktur.Text)

            objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur.Text)

            objData.Delete(GetDSN, "Jurnal_UangPecahan", "Faktur", , cFaktur.Text)
            'UpdActivity(Me, GetActivity)
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cFaktur.Text, "Nomor faktur tidak boleh kosong. Ulangi pengisian.") Then
            Return False
            cFaktur.Focus()
            Exit Function
        End If

        If Not SeekData("Rekening", "Kode", cDebet.Text) Then
            ValidSaving = False
            MsgBox("Rekening Debet Tidak Ditemukan, Transaksi tidak bisa disimpan ...!", vbExclamation)
            Exit Function
        End If

        If Not SeekData("Rekening", "Kode", cKredit.Text) Then
            ValidSaving = False
            MsgBox("Rekening Kredit Tidak Ditemukan, Transaksi tidak bisa disimpan ...!", vbExclamation)
            Exit Function
        End If

        If CDbl(nJumlah.Text) = 0 Then
            MsgBox("Jumlah Tidak Boleh Kosong, Transaksi tidak bisa disimpan ...!", vbExclamation)
            ValidSaving = False
            Exit Function
        End If

        Dim nTotalTemp As Double = 0
        For n = 0 To dtTable.Rows.Count - 1
            nTotalTemp = nTotalTemp + CDbl(dtTable.Rows(n).Item(3))
        Next
        If CDbl(nJumlah.Text) <> nTotalTemp Then
            If MessageBox.Show("Rincian Uang Tidak Balance, Transaksi Tidak bisa Disimpan. Transaksi Tetap Dilanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbNo Then
                ValidSaving = False
                Exit Function
            End If
        End If

        If IsHoliday(dTgl.DateTime) Then
            MsgBox("Proses Tidak Boleh Dilakukan Pada Hari Libur!", vbExclamation)
            ValidSaving = False
            dTgl.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    SumTotal()
                    GetFaktur(True)
                    Dim cCabang As String = aCfg(eCfg.msKodeCabang).ToString
                    Dim vaField() As Object = {"Faktur", "Tgl", "UserName", "DateTime", "TotalDebet", "TotalKredit", "CabangEntry"}
                    Dim vaValue() As Object = {cFaktur.Text, dTgl.DateTime, cUserName, SNow(), CDbl(nJumlah.Text), CDbl(nJumlah.Text), cCabang}
                    objData.Update(GetDSN, "TOTJURNAL", String.Format("Faktur = '{0}'", cFaktur.Text), vaField, vaValue)

                    'Hapus Data Lama dalam JURNAL
                    objData.Delete(GetDSN, "JURNAL", "Faktur", , cFaktur.Text)

                    'Simpan ke JURNAL (Detail)
                    Dim cKeteranganTemp As String
                    If Trim(cKeterangan.Text) = "" Then
                        cKeteranganTemp = "Saldo Awal Kas Kecil"
                    Else
                        cKeteranganTemp = cKeterangan.Text
                    End If
                    vaField = {"Faktur", "tgl", "Rekening", "Keterangan", "Debet", "Kredit", "cabangentry"}
                    vaValue = {cFaktur.Text, dTgl.DateTime, cDebet.Text, cKeteranganTemp, CDbl(nJumlah.Text), 0, cCabang}
                    objData.Add(GetDSN, "JURNAL", vaField, vaValue)

                    vaValue = {cFaktur.Text, dTgl.DateTime, cKredit.Text, cKeteranganTemp, 0, CDbl(nJumlah.Text), cCabang}
                    objData.Add(GetDSN, "JURNAL", vaField, vaValue)

                    ' Posting Jurnal
                    UpdRekJurnal(cFaktur.Text)

                    ' Hapus Uang Pecahan
                    objData.Delete(GetDSN, "Jurnal_UangPecahan", "Faktur", , cFaktur.Text)
                    For n = 0 To dtTable.Rows.Count - 1
                        With dtTable.Rows(n)
                            If CDbl(.Item(2)) > 0 Then
                                vaField = {"Faktur", "Tgl", "Kode", "Jumlah", "Qty", "Keterangan",
                                           "Status", "cabangEntry"}
                                vaValue = {cFaktur.Text, dTgl.DateTime, .Item(4).ToString, CDbl(.Item(1)), CDbl(.Item(2)), cKeteranganTemp,
                                           "M", cCabang}
                                objData.Add(GetDSN, "Jurnal_UangPecahan", vaField, vaValue)
                            End If
                        End With
                    Next
                    'UpdActivity(Me, GetActivity)
                    MsgBox("Data sudah disimpan.", vbInformation)
                Else
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub GetFaktur(ByVal lUpdate As Boolean)
        If nPos = myPos.add Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_PenerimaanKasBesar, dTgl.DateTime, , lUpdate)
        End If
    End Sub

    Private Sub SumTotal()
        Dim n As Integer
        Dim nTemp As Double
        Dim cTemp As String

        nTotal.Text = ""
        For n = 1 To GridView1.RowCount
            cTemp = GridView1.GetRowCellValue(n, "Total").ToString
            nTemp = CDbl(nTotal.Text) + CDbl(cTemp)
            nTotal.Text = nTemp.ToString
        Next
    End Sub

    Private Sub GetMemory()
        Const cField As String = "j.*,r.Keterangan as NamaRekening"
        Dim vaJoin() As Object = {"Left Join Rekening r on r.Kode = j.Rekening"}
        Dim cDataTable = objData.Browse(GetDSN, "jurnal j", cField, "j.faktur", data.myOperator.Assign, cFaktur.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            For n As Integer = 0 To cDataTable.Rows.Count - 1
                With cDataTable.Rows(n)
                    dTgl.DateTime = CDate(.Item("tgl").ToString)
                    If CDbl(.Item("Debet")) <> 0 Then
                        cDebet.Text = .Item("rekening").ToString
                        cNamaDebet.Text = .Item("NamaRekening").ToString
                        nJumlah.Text = .Item("Debet").ToString
                        cKeterangan.Text = .Item("Keterangan").ToString
                    Else
                        cKredit.Text = .Item("Rekening").ToString
                        cNamaKredit.Text = .Item("NamaRekening").ToString
                    End If
                End With
            Next
            initDetail()
            dbData = objData.Browse(GetDSN, "Jurnal_UangPecahan", , "Faktur", , cFaktur.Text)
            For i As Integer = 0 To dbData.Rows.Count - 1
                For n = 0 To dtTable.Rows.Count - 1
                    If dtTable.Rows(n).Item(0).ToString.Substring(0, 1) = dbData.Rows(i).Item("Kode").ToString.Substring(0, 1) And CDbl(dtTable.Rows(n).Item(1)) = CDbl(dbData.Rows(i).Item("nJumlah")) Then
                        dtTable.Rows(n).Item(2) = CDbl(dbData.Rows(i).Item("Qty"))
                        dtTable.Rows(n).Item(3) = CDbl(dtTable.Rows(n).Item(1)) * CDbl(dtTable.Rows(n).Item(2))
                        nJumlah.Text = (CDbl(nJumlah.Text) + CDbl(dtTable.Rows(n).Item(3))).ToString
                    End If
                Next
            Next
            GridControl1.RefreshDataSource()
        End If
    End Sub

    Private Sub initDetail()
        nJumlah.Text = ""
        nTotal.Text = ""

        InitTable()

        dbData = objData.Browse(GetDSN, "UangPecahan", , , , , , "Status,Nominal desc")
        For n As Integer = 0 To dbData.Rows.Count - 1
            With dbData.Rows(n)
                Dim row As DataRow = dtTable.NewRow()
                row(0) = IIf(.Item("Status").ToString = "K", "Kertas", "Logam").ToString
                row(1) = CDbl(.Item("Nominal"))
                row(2) = 0
                row(3) = 0
                row(4) = (.Item("Kode")).ToString
                dtTable.Rows.Add(row)
            End With
        Next
        GridControl1.DataSource = dtTable
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Jenis")
        GridColumnFormat(GridView1, "Nominal", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far, True)
        GridColumnFormat(GridView1, "Total", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far, True)
        GridColumnFormat(GridView1, "Kode")
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Jenis", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nominal", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Jumlah", System.Type.GetType("System.Int32"))
        AddColumn(dtTable, "Total", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Kode", System.Type.GetType("System.String"))
    End Sub

    Private Sub trPenerimaanKasBesar_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("Rekening", cKredit, "and jenis ='D'")
    End Sub

    Private Sub trPenerimaanKasBesar_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cFaktur.TabIndex, n)
        SetTabIndex(cKredit.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(nJumlah.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cFaktur.EnterMoveNextControl = True
        cKredit.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True
        nJumlah.EnterMoveNextControl = True

        FormatTextBox(nJumlah, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cFaktur.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKredit_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKredit.Closed
        GetKeteranganLookUp(cKredit, cNamaKredit)
    End Sub

    Private Sub cKredit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKredit.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cKredit, cNamaKredit)
        End If
    End Sub

    Private Sub cFaktur_KeyDown(sender As Object, e As KeyEventArgs) Handles cFaktur.KeyDown
        If e.KeyCode = Keys.Enter Then
            If cFaktur.Text.Trim = "" Then
                cFaktur.Focus()
                Exit Sub
            End If
            If ckFaktur.Checked Then
                cFaktur.Text = GetLastFaktur(eFaktur.fkt_PenerimaanKasBesar, dTgl.DateTime, cFaktur.Text)
            End If
            Dim cWhere As String = String.Format("and left(faktur,2)='AA' and tgl='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            dbData = objData.Browse(GetDSN, "Jurnal_uangpecahan", , "Faktur", , cFaktur.Text, cWhere)
            If dbData.Rows.Count > 0 Then
                cFaktur.Text = dbData.Rows(0).Item("faktur").ToString
                If nPos = myPos.add Then  ' Tambah
                    MessageBox.Show("Nomor Faktur Sudah Ada, Data Tidak bisa Ditambah", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cFaktur.Focus()
                    Exit Sub
                End If
                GetMemory()
                If nPos = myPos.delete Then
                    DeleteData()
                End If
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Nomor Faktur Tidak Ada, Ulangi Pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cFaktur.Focus()
            End If
        End If
    End Sub

    Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        GridView1.PostEditor()
    End Sub

    Private Sub UpdColumns(ByVal cColumnName As String)
        Dim nSelisih As Double
        Dim nValue As Double
        Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        If cColumnName = "Jumlah" Then
            row(3) = CDbl(row(1)) * CDbl(row(2))
        Else
            nValue = Devide(CDbl(row(3)), CDbl(row(1)))
            nSelisih = nValue - Int(nValue)
            If nSelisih <> 0 Then
                MessageBox.Show("Nilai Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                row(2) = nValue
            End If
        End If
        SumTotal()
        GridControl1.RefreshDataSource()
    End Sub

    Private Sub GridView1_FocusedColumnChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles GridView1.FocusedColumnChanged
        UpdColumns(e.PrevFocusedColumn.FieldName)
    End Sub
End Class