﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils

Public Class trSimulasiKredit
    ReadOnly objData As New data()
    Dim dtData As New DataTable
    Dim dtJadwal As New DataTable
    Dim dtJadwal1 As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCaraHitung.EditValue = ""
        cKeteranganHitung.Text = ""
        nPlafond.Text = ""
        nPersBunga.Text = ""
        nPersBungaBerikutnya.Text = ""
        nLama.Text = ""
        nGracePeriodePokok.Text = ""
        nGracePeriodeBunga.Text = ""
        nPembulatan.Text = ""
    End Sub

    Private Sub HitungJadwal()
        dtJadwal = GetJadwal(dTgl.DateTime, CDbl(nPlafond.Text), CInt(nLama.Text), CSng(nPersBunga.Text), cCaraHitung.Text,
                             CDbl(nGracePeriodePokok.Text), CDbl(nGracePeriodeBunga.Text), CDbl(nPembulatan.Text), ,
                             CSng(nPersBungaBerikutnya.Text))
        dtJadwal1 = GetJadwal(dTgl.DateTime, CDbl(nPlafond.Text), CInt(nLama.Text), CSng(nPersBunga.Text), cCaraHitung.Text,
                              , , CDbl(nPembulatan.Text), , CSng(nPersBungaBerikutnya.Text))
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Ke")
        GridColumnFormat(GridView1, "JthTmp")
        GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Total", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)

        GridControl2.DataSource = dtJadwal1
        InitGrid(GridView2)
        GridColumnFormat(GridView2, "Ke")
        GridColumnFormat(GridView2, "JthTmp")
        GridColumnFormat(GridView2, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView2, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView2, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView2, "SisaBunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView2, "SisaPokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
        GridColumnFormat(GridView2, "Total", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
    End Sub

    Private Sub trSimulasiKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("carahitung", cCaraHitung)
    End Sub

    Private Sub trSimulasiKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        CenterFormManual(Me, , True)
        SetButtonPicture(, , , , , cmdKeluar, , , , , , , , , cmdRefresh)
        InitForm(Me, , , cmdKeluar)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cCaraHitung.TabIndex, n)
        SetTabIndex(nPlafond.TabIndex, n)
        SetTabIndex(nPersBunga.TabIndex, n)
        SetTabIndex(nPersBungaBerikutnya.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nGracePeriodePokok.TabIndex, n)
        SetTabIndex(nGracePeriodeBunga.TabIndex, n)
        SetTabIndex(nPembulatan.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cCaraHitung.EnterMoveNextControl = True
        nPlafond.EnterMoveNextControl = True
        nPersBunga.EnterMoveNextControl = True
        nPersBungaBerikutnya.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nGracePeriodePokok.EnterMoveNextControl = True
        nGracePeriodeBunga.EnterMoveNextControl = True
        nPembulatan.EnterMoveNextControl = True

        FormatTextBox(nPlafond, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPersBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPersBungaBerikutnya, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nGracePeriodePokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nGracePeriodeBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPembulatan, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cCaraHitung_Closed(sender As Object, e As ClosedEventArgs) Handles cCaraHitung.Closed
        GetKeteranganLookUp(cCaraHitung, cKeteranganHitung)
    End Sub

    Private Sub cCaraHitung_KeyDown(sender As Object, e As KeyEventArgs) Handles cCaraHitung.KeyDown
        If e.KeyCode=Keys.Enter Then
            GetKeteranganLookUp(cCaraHitung, cKeteranganHitung)
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        HitungJadwal()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub nGracePeriodePokok_LostFocus(sender As Object, e As EventArgs) Handles nGracePeriodePokok.LostFocus
        If Val(nGracePeriodePokok.Text) >= Val(nLama.Text) Then
            nGracePeriodePokok.Text = CStr(Val(nLama.Text) - 1)
        End If
    End Sub

    Private Sub nGracePeriodeBunga_LostFocus(sender As Object, e As EventArgs) Handles nGracePeriodeBunga.LostFocus
        If Val(nGracePeriodeBunga.Text) >= Val(nLama.Text) Then
            nGracePeriodeBunga.Text = CStr(Val(nLama.Text) - 1)
        End If
    End Sub
End Class