﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trHapusAngsuranKreditInstansiTitipan
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim dtTable As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cInstansi.EditValue = ""
        cNamaInstansi.Text = ""
        cBendahara.EditValue = ""
        cNamaBendahara.Text = ""
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Faktur", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Nama", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "TotalAngsuran", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Status", System.Type.GetType("System.String"))
    End Sub

    Private Sub GetSQL()
        Try
            Dim row As DataRow = dtTable.NewRow()
            Dim cWhere As String = ""
            Dim cField As String = String.Format("a.*,if(t.caraperhitungan<>'7', t.plafond - IFNULL((SELECT SUM(KPokok-DPokok) FROM angsuran a WHERE a.rekening = t.rekening AND (a.status = {0} or status = {1}) AND a.tgl <= '{2}'),0),", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = String.Format("{0}ifnull((select Sum(a.DPokok-a.KPokok) from angsuran a where a.rekening = t.rekening and a.tgl <= '{1}'),0)) as SaldoPokok,", cField, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cField = cField & "r.Nama,t.kode"
            If cBendahara.Text = "" Then
                cWhere = String.Format("and r.instansi ='{0}'", cInstansi.Text)
            Else
                cWhere = String.Format("and r.instansi ='{0}' and t.bendahara = '{1}'", cInstansi.Text, cBendahara.Text)
            End If
            cWhere = String.Format("{0} and t.Tgl <= '{1}' and t.statuspencairan <> 0 ", cWhere, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = cWhere & " and (t.bendahara <> '' and t.bendahara <> '999') "
            Dim vaJoin() As Object = {"Left Join Debitur t on a.rekening = t.rekening",
                                      "left join registernasabah r on t.kode=r.kode",
                                      "Left Join Bendahara b on b.kode = t.bendahara"}
            dtData = objData.Browse(GetDSN, "AngsuranTemp a", cField, "a.tgl", , formatValue(dTgl.DateTime, formatType.yyyy_MM_dd),
                                    cWhere & "", "a.Rekening", vaJoin)
            If Not dtData.Rows.Count > 0 Then
                dTglTemp = DateAdd(DateInterval.Month, 0, dTgl.DateTime)
                For n As Integer = 0 To dtData.Rows.Count - 1
                    With dtData.Rows(n)
                        row("Faktur") = .Item("Faktur").ToString
                        row("Rekening") = .Item("Rekening").ToString
                        row("Nama") = .Item("Nama").ToString
                        row("Pokok") = CDbl(.Item("Pokok"))
                        row("Bunga") = CDbl(.Item("Bunga"))
                        row("TotalAngsuran") = CDbl(.Item("Pokok")) + CDbl(.Item("Bunga"))
                        row("Status") = ""
                        dtTable.Rows.Add(row)
                    End With
                Next
            End If
            GridControl1.DataSource = dtTable
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "Rekening")
            GridColumnFormat(GridView1, "Nama")
            GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TotalAngsuran", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    DelAngsuranInstansi(nRow("Faktur").ToString)
                    GridView1.SetRowCellValue(n, "Status", "Berhasil")
                Else
                    GridView1.SetRowCellValue(n, "Status", "Gagal")
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            cInstansi.Focus()
        End If
    End Sub

    Private Sub trHapusAngsuranKreditInstansiTitipan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("Instansi", cInstansi)
        GetDataLookup("bendahara", cBendahara, , "kode,nama")
    End Sub

    Private Sub trHapusAngsuranKreditInstansiTitipan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, True, True, cmdKeluar, Windows.Forms.FormBorderStyle.Sizable)
        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar, , , , , , cmdPerRekening, , , cmdRefresh)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cInstansi.TabIndex, n)
        SetTabIndex(cBendahara.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdPerRekening.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cInstansi.EnterMoveNextControl = True
        cBendahara.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub trAngsuranInstansiTitipan_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Me.WindowState = FormWindowState.Maximized
        PanelControl1.Width = Width - 20
        GridControl1.Width = PanelControl1.Width '- 100
        PanelControl4.Width = PanelControl1.Width
        GridControl1.Height = Height - PanelControl1.Height - PanelControl4.Height - 60
        PanelControl4.Top = GridControl1.Height + PanelControl1.Height + 15
        cmdSimpan.Left = PanelControl4.Width - cmdSimpan.Width - cmdKeluar.Width - 20
        cmdKeluar.Left = PanelControl4.Width - cmdKeluar.Width - 10
    End Sub
End Class