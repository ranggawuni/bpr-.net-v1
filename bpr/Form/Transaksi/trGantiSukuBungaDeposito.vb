﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class trGantiSukuBungaDeposito
    ReadOnly objData As New data
    Dim dtData As New DataTable
    Dim dtArray As New DataTable

    Private Sub InitTable()
        AddColumn(dtArray, "dTgl", System.Type.GetType("System.Date"), True)
        AddColumn(dtArray, "nBunga", System.Type.GetType("System.Double"), True)
    End Sub

    Private Sub InitValue()
        cRekening.EditValue = ""
        dTgl.DateTime = Now.Date
        cNama.Text = ""
        cAlamat.Text = ""
        nBunga.Text = "0"
    End Sub

    Private Sub trGantiSukuBungaDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trGantiSukuBungaDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, , , cmdHapus, cmdSimpan, cmdKeluar, , , cmdOK)
        InitValue()
        InitTable()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(cmdOK.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True

        FormatTextBox(nBunga, formatType.BilRpPict2, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub GetSQL()
        Dim n As Integer
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "tgl,SukuBunga"
            Dim cWhere As String = String.Format("and tgl<='{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "detailsukubungadeposito", cField, "rekening", , cRekening.Text, cWhere, "tgl desc")
            GridControl1.DataSource = cDataSet.Tables("detailsukubungadeposito")
            cDataSet.Dispose()
            InitGrid(GridView1)
            GridColumnFormat(GridView1, "Tgl", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, 50)
            GridColumnFormat(GridView1, "SukuBunga", , , 50, DevExpress.Utils.HorzAlignment.Far)

            dtData = objData.Browse(GetDSN, "detailsukubungadeposito", cField, "rekening", , cRekening.Text, cWhere, "tgl desc")
            For n = 0 To dtData.Rows.Count - 1
                With dtData.Rows(n)
                    dtArray.Rows.Add(New Object() {.Item("tgl"), .Item("sukubunga")})
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub cRekening_Closed(sender As Object, e As ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "nominal,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("nominal")) = 0 Then
                        MessageBox.Show("Pembukaan Deposito Belum Dilakukan, Data Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        If CInt(cDataTable.Rows(0).Item("Status")) <> 0 Then
                            MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            InitValue()
                            cRekening.Focus()
                        Else
                            GetSQL()
                        End If
                        If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                            MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.Focus()
                            Exit Sub
                        End If
                        GetSQL()
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub HapusData()
        If GridView1.RowCount >= 1 Then
            If MessageBox.Show("Data Benar-benar Dihapus ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                Dim cWhere As String = String.Format("and tgl='{0}' ", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
                cWhere = String.Format("{0} and SukuBunga = {1}", cWhere, Val(nBunga.Text))
                objData.Delete(GetDSN, "detailsukubungadeposito", "rekening", , cRekening.Text, cWhere)
                GetSQL()
                dTgl.DateTime = Now.Date
                cNama.Text = "0"
                cAlamat.Text = "0"
                nBunga.Text = "0"
            End If
        End If
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        HapusData()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        dtArray.Rows.Add(New Object() {dTgl.DateTime.Date, Val(nBunga.Text)})
        cNama.Focus()
    End Sub

    Private Sub SimpanData()
        Dim n As Integer
        Dim cWhere As String = String.Format(" and Tgl = '{0}'", formatValue(dTgl.DateTime.Date, formatType.yyyy_MM_dd))
        objData.Delete(GetDSN, "DetailSukuBungaDeposito", "Rekening", , cRekening.Text, cWhere)

        Dim vaField() As Object = {"Rekening", "Tgl", "SukuBunga"}
        Dim vaValue() As Object
        For n = 0 To dtArray.Rows.Count - 1
            With dtArray.Rows(n)
                vaValue = {cRekening.Text, .Item(0), .Item(1)}
                objData.Add(GetDSN, "DetailSukuBungaDeposito", vaField, vaValue)
            End With
        Next
        InitValue()
        cRekening.Focus()
    End Sub

    Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            dTgl.DateTime = CDate(row(0).ToString)
            nBunga.Text = row(3).ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class