﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trBreakAktiva
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trBreakAktiva))
        Dim ToolTipItem5 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem6 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cNamaRekeningKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningDebet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningDebet = New DevExpress.XtraEditors.LookUpEdit()
        Me.nSisaPenyusutan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nNilaiPenyusutan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.nNilaiResidu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nHargaPerolehan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.nTarif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganAktiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganAktiva = New DevExpress.XtraEditors.LookUpEdit()
        Me.nUnit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.optJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dJual = New DevExpress.XtraEditors.DateEdit()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cNamaRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiResidu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nHargaPerolehan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTarif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJual.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 386)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(728, 33)
        Me.PanelControl2.TabIndex = 214
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(645, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem5.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem5.Appearance.Options.UseImage = True
        ToolTipTitleItem5.Image = CType(resources.GetObject("ToolTipTitleItem5.Image"), System.Drawing.Image)
        ToolTipTitleItem5.Text = "Batal / Keluar"
        ToolTipItem5.LeftIndent = 6
        ToolTipItem5.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        SuperToolTip5.Items.Add(ToolTipItem5)
        Me.cmdKeluar.SuperTip = SuperToolTip5
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(564, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem6.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem6.Appearance.Options.UseImage = True
        ToolTipTitleItem6.Image = CType(resources.GetObject("ToolTipTitleItem6.Image"), System.Drawing.Image)
        ToolTipTitleItem6.Text = "Preview Data"
        ToolTipItem6.LeftIndent = 6
        ToolTipItem6.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        SuperToolTip6.Items.Add(ToolTipItem6)
        Me.cmdSimpan.SuperTip = SuperToolTip6
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(421, 3)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(310, 377)
        Me.GridControl1.TabIndex = 213
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.ViewCaption = "Daftar Aktiva Tetap"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningKredit)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cRekeningKredit)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningDebet)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cRekeningDebet)
        Me.PanelControl1.Controls.Add(Me.nSisaPenyusutan)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.nNilaiPenyusutan)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.nNilaiResidu)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.nLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.nHargaPerolehan)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.nTarif)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.cNamaGolonganAktiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cGolonganAktiva)
        Me.PanelControl1.Controls.Add(Me.nUnit)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.optJenis)
        Me.PanelControl1.Controls.Add(Me.LabelControl21)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dJual)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(412, 377)
        Me.PanelControl1.TabIndex = 212
        '
        'cNamaRekeningKredit
        '
        Me.cNamaRekeningKredit.Enabled = False
        Me.cNamaRekeningKredit.Location = New System.Drawing.Point(155, 347)
        Me.cNamaRekeningKredit.Name = "cNamaRekeningKredit"
        Me.cNamaRekeningKredit.Size = New System.Drawing.Size(141, 20)
        Me.cNamaRekeningKredit.TabIndex = 225
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(10, 350)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl13.TabIndex = 224
        Me.LabelControl13.Text = "Rekening Kredit"
        '
        'cRekeningKredit
        '
        Me.cRekeningKredit.Location = New System.Drawing.Point(105, 347)
        Me.cRekeningKredit.Name = "cRekeningKredit"
        Me.cRekeningKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningKredit.Size = New System.Drawing.Size(44, 20)
        Me.cRekeningKredit.TabIndex = 226
        '
        'cNamaRekeningDebet
        '
        Me.cNamaRekeningDebet.Enabled = False
        Me.cNamaRekeningDebet.Location = New System.Drawing.Point(155, 321)
        Me.cNamaRekeningDebet.Name = "cNamaRekeningDebet"
        Me.cNamaRekeningDebet.Size = New System.Drawing.Size(141, 20)
        Me.cNamaRekeningDebet.TabIndex = 222
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(10, 324)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl12.TabIndex = 221
        Me.LabelControl12.Text = "Rekening Debet"
        '
        'cRekeningDebet
        '
        Me.cRekeningDebet.Location = New System.Drawing.Point(105, 321)
        Me.cRekeningDebet.Name = "cRekeningDebet"
        Me.cRekeningDebet.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningDebet.Size = New System.Drawing.Size(44, 20)
        Me.cRekeningDebet.TabIndex = 223
        '
        'nSisaPenyusutan
        '
        Me.nSisaPenyusutan.Location = New System.Drawing.Point(103, 295)
        Me.nSisaPenyusutan.Name = "nSisaPenyusutan"
        Me.nSisaPenyusutan.Size = New System.Drawing.Size(105, 20)
        Me.nSisaPenyusutan.TabIndex = 216
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(8, 299)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl9.TabIndex = 215
        Me.LabelControl9.Text = "Sisa Penyusutan"
        '
        'nNilaiPenyusutan
        '
        Me.nNilaiPenyusutan.Location = New System.Drawing.Point(103, 269)
        Me.nNilaiPenyusutan.Name = "nNilaiPenyusutan"
        Me.nNilaiPenyusutan.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiPenyusutan.TabIndex = 214
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(8, 273)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl8.TabIndex = 213
        Me.LabelControl8.Text = "Nilai Penyusutan"
        '
        'nNilaiResidu
        '
        Me.nNilaiResidu.Location = New System.Drawing.Point(103, 243)
        Me.nNilaiResidu.Name = "nNilaiResidu"
        Me.nNilaiResidu.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiResidu.TabIndex = 212
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(8, 247)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl7.TabIndex = 211
        Me.LabelControl7.Text = "Nilai Residu"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(153, 194)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl6.TabIndex = 210
        Me.LabelControl6.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.Location = New System.Drawing.Point(103, 191)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(44, 20)
        Me.nLama.TabIndex = 208
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(8, 194)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl5.TabIndex = 207
        Me.LabelControl5.Text = "Lama Penyusutan"
        '
        'nHargaPerolehan
        '
        Me.nHargaPerolehan.Location = New System.Drawing.Point(103, 217)
        Me.nHargaPerolehan.Name = "nHargaPerolehan"
        Me.nHargaPerolehan.Size = New System.Drawing.Size(105, 20)
        Me.nHargaPerolehan.TabIndex = 206
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(8, 221)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl19.TabIndex = 205
        Me.LabelControl19.Text = "Harga Perolehan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(153, 168)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl16.TabIndex = 204
        Me.LabelControl16.Text = "%"
        '
        'nTarif
        '
        Me.nTarif.Location = New System.Drawing.Point(103, 165)
        Me.nTarif.Name = "nTarif"
        Me.nTarif.Size = New System.Drawing.Size(44, 20)
        Me.nTarif.TabIndex = 203
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(8, 168)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl18.TabIndex = 202
        Me.LabelControl18.Text = "Tarif Penyusutan"
        '
        'cNamaGolonganAktiva
        '
        Me.cNamaGolonganAktiva.Enabled = False
        Me.cNamaGolonganAktiva.Location = New System.Drawing.Point(153, 139)
        Me.cNamaGolonganAktiva.Name = "cNamaGolonganAktiva"
        Me.cNamaGolonganAktiva.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganAktiva.TabIndex = 200
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 142)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 199
        Me.LabelControl2.Text = "Golongan Aktiva"
        '
        'cGolonganAktiva
        '
        Me.cGolonganAktiva.Location = New System.Drawing.Point(103, 139)
        Me.cGolonganAktiva.Name = "cGolonganAktiva"
        Me.cGolonganAktiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganAktiva.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganAktiva.TabIndex = 201
        '
        'nUnit
        '
        Me.nUnit.Location = New System.Drawing.Point(103, 113)
        Me.nUnit.Name = "nUnit"
        Me.nUnit.Size = New System.Drawing.Size(44, 20)
        Me.nUnit.TabIndex = 198
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 116)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl1.TabIndex = 197
        Me.LabelControl1.Text = "Jumlah Unit"
        '
        'optJenis
        '
        Me.optJenis.Location = New System.Drawing.Point(103, 83)
        Me.optJenis.Name = "optJenis"
        Me.optJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Garis Lurus"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Saldo Menurun")})
        Me.optJenis.Size = New System.Drawing.Size(227, 24)
        Me.optJenis.TabIndex = 196
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(8, 88)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl21.TabIndex = 195
        Me.LabelControl21.Text = "Cara Perhitungan"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 34)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(44, 13)
        Me.lblTglRegister.TabIndex = 187
        Me.lblTglRegister.Text = "Tgl Break"
        '
        'dJual
        '
        Me.dJual.EditValue = Nothing
        Me.dJual.Location = New System.Drawing.Point(103, 31)
        Me.dJual.Name = "dJual"
        Me.dJual.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJual.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJual.Size = New System.Drawing.Size(82, 20)
        Me.dJual.TabIndex = 186
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 57)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(296, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 60)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(8, 8)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(24, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(103, 5)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'trBreakAktiva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 423)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trBreakAktiva"
        Me.Text = "Break Aktiva Tetap"
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cNamaRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiResidu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nHargaPerolehan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTarif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJual.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cNamaRekeningKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningDebet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningDebet As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nSisaPenyusutan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNilaiPenyusutan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNilaiResidu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nHargaPerolehan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTarif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganAktiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganAktiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nUnit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJual As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
End Class
