﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trAngsuranKredit
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Private ReadOnly x As TypeKolektibilitas
    Dim dtJadwal As New DataTable
    Dim dbData As New DataTable
    Dim lTunggakan As Boolean
    Dim nSelisihHari As Integer
    Dim cStatusPembayaran As String
    Dim nJumlahHari As Integer
    Dim nHariTunggakan As Integer
    Dim nHari As Integer
    Dim xArray As New DataTable
    Private ReadOnly vaKolek(2) As Double
    Dim nBulanTunggakan As Integer
    Dim dTanggalPK As Date
    Dim cNoSPK As String
    Dim nPlafond As Double
    Dim cCaraPerhitungan As String
    Dim nAngsuranJadwal As Double
    Dim dJthTmpAngsuran As Date
    Dim nAngsuranKe As Integer
    Dim nAngsuranPokokLebih As Double
    Dim nAngsuranBungaLebih As Double
    Dim nPokokTemp1 As Double
    Dim nBungaTemp1 As Double
    Dim nBungaDefault As Double
    Dim nPokokDefault As Double
    Dim lClick As Boolean

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekeningLama.EditValue = ""
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nPokok.Text = ""
        nBunga.Text = ""
        nDenda.Text = ""
        nTotal.Text = ""
        nPembulatanPokok.Text = ""
        nPembulatanBunga.Text = ""
        nPotonganBunga.Text = ""
        nPotonganDenda.Text = ""
        nPotonganTotal.Text = ""
        nSisaPembayaranPokok.Text = ""
        nSisaPembayaranBunga.Text = ""
        nSisaPembayaranDenda.Text = ""
        nSisaPembayaranTotal.Text = ""
        nTotalBayar.Text = ""
        optKas.SelectedIndex = 1
        cKeterangan.Text = ""

        dJthTmp.DateTime = Date.Today
        lblJthTmp.Text = ""
        nBakiDebet.Text = ""
        nBungaPelunasan.Text = ""
        nTunggakanPokok.Text = ""
        nTunggakanBunga.Text = ""
        nKolektibilitas.Text = ""
        lblKolek.Text = ""
        cRekeningTabungan.Text = ""
        nSaldoTabungan.Text = ""
        cGolonganKredit.Text = ""
        cNamaGolonganKredit.Text = ""
    End Sub

    Private Sub trAngsuranKredit_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Dim cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
        cField = "t.rekeninglama,t.rekening,r.nama,r.alamat"
        LookupSearch("debitur t", cRekeningLama, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trAngsuranKredit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        InitTable()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cRekeningLama.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(nPokok.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(nDenda.TabIndex, n)
        SetTabIndex(nPembulatanPokok.TabIndex, n)
        SetTabIndex(nPembulatanBunga.TabIndex, n)
        SetTabIndex(nPotonganBunga.TabIndex, n)
        SetTabIndex(nPotonganDenda.TabIndex, n)
        SetTabIndex(optKas.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdKartu.TabIndex, n)
        SetTabIndex(cmdJadwal.TabIndex, n)
        SetTabIndex(cmdCekList.TabIndex, n)
        SetTabIndex(cmdDenda.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekeningLama.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nPokok.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        nDenda.EnterMoveNextControl = True
        nPembulatanPokok.EnterMoveNextControl = True
        nPembulatanBunga.EnterMoveNextControl = True
        nPotonganBunga.EnterMoveNextControl = True
        nPotonganDenda.EnterMoveNextControl = True
        optKas.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True

        FormatTextBox(nPokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nDenda, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPembulatanPokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPembulatanBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPotonganBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPotonganDenda, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPotonganTotal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSisaPembayaranPokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSisaPembayaranBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSisaPembayaranDenda, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSisaPembayaranTotal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTotalBayar, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBakiDebet, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nBungaPelunasan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTunggakanPokok, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTunggakanBunga, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nKolektibilitas, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "statuspencairan,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If cDataTable.Rows(0).Item("statuspencairan").ToString = "0" Then
                        MessageBox.Show("Rekening Kredit Belum Dicairkan, Transaksi Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        Dim nBakiDebetTemp As Double = GetBakiDebet(cRekening.Text, dTgl.DateTime)
                        If nBakiDebetTemp <= 0 Then
                            MessageBox.Show("Kredit Sudah Lunas, Transaksi Tidak dapat Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cRekening.EditValue = ""
                            cRekening.Focus()
                            Exit Sub
                        Else
                            GetMemory()
                        End If
                        If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                            MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.Focus()
                            Exit Sub
                        End If
                        GetMemory()
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetJatuhTempo()
        '  nSelisihHari = DateDiff("d", dTgl.Value, dJthTmp.Value)
        lTunggakan = False
        '  If nSelisihHari > 0 Then ' jika tidak ada tunggakan
        '    cHari.ForeColor = vbBlue '&H80000008
        '    cStatusPembayaran = "2"
        '  ElseIf nSelisihHari = 0 Then
        If nSelisihHari = 0 Then
            lblJthTmp.ForeColor = Color.Black
            cStatusPembayaran = "2"
        Else ' jika ada tunggakan
            lblJthTmp.ForeColor = Color.Red
            nSelisihHari = Math.Abs(nSelisihHari)
            cStatusPembayaran = "1"
            nJumlahHari = 0
            For n = 1 To 3
                Dim dBulan As Date = DateAdd("m", n, dJthTmp.DateTime)
                nHari = CInt(DateDiff("d", BOM(dBulan), EOM(dBulan)))
                nJumlahHari = nJumlahHari + nHari
            Next
            nHariTunggakan = nJumlahHari
            If nSelisihHari <= nJumlahHari Then
                lTunggakan = True
            End If
        End If
        lblJthTmp.Text = nSelisihHari & " Hari"
    End Sub

    Private Sub InitTable()
        AddColumn(xArray, "Faktur", System.Type.GetType("System.String"))
        AddColumn(xArray, "Nomor", System.Type.GetType("System.String"))
        AddColumn(xArray, "Tgl", System.Type.GetType("System.Date"))
        AddColumn(xArray, "Keterangan", System.Type.GetType("System.String"))
        AddColumn(xArray, "Pokok", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Bunga", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Jumlah", System.Type.GetType("System.Double"))
        AddColumn(xArray, "Denda", System.Type.GetType("System.Double"))
        AddColumn(xArray, "BakiDebet", System.Type.GetType("System.Double"))
        AddColumn(xArray, "JenisBayar", System.Type.GetType("System.String"))
        AddColumn(xArray, "ID", System.Type.GetType("System.Double"))
    End Sub

    Private Sub GetSQLAngsuran()
        Dim n As Integer
        Dim i As Integer
        Dim nRow As DataRow
        Dim nPokokTemp As Double
        Dim nBungaTemp As Double

        Const cField As String = "faktur,tgl,kpokok,dpokok,kbunga,dbunga,keterangan,status,denda,kas,id"
        Dim cWhere As String = String.Format(" and (status='{0}' or status='{1}')", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
        dbData = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening.Text, cWhere, "Tgl,id")
        If dbData.Rows.Count > 0 Then
            i = 0
            For n = 0 To dbData.Rows.Count - 1
                With dbData.Rows(n)
                    nRow = xArray.NewRow()

                    If CInt(.Item("Status")) = eAngsuran.ags_Angsuran And CDbl(.Item("KPokok")) > 0 And CDbl(.Item("KBunga")) <= 0 And cCaraPerhitungan = "7" Then
                        i = -1
                    End If

                    nRow("Faktur") = .Item("faktur").ToString
                    nRow("Nomor") = IIf(CInt(.Item("Status")) = eAngsuran.ags_Angsuran, n + 1 + i, "")
                    nRow("Tgl") = CDate(.Item("Tgl"))
                    nRow("Keterangan") = IIf(CInt(.Item("Status")) = eAngsuran.ags_Angsuran, "Angsuran", "Koreksi")
                    nPokokTemp = CDbl(.Item("KPokok"))
                    nBungaTemp = CDbl(.Item("KBunga"))
                    If CInt(.Item("Status")) = eAngsuran.ags_Koreksi Then
                        nRow("Pokok") = Math.Abs(CDbl(.Item("DPokok")))
                        nRow("Bunga") = -(CDbl(.Item("DBunga")))
                    Else
                        nRow("Pokok") = nPokokTemp
                        nRow("Bunga") = nBungaTemp
                    End If
                    nRow("Jumlah") = nPokokTemp + nBungaTemp
                    nRow("Denda") = CDbl(.Item("Denda"))
                    nPlafond = nPlafond - nPokokTemp
                    nRow("BakiDebet") = nPlafond
                    nRow("JenisBayar") = IIf(.Item("Kas").ToString = "K", "Kas", IIf(.Item("Kas").ToString = "P", "PB", "Tab")).ToString
                    nRow("ID") = CDbl(.Item("Id"))
                    xArray.Rows.Add(nRow)
                End With
            Next
        End If
        GridControl1.DataSource = xArray
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Faktur")
        GridColumnFormat(GridView1, "Nomor")
        GridColumnFormat(GridView1, "Tgl", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, , HorzAlignment.Center)
        GridColumnFormat(GridView1, "Keterangan")
        GridColumnFormat(GridView1, "Pokok", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "Denda", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "BakiDebet", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
        GridColumnFormat(GridView1, "JenisBayar")
        GridColumnFormat(GridView1, "ID", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
    End Sub

    Private Sub GetMemory()
        Dim db As New DataTable
        Dim nBungaBulanDepan As Double
        Dim nBungaBulanIni As Double
        Dim nTotPokok As Double
        Dim nTotBunga As Double
        Dim nTotDenda As Double
        Dim nPokokTemp As Double
        Dim nBungaTemp As Double
        Dim cWhere As String
        Dim n As Integer

        Dim cField As String = "d.*,r.Nama as NamaDebitur,r.Alamat as AlamatDebitur,rt.Nama as NamaNasabah,"
        cField = cField & "g.keterangan as NamaGolonganKredit"
        Dim vaJoin() As Object = {"Left Join RegisterNasabah r on d.Kode = r.Kode",
                                  "Left Join Tabungan t on d.RekeningTabungan = t.Rekening",
                                  "Left Join RegisterNasabah rt on t.Kode = rt.Kode",
                                  "Left Join GolonganKredit g on g.kode = d.GolonganKredit"}
        Dim cDataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", , cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                GetTunggakan(cRekening.Text, dTgl.DateTime, x, , , True, , .Item("Kode").ToString)
                vaKolek(0) = CDbl(x.nKolek)
                vaKolek(1) = x.nTunggakanPokok
                vaKolek(2) = x.nTunggakanBunga
                nBulanTunggakan = CInt(x.nBulanTunggakan)
                nSelisihHari = CInt(x.nTotalHariTunggakan)
                dTanggalPK = x.dTglRealisasi
                cNoSPK = .Item("NoSPK").ToString
                cNama.Text = x.cNamaDebitur
                cAlamat.Text = .Item("AlamatDebitur").ToString
                nBakiDebet.Text = GetBakiDebet(cRekening.Text, dTgl.DateTime).ToString
                nBungaPelunasan.Text = ""
                If CDbl(nBakiDebet.Text) > 0 Then
                    ' jika jenis kredit adl musiman atau kredit pegawai maka bunga bulan berikutnya tidak dihitung
                    If CDbl(.Item("GracePeriodePokok")) > 1 Or .Item("GolonganKredit").ToString = "72" Then
                        nBungaBulanDepan = 0
                    Else
                        nBungaBulanDepan = GetAngsuranBunga(cRekening.Text, DateAdd("m", 1, dTgl.DateTime))
                    End If
                    nBungaPelunasan.Text = (nBungaBulanDepan + x.nTunggakanBunga).ToString
                    If x.nTunggakanBunga = 0 Then
                        nBungaBulanIni = GetAngsuranBunga(cRekening.Text, dTgl.DateTime)
                        nBungaPelunasan.Text = (nBungaBulanIni + nBungaBulanDepan).ToString
                    End If
                End If
                nTunggakanPokok.Text = x.nTunggakanPokok.ToString
                nTunggakanBunga.Text = x.nTunggakanBunga.ToString
                nKolektibilitas.Text = x.nKolek.ToString
                nplafond = x.nPlafond
                cCaraPerhitungan = x.cCaraPerhitungan
                cRekeningTabungan.Text = x.cRekeningTabungan
                nSaldoTabungan.Text = GetSaldoTabungan(cRekeningTabungan.Text, dTgl.DateTime).ToString
                cGolonganKredit.Text = .Item("GolonganKredit").ToString
                cNamaGolonganKredit.Text = .Item("NamaGolonganKredit").ToString

                GetKewajibanPokokBunga(cRekening.Text, dTgl.DateTime, nBungaTemp, nPokokTemp, True)
                nAngsuranJadwal = nPokokTemp + nBungaTemp
                nTotPokok = 0
                nTotBunga = 0
                nTotDenda = 0

                cField = "sum(kpokok) as pokok,sum(kbunga) as bunga"
                cWhere = String.Format(" and (status = '{0}' or status='{1}')", eAngsuran.ags_Angsuran, eAngsuran.ags_Koreksi)
                dbData = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening.Text, cWhere, "Rekening,Tgl,ID")
                If dbData.Rows.Count > 0 Then
                    nTotPokok = CDbl(GetNull(dbData.Rows(0).Item("Pokok")))
                    nTotBunga = CDbl(GetNull(dbData.Rows(0).Item("Bunga")))
                End If

                dtJadwal.Reset()
                cField = "0,Tgl,DPokok,DBunga"
                cWhere = "and status=" & eAngsuran.ags_jadwal
                dbData = objData.Browse(GetDSN, "Angsuran", cField, "Rekening", , cRekening.Text, cWhere, "Tgl")
                If dbData.Rows.Count > 0 Then
                    dtJadwal = dbData.Copy
                    For n = 0 To dtJadwal.Rows.Count - 1
                        dtJadwal.Rows(n).Item(0) = n + 1
                    Next
                End If

                dJthTmpAngsuran = Date.Today
                nAngsuranKe = 0
                nAngsuranPokokLebih = 0
                nAngsuranBungaLebih = 0
                If cCaraPerhitungan = "7" Then
                    nBunga.Text = "0"
                Else
                    Dim nBaris As Single
                    nPokokTemp = nTotPokok
                    nBungaTemp = nTotBunga
                    For n = 0 To dtJadwal.Rows.Count - 1
                        nPokokTemp = nPokokTemp - CDbl(dtJadwal.Rows(n).Item(2))
                        nBungaTemp = nBungaTemp - CDbl(dtJadwal.Rows(n).Item(3))
                        If nPokokTemp < CDbl(dtJadwal.Rows(n).Item(2)) Then
                            If n = dtJadwal.Rows.Count - 1 Then
                                nBaris = dtJadwal.Rows.Count - 1
                            Else
                                nBaris = n + 1
                            End If
                            nAngsuranKe = CInt(dtJadwal.Rows(n).Item(0))
                            dJthTmpAngsuran = CDate(dtJadwal.Rows(n).Item(1))
                            nPokok.Text = dtJadwal.Rows(n).Item(2).ToString
                            nBunga.Text = dtJadwal.Rows(n).Item(3).ToString
                            Exit For
                        End If
                    Next
                End If
                nBungaTemp = CDbl(nBunga.Text)
                nPokokTemp = CDbl(nPokok.Text)
                nBungaTemp1 = CDbl(nBunga.Text)
                nPokokTemp1 = CDbl(nPokok.Text)
                nBungaDefault = CDbl(nBunga.Text)
                nPokokDefault = CDbl(nPokok.Text)

                dJthTmp.DateTime = dJthTmpAngsuran
                GetJatuhTempo()

                SumKewajiban()
                SetKeterangan()
                KeteranganKolek()
                GetSQLAngsuran()
                HitungDenda()
            End With
        End If
        db.Dispose()
    End Sub

    Private Sub SumKewajiban()
        Dim nTemp As Double = CDbl(nPotonganBunga.Text) + CDbl(nPotonganDenda.Text)
        nPotonganTotal.Text = nTemp.ToString

        ' Sisa
        nSisaPembayaranPokok.Text = nPokok.Text
        nTemp = CDbl(nBunga.Text) - CDbl(nPotonganBunga.Text)
        nSisaPembayaranBunga.Text = nTemp.ToString
        nTemp = CDbl(nDenda.Text) - CDbl(nPotonganDenda.Text)
        nSisaPembayaranDenda.Text = nTemp.ToString
        nTemp = CDbl(nTotal.Text) - CDbl(nPotonganTotal.Text)
        nSisaPembayaranTotal.Text = nTemp.ToString
    End Sub

    Private Sub SetKeterangan()
        If CDbl(nBakiDebet.Text) = CDbl(nPokok.Text) Then
            cKeterangan.Text = "Lunas"
        Else
            If cCaraPerhitungan = "7" Then
                cKeterangan.Text = "Pembayaran Pokok an. " & cNama.Text
            Else
                cKeterangan.Text = "Angsuran Kredit An. " & cNama.Text
            End If
        End If
    End Sub

    Private Sub KeteranganKolek()
        lblKolek.Text = ""
        Select Case CInt(nKolektibilitas.Text)
            Case 0
                lblKolek.Text = "Lancar Tanpa Tunggakan"
            Case 1
                lblKolek.Text = "Lancar Dengan Tunggakan"
            Case 2
                lblKolek.Text = "Kurang Lancar"
            Case 3
                lblKolek.Text = "Diragukan"
            Case 4
                lblKolek.Text = "Macet"
            Case Else
                Exit Select
        End Select
    End Sub

    Private Sub HitungDenda()
        Dim nValue As Double
        Dim dtDenda As New DataTable

        dtDenda = JadwalDenda(cRekening.Text, dTanggalPK, dTgl.DateTime, cCaraPerhitungan, nPlafond, , , nValue)
        nDenda.Text = Max(nValue, 0).ToString
        dtDenda.Dispose()
    End Sub

    Private Sub SumTotal()
        Dim nTemp As Double = Val(nPokok.Text) + Val(nBakiDebet.Text) - Val(nTunggakanPokok.Text) - Val(nBungaPelunasan.Text)
        nTunggakanBunga.Text = nTemp.ToString
    End Sub

    Private Function ValidSaving() As Boolean
        Dim db As New DataTable

        ValidSaving = True
        If Not CheckData(cRekening, "Rekening tidak lengkap. Ulangi pengisian !") Then
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        db = objData.Browse(GetDSN, "debitur", "StatusPencairan", "Rekening", , cRekening.Text)
        If db.Rows.Count > 0 Then
            If db.Rows(0).Item("StatusPencairan").ToString = "0" Then
                MessageBox.Show("Kredit Belum Dilakukan, Transaksi Tidak Bisa Dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ValidSaving = False
                cRekening.Focus()
                db.Dispose()
                Exit Function
            Else
                If CDbl(nBakiDebet.Text) <= 0 Then
                    MessageBox.Show("Kredit Sudah Lunas, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ValidSaving = False
                    cRekening.Focus()
                    db.Dispose()
                    Exit Function
                End If
            End If
        Else
            MessageBox.Show("Rekening Tidak Ditemukan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ValidSaving = False
            cRekening.Focus()
            db.Dispose()
            Exit Function
        End If

        If CDbl(nBunga.Text) + CDbl(nPokok.Text) <= 0 Then
            MessageBox.Show("Angsuran Tidak Boleh 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            nPokok.Focus()
            ValidSaving = False
            Exit Function
        End If

        If Val(nBungaPelunasan.Text) < 0 Then
            MsgBox("Pajak tidak boleh minus ( - )", vbInformation)
            nBakiDebet.Focus()
            ValidSaving = False
            Exit Function
        End If

        If CDbl(nPokok.Text) > CDbl(nBakiDebet.Text) Then
            MessageBox.Show("Pelunasan Tidak Boleh Melebihi Baki Debet, Transaksi Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ValidSaving = False
            nPokok.Focus()
            Exit Function
        End If

    End Function

    Private Sub EditDebitur()
        Dim nSisaKredit As Double = GetBakiDebet(cRekening.Text, dTgl.DateTime)
        If nSisaKredit = 0 Then
            Dim vaField() As Object = {"tgllunas"}
            Dim vaValue() As Object = {dTgl.DateTime}
            Dim cWhere As String = String.Format("rekening='{0}'", cRekening.Text)
            objData.Edit(GetDSN, "debitur", cWhere, vaField, vaValue)
        End If
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim cKodeCabang As String
        Dim cCaraPembayaran As String
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim cStatus As eAngsuran
        Dim cIPTW As String
        Dim cWhere As String = ""

        cFaktur.Text = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , True)
        If ValidSaving() Then
            If MessageBox.Show("Data Akan Disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                cFaktur.Text = GetLastFaktur(eFaktur.fkt_Angsuran, dTgl.DateTime, , True)
                cStatus = eAngsuran.ags_Angsuran

                If CDbl(nBakiDebet.Text) = CDbl(nPokok.Text) Then
                    cStatusPembayaran = "3"
                End If
                If cGolonganKredit.Text = "73" And CDbl(nTunggakanPokok.Text) = 0 And
                    CDbl(nTunggakanBunga.Text) = 0 And CDbl(nKolektibilitas.Text) <= 1 And
                    formatValue(dTgl.DateTime, formatType.yyyy_MM_dd) <= formatValue(DateSerial(dTgl.DateTime.Year, dTgl.DateTime.Month, dTanggalPK.Day + 2), formatType.yyyy_MM_dd) And
                    (CDbl(nPokok.Text) + CDbl(nBunga.Text)) > nAngsuranJadwal - 1000 Then
                    cIPTW = "1"
                Else
                    cIPTW = "0"
                End If
                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                cCaraPembayaran = GetOpt(optKas)
                UpdAngsuranPembiayaan(cKodeCabang, cFaktur.Text, dTgl.DateTime, cRekening.Text, cStatus, CDbl(nPokok.Text), CDbl(nBunga.Text),
                                      CDbl(nDenda.Text), cKeterangan.Text, , , , , , cCaraPembayaran, True, cStatusPembayaran, , , , , , ,
                                      cIPTW)

                vaField = {"cabangentry", "rekening", "tgl", "kolek", "tpokok", "tbunga"}
                vaValue = {cKodeCabang, cRekening, dTgl.DateTime, vaKolek(0), vaKolek(1), vaKolek(2)}
                cWhere = String.Format("rekening = '{0}' and tgl = '{1}'", cRekening.Text, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
                objData.Update(GetDSN, "tmpkolek", cWhere, vaField, vaValue)

                EditDebitur()
                'UpdActivity(Me, GetAcvity, Me.hDC)

                'If MessageBox.Show("Apakah Kertas Bukti Angsuran Sudah Siap ?", "konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                '    If MsgBox("Cetak Slip Baru", vbYesNo) = vbYes Then
                '        CetakSlipAngsuranBaru(cNoSPK, nTotal.Value, nPokok.Value, nBunga.Value, _
                '                          nDenda.Value, nAngsuranKe, cNama.Text, dJthTmp.Value, _
                '                          cUserName, dTgl.Value, cNamaGolonganTabungan.Text, cRekening, aCfg(msKodeCabang))
                '    Else
                '        CetakSlipAngsuran(cNoSPK, nTotal.Value, nPokok.Value, nBunga.Value, _
                '                          nDenda.Value, nAngsuranKe, cNama.Text, dJthTmp.Value, _
                '                          cUserName, dTgl.Value, cNamaGolonganTabungan.Text)
                '    End If
                'End If

                'If MessageBox.Show("Cetak Validasi Angsuran ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                '    '        CetakValidasiAngsuran cFaktur.Text
                '    If MsgBox("Cetak Slip Baru", vbYesNo) = vbYes Then
                '        CetakValidasiAngsuranDetail(cFaktur.Text, True)
                '    Else
                '        CetakValidasiAngsuranDetail cFaktur.Text
                '    End If
                'End If

                InitValue()
                MessageBox.Show("Data Tersimpan.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cRekening.Focus()
            End If
        End If
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

    Private Sub nPokok_LostFocus(sender As Object, e As EventArgs) Handles nPokok.LostFocus
        SumTotal()
    End Sub

    Private Sub nBunga_LostFocus(sender As Object, e As EventArgs) Handles nBunga.LostFocus
        SumTotal()
    End Sub

    Private Sub nDenda_LostFocus(sender As Object, e As EventArgs) Handles nDenda.LostFocus
        SumTotal()
    End Sub

    Private Sub nPembulatanBunga_LostFocus(sender As Object, e As EventArgs) Handles nPembulatanBunga.LostFocus
        SumTotal()
    End Sub

    Private Sub nPembulatanPokok_LostFocus(sender As Object, e As EventArgs) Handles nPembulatanPokok.LostFocus
        SumTotal()
    End Sub

    Private Sub optKas_Click(sender As Object, e As EventArgs) Handles optKas.Click
        GetJatuhTempo()
        ReCountAngsuran()
        SetKeterangan()
        lClick = True
    End Sub

    Private Sub ReCountAngsuran()
        If cCaraPerhitungan = "7" Then
            nBunga.Text = "0"
        Else
            nBakiDebet.Text = GetBakiDebet(cRekening.Text, dTgl.DateTime).ToString
        End If
    End Sub

End Class