﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports bpr.cls_tabungan

Public Class trMutasiTabungan
    ReadOnly objData As New data()
    Dim cDK As String = ""
    Dim cKas As String = ""
    Dim dTglTemp As Date = #1/1/1900#

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nSaldoMinimum.Text = ""
        nSetoranMinimum.Text = ""
        nBlokir.Text = ""
        cKodeTransaksi.EditValue = ""
        cKeteranganKodeTransaksi.Text = ""
        nSaldoAwal.Text = ""
        nMutasi.Text = ""
        nSaldoAkhir.Text = ""
        cKeterangan.Text = ""
    End Sub

    Private Sub trMutasiTabungan_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("tabungan t", cRekening, "t.rekening", , cField, vaJoin)
        GetDataLookup("kodetransaksi", cKodeTransaksi, , "kode,keterangan,DK,Kas,Rekening")
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTgl)
        If dTgl.Enabled Then
            dTgl.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTgl.Text = CStr(GetTglTransaksi())
        Else
            dTgl.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trMutasiTabungan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim n As Integer

        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(cKodeTransaksi.TabIndex, n)
        SetTabIndex(nMutasi.TabIndex, n)
        SetTabIndex(cKeterangan.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        cKodeTransaksi.EnterMoveNextControl = True
        nMutasi.EnterMoveNextControl = True
        cKeterangan.EnterMoveNextControl = True

        FormatTextBox(nSaldoMinimum, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSetoranMinimum, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nBlokir, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoEfektif, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoAwal, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nMutasi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoAkhir, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        cls_tabungan.cRekening = "555"
        cls_tabungan.cKode = "444"
        Dim cTemp As String = HitungSaldo()

        Console.Write(cTemp)
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "close,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "tabungan", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If cDataTable.Rows(0).Item("close").ToString = "1" Then
                        MessageBox.Show("Rekening Tabungan Sudah di Tutup, Transaksi Tidak bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                    If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                    GetMemory()
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.Text = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub cKodeTransaksi_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cKodeTransaksi.Closed
        GetKeteranganLookUp(cKodeTransaksi, cKeteranganKodeTransaksi)
    End Sub

    Private Sub cKodeTransaksi_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKodeTransaksi.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cKodeTransaksi, cKeteranganKodeTransaksi)
            cDK = GetKeteranganLookUpFunction(cKodeTransaksi, "kode", "DK")
            cKas = GetKeteranganLookUpFunction(cKodeTransaksi, "kode", "Kas")
            cKeterangan.Text = String.Format("{0} a.n {1}", cKeteranganKodeTransaksi.Text, cNama.Text)
            GetMemory()
        End If
    End Sub

    Private Sub GetMemory()
        Dim cField As String = ""
        cField = "t.rekening,t.jumlahblokir,r.Nama as NamaNasabah,r.Alamat as AlamatNasabah,g.Keterangan as NamaGolonganTabungan,"
        cField = cField & "g.SaldoMinimum,g.SetoranMinimum"
        Dim vaJoin() As Object = {"Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan", _
                                  "Left Join RegisterNasabah r on t.Kode = r.Kode"}
        Dim cDataTable = objData.Browse(GetDSN, "tabungan t", cField, "t.rekening", data.myOperator.Assign, cRekening.Text, _
                                        , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            nSaldoAwal.Text = GetSaldoTabungan(cRekening.Text, CDate(dTgl.Text)).ToString
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_MutasiTabungan, CDate(dTgl.Text))
            With cDataTable.Rows(0)
                nSaldoMinimum.Text = .Item("SaldoMinimum").ToString
                nSetoranMinimum.Text = .Item("SetoranMinimum").ToString
                nBlokir.Text = .Item("JumlahBlokir").ToString
                nSaldoEfektif.Text = GetSaldoEfektif(cRekening.Text, CDate(dTgl.Text)).ToString
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As DataTable

        ValidSaving = True

        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        cDataTable = objData.Browse(GetDSN, "tabungan", "close", "rekening", , cRekening.Text)
        If cDataTable.Rows.Count > 0 Then
            If cDataTable.Rows(0).Item("close").ToString = "1" Then
                MessageBox.Show("Rekening tabungan sudah ditutup. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        Else
            MessageBox.Show("Rekening tabungan tidak ditemukan. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        If Not CheckData(cKodeTransaksi.Text, "Kode transaksi tidak boleh kosong.") Then
            ValidSaving = False
            cKodeTransaksi.Focus()
            Exit Function
        End If

        cDataTable = objData.Browse(GetDSN, "kodetransaksi", "kode", "kode", , cKodeTransaksi.Text)
        If cDataTable.Rows.Count = 0 Then
            MessageBox.Show("Kode transaksi tidak ditemukan. Ulangi pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cKodeTransaksi.Focus()
            Exit Function
        End If

        If cDK = "D" Then
            If Double.Parse(nBlokir.Text) > 0 Then
                MessageBox.Show("Rekening sedang diblokir.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
            '1. Saldo Tidak boleh < nSaldoMinimum + nBlokir
            If Double.Parse(nSaldoEfektif.Text) - Double.Parse(nMutasi.Text) < 0 Then
                Dim cMsg As String = ""
                cMsg = "Saldo Tabungan Anda Tidak Cukup," & Chr(13)
                cMsg = String.Format("{0}Saldo Effektif : Rp. {1}{2}", cMsg, formatValue(nSaldoEfektif.Text, formatType.BilRpPict2), Chr(13))
                cMsg = String.Format("{0}Penarikan      : Rp. {1}", cMsg, formatValue(nMutasi.Text, formatType.BilRpPict2))
                MessageBox.Show(cMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                ValidSaving = False
                nMutasi.Focus()
                Exit Function
            ElseIf cDK = "K" Then
                If Double.Parse(nMutasi.Text) <= 0 Then
                    '1. Setoran Tidak boleh lebih kecil dari Setoran Minimum
                    If Double.Parse(nMutasi.Text) < Double.Parse(nSetoranMinimum.Text) Then
                        MessageBox.Show("Maaf setoran anda dibawah Setoran minimum", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        ValidSaving = True
                        nMutasi.Focus()
                        Exit Function
                    End If
                End If
            End If
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_MutasiTabungan, CDate(dTgl.Text), , True)
            If ValidSaving() Then
                Dim cKodeCabang As String = CStr(aCfg(eCfg.msKodeCabang))
                UpdMutasiTabungan(cKodeCabang, cKodeTransaksi.Text, cFaktur.Text, CDate(dTgl.Text), cRekening.Text, _
                                  CDbl(nMutasi.Text), True, cKeterangan.Text)
                MessageBox.Show("Data sudah disimpan.", "Notifikasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cRekening.SelectedText = ""
                cRekening.EditValue = ""
                InitValue()
                cRekening.Focus()
                'cRekening.ClosePopup()
            End If
        End If
    End Sub

    Private Sub dTgl_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dTgl.LostFocus
        If Not checkTglTransaksi(CDate(dTgl.Text)) Then
            dTgl.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTgl.Text)
    End Sub

    Private Sub nMutasi_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles nMutasi.KeyDown
        If e.KeyCode = Keys.Enter Then
            If cDK = "D" Then
                ' saldo tidak boleh < saldo minimum + jumlah blokir
                Dim nSelisih As Double = Double.Parse(nSaldoEfektif.Text) - Double.Parse(nMutasi.Text)
                If nSelisih < 0 Then
                    Dim cMsg As String = "Saldo tabungan cukup," & Chr(13)
                    cMsg = String.Format("{0}Saldo Efektif : Rp. {1}{2}", cMsg, formatValue(nSaldoEfektif.Text, formatType.BilRpPict2), Chr(13))
                    cMsg = String.Format("{0}Penarikan     : Rp. {1}", cMsg, formatValue(nMutasi.Text, formatType.BilRpPict2))
                    MessageBox.Show(cMsg, "Erorr Mutasi", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    nMutasi.Focus()
                Else
                    nSelisih = Double.Parse(nSaldoAwal.Text) - Double.Parse(nMutasi.Text)
                    nSaldoAkhir.Text = nSelisih.ToString
                End If
            ElseIf cDK = "K" Then
                If Double.Parse(nMutasi.Text) > 0 Then
                    ' setoran tidak boleh kecil dari setoran minimum
                    If Double.Parse(nMutasi.Text) < Double.Parse(nSetoranMinimum.Text) Then
                        MessageBox.Show("Setoran tidak boleh dibawah setoran minimum.", "Error Mutasi", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        nMutasi.Focus()
                    End If
                End If
                Dim nSaldoTemp As Double = Double.Parse(nSaldoAwal.Text) + Double.Parse(nMutasi.Text)
                nSaldoAkhir.Text = nSaldoTemp.ToString
            End If
        End If
    End Sub

    Private Sub cRekening_EditValueChanged(sender As Object, e As EventArgs) Handles cRekening.EditValueChanged

    End Sub
End Class