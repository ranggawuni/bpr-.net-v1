﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trKoreksiPerpanjanganDepositoPerNasabah
    ReadOnly objData As New data()
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim nNominal As Double = 0
    ReadOnly x As TypeDeposito

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Dim dTanggal As Date = DateAdd("m", -6, dTgl.DateTime)
            Const cField As String = "m.faktur,m.tgl,m.jthtmp,m.StatusPajak"
            Dim cWhere As String = String.Format(" and tgl >= '{0}'", formatValue(dTanggal, formatType.yyyy_MM_dd))
            cWhere = String.Format("{0} and kas ='E'", cWhere)
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "MutasiDeposito", cField, "rekening", , cRekening.Text, cWhere, "tgl,id")
            GridControl1.DataSource = cDataSet.Tables("MutasiDeposito")
            cDataSet.Dispose()
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "Tgl", , , , , True)
            GridColumnFormat(GridView1, "JthTmp", , , , , True)
            GridColumnFormat(GridView1, "StatusPajak", , , , , True)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    cWhere = String.Format("rekening = '{0}' and faktur = {1}", cRekening.Text, nRow(0).ToString)
                    If nRow(0).ToString <> "" Then
                        vaField = {"Tgl", "JthTmp", "StatusPajak"}
                        vaValue = {formatValue(nRow(1), formatType.yyyy_MM_dd), formatValue(nRow(2), formatType.yyyy_MM_dd), nRow(3).ToString.ToUpper}
                        objData.Edit(GetDSN, "MutasiDeposito", cWhere, vaField, vaValue)
                    End If
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trKoreksiPerpanjanganDepositoPerNasabah_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trKoreksiPerpanjanganDepositoPerNasabah_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "Rekening,Nominal"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    GetDeposito(cRekening.Text, dTgl.DateTime, x)
                    nNominal = Val(.Item("Nominal"))
                    cNama.Text = x.cNama
                    cAlamat.Text = x.cAlamat
                    GetSQL()
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub
End Class