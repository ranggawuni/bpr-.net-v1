﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trPembelianAktiva
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trPembelianAktiva))
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdHitungJadwal = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHitung = New DevExpress.XtraEditors.SimpleButton()
        Me.nSaran = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.nSisaKonversi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaRekeningAkumulasiPenyusutan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningAkumulasiPenyusutan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaRekeningBiayaPenyusutan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningBiayaPenyusutan = New DevExpress.XtraEditors.LookUpEdit()
        Me.nJumlahKonversi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglKonversi = New DevExpress.XtraEditors.DateEdit()
        Me.nPenyusutanPerBulan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nNilaiPenyusutan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.nNilaiResidu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.dAkhir = New DevExpress.XtraEditors.DateEdit()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.nHargaPerolehan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.nTarif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganAktiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganAktiva = New DevExpress.XtraEditors.LookUpEdit()
        Me.nUnit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.optJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.nBerjalan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTanggal = New DevExpress.XtraEditors.DateEdit()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.nSaran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSisaKonversi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningAkumulasiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningAkumulasiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaRekeningBiayaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningBiayaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nJumlahKonversi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglKonversi.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglKonversi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPenyusutanPerBulan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiResidu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAkhir.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dAkhir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nHargaPerolehan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTarif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBerjalan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTanggal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTanggal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdHitungJadwal)
        Me.PanelControl1.Controls.Add(Me.cmdHitung)
        Me.PanelControl1.Controls.Add(Me.nSaran)
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.nSisaKonversi)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningAkumulasiPenyusutan)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.cRekeningAkumulasiPenyusutan)
        Me.PanelControl1.Controls.Add(Me.cNamaRekeningBiayaPenyusutan)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cRekeningBiayaPenyusutan)
        Me.PanelControl1.Controls.Add(Me.nJumlahKonversi)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.dTglKonversi)
        Me.PanelControl1.Controls.Add(Me.nPenyusutanPerBulan)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.nNilaiPenyusutan)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.nNilaiResidu)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.dAkhir)
        Me.PanelControl1.Controls.Add(Me.nLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.nHargaPerolehan)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.nTarif)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.cNamaGolonganAktiva)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cGolonganAktiva)
        Me.PanelControl1.Controls.Add(Me.nUnit)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.optJenis)
        Me.PanelControl1.Controls.Add(Me.LabelControl21)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.nBerjalan)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTanggal)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(412, 435)
        Me.PanelControl1.TabIndex = 17
        '
        'cmdHitungJadwal
        '
        Me.cmdHitungJadwal.Location = New System.Drawing.Point(224, 295)
        Me.cmdHitungJadwal.Name = "cmdHitungJadwal"
        Me.cmdHitungJadwal.Size = New System.Drawing.Size(117, 24)
        Me.cmdHitungJadwal.TabIndex = 233
        Me.cmdHitungJadwal.Text = "Hitung &Ulang Jadwal"
        '
        'cmdHitung
        '
        Me.cmdHitung.Location = New System.Drawing.Point(224, 242)
        Me.cmdHitung.Name = "cmdHitung"
        Me.cmdHitung.Size = New System.Drawing.Size(117, 24)
        Me.cmdHitung.TabIndex = 232
        Me.cmdHitung.Text = "&Hitung Jadwal"
        '
        'nSaran
        '
        Me.nSaran.Location = New System.Drawing.Point(323, 348)
        Me.nSaran.Name = "nSaran"
        Me.nSaran.Size = New System.Drawing.Size(76, 20)
        Me.nSaran.TabIndex = 231
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(222, 351)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl20.TabIndex = 230
        Me.LabelControl20.Text = "Saran Peny. Per Bln"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(362, 325)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl14.TabIndex = 229
        Me.LabelControl14.Text = "Bulan"
        '
        'nSisaKonversi
        '
        Me.nSisaKonversi.Location = New System.Drawing.Point(323, 322)
        Me.nSisaKonversi.Name = "nSisaKonversi"
        Me.nSisaKonversi.Size = New System.Drawing.Size(33, 20)
        Me.nSisaKonversi.TabIndex = 228
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(222, 325)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl15.TabIndex = 227
        Me.LabelControl15.Text = "Sisa Konversi"
        '
        'cNamaRekeningAkumulasiPenyusutan
        '
        Me.cNamaRekeningAkumulasiPenyusutan.Enabled = False
        Me.cNamaRekeningAkumulasiPenyusutan.Location = New System.Drawing.Point(153, 399)
        Me.cNamaRekeningAkumulasiPenyusutan.Name = "cNamaRekeningAkumulasiPenyusutan"
        Me.cNamaRekeningAkumulasiPenyusutan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaRekeningAkumulasiPenyusutan.TabIndex = 225
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(8, 402)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl13.TabIndex = 224
        Me.LabelControl13.Text = "Rek. Akm. Peny."
        '
        'cRekeningAkumulasiPenyusutan
        '
        Me.cRekeningAkumulasiPenyusutan.Location = New System.Drawing.Point(103, 399)
        Me.cRekeningAkumulasiPenyusutan.Name = "cRekeningAkumulasiPenyusutan"
        Me.cRekeningAkumulasiPenyusutan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningAkumulasiPenyusutan.Size = New System.Drawing.Size(44, 20)
        Me.cRekeningAkumulasiPenyusutan.TabIndex = 226
        '
        'cNamaRekeningBiayaPenyusutan
        '
        Me.cNamaRekeningBiayaPenyusutan.Enabled = False
        Me.cNamaRekeningBiayaPenyusutan.Location = New System.Drawing.Point(153, 373)
        Me.cNamaRekeningBiayaPenyusutan.Name = "cNamaRekeningBiayaPenyusutan"
        Me.cNamaRekeningBiayaPenyusutan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaRekeningBiayaPenyusutan.TabIndex = 222
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 376)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl12.TabIndex = 221
        Me.LabelControl12.Text = "Rek. Biaya Peny."
        '
        'cRekeningBiayaPenyusutan
        '
        Me.cRekeningBiayaPenyusutan.Location = New System.Drawing.Point(103, 373)
        Me.cRekeningBiayaPenyusutan.Name = "cRekeningBiayaPenyusutan"
        Me.cRekeningBiayaPenyusutan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningBiayaPenyusutan.Size = New System.Drawing.Size(44, 20)
        Me.cRekeningBiayaPenyusutan.TabIndex = 223
        '
        'nJumlahKonversi
        '
        Me.nJumlahKonversi.Location = New System.Drawing.Point(103, 347)
        Me.nJumlahKonversi.Name = "nJumlahKonversi"
        Me.nJumlahKonversi.Size = New System.Drawing.Size(105, 20)
        Me.nJumlahKonversi.TabIndex = 220
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(8, 351)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl11.TabIndex = 219
        Me.LabelControl11.Text = "Jumlah Konversi"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(8, 324)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl10.TabIndex = 218
        Me.LabelControl10.Text = "Tgl Konversi"
        '
        'dTglKonversi
        '
        Me.dTglKonversi.EditValue = Nothing
        Me.dTglKonversi.Location = New System.Drawing.Point(103, 321)
        Me.dTglKonversi.Name = "dTglKonversi"
        Me.dTglKonversi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglKonversi.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglKonversi.Size = New System.Drawing.Size(82, 20)
        Me.dTglKonversi.TabIndex = 217
        '
        'nPenyusutanPerBulan
        '
        Me.nPenyusutanPerBulan.Location = New System.Drawing.Point(103, 295)
        Me.nPenyusutanPerBulan.Name = "nPenyusutanPerBulan"
        Me.nPenyusutanPerBulan.Size = New System.Drawing.Size(105, 20)
        Me.nPenyusutanPerBulan.TabIndex = 216
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(8, 299)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl9.TabIndex = 215
        Me.LabelControl9.Text = "Penyusutan / Bulan"
        '
        'nNilaiPenyusutan
        '
        Me.nNilaiPenyusutan.Location = New System.Drawing.Point(103, 269)
        Me.nNilaiPenyusutan.Name = "nNilaiPenyusutan"
        Me.nNilaiPenyusutan.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiPenyusutan.TabIndex = 214
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(8, 273)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl8.TabIndex = 213
        Me.LabelControl8.Text = "Nilai Yg Disusutkan"
        '
        'nNilaiResidu
        '
        Me.nNilaiResidu.Location = New System.Drawing.Point(103, 243)
        Me.nNilaiResidu.Name = "nNilaiResidu"
        Me.nNilaiResidu.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiResidu.TabIndex = 212
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(8, 247)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl7.TabIndex = 211
        Me.LabelControl7.Text = "Nilai Akhir"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(153, 220)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl6.TabIndex = 210
        Me.LabelControl6.Text = "Bulan - Akhir Penyusutan"
        '
        'dAkhir
        '
        Me.dAkhir.EditValue = Nothing
        Me.dAkhir.Location = New System.Drawing.Point(279, 217)
        Me.dAkhir.Name = "dAkhir"
        Me.dAkhir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dAkhir.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dAkhir.Size = New System.Drawing.Size(82, 20)
        Me.dAkhir.TabIndex = 209
        '
        'nLama
        '
        Me.nLama.Location = New System.Drawing.Point(103, 217)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(44, 20)
        Me.nLama.TabIndex = 208
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(8, 220)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl5.TabIndex = 207
        Me.LabelControl5.Text = "Lama Penyusutan"
        '
        'nHargaPerolehan
        '
        Me.nHargaPerolehan.Location = New System.Drawing.Point(103, 191)
        Me.nHargaPerolehan.Name = "nHargaPerolehan"
        Me.nHargaPerolehan.Size = New System.Drawing.Size(105, 20)
        Me.nHargaPerolehan.TabIndex = 206
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(8, 195)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl19.TabIndex = 205
        Me.LabelControl19.Text = "Harga Perolehan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(153, 168)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl16.TabIndex = 204
        Me.LabelControl16.Text = "%"
        '
        'nTarif
        '
        Me.nTarif.Location = New System.Drawing.Point(103, 165)
        Me.nTarif.Name = "nTarif"
        Me.nTarif.Size = New System.Drawing.Size(44, 20)
        Me.nTarif.TabIndex = 203
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(8, 168)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl18.TabIndex = 202
        Me.LabelControl18.Text = "Tarif Penyusutan"
        '
        'cNamaGolonganAktiva
        '
        Me.cNamaGolonganAktiva.Enabled = False
        Me.cNamaGolonganAktiva.Location = New System.Drawing.Point(153, 139)
        Me.cNamaGolonganAktiva.Name = "cNamaGolonganAktiva"
        Me.cNamaGolonganAktiva.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganAktiva.TabIndex = 200
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 142)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 199
        Me.LabelControl2.Text = "Golongan Aktiva"
        '
        'cGolonganAktiva
        '
        Me.cGolonganAktiva.Location = New System.Drawing.Point(103, 139)
        Me.cGolonganAktiva.Name = "cGolonganAktiva"
        Me.cGolonganAktiva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganAktiva.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganAktiva.TabIndex = 201
        '
        'nUnit
        '
        Me.nUnit.Location = New System.Drawing.Point(103, 113)
        Me.nUnit.Name = "nUnit"
        Me.nUnit.Size = New System.Drawing.Size(44, 20)
        Me.nUnit.TabIndex = 198
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 116)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl1.TabIndex = 197
        Me.LabelControl1.Text = "Jumlah Unit"
        '
        'optJenis
        '
        Me.optJenis.Location = New System.Drawing.Point(103, 83)
        Me.optJenis.Name = "optJenis"
        Me.optJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Garis Lurus"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Saldo Menurun")})
        Me.optJenis.Size = New System.Drawing.Size(227, 24)
        Me.optJenis.TabIndex = 196
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(8, 88)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl21.TabIndex = 195
        Me.LabelControl21.Text = "Cara Perhitungan"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(347, 60)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl4.TabIndex = 194
        Me.LabelControl4.Text = "Bulan"
        '
        'nBerjalan
        '
        Me.nBerjalan.Location = New System.Drawing.Point(297, 57)
        Me.nBerjalan.Name = "nBerjalan"
        Me.nBerjalan.Size = New System.Drawing.Size(44, 20)
        Me.nBerjalan.TabIndex = 193
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(191, 60)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(100, 13)
        Me.LabelControl3.TabIndex = 192
        Me.LabelControl3.Text = "s/d skrg sdh berjalan"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 60)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(65, 13)
        Me.lblTglRegister.TabIndex = 187
        Me.lblTglRegister.Text = "Tgl Perolehan"
        '
        'dTanggal
        '
        Me.dTanggal.EditValue = Nothing
        Me.dTanggal.Location = New System.Drawing.Point(103, 57)
        Me.dTanggal.Name = "dTanggal"
        Me.dTanggal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTanggal.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTanggal.Size = New System.Drawing.Size(82, 20)
        Me.dTanggal.TabIndex = 186
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 31)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(296, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 34)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(8, 8)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(24, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "Kode"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(103, 5)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(421, 2)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(310, 435)
        Me.GridControl1.TabIndex = 206
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.ViewCaption = "Daftar Aktiva Tetap"
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(737, 2)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(294, 435)
        Me.GridControl2.TabIndex = 207
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.ViewCaption = "Jadwal Penyusutan"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 443)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1028, 33)
        Me.PanelControl2.TabIndex = 208
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(948, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Batal / Keluar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.cmdKeluar.SuperTip = SuperToolTip3
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(867, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem4.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem4.Appearance.Options.UseImage = True
        ToolTipTitleItem4.Image = CType(resources.GetObject("ToolTipTitleItem4.Image"), System.Drawing.Image)
        ToolTipTitleItem4.Text = "Preview Data"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.cmdSimpan.SuperTip = SuperToolTip4
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'trPembelianAktiva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1033, 479)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.GridControl2)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trPembelianAktiva"
        Me.Text = "Pembelian Aktiva Tetap"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.nSaran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSisaKonversi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningAkumulasiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningAkumulasiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaRekeningBiayaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningBiayaPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nJumlahKonversi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglKonversi.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglKonversi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPenyusutanPerBulan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiPenyusutan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiResidu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAkhir.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dAkhir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nHargaPerolehan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTarif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganAktiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBerjalan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTanggal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTanggal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTanggal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBerjalan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nUnit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganAktiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganAktiva As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTarif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaRekeningAkumulasiPenyusutan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningAkumulasiPenyusutan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaRekeningBiayaPenyusutan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningBiayaPenyusutan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nJumlahKonversi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglKonversi As DevExpress.XtraEditors.DateEdit
    Friend WithEvents nPenyusutanPerBulan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNilaiPenyusutan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNilaiResidu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dAkhir As DevExpress.XtraEditors.DateEdit
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nHargaPerolehan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents nSaran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSisaKonversi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHitungJadwal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHitung As DevExpress.XtraEditors.SimpleButton
End Class
