﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class trRegistrasiDeposito
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim cKodeRegister As String
    Private ReadOnly dTglTemp As Date = #1/1/1900#
    Dim dtJadwal As New DataTable
    Dim lNoDelete As Boolean

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        cWilayah.EditValue = aCfg(eCfg.msKodeDefaultWilayah)
        If nPos = myPos.add Then
            If dTglTemp = #1/1/1900# Then
                dTgl.DateTime = Date.Today
            Else
                dTgl.DateTime = dTglTemp
            End If
        End If
    End Sub

    Private Sub Koreksi(ByVal lKoreksi As Boolean)
        cCabang1.Enabled = lKoreksi
        cRekening.Enabled = lKoreksi
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cCabang1.Text = CStr(aCfg(eCfg.msKodeCabang))
        cKode.Text = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cTelepon.Text = ""
        cRekening.EditValue = ""
        dTgl.DateTime = Now.Date
        cGolonganDeposan.EditValue = ""
        cNamaGolonganDeposan.Text = ""
        cGolonganDeposito.EditValue = ""
        cNamaGolonganDeposito.Text = ""
        cWilayah.EditValue = ""
        cNamaWilayah.Text = ""
        cAO.EditValue = ""
        cNamaAO.Text = ""
        cmbKeterkaitan.EditValue = ""
        nLama.Text = ""
        dJthTmp.DateTime = Now.Date
        optSetoran.SelectedIndex = 0
        optPencairan.SelectedIndex = 0
        optPencairanBunga.SelectedIndex = 0
        optDeposan.SelectedIndex = 0
        cbJenisDeposito.EditValue = ""
        nBunga.Text = ""
        cNoBilyet.Text = ""
        cNasabah.EditValue = ""
        cKeteranganNasabah.Text = ""
        optPajak.SelectedIndex = 0
        cRekeningTabungan.EditValue = ""
        cNamaNasabah.Text = ""
        cAlamatNasabah.Text = ""
        nSaldoTabungan.Text = ""
    End Sub

    Private Sub DeleteData()
        Dim db As New DataTable
        If MessageBox.Show("Data Benar-benar akan Dihapus", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            db = objData.Browse(GetDSN, "mutasideposito", "rekening,setoranplafond", "rekening", , cRekening.Text)
            If db.Rows.Count > 0 Then
                MsgBox("Rekening Tidak Bisa Dihapus." & vbCrLf & "Nasabah Masih Memiliki Data Mutasi.", vbExclamation)
                lNoDelete = True
            Else
                objData.Delete(GetDSN, "Deposito", "Rekening", , cRekening.Text)
                objData.Delete(GetDSN, "jadwalbungadeposito", "rekening", , cRekening.Text)
                lNoDelete = False
                InitValue()
                MessageBox.Show("Data Sudah Dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            GetEdit(False)
            InitValue()
        End If
        db.Dispose()
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As DataTable
        ValidSaving = True

        If Len(cKode.Text) < 6 Then
            MessageBox.Show("Kode Register Tidak Lengkap. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganDeposito.Text, "Golongan Deposito harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganDeposito.Focus()
            Exit Function
        End If

        If Not CheckData(cGolonganDeposan.Text, "Golongan Deposan harus diisi. Ulangi pengisian.") Then
            Return False
            cGolonganDeposito.Focus()
            Exit Function
        End If

        If Len(cRekening.Text) < 10 Then
            Return False
            cRekening.Focus()
            Exit Function
        End If

        If nPos = myPos.add Then
            cDataTable = objData.Browse(GetDSN, "Deposito", "rekening", "rekening", data.myOperator.Assign, cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                MessageBox.Show("Rekening Deposito sudah digunakan. Ulangi pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
                cRekening.Focus()
                Exit Function
            End If
        End If
    End Function

    Private Sub InitTable()
        Dim dtLabel1 As DataTable = New DataTable
        AddColumn(dtLabel1, "Ke", System.Type.GetType("System.Int32"), True)
        AddColumn(dtLabel1, "TglValuta", System.Type.GetType("System.Date"), True)
        AddColumn(dtLabel1, "JthTmp", System.Type.GetType("System.Date"), True)
        AddColumn(dtLabel1, "Keterangan", System.Type.GetType("System.String"), True)
    End Sub

    Private Sub HitungJadwal()
        Dim dTemp As Date
        Dim dValuta As Date
        Dim dJatuhTempo1 As Date
        Dim n As Double
        Dim cKeteranganTemp As String = ""

        InitTable()
        For n = 1 To Val(nLama.Text)
            If n > 1 Then
                dTemp = dJatuhTempo1
            Else
                dTemp = dTgl.DateTime
            End If
            dValuta = DateAdd("m", 0, dTemp)
            dJatuhTempo1 = DateAdd("m", 1, dValuta)
            cKeteranganTemp = GetLocalDate(dValuta, False)
            dtJadwal.Rows.Add(New Object() {n, dValuta, dJatuhTempo1, cKeteranganTemp})
        Next
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Ke")
        GridColumnFormat(GridView1, "TglValuta")
        GridColumnFormat(GridView1, "JthTmp")
        GridColumnFormat(GridView1, "Keterangan")
    End Sub

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cNoRegister As String = String.Format("{0}.{1}", cCabang1.Text, cKode.Text)
                    If nPos = myPos.edit Then
                        lEdit = True
                    Else
                        lEdit = False
                        If nPos = myPos.add And (dtJadwal.Rows(0).Item(1).ToString = "" Or dtJadwal.Rows(0).Item(1).ToString = Nothing) Then
                            HitungJadwal()
                        End If
                    End If
                    Dim vaField() As Object = Nothing
                    Dim vaValue() As Object = Nothing
                    Dim cStatusPajak As String = GetOpt(optPajak)
                    Dim cStatusJenisDeposan As String = GetOpt(optDeposan)
                    Dim cSistemSetoran As String = GetOpt(optSetoran)
                    Dim cSistemPencairan As String = GetOpt(optPencairan)
                    Dim cSistemPencairanBunga As String = GetOpt(optPencairanBunga)
                    Dim cJenisKeterkaitan As String = cmbKeterkaitan.Text.Substring(0, 1)
                    Dim cJenisDeposito As String = cbJenisDeposito.Text.Substring(0, 1)
                    UpdPembukaanDeposito(cRekening.Text, dTgl.DateTime, dJthTmp.DateTime, cNoRegister, _
                                         cGolonganDeposan.Text, cGolonganDeposito.Text, cAO.Text, cJenisDeposito, _
                                         CType(Format(DateTime.Now, "yyyy-mm-dd hh:mm:ss"), DateTime), cSistemPencairan, cSistemPencairanBunga, _
                                         cSistemSetoran, Val(nBunga.Text), cRekeningTabungan.Text, cStatusPajak, cStatusJenisDeposan _
                                         , cJenisKeterkaitan, cWilayah.Text, cNoBilyet.Text, cStatusJenisDeposan, _
                                         Val(nLama.Text), lEdit, cNasabah.Text)

                    For n = 0 To dtJadwal.Rows.Count - 1
                        With dtJadwal.Rows(n)
                            Dim cWhere As String = " and bulanke = " & CInt(.Item("Ke").ToString)
                            cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(.Item("TglValuta").ToString, formatType.yyyy_MM_dd))
                            cWhere = String.Format("{0} and date_format(jthtmp,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(.Item("JthTmp").ToString, formatType.yyyy_MM_dd))
                            cWhere = cWhere & " and status=0"
                            objData.Delete(GetDSN, "jadwalbungadeposito", "rekening", , cRekening.Text, cWhere)

                            vaField = {"cabangentry", "rekening", "bulanke", "tgl", _
                                       "jthtmp", "status", "tglpembukaan", "datetime", "sukubunga"}
                            vaValue = {cCabang1.Text, cRekening, CInt(.Item("Ke")), CDate(.Item("TglValuta")), _
                                       CDate(.Item("JthTmp")), 0, CDate(dTgl.Text), SNow(), CInt(nBunga.Text)}
                            objData.Add(GetDSN, "jadwalbungadeposito", vaField, vaValue)
                        End With
                    Next
                Else
                    cRekening.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Function GetFrekuensi() As String
        Dim cTemp As String = ""
        Dim nFrekuensi As Integer = 1
        Dim cCabangTemp As String
        If Len(cRekening.Text) = 10 Then
            cCabangTemp = cRekening.Text.Substring(1, 3)
        Else
            cCabangTemp = CStr(aCfg(eCfg.msKodeCabang))
        End If
        Dim cDataTable = objData.Browse(GetDSN, "Deposito", "max(rekening) as kodeKantor", "left(rekening,2)", data.myOperator.Assign, cCabangTemp)
        With cDataTable
            If .Rows.Count > 0 Then
                cTemp = .Rows(0).Item("kodeKantor").ToString.Substring(4)
                nFrekuensi = CInt(cTemp) + 1
            End If
        End With
        Dim cKodeCabangTemp As String = CStr(aCfg(eCfg.msKodeCabang))
        cTemp = String.Format("{0}1{1}", Padl(cKodeCabangTemp, 3, "0"), Padl(CStr(nFrekuensi), 6, "0"))
        Return cTemp
    End Function

    Private Sub GetMemory()
        Dim cFields As String = " d.*,r. Nama, r.Alamat, r.telepon,"
        cFields = cFields & " a.Keterangan as NamaGolonganDeposan,"
        cFields = cFields & " b.Keterangan as NamaGolonganDeposito,"
        cFields = cFields & " t.Rekening,e.Nama as NamaNasabah,e.Alamat as AlamatNasabah,"
        cFields = cFields & " w.keterangan as NamaWilayah,o.nama as NamaAO,c.keterangan as NamaKantor"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=d.Kode", _
                                  " Left Join GolonganDeposan a on a.Kode=d.GolonganDeposan", _
                                  " Left Join GolonganDeposito b on b.Kode=d.GolonganDeposito", _
                                  " Left Join Tabungan t on t.Rekening=d.RekeningTabungan", _
                                  " Left Join RegisterNasabah e on e.Kode=t.Kode", _
                                  " Left Join AO o on o.Kode = d.AO", _
                                  " Left Join Wilayah w on w.kode = d.Wilayah", _
                                  " Left Join cabang c on c.kode = d.nasabah"}
        Dim cDataTable = objData.Browse(GetDSN, "Deposito d", cFields, "d.rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cCabang1.Text = .Item("kode").ToString.Substring(1, 2)
                cKode.Text = .Item("kode").ToString.Substring(4)
                cNama.Text = .Item("nama").ToString
                cAlamat.Text = .Item("alamat").ToString
                cTelepon.Text = .Item("telepon").ToString
                dTgl.DateTime = CDate(.Item("tgl").ToString)
                cGolonganDeposito.Text = .Item("GolonganDeposan").ToString
                cNamaGolonganDeposan.Text = .Item("NamaGolonganDeposan").ToString
                cGolonganDeposito.EditValue = .Item("GolonganDeposito").ToString
                cNamaGolonganDeposito.Text = .Item("NamaGolonganDeposito").ToString
                cWilayah.EditValue = .Item("wilayah").ToString
                cNamaWilayah.Text = .Item("NamaWilayah").ToString
                cAO.EditValue = .Item("AO").ToString
                cNamaAO.Text = .Item("NamaAO").ToString
                For n = 0 To cmbKeterkaitan.Properties.Items.Count
                    If cmbKeterkaitan.Properties.Items(n).ToString.Substring(1, 1) = .Item("Keterkaitan").ToString Then
                        cmbKeterkaitan.EditValue = cmbKeterkaitan.Properties.Items(n).ToString
                    End If
                Next
                For n = 0 To cbJenisDeposito.Properties.Items.Count - 1
                    If cbJenisDeposito.Properties.Items(n).ToString = .Item("ARO").ToString Then
                        cbJenisDeposito.Text = cbJenisDeposito.Properties.Items(n).ToString
                    End If
                Next
                nLama.Text = .Item("Lama").ToString
                dJthTmp.Text = GetJTHTMPDeposito(dTgl.DateTime, Val(nLama.Text), dTgl.DateTime, "T").ToString
                SetOpt(optSetoran, .Item("AsalSetoran").ToString)
                SetOpt(optPencairan, .Item("TujuanPencairanPokok").ToString)
                SetOpt(optPencairanBunga, .Item("TujuanPencairanBunga").ToString)
                SetOpt(optDeposan, .Item("JenisDeposan").ToString)
                nBunga.Text = .Item("SukuBunga").ToString
                cNoBilyet.Text = .Item("NoBilyet").ToString
                SetOpt(optPajak, .Item("StatusPajak").ToString)
                cNasabah.EditValue = .Item("Nasabah").ToString
                cKeteranganNasabah.Text = .Item("NamaKantor").ToString
                If .Item("RekeningTabungan").ToString.Length > 8 Then
                    cRekeningTabungan.Text = .Item("RekeningTabungan").ToString
                    cNamaNasabah.Text = .Item("NamaNasabah").ToString
                    cAlamatNasabah.Text = .Item("AlamatNasabah").ToString
                    nSaldoTabungan.Text = GetSaldoTabungan(cRekeningTabungan.Text, dTgl.DateTime).ToString
                End If
                getjadwal()
            End With
        End If
    End Sub

    Private Sub GetJadwal()
        Dim db As New DataTable
        Dim n As Integer
        Const cField As String = "BulanKe,Tgl,JthTmp,''"
        Dim cWhere As String = String.Format(" and tgl >= '{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
        cWhere = String.Format("{0} and jthtmp <= '{1}'", cWhere, formatValue(dJthTmp.DateTime, formatType.yyyy_MM_dd))
        cWhere = cWhere & " and status = 0"
        db = objData.Browse(GetDSN, "JadwalBungaDeposito", cField, "Rekening", , cRekening.Text, cWhere)
        If db.Rows.Count > 0 Then
            dtJadwal.Clear()
            dtJadwal = db.Copy
            For n = 0 To dtJadwal.Rows.Count - 1
                dtJadwal.Rows(n).Item(3) = GetLocalDate(CDate(dtJadwal.Rows(n).Item(1)), False)
            Next
        Else
            dtJadwal.Clear()
        End If
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Ke")
        GridColumnFormat(GridView1, "TglValuta")
        GridColumnFormat(GridView1, "JthTmp")
        GridColumnFormat(GridView1, "Keterangan")
        db.Dispose()
    End Sub

    Private Sub IsiCombo()
        Dim properties As Repository.RepositoryItemComboBox = cmbKeterkaitan.Properties
        With properties
            .Items.AddRange(New String() {"1 - Terkait", "2 - Tidak Terkait"})
            .TextEditStyle = TextEditStyles.Standard
            '.ReadOnly = True
        End With
        cmbKeterkaitan.SelectedIndex = 1
        properties = cbJenisDeposito.Properties
        With properties
            .Items.AddRange(New String() {"1. ARO Biasa", "2. ARO Bunga Masuk Pokok", "3. Non ARO"})
        End With
        cbJenisDeposito.SelectedIndex = 1
    End Sub

    Private Sub trRegistrasiDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("GolonganDeposan", cGolonganDeposan)
        GetDataLookup("GolonganDeposito", cGolonganDeposito)
        GetDataLookup("AO", cAO, , "kode,nama")
        GetDataLookup("Wilayah", cWilayah)
        GetDataLookup("Cabang", cNasabah)
    End Sub

    Private Sub trRegistrasiDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        IsiCombo()
        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cGolonganDeposan.TabIndex, n)
        SetTabIndex(cGolonganDeposito.TabIndex, n)
        SetTabIndex(cWilayah.TabIndex, n)
        SetTabIndex(cAO.TabIndex, n)
        SetTabIndex(cmbKeterkaitan.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(dJthTmp.TabIndex, n)
        SetTabIndex(cmdHitungJadwal.TabIndex, n)
        SetTabIndex(optSetoran.TabIndex, n)
        SetTabIndex(optPencairan.TabIndex, n)
        SetTabIndex(optPencairanBunga.TabIndex, n)
        SetTabIndex(optDeposan.TabIndex, n)
        SetTabIndex(cbJenisDeposito.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(cNoBilyet.TabIndex, n)
        SetTabIndex(cNasabah.TabIndex, n)
        SetTabIndex(optPajak.TabIndex, n)
        SetTabIndex(cRekeningTabungan.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cCabang1.EnterMoveNextControl = True
        cKode.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cGolonganDeposan.EnterMoveNextControl = True
        cGolonganDeposito.EnterMoveNextControl = True
        cWilayah.EnterMoveNextControl = True
        cAO.EnterMoveNextControl = True
        cmbKeterkaitan.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        optSetoran.EnterMoveNextControl = True
        optPencairan.EnterMoveNextControl = True
        optPencairanBunga.EnterMoveNextControl = True
        optDeposan.EnterMoveNextControl = True
        cbJenisDeposito.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        cNoBilyet.EnterMoveNextControl = True
        cNasabah.EnterMoveNextControl = True
        optPajak.EnterMoveNextControl = True
        cRekeningTabungan.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cKode.Focus()
        Koreksi(True)
        cCabang1.Enabled = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        Koreksi(True)
        cRekening.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            cKode.Text = Padl(cKode.Text, cKode.Properties.MaxLength, "0")
            cKodeRegister = setKode(cCabang1.Text, cKode.Text)
            Dim cDataTable = objData.Browse(GetDSN, "registernasabah", "nama,alamat,telepon", "kode", data.myOperator.Assign, cKodeRegister)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cTelepon.Text = .Item("Telepon").ToString
                End With
            Else
                MessageBox.Show("No. Register tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                cAlamat.Text = ""
                cTelepon.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cRekening_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cRekening.GotFocus
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cGolonganDeposito.Closed
        GetKeteranganLookUp(cGolonganDeposito, cNamaGolonganDeposito)
    End Sub

    Private Sub cGolonganDeposito_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cGolonganDeposito.Closed
        GetKeteranganLookUp(cGolonganDeposito, cNamaGolonganDeposito)
        If nPos = myPos.add Then
            cRekening.Text = GetFrekuensi()
        End If
    End Sub

    Private Sub cGolonganNasabah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cGolonganDeposito.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganDeposito, cNamaGolonganDeposito)
        End If
    End Sub

    Private Sub cGolonganDeposito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cGolonganDeposito.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cGolonganDeposito, cNamaGolonganDeposito)
            If nPos = myPos.add Then
                cRekening.Text = GetFrekuensi()
            End If
        End If
    End Sub

    Private Sub cAO_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cAO.Closed
        GetKeteranganLookUp(cAO, cNamaAO, , "nama")
    End Sub

    Private Sub cAO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cAO.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cAO, cNamaAO, , "nama")
        End If
    End Sub

    Private Sub cWilayah_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cWilayah.Closed
        GetKeteranganLookUp(cWilayah, cNamaWilayah)
    End Sub

    Private Sub cWilayah_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cWilayah.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetKeteranganLookUp(cWilayah, cNamaWilayah)
        End If
    End Sub

    Private Sub cRekening_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim dbData As New DataTable
            dbData = objData.Browse(GetDSN, "Deposito", , "Rekening", , cRekening.Text)
            If dbData.Rows.Count > 0 Then
                If nPos = myPos.add Then
                    MessageBox.Show("Nomor Rekening sudah ada. Silahkan Ulangi pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cRekening.EditValue = ""
                    cRekening.Focus()
                    dbData.Dispose()
                    Exit Sub
                End If
                GetMemory()
                If nPos = myPos.delete Then
                    DeleteData()
                    If lNoDelete Then
                        cRekening.Focus()
                    End If
                End If
                dbData.Dispose()
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Data Tidak Ada. Silahkan mengulang pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cRekening.EditValue = ""
                cRekening.Focus()
                dbData.Dispose()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cRekeningTabungan_KeyDown(sender As Object, e As KeyEventArgs) Handles cRekeningTabungan.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim dbData As New DataTable
            Const cField As String = "t.Rekening,r.Nama,r.Alamat,g.Keterangan as NamaGolongan,t.close"
            Dim vaJoin() As Object = {"Left Join RegisterNasabah r on t.Kode = r.Kode", _
                                      "Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan"}
            dbData = objData.Browse(GetDSN, "Tabungan t", cField, "t.Rekening", , cRekeningTabungan.Text, , , vaJoin)
            If dbData.Rows.Count > 0 Then
                With dbData.Rows(0)
                    cNamaNasabah.Text = .Item("Nama").ToString
                    cAlamatNasabah.Text = .Item("Alamat").ToString

                    nSaldoTabungan.Text = GetSaldoTabungan(.Item("Rekening").ToString, dTgl.DateTime).ToString
                    If GetNull(.Item("Close").ToString).ToString = "1" Then
                        MessageBox.Show("Rekening tabungan sudah ditutup,transaksi tidak bisa dilanjutkan...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cRekeningTabungan.EditValue = ""
                        cRekeningTabungan.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening Tabungan, Tidak Ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cRekeningTabungan.EditValue = ""
                cRekeningTabungan.Focus()
            End If
            dbData.Dispose()
        End If
    End Sub
End Class