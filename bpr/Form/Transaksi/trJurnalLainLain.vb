﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraBars

Public Class trJurnalLainLain
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dbData As New DataTable
    Dim dtTable As New DataTable
    Dim nTotalDebet As Double, nTotalKredit As Double
    Dim dvTable As New DataView
    Dim nKeyStroke As Integer = 0

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
        If Not lEdit Or nPos = myPos.add Then
            InitValue()
        End If
        If lEdit Then
            If nPos = myPos.add Then
                cFaktur.Enabled = False
                ckFaktur.Visible = False
            Else
                cFaktur.Enabled = True
                ckFaktur.Visible = True
                cFaktur.Focus()
            End If
            GetFaktur(False)
        End If
    End Sub

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cFaktur.Text = ""
    End Sub

    Private Sub InitTable()
        dtTable.Reset()
        AddColumn(dtTable, "Nomor", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Rekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "NamaRekening", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Keterangan", System.Type.GetType("System.String"))
        AddColumn(dtTable, "Debet", System.Type.GetType("System.Double"))
        AddColumn(dtTable, "Kredit", System.Type.GetType("System.Double"))
        Dim row As DataRow = dtTable.NewRow()
        For n As Integer = 0 To dtTable.Rows.Count - 1
            If n = 0 Then
                row(n) = 1
            Else
                row(n) = ""
            End If
        Next
        dtTable.Rows.Add(row)
    End Sub

    Private Sub SetGrid()
        InitTable()
        GridControl1.DataSource = dtTable
        InitGrid(GridView1, True, , , , , , , eGridRepoType.eLookUpEdit, , , 0, 1, "rekening", "kode,keterangan", "jenis='D'", "kode")
        GridColumnFormat(GridView1, "Nomor", , , 5, , , "No.")
        GridColumnFormat(GridView1, "Rekening", , , 5, , True)
        GridColumnFormat(GridView1, "NamaRekening", , , 8)
        GridColumnFormat(GridView1, "Keterangan", , , , , True)
        GridColumnFormat(GridView1, "Debet", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, 7, HorzAlignment.Far, True)
        GridColumnFormat(GridView1, "Kredit", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, 7, HorzAlignment.Far, True)
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            DelBukuBesar(cFaktur.Text)

            objData.Delete(GetDSN, "Jurnal", "Faktur", , cFaktur.Text)

            objData.Delete(GetDSN, "totjurnal", "Faktur", , cFaktur.Text)
            'UpdActivity(Me, GetActivity)
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cFaktur.Text, "Nomor faktur tidak boleh kosong. Ulangi pengisian.") Then
            Return False
            cFaktur.Focus()
            Exit Function
        End If

        If IsHoliday(dTgl.DateTime) Then
            MsgBox("Proses Tidak Boleh Dilakukan Pada Hari Libur!", vbExclamation)
            ValidSaving = False
            dTgl.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    GetFaktur(True)
                    Dim cCabang As String = aCfg(eCfg.msKodeCabang).ToString
                    Dim cKeterangan As String = dtTable.Rows(0).Item("Keterangan").ToString
                    UpdTotJurnalLainLain(cFaktur.Text, dTgl.DateTime, cUserName, SNow, nTotalDebet, nTotalKredit, cCabang, cKeterangan)

                    For n = 0 To dtTable.Rows.Count - 1
                        With dtTable.Rows(n)
                            UpdJurnalLainLain(cFaktur.Text, dTgl.DateTime, .Item("Rekening").ToString, .Item("Keterangan").ToString,
                                              CDbl(.Item("Debet")), CDbl(.Item("Kredit")), cCabang)
                        End With
                    Next

                    ' Posting Jurnal
                    UpdRekJurnal(cFaktur.Text)

                    'UpdActivity(Me, GetActivity)
                    'If MsgBox("Cetak Bukti Jurnal?", vbQuestion + vbYesNo) = vbYes Then
                    '    CetakSlipKas()
                    'End If
                    'If MsgBox("Cetak Validasi Jurnal?", vbQuestion + vbYesNo) = vbYes Then
                    '    CetakValidasiJurnalLainLainDetail cFaktur.Text
                    'End If
                    'cFaktur.Text = GetLastFaktur(fkt_Jurnal, dTgl.Value, , True)

                    'If MsgBox("Apakah anda akan mencetak bukti kas masuk...?", vbQuestion + vbYesNo) = vbYes Then
                    '    CetakSlipKas()
                    'End If
                    'If MsgBox("Cetak Validasi Kas?", vbQuestion + vbYesNo) = vbYes Then
                    '    '        CetakValidasiKas cFaktur.Text
                    '    CetakValidasiKasDetail cFaktur.Text
                    'End If

                    'UpdActivity(Me, GetActivity)
                    MsgBox("Data sudah disimpan.", vbInformation)
                Else
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub GetFaktur(ByVal lUpdate As Boolean)
        If nPos = myPos.add Then
            cFaktur.Text = GetLastFaktur(eFaktur.fkt_Jurnal, dTgl.DateTime, , lUpdate)
        End If
    End Sub

    Private Sub GetMemory()
        Const cField As String = "j.*,r.Keterangan as NamaRekening"
        Dim vaJoin() As Object = {"Left Join Rekening r on r.Kode = j.Rekening",
                                  "left join TotJurnal t on j.faktur=t.faktur"}
        Dim cDataTable = objData.Browse(GetDSN, "jurnal j", cField, "j.faktur", data.myOperator.Assign, cFaktur.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            SetGrid()
            For n As Integer = 0 To cDataTable.Rows.Count - 1
                With cDataTable.Rows(n)
                    dTgl.DateTime = CDate(.Item("tgl").ToString)
                    dtTable.Rows(n).Item("Nomor") = n + 1
                    dtTable.Rows(n).Item("Rekening") = .Item("Rekening").ToString
                    dtTable.Rows(n).Item("NamaRekening") = .Item("NamaRekening").ToString
                    dtTable.Rows(n).Item("Keterangan") = .Item("Keterangan").ToString
                    dtTable.Rows(n).Item("Debet") = CDbl(.Item("Debet"))
                    dtTable.Rows(n).Item("Kredit") = CDbl(.Item("Kredit"))
                End With
            Next
            GridControl1.RefreshDataSource()
        End If
    End Sub

    Private Sub trJurnalLainLain_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        nKeyStroke = 0
    End Sub

    Private Sub trJurnalLainLain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        SetGrid()

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cFaktur.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cFaktur.EnterMoveNextControl = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        cFaktur.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        InitValue()
        cFaktur.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cFaktur_KeyDown(sender As Object, e As KeyEventArgs) Handles cFaktur.KeyDown
        If e.KeyCode = Keys.Enter Then
            If cFaktur.Text.Trim = "" Then
                cFaktur.Focus()
                Exit Sub
            End If
            If ckFaktur.Checked Then
                cFaktur.Text = GetLastFaktur(eFaktur.fkt_Jurnal, dTgl.DateTime, cFaktur.Text)
            End If
            Dim cWhere As String = String.Format("and left(faktur,2)='JR' and tgl='{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            dbData = objData.Browse(GetDSN, "jurnal", , "Faktur", , cFaktur.Text, cWhere)
            If dbData.Rows.Count > 0 Then
                cFaktur.Text = dbData.Rows(0).Item("faktur").ToString
                If nPos = myPos.add Then  ' Tambah
                    MessageBox.Show("Nomor Faktur Sudah Ada, Data Tidak bisa Ditambah", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cFaktur.Focus()
                    Exit Sub
                End If
                GetMemory()
                If nPos = myPos.delete Then
                    DeleteData()
                End If
            ElseIf dbData.Rows.Count = 0 And nPos <> myPos.add Then
                MessageBox.Show("Nomor Faktur Tidak Ada, Ulangi Pengisian", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cFaktur.Focus()
            End If
        End If
    End Sub

    Private Sub AddRowGrid()
        If (MessageBox.Show("Add row?", "Confirmation", MessageBoxButtons.YesNo) <> DialogResult.Yes) Then Return
        GridView1.AddNewRow()
    End Sub

    Private Sub InsertRowGrid(ByVal nBaris As Integer, Optional ByVal lBelow As Boolean = False)
        If (MessageBox.Show("Insert row?", "Confirmation", MessageBoxButtons.YesNo) <> DialogResult.Yes) Then Return
        Dim row As DataRow = dtTable.NewRow
        If lBelow Then
            nBaris = nBaris + 1
        End If
        dtTable.Rows.InsertAt(row, nBaris)
        dtTable.AcceptChanges()
        GridControl1.RefreshDataSource()
    End Sub

    Private Sub DeleteRowGrid(ByVal nBaris As Integer)
        If (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) <> DialogResult.Yes) Then Return
        GridView1.DeleteRow(nBaris)
    End Sub

    Private Sub GridView1_KeyDown(sender As Object, e As KeyEventArgs) Handles GridView1.KeyDown
        Dim view As GridView = CType(sender, GridView)
        Dim nBaris As Integer = view.FocusedRowHandle
        If e.KeyCode = Keys.Down AndAlso nBaris = GridView1.RowCount - 1 Then
            nKeyStroke = nKeyStroke + 1
            Const nTemp As Integer = 2
            If nKeyStroke = nTemp Then
                AddRowGrid()
                nKeyStroke = 0
                RefreshNumber()
            End If
        End If
        If e.KeyCode = Keys.Insert And e.Modifiers = Keys.Control Then
            InsertRowGrid(nBaris)
            RefreshNumber()
        End If
        If e.KeyCode = Keys.Insert And e.Modifiers = Keys.Shift Then
            InsertRowGrid(nBaris, True)
            RefreshNumber()
        End If
        If (e.KeyCode = Keys.Delete And e.Modifiers = Keys.Control) Then
            DeleteRowGrid(nBaris)
            RefreshNumber()
        End If
    End Sub

    Private Sub RefreshNumber()
        nTotalDebet = 0
        nTotalKredit = 0
        Dim cDebet As String = "0"
        Dim cKredit As String = "0"
        For n As Integer = 0 To dtTable.Rows.Count - 1
            dtTable.Rows(n).Item("Nomor") = n + 1
            cDebet = dtTable.Rows(n).Item("Debet").ToString
            cKredit = dtTable.Rows(n).Item("Kredit ").ToString
            nTotalDebet = nTotalDebet + CDbl(cDebet)
            nTotalKredit = nTotalKredit + CDbl(cKredit)
        Next
        GridControl1.RefreshDataSource()
    End Sub

    Private Sub bbAdd_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bbAdd.ItemClick
        AddRowGrid()
    End Sub

    Private Sub bbDelete_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bbDelete.ItemClick
        Dim nBaris As Integer = GridView1.FocusedRowHandle
        DeleteRowGrid(nBaris)
    End Sub

    Private Sub bbInsertBelow_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bbInsertBelow.ItemClick
        Dim nBaris As Integer = GridView1.FocusedRowHandle
        InsertRowGrid(nBaris, True)
    End Sub

    Private Sub bbInsertUpper_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bbInsertUpper.ItemClick
        Dim nBaris As Integer = GridView1.FocusedRowHandle
        InsertRowGrid(nBaris)
    End Sub

    Private Sub GridView1_RowClick(sender As Object, e As RowClickEventArgs) Handles GridView1.RowClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim pt As Point
            pt.Offset(e.X, e.Y)
            'BarItem1.ItemInMenuAppearance.Normal.BorderColor()
            With RadialMenu1
                .SubMenuHoverColor = Color.Red

                .ClearLinks()
                .AddItems(RadialMenuItem())
                .ShowPopup(pt)
            End With
        End If
    End Sub

    Private Sub RadialMenu1_Popup(sender As Object, e As EventArgs) Handles RadialMenu1.Popup
        RadialMenu1.Expand()
    End Sub

    Private Function RadialMenuItem() As BarItem()
        BarManager1.Images = imageCollection1
        Dim barButtonItem0 As BarItem = New BarButtonItem(BarManager1, "Add", 0)
        Dim barButtonItem1 As BarItem = New BarButtonItem(BarManager1, "Insert Upper", 0)
        Dim barButtonItem2 As BarItem = New BarButtonItem(BarManager1, "Insert Below", 0)
        Dim barButtonItem3 As BarItem = New BarButtonItem(BarManager1, "Delete", 1)
        Return New BarItem() {barButtonItem0, barButtonItem1, barButtonItem2, barButtonItem3}
    End Function

    Private Sub BarManager1_ItemClick(sender As Object, e As ItemClickEventArgs) Handles BarManager1.ItemClick
        AddRowGrid()
    End Sub
End Class