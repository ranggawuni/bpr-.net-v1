﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trKoreksiPerpanjanganDepositoPerNasabah
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtData IsNot Nothing Then
                    dtData.Dispose()
                    dtData = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trKoreksiPerpanjanganDepositoPerNasabah))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(2, 123)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(531, 246)
        Me.GridControl1.TabIndex = 32
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Location = New System.Drawing.Point(2, 375)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(531, 33)
        Me.PanelControl4.TabIndex = 31
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(362, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        Me.cmdSimpan.TabIndex = 10
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(443, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(531, 115)
        Me.PanelControl1.TabIndex = 30
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 35)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(131, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 87)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 39)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 61)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'trKoreksiPerpanjanganDepositoPerNasabah
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 410)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trKoreksiPerpanjanganDepositoPerNasabah"
        Me.Text = "Koreksi Perpanjangan Deposito Per Nasabah"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
End Class
