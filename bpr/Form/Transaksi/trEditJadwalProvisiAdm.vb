﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trEditJadwalProvisiAdm
    ReadOnly objData As New data()
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim cFaktur As String = ""
    Dim nProvisiTemp As Double = 0
    Dim nAdmTemp As Double = 0
    Dim nProvisiPerBulan As Double = 0
    Dim nAdmPerBulan As Double = 0
    Const nLamaTemp As Double = 0
    Dim dtPerpanjangan As New DataTable

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cKode.Text = ""
        nPlafond.Text = ""
        cCaraPerhitungan.Text = ""
        nLama.Text = ""
        nAdministrasi.Text = ""
        nProvisi.Text = ""
        nTotal.Text = ""
        optJenis.SelectedIndex = 2
    End Sub

    Private Shared Function ValidSaving() As Boolean
        ValidSaving = True
    End Function

    Private Sub SimpanData()
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        Dim dTanggal As Date
        If ValidSaving() Then
            If MessageBox.Show("Data akan disimpan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim cKodeCabang As String = cRekening.Text.Substring(0, 2)
                Dim nTotalProvisi As Double = 0
                Dim nTotalAdm As Double = 0
                If nProvisiTemp = 0 Or nProvisiTemp <> CDbl(nProvisi.Text) Then
                    nProvisiTemp = CDbl(nProvisi.Text)
                End If
                If nAdmTemp = 0 Or nAdmTemp <> CDbl(nAdministrasi.Text) Then
                    nAdmTemp = CDbl(nAdministrasi.Text)
                End If
                nProvisiPerBulan = Math.Round(nProvisiTemp / nLamaTemp, 2)
                nAdmPerBulan = Math.Round(nAdmTemp / nLamaTemp, 2)
                cWhere = String.Format("rekening='{0}'", cRekening.Text)
                vaField = {"provisi", "administrasi"}
                vaValue = {CDbl(nProvisi.Text), CDbl(nAdministrasi.Text)}
                objData.Edit(GetDSN, "debitur", cWhere, vaField, vaValue)

                cWhere = String.Format("and date_format(tglmutasi,'%Y%m') = '{0}'", Format(dTgl.DateTime, "YYYYmm"))
                objData.Delete(GetDSN, "provisi_adm", "rekening", , cRekening.Text)
                If CDbl(nProvisi.Text) > 0 Or CDbl(nAdministrasi.Text) > 0 Then
                    If optJenis.SelectedIndex = 1 Then
                        For n = 1 To nLamaTemp
                            If n = nLamaTemp Then
                                nProvisiPerBulan = Math.Round(nProvisiTemp - nTotalProvisi, 2)
                                nAdmPerBulan = Math.Round(nAdmTemp - nTotalAdm, 2)
                            Else
                                nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                                nTotalAdm = nTotalAdm + nAdmPerBulan
                            End If
                            dTanggal = DateAdd("m", n, dTgl.DateTime)
                            vaField = {"rekening", "tgl", "provisi", "adm", "tglMutasi", "cabangentry"}
                            vaValue = {cRekening.Text, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.Text, cKodeCabang}
                            cWhere = String.Format("rekening='{0}' and tgl='{1}'", cRekening.Text, formatValue(dTanggal, formatType.yyyy_MM_dd))
                            objData.Add(GetDSN, "provisi_adm", vaField, vaValue)
                        Next
                        If cCaraPerhitungan.Text = "2" Or cCaraPerhitungan.Text = "5" Then
                            For i = 0 To dtPerpanjangan.Rows.Count - 1
                                '0=rekening
                                '1=tgl
                                '2=lama
                                '3=provisi
                                '4=adm
                                '5=total
                                nTotalProvisi = 0
                                nTotalAdm = 0
                                With dtPerpanjangan.Rows(i)
                                    nProvisiPerBulan = Math.Round(CDbl(.Item(3)) / CDbl(.Item(2)), 2)
                                    nAdmPerBulan = Math.Round(CDbl(.Item(4)) / CDbl(.Item(2)), 2)
                                    For n = 1 + nLamaTemp To CDbl(.Item(2)) + nLamaTemp
                                        If n = CDbl(.Item(2)) + nLamaTemp Then
                                            nProvisiPerBulan = Math.Round(CDbl(.Item(3)) - nTotalProvisi, 2)
                                            nAdmPerBulan = Math.Round(CDbl(.Item(4)) - nTotalAdm, 2)
                                        Else
                                            nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                                            nTotalAdm = nTotalAdm + nAdmPerBulan
                                        End If
                                        dTanggal = DateAdd("m", n, dTgl.DateTime)
                                        vaField = {"rekening", "tgl", "provisi", "adm", "tglMutasi", "cabangentry"}
                                        vaValue = {cRekening, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.DateTime, cKodeCabang}
                                        cWhere = String.Format("rekening='{0}' and tgl='{1}'", cRekening.Text, formatValue(dTanggal, formatType.yyyy_MM_dd))
                                        objData.Add(GetDSN, "provisi_adm", vaField, vaValue)
                                    Next
                                End With
                            Next
                        End If
                    Else
                        For n = 0 To nLamaTemp - 1
                            If n = nLamaTemp - 1 Then
                                nProvisiPerBulan = Math.Round(nProvisiTemp - nTotalProvisi, 2)
                                nAdmPerBulan = Math.Round(nAdmTemp - nTotalAdm, 2)
                            Else
                                nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                                nTotalAdm = nTotalAdm + nAdmPerBulan
                            End If
                            dTanggal = DateAdd("m", n, dTgl.DateTime)
                            vaField = {"rekening", "tgl", "provisi", "adm", "tglMutasi", "CabangEntry"}
                            vaValue = {cRekening, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.DateTime, cKodeCabang}
                            cWhere = String.Format("rekening='{0}' and tgl='{1}'", cRekening.Text, formatValue(dTanggal, formatType.yyyy_MM_dd))
                            objData.Add(GetDSN, "provisi_adm", vaField, vaValue)
                        Next
                        If cCaraPerhitungan.Text = "2" Or cCaraPerhitungan.Text = "5" Then
                            For i = 0 To dtPerpanjangan.Rows.Count - 1
                                '0=rekening
                                '1=tgl
                                '2=lama
                                '3=provisi
                                '4=adm
                                '5=total
                                nTotalProvisi = 0
                                nTotalAdm = 0
                                With dtPerpanjangan.Rows(i)
                                    nProvisiPerBulan = Math.Round(CDbl(.Item(3)) / CDbl(.Item(2)), 2)
                                    nAdmPerBulan = Math.Round(CDbl(.Item(4)) / CDbl(.Item(2)), 2)
                                    For n = 0 + nLamaTemp To CDbl(.Item(2)) - 1 + nLamaTemp
                                        If n = CDbl(.Item(2)) - 1 Then
                                            nProvisiPerBulan = Math.Round(CDbl(.Item(3)) - nTotalProvisi, 2)
                                            nAdmPerBulan = Math.Round(CDbl(.Item(4)) - nTotalAdm, 2)
                                        Else
                                            nTotalProvisi = nTotalProvisi + nProvisiPerBulan
                                            nTotalAdm = nTotalAdm + nAdmPerBulan
                                        End If
                                        dTanggal = DateAdd("m", n, dTgl.DateTime)
                                        vaField = {"rekening", "tgl", "provisi", "adm", "tglMutasi", "cabangentry"}
                                        vaValue = {cRekening, dTanggal, nProvisiPerBulan, nAdmPerBulan, dTgl.DateTime, cKodeCabang}
                                        cWhere = String.Format("rekening='{0}' and tgl='{1}'", cRekening.Text, formatValue(dTanggal, formatType.yyyy_MM_dd))
                                        objData.Add(GetDSN, "provisi_adm", vaField, vaValue)
                                    Next
                                End With
                            Next
                        End If
                    End If
                End If
                MessageBox.Show("Data sudah disimpan.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                InitValue()
            End If
        End If
    End Sub

    Private Sub trEditJadwalProvisiAdmvb_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("debitur t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trEditJadwalProvisiAdmvb_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(nAdministrasi.TabIndex, n)
        SetTabIndex(nProvisi.TabIndex, n)
        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nAdministrasi.EnterMoveNextControl = True
        nProvisi.EnterMoveNextControl = True
        optJenis.EnterMoveNextControl = True

        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nProvisi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nTotal, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim db As New DataTable
            Dim nSelisihBulan As Integer
            Dim cWhere As String = ""
            Dim cField As String = "d.kode,d.plafond,d.tgl,d.provisi,d.administrasi,d.lama,"
            cField = cField & "d.sukubunga,r.nama,r.alamat,d.faktur,d.caraperhitungan"
            Dim vaJoin() As Object = {"Left Join registernasabah r on r.kode = d.kode"}
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "debitur d", cField, "d.rekening", , cRekening.Text, , , vaJoin)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    cNama.Text = .Item("Nama").ToString
                    cAlamat.Text = .Item("Alamat").ToString
                    cKode.Text = .Item("Kode").ToString
                    dTgl.DateTime = CDate(.Item("Tgl"))
                    nPlafond.Text = .Item("Plafond").ToString
                    nLama.Text = .Item("Lama").ToString
                    nAdministrasi.Text = .Item("Administrasi").ToString
                    nProvisi.Text = .Item("Provisi").ToString
                    nTotal.Text = (CDbl(nAdministrasi.Text) + CDbl(nProvisi.Text)).ToString
                    nAdmTemp = CDbl(.Item("Administrasi"))
                    nProvisiTemp = CDbl(.Item("Provisi"))
                    cFaktur = .Item("Faktur").ToString
                    cCaraPerhitungan.Text = .Item("CaraPerhitungan").ToString
                    lblCaraHitung.Text = .Item("NamaCaraPerhitungan").ToString
                    Dim cSQL As String = String.Format("select tgl as tgl From provisi_adm where rekening='{0}' order by tgl limit 1", cRekening.Text)
                    db = objData.SQL(GetDSN, cSQL)
                    If db.Rows.Count > 0 Then
                        nSelisihBulan = CInt(DateDiff("m", dTgl.Text, CDate(.Item("Tgl"))))
                        If nSelisihBulan = 0 Then
                            optJenis.SelectedIndex = 1
                        Else
                            optJenis.SelectedIndex = 2
                        End If
                    End If
                    If cCaraPerhitungan.Text = "2" Or cCaraPerhitungan.Text = "5" Then
                        cField = "rekening,tgl,lama,provisi,administrasi,provisi+administrasi as total"
                        cWhere = String.Format("and tgl<='{0}'", formatValue(Date.Today, formatType.yyyy_MM_dd))
                        db = objData.Browse(GetDSN, "perpanjangankredit", cField, "rekening", , cRekening.Text, cWhere)
                        If db.Rows.Count > 0 Then
                            dtPerpanjangan = db.Copy
                            For n = 0 To dtPerpanjangan.Rows.Count - 1
                                nProvisi.Text = (CDbl(nProvisi.Text) + CDbl(dtPerpanjangan.Rows(n).Item(3))).ToString
                                nAdministrasi.Text = (CDbl(nAdministrasi.Text) + CDbl(dtPerpanjangan.Rows(n).Item(4))).ToString
                                nTotal.Text = (CDbl(nAdministrasi.Text) + CDbl(nProvisi.Text)).ToString
                            Next
                        Else
                            dtPerpanjangan.Reset()
                        End If
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            db.Dispose()
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub
End Class