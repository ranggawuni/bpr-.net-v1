﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils

Public Class trPerpanjanganDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Private ReadOnly x As TypeDeposito
    Dim nTarifPajak As Single
    Dim nSaldoKenaPajak As Double
    Dim nBungaTemp As Double
    Dim nPajakTemp As Double
    Dim dtJadwal As New DataTable
    Dim lAROBungaMasukPokok As Boolean
    Dim dbData As New DataTable
    Private ReadOnly dTglPenempatan As Date

    Private Sub InitValue()
        dTanggal.DateTime = Date.Today
        cRekeningLama.EditValue = ""
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cGolonganDeposito.EditValue = ""
        cKeteranganGolonganDeposito.Text = ""
        cGolonganDeposan.EditValue = ""
        cKeteranganGolonganDeposan.Text = ""
        nBunga.Text = ""
        nBungaBaru.Text = ""
        nLama.Text = ""
        dTgl.DateTime = Date.Today
        dJatuhTempo.DateTime = Date.Today

        nNominal.Text = ""
        nTotBunga.Text = ""
        nPajak.Text = ""
        nNominalBaru.Text = ""
        nPerpanjanganKe.Text = ""
        cRekeningTabungan.Text = ""
        cNamaTabungan.Text = ""
    End Sub

    Private Sub trPerpanjanganDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Dim cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
        cField = "t.rekeninglama,t.rekening,r.nama,r.alamat"
        LookupSearch("deposito t", cRekeningLama, "t.rekening", , cField, vaJoin)
        If Not KasTeller() Then
            cmdKeluar.PerformClick()
            Exit Sub
        End If
        SetTglTransaksi(dTanggal)
        If dTanggal.Enabled Then
            dTanggal.Focus()
        Else
            cRekening.Focus()
        End If
        If dTglTemp = #1/1/1900# Then
            dTanggal.Text = CStr(GetTglTransaksi())
        Else
            dTanggal.Text = CStr(dTglTemp)
        End If
        If Not IsOpenTransaksi() Then
            Close()
            Exit Sub
        End If
        cRekening.Focus()
    End Sub

    Private Sub trPerpanjanganDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        InitValue()
        InitTable()
        InitForm(Me, , , cmdKeluar)
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTanggal.TabIndex, n)
        SetTabIndex(cRekeningLama.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(nBungaBaru.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dJatuhTempo.TabIndex, n)
        SetTabIndex(nTotBunga.TabIndex, n)
        SetTabIndex(nPajak.TabIndex, n)
        SetTabIndex(nPerpanjanganKe.TabIndex, n)

        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTanggal.EnterMoveNextControl = True
        cRekeningLama.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True
        nBungaBaru.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        dJatuhTempo.EnterMoveNextControl = True
        nTotBunga.EnterMoveNextControl = True
        nPajak.EnterMoveNextControl = True
        nPerpanjanganKe.EnterMoveNextControl = True

        FormatTextBox(nBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nBungaBaru, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNominal, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nTotBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPajak, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nNominalBaru, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPerpanjanganKe, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
            If Len(cRekening.Text) = 12 Then
                Const cField As String = "nominal,tgl"
                Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
                If cDataTable.Rows.Count > 0 Then
                    If Val(cDataTable.Rows(0).Item("nominal")) = 0 Then
                        MessageBox.Show("Pembukaan Deposito Belum Dilakukan, Data Tidak Bisa Dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        If CInt(cDataTable.Rows(0).Item("Status")) <> 0 Then
                            MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            InitValue()
                            cRekening.Focus()
                        Else
                            Dim cWhere As String = String.Format("and date_format(tgl,'%Y%m') = date_format('{0}', '%Y%m')", formatValue(dTanggal.DateTime, formatType.yyyy_MM_dd))
                            cWhere = cWhere & " and kas='E'"
                            Dim db As New DataTable
                            db = objData.Browse(GetDSN, "mutasideposito", "rekening", "Rekening", , cRekening.Text, cWhere)
                            If db.Rows.Count > 0 Then
                                MsgBox("Rekening Sudah Diperpanjang.", vbInformation)
                                cRekening.Focus()
                                Exit Sub
                            End If
                            db.Dispose()
                            GetMemory()
                        End If
                        If CDate(cDataTable.Rows(0).Item("tgl").ToString) > CDate(dTanggal.Text) Then
                            MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            cRekening.Focus()
                            Exit Sub
                        End If
                        GetMemory()
                    End If
                Else
                    MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cRekening.Focus()
                    cRekening.EditValue = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub GetMemory()
        Dim db As New DataTable
        Const cFields As String = "d.rekening,g.lama"
        Dim vaJoin() As Object = {"left join golongandeposito g on g.kode = d.golongandeposito"}
        Dim cDataTable = objData.Browse(GetDSN, "deposito d", cFields, "d.rekening", data.myOperator.Assign, cRekening.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                If CInt(.Item("Status")) <> 0 Then
                    MessageBox.Show("Deposito Sudah Cair, Transaksi Tidak Bisa dilanjutkan !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cRekening.Focus()
                    Exit Sub
                End If
                GetDeposito(cRekening.Text, dTanggal.DateTime, x)
                cNama.Text = x.cNama
                cAlamat.Text = x.cAlamat
                cGolonganDeposan.Text = x.cGolonganDeposan
                cKeteranganGolonganDeposan.Text = x.cNamaGolonganDeposan
                cGolonganDeposito.Text = x.cGolonganDeposito
                cKeteranganGolonganDeposito.Text = x.cNamaGolonganDeposito
                cRekeningTabungan.Text = x.cRekeningTabungan
                cNamaTabungan.Text = x.cNamaNasabahTabungan
                nNominal.Text = x.nNominal.ToString
                nBunga.Text = x.nSukuBunga.ToString
                nLama.Text = .Item("Lama").ToString
                nBungaBaru.Text = x.nSukuBunga.ToString
                nPerpanjanganKe.Text = x.nPerpanjanganKe.ToString

                dJatuhTempo.DateTime = DateAdd(DateInterval.Month, Val(nLama.Text), dTgl.DateTime)
                lblJatuhTempo.Text = GetNamaHari(dJatuhTempo.DateTime)
                lblTglPenempatan.Text = GetNamaHari(dTgl.DateTime)
                For n = 0 To cbJenisDeposito.Properties.Items.Count - 1
                    If cbJenisDeposito.Properties.Items(n).ToString.Substring(0, 1) = x.cARO Then
                        cbJenisDeposito.Text = cbJenisDeposito.Properties.Items(n).ToString
                    End If
                Next

                nSaldoKenaPajak = x.nTotalNominalDeposito
                nNominal.Text = x.nNominal.ToString
                nBunga.Text = x.nSukuBunga.ToString
                nBungaBaru.Text = nBunga.Text
                nTarifPajak = x.nTarifPajak
                If x.cARO = "2" Then
                    lAROBungaMasukPokok = True
                Else
                    lAROBungaMasukPokok = False
                End If
                SetBunga(lAROBungaMasukPokok)
            End With
        End If
        db.Dispose()
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cRekening.Text, "Rekening Harus Diisi, Ulangi Pengisian.....!") Then
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
    End Function

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Dim dJthTmp As Date
        Dim cKodeCabang As String
        Dim dTglValutaTemp As Date
        Dim cFakturPerpanjangan As String
        Dim dTglPerpanjangan As Date
        Dim dJthTmpPerpanjangan As Date
        Dim vaField() As Object
        Dim vaValue() As Object

        If ValidSaving() Then
            dJthTmp = DateAdd("m", 1, dTanggal.DateTime)
            Const cField As String = "m.rekening,m.tgl,g.lama,date_add(m.tgl,interval g.lama month) as jatuhtempo"
            Dim cWhere As String = String.Format("and m.tgl<'{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            Dim vaJoin() As Object = {"left join deposito d on d.rekening = m.rekening",
                                       "left join golongandeposito g on g.kode = d.golongandeposito"}
            dbData = objData.Browse(GetDSN, "mutasideposito m", cField, "m.rekening", , cRekening.Text, cWhere, "m.tgl desc", vaJoin)
            If dbData.Rows.Count > 0 Then
                If dTgl.DateTime >= CDate(dbData.Rows(0).Item("jatuhtempo")) Then
                    dTglValutaTemp = GetTglDeposito(dTgl.DateTime, dTglPenempatan)
                    HitungJadwal(dTglValutaTemp)

                    cFakturPerpanjangan = GetLastFaktur(eFaktur.fkt_Deposito, dTgl.DateTime, , True)
        
                    dTglPerpanjangan = CDate(dtJadwal.Rows(0).Item("Valuta"))
                    dJthTmpPerpanjangan = CDate(dtJadwal.Rows(dtJadwal.Rows.Count - 1).Item("JthTmp"))
                    cKodeCabang = cRekening.Text.Substring(0, 2)
                    UpdMutasiDeposito(cKodeCabang, cFakturPerpanjangan, cRekening.Text, dTglPerpanjangan, _
                                      dJthTmpPerpanjangan, , , , , , True, , , , "E", Val(nBungaBaru.Text))

                    For n = 0 To dtJadwal.Rows.Count - 1
                        With dtJadwal.Rows(0)
                            Dim nKe As Integer = CInt(.Item("Ke"))
                            Dim dValutaTemp As Date = CDate(.Item("Valuta"))
                            Dim dTempoTemp As Date = CDate(.Item("JthTmp"))
                            cWhere = " and bulanke = " & nKe
                            cWhere = String.Format("{0} and date_format(tgl,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(dValutaTemp, formatType.yyyy_MM_dd))
                            cWhere = String.Format("{0} and date_format(jthtmp,'%Y%m') = date_format('{1}','%Y%m') ", cWhere, formatValue(dTempoTemp, formatType.yyyy_MM_dd))
                            cWhere = cWhere & " and status=0"
                            objData.Delete(GetDSN, "jadwalbungadeposito", "rekening", , cRekening.Text, cWhere)

                            vaField = {"cabangentry", "rekening", "bulanke", "tgl",
                                       "jthtmp", "status", "tglpembukaan", "datetime", "sukubunga",
                                       "PerpanjanganKe", "faktur"}
                            vaValue = {cKodeCabang, cRekening.Text, nKe, formatValue(dValutaTemp, formatType.yyyy_MM_dd),
                                       formatValue(dTempoTemp, formatType.yyyy_MM_dd), 0, formatValue(dTgl.DateTime, formatType.yyyy_MM_dd), SNow(), Val(nBungaBaru.Text),
                                       0, cFakturPerpanjangan}
                            objData.Add(GetDSN, "jadwalbungadeposito", vaField, vaValue)
                        End With
                    Next
                End If
            End If
            MsgBox("Data sudah disimpan.", vbInformation)
        End If
    End Sub

    Function GetTglDeposito(ByVal dTglProses As Date, ByVal dTglPenempatan As Date) As Date
        If dTglProses.Day = EOM(dTglPenempatan).Day And dTglProses.Day < dTglPenempatan.Day Then
            GetTglDeposito = dTglProses
        Else
            GetTglDeposito = DateSerial(dTglProses.Year, dTglProses.Month, dTglPenempatan.Day)
        End If
    End Function

    Private Sub HitungJadwal(ByVal dTanggal As Date)
        Dim dTemp As Date = dTgl.DateTime
        Dim dValuta As Date
        Dim dJatuhTempo1 As Date
        Dim n As Integer = CInt(nLama.Text)
        Dim i As Integer = 1
        Dim m As Double = 0

        dtJadwal.Clear()
        dJatuhTempo1 = DateAdd("m", Val(nLama.Text), dTanggal)
        Do Until i > Val(nLama.Text)
            dTemp = DateAdd("m", -(n - i), dJatuhTempo1)
            dValuta = DateAdd("m", -1, dTemp)
            m = m + 1
            dtJadwal.Rows.Add(New Object() {m, dValuta, dTemp, GetLocalDate(dValuta, False)})
            i = i + 1
        Loop
        GridControl1.DataSource = dtJadwal
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Ke", , , 50, DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView1, "Valuta", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, 50)
        GridColumnFormat(GridView1, "JthTmp", DevExpress.Utils.FormatType.DateTime, formatType.dd_MM_yyyy, 50)
        GridColumnFormat(GridView1, "SukuBunga", , , 50, DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub InitTable()
        AddColumn(dtJadwal, "Ke", System.Type.GetType("System.Int32"))
        AddColumn(dtJadwal, "Valuta", System.Type.GetType("System.Date"))
        AddColumn(dtJadwal, "JthTmp", System.Type.GetType("System.Date"))
        AddColumn(dtJadwal, "Keterangan", System.Type.GetType("System.String"))
    End Sub

    Private Sub dTgl_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dTanggal.LostFocus
        If Not checkTglTransaksi(CDate(dTanggal.Text)) Then
            dTanggal.Focus()
        End If
        GetMemory()
        dTglTemp = CDate(dTanggal.Text)
    End Sub

    Private Sub nLama_KeyDown(sender As Object, e As KeyEventArgs) Handles nLama.KeyDown
        If e.KeyCode = Keys.Enter Then
            dTgl.DateTime = dTanggal.DateTime
            dJatuhTempo.DateTime = DateAdd("m", Val(nLama.Text), dTgl.DateTime)
            GetBunga()
            SumNominal()
            lblTglPenempatan.Text = GetNamaHari(dTgl.DateTime)
            SetBunga(lAROBungaMasukPokok)
        End If
    End Sub

    Private Sub SetBunga(ByVal lBungaMasukPokok As Boolean)
        If lBungaMasukPokok Then
            nTotBunga.Text = nBungaTemp.ToString
            nPajak.Text = nPajakTemp.tostring
        Else
            nTotBunga.Text = "0"
            nPajak.Text = "0"
        End If
    End Sub

    Private Sub GetBunga()
        Dim nTotalBunga As Double
        'Dim nHari As Double = DateDiff("d", dTgl.DateTime, dJatuhTempo.DateTime)
        Dim nBungaBulanan As Double

        nPajak.Text = "0"
        nBungaBulanan = Val(nNominal.Text) * Val(nBungaBaru.Text) / 1200
        nTotalBunga = Math.Round(nBungaBulanan * Val(nLama.Text), 0)
        '  nBungaHarian = nNominal.value * nBungaBaru.value / 36500
        '  nTotalBunga = Round(nBungaHarian * nHari, 0)
        nTotBunga.Text = nTotalBunga.ToString
        If nSaldoKenaPajak > Val(aCfg(eCfg.msSaldoMinimumKenaPajak)) Then
            nPajak.Text = Math.Round(nTotalBunga * Val(nLama.Text) * nTarifPajak / 100).ToString
        End If
        nBungaTemp = Val(nTotBunga.Text)
        nPajakTemp = Val(nPajak.Text)
        If Not lAROBungaMasukPokok Then
            nBungaTemp = 0
            nPajakTemp = 0
            nTotBunga.Text = "0"
            nPajak.Text = "0"
        End If
    End Sub

    Private Sub SumNominal()
        If lAROBungaMasukPokok Then
            Dim nTemp As Double = Val(nNominal.Text) + Val(nTotBunga.Text) - Val(nPajak.Text)
            nNominalBaru.Text = nTemp.ToString
        Else
            nNominalBaru.Text = nNominal.Text
        End If
    End Sub

    Private Sub nTotBunga_KeyDown(sender As Object, e As KeyEventArgs) Handles nTotBunga.KeyDown
        If e.KeyCode = Keys.Enter Then
            GetBunga()
        End If
        SumNominal()
    End Sub
End Class