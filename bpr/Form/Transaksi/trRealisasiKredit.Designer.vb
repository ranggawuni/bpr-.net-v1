﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trRealisasiKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
                If vaSifatKredit IsNot Nothing Then
                    vaSifatKredit.Dispose()
                    vaSifatKredit = Nothing
                End If
                If vaSektorEkonomi IsNot Nothing Then
                    vaSektorEkonomi.Dispose()
                    vaSektorEkonomi = Nothing
                End If
                If vaJenisPenggunaan IsNot Nothing Then
                    vaJenisPenggunaan.Dispose()
                    vaJenisPenggunaan = Nothing
                End If
                If vaGolonganDebitur IsNot Nothing Then
                    vaGolonganDebitur.Dispose()
                    vaGolonganDebitur = Nothing
                End If
                If vaGolonganPenjamin IsNot Nothing Then
                    vaGolonganPenjamin.Dispose()
                    vaGolonganPenjamin = Nothing
                End If
                If vaWilayah IsNot Nothing Then
                    vaWilayah.Dispose()
                    vaWilayah = Nothing
                End If
                If vaJadwal IsNot Nothing Then
                    vaJadwal.Dispose()
                    vaJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trRealisasiKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.cRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cCabang1 = New DevExpress.XtraEditors.TextEdit()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cGolonganKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamatNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.cbJenisUsaha = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.cbPeriodePembayaran = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cbSumberDana = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cBagianYangDijamin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.optAsuransi = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganPenjamin = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganPenjamin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.cAO = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAO = New DevExpress.XtraEditors.TextEdit()
        Me.cBendahara = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaBendahara = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cWilayah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaWilayah = New DevExpress.XtraEditors.TextEdit()
        Me.cNoSPK = New DevExpress.XtraEditors.TextEdit()
        Me.optAutoDebet = New DevExpress.XtraEditors.RadioGroup()
        Me.cNamaSifatKredit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.cSifatKredit = New DevExpress.XtraEditors.LookUpEdit()
        Me.cbKeterkaitan = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaJenisPenggunaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cSektorEkonomi = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaSektorEkonomi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDebitur = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaGolonganDebitur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cJenisPenggunaan = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cPengajuan = New DevExpress.XtraEditors.LookUpEdit()
        Me.nPlafondPengajuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaPengajuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl()
        Me.nNilaiPengikatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.nNilaiNJOP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.nNomor = New DevExpress.XtraEditors.TextEdit()
        Me.nNilaiJaminan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeteranganJaminan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.cJaminan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.cNamaCaraPerhitungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl63 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.nAngsuran = New DevExpress.XtraEditors.TextEdit()
        Me.nPembulatan = New DevExpress.XtraEditors.TextEdit()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.nProvisi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.cProvisi = New DevExpress.XtraEditors.LookUpEdit()
        Me.nPersenProvisi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.nAsuransi = New DevExpress.XtraEditors.TextEdit()
        Me.nMaterai = New DevExpress.XtraEditors.TextEdit()
        Me.nNotaris = New DevExpress.XtraEditors.TextEdit()
        Me.nAdministrasi = New DevExpress.XtraEditors.TextEdit()
        Me.cAdministrasi = New DevExpress.XtraEditors.LookUpEdit()
        Me.nPersenAdm = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.optInstansi = New DevExpress.XtraEditors.RadioGroup()
        Me.optKaryawan = New DevExpress.XtraEditors.RadioGroup()
        Me.optDenda = New DevExpress.XtraEditors.RadioGroup()
        Me.optJenis = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.nPlafond = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.cCaraPerhitungan = New DevExpress.XtraEditors.LookUpEdit()
        Me.nGraceBunga = New DevExpress.XtraEditors.TextEdit()
        Me.nPersBunga = New DevExpress.XtraEditors.TextEdit()
        Me.optProvisi = New DevExpress.XtraEditors.RadioGroup()
        Me.nPersBungaBerikutnya = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.nBungaEfektif = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.nGracePokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl8 = New DevExpress.XtraEditors.PanelControl()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.cmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.nBungaPenyesuaian = New DevExpress.XtraEditors.TextEdit()
        Me.nPokokPenyesuaian = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl69 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbJenisUsaha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodePembayaran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbSumberDana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBagianYangDijamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optAsuransi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cBendahara.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaBendahara.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNoSPK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optAutoDebet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSifatKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSifatKredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cPengajuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafondPengajuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaPengajuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        CType(Me.nNilaiPengikatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiNJOP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNomor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNilaiJaminan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganJaminan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cJaminan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.cNamaCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAngsuran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPembulatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPersenProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAsuransi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nMaterai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNotaris.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPersenAdm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optInstansi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optDenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nGraceBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPersBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPersBungaBerikutnya.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBungaEfektif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nGracePokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl8.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBungaPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPokokPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cCabang1)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(2, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(778, 142)
        Me.PanelControl1.TabIndex = 17
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 120)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(58, 13)
        Me.lblTglRegister.TabIndex = 189
        Me.lblTglRegister.Text = "Tgl Realisasi"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(103, 116)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 188
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(103, 94)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Size = New System.Drawing.Size(105, 20)
        Me.cRekening.TabIndex = 187
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 98)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 186
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cCabang1
        '
        Me.cCabang1.EditValue = "12"
        Me.cCabang1.Location = New System.Drawing.Point(103, 5)
        Me.cCabang1.Name = "cCabang1"
        Me.cCabang1.Properties.Mask.EditMask = "##.######"
        Me.cCabang1.Properties.MaxLength = 2
        Me.cCabang1.Size = New System.Drawing.Size(30, 20)
        Me.cCabang1.TabIndex = 135
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(103, 72)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(8, 76)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(103, 50)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(294, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 54)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 27)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 31)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(8, 8)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(60, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Register"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(139, 5)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(3, 149)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(778, 365)
        Me.XtraTabControl1.TabIndex = 191
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PanelControl4)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(772, 337)
        Me.XtraTabPage1.Text = "Data Kredit"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cGolonganKredit)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganKredit)
        Me.PanelControl4.Controls.Add(Me.LabelControl19)
        Me.PanelControl4.Controls.Add(Me.PanelControl5)
        Me.PanelControl4.Controls.Add(Me.cbJenisUsaha)
        Me.PanelControl4.Controls.Add(Me.LabelControl22)
        Me.PanelControl4.Controls.Add(Me.cbPeriodePembayaran)
        Me.PanelControl4.Controls.Add(Me.LabelControl5)
        Me.PanelControl4.Controls.Add(Me.cbSumberDana)
        Me.PanelControl4.Controls.Add(Me.LabelControl4)
        Me.PanelControl4.Controls.Add(Me.cBagianYangDijamin)
        Me.PanelControl4.Controls.Add(Me.LabelControl21)
        Me.PanelControl4.Controls.Add(Me.optAsuransi)
        Me.PanelControl4.Controls.Add(Me.LabelControl20)
        Me.PanelControl4.Controls.Add(Me.LabelControl1)
        Me.PanelControl4.Controls.Add(Me.cGolonganPenjamin)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganPenjamin)
        Me.PanelControl4.Controls.Add(Me.LabelControl7)
        Me.PanelControl4.Controls.Add(Me.cAO)
        Me.PanelControl4.Controls.Add(Me.cNamaAO)
        Me.PanelControl4.Controls.Add(Me.cBendahara)
        Me.PanelControl4.Controls.Add(Me.cNamaBendahara)
        Me.PanelControl4.Controls.Add(Me.LabelControl9)
        Me.PanelControl4.Controls.Add(Me.LabelControl8)
        Me.PanelControl4.Controls.Add(Me.cWilayah)
        Me.PanelControl4.Controls.Add(Me.cNamaWilayah)
        Me.PanelControl4.Controls.Add(Me.cNoSPK)
        Me.PanelControl4.Controls.Add(Me.optAutoDebet)
        Me.PanelControl4.Controls.Add(Me.cNamaSifatKredit)
        Me.PanelControl4.Controls.Add(Me.LabelControl6)
        Me.PanelControl4.Controls.Add(Me.cSifatKredit)
        Me.PanelControl4.Controls.Add(Me.cbKeterkaitan)
        Me.PanelControl4.Controls.Add(Me.LabelControl17)
        Me.PanelControl4.Controls.Add(Me.LabelControl10)
        Me.PanelControl4.Controls.Add(Me.cNamaJenisPenggunaan)
        Me.PanelControl4.Controls.Add(Me.LabelControl11)
        Me.PanelControl4.Controls.Add(Me.cSektorEkonomi)
        Me.PanelControl4.Controls.Add(Me.cNamaSektorEkonomi)
        Me.PanelControl4.Controls.Add(Me.LabelControl13)
        Me.PanelControl4.Controls.Add(Me.cGolonganDebitur)
        Me.PanelControl4.Controls.Add(Me.cNamaGolonganDebitur)
        Me.PanelControl4.Controls.Add(Me.LabelControl16)
        Me.PanelControl4.Controls.Add(Me.LabelControl18)
        Me.PanelControl4.Controls.Add(Me.cJenisPenggunaan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 63)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(766, 269)
        Me.PanelControl4.TabIndex = 18
        '
        'cGolonganKredit
        '
        Me.cGolonganKredit.Location = New System.Drawing.Point(128, 27)
        Me.cGolonganKredit.Name = "cGolonganKredit"
        Me.cGolonganKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganKredit.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganKredit.TabIndex = 229
        '
        'cNamaGolonganKredit
        '
        Me.cNamaGolonganKredit.Enabled = False
        Me.cNamaGolonganKredit.Location = New System.Drawing.Point(178, 26)
        Me.cNamaGolonganKredit.Name = "cNamaGolonganKredit"
        Me.cNamaGolonganKredit.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganKredit.TabIndex = 228
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(5, 30)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl19.TabIndex = 226
        Me.LabelControl19.Text = "Golongan Kredit"
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl5.Controls.Add(Me.cAlamatNasabah)
        Me.PanelControl5.Controls.Add(Me.LabelControl25)
        Me.PanelControl5.Controls.Add(Me.cNamaNasabah)
        Me.PanelControl5.Controls.Add(Me.LabelControl24)
        Me.PanelControl5.Controls.Add(Me.LabelControl23)
        Me.PanelControl5.Location = New System.Drawing.Point(364, 184)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(396, 76)
        Me.PanelControl5.TabIndex = 225
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(128, 5)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningTabungan.Size = New System.Drawing.Size(141, 20)
        Me.cRekeningTabungan.TabIndex = 206
        '
        'cAlamatNasabah
        '
        Me.cAlamatNasabah.Enabled = False
        Me.cAlamatNasabah.Location = New System.Drawing.Point(128, 50)
        Me.cAlamatNasabah.Name = "cAlamatNasabah"
        Me.cAlamatNasabah.Size = New System.Drawing.Size(263, 20)
        Me.cAlamatNasabah.TabIndex = 205
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(8, 54)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl25.TabIndex = 204
        Me.LabelControl25.Text = "Alamat"
        '
        'cNamaNasabah
        '
        Me.cNamaNasabah.Enabled = False
        Me.cNamaNasabah.Location = New System.Drawing.Point(128, 27)
        Me.cNamaNasabah.Name = "cNamaNasabah"
        Me.cNamaNasabah.Size = New System.Drawing.Size(170, 20)
        Me.cNamaNasabah.TabIndex = 203
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(8, 31)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl24.TabIndex = 202
        Me.LabelControl24.Text = "Nama"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(8, 9)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl23.TabIndex = 200
        Me.LabelControl23.Text = "Rekening Tabungan"
        '
        'cbJenisUsaha
        '
        Me.cbJenisUsaha.Location = New System.Drawing.Point(492, 126)
        Me.cbJenisUsaha.Name = "cbJenisUsaha"
        Me.cbJenisUsaha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenisUsaha.Size = New System.Drawing.Size(138, 20)
        Me.cbJenisUsaha.TabIndex = 224
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(372, 130)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl22.TabIndex = 223
        Me.LabelControl22.Text = "Jenis Usaha"
        '
        'cbPeriodePembayaran
        '
        Me.cbPeriodePembayaran.Location = New System.Drawing.Point(492, 104)
        Me.cbPeriodePembayaran.Name = "cbPeriodePembayaran"
        Me.cbPeriodePembayaran.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodePembayaran.Size = New System.Drawing.Size(138, 20)
        Me.cbPeriodePembayaran.TabIndex = 222
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(372, 108)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl5.TabIndex = 221
        Me.LabelControl5.Text = "Periode Pembayaran"
        '
        'cbSumberDana
        '
        Me.cbSumberDana.Location = New System.Drawing.Point(492, 81)
        Me.cbSumberDana.Name = "cbSumberDana"
        Me.cbSumberDana.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbSumberDana.Size = New System.Drawing.Size(138, 20)
        Me.cbSumberDana.TabIndex = 220
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(372, 85)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl4.TabIndex = 219
        Me.LabelControl4.Text = "Sumber Dana Pelunasan"
        '
        'cBagianYangDijamin
        '
        Me.cBagianYangDijamin.Location = New System.Drawing.Point(492, 36)
        Me.cBagianYangDijamin.Name = "cBagianYangDijamin"
        Me.cBagianYangDijamin.Size = New System.Drawing.Size(44, 20)
        Me.cBagianYangDijamin.TabIndex = 218
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(372, 40)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl21.TabIndex = 217
        Me.LabelControl21.Text = "Bagian Yg Dijamin"
        '
        'optAsuransi
        '
        Me.optAsuransi.Location = New System.Drawing.Point(492, 5)
        Me.optAsuransi.Name = "optAsuransi"
        Me.optAsuransi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tidak Ada"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. ...."), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. ....")})
        Me.optAsuransi.Size = New System.Drawing.Size(269, 29)
        Me.optAsuransi.TabIndex = 216
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(372, 13)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl20.TabIndex = 215
        Me.LabelControl20.Text = "Asuransi"
        '
        'LabelControl1
        '
        Me.LabelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl1.Location = New System.Drawing.Point(5, 210)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl1.TabIndex = 214
        Me.LabelControl1.Text = "Golongan Penjamin"
        '
        'cGolonganPenjamin
        '
        Me.cGolonganPenjamin.Location = New System.Drawing.Point(128, 206)
        Me.cGolonganPenjamin.Name = "cGolonganPenjamin"
        Me.cGolonganPenjamin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganPenjamin.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganPenjamin.TabIndex = 213
        '
        'cNamaGolonganPenjamin
        '
        Me.cNamaGolonganPenjamin.Enabled = False
        Me.cNamaGolonganPenjamin.Location = New System.Drawing.Point(178, 206)
        Me.cNamaGolonganPenjamin.Name = "cNamaGolonganPenjamin"
        Me.cNamaGolonganPenjamin.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganPenjamin.TabIndex = 212
        '
        'LabelControl7
        '
        Me.LabelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl7.Location = New System.Drawing.Point(5, 188)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl7.TabIndex = 211
        Me.LabelControl7.Text = "Account Officer"
        '
        'cAO
        '
        Me.cAO.Location = New System.Drawing.Point(128, 184)
        Me.cAO.Name = "cAO"
        Me.cAO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAO.Size = New System.Drawing.Size(44, 20)
        Me.cAO.TabIndex = 210
        '
        'cNamaAO
        '
        Me.cNamaAO.Enabled = False
        Me.cNamaAO.Location = New System.Drawing.Point(178, 184)
        Me.cNamaAO.Name = "cNamaAO"
        Me.cNamaAO.Size = New System.Drawing.Size(141, 20)
        Me.cNamaAO.TabIndex = 209
        '
        'cBendahara
        '
        Me.cBendahara.Location = New System.Drawing.Point(128, 161)
        Me.cBendahara.Name = "cBendahara"
        Me.cBendahara.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cBendahara.Size = New System.Drawing.Size(44, 20)
        Me.cBendahara.TabIndex = 208
        '
        'cNamaBendahara
        '
        Me.cNamaBendahara.Enabled = False
        Me.cNamaBendahara.Location = New System.Drawing.Point(178, 161)
        Me.cNamaBendahara.Name = "cNamaBendahara"
        Me.cNamaBendahara.Size = New System.Drawing.Size(141, 20)
        Me.cNamaBendahara.TabIndex = 207
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(5, 165)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl9.TabIndex = 206
        Me.LabelControl9.Text = "Bendahara"
        '
        'LabelControl8
        '
        Me.LabelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl8.Location = New System.Drawing.Point(5, 142)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl8.TabIndex = 202
        Me.LabelControl8.Text = "Wilayah"
        '
        'cWilayah
        '
        Me.cWilayah.Location = New System.Drawing.Point(128, 138)
        Me.cWilayah.Name = "cWilayah"
        Me.cWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cWilayah.Size = New System.Drawing.Size(44, 20)
        Me.cWilayah.TabIndex = 201
        '
        'cNamaWilayah
        '
        Me.cNamaWilayah.Enabled = False
        Me.cNamaWilayah.Location = New System.Drawing.Point(178, 138)
        Me.cNamaWilayah.Name = "cNamaWilayah"
        Me.cNamaWilayah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaWilayah.TabIndex = 200
        '
        'cNoSPK
        '
        Me.cNoSPK.Enabled = False
        Me.cNoSPK.Location = New System.Drawing.Point(128, 5)
        Me.cNoSPK.Name = "cNoSPK"
        Me.cNoSPK.Size = New System.Drawing.Size(191, 20)
        Me.cNoSPK.TabIndex = 199
        '
        'optAutoDebet
        '
        Me.optAutoDebet.Location = New System.Drawing.Point(492, 150)
        Me.optAutoDebet.Name = "optAutoDebet"
        Me.optAutoDebet.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optAutoDebet.Size = New System.Drawing.Size(138, 29)
        Me.optAutoDebet.TabIndex = 198
        '
        'cNamaSifatKredit
        '
        Me.cNamaSifatKredit.Enabled = False
        Me.cNamaSifatKredit.Location = New System.Drawing.Point(178, 48)
        Me.cNamaSifatKredit.Name = "cNamaSifatKredit"
        Me.cNamaSifatKredit.Size = New System.Drawing.Size(141, 20)
        Me.cNamaSifatKredit.TabIndex = 187
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(5, 52)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl6.TabIndex = 186
        Me.LabelControl6.Text = "Sifat Kredit"
        '
        'cSifatKredit
        '
        Me.cSifatKredit.Location = New System.Drawing.Point(128, 48)
        Me.cSifatKredit.Name = "cSifatKredit"
        Me.cSifatKredit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSifatKredit.Size = New System.Drawing.Size(44, 20)
        Me.cSifatKredit.TabIndex = 188
        '
        'cbKeterkaitan
        '
        Me.cbKeterkaitan.Location = New System.Drawing.Point(492, 59)
        Me.cbKeterkaitan.Name = "cbKeterkaitan"
        Me.cbKeterkaitan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbKeterkaitan.Size = New System.Drawing.Size(138, 20)
        Me.cbKeterkaitan.TabIndex = 180
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(372, 63)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl17.TabIndex = 177
        Me.LabelControl17.Text = "Keterkaitan"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl10.TabIndex = 175
        Me.LabelControl10.Text = "No. PK"
        '
        'cNamaJenisPenggunaan
        '
        Me.cNamaJenisPenggunaan.Enabled = False
        Me.cNamaJenisPenggunaan.Location = New System.Drawing.Point(178, 71)
        Me.cNamaJenisPenggunaan.Name = "cNamaJenisPenggunaan"
        Me.cNamaJenisPenggunaan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaJenisPenggunaan.TabIndex = 173
        '
        'LabelControl11
        '
        Me.LabelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl11.Location = New System.Drawing.Point(5, 119)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl11.TabIndex = 172
        Me.LabelControl11.Text = "Sektor Ekonomi"
        '
        'cSektorEkonomi
        '
        Me.cSektorEkonomi.Location = New System.Drawing.Point(128, 115)
        Me.cSektorEkonomi.Name = "cSektorEkonomi"
        Me.cSektorEkonomi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cSektorEkonomi.Size = New System.Drawing.Size(44, 20)
        Me.cSektorEkonomi.TabIndex = 171
        '
        'cNamaSektorEkonomi
        '
        Me.cNamaSektorEkonomi.Enabled = False
        Me.cNamaSektorEkonomi.Location = New System.Drawing.Point(178, 115)
        Me.cNamaSektorEkonomi.Name = "cNamaSektorEkonomi"
        Me.cNamaSektorEkonomi.Size = New System.Drawing.Size(141, 20)
        Me.cNamaSektorEkonomi.TabIndex = 170
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(5, 75)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl13.TabIndex = 169
        Me.LabelControl13.Text = "Jenis Penggunaan"
        '
        'cGolonganDebitur
        '
        Me.cGolonganDebitur.Location = New System.Drawing.Point(128, 93)
        Me.cGolonganDebitur.Name = "cGolonganDebitur"
        Me.cGolonganDebitur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganDebitur.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganDebitur.TabIndex = 168
        '
        'cNamaGolonganDebitur
        '
        Me.cNamaGolonganDebitur.Enabled = False
        Me.cNamaGolonganDebitur.Location = New System.Drawing.Point(178, 93)
        Me.cNamaGolonganDebitur.Name = "cNamaGolonganDebitur"
        Me.cNamaGolonganDebitur.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganDebitur.TabIndex = 167
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(5, 97)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl16.TabIndex = 166
        Me.LabelControl16.Text = "Golongan Debitur"
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(372, 158)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl18.TabIndex = 164
        Me.LabelControl18.Text = "Auto Debet Tabungan"
        '
        'cJenisPenggunaan
        '
        Me.cJenisPenggunaan.Location = New System.Drawing.Point(128, 71)
        Me.cJenisPenggunaan.Name = "cJenisPenggunaan"
        Me.cJenisPenggunaan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cJenisPenggunaan.Size = New System.Drawing.Size(44, 20)
        Me.cJenisPenggunaan.TabIndex = 174
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cPengajuan)
        Me.PanelControl3.Controls.Add(Me.nPlafondPengajuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.cNamaPengajuan)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(766, 54)
        Me.PanelControl3.TabIndex = 17
        '
        'cPengajuan
        '
        Me.cPengajuan.Location = New System.Drawing.Point(128, 5)
        Me.cPengajuan.Name = "cPengajuan"
        Me.cPengajuan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cPengajuan.Size = New System.Drawing.Size(139, 20)
        Me.cPengajuan.TabIndex = 172
        '
        'nPlafondPengajuan
        '
        Me.nPlafondPengajuan.Location = New System.Drawing.Point(128, 27)
        Me.nPlafondPengajuan.Name = "nPlafondPengajuan"
        Me.nPlafondPengajuan.Size = New System.Drawing.Size(139, 20)
        Me.nPlafondPengajuan.TabIndex = 117
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 30)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl2.TabIndex = 116
        Me.LabelControl2.Text = "Plafond Pengajuan"
        '
        'cNamaPengajuan
        '
        Me.cNamaPengajuan.Location = New System.Drawing.Point(273, 5)
        Me.cNamaPengajuan.Name = "cNamaPengajuan"
        Me.cNamaPengajuan.Size = New System.Drawing.Size(418, 20)
        Me.cNamaPengajuan.TabIndex = 85
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(4, 8)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl3.TabIndex = 84
        Me.LabelControl3.Text = "No. Pengajuan"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.PanelControl6)
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Controls.Add(Me.GridControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(772, 337)
        Me.XtraTabPage2.Text = "Jaminan"
        '
        'PanelControl6
        '
        Me.PanelControl6.Controls.Add(Me.nNilaiPengikatan)
        Me.PanelControl6.Controls.Add(Me.LabelControl26)
        Me.PanelControl6.Controls.Add(Me.nNilaiNJOP)
        Me.PanelControl6.Controls.Add(Me.LabelControl27)
        Me.PanelControl6.Controls.Add(Me.nNomor)
        Me.PanelControl6.Controls.Add(Me.nNilaiJaminan)
        Me.PanelControl6.Controls.Add(Me.LabelControl28)
        Me.PanelControl6.Controls.Add(Me.cKeteranganJaminan)
        Me.PanelControl6.Controls.Add(Me.LabelControl29)
        Me.PanelControl6.Controls.Add(Me.cJaminan)
        Me.PanelControl6.Controls.Add(Me.LabelControl30)
        Me.PanelControl6.Controls.Add(Me.LabelControl31)
        Me.PanelControl6.Location = New System.Drawing.Point(477, 3)
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(292, 331)
        Me.PanelControl6.TabIndex = 248
        '
        'nNilaiPengikatan
        '
        Me.nNilaiPengikatan.Location = New System.Drawing.Point(103, 117)
        Me.nNilaiPengikatan.Name = "nNilaiPengikatan"
        Me.nNilaiPengikatan.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiPengikatan.TabIndex = 190
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(8, 120)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl26.TabIndex = 189
        Me.LabelControl26.Text = "Nilai Pengikatan"
        '
        'nNilaiNJOP
        '
        Me.nNilaiNJOP.Location = New System.Drawing.Point(103, 94)
        Me.nNilaiNJOP.Name = "nNilaiNJOP"
        Me.nNilaiNJOP.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiNJOP.TabIndex = 187
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(8, 98)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl27.TabIndex = 186
        Me.LabelControl27.Text = "Nilai NJOP"
        '
        'nNomor
        '
        Me.nNomor.EditValue = "12"
        Me.nNomor.Location = New System.Drawing.Point(103, 5)
        Me.nNomor.Name = "nNomor"
        Me.nNomor.Properties.Mask.EditMask = "##.######"
        Me.nNomor.Properties.MaxLength = 2
        Me.nNomor.Size = New System.Drawing.Size(30, 20)
        Me.nNomor.TabIndex = 135
        '
        'nNilaiJaminan
        '
        Me.nNilaiJaminan.Location = New System.Drawing.Point(103, 72)
        Me.nNilaiJaminan.Name = "nNilaiJaminan"
        Me.nNilaiJaminan.Size = New System.Drawing.Size(105, 20)
        Me.nNilaiJaminan.TabIndex = 119
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(8, 76)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl28.TabIndex = 118
        Me.LabelControl28.Text = "Nilai Taksasi"
        '
        'cKeteranganJaminan
        '
        Me.cKeteranganJaminan.Location = New System.Drawing.Point(103, 50)
        Me.cKeteranganJaminan.Name = "cKeteranganJaminan"
        Me.cKeteranganJaminan.Size = New System.Drawing.Size(184, 20)
        Me.cKeteranganJaminan.TabIndex = 117
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(8, 54)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl29.TabIndex = 116
        Me.LabelControl29.Text = "Ket. Jaminan"
        '
        'cJaminan
        '
        Me.cJaminan.Location = New System.Drawing.Point(103, 27)
        Me.cJaminan.Name = "cJaminan"
        Me.cJaminan.Size = New System.Drawing.Size(30, 20)
        Me.cJaminan.TabIndex = 85
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(8, 31)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl30.TabIndex = 84
        Me.LabelControl30.Text = "Kode Jaminan"
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(8, 8)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl31.TabIndex = 81
        Me.LabelControl31.Text = "Nomor"
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(4, 160)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(468, 174)
        Me.GridControl2.TabIndex = 247
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(3, 3)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(468, 151)
        Me.GridControl1.TabIndex = 246
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.PanelControl7)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(772, 337)
        Me.XtraTabPage3.Text = "Realisasi"
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.cNamaCaraPerhitungan)
        Me.PanelControl7.Controls.Add(Me.LabelControl63)
        Me.PanelControl7.Controls.Add(Me.LabelControl62)
        Me.PanelControl7.Controls.Add(Me.LabelControl61)
        Me.PanelControl7.Controls.Add(Me.LabelControl60)
        Me.PanelControl7.Controls.Add(Me.LabelControl59)
        Me.PanelControl7.Controls.Add(Me.LabelControl58)
        Me.PanelControl7.Controls.Add(Me.nBunga)
        Me.PanelControl7.Controls.Add(Me.LabelControl56)
        Me.PanelControl7.Controls.Add(Me.LabelControl55)
        Me.PanelControl7.Controls.Add(Me.nAngsuran)
        Me.PanelControl7.Controls.Add(Me.nPembulatan)
        Me.PanelControl7.Controls.Add(Me.nTotal)
        Me.PanelControl7.Controls.Add(Me.nProvisi)
        Me.PanelControl7.Controls.Add(Me.LabelControl34)
        Me.PanelControl7.Controls.Add(Me.LabelControl52)
        Me.PanelControl7.Controls.Add(Me.LabelControl53)
        Me.PanelControl7.Controls.Add(Me.LabelControl54)
        Me.PanelControl7.Controls.Add(Me.cProvisi)
        Me.PanelControl7.Controls.Add(Me.nPersenProvisi)
        Me.PanelControl7.Controls.Add(Me.LabelControl33)
        Me.PanelControl7.Controls.Add(Me.nAsuransi)
        Me.PanelControl7.Controls.Add(Me.nMaterai)
        Me.PanelControl7.Controls.Add(Me.nNotaris)
        Me.PanelControl7.Controls.Add(Me.nAdministrasi)
        Me.PanelControl7.Controls.Add(Me.cAdministrasi)
        Me.PanelControl7.Controls.Add(Me.nPersenAdm)
        Me.PanelControl7.Controls.Add(Me.LabelControl32)
        Me.PanelControl7.Controls.Add(Me.optInstansi)
        Me.PanelControl7.Controls.Add(Me.optKaryawan)
        Me.PanelControl7.Controls.Add(Me.optDenda)
        Me.PanelControl7.Controls.Add(Me.optJenis)
        Me.PanelControl7.Controls.Add(Me.LabelControl35)
        Me.PanelControl7.Controls.Add(Me.LabelControl36)
        Me.PanelControl7.Controls.Add(Me.LabelControl37)
        Me.PanelControl7.Controls.Add(Me.LabelControl38)
        Me.PanelControl7.Controls.Add(Me.LabelControl39)
        Me.PanelControl7.Controls.Add(Me.LabelControl40)
        Me.PanelControl7.Controls.Add(Me.LabelControl41)
        Me.PanelControl7.Controls.Add(Me.nPlafond)
        Me.PanelControl7.Controls.Add(Me.LabelControl42)
        Me.PanelControl7.Controls.Add(Me.LabelControl43)
        Me.PanelControl7.Controls.Add(Me.LabelControl44)
        Me.PanelControl7.Controls.Add(Me.cCaraPerhitungan)
        Me.PanelControl7.Controls.Add(Me.nGraceBunga)
        Me.PanelControl7.Controls.Add(Me.nPersBunga)
        Me.PanelControl7.Controls.Add(Me.optProvisi)
        Me.PanelControl7.Controls.Add(Me.nPersBungaBerikutnya)
        Me.PanelControl7.Controls.Add(Me.LabelControl45)
        Me.PanelControl7.Controls.Add(Me.LabelControl46)
        Me.PanelControl7.Controls.Add(Me.LabelControl47)
        Me.PanelControl7.Controls.Add(Me.nBungaEfektif)
        Me.PanelControl7.Controls.Add(Me.LabelControl48)
        Me.PanelControl7.Controls.Add(Me.nGracePokok)
        Me.PanelControl7.Controls.Add(Me.LabelControl49)
        Me.PanelControl7.Controls.Add(Me.nLama)
        Me.PanelControl7.Controls.Add(Me.LabelControl50)
        Me.PanelControl7.Controls.Add(Me.LabelControl51)
        Me.PanelControl7.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(766, 331)
        Me.PanelControl7.TabIndex = 19
        '
        'cNamaCaraPerhitungan
        '
        Me.cNamaCaraPerhitungan.Enabled = False
        Me.cNamaCaraPerhitungan.Location = New System.Drawing.Point(227, 139)
        Me.cNamaCaraPerhitungan.Name = "cNamaCaraPerhitungan"
        Me.cNamaCaraPerhitungan.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.cNamaCaraPerhitungan.Properties.Appearance.Options.UseBackColor = True
        Me.cNamaCaraPerhitungan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.cNamaCaraPerhitungan.Size = New System.Drawing.Size(141, 18)
        Me.cNamaCaraPerhitungan.TabIndex = 259
        '
        'LabelControl63
        '
        Me.LabelControl63.Location = New System.Drawing.Point(227, 121)
        Me.LabelControl63.Name = "LabelControl63"
        Me.LabelControl63.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl63.TabIndex = 258
        Me.LabelControl63.Text = "bulan"
        '
        'LabelControl62
        '
        Me.LabelControl62.Location = New System.Drawing.Point(227, 98)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl62.TabIndex = 257
        Me.LabelControl62.Text = "bulan"
        '
        'LabelControl61
        '
        Me.LabelControl61.Location = New System.Drawing.Point(227, 76)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl61.TabIndex = 256
        Me.LabelControl61.Text = "bulan"
        '
        'LabelControl60
        '
        Me.LabelControl60.Location = New System.Drawing.Point(263, 54)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl60.TabIndex = 255
        Me.LabelControl60.Text = "%"
        '
        'LabelControl59
        '
        Me.LabelControl59.Location = New System.Drawing.Point(228, 31)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl59.TabIndex = 254
        Me.LabelControl59.Text = "% p.a"
        '
        'LabelControl58
        '
        Me.LabelControl58.Location = New System.Drawing.Point(228, 9)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl58.TabIndex = 253
        Me.LabelControl58.Text = "% p.a"
        '
        'nBunga
        '
        Me.nBunga.Enabled = False
        Me.nBunga.Location = New System.Drawing.Point(178, 184)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Size = New System.Drawing.Size(112, 20)
        Me.nBunga.TabIndex = 252
        '
        'LabelControl56
        '
        Me.LabelControl56.Location = New System.Drawing.Point(675, 215)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl56.TabIndex = 250
        Me.LabelControl56.Text = "%"
        '
        'LabelControl55
        '
        Me.LabelControl55.Location = New System.Drawing.Point(675, 71)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl55.TabIndex = 249
        Me.LabelControl55.Text = "%"
        '
        'nAngsuran
        '
        Me.nAngsuran.Enabled = False
        Me.nAngsuran.Location = New System.Drawing.Point(576, 300)
        Me.nAngsuran.Name = "nAngsuran"
        Me.nAngsuran.Size = New System.Drawing.Size(141, 20)
        Me.nAngsuran.TabIndex = 248
        '
        'nPembulatan
        '
        Me.nPembulatan.Enabled = False
        Me.nPembulatan.Location = New System.Drawing.Point(576, 278)
        Me.nPembulatan.Name = "nPembulatan"
        Me.nPembulatan.Size = New System.Drawing.Size(43, 20)
        Me.nPembulatan.TabIndex = 247
        '
        'nTotal
        '
        Me.nTotal.Enabled = False
        Me.nTotal.Location = New System.Drawing.Point(576, 256)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(141, 20)
        Me.nTotal.TabIndex = 246
        '
        'nProvisi
        '
        Me.nProvisi.Enabled = False
        Me.nProvisi.Location = New System.Drawing.Point(576, 233)
        Me.nProvisi.Name = "nProvisi"
        Me.nProvisi.Size = New System.Drawing.Size(141, 20)
        Me.nProvisi.TabIndex = 245
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(442, 304)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl34.TabIndex = 244
        Me.LabelControl34.Text = "Angsuran Per Bulan"
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(442, 282)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl52.TabIndex = 243
        Me.LabelControl52.Text = "Pembulatan"
        '
        'LabelControl53
        '
        Me.LabelControl53.Location = New System.Drawing.Point(442, 260)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl53.TabIndex = 242
        Me.LabelControl53.Text = "Total Realisasi"
        '
        'LabelControl54
        '
        Me.LabelControl54.Location = New System.Drawing.Point(442, 237)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl54.TabIndex = 241
        Me.LabelControl54.Text = "Nominal Provisi"
        '
        'cProvisi
        '
        Me.cProvisi.Location = New System.Drawing.Point(576, 211)
        Me.cProvisi.Name = "cProvisi"
        Me.cProvisi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cProvisi.Size = New System.Drawing.Size(44, 20)
        Me.cProvisi.TabIndex = 240
        '
        'nPersenProvisi
        '
        Me.nPersenProvisi.Enabled = False
        Me.nPersenProvisi.Location = New System.Drawing.Point(627, 211)
        Me.nPersenProvisi.Name = "nPersenProvisi"
        Me.nPersenProvisi.Size = New System.Drawing.Size(42, 20)
        Me.nPersenProvisi.TabIndex = 239
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(442, 215)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl33.TabIndex = 238
        Me.LabelControl33.Text = "Provisi"
        '
        'nAsuransi
        '
        Me.nAsuransi.Enabled = False
        Me.nAsuransi.Location = New System.Drawing.Point(576, 158)
        Me.nAsuransi.Name = "nAsuransi"
        Me.nAsuransi.Size = New System.Drawing.Size(141, 20)
        Me.nAsuransi.TabIndex = 237
        '
        'nMaterai
        '
        Me.nMaterai.Enabled = False
        Me.nMaterai.Location = New System.Drawing.Point(576, 136)
        Me.nMaterai.Name = "nMaterai"
        Me.nMaterai.Size = New System.Drawing.Size(141, 20)
        Me.nMaterai.TabIndex = 236
        '
        'nNotaris
        '
        Me.nNotaris.Enabled = False
        Me.nNotaris.Location = New System.Drawing.Point(576, 114)
        Me.nNotaris.Name = "nNotaris"
        Me.nNotaris.Size = New System.Drawing.Size(141, 20)
        Me.nNotaris.TabIndex = 235
        '
        'nAdministrasi
        '
        Me.nAdministrasi.Enabled = False
        Me.nAdministrasi.Location = New System.Drawing.Point(576, 91)
        Me.nAdministrasi.Name = "nAdministrasi"
        Me.nAdministrasi.Size = New System.Drawing.Size(141, 20)
        Me.nAdministrasi.TabIndex = 234
        '
        'cAdministrasi
        '
        Me.cAdministrasi.Location = New System.Drawing.Point(576, 68)
        Me.cAdministrasi.Name = "cAdministrasi"
        Me.cAdministrasi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAdministrasi.Size = New System.Drawing.Size(44, 20)
        Me.cAdministrasi.TabIndex = 233
        '
        'nPersenAdm
        '
        Me.nPersenAdm.Enabled = False
        Me.nPersenAdm.Location = New System.Drawing.Point(627, 68)
        Me.nPersenAdm.Name = "nPersenAdm"
        Me.nPersenAdm.Size = New System.Drawing.Size(42, 20)
        Me.nPersenAdm.TabIndex = 232
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(442, 72)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl32.TabIndex = 231
        Me.LabelControl32.Text = "Administrasi"
        '
        'optInstansi
        '
        Me.optInstansi.Location = New System.Drawing.Point(576, 36)
        Me.optInstansi.Name = "optInstansi"
        Me.optInstansi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optInstansi.Size = New System.Drawing.Size(138, 29)
        Me.optInstansi.TabIndex = 230
        '
        'optKaryawan
        '
        Me.optKaryawan.Location = New System.Drawing.Point(576, 4)
        Me.optKaryawan.Name = "optKaryawan"
        Me.optKaryawan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optKaryawan.Size = New System.Drawing.Size(138, 29)
        Me.optKaryawan.TabIndex = 229
        '
        'optDenda
        '
        Me.optDenda.Location = New System.Drawing.Point(178, 206)
        Me.optDenda.Name = "optDenda"
        Me.optDenda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optDenda.Size = New System.Drawing.Size(141, 29)
        Me.optDenda.TabIndex = 228
        '
        'optJenis
        '
        Me.optJenis.Location = New System.Drawing.Point(178, 238)
        Me.optJenis.Name = "optJenis"
        Me.optJenis.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Baru"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Ulangan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. Perpanjangan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&4. Tambahan")})
        Me.optJenis.Size = New System.Drawing.Size(223, 52)
        Me.optJenis.TabIndex = 227
        '
        'LabelControl35
        '
        Me.LabelControl35.Location = New System.Drawing.Point(442, 162)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl35.TabIndex = 223
        Me.LabelControl35.Text = "Asuransi"
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(442, 140)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl36.TabIndex = 221
        Me.LabelControl36.Text = "Materai"
        '
        'LabelControl37
        '
        Me.LabelControl37.Location = New System.Drawing.Point(442, 118)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl37.TabIndex = 219
        Me.LabelControl37.Text = "Notaris"
        '
        'LabelControl38
        '
        Me.LabelControl38.Location = New System.Drawing.Point(442, 38)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl38.TabIndex = 217
        Me.LabelControl38.Text = "Kredit Instansi"
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(442, 12)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl39.TabIndex = 215
        Me.LabelControl39.Text = "Kredit Karyawan"
        '
        'LabelControl40
        '
        Me.LabelControl40.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl40.Location = New System.Drawing.Point(5, 218)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl40.TabIndex = 214
        Me.LabelControl40.Text = "Hitung Denda"
        '
        'LabelControl41
        '
        Me.LabelControl41.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl41.Location = New System.Drawing.Point(5, 188)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl41.TabIndex = 211
        Me.LabelControl41.Text = "Jumlah Bunga"
        '
        'nPlafond
        '
        Me.nPlafond.Enabled = False
        Me.nPlafond.Location = New System.Drawing.Point(178, 161)
        Me.nPlafond.Name = "nPlafond"
        Me.nPlafond.Size = New System.Drawing.Size(141, 20)
        Me.nPlafond.TabIndex = 207
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(5, 165)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl42.TabIndex = 206
        Me.LabelControl42.Text = "Plafond"
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(5, 143)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl43.TabIndex = 203
        Me.LabelControl43.Text = "Cara Perhitungan"
        '
        'LabelControl44
        '
        Me.LabelControl44.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl44.Location = New System.Drawing.Point(5, 121)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(145, 13)
        Me.LabelControl44.TabIndex = 202
        Me.LabelControl44.Text = "Grace Period / Musiman Bunga"
        '
        'cCaraPerhitungan
        '
        Me.cCaraPerhitungan.Location = New System.Drawing.Point(178, 139)
        Me.cCaraPerhitungan.Name = "cCaraPerhitungan"
        Me.cCaraPerhitungan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cCaraPerhitungan.Size = New System.Drawing.Size(44, 20)
        Me.cCaraPerhitungan.TabIndex = 201
        '
        'nGraceBunga
        '
        Me.nGraceBunga.Enabled = False
        Me.nGraceBunga.Location = New System.Drawing.Point(178, 117)
        Me.nGraceBunga.Name = "nGraceBunga"
        Me.nGraceBunga.Size = New System.Drawing.Size(44, 20)
        Me.nGraceBunga.TabIndex = 200
        '
        'nPersBunga
        '
        Me.nPersBunga.Enabled = False
        Me.nPersBunga.Location = New System.Drawing.Point(178, 5)
        Me.nPersBunga.Name = "nPersBunga"
        Me.nPersBunga.Size = New System.Drawing.Size(44, 20)
        Me.nPersBunga.TabIndex = 199
        '
        'optProvisi
        '
        Me.optProvisi.Location = New System.Drawing.Point(576, 180)
        Me.optProvisi.Name = "optProvisi"
        Me.optProvisi.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optProvisi.Size = New System.Drawing.Size(141, 29)
        Me.optProvisi.TabIndex = 198
        '
        'nPersBungaBerikutnya
        '
        Me.nPersBungaBerikutnya.Enabled = False
        Me.nPersBungaBerikutnya.Location = New System.Drawing.Point(178, 27)
        Me.nPersBungaBerikutnya.Name = "nPersBungaBerikutnya"
        Me.nPersBungaBerikutnya.Size = New System.Drawing.Size(44, 20)
        Me.nPersBungaBerikutnya.TabIndex = 187
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(5, 31)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl45.TabIndex = 186
        Me.LabelControl45.Text = "Suku Bunga Tahun Berikutnya"
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(442, 95)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl46.TabIndex = 177
        Me.LabelControl46.Text = "Nominal Administrasi"
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl47.TabIndex = 175
        Me.LabelControl47.Text = "Suku Bunga"
        '
        'nBungaEfektif
        '
        Me.nBungaEfektif.Enabled = False
        Me.nBungaEfektif.Location = New System.Drawing.Point(178, 50)
        Me.nBungaEfektif.Name = "nBungaEfektif"
        Me.nBungaEfektif.Size = New System.Drawing.Size(79, 20)
        Me.nBungaEfektif.TabIndex = 173
        '
        'LabelControl48
        '
        Me.LabelControl48.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl48.Location = New System.Drawing.Point(5, 98)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl48.TabIndex = 172
        Me.LabelControl48.Text = "Grace Period / Musiman Pokok"
        '
        'nGracePokok
        '
        Me.nGracePokok.Enabled = False
        Me.nGracePokok.Location = New System.Drawing.Point(178, 94)
        Me.nGracePokok.Name = "nGracePokok"
        Me.nGracePokok.Size = New System.Drawing.Size(44, 20)
        Me.nGracePokok.TabIndex = 170
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(5, 54)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl49.TabIndex = 169
        Me.LabelControl49.Text = "Bunga Efektif"
        '
        'nLama
        '
        Me.nLama.Enabled = False
        Me.nLama.Location = New System.Drawing.Point(178, 72)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(44, 20)
        Me.nLama.TabIndex = 167
        '
        'LabelControl50
        '
        Me.LabelControl50.Location = New System.Drawing.Point(5, 76)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl50.TabIndex = 166
        Me.LabelControl50.Text = "Lama Kredit"
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(442, 188)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl51.TabIndex = 164
        Me.LabelControl51.Text = "Pendapatan Provisi Diakui"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.PanelControl8)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(772, 337)
        Me.XtraTabPage4.Text = "Jadwal"
        '
        'PanelControl8
        '
        Me.PanelControl8.Controls.Add(Me.GridControl3)
        Me.PanelControl8.Controls.Add(Me.cmdOK)
        Me.PanelControl8.Controls.Add(Me.nBungaPenyesuaian)
        Me.PanelControl8.Controls.Add(Me.nPokokPenyesuaian)
        Me.PanelControl8.Controls.Add(Me.LabelControl69)
        Me.PanelControl8.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl8.Name = "PanelControl8"
        Me.PanelControl8.Size = New System.Drawing.Size(766, 331)
        Me.PanelControl8.TabIndex = 249
        '
        'GridControl3
        '
        Me.GridControl3.Location = New System.Drawing.Point(5, 33)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(756, 293)
        Me.GridControl3.TabIndex = 247
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(360, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(27, 24)
        Me.cmdOK.TabIndex = 136
        Me.cmdOK.Visible = False
        '
        'nBungaPenyesuaian
        '
        Me.nBungaPenyesuaian.EditValue = "12"
        Me.nBungaPenyesuaian.Location = New System.Drawing.Point(132, 5)
        Me.nBungaPenyesuaian.Name = "nBungaPenyesuaian"
        Me.nBungaPenyesuaian.Properties.Mask.EditMask = "##.######"
        Me.nBungaPenyesuaian.Properties.MaxLength = 2
        Me.nBungaPenyesuaian.Size = New System.Drawing.Size(108, 20)
        Me.nBungaPenyesuaian.TabIndex = 135
        '
        'nPokokPenyesuaian
        '
        Me.nPokokPenyesuaian.Location = New System.Drawing.Point(246, 5)
        Me.nPokokPenyesuaian.Name = "nPokokPenyesuaian"
        Me.nPokokPenyesuaian.Size = New System.Drawing.Size(108, 20)
        Me.nPokokPenyesuaian.TabIndex = 119
        '
        'LabelControl69
        '
        Me.LabelControl69.Location = New System.Drawing.Point(8, 8)
        Me.LabelControl69.Name = "LabelControl69"
        Me.LabelControl69.Size = New System.Drawing.Size(118, 13)
        Me.LabelControl69.TabIndex = 81
        Me.LabelControl69.Text = "Penyesuaian Seluruhnya"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 515)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(777, 33)
        Me.PanelControl2.TabIndex = 192
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(695, 4)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(614, 4)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'trRealisasiKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(781, 550)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trRealisasiKredit"
        Me.Text = "Realisasi Kredit"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.cGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.PanelControl5.PerformLayout()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbJenisUsaha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodePembayaran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbSumberDana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBagianYangDijamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optAsuransi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganPenjamin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cBendahara.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaBendahara.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNoSPK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optAutoDebet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSifatKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSifatKredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaSektorEkonomi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganDebitur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cJenisPenggunaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cPengajuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafondPengajuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaPengajuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        Me.PanelControl6.PerformLayout()
        CType(Me.nNilaiPengikatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiNJOP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNomor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNilaiJaminan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganJaminan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cJaminan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.PanelControl7.PerformLayout()
        CType(Me.cNamaCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAngsuran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPembulatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPersenProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAsuransi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nMaterai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNotaris.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPersenAdm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optInstansi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKaryawan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optDenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJenis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nGraceBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPersBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPersBungaBerikutnya.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBungaEfektif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nGracePokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl8.ResumeLayout(False)
        Me.PanelControl8.PerformLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBungaPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPokokPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cCabang1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nPlafondPengajuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaPengajuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cPengajuan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganPenjamin As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganPenjamin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAO As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cBendahara As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaBendahara As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cWilayah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaWilayah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cNoSPK As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optAutoDebet As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents cNamaSifatKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSifatKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cbKeterkaitan As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaJenisPenggunaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cSektorEkonomi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaSektorEkonomi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDebitur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganDebitur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cJenisPenggunaan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamatNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbJenisUsaha As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbPeriodePembayaran As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbSumberDana As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cBagianYangDijamin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optAsuransi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nNilaiPengikatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNilaiNJOP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNomor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nNilaiJaminan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeteranganJaminan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cJaminan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents optKaryawan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optDenda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optJenis As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPlafond As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cCaraPerhitungan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nGraceBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPersBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents optProvisi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents nPersBungaBerikutnya As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBungaEfektif As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nGracePokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl63 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAngsuran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPembulatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nProvisi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cProvisi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nPersenProvisi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAsuransi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nMaterai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nNotaris As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nAdministrasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cAdministrasi As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents nPersenAdm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optInstansi As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents PanelControl8 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents nBungaPenyesuaian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nPokokPenyesuaian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl69 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaCaraPerhitungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cGolonganKredit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaGolonganKredit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
End Class
