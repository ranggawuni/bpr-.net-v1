﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trUpdateJadwalKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtData IsNot Nothing Then
                    dtData.Dispose()
                    dtData = Nothing
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trUpdateJadwalKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.nTotalPenyesuaian = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.nBungaPenyesuaian = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.nPokokPenyesuaian = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.nTotalAngsuran = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.nAngsuranBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.nAngsuranPokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.optJadwal = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.nPembulatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.nGracePeriodeBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.nGracePeriodePokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nSukuBungaBerikutnya = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lblCaraHitung = New DevExpress.XtraEditors.LabelControl()
        Me.cCaraPerhitungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nPlafond = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.nSukuBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.nTotalPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBungaPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPokokPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotalAngsuran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAngsuranBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAngsuranPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optJadwal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPembulatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nGracePeriodeBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nGracePeriodePokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSukuBungaBerikutnya.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSukuBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(1, 318)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(696, 152)
        Me.GridControl1.TabIndex = 32
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Location = New System.Drawing.Point(1, 476)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(696, 33)
        Me.PanelControl4.TabIndex = 31
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(530, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        Me.cmdSimpan.TabIndex = 10
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(611, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdOK)
        Me.PanelControl1.Controls.Add(Me.nTotalPenyesuaian)
        Me.PanelControl1.Controls.Add(Me.LabelControl22)
        Me.PanelControl1.Controls.Add(Me.nBungaPenyesuaian)
        Me.PanelControl1.Controls.Add(Me.LabelControl23)
        Me.PanelControl1.Controls.Add(Me.nPokokPenyesuaian)
        Me.PanelControl1.Controls.Add(Me.LabelControl24)
        Me.PanelControl1.Controls.Add(Me.nTotalAngsuran)
        Me.PanelControl1.Controls.Add(Me.LabelControl21)
        Me.PanelControl1.Controls.Add(Me.nAngsuranBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Controls.Add(Me.nAngsuranPokok)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.optJadwal)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.nPembulatan)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.nGracePeriodeBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.nGracePeriodePokok)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.nSukuBungaBerikutnya)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.nLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.lblCaraHitung)
        Me.PanelControl1.Controls.Add(Me.cCaraPerhitungan)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.nPlafond)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.nSukuBunga)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(1, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(696, 309)
        Me.PanelControl1.TabIndex = 30
        '
        'cmdOK
        '
        Me.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdOK.Location = New System.Drawing.Point(663, 265)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(23, 23)
        Me.cmdOK.TabIndex = 213
        '
        'nTotalPenyesuaian
        '
        Me.nTotalPenyesuaian.Enabled = False
        Me.nTotalPenyesuaian.Location = New System.Drawing.Point(555, 267)
        Me.nTotalPenyesuaian.Name = "nTotalPenyesuaian"
        Me.nTotalPenyesuaian.Size = New System.Drawing.Size(102, 20)
        Me.nTotalPenyesuaian.TabIndex = 212
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(451, 270)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl22.TabIndex = 211
        Me.LabelControl22.Text = "Total Penyesuaian"
        '
        'nBungaPenyesuaian
        '
        Me.nBungaPenyesuaian.Enabled = False
        Me.nBungaPenyesuaian.Location = New System.Drawing.Point(555, 244)
        Me.nBungaPenyesuaian.Name = "nBungaPenyesuaian"
        Me.nBungaPenyesuaian.Size = New System.Drawing.Size(102, 20)
        Me.nBungaPenyesuaian.TabIndex = 210
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(451, 247)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl23.TabIndex = 209
        Me.LabelControl23.Text = "Bunga Penyesuaian"
        '
        'nPokokPenyesuaian
        '
        Me.nPokokPenyesuaian.Enabled = False
        Me.nPokokPenyesuaian.Location = New System.Drawing.Point(555, 221)
        Me.nPokokPenyesuaian.Name = "nPokokPenyesuaian"
        Me.nPokokPenyesuaian.Size = New System.Drawing.Size(102, 20)
        Me.nPokokPenyesuaian.TabIndex = 208
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(451, 224)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl24.TabIndex = 207
        Me.LabelControl24.Text = "Pokok Penyesuaian"
        '
        'nTotalAngsuran
        '
        Me.nTotalAngsuran.Enabled = False
        Me.nTotalAngsuran.Location = New System.Drawing.Point(329, 267)
        Me.nTotalAngsuran.Name = "nTotalAngsuran"
        Me.nTotalAngsuran.Size = New System.Drawing.Size(102, 20)
        Me.nTotalAngsuran.TabIndex = 206
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(225, 270)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl21.TabIndex = 205
        Me.LabelControl21.Text = "Total Lama"
        '
        'nAngsuranBunga
        '
        Me.nAngsuranBunga.Enabled = False
        Me.nAngsuranBunga.Location = New System.Drawing.Point(329, 244)
        Me.nAngsuranBunga.Name = "nAngsuranBunga"
        Me.nAngsuranBunga.Size = New System.Drawing.Size(102, 20)
        Me.nAngsuranBunga.TabIndex = 204
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(225, 247)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl20.TabIndex = 203
        Me.LabelControl20.Text = "Bunga Lama"
        '
        'nAngsuranPokok
        '
        Me.nAngsuranPokok.Enabled = False
        Me.nAngsuranPokok.Location = New System.Drawing.Point(329, 221)
        Me.nAngsuranPokok.Name = "nAngsuranPokok"
        Me.nAngsuranPokok.Size = New System.Drawing.Size(102, 20)
        Me.nAngsuranPokok.TabIndex = 202
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(225, 224)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl19.TabIndex = 201
        Me.LabelControl19.Text = "Pokok Lama"
        '
        'optJadwal
        '
        Me.optJadwal.Location = New System.Drawing.Point(329, 189)
        Me.optJadwal.Name = "optJadwal"
        Me.optJadwal.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optJadwal.Size = New System.Drawing.Size(102, 29)
        Me.optJadwal.TabIndex = 200
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(225, 193)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl18.TabIndex = 199
        Me.LabelControl18.Text = "Jadwal Penyesuaian"
        '
        'nPembulatan
        '
        Me.nPembulatan.Enabled = False
        Me.nPembulatan.Location = New System.Drawing.Point(103, 282)
        Me.nPembulatan.Name = "nPembulatan"
        Me.nPembulatan.Size = New System.Drawing.Size(42, 20)
        Me.nPembulatan.TabIndex = 157
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(8, 286)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl17.TabIndex = 156
        Me.LabelControl17.Text = "Pembulatan"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(151, 263)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl15.TabIndex = 155
        Me.LabelControl15.Text = "%"
        '
        'nGracePeriodeBunga
        '
        Me.nGracePeriodeBunga.Enabled = False
        Me.nGracePeriodeBunga.Location = New System.Drawing.Point(103, 259)
        Me.nGracePeriodeBunga.Name = "nGracePeriodeBunga"
        Me.nGracePeriodeBunga.Size = New System.Drawing.Size(42, 20)
        Me.nGracePeriodeBunga.TabIndex = 154
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(8, 263)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl16.TabIndex = 153
        Me.LabelControl16.Text = "Skim Bunga"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(151, 240)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl11.TabIndex = 152
        Me.LabelControl11.Text = "%"
        '
        'nGracePeriodePokok
        '
        Me.nGracePeriodePokok.Enabled = False
        Me.nGracePeriodePokok.Location = New System.Drawing.Point(103, 236)
        Me.nGracePeriodePokok.Name = "nGracePeriodePokok"
        Me.nGracePeriodePokok.Size = New System.Drawing.Size(42, 20)
        Me.nGracePeriodePokok.TabIndex = 151
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(8, 240)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl13.TabIndex = 150
        Me.LabelControl13.Text = "Skim Pokok"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(151, 217)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl9.TabIndex = 149
        Me.LabelControl9.Text = "%"
        '
        'nSukuBungaBerikutnya
        '
        Me.nSukuBungaBerikutnya.Enabled = False
        Me.nSukuBungaBerikutnya.Location = New System.Drawing.Point(103, 213)
        Me.nSukuBungaBerikutnya.Name = "nSukuBungaBerikutnya"
        Me.nSukuBungaBerikutnya.Size = New System.Drawing.Size(42, 20)
        Me.nSukuBungaBerikutnya.TabIndex = 148
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(8, 217)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl10.TabIndex = 147
        Me.LabelControl10.Text = "SB Thn Berikutnya"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(151, 170)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(14, 13)
        Me.LabelControl7.TabIndex = 146
        Me.LabelControl7.Text = "Bln"
        '
        'nLama
        '
        Me.nLama.Enabled = False
        Me.nLama.Location = New System.Drawing.Point(103, 166)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(42, 20)
        Me.nLama.TabIndex = 145
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(8, 170)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl8.TabIndex = 144
        Me.LabelControl8.Text = "Lama"
        '
        'lblCaraHitung
        '
        Me.lblCaraHitung.Location = New System.Drawing.Point(151, 147)
        Me.lblCaraHitung.Name = "lblCaraHitung"
        Me.lblCaraHitung.Size = New System.Drawing.Size(57, 13)
        Me.lblCaraHitung.TabIndex = 143
        Me.lblCaraHitung.Text = "Cara Hitung"
        '
        'cCaraPerhitungan
        '
        Me.cCaraPerhitungan.Enabled = False
        Me.cCaraPerhitungan.Location = New System.Drawing.Point(103, 143)
        Me.cCaraPerhitungan.Name = "cCaraPerhitungan"
        Me.cCaraPerhitungan.Size = New System.Drawing.Size(42, 20)
        Me.cCaraPerhitungan.TabIndex = 142
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(8, 147)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl6.TabIndex = 141
        Me.LabelControl6.Text = "Cara Perhitungan"
        '
        'nPlafond
        '
        Me.nPlafond.Enabled = False
        Me.nPlafond.Location = New System.Drawing.Point(103, 120)
        Me.nPlafond.Name = "nPlafond"
        Me.nPlafond.Size = New System.Drawing.Size(131, 20)
        Me.nPlafond.TabIndex = 140
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(8, 123)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl4.TabIndex = 139
        Me.LabelControl4.Text = "Plafond"
        '
        'cKode
        '
        Me.cKode.Enabled = False
        Me.cKode.Location = New System.Drawing.Point(103, 74)
        Me.cKode.Name = "cKode"
        Me.cKode.Size = New System.Drawing.Size(82, 20)
        Me.cKode.TabIndex = 138
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 77)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl1.TabIndex = 137
        Me.LabelControl1.Text = "No. Register"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(151, 193)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl3.TabIndex = 136
        Me.LabelControl3.Text = "%"
        '
        'nSukuBunga
        '
        Me.nSukuBunga.Enabled = False
        Me.nSukuBunga.Location = New System.Drawing.Point(103, 189)
        Me.nSukuBunga.Name = "nSukuBunga"
        Me.nSukuBunga.Size = New System.Drawing.Size(42, 20)
        Me.nSukuBunga.TabIndex = 133
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 193)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl2.TabIndex = 132
        Me.LabelControl2.Text = "SB Tahun I"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(103, 5)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(131, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(103, 51)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(322, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 54)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 9)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(103, 28)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 31)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 100)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(82, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal Realisasi"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(103, 97)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'trUpdateJadwalKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 511)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trUpdateJadwalKredit"
        Me.Text = "Update Jadwal Kredit"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.nTotalPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBungaPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPokokPenyesuaian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotalAngsuran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAngsuranBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAngsuranPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optJadwal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPembulatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nGracePeriodeBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nGracePeriodePokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSukuBungaBerikutnya.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cCaraPerhitungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSukuBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents nPembulatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nGracePeriodeBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nGracePeriodePokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSukuBungaBerikutnya As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCaraHitung As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cCaraPerhitungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPlafond As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSukuBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents nTotalAngsuran As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAngsuranBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAngsuranPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optJadwal As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents nTotalPenyesuaian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBungaPenyesuaian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPokokPenyesuaian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
End Class
