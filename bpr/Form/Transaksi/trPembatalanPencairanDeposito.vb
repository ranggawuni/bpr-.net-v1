﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trPembatalanPencairanDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Private ReadOnly x As TypeDeposito

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nNominal.Text = ""
        nSukuBunga.Text = ""
        cRekeningTabungan.Text = ""
        cNamaNasabah.Text = ""
        nSaldoTabungan.Text = ""
        cKunci.Text = ""
    End Sub

    Private Sub trPembatalanPencairanDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trPembatalanPencairanDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(cKunci.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        cKunci.EnterMoveNextControl = True

        FormatTextBox(nNominal, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSukuBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nSaldoTabungan, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "status,nominal"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    If .Item("Status").ToString = "1" Then
                        GetMemory()
                    Else
                        MessageBox.Show("Pokok Deposito Belum Dicairkan, Tidak Bisa Dibatalkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    End If
                    If CDate(.Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub GetMemory()
        Const cField As String = "tgl,faktur"
        Dim cWhere As String = String.Format(" and d.tgl='{0}' and d.PencairanPlafond > 0 ", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
        dtData = objData.Browse(GetDSN, "MutasiDeposito", cField, "rekening", , cRekening.Text, cWhere)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                GetDeposito(cRekening.Text, dTgl.DateTime, x)
                cFaktur.Text = .Item("faktur").ToString
                cNama.Text = x.cNama
                cAlamat.Text = x.cAlamat
                nNominal.Text = x.nNominalSetoranDeposito.ToString
                nSukuBunga.Text = x.nSukuBunga.ToString
                cRekeningTabungan.Text = x.cRekeningTabungan
                cNamaNasabah.Text = x.cNamaNasabahTabungan
                nSaldoTabungan.Text = GetSaldoTabungan(cRekening.Text, dTgl.DateTime).ToString
            End With
        End If
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As New DataTable
        ValidSaving = True

        If cKunci.Text <> "PEMBATALAN PENCAIRAN DEPOSITO" Then
            MsgBox("Kata Kunci Salah, Transaksi Tidak Bisa di Lanjutkan ..!", vbExclamation)
            ValidSaving = False
            cKunci.Focus()
            cKunci.Text = ""
            Exit Function
        End If

        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
        cDataTable.Dispose()
    End Function

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        simpanData()
    End Sub

    Private Sub simpanData()
        If ValidSaving() Then
            DelPencairanDeposito(cRekening.Text, dTgl.DateTime)
            InitValue()
            cRekening.Focus()
        End If
        MessageBox.Show("Pembatalan selesai.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class