﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trHapusMutasiDeposito
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim nNominal As Double = 0

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        dTgl1.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        nSaldoAkhir.Text = ""
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "0,Faktur,Tgl,SetoranPlafond,PencairanPlafond,Bunga,Pajak,Pinalty,Kas,UserName,ID"
            Dim cWhere As String = String.Format(" and tgl >= '{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = String.Format(" {0} and tgl <= '{1}'", cWhere, formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "mutasideposito", cField, "rekening", , cRekening.Text, cWhere, "tgl,id")
            GridControl1.DataSource = cDataSet.Tables("mutasideposito")
            cDataSet.Dispose()
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "Tgl")
            GridColumnFormat(GridView1, "SetoranPlafond", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Bunga", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pajak", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Pinalty", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict2, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "Status")
            GridColumnFormat(GridView1, "UserName")
            GridColumnFormat(GridView1, "ID", , , 0)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub HapusMutasi()
        Dim nRow As DataRow
        Dim nStatus As eMutasiDeposito
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    DelMutasiTabungan(nRow(0).ToString, nRow(2).ToString, True)
                    If Val(nRow(3).ToString) <> 0 And nRow(8).ToString <> "Ppj" Then
                        nStatus = eMutasiDeposito.Setoran_Deposito
                    End If
                    If Val(nRow(3).ToString) = nNominal Then
                        cWhere = String.Format("rekening = '{0}'", cRekening.Text)
                        vaField = {"nominal", "status"}
                        vaValue = {0, ""}
                        objData.Edit(GetDSN, "deposito", cWhere, vaField, vaValue)
                    End If
                    DelMutasiDeposito(nRow(1).ToString, nStatus)
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trHapusMutasiDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trHapusMutasiDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , cmdHapus, cmdRefresh, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dTgl1.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdRefresh.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True

        FormatTextBox(nSaldoAkhir, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "d.Rekening,d.Nominal,r.nama,r.alamat"
            Dim vajoin() As Object = {"Left Join registernasabah r on r.kode = d.kode"}
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito d", cField, "d.rekening", , cRekening.Text, , , vajoin)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    nNominal = Val(.Item("Nominal"))
                    cNama.Text = .Item("nama").ToString
                    cAlamat.Text = .Item("alamat").ToString
                    nSaldoAkhir.Text = .Item("nominal").ToString
                    GetSQL()
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        GetSQL()
    End Sub

    Private Sub cmdHapus_Click(sender As Object, e As EventArgs) Handles cmdHapus.Click
        HapusMutasi()
    End Sub
End Class