﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trRegistrasiDeposito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trRegistrasiDeposito))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cCabang1 = New DevExpress.XtraEditors.TextEdit()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblNoRegister = New DevExpress.XtraEditors.LabelControl()
        Me.cKode = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdAktivasi = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.optSetoran = New DevExpress.XtraEditors.RadioGroup()
        Me.optPencairan = New DevExpress.XtraEditors.RadioGroup()
        Me.optPencairanBunga = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cmdHitungJadwal = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.dJthTmp = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposan = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.cmbKeterkaitan = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cAO = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaAO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cWilayah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cNamaWilayah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposito = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.nSaldoTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamatNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.cbJenisDeposito = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.optPajak = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.cNoBilyet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.cNasabah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cKeteranganNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.optDeposan = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.optSetoran.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPencairan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPencairanBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbJenisDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNoBilyet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cCabang1)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblNoRegister)
        Me.PanelControl1.Controls.Add(Me.cKode)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(405, 109)
        Me.PanelControl1.TabIndex = 16
        '
        'cCabang1
        '
        Me.cCabang1.EditValue = "12"
        Me.cCabang1.Location = New System.Drawing.Point(103, 5)
        Me.cCabang1.Name = "cCabang1"
        Me.cCabang1.Properties.Mask.EditMask = "##.######"
        Me.cCabang1.Properties.MaxLength = 2
        Me.cCabang1.Size = New System.Drawing.Size(30, 20)
        Me.cCabang1.TabIndex = 135
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(103, 83)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(8, 87)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(103, 57)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(294, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 60)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 31)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 34)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblNoRegister
        '
        Me.lblNoRegister.Location = New System.Drawing.Point(8, 8)
        Me.lblNoRegister.Name = "lblNoRegister"
        Me.lblNoRegister.Size = New System.Drawing.Size(60, 13)
        Me.lblNoRegister.TabIndex = 81
        Me.lblNoRegister.Text = "No. Register"
        '
        'cKode
        '
        Me.cKode.EditValue = "123456"
        Me.cKode.Location = New System.Drawing.Point(139, 5)
        Me.cKode.Name = "cKode"
        Me.cKode.Properties.Mask.EditMask = "##.######"
        Me.cKode.Properties.MaxLength = 6
        Me.cKode.Size = New System.Drawing.Size(46, 20)
        Me.cKode.TabIndex = 80
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdAktivasi)
        Me.PanelControl2.Controls.Add(Me.cmdKeluar)
        Me.PanelControl2.Controls.Add(Me.cmdSimpan)
        Me.PanelControl2.Controls.Add(Me.cmdHapus)
        Me.PanelControl2.Controls.Add(Me.cmdEdit)
        Me.PanelControl2.Controls.Add(Me.cmdAdd)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 471)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(809, 33)
        Me.PanelControl2.TabIndex = 15
        '
        'cmdAktivasi
        '
        Me.cmdAktivasi.Location = New System.Drawing.Point(7, 4)
        Me.cmdAktivasi.Name = "cmdAktivasi"
        Me.cmdAktivasi.Size = New System.Drawing.Size(27, 24)
        Me.cmdAktivasi.TabIndex = 12
        Me.cmdAktivasi.Visible = False
        '
        'cmdKeluar
        '
        Me.cmdKeluar.Location = New System.Drawing.Point(726, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.Image = CType(resources.GetObject("cmdSimpan.Image"), System.Drawing.Image)
        Me.cmdSimpan.Location = New System.Drawing.Point(645, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cmdHapus
        '
        Me.cmdHapus.Location = New System.Drawing.Point(191, 4)
        Me.cmdHapus.Name = "cmdHapus"
        Me.cmdHapus.Size = New System.Drawing.Size(70, 24)
        Me.cmdHapus.TabIndex = 2
        Me.cmdHapus.Text = "Hapus"
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(115, 4)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(70, 24)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(39, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(70, 24)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Tambah"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.optSetoran)
        Me.PanelControl3.Controls.Add(Me.optPencairan)
        Me.PanelControl3.Controls.Add(Me.optPencairanBunga)
        Me.PanelControl3.Controls.Add(Me.LabelControl1)
        Me.PanelControl3.Controls.Add(Me.cmdHitungJadwal)
        Me.PanelControl3.Controls.Add(Me.LabelControl7)
        Me.PanelControl3.Controls.Add(Me.dJthTmp)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.nLama)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Controls.Add(Me.cNamaGolonganDeposan)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.cGolonganDeposan)
        Me.PanelControl3.Controls.Add(Me.lblTglRegister)
        Me.PanelControl3.Controls.Add(Me.dTgl)
        Me.PanelControl3.Controls.Add(Me.cmbKeterkaitan)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.LabelControl17)
        Me.PanelControl3.Controls.Add(Me.cRekening)
        Me.PanelControl3.Controls.Add(Me.LabelControl12)
        Me.PanelControl3.Controls.Add(Me.cNamaGolonganDeposito)
        Me.PanelControl3.Controls.Add(Me.LabelControl11)
        Me.PanelControl3.Controls.Add(Me.cAO)
        Me.PanelControl3.Controls.Add(Me.cNamaAO)
        Me.PanelControl3.Controls.Add(Me.LabelControl10)
        Me.PanelControl3.Controls.Add(Me.cWilayah)
        Me.PanelControl3.Controls.Add(Me.cNamaWilayah)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.cGolonganDeposito)
        Me.PanelControl3.Location = New System.Drawing.Point(3, 117)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(405, 348)
        Me.PanelControl3.TabIndex = 17
        '
        'optSetoran
        '
        Me.optSetoran.Location = New System.Drawing.Point(128, 243)
        Me.optSetoran.Name = "optSetoran"
        Me.optSetoran.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. PB")})
        Me.optSetoran.Size = New System.Drawing.Size(269, 29)
        Me.optSetoran.TabIndex = 198
        '
        'optPencairan
        '
        Me.optPencairan.Location = New System.Drawing.Point(128, 277)
        Me.optPencairan.Name = "optPencairan"
        Me.optPencairan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. PB")})
        Me.optPencairan.Size = New System.Drawing.Size(269, 29)
        Me.optPencairan.TabIndex = 197
        '
        'optPencairanBunga
        '
        Me.optPencairanBunga.Location = New System.Drawing.Point(128, 312)
        Me.optPencairanBunga.Name = "optPencairanBunga"
        Me.optPencairanBunga.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. PB")})
        Me.optPencairanBunga.Size = New System.Drawing.Size(269, 29)
        Me.optPencairanBunga.TabIndex = 196
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 320)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl1.TabIndex = 195
        Me.LabelControl1.Text = "Sistem Pencairan Bunga"
        '
        'cmdHitungJadwal
        '
        Me.cmdHitungJadwal.Location = New System.Drawing.Point(216, 213)
        Me.cmdHitungJadwal.Name = "cmdHitungJadwal"
        Me.cmdHitungJadwal.Size = New System.Drawing.Size(103, 24)
        Me.cmdHitungJadwal.TabIndex = 194
        Me.cmdHitungJadwal.Text = "Hitung Jadwal"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 220)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl7.TabIndex = 193
        Me.LabelControl7.Text = "Tgl Jatuh Tempo"
        '
        'dJthTmp
        '
        Me.dJthTmp.EditValue = Nothing
        Me.dJthTmp.Location = New System.Drawing.Point(128, 217)
        Me.dJthTmp.Name = "dJthTmp"
        Me.dJthTmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJthTmp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJthTmp.Size = New System.Drawing.Size(82, 20)
        Me.dJthTmp.TabIndex = 192
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(178, 194)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl4.TabIndex = 191
        Me.LabelControl4.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.Location = New System.Drawing.Point(128, 191)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(44, 20)
        Me.nLama.TabIndex = 190
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 194)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl3.TabIndex = 189
        Me.LabelControl3.Text = "Jangka Waktu"
        '
        'cNamaGolonganDeposan
        '
        Me.cNamaGolonganDeposan.Enabled = False
        Me.cNamaGolonganDeposan.Location = New System.Drawing.Point(178, 61)
        Me.cNamaGolonganDeposan.Name = "cNamaGolonganDeposan"
        Me.cNamaGolonganDeposan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganDeposan.TabIndex = 187
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 64)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 186
        Me.LabelControl2.Text = "Gol. Deposan"
        '
        'cGolonganDeposan
        '
        Me.cGolonganDeposan.Location = New System.Drawing.Point(128, 61)
        Me.cGolonganDeposan.Name = "cGolonganDeposan"
        Me.cGolonganDeposan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganDeposan.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganDeposan.TabIndex = 188
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 38)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(72, 13)
        Me.lblTglRegister.TabIndex = 185
        Me.lblTglRegister.Text = "Tgl Pembukaan"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(128, 35)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 184
        '
        'cmbKeterkaitan
        '
        Me.cmbKeterkaitan.Location = New System.Drawing.Point(128, 165)
        Me.cmbKeterkaitan.Name = "cmbKeterkaitan"
        Me.cmbKeterkaitan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbKeterkaitan.Size = New System.Drawing.Size(138, 20)
        Me.cmbKeterkaitan.TabIndex = 180
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(8, 285)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl6.TabIndex = 178
        Me.LabelControl6.Text = "Sistem Pencairan Pokok"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(9, 168)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl17.TabIndex = 177
        Me.LabelControl17.Text = "Keterkaitan"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(128, 9)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Size = New System.Drawing.Size(105, 20)
        Me.cRekening.TabIndex = 176
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 12)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 175
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNamaGolonganDeposito
        '
        Me.cNamaGolonganDeposito.Enabled = False
        Me.cNamaGolonganDeposito.Location = New System.Drawing.Point(178, 87)
        Me.cNamaGolonganDeposito.Name = "cNamaGolonganDeposito"
        Me.cNamaGolonganDeposito.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganDeposito.TabIndex = 173
        '
        'LabelControl11
        '
        Me.LabelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl11.Location = New System.Drawing.Point(8, 142)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl11.TabIndex = 172
        Me.LabelControl11.Text = "AO"
        '
        'cAO
        '
        Me.cAO.Location = New System.Drawing.Point(128, 139)
        Me.cAO.Name = "cAO"
        Me.cAO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cAO.Size = New System.Drawing.Size(44, 20)
        Me.cAO.TabIndex = 171
        '
        'cNamaAO
        '
        Me.cNamaAO.Enabled = False
        Me.cNamaAO.Location = New System.Drawing.Point(178, 139)
        Me.cNamaAO.Name = "cNamaAO"
        Me.cNamaAO.Size = New System.Drawing.Size(141, 20)
        Me.cNamaAO.TabIndex = 170
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(8, 90)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl10.TabIndex = 169
        Me.LabelControl10.Text = "Gol. Deposito"
        '
        'cWilayah
        '
        Me.cWilayah.Location = New System.Drawing.Point(128, 113)
        Me.cWilayah.Name = "cWilayah"
        Me.cWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cWilayah.Size = New System.Drawing.Size(44, 20)
        Me.cWilayah.TabIndex = 168
        '
        'cNamaWilayah
        '
        Me.cNamaWilayah.Enabled = False
        Me.cNamaWilayah.Location = New System.Drawing.Point(178, 113)
        Me.cNamaWilayah.Name = "cNamaWilayah"
        Me.cNamaWilayah.Size = New System.Drawing.Size(141, 20)
        Me.cNamaWilayah.TabIndex = 167
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(8, 116)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl9.TabIndex = 166
        Me.LabelControl9.Text = "Wilayah"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(8, 251)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl5.TabIndex = 164
        Me.LabelControl5.Text = "Sistem Penyetoran"
        '
        'cGolonganDeposito
        '
        Me.cGolonganDeposito.Location = New System.Drawing.Point(128, 87)
        Me.cGolonganDeposito.Name = "cGolonganDeposito"
        Me.cGolonganDeposito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganDeposito.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganDeposito.TabIndex = 174
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.GridControl1)
        Me.PanelControl4.Controls.Add(Me.nSaldoTabungan)
        Me.PanelControl4.Controls.Add(Me.LabelControl19)
        Me.PanelControl4.Controls.Add(Me.cAlamatNasabah)
        Me.PanelControl4.Controls.Add(Me.LabelControl20)
        Me.PanelControl4.Controls.Add(Me.cNamaNasabah)
        Me.PanelControl4.Controls.Add(Me.LabelControl23)
        Me.PanelControl4.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl4.Controls.Add(Me.LabelControl13)
        Me.PanelControl4.Controls.Add(Me.LabelControl16)
        Me.PanelControl4.Controls.Add(Me.nBunga)
        Me.PanelControl4.Controls.Add(Me.LabelControl18)
        Me.PanelControl4.Controls.Add(Me.cbJenisDeposito)
        Me.PanelControl4.Controls.Add(Me.optPajak)
        Me.PanelControl4.Controls.Add(Me.LabelControl21)
        Me.PanelControl4.Controls.Add(Me.LabelControl22)
        Me.PanelControl4.Controls.Add(Me.cNoBilyet)
        Me.PanelControl4.Controls.Add(Me.LabelControl24)
        Me.PanelControl4.Controls.Add(Me.cNasabah)
        Me.PanelControl4.Controls.Add(Me.cKeteranganNasabah)
        Me.PanelControl4.Controls.Add(Me.LabelControl25)
        Me.PanelControl4.Controls.Add(Me.optDeposan)
        Me.PanelControl4.Controls.Add(Me.LabelControl27)
        Me.PanelControl4.Location = New System.Drawing.Point(414, 3)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(398, 462)
        Me.PanelControl4.TabIndex = 18
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(8, 283)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(382, 172)
        Me.GridControl1.TabIndex = 205
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.ViewCaption = "Jadwal Bunga"
        '
        'nSaldoTabungan
        '
        Me.nSaldoTabungan.Location = New System.Drawing.Point(103, 257)
        Me.nSaldoTabungan.Name = "nSaldoTabungan"
        Me.nSaldoTabungan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoTabungan.TabIndex = 204
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(8, 261)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl19.TabIndex = 203
        Me.LabelControl19.Text = "Saldo Tabungan"
        '
        'cAlamatNasabah
        '
        Me.cAlamatNasabah.Location = New System.Drawing.Point(103, 231)
        Me.cAlamatNasabah.Name = "cAlamatNasabah"
        Me.cAlamatNasabah.Size = New System.Drawing.Size(281, 20)
        Me.cAlamatNasabah.TabIndex = 202
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(8, 234)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl20.TabIndex = 201
        Me.LabelControl20.Text = "Alamat Nasabah"
        '
        'cNamaNasabah
        '
        Me.cNamaNasabah.Location = New System.Drawing.Point(103, 205)
        Me.cNamaNasabah.Name = "cNamaNasabah"
        Me.cNamaNasabah.Size = New System.Drawing.Size(227, 20)
        Me.cNamaNasabah.TabIndex = 200
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(8, 208)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl23.TabIndex = 199
        Me.LabelControl23.Text = "Nama Nasabah"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(103, 179)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 198
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(8, 182)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl13.TabIndex = 197
        Me.LabelControl13.Text = "Rek. Tabungan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(154, 69)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl16.TabIndex = 191
        Me.LabelControl16.Text = "%"
        '
        'nBunga
        '
        Me.nBunga.Location = New System.Drawing.Point(104, 66)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Size = New System.Drawing.Size(44, 20)
        Me.nBunga.TabIndex = 190
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(9, 69)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl18.TabIndex = 189
        Me.LabelControl18.Text = "Suku Bunga"
        '
        'cbJenisDeposito
        '
        Me.cbJenisDeposito.Location = New System.Drawing.Point(104, 40)
        Me.cbJenisDeposito.Name = "cbJenisDeposito"
        Me.cbJenisDeposito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbJenisDeposito.Size = New System.Drawing.Size(138, 20)
        Me.cbJenisDeposito.TabIndex = 180
        '
        'optPajak
        '
        Me.optPajak.Location = New System.Drawing.Point(104, 144)
        Me.optPajak.Name = "optPajak"
        Me.optPajak.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya")})
        Me.optPajak.Size = New System.Drawing.Size(177, 29)
        Me.optPajak.TabIndex = 179
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(9, 151)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl21.TabIndex = 178
        Me.LabelControl21.Text = "Hitung Pajak"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(9, 43)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl22.TabIndex = 177
        Me.LabelControl22.Text = "Jenis Deposito"
        '
        'cNoBilyet
        '
        Me.cNoBilyet.Enabled = False
        Me.cNoBilyet.Location = New System.Drawing.Point(103, 92)
        Me.cNoBilyet.Name = "cNoBilyet"
        Me.cNoBilyet.Size = New System.Drawing.Size(73, 20)
        Me.cNoBilyet.TabIndex = 173
        '
        'LabelControl24
        '
        Me.LabelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl24.Location = New System.Drawing.Point(9, 121)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl24.TabIndex = 172
        Me.LabelControl24.Text = "Nasabah Kantor"
        '
        'cNasabah
        '
        Me.cNasabah.Location = New System.Drawing.Point(104, 118)
        Me.cNasabah.Name = "cNasabah"
        Me.cNasabah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cNasabah.Size = New System.Drawing.Size(44, 20)
        Me.cNasabah.TabIndex = 171
        '
        'cKeteranganNasabah
        '
        Me.cKeteranganNasabah.Enabled = False
        Me.cKeteranganNasabah.Location = New System.Drawing.Point(154, 118)
        Me.cKeteranganNasabah.Name = "cKeteranganNasabah"
        Me.cKeteranganNasabah.Size = New System.Drawing.Size(141, 20)
        Me.cKeteranganNasabah.TabIndex = 170
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(8, 95)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl25.TabIndex = 169
        Me.LabelControl25.Text = "Nomor Bilyet"
        '
        'optDeposan
        '
        Me.optDeposan.Location = New System.Drawing.Point(103, 5)
        Me.optDeposan.Name = "optDeposan"
        Me.optDeposan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Non Instansi"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Instansi")})
        Me.optDeposan.Size = New System.Drawing.Size(177, 29)
        Me.optDeposan.TabIndex = 165
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(8, 13)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl27.TabIndex = 164
        Me.LabelControl27.Text = "Jenis Deposan"
        '
        'trRegistrasiDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 508)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "trRegistrasiDeposito"
        Me.Text = "Pembukaan Deposito"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cCabang1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.optSetoran.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPencairan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPencairanBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbKeterkaitan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaAO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbJenisDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNoBilyet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cCabang1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNoRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdAktivasi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmbKeterkaitan As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAO As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaAO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cWilayah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cNamaWilayah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposito As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents optPencairanBunga As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmdHitungJadwal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJthTmp As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbJenisDeposito As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents optPajak As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNoBilyet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNasabah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cKeteranganNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optDeposan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamatNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optSetoran As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents optPencairan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
End Class
