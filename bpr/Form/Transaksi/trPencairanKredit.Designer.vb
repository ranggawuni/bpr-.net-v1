﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trPencairanKredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trPencairanKredit))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cNamaNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nProvisi = New DevExpress.XtraEditors.TextEdit()
        Me.nMaterai = New DevExpress.XtraEditors.TextEdit()
        Me.optKas = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.nAdministrasi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.nPlafond = New DevExpress.XtraEditors.TextEdit()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nAsuransi = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.nNotaris = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nProvisi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nMaterai.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nAsuransi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNotaris.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(517, 118)
        Me.PanelControl1.TabIndex = 17
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 9)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(106, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 87)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 90)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 13)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 61)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 64)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 38)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 35)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 444)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(517, 33)
        Me.PanelControl4.TabIndex = 21
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(434, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(353, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cNamaNasabah)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.nProvisi)
        Me.PanelControl3.Controls.Add(Me.nMaterai)
        Me.PanelControl3.Controls.Add(Me.optKas)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.LabelControl27)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Controls.Add(Me.nAdministrasi)
        Me.PanelControl3.Controls.Add(Me.LabelControl1)
        Me.PanelControl3.Controls.Add(Me.nPlafond)
        Me.PanelControl3.Controls.Add(Me.cFaktur)
        Me.PanelControl3.Controls.Add(Me.LabelControl15)
        Me.PanelControl3.Controls.Add(Me.nTotal)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.nAsuransi)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.nNotaris)
        Me.PanelControl3.Controls.Add(Me.LabelControl7)
        Me.PanelControl3.Controls.Add(Me.LabelControl8)
        Me.PanelControl3.Location = New System.Drawing.Point(3, 127)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(517, 311)
        Me.PanelControl3.TabIndex = 20
        '
        'cNamaNasabah
        '
        Me.cNamaNasabah.Location = New System.Drawing.Point(104, 245)
        Me.cNamaNasabah.Name = "cNamaNasabah"
        Me.cNamaNasabah.Size = New System.Drawing.Size(227, 20)
        Me.cNamaNasabah.TabIndex = 204
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 249)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl2.TabIndex = 203
        Me.LabelControl2.Text = "Atas Nama"
        '
        'nProvisi
        '
        Me.nProvisi.Enabled = False
        Me.nProvisi.Location = New System.Drawing.Point(105, 167)
        Me.nProvisi.Name = "nProvisi"
        Me.nProvisi.Size = New System.Drawing.Size(105, 20)
        Me.nProvisi.TabIndex = 202
        '
        'nMaterai
        '
        Me.nMaterai.Enabled = False
        Me.nMaterai.Location = New System.Drawing.Point(104, 115)
        Me.nMaterai.Name = "nMaterai"
        Me.nMaterai.Size = New System.Drawing.Size(105, 20)
        Me.nMaterai.TabIndex = 201
        '
        'optKas
        '
        Me.optKas.Location = New System.Drawing.Point(105, 271)
        Me.optKas.Name = "optKas"
        Me.optKas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&PB")})
        Me.optKas.Size = New System.Drawing.Size(226, 29)
        Me.optKas.TabIndex = 200
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(10, 279)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl4.TabIndex = 199
        Me.LabelControl4.Text = "Cara Pencairan"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(10, 170)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl27.TabIndex = 166
        Me.LabelControl27.Text = "Provisi"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(10, 118)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl3.TabIndex = 139
        Me.LabelControl3.Text = "Materai"
        '
        'nAdministrasi
        '
        Me.nAdministrasi.Enabled = False
        Me.nAdministrasi.Location = New System.Drawing.Point(105, 63)
        Me.nAdministrasi.Name = "nAdministrasi"
        Me.nAdministrasi.Properties.Mask.EditMask = "n"
        Me.nAdministrasi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nAdministrasi.Size = New System.Drawing.Size(104, 20)
        Me.nAdministrasi.TabIndex = 136
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 66)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl1.TabIndex = 134
        Me.LabelControl1.Text = "Administrasi"
        '
        'nPlafond
        '
        Me.nPlafond.Enabled = False
        Me.nPlafond.Location = New System.Drawing.Point(105, 37)
        Me.nPlafond.Name = "nPlafond"
        Me.nPlafond.Properties.Mask.EditMask = "n"
        Me.nPlafond.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nPlafond.Size = New System.Drawing.Size(104, 20)
        Me.nPlafond.TabIndex = 133
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(105, 11)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(157, 20)
        Me.cFaktur.TabIndex = 132
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(10, 15)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 131
        Me.LabelControl15.Text = "No. Faktur"
        '
        'nTotal
        '
        Me.nTotal.Location = New System.Drawing.Point(105, 193)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(105, 20)
        Me.nTotal.TabIndex = 130
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(10, 196)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl9.TabIndex = 129
        Me.LabelControl9.Text = "Pencairan"
        '
        'nAsuransi
        '
        Me.nAsuransi.Enabled = False
        Me.nAsuransi.Location = New System.Drawing.Point(104, 141)
        Me.nAsuransi.Name = "nAsuransi"
        Me.nAsuransi.Size = New System.Drawing.Size(105, 20)
        Me.nAsuransi.TabIndex = 127
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 145)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 126
        Me.LabelControl5.Text = "Asuransi"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(104, 219)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 125
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 223)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl6.TabIndex = 124
        Me.LabelControl6.Text = "Rek. Tabungan"
        '
        'nNotaris
        '
        Me.nNotaris.Enabled = False
        Me.nNotaris.Location = New System.Drawing.Point(105, 89)
        Me.nNotaris.Name = "nNotaris"
        Me.nNotaris.Properties.Mask.EditMask = "n"
        Me.nNotaris.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nNotaris.Size = New System.Drawing.Size(104, 20)
        Me.nNotaris.TabIndex = 123
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(10, 93)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl7.TabIndex = 122
        Me.LabelControl7.Text = "Notaris"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(10, 40)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl8.TabIndex = 120
        Me.LabelControl8.Text = "Plafond"
        '
        'trPencairanKredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 478)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trPencairanKredit"
        Me.Text = "Pencairan Kredit"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nProvisi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nMaterai.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAdministrasi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPlafond.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nAsuransi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNotaris.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents optKas As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAdministrasi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPlafond As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nAsuransi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nNotaris As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nMaterai As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cNamaNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nProvisi As DevExpress.XtraEditors.TextEdit
End Class
