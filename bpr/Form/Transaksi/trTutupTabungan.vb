﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trTutupTabungan
    ReadOnly objData As New data()
    Dim dTglTemp As Date = #1/1/1900#
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        cFaktur.Text = ""
        nSaldoAwal.Text = ""
        nBunga.Text = ""
        nPajak.Text = ""
        nAdministrasi.Text = ""
        nPenarikanTunai.Text = ""
    End Sub

    Private Sub trTutupTabungan_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("tabungan t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trTutupTabungan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(nBunga.TabIndex, n)
        SetTabIndex(nPajak.TabIndex, n)
        SetTabIndex(nAdministrasi.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        cRekening.EnterMoveNextControl = True
        dTgl.EnterMoveNextControl = True
        nBunga.EnterMoveNextControl = True
        nPajak.EnterMoveNextControl = True
        nAdministrasi.EnterMoveNextControl = True

        FormatTextBox(nSaldoAwal, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nBunga, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPajak, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nAdministrasi, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nPenarikanTunai, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "close,tgl"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "tabungan", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    If .Item("close").ToString = "1" Then
                        MessageBox.Show("Rekening sudah ditutup. Lakukan proses PEMBATALAN PENUTUPAN REKENING TABUNGAN terlebih dahulu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Text = ""
                        cRekening.EditValue = ""
                        cRekening.Focus()
                        Exit Sub
                    Else
                        getmemory()
                    End If
                    If CDate(.Item("tgl").ToString) > CDate(dTgl.Text) Then
                        MessageBox.Show("Tanggal transaksi melebihi tanggal pembukaan rekening.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cRekening.Focus()
                        Exit Sub
                    End If
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub GetMemory()
        Const nTotalBunga As Double = 0
        Const nTotalPajak As Double = 0

        cFaktur.Text = GetLastFaktur(eFaktur.fkt_MutasiTabungan, dTgl.DateTime, , False)
        nSaldoAwal.Text = GetSaldoTabungan(cRekening.Text, dTgl.DateTime).ToString

        ' Masukkan Sifat Tabungan
        Dim cField As String = "t.Rekening,t.Kode,t.GolonganTabungan,r.Nama as NamaNasabah,r.Alamat as AlamatNasabah,"
        cField = cField & "g.Keterangan as NamaGolonganTabungan,g.SaldoMinimum,g.SetoranMinimum,g.AdministrasiTutup"
        Dim vaJoin() As Object = {"Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan", _
                                  "Left Join RegisterNasabah r on t.Kode = r.Kode"}
        dtData = objData.Browse(GetDSN, "Tabungan t", cField, "t.Rekening", , cRekening.Text, , , vaJoin)
        If dtData.Rows.Count > 0 Then
            With dtData.Rows(0)
                cNama.Text = .Item("NamaNasabah").ToString
                cAlamat.Text = .Item("AlamatNasabah").ToString
                ' Edit Tanggal 21-08-2013 ( Rangga )
                ' untuk sementara nilai defaultnya 0 karena disamakan dengan program lama.
                nAdministrasi.Text = "0" 'dbdata!AdministrasiTutup
                '    GetBungaTabungan dbData!Rekening, dTgl.value, nTotalBunga, nTotalPajak, dbData!GolonganTabungan, dbData!Kode
                nBunga.Text = nTotalBunga.ToString
                nPajak.Text = nTotalPajak.ToString
            End With
        End If
        SumPenarikan()
    End Sub

    Private Sub SumPenarikan()
        Dim nJumlah As Double = Val(nSaldoAwal.Text) + Val(nBunga.Text) - Val(nPajak.Text) - Val(nAdministrasi.Text)
        nPenarikanTunai.Text = nJumlah.ToString
    End Sub

    Private Function ValidSaving() As Boolean
        Dim cDataTable As New DataTable
        ValidSaving = True
        If Len(cRekening.Text) <> 12 Then
            MessageBox.Show("Nomor Rekening Tidak Valid. Ulangi Pengisian.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If

        cDataTable = objData.Browse(GetDSN, "tabungan", "close", "rekening", , cRekening.Text)
        If cDataTable.Rows.Count > 0 Then
            If cDataTable.Rows(0).Item("close").ToString = "1" Then
                MessageBox.Show("Rekening tabungan sudah ditutup. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ValidSaving = False
                cRekening.Focus()
                Exit Function
            End If
        Else
            MessageBox.Show("Rekening tabungan tidak ditemukan. Transaksi tidak bisa dilanjutkan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ValidSaving = False
            cRekening.Focus()
            Exit Function
        End If
        cDataTable.Dispose()
    End Function

    Private Sub dTgl_DateTimeChanged(sender As Object, e As EventArgs) Handles dTgl.DateTimeChanged
        If lFormLoad Then
            If Not checkTglTransaksi(dTgl.DateTime) Then
                dTgl.Focus()
            End If
            dTglTemp = dTgl.DateTime
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        simpanData()
    End Sub

    Private Sub simpanData()
        Dim cKeterangan As String
        Dim cKodeCabang As String

        If ValidSaving() Then
            If MessageBox.Show("Proses Dilanjutkan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = vbYes Then
                cFaktur.Text = GetLastFaktur(eFaktur.fkt_MutasiTabungan, dTgl.DateTime, , True)
                cKodeCabang = aCfg(eCfg.msKodeCabang).ToString
                DelMutasiTabungan(cFaktur.Text)

                ' Bunga Tabungan
                If Val(nBunga.Text) > 0 Then
                    '        cFakturBunga = cFaktur.Text
                    '        If dTgl.value > "06-09-2013" Then
                    '          cFakturBunga = GetLastFaktur(fkt_MutasiTabungan, dTgl.value, , True)
                    '        End If
                    UpdMutasiTabungan(cKodeCabang, aCfg(eCfg.msKodeBunga).ToString, cFaktur.Text, dTgl.DateTime, cRekening.Text, _
                                      Val(nBunga.Text), False, "Bunga Tabungan", True)
                End If

                ' pajak Bunga Tabungan
                If Val(nPajak.Text) > 0 Then
                    UpdMutasiTabungan(cKodeCabang, aCfg(eCfg.msKodePajakBunga).ToString, cFaktur.Text, dTgl.DateTime, cRekening.Text, Val(nPajak.Text), _
                                      False, "Pajak Bunga", , , , , , , , False)
                End If

                ' Administrasi Tutup Tabungan
                If Val(nAdministrasi.Text) > 0 Then
                    cKeterangan = String.Format("Adm. Tutup Rek Tab [{0}] an. {1}", cRekening.Text, cNama.Text)
                    '        UpdRekTutupTabungan nAdministrasi.value, dTgl.value, cKeterangan, cFaktur.Text
                    UpdMutasiTabungan(cKodeCabang, aCfg(eCfg.msKodePenutupanTabungan).ToString, cFaktur.Text, dTgl.DateTime, cRekening.Text, _
                                      Val(nAdministrasi.Text), , cKeterangan, , , , , , , , False)
                End If

                ' Edit Tabungan
                Dim vaField() As Object = {"Close", "TglPenutupan"}
                Dim vaValue() As Object = {"1", dTgl.EditValue}
                objData.Edit(GetDSN, "Tabungan", String.Format("Rekening = '{0}'", cRekening.Text), vaField, vaValue)

                ' Tutup tabungan ( saldo ditarik tunai )
                cKeterangan = String.Format("Tutup Rek Tab [{0}] an. {1}", cRekening.Text, cNama.Text)
                UpdMutasiTabungan(cKodeCabang, aCfg(eCfg.msKodePenarikanTunai).ToString, cFaktur.Text, dTgl.DateTime, cRekening.Text, _
                                  Val(nPenarikanTunai.Text), , cKeterangan, , , , , , , , False)

                If MessageBox.Show("Cetak validasi tabungan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
                    'CetakValidasiTabungan(cFaktur.Text, dTgl.DateTime, SNow, cRekening.Text, cNama.Text, "11", "Penutupan Rekening", "D", Val(nPenarikanTunai.Text))
                End If

                cFaktur.Text = GetLastFaktur(eFaktur.fkt_MutasiTabungan, dTgl.DateTime, , True)

                MsgBox("Rekening Sudah Di Tutup ...", vbInformation)
                InitValue()
            End If
        End If
    End Sub
End Class