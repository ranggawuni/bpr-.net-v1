﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls

Public Class trPembelianAktiva
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dtJadwal As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub InitValue()
        dTanggal.DateTime = Date.Today
        cKode.Text = ""
        cNama.Text = ""
        dTanggal.DateTime = Now.Date
        nBerjalan.Text = "0"
        optJenis.SelectedIndex = 1
        nUnit.Text = "0"
        cGolonganAktiva.EditValue = ""
        cNamaGolonganAktiva.Text = ""
        nTarif.Text = "0"
        nHargaPerolehan.Text = "0"
        nLama.Text = "0"
        dAkhir.DateTime = Date.Today
        nNilaiResidu.Text = "0"
        nNilaiPenyusutan.Text = "0"
        nPenyusutanPerBulan.Text = "0"
        dTglKonversi.DateTime = Date.Today
        nJumlahKonversi.Text = "0"
        nSisaKonversi.Text = "0"
        nSaran.Text = "0"
        cRekeningBiayaPenyusutan.EditValue = ""
        cNamaRekeningBiayaPenyusutan.Text = ""
        cRekeningAkumulasiPenyusutan.EditValue = ""
        cNamaRekeningAkumulasiPenyusutan.Text = ""
    End Sub

    Private Sub DeleteData()
        If MessageBox.Show("Data Benar-benar akan Dihapus", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes Then
            objData.Delete(GetDSN, "aktiva", "kode", , cKode.Text)
            MessageBox.Show("Data Sudah Dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        GetEdit(False)
        InitValue()
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cKode.Text, "Kode harus diisi. Ulangi pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(nLama.Text, "Lama penyusutan harus diisi. Ulangi pengisian.") Then
            Return False
            nLama.Focus()
            Exit Function
        End If
    End Function

    Private Sub HitungJadwal()
        If optJenis.SelectedIndex = 1 Then
            Dim cCaraPerhitungan As String = GetOpt(optJenis)
            nPenyusutanPerBulan.Text = GetPenyusutan(CDbl(nHargaPerolehan.Text), CDbl(nNilaiResidu.Text), CInt(nLama.Text), 1, CSng(nTarif.Text), cCaraPerhitungan).ToString
        End If
        GetJadwal()
    End Sub

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim cJenisPenyusutan As String = GetOpt(optJenis)
                    Dim vaField() As Object = {"Kode", "Nama", "TglPerolehan", "Unit",
                                               "JenisPenyusutan", "Golongan", "Lama", "HargaPerolehan",
                                               "Residu", "RekPenyusutan", "RekBiayaPenyusutan", "tglkonversi",
                                               "jumlahkonversi", "Tarif", "Penyusutan"}
                    Dim vaValue() As Object = {cKode.Text, cNama.Text, dTanggal.DateTime, CDbl(nUnit.Text),
                                               cJenisPenyusutan, cGolonganAktiva.Text, CDbl(nLama.Text), CDbl(nHargaPerolehan.Text),
                                               CDbl(nNilaiResidu.Text), cRekeningAkumulasiPenyusutan.Text, cRekeningBiayaPenyusutan.Text, dTglKonversi.DateTime,
                                               CDbl(nJumlahKonversi.Text), CDbl(nTarif.Text), CDbl(nPenyusutanPerBulan.Text)}
                    objData.Update(GetDSN, "Aktiva", String.Format("Kode ='{0}'", cKode.Text), vaField, vaValue)
                    UpdateJadwal()
                Else
                    cNama.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub UpdateJadwal()
        Dim cWhere As String = String.Format(" and TglPerolehan = '{0}'", formatValue(dTanggal.DateTime, formatType.yyyy_MM_dd))
        objData.Delete(GetDSN, "JadwalPenyusutan", "Kode", , cKode.Text, cWhere)
        Dim vaField() As Object = {"Kode", "TglPerolehan", "Periode", "Jumlah", "NilaiBuku"}
        Dim vaValue() As Object = Nothing
        For n As Integer = 1 To dtJadwal.Rows.Count - 1
            With dtJadwal.Rows(n)
                vaValue = {cKode.Text, dTanggal.DateTime, formatValue(.Item(0), formatType.yyyy_MM_dd), CDbl(.Item(1)), CDbl(.Item(2))}
                objData.Add(GetDSN, "JadwalPenyusutan", vaField, vaValue)
            End With
        Next
    End Sub

    Private Sub Otomatis()
        If Trim(cKode.Text) = "" Then
            Dim db As New DataTable
            db = objData.Browse(GetDSN, "Aktiva", "Max(Kode) as Kode")
            If db.Rows.Count = 0 Then
                cKode.Text = "1"
            Else
                cKode.Text = CStr(Val(GetNull(db.Rows(0).Item("Kode"))) + 1)
            End If
            db.Dispose()
        End If
        cKode.Text = Padl(Trim(cKode.Text), 6, "0")
    End Sub

    Private Sub GetMemory()
        Const cFields As String = "a.*,g.keterangan"
        Dim vaJoin() As Object = {"left join golonganaktiva g on g.kode = a.golongan"}
        Dim cDataTable = objData.Browse(GetDSN, "aktiva a", cFields, "a.kode", data.myOperator.Assign, cKode.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("nama").ToString
                dTanggal.DateTime = CDate(.Item("tglPerolehan").ToString)
                nPenyusutanPerBulan.Text = .Item("Penyusutan").ToString
                cGolonganAktiva.Text = .Item("Golongan").ToString
                cNamaGolonganAktiva.Text = .Item("Keterangan").ToString
                nLama.Text = .Item("Lama").ToString
                nHargaPerolehan.Text = .Item("HargaPerolehan").ToString
                nNilaiResidu.Text = .Item("Residu").ToString
                nNilaiPenyusutan.Text = (CDbl(nHargaPerolehan.Text) - CDbl(nNilaiResidu.Text)).ToString
                cRekeningBiayaPenyusutan.Text = .Item("RekBiayaPenyusutan").ToString
                cRekeningAkumulasiPenyusutan.Text = .Item("RekPenyusutan").ToString
                nUnit.Text = .Item("Unit").ToString
                dTglKonversi.DateTime = CDate(.Item("TglKonversi"))
                nJumlahKonversi.Text = .Item("JumlahKonversi").ToString
                SetOpt(optJenis, .Item("JenisPenyusutan").ToString)
                dAkhir.DateTime = DateAdd("m", CDbl(nLama.Text) - 1, dTanggal.DateTime)
                nTarif.Text = .Item("Tarif").ToString

                nSisaKonversi.Text = DateDiff(DateInterval.Month, dTglKonversi.DateTime, dAkhir.DateTime).ToString
                nSaran.Text = Math.Round(Devide(CDbl(nJumlahKonversi.Text), CDbl(nSisaKonversi.Text))).ToString
                nBerjalan.Text = (DateDiff(DateInterval.Month, dTanggal.DateTime, Date.Today) + 1).ToString
                GetJadwal()
                If CDbl(nPenyusutanPerBulan.Text) > 0 Then
                    HitungJadwal()
                End If
            End With
        End If
    End Sub

    Private Sub GetJadwal()
        Dim cJenisPenyusutan As String = GetOpt(optJenis)
        dtJadwal = GetJadwalAktiva(CDbl(nHargaPerolehan.Text), dTanggal.DateTime, CDbl(nPenyusutanPerBulan.Text), CDbl(nJumlahKonversi.Text),
                                   dTglKonversi.DateTime, dAkhir.DateTime, cJenisPenyusutan, CDbl(nNilaiResidu.Text), CSng(nTarif.Text))
        GridControl2.DataSource = dtJadwal
        InitGrid(GridView2)
        GridColumnFormat(GridView2, "Kode")
        GridColumnFormat(GridView2, "Nama")
        GridColumnFormat(GridView2, "TglPerolehan")
    End Sub

    Private Sub GetSQL()
        Const cField As String = "Kode,Nama,TglPerolehan"
        Dim dbData As New DataTable
        dbData = objData.Browse(GetDSN, "aktiva", cField, , , , , "Kode")
        GridControl1.DataSource = dbData
        dbData.Dispose()
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Periode")
        GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView1, "NilaiBuku", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub trPembelianAktiva_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        GetDataLookup("GolonganAktiva", cGolonganAktiva)
        GetDataLookup("Rekening", cRekeningBiayaPenyusutan, "and jenis ='D'")
        GetDataLookup("Rekening", cRekeningAkumulasiPenyusutan, "and jenis ='D'")
    End Sub

    Private Sub trPembelianAktiva_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        GetSQL()

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cNama.TabIndex, n)
        SetTabIndex(dTanggal.TabIndex, n)
        SetTabIndex(optJenis.TabIndex, n)
        SetTabIndex(nUnit.TabIndex, n)
        SetTabIndex(cGolonganAktiva.TabIndex, n)
        SetTabIndex(nTarif.TabIndex, n)
        SetTabIndex(nHargaPerolehan.TabIndex, n)
        SetTabIndex(nLama.TabIndex, n)
        SetTabIndex(nNilaiResidu.TabIndex, n)
        SetTabIndex(cmdHitung.TabIndex, n)
        SetTabIndex(nPenyusutanPerBulan.TabIndex, n)
        SetTabIndex(nNilaiPenyusutan.TabIndex, n)
        SetTabIndex(cRekeningBiayaPenyusutan.TabIndex, n)
        SetTabIndex(cRekeningAkumulasiPenyusutan.TabIndex, n)
        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cNama.EnterMoveNextControl = True
        dTanggal.EnterMoveNextControl = True
        optJenis.EnterMoveNextControl = True
        nUnit.EnterMoveNextControl = True
        cGolonganAktiva.EnterMoveNextControl = True
        nTarif.EnterMoveNextControl = True
        nHargaPerolehan.EnterMoveNextControl = True
        nLama.EnterMoveNextControl = True
        nNilaiResidu.EnterMoveNextControl = True
        nPenyusutanPerBulan.EnterMoveNextControl = True
        nNilaiPenyusutan.EnterMoveNextControl = True
        cRekeningBiayaPenyusutan.EnterMoveNextControl = True
        cRekeningAkumulasiPenyusutan.EnterMoveNextControl = True

        FormatTextBox(nBerjalan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nUnit, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTarif, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nHargaPerolehan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiResidu, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiPenyusutan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nPenyusutanPerBulan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nJumlahKonversi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSisaKonversi, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nSaran, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        Otomatis()
        cKode.Enabled = False
        cNama.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cDataTable = objData.Browse(GetDSN, "aktiva", "kode", "kode", data.myOperator.Assign, cKode.Text)
            If cDataTable.Rows.Count > 0 Then
                If nPos = myPos.add Then
                    MessageBox.Show("Kode Sudah Ada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    cKode.Text = ""
                    cKode.Focus()
                    Exit Sub
                End If
                GetMemory()
                If nPos = myPos.delete Then
                    DeleteData()
                End If
            Else
                MessageBox.Show("Data tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cGolonganAktiva_Closed(sender As Object, e As ClosedEventArgs) Handles cGolonganAktiva.Closed
        GetKeteranganLookUp(cGolonganAktiva, cNamaGolonganAktiva)
    End Sub

    Private Sub cmdHitung_Click(sender As Object, e As EventArgs) Handles cmdHitung.Click
        HitungJadwal()
    End Sub

    Private Sub cmdHitungJadwal_Click(sender As Object, e As EventArgs) Handles cmdHitungJadwal.Click
        GetJadwal()
    End Sub

    Private Sub cRekeningAkumulasiPenyusutan_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningAkumulasiPenyusutan.Closed
        GetKeteranganLookUp(cRekeningAkumulasiPenyusutan, cNamaRekeningAkumulasiPenyusutan)
    End Sub

    Private Sub cRekeningBiayaPenyusutan_Closed(sender As Object, e As ClosedEventArgs) Handles cRekeningBiayaPenyusutan.Closed
        GetKeteranganLookUp(cRekeningBiayaPenyusutan, cNamaRekeningBiayaPenyusutan)
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            GetMemory()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub nLama_KeyDown(sender As Object, e As KeyEventArgs) Handles nLama.KeyDown
        If e.KeyCode = Keys.Enter Then
            dAkhir.DateTime = EOM(DateAdd("m", CDbl(nLama.Text) - 1, dTanggal.DateTime))
            If nPos = myPos.add Then
                GetJadwal()
            End If
        End If
    End Sub

    Private Sub nNilaiPenyusutan_KeyDown(sender As Object, e As KeyEventArgs) Handles nNilaiPenyusutan.KeyDown
        If e.KeyCode = Keys.Enter Then
            nNilaiPenyusutan.Text = (CDbl(nHargaPerolehan.Text) - CDbl(nNilaiResidu.Text)).ToString
        End If
    End Sub

    Private Sub dTanggal_DateTimeChanged(sender As Object, e As EventArgs) Handles dTanggal.DateTimeChanged
        nBerjalan.Text = (DateDiff(DateInterval.Month, dTanggal.DateTime, Date.Today) + 1).ToString
    End Sub
End Class