﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository

Public Class trKoreksiJadwalBungaDeposito
    ReadOnly objData As New data()
    Dim dtData As New DataTable
    Dim lFormLoad As Boolean = False
    Dim nNominal As Double = 0
    ReadOnly x As TypeDeposito

    Private Sub InitValue()
        dTgl.DateTime = Date.Today
        dTgl1.DateTime = Date.Today
        cRekening.EditValue = ""
        cNama.Text = ""
        cAlamat.Text = ""
        nSukuBunga.Text = ""
    End Sub

    Private Sub GetSQL()
        Try
            Dim cDataSet As New DataSet
            Const cField As String = "0,bulanke,tgl as TglLama,jthtmp as JthTmpLama,tgl as TglBaru,jthtmp as JthTmpBaru,faktur,id"
            Dim cWhere As String = String.Format(" and tgl >= '{0}'", formatValue(dTgl.DateTime, formatType.yyyy_MM_dd))
            cWhere = String.Format(" {0} and tgl <= '{1}'", cWhere, formatValue(dTgl1.DateTime, formatType.yyyy_MM_dd))
            cDataSet.Clear()
            cDataSet = objData.BrowseDataSet(GetDSN, "JadwalBungaDeposito", cField, "rekening", , cRekening.Text, cWhere, "tgl,id")
            GridControl1.DataSource = cDataSet.Tables("JadwalBungaDeposito")
            cDataSet.Dispose()
            InitGrid(GridView1, , , , , , , , eGridRepoType.eCheckBox)
            GridColumnFormat(GridView1, "BulanKe", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , HorzAlignment.Far)
            GridColumnFormat(GridView1, "TglLama")
            GridColumnFormat(GridView1, "JthTmpLama")
            GridColumnFormat(GridView1, "TglBaru", , , , , True)
            GridColumnFormat(GridView1, "JthTmpBaru", , , , , True)
            GridColumnFormat(GridView1, "Faktur")
            GridColumnFormat(GridView1, "ID", , , 0)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub SimpanData()
        Dim nRow As DataRow
        Dim cWhere As String = ""
        Dim vaField() As Object
        Dim vaValue() As Object
        If MessageBox.Show("Data akan dihapus?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            For n As Integer = 0 To GridView1.SelectedRowsCount - 1
                If GridView1.GetSelectedRows()(n) >= 0 Then
                    nRow = GridView1.GetDataRow(n)
                    cWhere = String.Format("rekening = '{0}' and bulanke = {1}", cRekening.Text, nRow(1).ToString)
                    cWhere = String.Format("{0} and tgl = '{1}' and jthtmp = '{2}'", cWhere, formatValue(nRow(2), formatType.yyyy_MM_dd), formatValue(nRow(3), formatType.yyyy_MM_dd))
                    vaField = {"Tgl", "JthTmp",
                                   "SukuBunga", "Faktur"}
                    vaValue = {formatValue(nRow(4), formatType.yyyy_MM_dd), formatValue(nRow(5), formatType.yyyy_MM_dd),
                               Val(nSukuBunga.Text), nRow(6).ToString}
                    objData.Edit(GetDSN, "jadwalbungadeposito", cWhere, vaField, vaValue)
                Else
                    objData.Delete(GetDSN, "jadwalbungadeposito", , , , cWhere)
                End If
            Next
            MessageBox.Show("Data sudah dihapus.", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            InitValue()
            GetSQL()
        End If
    End Sub

    Private Sub trKoreksiJadwalBungaDeposito_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Const cField As String = "t.rekening,r.nama,r.alamat"
        Dim vaJoin() As Object = {" Left Join RegisterNasabah r on r.Kode=t.Kode"}
        LookupSearch("deposito t", cRekening, "t.rekening", , cField, vaJoin)
    End Sub

    Private Sub trKoreksiJadwalBungaDeposito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer
        lFormLoad = True
        InitForm(Me, , , cmdKeluar)
        InitValue()
        SetButtonPicture(, , , , cmdSimpan, cmdKeluar)
        CenterFormManual(Me, , True)

        SetTabIndex(dTgl.TabIndex, n)
        SetTabIndex(dTgl1.TabIndex, n)
        SetTabIndex(cRekening.TabIndex, n)
        SetTabIndex(GridControl1.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)

        dTgl.EnterMoveNextControl = True
        cRekening.EnterMoveNextControl = True

        FormatTextBox(nSukuBunga, formatType.BilRpPict2, HorzAlignment.Far)
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        Close()
    End Sub

    Private Sub cRekening_Closed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles cRekening.Closed
        KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
        KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
    End Sub

    Private Sub cRekening_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cRekening.KeyDown
        If e.KeyCode = Keys.Enter Then
            Const cField As String = "Rekening,Nominal"
            Dim cDataTable As DataTable = objData.Browse(GetDSN, "deposito", cField, "rekening", , cRekening.Text)
            If cDataTable.Rows.Count > 0 Then
                With cDataTable.Rows(0)
                    GetDeposito(cRekening.Text, dTgl.DateTime, x)
                    nNominal = Val(.Item("Nominal"))
                    cNama.Text = x.cNama
                    cAlamat.Text = x.cAlamat
                    nSukuBunga.Text = x.nSukuBunga.ToString
                    GetSQL()
                End With
            Else
                MessageBox.Show("Rekening tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cRekening.Focus()
                cRekening.Text = ""
                Exit Sub
            End If
            KeteranganLookupSearch(cRekening, cNama, "rekening", "nama")
            KeteranganLookupSearch(cRekening, cAlamat, "rekening", "alamat")
        End If
    End Sub

    Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub
End Class