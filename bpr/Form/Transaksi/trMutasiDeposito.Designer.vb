﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trMutasiDeposito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trMutasiDeposito))
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.lblBungaJatuhTempo = New DevExpress.XtraEditors.LabelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.lblTabunganTutup = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamatTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.cKeterangan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.nTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.nPinalti = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.nPokok = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.ckBunga = New DevExpress.XtraEditors.CheckEdit()
        Me.ckPokok = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.nJumlahHari = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.optKas = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.nPajak = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.nSaldoTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lblBlokir = New DevExpress.XtraEditors.LabelControl()
        Me.cFaktur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningLama = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cNoBilyet = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.dJthTmpBulanIni = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglValutaBulanIni = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.dTempo = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.dTglPenempatan = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.nSukuBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.nPenempatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.cKeteranganGolonganDeposito = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.cKeteranganGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.cAlamatTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPinalti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckPokok.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nJumlahHari.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.cNoBilyet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmpBulanIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmpBulanIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglValutaBulanIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglValutaBulanIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTempo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPenempatan.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTglPenempatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nSukuBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nPenempatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.lblBungaJatuhTempo)
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(1, 525)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(825, 33)
        Me.PanelControl4.TabIndex = 22
        '
        'lblBungaJatuhTempo
        '
        Me.lblBungaJatuhTempo.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBungaJatuhTempo.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblBungaJatuhTempo.Location = New System.Drawing.Point(5, 5)
        Me.lblBungaJatuhTempo.Name = "lblBungaJatuhTempo"
        Me.lblBungaJatuhTempo.Size = New System.Drawing.Size(347, 23)
        Me.lblBungaJatuhTempo.TabIndex = 224
        Me.lblBungaJatuhTempo.Text = "Bunga Deposito Belum Jatuh Tempo"
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(739, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Batal / Keluar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdKeluar.SuperTip = SuperToolTip1
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(658, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Preview Data"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.cmdSimpan.SuperTip = SuperToolTip2
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.lblTabunganTutup)
        Me.PanelControl3.Controls.Add(Me.cAlamatTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl28)
        Me.PanelControl3.Controls.Add(Me.cNamaTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl29)
        Me.PanelControl3.Controls.Add(Me.cKeterangan)
        Me.PanelControl3.Controls.Add(Me.LabelControl26)
        Me.PanelControl3.Controls.Add(Me.nTotal)
        Me.PanelControl3.Controls.Add(Me.LabelControl25)
        Me.PanelControl3.Controls.Add(Me.nPinalti)
        Me.PanelControl3.Controls.Add(Me.LabelControl24)
        Me.PanelControl3.Controls.Add(Me.nBunga)
        Me.PanelControl3.Controls.Add(Me.LabelControl23)
        Me.PanelControl3.Controls.Add(Me.nPokok)
        Me.PanelControl3.Controls.Add(Me.LabelControl22)
        Me.PanelControl3.Controls.Add(Me.ckBunga)
        Me.PanelControl3.Controls.Add(Me.ckPokok)
        Me.PanelControl3.Controls.Add(Me.LabelControl21)
        Me.PanelControl3.Controls.Add(Me.nJumlahHari)
        Me.PanelControl3.Controls.Add(Me.LabelControl20)
        Me.PanelControl3.Controls.Add(Me.optKas)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.LabelControl27)
        Me.PanelControl3.Controls.Add(Me.nPajak)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.nSaldoTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Location = New System.Drawing.Point(1, 273)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(825, 246)
        Me.PanelControl3.TabIndex = 21
        '
        'lblTabunganTutup
        '
        Me.lblTabunganTutup.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTabunganTutup.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblTabunganTutup.Location = New System.Drawing.Point(312, 131)
        Me.lblTabunganTutup.Name = "lblTabunganTutup"
        Me.lblTabunganTutup.Size = New System.Drawing.Size(332, 23)
        Me.lblTabunganTutup.TabIndex = 223
        Me.lblTabunganTutup.Text = "Rekening Tabungan Sudah Ditutup"
        '
        'cAlamatTabungan
        '
        Me.cAlamatTabungan.Enabled = False
        Me.cAlamatTabungan.Location = New System.Drawing.Point(407, 57)
        Me.cAlamatTabungan.Name = "cAlamatTabungan"
        Me.cAlamatTabungan.Size = New System.Drawing.Size(404, 20)
        Me.cAlamatTabungan.TabIndex = 220
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(312, 60)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl28.TabIndex = 219
        Me.LabelControl28.Text = "Alamat"
        '
        'cNamaTabungan
        '
        Me.cNamaTabungan.Enabled = False
        Me.cNamaTabungan.Location = New System.Drawing.Point(407, 31)
        Me.cNamaTabungan.Name = "cNamaTabungan"
        Me.cNamaTabungan.Size = New System.Drawing.Size(227, 20)
        Me.cNamaTabungan.TabIndex = 218
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(312, 34)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl29.TabIndex = 217
        Me.LabelControl29.Text = "Nama"
        '
        'cKeterangan
        '
        Me.cKeterangan.Enabled = False
        Me.cKeterangan.Location = New System.Drawing.Point(104, 186)
        Me.cKeterangan.Name = "cKeterangan"
        Me.cKeterangan.Size = New System.Drawing.Size(653, 20)
        Me.cKeterangan.TabIndex = 216
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(9, 190)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl26.TabIndex = 215
        Me.LabelControl26.Text = "Keterangan"
        '
        'nTotal
        '
        Me.nTotal.Enabled = False
        Me.nTotal.Location = New System.Drawing.Point(104, 160)
        Me.nTotal.Name = "nTotal"
        Me.nTotal.Size = New System.Drawing.Size(121, 20)
        Me.nTotal.TabIndex = 214
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(9, 164)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl25.TabIndex = 213
        Me.LabelControl25.Text = "Total"
        '
        'nPinalti
        '
        Me.nPinalti.Enabled = False
        Me.nPinalti.Location = New System.Drawing.Point(104, 134)
        Me.nPinalti.Name = "nPinalti"
        Me.nPinalti.Size = New System.Drawing.Size(121, 20)
        Me.nPinalti.TabIndex = 212
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(9, 138)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl24.TabIndex = 211
        Me.LabelControl24.Text = "Pinalti"
        '
        'nBunga
        '
        Me.nBunga.Enabled = False
        Me.nBunga.Location = New System.Drawing.Point(104, 82)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Size = New System.Drawing.Size(121, 20)
        Me.nBunga.TabIndex = 210
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(9, 86)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl23.TabIndex = 209
        Me.LabelControl23.Text = "Bunga"
        '
        'nPokok
        '
        Me.nPokok.Enabled = False
        Me.nPokok.Location = New System.Drawing.Point(104, 56)
        Me.nPokok.Name = "nPokok"
        Me.nPokok.Size = New System.Drawing.Size(121, 20)
        Me.nPokok.TabIndex = 208
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(9, 60)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl22.TabIndex = 207
        Me.LabelControl22.Text = "Nominal"
        '
        'ckBunga
        '
        Me.ckBunga.Location = New System.Drawing.Point(167, 31)
        Me.ckBunga.Name = "ckBunga"
        Me.ckBunga.Properties.Caption = "&Bunga"
        Me.ckBunga.Size = New System.Drawing.Size(58, 19)
        Me.ckBunga.TabIndex = 206
        '
        'ckPokok
        '
        Me.ckPokok.Location = New System.Drawing.Point(102, 31)
        Me.ckPokok.Name = "ckPokok"
        Me.ckPokok.Properties.Caption = "&Pokok"
        Me.ckPokok.Size = New System.Drawing.Size(59, 19)
        Me.ckPokok.TabIndex = 205
        '
        'LabelControl21
        '
        Me.LabelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl21.Location = New System.Drawing.Point(142, 9)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl21.TabIndex = 203
        Me.LabelControl21.Text = "Hari"
        '
        'nJumlahHari
        '
        Me.nJumlahHari.Enabled = False
        Me.nJumlahHari.Location = New System.Drawing.Point(104, 5)
        Me.nJumlahHari.Name = "nJumlahHari"
        Me.nJumlahHari.Size = New System.Drawing.Size(32, 20)
        Me.nJumlahHari.TabIndex = 202
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(9, 9)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl20.TabIndex = 201
        Me.LabelControl20.Text = "Jumlah Hari"
        '
        'optKas
        '
        Me.optKas.Location = New System.Drawing.Point(104, 212)
        Me.optKas.Name = "optKas"
        Me.optKas.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&1. Tunai"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&2. Tabungan"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&3. PB")})
        Me.optKas.Size = New System.Drawing.Size(269, 29)
        Me.optKas.TabIndex = 200
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(9, 220)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl4.TabIndex = 199
        Me.LabelControl4.Text = "Cara Pencairan"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(9, 34)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl27.TabIndex = 166
        Me.LabelControl27.Text = "Jenis Pencairan"
        '
        'nPajak
        '
        Me.nPajak.Location = New System.Drawing.Point(104, 108)
        Me.nPajak.Name = "nPajak"
        Me.nPajak.Size = New System.Drawing.Size(121, 20)
        Me.nPajak.TabIndex = 130
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 111)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl9.TabIndex = 129
        Me.LabelControl9.Text = "Pajak"
        '
        'nSaldoTabungan
        '
        Me.nSaldoTabungan.Enabled = False
        Me.nSaldoTabungan.Location = New System.Drawing.Point(407, 83)
        Me.nSaldoTabungan.Name = "nSaldoTabungan"
        Me.nSaldoTabungan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoTabungan.TabIndex = 127
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(312, 87)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl5.TabIndex = 126
        Me.LabelControl5.Text = "Saldo Tabungan"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(407, 5)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 125
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(312, 9)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl6.TabIndex = 124
        Me.LabelControl6.Text = "Rek. Tabungan"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lblBlokir)
        Me.PanelControl1.Controls.Add(Me.cFaktur)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cRekeningLama)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Controls.Add(Me.lblTglRegister)
        Me.PanelControl1.Controls.Add(Me.dTgl)
        Me.PanelControl1.Location = New System.Drawing.Point(1, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(825, 168)
        Me.PanelControl1.TabIndex = 20
        '
        'lblBlokir
        '
        Me.lblBlokir.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlokir.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblBlokir.Location = New System.Drawing.Point(312, 43)
        Me.lblBlokir.Name = "lblBlokir"
        Me.lblBlokir.Size = New System.Drawing.Size(170, 23)
        Me.lblBlokir.TabIndex = 224
        Me.lblBlokir.Text = "Rekening Diblokir"
        '
        'cFaktur
        '
        Me.cFaktur.Enabled = False
        Me.cFaktur.Location = New System.Drawing.Point(104, 139)
        Me.cFaktur.Name = "cFaktur"
        Me.cFaktur.Size = New System.Drawing.Size(227, 20)
        Me.cFaktur.TabIndex = 134
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(9, 143)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl15.TabIndex = 133
        Me.LabelControl15.Text = "No. Faktur"
        '
        'cRekeningLama
        '
        Me.cRekeningLama.Location = New System.Drawing.Point(104, 35)
        Me.cRekeningLama.Name = "cRekeningLama"
        Me.cRekeningLama.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekeningLama.Size = New System.Drawing.Size(158, 20)
        Me.cRekeningLama.TabIndex = 131
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 39)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl10.TabIndex = 130
        Me.LabelControl10.Text = "Rekening Lama"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(104, 61)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(158, 20)
        Me.cRekening.TabIndex = 129
        '
        'cAlamat
        '
        Me.cAlamat.Enabled = False
        Me.cAlamat.Location = New System.Drawing.Point(104, 113)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(404, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(9, 116)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 65)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 112
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cNama
        '
        Me.cNama.Enabled = False
        Me.cNama.Location = New System.Drawing.Point(104, 87)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(9, 90)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(9, 12)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(38, 13)
        Me.lblTglRegister.TabIndex = 83
        Me.lblTglRegister.Text = "Tanggal"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(104, 9)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 82
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cNoBilyet)
        Me.PanelControl2.Controls.Add(Me.LabelControl30)
        Me.PanelControl2.Controls.Add(Me.LabelControl19)
        Me.PanelControl2.Controls.Add(Me.dJthTmpBulanIni)
        Me.PanelControl2.Controls.Add(Me.LabelControl18)
        Me.PanelControl2.Controls.Add(Me.dTglValutaBulanIni)
        Me.PanelControl2.Controls.Add(Me.LabelControl17)
        Me.PanelControl2.Controls.Add(Me.dTempo)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.dTglPenempatan)
        Me.PanelControl2.Controls.Add(Me.LabelControl13)
        Me.PanelControl2.Controls.Add(Me.nSukuBunga)
        Me.PanelControl2.Controls.Add(Me.LabelControl16)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.nLama)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.nPenempatan)
        Me.PanelControl2.Controls.Add(Me.LabelControl11)
        Me.PanelControl2.Controls.Add(Me.cGolonganDeposito)
        Me.PanelControl2.Controls.Add(Me.cKeteranganGolonganDeposito)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.cGolonganDeposan)
        Me.PanelControl2.Controls.Add(Me.cKeteranganGolonganDeposan)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Location = New System.Drawing.Point(1, 177)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(825, 90)
        Me.PanelControl2.TabIndex = 23
        '
        'cNoBilyet
        '
        Me.cNoBilyet.Enabled = False
        Me.cNoBilyet.Location = New System.Drawing.Point(658, 57)
        Me.cNoBilyet.Name = "cNoBilyet"
        Me.cNoBilyet.Size = New System.Drawing.Size(82, 20)
        Me.cNoBilyet.TabIndex = 224
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(538, 60)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl30.TabIndex = 223
        Me.LabelControl30.Text = "No. Bilyet"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(538, 34)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl19.TabIndex = 158
        Me.LabelControl19.Text = "Jatuh Tempo Bulan Ini"
        '
        'dJthTmpBulanIni
        '
        Me.dJthTmpBulanIni.EditValue = Nothing
        Me.dJthTmpBulanIni.Location = New System.Drawing.Point(658, 31)
        Me.dJthTmpBulanIni.Name = "dJthTmpBulanIni"
        Me.dJthTmpBulanIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJthTmpBulanIni.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJthTmpBulanIni.Size = New System.Drawing.Size(82, 20)
        Me.dJthTmpBulanIni.TabIndex = 157
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(538, 8)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl18.TabIndex = 156
        Me.LabelControl18.Text = "Tgl Valuta Bulan Ini"
        '
        'dTglValutaBulanIni
        '
        Me.dTglValutaBulanIni.EditValue = Nothing
        Me.dTglValutaBulanIni.Location = New System.Drawing.Point(658, 5)
        Me.dTglValutaBulanIni.Name = "dTglValutaBulanIni"
        Me.dTglValutaBulanIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglValutaBulanIni.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglValutaBulanIni.Size = New System.Drawing.Size(82, 20)
        Me.dTglValutaBulanIni.TabIndex = 155
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(350, 60)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl17.TabIndex = 154
        Me.LabelControl17.Text = "Jatuh Tempo"
        '
        'dTempo
        '
        Me.dTempo.EditValue = Nothing
        Me.dTempo.Location = New System.Drawing.Point(434, 57)
        Me.dTempo.Name = "dTempo"
        Me.dTempo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTempo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTempo.Size = New System.Drawing.Size(82, 20)
        Me.dTempo.TabIndex = 153
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(350, 34)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl3.TabIndex = 152
        Me.LabelControl3.Text = "Tgl Penempatan"
        '
        'dTglPenempatan
        '
        Me.dTglPenempatan.EditValue = Nothing
        Me.dTglPenempatan.Location = New System.Drawing.Point(434, 31)
        Me.dTglPenempatan.Name = "dTglPenempatan"
        Me.dTglPenempatan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTglPenempatan.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTglPenempatan.Size = New System.Drawing.Size(82, 20)
        Me.dTglPenempatan.TabIndex = 151
        '
        'LabelControl13
        '
        Me.LabelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl13.Location = New System.Drawing.Point(320, 61)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl13.TabIndex = 150
        Me.LabelControl13.Text = "%"
        '
        'nSukuBunga
        '
        Me.nSukuBunga.Enabled = False
        Me.nSukuBunga.Location = New System.Drawing.Point(283, 57)
        Me.nSukuBunga.Name = "nSukuBunga"
        Me.nSukuBunga.Properties.Mask.EditMask = "n"
        Me.nSukuBunga.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nSukuBunga.Size = New System.Drawing.Size(32, 20)
        Me.nSukuBunga.TabIndex = 149
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(221, 61)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl16.TabIndex = 148
        Me.LabelControl16.Text = "Suku Bunga"
        '
        'LabelControl2
        '
        Me.LabelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl2.Location = New System.Drawing.Point(472, 8)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl2.TabIndex = 147
        Me.LabelControl2.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.Enabled = False
        Me.nLama.Location = New System.Drawing.Point(434, 5)
        Me.nLama.Name = "nLama"
        Me.nLama.Properties.Mask.EditMask = "n"
        Me.nLama.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.nLama.Size = New System.Drawing.Size(32, 20)
        Me.nLama.TabIndex = 146
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(350, 8)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl7.TabIndex = 145
        Me.LabelControl7.Text = "Jangka Waktu"
        '
        'nPenempatan
        '
        Me.nPenempatan.Enabled = False
        Me.nPenempatan.Location = New System.Drawing.Point(104, 57)
        Me.nPenempatan.Name = "nPenempatan"
        Me.nPenempatan.Size = New System.Drawing.Size(105, 20)
        Me.nPenempatan.TabIndex = 144
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(9, 61)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl11.TabIndex = 143
        Me.LabelControl11.Text = "Penempatan"
        '
        'cGolonganDeposito
        '
        Me.cGolonganDeposito.Enabled = False
        Me.cGolonganDeposito.Location = New System.Drawing.Point(104, 31)
        Me.cGolonganDeposito.Name = "cGolonganDeposito"
        Me.cGolonganDeposito.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposito.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposito.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposito.TabIndex = 142
        '
        'cKeteranganGolonganDeposito
        '
        Me.cKeteranganGolonganDeposito.Enabled = False
        Me.cKeteranganGolonganDeposito.Location = New System.Drawing.Point(142, 31)
        Me.cKeteranganGolonganDeposito.Name = "cKeteranganGolonganDeposito"
        Me.cKeteranganGolonganDeposito.Size = New System.Drawing.Size(189, 20)
        Me.cKeteranganGolonganDeposito.TabIndex = 141
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 34)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl1.TabIndex = 140
        Me.LabelControl1.Text = "Gol. Deposito"
        '
        'cGolonganDeposan
        '
        Me.cGolonganDeposan.Enabled = False
        Me.cGolonganDeposan.Location = New System.Drawing.Point(104, 5)
        Me.cGolonganDeposan.Name = "cGolonganDeposan"
        Me.cGolonganDeposan.Properties.Mask.EditMask = "n"
        Me.cGolonganDeposan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.cGolonganDeposan.Size = New System.Drawing.Size(32, 20)
        Me.cGolonganDeposan.TabIndex = 139
        '
        'cKeteranganGolonganDeposan
        '
        Me.cKeteranganGolonganDeposan.Enabled = False
        Me.cKeteranganGolonganDeposan.Location = New System.Drawing.Point(142, 5)
        Me.cKeteranganGolonganDeposan.Name = "cKeteranganGolonganDeposan"
        Me.cKeteranganGolonganDeposan.Size = New System.Drawing.Size(189, 20)
        Me.cKeteranganGolonganDeposan.TabIndex = 138
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(9, 8)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl8.TabIndex = 137
        Me.LabelControl8.Text = "Gol. Deposan"
        '
        'trMutasiDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(827, 559)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trMutasiDeposito"
        Me.Text = "Mutasi Deposito"
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.cAlamatTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeterangan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPinalti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckPokok.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nJumlahHari.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optKas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cFaktur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.cNoBilyet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmpBulanIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmpBulanIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglValutaBulanIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglValutaBulanIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTempo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPenempatan.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTglPenempatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nSukuBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nPenempatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents optKas As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPajak As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSaldoTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cFaktur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningLama As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nSukuBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPenempatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeteranganGolonganDeposito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cKeteranganGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblBungaJatuhTempo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTabunganTutup As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamatTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cKeterangan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPinalti As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nPokok As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ckBunga As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckPokok As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nJumlahHari As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblBlokir As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNoBilyet As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJthTmpBulanIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglValutaBulanIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTempo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dTglPenempatan As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
End Class
