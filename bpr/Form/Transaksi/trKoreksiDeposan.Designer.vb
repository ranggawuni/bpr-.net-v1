﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class trKoreksiDeposan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If objData IsNot Nothing Then
                    objData.Dispose()
                End If
                If dtJadwal IsNot Nothing Then
                    dtJadwal.Dispose()
                    dtJadwal = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(trKoreksiDeposan))
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.nSaldoTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamatNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.cRekeningTabungan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.cNasabah = New DevExpress.XtraEditors.LookUpEdit()
        Me.cKeteranganNasabah = New DevExpress.XtraEditors.TextEdit()
        Me.nNominal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.optARO = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.optPajak = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.optDeposan = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.nBunga = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.dTgl = New DevExpress.XtraEditors.DateEdit()
        Me.lblTglRegister = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.dJthTmp = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.nLama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cNamaGolonganDeposan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cGolonganDeposan = New DevExpress.XtraEditors.LookUpEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cTelepon = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.cAlamat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cNama = New DevExpress.XtraEditors.TextEdit()
        Me.lblNama = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.cRekening = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cKeteranganNasabah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optARO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPajak.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNamaGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.nSaldoTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl1)
        Me.PanelControl3.Controls.Add(Me.cAlamatNasabah)
        Me.PanelControl3.Controls.Add(Me.LabelControl20)
        Me.PanelControl3.Controls.Add(Me.cNamaNasabah)
        Me.PanelControl3.Controls.Add(Me.LabelControl23)
        Me.PanelControl3.Controls.Add(Me.cRekeningTabungan)
        Me.PanelControl3.Controls.Add(Me.LabelControl13)
        Me.PanelControl3.Controls.Add(Me.LabelControl24)
        Me.PanelControl3.Controls.Add(Me.cNasabah)
        Me.PanelControl3.Controls.Add(Me.cKeteranganNasabah)
        Me.PanelControl3.Controls.Add(Me.nNominal)
        Me.PanelControl3.Controls.Add(Me.LabelControl19)
        Me.PanelControl3.Controls.Add(Me.optARO)
        Me.PanelControl3.Controls.Add(Me.LabelControl8)
        Me.PanelControl3.Controls.Add(Me.optPajak)
        Me.PanelControl3.Controls.Add(Me.LabelControl21)
        Me.PanelControl3.Controls.Add(Me.optDeposan)
        Me.PanelControl3.Controls.Add(Me.LabelControl27)
        Me.PanelControl3.Controls.Add(Me.LabelControl16)
        Me.PanelControl3.Controls.Add(Me.nBunga)
        Me.PanelControl3.Controls.Add(Me.LabelControl18)
        Me.PanelControl3.Controls.Add(Me.dTgl)
        Me.PanelControl3.Controls.Add(Me.lblTglRegister)
        Me.PanelControl3.Controls.Add(Me.LabelControl7)
        Me.PanelControl3.Controls.Add(Me.dJthTmp)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.nLama)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Controls.Add(Me.cNamaGolonganDeposan)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.cGolonganDeposan)
        Me.PanelControl3.Location = New System.Drawing.Point(3, 116)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(710, 293)
        Me.PanelControl3.TabIndex = 19
        '
        'nSaldoTabungan
        '
        Me.nSaldoTabungan.Location = New System.Drawing.Point(419, 83)
        Me.nSaldoTabungan.Name = "nSaldoTabungan"
        Me.nSaldoTabungan.Size = New System.Drawing.Size(105, 20)
        Me.nSaldoTabungan.TabIndex = 222
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(324, 87)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl1.TabIndex = 221
        Me.LabelControl1.Text = "Saldo Tabungan"
        '
        'cAlamatNasabah
        '
        Me.cAlamatNasabah.Location = New System.Drawing.Point(419, 57)
        Me.cAlamatNasabah.Name = "cAlamatNasabah"
        Me.cAlamatNasabah.Size = New System.Drawing.Size(281, 20)
        Me.cAlamatNasabah.TabIndex = 220
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(324, 60)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl20.TabIndex = 219
        Me.LabelControl20.Text = "Alamat Nasabah"
        '
        'cNamaNasabah
        '
        Me.cNamaNasabah.Location = New System.Drawing.Point(419, 31)
        Me.cNamaNasabah.Name = "cNamaNasabah"
        Me.cNamaNasabah.Size = New System.Drawing.Size(227, 20)
        Me.cNamaNasabah.TabIndex = 218
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(324, 34)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl23.TabIndex = 217
        Me.LabelControl23.Text = "Nama Nasabah"
        '
        'cRekeningTabungan
        '
        Me.cRekeningTabungan.Location = New System.Drawing.Point(419, 5)
        Me.cRekeningTabungan.Name = "cRekeningTabungan"
        Me.cRekeningTabungan.Size = New System.Drawing.Size(105, 20)
        Me.cRekeningTabungan.TabIndex = 216
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(324, 8)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl13.TabIndex = 215
        Me.LabelControl13.Text = "Rek. Tabungan"
        '
        'LabelControl24
        '
        Me.LabelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl24.Location = New System.Drawing.Point(8, 269)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl24.TabIndex = 214
        Me.LabelControl24.Text = "Nasabah Kantor"
        '
        'cNasabah
        '
        Me.cNasabah.Location = New System.Drawing.Point(103, 266)
        Me.cNasabah.Name = "cNasabah"
        Me.cNasabah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cNasabah.Size = New System.Drawing.Size(44, 20)
        Me.cNasabah.TabIndex = 213
        '
        'cKeteranganNasabah
        '
        Me.cKeteranganNasabah.Enabled = False
        Me.cKeteranganNasabah.Location = New System.Drawing.Point(153, 266)
        Me.cKeteranganNasabah.Name = "cKeteranganNasabah"
        Me.cKeteranganNasabah.Size = New System.Drawing.Size(141, 20)
        Me.cKeteranganNasabah.TabIndex = 212
        '
        'nNominal
        '
        Me.nNominal.Location = New System.Drawing.Point(103, 240)
        Me.nNominal.Name = "nNominal"
        Me.nNominal.Size = New System.Drawing.Size(105, 20)
        Me.nNominal.TabIndex = 211
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(8, 244)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl19.TabIndex = 210
        Me.LabelControl19.Text = "Nominal"
        '
        'optARO
        '
        Me.optARO.Location = New System.Drawing.Point(103, 118)
        Me.optARO.Name = "optARO"
        Me.optARO.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optARO.Size = New System.Drawing.Size(177, 29)
        Me.optARO.TabIndex = 209
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(8, 125)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl8.TabIndex = 208
        Me.LabelControl8.Text = "Sistem ARO"
        '
        'optPajak
        '
        Me.optPajak.Location = New System.Drawing.Point(103, 153)
        Me.optPajak.Name = "optPajak"
        Me.optPajak.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Ya"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Tidak")})
        Me.optPajak.Size = New System.Drawing.Size(177, 29)
        Me.optPajak.TabIndex = 207
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(8, 160)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl21.TabIndex = 206
        Me.LabelControl21.Text = "Hitung Pajak"
        '
        'optDeposan
        '
        Me.optDeposan.Location = New System.Drawing.Point(103, 83)
        Me.optDeposan.Name = "optDeposan"
        Me.optDeposan.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Non Instansi"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "&Instansi")})
        Me.optDeposan.Size = New System.Drawing.Size(177, 29)
        Me.optDeposan.TabIndex = 205
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(8, 91)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl27.TabIndex = 204
        Me.LabelControl27.Text = "Jenis Deposan"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(153, 60)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl16.TabIndex = 203
        Me.LabelControl16.Text = "%"
        '
        'nBunga
        '
        Me.nBunga.Location = New System.Drawing.Point(103, 57)
        Me.nBunga.Name = "nBunga"
        Me.nBunga.Size = New System.Drawing.Size(44, 20)
        Me.nBunga.TabIndex = 202
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(8, 60)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl18.TabIndex = 201
        Me.LabelControl18.Text = "Suku Bunga"
        '
        'dTgl
        '
        Me.dTgl.EditValue = Nothing
        Me.dTgl.Location = New System.Drawing.Point(103, 188)
        Me.dTgl.Name = "dTgl"
        Me.dTgl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dTgl.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dTgl.Size = New System.Drawing.Size(82, 20)
        Me.dTgl.TabIndex = 199
        '
        'lblTglRegister
        '
        Me.lblTglRegister.Location = New System.Drawing.Point(8, 191)
        Me.lblTglRegister.Name = "lblTglRegister"
        Me.lblTglRegister.Size = New System.Drawing.Size(47, 13)
        Me.lblTglRegister.TabIndex = 200
        Me.lblTglRegister.Text = "Tgl Valuta"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(8, 217)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl7.TabIndex = 193
        Me.LabelControl7.Text = "Tgl Jatuh Tempo"
        '
        'dJthTmp
        '
        Me.dJthTmp.EditValue = Nothing
        Me.dJthTmp.Location = New System.Drawing.Point(103, 214)
        Me.dJthTmp.Name = "dJthTmp"
        Me.dJthTmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dJthTmp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dJthTmp.Size = New System.Drawing.Size(82, 20)
        Me.dJthTmp.TabIndex = 192
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(153, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl4.TabIndex = 191
        Me.LabelControl4.Text = "Bulan"
        '
        'nLama
        '
        Me.nLama.Location = New System.Drawing.Point(103, 31)
        Me.nLama.Name = "nLama"
        Me.nLama.Size = New System.Drawing.Size(44, 20)
        Me.nLama.TabIndex = 190
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 34)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl3.TabIndex = 189
        Me.LabelControl3.Text = "Jangka Waktu"
        '
        'cNamaGolonganDeposan
        '
        Me.cNamaGolonganDeposan.Enabled = False
        Me.cNamaGolonganDeposan.Location = New System.Drawing.Point(153, 5)
        Me.cNamaGolonganDeposan.Name = "cNamaGolonganDeposan"
        Me.cNamaGolonganDeposan.Size = New System.Drawing.Size(141, 20)
        Me.cNamaGolonganDeposan.TabIndex = 187
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 8)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 186
        Me.LabelControl2.Text = "Gol. Deposan"
        '
        'cGolonganDeposan
        '
        Me.cGolonganDeposan.Location = New System.Drawing.Point(103, 5)
        Me.cGolonganDeposan.Name = "cGolonganDeposan"
        Me.cGolonganDeposan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cGolonganDeposan.Size = New System.Drawing.Size(44, 20)
        Me.cGolonganDeposan.TabIndex = 188
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cRekening)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.cTelepon)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.cAlamat)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.cNama)
        Me.PanelControl1.Controls.Add(Me.lblNama)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(710, 109)
        Me.PanelControl1.TabIndex = 18
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 8)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl12.TabIndex = 177
        Me.LabelControl12.Text = "No. Rekening"
        '
        'cTelepon
        '
        Me.cTelepon.Location = New System.Drawing.Point(103, 83)
        Me.cTelepon.Name = "cTelepon"
        Me.cTelepon.Size = New System.Drawing.Size(105, 20)
        Me.cTelepon.TabIndex = 119
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(8, 87)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl15.TabIndex = 118
        Me.LabelControl15.Text = "Telepon"
        '
        'cAlamat
        '
        Me.cAlamat.Location = New System.Drawing.Point(103, 57)
        Me.cAlamat.Name = "cAlamat"
        Me.cAlamat.Size = New System.Drawing.Size(294, 20)
        Me.cAlamat.TabIndex = 117
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(8, 60)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl14.TabIndex = 116
        Me.LabelControl14.Text = "Alamat"
        '
        'cNama
        '
        Me.cNama.Location = New System.Drawing.Point(103, 31)
        Me.cNama.Name = "cNama"
        Me.cNama.Size = New System.Drawing.Size(227, 20)
        Me.cNama.TabIndex = 85
        '
        'lblNama
        '
        Me.lblNama.Location = New System.Drawing.Point(8, 34)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(27, 13)
        Me.lblNama.TabIndex = 84
        Me.lblNama.Text = "Nama"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmdKeluar)
        Me.PanelControl4.Controls.Add(Me.cmdSimpan)
        Me.PanelControl4.Location = New System.Drawing.Point(3, 415)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(710, 33)
        Me.PanelControl4.TabIndex = 27
        '
        'cmdKeluar
        '
        Me.cmdKeluar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdKeluar.Location = New System.Drawing.Point(625, 5)
        Me.cmdKeluar.Name = "cmdKeluar"
        Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Batal / Keluar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Klik tombol ini jika akan membatalkan proses entry data atau menutup form ini"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.cmdKeluar.SuperTip = SuperToolTip3
        Me.cmdKeluar.TabIndex = 9
        Me.cmdKeluar.Text = "&Keluar"
        '
        'cmdSimpan
        '
        Me.cmdSimpan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSimpan.Location = New System.Drawing.Point(544, 5)
        Me.cmdSimpan.Name = "cmdSimpan"
        Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
        ToolTipTitleItem4.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem4.Appearance.Options.UseImage = True
        ToolTipTitleItem4.Image = CType(resources.GetObject("ToolTipTitleItem4.Image"), System.Drawing.Image)
        ToolTipTitleItem4.Text = "Preview Data"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Klik tombol ini jika data akan dipreview"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.cmdSimpan.SuperTip = SuperToolTip4
        Me.cmdSimpan.TabIndex = 8
        Me.cmdSimpan.Text = "&Simpan"
        '
        'cRekening
        '
        Me.cRekening.Location = New System.Drawing.Point(103, 5)
        Me.cRekening.Name = "cRekening"
        Me.cRekening.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cRekening.Size = New System.Drawing.Size(131, 20)
        Me.cRekening.TabIndex = 178
        '
        'trKoreksiDeposan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 450)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "trKoreksiDeposan"
        Me.Text = "Koreksi Data Deposito"
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.nSaldoTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamatNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cRekeningTabungan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cKeteranganNasabah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nNominal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optARO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPajak.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nBunga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dTgl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dJthTmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nLama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNamaGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cGolonganDeposan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.cTelepon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cAlamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cNama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.cRekening.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dTgl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblTglRegister As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dJthTmp As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nLama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaGolonganDeposan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cGolonganDeposan As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cTelepon As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNama As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nBunga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optDeposan As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optPajak As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents optARO As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents nNominal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNasabah As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cKeteranganNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nSaldoTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cAlamatNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cNamaNasabah As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cRekeningTabungan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cRekening As DevExpress.XtraEditors.LookUpEdit
End Class
