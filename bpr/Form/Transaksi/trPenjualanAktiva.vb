﻿Imports bpr.MySQL_Data_Library
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls

Public Class trPenjualanAktiva
    ReadOnly objData As New data()
    Dim nPos As myPos = myPos.normal
    Dim lEdit As Boolean
    Dim dtJadwal As New DataTable

    Private Sub GetEdit(ByVal lPar As Boolean)
        PanelControl1.Enabled = lPar
        lEdit = lPar
        SetButton(cmdSimpan, cmdKeluar, cmdAdd, cmdEdit, cmdHapus, nPos, lPar, cmdAktivasi)
    End Sub

    Private Sub InitValue()
        dJual.DateTime = Date.Today
        cKode.Text = ""
        cNama.Text = ""
        dJual.DateTime = Now.Date
        optJenis.SelectedIndex = 1
        nUnit.Text = "0"
        cGolonganAktiva.EditValue = ""
        cNamaGolonganAktiva.Text = ""
        nTarif.Text = "0"
        nHargaPerolehan.Text = "0"
        nLama.Text = "0"
        nNilaiResidu.Text = "0"
        nNilaiPenyusutan.Text = "0"
        nHargaJual.Text = "0"
        cRekeningDebet.EditValue = ""
        cNamaRekeningDebet.Text = ""
        cRekeningKredit.EditValue = ""
        cNamaRekeningKredit.Text = ""
    End Sub

    Private Function ValidSaving() As Boolean
        ValidSaving = True

        If Not CheckData(cKode.Text, "Kode harus diisi. Ulangi pengisian.") Then
            Return False
            cKode.Focus()
            Exit Function
        End If

        If Not CheckData(nLama.Text, "Lama penyusutan harus diisi. Ulangi pengisian.") Then
            Return False
            nLama.Focus()
            Exit Function
        End If
    End Function

    Private Sub SimpanData()
        If nPos = myPos.add Or nPos = myPos.edit Then
            If ValidSaving() Then
                If MessageBox.Show("Data akan disimpan ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    UpdCfg(eCfg.msKodeRekeningDebetAktivaTetap, cRekeningDebet.Text)
                    UpdCfg(eCfg.msKodeRekeningKreditAktivaTetap, cRekeningKredit.Text)
                    Dim cFaktur As String = GetLastFaktur(eFaktur.fkt_PenjualanAktiva, dJual.DateTime)
                    Dim vaField() As Object = {"Status", "HargaJual", "TglPenjualan"}
                    Dim vaValue() As Object = {"2", CDbl(nHargaJual.text), dJual.DateTime}
                    objData.Edit(GetDSN(), "Aktiva", String.Format("Kode ='{0}'", cKode.Text), vaField, vaValue)
                    UpdRekPenjualanAktiva(cFaktur, cKode.Text, dJual.DateTime)
                    cFaktur = GetLastFaktur(eFaktur.fkt_PenjualanAktiva, dJual.DateTime, , True)
                Else
                    cNama.Focus()
                    Exit Sub
                End If
                InitValue()
                GetEdit(False)
            End If
        End If
    End Sub

    Private Sub GetMemory()
        Const cFields As String = "a.*,g.keterangan"
        Dim vaJoin() As Object = {"left join golonganaktiva g on g.kode = a.golongan"}
        Dim cDataTable = objData.Browse(GetDSN, "aktiva a", cFields, "a.kode", data.myOperator.Assign, cKode.Text, , , vaJoin)
        If cDataTable.Rows.Count > 0 Then
            With cDataTable.Rows(0)
                cNama.Text = .Item("nama").ToString
                If nPos = myPos.add Then
                    dJual.DateTime = GetTglTransaksi()
                Else
                    dJual.DateTime = CDate(GetNull(.Item("TglPenjualan")))
                End If
                nHargaJual.Text = .Item("Penyusutan").ToString
                cGolonganAktiva.Text = .Item("Golongan").ToString
                cNamaGolonganAktiva.Text = .Item("Keterangan").ToString
                nLama.Text = .Item("Lama").ToString
                nHargaPerolehan.Text = .Item("HargaPerolehan").ToString
                nNilaiResidu.Text = .Item("Residu").ToString
                nNilaiPenyusutan.Text = (CDbl(nHargaPerolehan.Text) - CDbl(nNilaiResidu.Text)).ToString
                cRekeningDebet.Text = .Item("RekBiayaPenyusutan").ToString
                cRekeningKredit.Text = .Item("RekPenyusutan").ToString
                nUnit.Text = .Item("Unit").ToString
                SetOpt(optJenis, .Item("JenisPenyusutan").ToString)
                nTarif.Text = .Item("Tarif").ToString
            End With
        End If
    End Sub

    Private Sub GetSQL()
        Const cField As String = "Kode,Nama,TglPerolehan"
        Dim dbData As New DataTable
        dbData = objData.Browse(GetDSN, "aktiva", cField, , , , , "Kode")
        GridControl1.DataSource = dbData
        dbData.Dispose()
        InitGrid(GridView1)
        GridColumnFormat(GridView1, "Periode")
        GridColumnFormat(GridView1, "Jumlah", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
        GridColumnFormat(GridView1, "NilaiBuku", DevExpress.Utils.FormatType.Numeric, formatType.BilRpPict, , DevExpress.Utils.HorzAlignment.Far)
    End Sub

    Private Sub trPenjualanAktiva_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim n As Integer

        CenterFormManual(Me, , True)
        SetButtonPicture(Me, cmdAdd, cmdEdit, cmdHapus, cmdSimpan, cmdKeluar, cmdAktivasi)
        GetEdit(False)
        InitValue()
        GetSQL()

        SetTabIndex(cKode.TabIndex, n)
        SetTabIndex(cNama.TabIndex, n)
        SetTabIndex(dJual.TabIndex, n)

        SetTabIndex(cmdAdd.TabIndex, n)
        SetTabIndex(cmdEdit.TabIndex, n)
        SetTabIndex(cmdHapus.TabIndex, n)
        SetTabIndex(cmdSimpan.TabIndex, n)
        SetTabIndex(cmdKeluar.TabIndex, n)
        SetTabIndex(cmdAktivasi.TabIndex, n)

        cKode.EnterMoveNextControl = True
        cNama.EnterMoveNextControl = True
        dJual.EnterMoveNextControl = True
        nHargaJual.EnterMoveNextControl = True
        nNilaiPenyusutan.EnterMoveNextControl = True
        cRekeningDebet.EnterMoveNextControl = True
        cRekeningKredit.EnterMoveNextControl = True

        FormatTextBox(nUnit, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nTarif, formatType.BilRpPict2, HorzAlignment.Far)
        FormatTextBox(nHargaPerolehan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nLama, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiResidu, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nNilaiPenyusutan, formatType.BilRpPict, HorzAlignment.Far)
        FormatTextBox(nHargaJual, formatType.BilRpPict, HorzAlignment.Far)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        nPos = myPos.add
        GetEdit(True)
        InitValue()
        cKode.Enabled = False
        cNama.Focus()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        nPos = myPos.edit
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdHapus.Click
        nPos = myPos.delete
        GetEdit(True)
        cKode.Focus()
    End Sub

    Private Sub cmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        SimpanData()
    End Sub

    Private Sub cmdKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKeluar.Click
        If Not lEdit Then
            Close()
        Else
            GetEdit(False)
            InitValue()
        End If
    End Sub

    Private Sub cKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cKode.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cDataTable = objData.Browse(GetDSN, "aktiva", "kode", "kode", data.myOperator.Assign, cKode.Text)
            If cDataTable.Rows.Count > 0 Then
                If cDataTable.Rows(0).Item("Status").ToString = "1" Then
                    GetMemory()
                ElseIf cDataTable.Rows(0).Item("Status").ToString = "2" Then
                    MessageBox.Show("Aktiva tetap ini sudah dijual.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    InitValue()
                    cKode.Focus()
                    Exit Sub
                ElseIf cDataTable.Rows(0).Item("Status").ToString = "3" Then
                    MessageBox.Show("Aktiva tetap ini sudah break.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    InitValue()
                    cKode.Focus()
                    Exit Sub
                End If
            Else
                MessageBox.Show("Data tidak ditemukan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cKode.Text = ""
                cNama.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Try
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            cKode.Text = row(0).ToString
            GetMemory()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class