﻿Imports bpr.MySQL_Data_Library
Imports bpr.MySQL_Data_Library.data.myOperator

Public Class FrmLogin
    Private ReadOnly objMenu As New menu_class()
    Private ReadOnly objData As New data()
    Private ReadOnly md5k As New clsMD5
    Dim vPass As String, vFull As String
    ReadOnly lPress As Integer
    Dim cID As String
    Dim lPasswordOnly As Boolean

    Private Function CekData() As Boolean
        Dim cField As String = "count(username) as kode"
        Dim cDataTable = objData.Browse(GetDSN, "UserName", cField, "Kode", Assign, cKode.Text)
        If cDataTable.Rows.Count > 0 Then
            If UCase(cUser.Text) = "HEASOFT" Then
                vPass = md5k.MD5("HeaI")
                vFull = "supervisor"
                cID = "001"
                CekData = True
                nLevel.Text = CStr(0)
            Else
                MessageBox.Show("Kata kunci anda salah.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                vPass = ""
                cID = ""
                nLevel.Text = CStr(0)
                cUser.Focus()
                CekData = False
            End If
        Else
            cField = "Userpassword,FullName,id"
            Dim cWhere As String = String.Format(" and kode='{0}'", cKode.Text)
            cDataTable = objData.Browse(GetDSN, "username", cField, _
                                        "username", data.myOperator.Assign, cUser.Text, cWhere)
            With cDataTable
                If .DefaultView.Count > 0 Then
                    vPass = .Rows(0).Item("UserPassword").ToString
                    vFull = .Rows(0).Item("FullName").ToString
                    cID = .Rows(0).Item("ID").ToString
                    nLevel.Text = .Rows(0).Item("Level").ToString
                Else
                    If Not cUser.Text = "HEASOFT" Then
                        MessageBox.Show("User name tidak valid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        vPass = ""
                        cID = ""
                        nLevel.Text = CStr(0)
                        cUser.Focus()
                    Else
                        vPass = md5k.MD5("HeaI")
                        vFull = "Supervisor"
                        cID = "001"
                        nLevel.Text = "0"
                        CekData = True
                    End If
                End If
            End With
        End If

    End Function

    Private Sub SetUser()
        SetUserName(1, cUser.Text, True)
        SetUserName(2, Zip(cPass.Text), True)
        SetUserName(3, vFull, True)
        SetUserName(4, nLevel.Text, True)
        SetUserName(5, cID, True)
        nUserLevel = CInt(nLevel.Text)
        cUserID = cID
        cUserName = cUser.Text
        cFullName = vFull
    End Sub

    Private Sub Login()
        Dim cSQL As String = "select current_date as tanggal,current_time as waktu"
        Dim db As DataTable = objData.SQL(GetDSN, cSQL)
        If Not db.Rows.Count > 0 Then
            Dim cWaktu As String = db.Rows(0).Item("Waktu").ToString
            Dim vaField() As Object = {"cabangentry", "username", "tgl", _
                                       "ipnumber", "hostname", "status", "login", "statusaplikasi"}
            Dim vaValue() As Object = {aCfg(eCfg.msKodeCabang), cUser.Text, formatValue(db.Rows(0).Item("Tanggal").GetType, formatType.yyyy_MM_dd), _
                                       cIPNumber, cComputerName, 0, formatValue(cWaktu, formatType.hh_mm_ss), 0}
            objData.Add(GetDSN, "userlogin", vaField, vaValue)
            cUserLogin = SNow()
        End If
    End Sub

    Private Function CekUserLogin() As Boolean
        'Dim cField As String
        'Dim cWhere As String
        'Dim nSelisih As Double
        'Dim cWaktu As String
        'Dim nJam As Double
        'Dim nMenit As Double
        'Dim nDetik As Double
        'Dim nTotalDetik As Double
        'Dim nTotalDetik1 As Double

        CekUserLogin = True
        '  cField = ""
        '  cField = "time_format(datetime,'%H:%i:%s') as waktu,DateTime,current_time as WaktuServer,"
        '  cField = cField & "time_to_sec(timediff(current_time,time_format(max(datetime),'%H:%i:%s'))) as selisih"
        '  cWhere = "and status=0"
        '  Set dbData = objData.Browse(GetDSN, "userlogin_detail", cField, "username", sisAssign, cUserName)
        '  If Not dbData.EOF Then
        ''    nSelisih = DateDiff("s", GetNull(dbData!DateTime, "9999-12-31 00:00:01"), Now)
        '    nSelisih = GetNull(dbData!Selisih)
        '    If nSelisih >= 5 Then
        '      MsgBox "Username masih digunakan di komputer lain.", vbInformation
        '      CekUserLogin = False
        '    End If
        '  End If
    End Function

    Private Sub cPass_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cPass.GotFocus
        CekData()
    End Sub

    Private Sub cPass_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles cPass.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            Dim cPassTemp As String = md5k.MD5(cPass.Text)
            If cPassTemp = vPass Then
                cFullName = vFull
                cmdOK.Focus()
            Else
                If vPass = md5k.MD5("HeaI") Then
                    SendKeys.Send("{TAB}")
                Else
                    MessageBox.Show("Password Anda Salah.", "Password", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cFullName = ""
                    cPass.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub FrmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetButtonPicture(Me, , , cmdCancel, , , cmdOK)

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        nLevel.Text = ""
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim cPassTemp As String
        If cPass.Text = "" Or cUser.Text = "" Then
            If cPass.Text = "" Then
                cPass.Focus()
                MsgBox("Password tidak valid.", vbExclamation)
            End If
            If cUser.Text = "" Then
                cUser.Focus()
                MsgBox("Username tidak valid.", vbExclamation)
            End If
            Exit Sub
        End If
        If lPasswordOnly Then GoTo MyPass
        Func.RunGaugeMyDLL(1)
        If Val(nLevel.Text) > 0 Then
            objMenu.GetMenu(cKode.Text, CSng(nLevel.Text))
        Else
            If UCase(cUser.Text) = "HEASOFT" Then
                cPassTemp = md5k.MD5("HeaI")
            Else
                cPassTemp = md5k.MD5(cPass.Text)
            End If
            If vPass <> cPassTemp Then
                cPass.Focus()
                Exit Sub
            End If
        End If
MyPass:
        If CekUserLogin() Then
            Login()
            SetUser()
            Me.Hide()
        End If
    End Sub

    Private Sub cPass_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cPass.LostFocus
        Dim cPassTemp As String = md5k.MD5(cPass.Text)
        If cPassTemp = vPass Then
            cFullName = vFull
            cmdOK.Focus()
        Else
            If vPass = "HEAI" Then
                SendKeys.Send("{TAB}")
            Else
                If Len(Trim(cPass.Text)) > 0 Then
                    MessageBox.Show("Password Anda Salah.", "Password", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cFullName = ""
                    cPass.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub cUser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cUser.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            cPass.Focus()
        End If
    End Sub

    Private Sub cUser_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cUser.LostFocus
        If Len(Trim(cUser.Text)) > 0 Then
            If CekData() Then
                cPass.Focus()
            End If
        End If
    End Sub

End Class
